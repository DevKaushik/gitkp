package com.kp.facedetection.interfaces;

/**
 * Created by DAT-Asset-128 on 06-12-2017.
 */

public interface ChargesheetNotificationUpdateListener {
    public void onChargesheetNotificationUpdate(String value);
}
