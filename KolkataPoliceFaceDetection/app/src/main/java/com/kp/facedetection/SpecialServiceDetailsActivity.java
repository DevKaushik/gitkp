package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kp.facedetection.adapter.SpecialServiceDetailsAdapter;
import com.kp.facedetection.adapter.SpecialServiceMcellObservationAdapter;
import com.kp.facedetection.adapter.SpecialServiceNodalSuggestionAdapter;
import com.kp.facedetection.adapter.SpecialSeviceReminderAdapter;
import com.kp.facedetection.databinding.ActivitySpecialServiceDetailsBinding;
import com.kp.facedetection.interfaces.OnPartialDisconnectionListener;
import com.kp.facedetection.interfaces.OnSpecialServiceDesignatedActionClickListener;
import com.kp.facedetection.model.AC;
import com.kp.facedetection.model.ADDLCP;
import com.kp.facedetection.model.CP;
import com.kp.facedetection.model.DC;
import com.kp.facedetection.model.Detail;
import com.kp.facedetection.model.Example;
import com.kp.facedetection.model.IO;
import com.kp.facedetection.model.JTCP;
import com.kp.facedetection.model.NotificationDetail;
import com.kp.facedetection.model.OC;
import com.kp.facedetection.model.Result;
import com.kp.facedetection.model.SuggestionModel;
import com.kp.facedetection.model.UserInfo;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static com.kp.facedetection.utility.Constants.SPECIAL_SERVICE_ARRAY;

public class SpecialServiceDetailsActivity extends BaseActivity implements View.OnClickListener, OnPartialDisconnectionListener, OnSpecialServiceDesignatedActionClickListener {
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";
    TextView chargesheetNotificationCount;
    String notificationCount = "";
    IO io;
    OC oc;
    DC dc;
    JTCP jtcp;
    AC ac;
    ADDLCP addlcp;
    CP cp;
    Result result;
    ActivitySpecialServiceDetailsBinding activitySpecialServiceDetailsBinding;
    String requesterRole = "";
    String requesterDiv = "";
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.LayoutManager layoutManagerReminder;
    RecyclerView.LayoutManager layoutManagerSuggestion;
    String status = "";
    String requestID = "";
    Example modelService = null;
    String revokeFlag = "";
    String ocApprovalRequiredFlag = "";
    String acApprovalRequiredFlag = "";
    String dcApprovalRequiredFlag = "";
    String jtcpApprovalRequiredFlag = "";
    String nodalApprovalRequiredFlag = "";
    String designatedApprovalRequiredFlag = "";
    String mcellVerificationDoneFlag = "";
    String ocApprovalDoneFlag = "";
    String acApprovalDoneFlag = "";
    String dcApprovalDoneFlag = "";
    String jtcpApprovalDoneFlag = "";
    String nodalApprovalDoneFlag = "";
    String designatedApprovalDoneFlag = "";
    String nodalApprovalDoneString="";

    String draftSaveFlag="";
    String completeSaveFlag="";

    int type = 0;
    private String[] justificationTitle;
    private String[] justificationTemplate;
    private Spinner spjustTitle;
    private EditText et_justification;
    ArrayList arrayListPartialDisconnection = new ArrayList();
    ArrayList allselectArraylist = new ArrayList();
    JSONArray jsonArrayData = new JSONArray();
    boolean alldisconnectionFlag;
    boolean allSelectFlag;
    String regretremarks = "";
    ArrayList<SuggestionModel> arraylistSugg = new ArrayList<SuggestionModel>();
    SpecialServiceNodalSuggestionAdapter specialServiceNodalSuggestionAdapter;
    SpecialServiceMcellObservationAdapter specialServiceMcellObservationAdapter;
    String requestCategory = "";
    String urgentGeneralFlag="";
    String [] requestProcessTypeArray={"URGENT","GENERAL"};
    String headerTextFir="";
    boolean isRankChanged=false;
    String unModifiedRank="";


    @Override
    public void onBackPressed(){

        UserInfo userInfo=Utility.getUserInfo(this);
        userInfo.setUserRank(unModifiedRank);
        Utility.setUserInfo(this,userInfo);
        finish();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySpecialServiceDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_special_service_details);
        setToolBar();
        getALLIntent();
        initView();
        initilizevalue();
        if(Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("NODAL OFFICER")|| Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("DESIGNATED OFFICER")) {
            showNodalSuggestion();
            showMcellSuggestion();

        }


    }

    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout) mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this, charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount = (TextView) mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount = Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }

    public void updateNotificationCount(String notificationCount) {

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        } else {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }

    //------------------------------------------Reciving value  and setting flags according the ROLE-------------------------------------------------------------------------//
    public void getALLIntent() {

        if (getIntent().hasExtra("IO_DETAILS")) {

            io = (IO) getIntent().getSerializableExtra("IO_DETAILS");
            requesterRole = "IO"; // set the request details type
            requestID = io.getId();//According the request object is setting the request id;
            requestCategory = io.getRequestType();
            requesterDiv = io.getDiv();


            try {
                revokeFlag = io.getRevoked();
                ocApprovalRequiredFlag = io.getOcApprovalFlag();
                acApprovalRequiredFlag = io.getAcApprovalFlag();
                dcApprovalRequiredFlag = io.getDcApprovalFlag();
                jtcpApprovalRequiredFlag = io.getJtcpApprovalFlag();
                mcellVerificationDoneFlag = io.getMcellActionLi();
                nodalApprovalRequiredFlag = io.getNodalApprovalFlag();
                designatedApprovalRequiredFlag = io.getDesignatedApprovalFlag();
                draftSaveFlag=io.getDraftSave();
                completeSaveFlag=io.getCompleteSave();


                ocApprovalDoneFlag = io.getApprovedByOC();
                acApprovalDoneFlag = io.getApprovedByAC();
                dcApprovalDoneFlag = io.getApprovedByDC();
                jtcpApprovalDoneFlag = io.getApprovedJtCp();
                nodalApprovalDoneFlag = io.getApprovedNodal();
                designatedApprovalDoneFlag = io.getApprovedDesignatedOficer();
            } catch (Exception e) {

            }


        }
        if (getIntent().hasExtra("OC_DETAILS")) {
            oc = (OC) getIntent().getSerializableExtra("OC_DETAILS");
            requesterRole = "OC";
            requestID = oc.getId();
            requestCategory = oc.getRequestType();
            requesterDiv = oc.getDiv();
            try {
                revokeFlag = oc.getRevoked();
                ocApprovalRequiredFlag = oc.getOcApprovalFlag();
                acApprovalRequiredFlag = oc.getAcApprovalFlag();
                dcApprovalRequiredFlag = oc.getDcApprovalFlag();
                jtcpApprovalRequiredFlag = oc.getJtcpApprovalFlag();
                mcellVerificationDoneFlag = oc.getMcellActionLi();
                nodalApprovalRequiredFlag = oc.getNodalApprovalFlag();
                designatedApprovalRequiredFlag = oc.getDesignatedApprovalFlag();
                draftSaveFlag=oc.getDraftSave();
                completeSaveFlag=oc.getCompleteSave();

                ocApprovalDoneFlag = oc.getApprovedByOC();
                acApprovalDoneFlag = oc.getApprovedByAC();
                dcApprovalDoneFlag = oc.getApprovedByDC();
                jtcpApprovalDoneFlag = oc.getApprovedJtCp();
                nodalApprovalDoneFlag = oc.getApprovedNodal();
                designatedApprovalDoneFlag = oc.getApprovedDesignatedOficer();
            } catch (Exception e) {

            }
        }
        if (getIntent().hasExtra("AC_DETAILS")) {
            ac = (AC) getIntent().getSerializableExtra("AC_DETAILS");
            requesterRole = "AC";
            requestID = ac.getId();
            requestCategory = ac.getRequestType();
            requesterDiv = ac.getDiv();
            try {
                revokeFlag = ac.getRevoked();
                ocApprovalRequiredFlag = ac.getOcApprovalFlag();
                acApprovalRequiredFlag = ac.getAcApprovalFlag();
                dcApprovalRequiredFlag = ac.getDcApprovalFlag();
                jtcpApprovalRequiredFlag = ac.getJtcpApprovalFlag();
                mcellVerificationDoneFlag = ac.getMcellActionLi();
                nodalApprovalRequiredFlag = ac.getNodalApprovalFlag();
                designatedApprovalRequiredFlag = ac.getDesignatedApprovalFlag();
                draftSaveFlag=ac.getDraftSave();
                completeSaveFlag=ac.getCompleteSave();

                ocApprovalDoneFlag = ac.getApprovedByOC();
                acApprovalDoneFlag = ac.getApprovedByAC();
                dcApprovalDoneFlag = ac.getApprovedByDC();
                jtcpApprovalDoneFlag = ac.getApprovedJtCp();
                nodalApprovalDoneFlag = ac.getApprovedNodal();
                designatedApprovalDoneFlag = ac.getApprovedDesignatedOficer();
            } catch (Exception e) {

            }
        }
        if (getIntent().hasExtra("DC_DETAILS")) {
            dc = (DC) getIntent().getSerializableExtra("DC_DETAILS");
            requesterRole = "DC";
            requestID = dc.getId();//
            requestCategory = dc.getRequestType();
            requesterDiv = dc.getDiv();
            try {
                revokeFlag = dc.getRevoked();
                ocApprovalRequiredFlag = dc.getOcApprovalFlag();
                acApprovalRequiredFlag = dc.getAcApprovalFlag();
                dcApprovalRequiredFlag = dc.getDcApprovalFlag();
                jtcpApprovalRequiredFlag = dc.getJtcpApprovalFlag();
                mcellVerificationDoneFlag = oc.getMcellActionLi();
                nodalApprovalRequiredFlag = dc.getNodalApprovalFlag();
                designatedApprovalRequiredFlag = dc.getDesignatedApprovalFlag();
                draftSaveFlag=dc.getDraftSave();
                completeSaveFlag=dc.getCompleteSave();

                ocApprovalDoneFlag = dc.getApprovedByOC();
                acApprovalDoneFlag = dc.getApprovedByAC();
                acApprovalDoneFlag = dc.getApprovedByAC();
                dcApprovalDoneFlag = dc.getApprovedByDC();
                jtcpApprovalDoneFlag = dc.getApprovedJtCp();
                nodalApprovalDoneFlag = dc.getApprovedNodal();
                designatedApprovalDoneFlag = dc.getApprovedDesignatedOficer();
            } catch (Exception e) {

            }

        }
        if (getIntent().hasExtra("JTCP_DETAILS")) {
            jtcp = (JTCP) getIntent().getSerializableExtra("JTCP_DETAILS");
            requesterRole = "JTCP";
            requestID = jtcp.getId();//
            requestCategory = jtcp.getRequestType();
            requesterDiv = jtcp.getDiv();
            try {
                revokeFlag = jtcp.getRevoked();
                ocApprovalRequiredFlag = jtcp.getOcApprovalFlag();
                acApprovalRequiredFlag = jtcp.getAcApprovalFlag();
                dcApprovalRequiredFlag = jtcp.getDcApprovalFlag();
                jtcpApprovalRequiredFlag = jtcp.getJtcpApprovalFlag();
                mcellVerificationDoneFlag = jtcp.getMcellActionLi();
                nodalApprovalRequiredFlag = jtcp.getNodalApprovalFlag();
                designatedApprovalRequiredFlag = jtcp.getDesignatedApprovalFlag();
                draftSaveFlag=jtcp.getDraftSave();
                completeSaveFlag=jtcp.getCompleteSave();

                ocApprovalDoneFlag = jtcp.getApprovedByOC();
                acApprovalDoneFlag = jtcp.getApprovedByAC();
                dcApprovalDoneFlag = jtcp.getApprovedByDC();
                jtcpApprovalDoneFlag = jtcp.getApprovedJtCp();
                nodalApprovalDoneFlag = jtcp.getApprovedNodal();
                designatedApprovalDoneFlag = jtcp.getApprovedDesignatedOficer();
            } catch (Exception e) {

            }

        }
        if (getIntent().hasExtra("ADDLCP_DETAILS")) {
            addlcp = (ADDLCP) getIntent().getSerializableExtra("ADDLCP_DETAILS");
            requesterRole = "ADDLCP";
            requestID = addlcp.getId();//
            requestCategory = addlcp.getRequestType();
            requesterDiv = addlcp.getDiv();
            try {
                revokeFlag = addlcp.getRevoked();
                ocApprovalRequiredFlag = addlcp.getOcApprovalFlag();
                acApprovalRequiredFlag = addlcp.getAcApprovalFlag();
                dcApprovalRequiredFlag = addlcp.getDcApprovalFlag();
                jtcpApprovalRequiredFlag = addlcp.getJtcpApprovalFlag();
                mcellVerificationDoneFlag = addlcp.getMcellActionLi();
                nodalApprovalRequiredFlag = addlcp.getNodalApprovalFlag();
                designatedApprovalRequiredFlag = addlcp.getDesignatedApprovalFlag();

                draftSaveFlag=addlcp.getDraftSave();
                completeSaveFlag=addlcp.getCompleteSave();

                ocApprovalDoneFlag = addlcp.getApprovedByOC();
                acApprovalDoneFlag = addlcp.getApprovedByAC();
                dcApprovalDoneFlag = addlcp.getApprovedByDC();
                jtcpApprovalDoneFlag = addlcp.getApprovedJtCp();
                nodalApprovalDoneFlag = addlcp.getApprovedNodal();
                designatedApprovalDoneFlag = addlcp.getApprovedDesignatedOficer();
            } catch (Exception e) {

            }

        }
        if (getIntent().hasExtra("CP_DETAILS")) {
            cp = (CP) getIntent().getSerializableExtra("CP_DETAILS");
            requesterRole = "CP";
            requestID = cp.getId();//
            requestCategory = cp.getRequestType();
            requesterDiv = cp.getDiv();
            try {
                revokeFlag = cp.getRevoked();
                ocApprovalRequiredFlag = cp.getOcApprovalFlag();
                acApprovalRequiredFlag = cp.getAcApprovalFlag();
                dcApprovalRequiredFlag = cp.getDcApprovalFlag();
                jtcpApprovalRequiredFlag = cp.getJtcpApprovalFlag();
                mcellVerificationDoneFlag = cp.getMcellActionLi();
                nodalApprovalRequiredFlag = cp.getNodalApprovalFlag();
                designatedApprovalRequiredFlag = cp.getDesignatedApprovalFlag();

                draftSaveFlag=cp.getDraftSave();
                completeSaveFlag=cp.getCompleteSave();

                ocApprovalDoneFlag = cp.getApprovedByOC();
                acApprovalDoneFlag = cp.getApprovedByAC();
                dcApprovalDoneFlag = cp.getApprovedByDC();
                jtcpApprovalDoneFlag = cp.getApprovedJtCp();
                nodalApprovalDoneFlag = cp.getApprovedNodal();
                designatedApprovalDoneFlag = cp.getApprovedDesignatedOficer();
            } catch (Exception e) {

            }

        }

    }

    //------------------------------------------Initiate view and showing GRANT ,SKIP  and REVOKE button checking permission according the Role-------------------------------------------------------------------------//
    public void initView() {
        activitySpecialServiceDetailsBinding.editIV.setOnClickListener(this);
        activitySpecialServiceDetailsBinding.RbGeneral.setOnClickListener(this);
        activitySpecialServiceDetailsBinding.RbUrgent.setOnClickListener(this);
        activitySpecialServiceDetailsBinding.bttnReconnectionSubmit.setOnClickListener(this);
        activitySpecialServiceDetailsBinding.bttnDisconnectionSubmit.setOnClickListener(this);
        layoutManagerSuggestion = new LinearLayoutManager(this);
        activitySpecialServiceDetailsBinding.nodalCommentRv.setLayoutManager(layoutManagerSuggestion);
        specialServiceNodalSuggestionAdapter = new SpecialServiceNodalSuggestionAdapter(this, arraylistSugg);
        activitySpecialServiceDetailsBinding.nodalCommentRv.setAdapter(specialServiceNodalSuggestionAdapter);
        unModifiedRank=Utility.getUserInfo(this).getUserRank();


        if (dcApprovalRequiredFlag.equals("1") && dcApprovalDoneFlag.equals("0")){
            if (Utility.getUserInfo(this).getIS_DC().equalsIgnoreCase("1") && Utility.getUserInfo(this).getUserDivisionCode().equalsIgnoreCase(requesterDiv)) {
                // unModifiedRank=Utility.getUserInfo(this).getUserRank();
                UserInfo userInfo = Utility.getUserInfo(this);
                userInfo.setUserRank("DC");
                Utility.setUserInfo(this, userInfo);
                isRankChanged = true;


            }
        }


        if (Utility.getUserInfo(this).getUserRank().equals("DESIGNATED OFFICER") || Utility.getUserInfo(this).getUserRank().equals("NODAL OFFICER")) {
            if (requestCategory.equals(SPECIAL_SERVICE_ARRAY[9])) {
                activitySpecialServiceDetailsBinding.nodalCommentRv.setVisibility(View.VISIBLE);
            }
        }
        if (Utility.getUserInfo(this).getUserRank().equals("NODAL OFFICER") || Utility.getUserInfo(this).getUserRank().equals("DESIGNATED OFFICER") ) {
            if (requestCategory.equals(SPECIAL_SERVICE_ARRAY[9])) {
                activitySpecialServiceDetailsBinding.RlNodalCommentInput.setVisibility(View.VISIBLE);
            }
        }





        //------------------------------------clear the view background color----------------------------------------------------------------//
        if (requestCategory.equals("LI")) {
            Log.e("DAPL", "RequestCategory:----------" + requestCategory);
            GradientDrawable drawableOC = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiOcApprovalCircle.getBackground();
            drawableOC.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableAC = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiAcApprovalCircle.getBackground();
            drawableAC.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableDC = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDcApprovalCircle.getBackground();
            drawableDC.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableJTCP = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiJtcpApprovalCircle.getBackground();
            drawableJTCP.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableMcell = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawableMcell.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableNODAL = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawableNODAL.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableDesig = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawableDesig.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawablesentMcell = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawablesentMcell.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableactivated = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
            drawableactivated.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableHsSend = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawableHsSend.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableHsRecived = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawableHsRecived.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableprovider = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawableprovider.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawablerequestActivated = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawablerequestActivated.setColor(Color.parseColor("#CECECE"));
        } else {
            Log.e("DAPL", "RequestCategory:----------" + requestCategory);
            GradientDrawable drawableOC = (GradientDrawable) activitySpecialServiceDetailsBinding.viewOcApprovedCircle.getBackground();
            drawableOC.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableAC = (GradientDrawable) activitySpecialServiceDetailsBinding.viewAcApprovedCircle.getBackground();
            drawableAC.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableDC = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDcApprovedCircle.getBackground();
            drawableDC.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableJTCP = (GradientDrawable) activitySpecialServiceDetailsBinding.viewJtcpApprovedCircle.getBackground();
            drawableJTCP.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableNODAL = (GradientDrawable) activitySpecialServiceDetailsBinding.viewNodalApprovedCircle.getBackground();
            drawableNODAL.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableDESIGNATED = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDesignatedApprovedCircle.getBackground();
            drawableDESIGNATED.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableMcell = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawableMcell.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableprovider = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawableprovider.setColor(Color.parseColor("#CECECE"));
            GradientDrawable drawableInfo = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawableInfo.setColor(Color.parseColor("#CECECE"));


        }
        //------------------------------------clear the view background color----------------------------------------------------------------//

        if (!Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("SI") && !Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("ASI(W)") &&
                !Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("LC") && !Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("L/SI") && !Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("CONST") && !Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("HG")) {

            if (Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("INSP") || Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("INSPR(W)") || Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("INSPR")) {

                    if (ocApprovalRequiredFlag.equals("1") && ocApprovalDoneFlag.equals("0")) {
                        if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setVisibility(View.VISIBLE);
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setText("GRANT REQUEST");
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setOnClickListener(this);
                                if (requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                                    checkUrgentGeneralLayoutVisibility();
                                }
                                Log.e("DAPL", "OC VALIDATE");

                        }
                    }


            } else if (Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("AC")) {

                if (acApprovalRequiredFlag.equals("1") && acApprovalDoneFlag.equals("0") && ocApprovalDoneFlag.equals("1")) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvGrantRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvGrantRequest.setText("GRANT REQUEST");
                        activitySpecialServiceDetailsBinding.tvGrantRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }


                    }
                }

                if ((ocApprovalRequiredFlag.equals("1") && ocApprovalDoneFlag.equals("0")) && (acApprovalRequiredFlag.equals("1") && acApprovalDoneFlag.equals("0"))) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setText("SKIP OC");
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }

                    }
                }


            } else if (Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("DC")) {


                if ((ocApprovalRequiredFlag.equals("1") && ocApprovalDoneFlag.equals("0")) && (dcApprovalRequiredFlag.equals("1") && dcApprovalDoneFlag.equals("0"))) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setText("SKIP OC");
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }
                    }
                } else if ((acApprovalRequiredFlag.equals("1") && acApprovalDoneFlag.equals("0")) && (dcApprovalRequiredFlag.equals("1") && dcApprovalDoneFlag.equals("0"))) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setText("SKIP AC");
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }

                    }
                }
                else  if (dcApprovalRequiredFlag.equals("1") && dcApprovalDoneFlag.equals("0") && acApprovalDoneFlag.equals("1")) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvGrantRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvGrantRequest.setText("GRANT REQUEST");
                        activitySpecialServiceDetailsBinding.tvGrantRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }

                    }
                }


            } else if (Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("JT.CP.")) {

                if (jtcpApprovalRequiredFlag.equals("1") && jtcpApprovalDoneFlag.equals("0") && dcApprovalDoneFlag.equals("1")) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvGrantRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvGrantRequest.setText("GRANT REQUEST");
                        activitySpecialServiceDetailsBinding.tvGrantRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }

                    }
                }

                if ((ocApprovalRequiredFlag.equals("1") && ocApprovalDoneFlag.equals("0")) && (jtcpApprovalRequiredFlag.equals("1") && jtcpApprovalDoneFlag.equals("0"))) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setText("SKIP OC");
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }

                    }
                } else if ((acApprovalRequiredFlag.equals("1") && acApprovalDoneFlag.equals("0")) && ocApprovalDoneFlag.equals("1") && (jtcpApprovalRequiredFlag.equals("1") && jtcpApprovalDoneFlag.equals("0"))) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setText("SKIP AC");
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }

                    }

                } else if ((dcApprovalRequiredFlag.equals("1") && dcApprovalDoneFlag.equals("0")) && acApprovalDoneFlag.equals("1") && (jtcpApprovalRequiredFlag.equals("1") && jtcpApprovalDoneFlag.equals("0"))) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setText("SKIP DC");
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }

                    }

                }

            } else if (Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("NODAL OFFICER")) {

                if (requestCategory.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[9])) {
                    if ( mcellVerificationDoneFlag.equalsIgnoreCase("1") && (nodalApprovalRequiredFlag.equals("1") && nodalApprovalDoneFlag.equals("0"))) {
                        if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setVisibility(View.VISIBLE);
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setText("GRANT REQUEST");
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setOnClickListener(this);
                            if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                                checkUrgentGeneralLayoutVisibility();
                            }


                        }
                    }
                } else {
                    if ((jtcpApprovalRequiredFlag.equals("1") && jtcpApprovalDoneFlag.equals("1")) && (nodalApprovalRequiredFlag.equals("1") && nodalApprovalDoneFlag.equals("0"))) {
                        if (revokeFlag.equals("0") ) {
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setVisibility(View.VISIBLE);
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setText("GRANT REQUEST");
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setOnClickListener(this);
                        }
                    } else if (jtcpApprovalRequiredFlag.equals("0") && (dcApprovalRequiredFlag.equals("1") && dcApprovalDoneFlag.equals("1")) && (nodalApprovalRequiredFlag.equals("1") && nodalApprovalDoneFlag.equals("0"))) {
                        if (revokeFlag.equals("0")) {
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setVisibility(View.VISIBLE);
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setText("GRANT REQUEST");
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setOnClickListener(this);
                        }
                    }
                }

                if ((ocApprovalRequiredFlag.equals("1") && ocApprovalDoneFlag.equals("0")) && (nodalApprovalRequiredFlag.equals("1") && nodalApprovalDoneFlag.equals("0"))) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setText("SKIP OC");
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }
                    }
                } else if ((acApprovalRequiredFlag.equals("1") && acApprovalDoneFlag.equals("0")) && (nodalApprovalRequiredFlag.equals("1") && nodalApprovalDoneFlag.equals("0"))) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setText("SKIP AC");
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }

                    }
                } else if ((dcApprovalRequiredFlag.equals("1") && dcApprovalDoneFlag.equals("0")) && (nodalApprovalRequiredFlag.equals("1") && nodalApprovalDoneFlag.equals("0"))) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setText("SKIP DC");
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }
                    }
                } else if ((jtcpApprovalRequiredFlag.equals("1") && jtcpApprovalDoneFlag.equals("0")) && (nodalApprovalRequiredFlag.equals("1") && nodalApprovalDoneFlag.equals("0"))) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setText("SKIP JTCP");
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }
                    }
                }


            } else if (Utility.getUserInfo(this).getUserRank().equalsIgnoreCase("DESIGNATED OFFICER")) {
                if (requestCategory.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[9])) {
                    if (  (nodalApprovalRequiredFlag.equals("1") && nodalApprovalDoneFlag.equals("1"))&&(designatedApprovalRequiredFlag.equals("1") && designatedApprovalDoneFlag.equals("0"))) {
                        if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setVisibility(View.VISIBLE);
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setText("GRANT REQUEST");
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setOnClickListener(this);
                            if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                                checkUrgentGeneralLayoutVisibility();
                            }
                        }
                    }
                } else {

                    if ((jtcpApprovalRequiredFlag.equals("1") && jtcpApprovalDoneFlag.equals("1")) && (designatedApprovalRequiredFlag.equals("1") && designatedApprovalDoneFlag.equals("0"))) {
                        if (revokeFlag.equals("0")) {
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setVisibility(View.VISIBLE);
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setText("GRANT REQUEST");
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setOnClickListener(this);


                        }
                    } else if (jtcpApprovalRequiredFlag.equals("0") && (dcApprovalRequiredFlag.equals("1") && dcApprovalDoneFlag.equals("1")) && (designatedApprovalRequiredFlag.equals("1") && designatedApprovalDoneFlag.equals("0"))) {
                        if (revokeFlag.equals("0")) {
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setVisibility(View.VISIBLE);
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setText("GRANT REQUEST");
                            activitySpecialServiceDetailsBinding.tvGrantRequest.setOnClickListener(this);
                        }
                    }
                }


                if ((ocApprovalRequiredFlag.equals("1") && ocApprovalDoneFlag.equals("0")) && (designatedApprovalRequiredFlag.equals("1") && designatedApprovalDoneFlag.equals("0"))) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setText("SKIP OC");
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }
                    }
                } else if ((acApprovalRequiredFlag.equals("1") && acApprovalDoneFlag.equals("0")) && (designatedApprovalRequiredFlag.equals("1") && designatedApprovalDoneFlag.equals("0"))) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setText("SKIP AC");
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }
                    }
                } else if ((dcApprovalRequiredFlag.equals("1") && dcApprovalDoneFlag.equals("0")) && (designatedApprovalRequiredFlag.equals("1") && designatedApprovalDoneFlag.equals("0"))) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setText("SKIP DC");
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }
                    }
                } else if ((jtcpApprovalRequiredFlag.equals("1") && jtcpApprovalDoneFlag.equals("0")) && (designatedApprovalRequiredFlag.equals("1") && designatedApprovalDoneFlag.equals("0"))) {
                    if (revokeFlag.equals("0") && completeSaveFlag.equalsIgnoreCase("1") && draftSaveFlag.equalsIgnoreCase("0")) {
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setText("SKIP JTCP");
                        activitySpecialServiceDetailsBinding.tvSkipRequest.setOnClickListener(this);
                        if(requestCategory.equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                            checkUrgentGeneralLayoutVisibility();
                        }
                    }
                }


            } else {
                activitySpecialServiceDetailsBinding.tvGrantRequest.setVisibility(View.GONE);
            }
        }
    }
    public void  checkUrgentGeneralLayoutVisibility(){
        if (requesterRole.equalsIgnoreCase("IO")) {

            if(io.getUrgentGeneralFlag().equalsIgnoreCase("1")){

                if(io.getActivated().equalsIgnoreCase("0")){
                   activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);
               }
               else if(io.getActivated().equalsIgnoreCase("1")){
                   activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                   activitySpecialServiceDetailsBinding.TvReqestProcessType.setVisibility(View.VISIBLE);
                   activitySpecialServiceDetailsBinding.TvReqestProcessType.setText(requestProcessTypeArray[0]);
               }

            }else{

                if(io.getSendToHs().equalsIgnoreCase("1")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setText(requestProcessTypeArray[1]);
                }
                else if(io.getSendToHs().equalsIgnoreCase("0")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);

                }
            }
        }
        if (requesterRole.equalsIgnoreCase("OC")) {
            if(oc.getUrgentGeneralFlag().equalsIgnoreCase("1")){
                if(oc.getActivated().equalsIgnoreCase("0")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);
                }
                else if(oc.getActivated().equalsIgnoreCase("1")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setText(requestProcessTypeArray[0]);
                }

            }else{
                if(oc.getSendToHs().equalsIgnoreCase("1")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setText(requestProcessTypeArray[1]);
                }
                else if(oc.getSendToHs().equalsIgnoreCase("0")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);

                }
            }

        }
        if (requesterRole.equalsIgnoreCase("AC")) {
            if(ac.getUrgentGeneralFlag().equalsIgnoreCase("1")){
                if(ac.getActivated().equalsIgnoreCase("0")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);
                }
                else if(ac.getActivated().equalsIgnoreCase("1")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setText(requestProcessTypeArray[0]);
                }

            }else{
                if(ac.getSendToHs().equalsIgnoreCase("1")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setText(requestProcessTypeArray[1]);
                }
                else if(ac.getSendToHs().equalsIgnoreCase("0")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);

                }
            }

        }
        if (requesterRole.equalsIgnoreCase("DC")) {
            if(dc.getUrgentGeneralFlag().equalsIgnoreCase("1")){
                if(dc.getActivated().equalsIgnoreCase("0")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);
                }
                else if(dc.getActivated().equalsIgnoreCase("1")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setText(requestProcessTypeArray[0]);
                }

            }else{
                if(dc.getSendToHs().equalsIgnoreCase("1")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setText(requestProcessTypeArray[1]);
                }
                else if(dc.getSendToHs().equalsIgnoreCase("0")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);

                }
            }

        }
        if (requesterRole.equalsIgnoreCase("JTCP")) {
            if(jtcp.getUrgentGeneralFlag().equalsIgnoreCase("1")){
                if(jtcp.getActivated().equalsIgnoreCase("0")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);
                }
                else if(jtcp.getActivated().equalsIgnoreCase("1")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setText(requestProcessTypeArray[0]);
                }

            }else{
                if(jtcp.getSendToHs().equalsIgnoreCase("1")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setText(requestProcessTypeArray[1]);
                }
                else if(jtcp.getSendToHs().equalsIgnoreCase("0")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);

                }
            }

        }
        if (requesterRole.equalsIgnoreCase("ADDLCP")) {
            if(addlcp.getUrgentGeneralFlag().equalsIgnoreCase("1")){
                if(addlcp.getActivated().equalsIgnoreCase("0")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);
                }
                else if(addlcp.getActivated().equalsIgnoreCase("1")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setText(requestProcessTypeArray[0]);
                }

            }else{
                if(addlcp.getSendToHs().equalsIgnoreCase("1")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setText(requestProcessTypeArray[1]);
                }
                else if(addlcp.getSendToHs().equalsIgnoreCase("0")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);

                }
            }

        }
        if (requesterRole.equalsIgnoreCase("CP")) {
            if(cp.getUrgentGeneralFlag().equalsIgnoreCase("1")){
                if(cp.getActivated().equalsIgnoreCase("0")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);
                }
                else if(cp.getActivated().equalsIgnoreCase("1")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setText(requestProcessTypeArray[0]);
                }

            }else{
                if(cp.getSendToHs().equalsIgnoreCase("1")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.TvReqestProcessType.setText(requestProcessTypeArray[1]);
                }
                else if(cp.getSendToHs().equalsIgnoreCase("0")){
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);

                }
            }

        }


    }


    public void initilizevalue() {

        try {


            if (requesterRole.equalsIgnoreCase("IO")) {
                //Toast.makeText(this, "requestType" + io.getRequestType(), Toast.LENGTH_LONG).show();
                Log.e("DAPL", "draftSaveFlag:------" + draftSaveFlag + "completeSaveFlag:------" + completeSaveFlag);
                //  Toast.makeText(this, "draftSaveFlag:------"+draftSaveFlag+"completeSaveFlag:------"+completeSaveFlag, Toast.LENGTH_SHORT).show();
                if (draftSaveFlag.equalsIgnoreCase("1") && completeSaveFlag.equalsIgnoreCase("0") && io.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                    activitySpecialServiceDetailsBinding.tvEditRequest.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.tvEditRequest.setOnClickListener(this);
                } else if (draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvEditRequest.setVisibility(View.GONE);
                }
                if (io.getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.RbUrgent.setChecked(true);

                } else {
                    activitySpecialServiceDetailsBinding.RbGeneral.setChecked(true);

                }
                urgentGeneralFlag = io.getUrgentGeneralFlag();

                if (io.getRequestType().equalsIgnoreCase("LI")) {
                    if (io.getActivated().equals("1") && io.getDateOfActivation() != null) {
                        activitySpecialServiceDetailsBinding.editIV.setVisibility(View.VISIBLE);
                    } else {
                        activitySpecialServiceDetailsBinding.editIV.setVisibility(View.GONE);

                    }
                    if (io.getActivated().equalsIgnoreCase("0") && io.getRevoked().equalsIgnoreCase("0") && draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setOnClickListener(this);
                        if (io.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Withdraw");
                        } else {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Regret");
                        }

                    } else {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    }

                } else {

                    if (io.getMCellToPsAction().equalsIgnoreCase("0") && io.getRevoked().equalsIgnoreCase("0") && draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setOnClickListener(this);
                        if (io.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Withdraw");
                        } else {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Regret");
                        }

                    } else {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    }


                }
                activitySpecialServiceDetailsBinding.tvRequestIDvalue.setText(io.getId());
                activitySpecialServiceDetailsBinding.tvRequestTimevalue.setText(io.getRequestTime());
                activitySpecialServiceDetailsBinding.tvRequestTypevalue.setText(io.getRequestType());
                if (io.getReconnection().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[0]);
                } else if (io.getDisconnection().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[1]);
                } else {
                    if (io.getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(URGENT)");
                    } else {
                        activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(GENERAL)");
                    }

                }

                activitySpecialServiceDetailsBinding.tvRequestingOffvalue.setText(io.getOfficerName());
                activitySpecialServiceDetailsBinding.tvRequestingOffvalue.setOnClickListener(this);
                if (io.getCaseRef().contains("_")) {
                    activitySpecialServiceDetailsBinding.tvCaseraference.setText("Case:");
                } else {
                    activitySpecialServiceDetailsBinding.tvCaseraference.setText("Enquiry:");
                }
                activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setText(io.getCaseRef());
                activitySpecialServiceDetailsBinding.tvCaseraferenceDetailsValue.setText(io.getRequestBriefDataSuperiors());
                if (activitySpecialServiceDetailsBinding.tvCaseraference.getText().toString().equalsIgnoreCase("Case:")) {
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setOnClickListener(this);
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setPaintFlags(activitySpecialServiceDetailsBinding.tvCaseraferencevalue.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setTextColor(Color.parseColor("#2353B1"));
                }
                activitySpecialServiceDetailsBinding.tvRequestedbyvalue.setText(io.getRequestedBy());
                activitySpecialServiceDetailsBinding.tvStatusvalue.setText(io.getStatusMsg());
                if (io.getRequestType().equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                    activitySpecialServiceDetailsBinding.llGroup.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.tvGroupvalue.setText(io.getGroup());
                } else {
                    activitySpecialServiceDetailsBinding.llGroup.setVisibility(View.GONE);
                }
                activitySpecialServiceDetailsBinding.bttnNodalCommentSubmit.setOnClickListener(this);
                if (requestCategory.equals(SPECIAL_SERVICE_ARRAY[9])) {
                    if (io.getJustSubject() != null) {
                        if (!io.getJustSubject().equalsIgnoreCase("")) {
                            activitySpecialServiceDetailsBinding.llJustificationHolder.setVisibility(View.VISIBLE);
                            activitySpecialServiceDetailsBinding.tvJustification.setText(io.getJustSubject());

                        } else {
                            activitySpecialServiceDetailsBinding.llJustificationHolder.setVisibility(View.GONE);
                        }
                    }
                }
                setDetails(io.getRequestType(), io.getDetails(), io.getApprovedNodal());           //-----------------------------------------------------Showing details of request-----------------------------------------//
                setNotification(io.getNotificationDetails());
                //for current status
                showStatusBarIO();                                           //----------------------------------------------------Showing Status Bar---------------------------------------------------//
                showReminderButton(io.getRequestTime());                     //----------------------------------------------------Show Reminder button after------------------------------------------//

            }
            if (requesterRole.equalsIgnoreCase("OC")) {
                if (draftSaveFlag.equalsIgnoreCase("1") && completeSaveFlag.equalsIgnoreCase("0") && oc.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                    activitySpecialServiceDetailsBinding.tvEditRequest.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.tvEditRequest.setOnClickListener(this);
                } else if (draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvEditRequest.setVisibility(View.GONE);
                }
                if (oc.getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.RbUrgent.setChecked(true);
                } else {
                    activitySpecialServiceDetailsBinding.RbGeneral.setChecked(true);
                }
                urgentGeneralFlag = oc.getUrgentGeneralFlag();

                if (oc.getRequestType().equalsIgnoreCase("LI")) {
                    if (oc.getActivated().equals("1") && oc.getDateOfActivation() != null) {
                        activitySpecialServiceDetailsBinding.editIV.setVisibility(View.VISIBLE);
                    } else {
                        activitySpecialServiceDetailsBinding.editIV.setVisibility(View.GONE);
                    }
                    if (oc.getActivated().equalsIgnoreCase("0") && oc.getRevoked().equalsIgnoreCase("0") && draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setOnClickListener(this);
                        if (oc.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Withdraw");
                        } else {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Regret");
                        }

                    } else {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    }


                } else {

                    if (oc.getMCellToPsAction().equalsIgnoreCase("0") && oc.getRevoked().equalsIgnoreCase("0") && draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setOnClickListener(this);
                        if (oc.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Withdraw");
                        } else {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Regret");
                        }

                    } else {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    }


                }
                activitySpecialServiceDetailsBinding.tvRequestIDvalue.setText(oc.getId());
                activitySpecialServiceDetailsBinding.tvRequestTimevalue.setText(oc.getRequestTime());
                activitySpecialServiceDetailsBinding.tvRequestTypevalue.setText(oc.getRequestType());
                activitySpecialServiceDetailsBinding.tvRequestingOffvalue.setText(oc.getOfficerName());
                activitySpecialServiceDetailsBinding.tvRequestingOffvalue.setOnClickListener(this);
                if (oc.getReconnection().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[0]);
                } else if (oc.getDisconnection().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[1]);
                } else {
                    if (oc.getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(URGENT)");
                    } else {
                        activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(GENERAL)");
                    }

                }

                if (oc.getCaseRef().contains("_")) {
                    activitySpecialServiceDetailsBinding.tvCaseraference.setText("Case:");
                } else {
                    activitySpecialServiceDetailsBinding.tvCaseraference.setText("Enquiry:");
                }
                activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setText(oc.getCaseRef());
                activitySpecialServiceDetailsBinding.tvCaseraferenceDetailsValue.setText(oc.getRequestBriefDataSuperiors());
                if (activitySpecialServiceDetailsBinding.tvCaseraference.getText().toString().equalsIgnoreCase("Case:")) {
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setOnClickListener(this);
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setPaintFlags(activitySpecialServiceDetailsBinding.tvCaseraferencevalue.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setTextColor(Color.parseColor("#2353B1"));
                }
                activitySpecialServiceDetailsBinding.tvRequestedbyvalue.setText(oc.getRequestedBy());
                activitySpecialServiceDetailsBinding.tvStatusvalue.setText(oc.getStatusMsg());
                if (oc.getRequestType().equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                    activitySpecialServiceDetailsBinding.llGroup.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.tvGroupvalue.setText(oc.getGroup());
                } else {
                    activitySpecialServiceDetailsBinding.llGroup.setVisibility(View.GONE);
                }
                if (requestCategory.equals(SPECIAL_SERVICE_ARRAY[9])) {
                    if (oc.getJustSubject() != null) {
                        if (!oc.getJustSubject().equalsIgnoreCase("")) {
                            activitySpecialServiceDetailsBinding.llJustificationHolder.setVisibility(View.VISIBLE);
                            activitySpecialServiceDetailsBinding.tvJustification.setText(oc.getJustSubject());

                        } else {
                            activitySpecialServiceDetailsBinding.llJustificationHolder.setVisibility(View.GONE);
                        }
                    }
                }
                setDetails(oc.getRequestType(), oc.getDetails(), oc.getApprovedNodal());     //-----------------------------------------------------Showing details of request-----------------------------------------//
                setNotification(oc.getNotificationDetails());
                showStatusBarOC();                                    //----------------------------------------------------Showing Status Bar---------------------------------------------------//
                showReminderButton(oc.getRequestTime());             //----------------------------------------------------Show Reminder button after 2days------------------------------------------//


            }

            if (requesterRole.equalsIgnoreCase("AC")) {
                if (draftSaveFlag.equalsIgnoreCase("1") && completeSaveFlag.equalsIgnoreCase("0") && ac.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                    activitySpecialServiceDetailsBinding.tvEditRequest.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.tvEditRequest.setOnClickListener(this);
                } else if (draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvEditRequest.setVisibility(View.GONE);
                }
                if (ac.getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.RbUrgent.setChecked(true);
                } else {
                    activitySpecialServiceDetailsBinding.RbGeneral.setChecked(true);
                }
                urgentGeneralFlag = ac.getUrgentGeneralFlag();

                if (ac.getRequestType().equalsIgnoreCase("LI")) {
                    if (ac.getActivated().equals("1") && ac.getDateOfActivation() != null) {
                        activitySpecialServiceDetailsBinding.editIV.setVisibility(View.VISIBLE);
                    } else {
                        activitySpecialServiceDetailsBinding.editIV.setVisibility(View.GONE);

                    }
                    if (ac.getActivated().equalsIgnoreCase("0") && ac.getRevoked().equalsIgnoreCase("0") && draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setOnClickListener(this);
                        if (ac.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Withdraw");
                        } else {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Regret");
                        }

                    } else {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    }


                } else {

                    if (ac.getMCellToPsAction().equalsIgnoreCase("0") && ac.getRevoked().equalsIgnoreCase("0") && draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setOnClickListener(this);
                        if (ac.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Withdraw");
                        } else {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Regret");
                        }

                    } else {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    }


                }

                activitySpecialServiceDetailsBinding.tvRequestIDvalue.setText(ac.getId());
                activitySpecialServiceDetailsBinding.tvRequestTimevalue.setText(ac.getRequestTime());
                activitySpecialServiceDetailsBinding.tvRequestTypevalue.setText(ac.getRequestType());
                activitySpecialServiceDetailsBinding.tvRequestingOffvalue.setText(ac.getOfficerName());
                activitySpecialServiceDetailsBinding.tvRequestingOffvalue.setOnClickListener(this);
                if (ac.getReconnection().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[0]);
                } else if (ac.getDisconnection().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[1]);
                } else {

                    if (ac.getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(URGENT)");
                    } else {
                        activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(GENERAL)");
                    }


                }

                if (ac.getCaseRef().contains("_")) {
                    activitySpecialServiceDetailsBinding.tvCaseraference.setText("Case:");
                } else {
                    activitySpecialServiceDetailsBinding.tvCaseraference.setText("Enquiry:");
                }
                activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setText(ac.getCaseRef());
                activitySpecialServiceDetailsBinding.tvCaseraferenceDetailsValue.setText(ac.getRequestBriefDataSuperiors());
                if (activitySpecialServiceDetailsBinding.tvCaseraference.getText().toString().equalsIgnoreCase("Case:")) {
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setOnClickListener(this);
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setPaintFlags(activitySpecialServiceDetailsBinding.tvCaseraferencevalue.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setTextColor(Color.parseColor("#2353B1"));
                }
                activitySpecialServiceDetailsBinding.tvRequestedbyvalue.setText(ac.getRequestedBy());
                activitySpecialServiceDetailsBinding.tvStatusvalue.setText(ac.getStatusMsg());
                if (ac.getRequestType().equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                    activitySpecialServiceDetailsBinding.llGroup.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.tvGroupvalue.setText(ac.getGroup());
                } else {
                    activitySpecialServiceDetailsBinding.llGroup.setVisibility(View.GONE);
                }
                if (requestCategory.equals(SPECIAL_SERVICE_ARRAY[9])) {
                    if (ac.getJustSubject() != null) {
                        if (!ac.getJustSubject().equalsIgnoreCase("")) {
                            activitySpecialServiceDetailsBinding.llJustificationHolder.setVisibility(View.VISIBLE);
                            activitySpecialServiceDetailsBinding.tvJustification.setText(ac.getJustSubject());

                        } else {
                            activitySpecialServiceDetailsBinding.llJustificationHolder.setVisibility(View.GONE);
                        }
                    }
                }
                setDetails(ac.getRequestType(), ac.getDetails(), ac.getApprovedNodal());     //-----------------------------------------------------Showing details of request-----------------------------------------//
                setNotification(ac.getNotificationDetails());
                showStatusBarAC();
                showReminderButton(ac.getRequestTime());


            }


            if (requesterRole.equalsIgnoreCase("DC")) {
                if (draftSaveFlag.equalsIgnoreCase("1") && completeSaveFlag.equalsIgnoreCase("0") && dc.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                    activitySpecialServiceDetailsBinding.tvEditRequest.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.tvEditRequest.setOnClickListener(this);
                } else if (draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvEditRequest.setVisibility(View.GONE);
                }
                if (dc.getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.RbUrgent.setChecked(true);
                } else {
                    activitySpecialServiceDetailsBinding.RbGeneral.setChecked(true);
                }
                urgentGeneralFlag = dc.getUrgentGeneralFlag();


                if (dc.getRequestType().equalsIgnoreCase("LI")) {
                    if (dc.getActivated().equals("1") && dc.getDateOfActivation() != null) {
                        activitySpecialServiceDetailsBinding.editIV.setVisibility(View.VISIBLE);
                    } else {
                        activitySpecialServiceDetailsBinding.editIV.setVisibility(View.GONE);

                    }
                    if (dc.getActivated().equalsIgnoreCase("0") && dc.getRevoked().equalsIgnoreCase("0") && draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setOnClickListener(this);
                        if (dc.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Withdraw");
                        } else {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Regret");
                        }

                    } else {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    }


                } else {

                    if (dc.getMCellToPsAction().equalsIgnoreCase("0") && dc.getRevoked().equalsIgnoreCase("0") && draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setOnClickListener(this);
                        if (dc.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Withdraw");
                        } else {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Regret");
                        }

                    } else {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    }


                }
                activitySpecialServiceDetailsBinding.tvRequestIDvalue.setText(dc.getId());
                activitySpecialServiceDetailsBinding.tvRequestTimevalue.setText(dc.getRequestTime());
                activitySpecialServiceDetailsBinding.tvRequestTypevalue.setText(dc.getRequestType());
                activitySpecialServiceDetailsBinding.tvRequestingOffvalue.setText(dc.getOfficerName());
                activitySpecialServiceDetailsBinding.tvRequestingOffvalue.setOnClickListener(this);
                if (dc.getReconnection().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[0]);
                } else if (dc.getDisconnection().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[1]);
                } else {
                    if (dc.getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(URGENT)");
                    } else {
                        activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(GENERAL)");
                    }

                }

                if (dc.getCaseRef().contains("_")) {
                    activitySpecialServiceDetailsBinding.tvCaseraference.setText("Case:");
                } else {
                    activitySpecialServiceDetailsBinding.tvCaseraference.setText("Enquiry:");
                }
                activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setText(dc.getCaseRef());
                activitySpecialServiceDetailsBinding.tvCaseraferenceDetailsValue.setText(dc.getRequestBriefDataSuperiors());
                if (activitySpecialServiceDetailsBinding.tvCaseraference.getText().toString().equalsIgnoreCase("Case:")) {
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setOnClickListener(this);
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setPaintFlags(activitySpecialServiceDetailsBinding.tvCaseraferencevalue.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setTextColor(Color.parseColor("#2353B1"));
                }
                activitySpecialServiceDetailsBinding.tvRequestedbyvalue.setText(dc.getRequestedBy());
                activitySpecialServiceDetailsBinding.tvStatusvalue.setText(dc.getStatusMsg());
                if (dc.getRequestType().equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                    activitySpecialServiceDetailsBinding.llGroup.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.tvGroupvalue.setText(dc.getGroup());
                } else {
                    activitySpecialServiceDetailsBinding.llGroup.setVisibility(View.GONE);
                }
                if (requestCategory.equals(SPECIAL_SERVICE_ARRAY[9])) {
                    if (dc.getJustSubject() != null) {
                        if (!dc.getJustSubject().equalsIgnoreCase("")) {
                            activitySpecialServiceDetailsBinding.llJustificationHolder.setVisibility(View.VISIBLE);
                            activitySpecialServiceDetailsBinding.tvJustification.setText(dc.getJustSubject());

                        } else {
                            activitySpecialServiceDetailsBinding.llJustificationHolder.setVisibility(View.GONE);
                        }
                    }
                }
                setDetails(dc.getRequestType(), dc.getDetails(), dc.getApprovedNodal());
                setNotification(dc.getNotificationDetails());
                showStatusBarDC();
                showReminderButton(dc.getRequestTime());
            }

            if (requesterRole.equalsIgnoreCase("JTCP")) {
                if (draftSaveFlag.equalsIgnoreCase("1") && completeSaveFlag.equalsIgnoreCase("0") && jtcp.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                    activitySpecialServiceDetailsBinding.tvEditRequest.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.tvEditRequest.setOnClickListener(this);
                } else if (draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvEditRequest.setVisibility(View.GONE);
                }
                if (jtcp.getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.RbUrgent.setChecked(true);
                } else {
                    activitySpecialServiceDetailsBinding.RbGeneral.setChecked(true);
                }
                urgentGeneralFlag = jtcp.getUrgentGeneralFlag();

                if (jtcp.getRequestType().equalsIgnoreCase("LI")) {
                    if (jtcp.getActivated().equals("1") && jtcp.getDateOfActivation() != null) {
                        activitySpecialServiceDetailsBinding.editIV.setVisibility(View.VISIBLE);
                    } else {
                        activitySpecialServiceDetailsBinding.editIV.setVisibility(View.GONE);

                    }
                    if (jtcp.getActivated().equalsIgnoreCase("0") && jtcp.getRevoked().equalsIgnoreCase("0") && draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setOnClickListener(this);
                        if (jtcp.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Withdraw");
                        } else {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Regret");
                        }

                    } else {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    }


                } else {

                    if (jtcp.getMCellToPsAction().equalsIgnoreCase("0") && jtcp.getRevoked().equalsIgnoreCase("0") && draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setOnClickListener(this);
                        if (jtcp.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Withdraw");
                        } else {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Regret");
                        }

                    } else {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    }


                }
                if (jtcp.getDcApprovalFlag().equals("1") && jtcp.getUrgentGeneralFlag().equals("0")) {
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.VISIBLE);
                } else if (jtcp.getDcApprovalFlag().equals("1") && jtcp.getUrgentGeneralFlag().equals("1")) {
                    activitySpecialServiceDetailsBinding.RlReqestProcessType.setVisibility(View.GONE);
                }

                activitySpecialServiceDetailsBinding.tvRequestIDvalue.setText(jtcp.getId());
                activitySpecialServiceDetailsBinding.tvRequestTimevalue.setText(jtcp.getRequestTime());
                activitySpecialServiceDetailsBinding.tvRequestTypevalue.setText(jtcp.getRequestType());
                activitySpecialServiceDetailsBinding.tvRequestingOffvalue.setText(jtcp.getOfficerName());
                activitySpecialServiceDetailsBinding.tvRequestingOffvalue.setOnClickListener(this);
                if (jtcp.getReconnection().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[0]);
                } else if (jtcp.getDisconnection().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[1]);
                } else {

                    if (jtcp.getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(URGENT)");
                    } else {
                        activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(GENERAL)");
                    }


                }

                if (jtcp.getCaseRef().contains("_")) {
                    activitySpecialServiceDetailsBinding.tvCaseraference.setText("Case:");
                } else {
                    activitySpecialServiceDetailsBinding.tvCaseraference.setText("Enquiry:");
                }
                activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setText(jtcp.getCaseRef());
                activitySpecialServiceDetailsBinding.tvCaseraferenceDetailsValue.setText(jtcp.getRequestBriefDataSuperiors());
                if (activitySpecialServiceDetailsBinding.tvCaseraference.getText().toString().equalsIgnoreCase("Case:")) {
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setOnClickListener(this);
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setPaintFlags(activitySpecialServiceDetailsBinding.tvCaseraferencevalue.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setTextColor(Color.parseColor("#2353B1"));
                }
                activitySpecialServiceDetailsBinding.tvRequestedbyvalue.setText(jtcp.getRequestedBy());
                activitySpecialServiceDetailsBinding.tvStatusvalue.setText(jtcp.getStatusMsg());
                if (jtcp.getRequestType().equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                    activitySpecialServiceDetailsBinding.llGroup.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.tvGroupvalue.setText(jtcp.getGroup());
                } else {
                    activitySpecialServiceDetailsBinding.llGroup.setVisibility(View.GONE);
                }
                if (requestCategory.equals(SPECIAL_SERVICE_ARRAY[9])) {
                    if (jtcp.getJustSubject() != null) {
                        if (!jtcp.getJustSubject().equalsIgnoreCase("")) {
                            activitySpecialServiceDetailsBinding.llJustificationHolder.setVisibility(View.VISIBLE);
                            activitySpecialServiceDetailsBinding.tvJustification.setText(jtcp.getJustSubject());

                        } else {
                            activitySpecialServiceDetailsBinding.llJustificationHolder.setVisibility(View.GONE);
                        }
                    }
                }
                setDetails(jtcp.getRequestType(), jtcp.getDetails(), jtcp.getApprovedNodal());
                setNotification(jtcp.getNotificationDetails());
                showStatusBarJTCP();
                showReminderButton(jtcp.getRequestTime());
            }
            if (requesterRole.equalsIgnoreCase("ADDLCP")) {
                if (draftSaveFlag.equalsIgnoreCase("1") && completeSaveFlag.equalsIgnoreCase("0") && addlcp.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                    activitySpecialServiceDetailsBinding.tvEditRequest.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.tvEditRequest.setOnClickListener(this);
                } else if (draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvEditRequest.setVisibility(View.GONE);
                }
                if (addlcp.getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.RbUrgent.setChecked(true);
                } else {
                    activitySpecialServiceDetailsBinding.RbGeneral.setChecked(true);
                }
                urgentGeneralFlag = addlcp.getUrgentGeneralFlag();

                if (addlcp.getRequestType().equalsIgnoreCase("LI")) {
                    if (addlcp.getActivated().equals("1") && addlcp.getDateOfActivation() != null) {
                        activitySpecialServiceDetailsBinding.editIV.setVisibility(View.VISIBLE);
                    } else {
                        activitySpecialServiceDetailsBinding.editIV.setVisibility(View.GONE);

                    }
                    if (addlcp.getActivated().equalsIgnoreCase("0") && addlcp.getRevoked().equalsIgnoreCase("0") && draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setOnClickListener(this);
                        if (addlcp.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Withdraw");
                        } else {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Regret");
                        }

                    } else {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    }


                } else {

                    if (addlcp.getMCellToPsAction().equalsIgnoreCase("0") && addlcp.getRevoked().equalsIgnoreCase("0") && draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setOnClickListener(this);
                        if (addlcp.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Withdraw");
                        } else {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Regret");
                        }

                    } else {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    }


                }
                activitySpecialServiceDetailsBinding.tvRequestIDvalue.setText(addlcp.getId());
                activitySpecialServiceDetailsBinding.tvRequestTimevalue.setText(addlcp.getRequestTime());
                activitySpecialServiceDetailsBinding.tvRequestTypevalue.setText(addlcp.getRequestType());
                activitySpecialServiceDetailsBinding.tvRequestingOffvalue.setText(addlcp.getOfficerName());
                activitySpecialServiceDetailsBinding.tvRequestingOffvalue.setOnClickListener(this);
                if (addlcp.getReconnection().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[0]);
                } else if (addlcp.getDisconnection().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[1]);
                } else {
                    if (addlcp.getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(URGENT)");
                    } else {
                        activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(GENERAL)");
                    }

                }

                if (addlcp.getCaseRef().contains("_")) {
                    activitySpecialServiceDetailsBinding.tvCaseraference.setText("Case:");
                } else {
                    activitySpecialServiceDetailsBinding.tvCaseraference.setText("Enquiry:");
                }
                activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setText(addlcp.getCaseRef());
                activitySpecialServiceDetailsBinding.tvCaseraferenceDetailsValue.setText(addlcp.getRequestBriefDataSuperiors());
                if (activitySpecialServiceDetailsBinding.tvCaseraference.getText().toString().equalsIgnoreCase("Case:")) {
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setOnClickListener(this);
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setPaintFlags(activitySpecialServiceDetailsBinding.tvCaseraferencevalue.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setTextColor(Color.parseColor("#2353B1"));
                }
                activitySpecialServiceDetailsBinding.tvRequestedbyvalue.setText(addlcp.getRequestedBy());
                activitySpecialServiceDetailsBinding.tvStatusvalue.setText(addlcp.getStatusMsg());
                if (addlcp.getRequestType().equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                    activitySpecialServiceDetailsBinding.llGroup.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.tvGroupvalue.setText(addlcp.getGroup());
                } else {
                    activitySpecialServiceDetailsBinding.llGroup.setVisibility(View.GONE);
                }
                if (requestCategory.equals(SPECIAL_SERVICE_ARRAY[9])) {
                    if (addlcp.getJustSubject() != null) {
                        if (!addlcp.getJustSubject().equalsIgnoreCase("")) {
                            activitySpecialServiceDetailsBinding.llJustificationHolder.setVisibility(View.VISIBLE);
                            activitySpecialServiceDetailsBinding.tvJustification.setText(addlcp.getJustSubject());

                        } else {
                            activitySpecialServiceDetailsBinding.llJustificationHolder.setVisibility(View.GONE);
                        }
                    }
                }
                setDetails(addlcp.getRequestType(), addlcp.getDetails(), addlcp.getApprovedNodal());
                setNotification(addlcp.getNotificationDetails());
                showStatusBarADDLCP();
                showReminderButton(addlcp.getRequestTime());

            }
            if (requesterRole.equalsIgnoreCase("CP")) {
                if (draftSaveFlag.equalsIgnoreCase("1") && completeSaveFlag.equalsIgnoreCase("0") && cp.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                    activitySpecialServiceDetailsBinding.tvEditRequest.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.tvEditRequest.setOnClickListener(this);
                } else if (draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvEditRequest.setVisibility(View.GONE);
                }
                if (cp.getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.RbUrgent.setChecked(true);
                } else {
                    activitySpecialServiceDetailsBinding.RbGeneral.setChecked(true);
                }
                urgentGeneralFlag = cp.getUrgentGeneralFlag();

                if (cp.getRequestType().equalsIgnoreCase("LI")) {
                    if (cp.getActivated().equals("1") && cp.getDateOfActivation() != null) {
                        activitySpecialServiceDetailsBinding.editIV.setVisibility(View.VISIBLE);
                    } else {
                        activitySpecialServiceDetailsBinding.editIV.setVisibility(View.GONE);
                    }
                    if (cp.getActivated().equalsIgnoreCase("0") && cp.getRevoked().equalsIgnoreCase("0") && draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setOnClickListener(this);
                        if (cp.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Withdraw");
                        } else {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Regret");
                        }

                    } else {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    }


                } else {

                    if (cp.getMCellToPsAction().equalsIgnoreCase("0") && cp.getRevoked().equalsIgnoreCase("0") && draftSaveFlag.equalsIgnoreCase("0") && completeSaveFlag.equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setOnClickListener(this);
                        if (cp.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Withdraw");
                        } else {
                            activitySpecialServiceDetailsBinding.tvRevokeRequest.setText("Regret");
                        }

                    } else {
                        activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    }


                }
                activitySpecialServiceDetailsBinding.tvRequestIDvalue.setText(cp.getId());
                activitySpecialServiceDetailsBinding.tvRequestTimevalue.setText(cp.getRequestTime());
                activitySpecialServiceDetailsBinding.tvRequestTypevalue.setText(cp.getRequestType());
                activitySpecialServiceDetailsBinding.tvRequestingOffvalue.setText(cp.getOfficerName());
                activitySpecialServiceDetailsBinding.tvRequestingOffvalue.setOnClickListener(this);
                if (cp.getReconnection().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[0]);
                } else if (cp.getDisconnection().equalsIgnoreCase("1")) {
                    activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[1]);
                } else {

                    if (cp.getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                        activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(URGENT)");
                    } else {
                        activitySpecialServiceDetailsBinding.tvRequestSubTypevalue.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(GENERAL)");
                    }

                }
                if (cp.getCaseRef().contains("_")) {
                    activitySpecialServiceDetailsBinding.tvCaseraference.setText("Case:");

                } else {
                    activitySpecialServiceDetailsBinding.tvCaseraference.setText("Enquiry:");
                }
                activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setText(cp.getCaseRef());
                activitySpecialServiceDetailsBinding.tvCaseraferenceDetailsValue.setText(cp.getRequestBriefDataSuperiors());
                if (activitySpecialServiceDetailsBinding.tvCaseraference.getText().toString().equalsIgnoreCase("Case:")) {
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setOnClickListener(this);
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setPaintFlags(activitySpecialServiceDetailsBinding.tvCaseraferencevalue.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    activitySpecialServiceDetailsBinding.tvCaseraferencevalue.setTextColor(Color.parseColor("#2353B1"));
                }
                activitySpecialServiceDetailsBinding.tvRequestedbyvalue.setText(cp.getRequestedBy());
                activitySpecialServiceDetailsBinding.tvStatusvalue.setText(cp.getStatusMsg());
                if (cp.getRequestType().equalsIgnoreCase(SPECIAL_SERVICE_ARRAY[9])) {
                    activitySpecialServiceDetailsBinding.llGroup.setVisibility(View.VISIBLE);
                    activitySpecialServiceDetailsBinding.tvGroupvalue.setText(cp.getGroup());
                } else {
                    activitySpecialServiceDetailsBinding.llGroup.setVisibility(View.GONE);
                }
                if (requestCategory.equals(SPECIAL_SERVICE_ARRAY[9])) {
                    if (cp.getJustSubject() != null) {
                        if (!cp.getJustSubject().equalsIgnoreCase("")) {
                            activitySpecialServiceDetailsBinding.llJustificationHolder.setVisibility(View.VISIBLE);
                            activitySpecialServiceDetailsBinding.tvJustification.setText(cp.getJustSubject());

                        } else {
                            activitySpecialServiceDetailsBinding.llJustificationHolder.setVisibility(View.GONE);
                        }
                    }
                }

                setDetails(cp.getRequestType(), cp.getDetails(), cp.getApprovedNodal());
                setNotification(cp.getNotificationDetails());
                showStatusBarCP();
                showReminderButton(cp.getRequestTime());
            }
            //for current status
            selectAllcreatejsonArray();
        }catch(Exception e){

        }

    }

    public void showNodalSuggestion() {
        //if(Utility.getUserInfo(this).getUserRank().equals("")|| Utility.getUserInfo(this).getUserRank().equals("")){
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_LI_REQUEST_NODAL_SUGGESTION_ONLOAD_DETAILS);
        taskManager.setLINodalSuggestionOnLoadingDetails(true);
        if (requesterRole.equalsIgnoreCase("IO")) {
            String[] keys = {"request_id",};
            String[] values = {io.getId()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("OC")) {
            String[] keys = {"request_id"};
            String[] values = {oc.getId()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("DC")) {
            String[] keys = {"request_id"};
            String[] values = {dc.getId()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("JTCP")) {
            String[] keys = {"request_id"};
            String[] values = {jtcp.getId()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("ADDLCP")) {
            String[] keys = {"request_id"};
            String[] values = {addlcp.getId()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("CP")) {
            String[] keys = {"request_id"};
            String[] values = {cp.getId()};
            taskManager.doStartTask(keys, values, true, true);
        }
        // }

    }
    public void showMcellSuggestion(){
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_LI_MCELL_OBSERBATION_DETAILS);
        taskManager.setLIMcellObservationDetails(true);
        if (requesterRole.equalsIgnoreCase("IO")) {
            String[] keys = {"request_id",};
            String[] values = {io.getId()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("OC")) {
            String[] keys = {"request_id"};
            String[] values = {oc.getId()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("DC")) {
            String[] keys = {"request_id"};
            String[] values = {dc.getId()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("JTCP")) {
            String[] keys = {"request_id"};
            String[] values = {jtcp.getId()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("ADDLCP")) {
            String[] keys = {"request_id"};
            String[] values = {addlcp.getId()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("CP")) {
            String[] keys = {"request_id"};
            String[] values = {cp.getId()};
            taskManager.doStartTask(keys, values, true, true);
        }
    }
    public void parseMcellObservationResult(String response) {
        Log.e("DAPL",response);
        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optString("status").equals("1")) {

                    JSONArray jsonArray = jsonObject.optJSONArray("result");
                    activitySpecialServiceDetailsBinding.RlMcellObservationInfo.setVisibility(View.VISIBLE);
                    if (jsonArray.length() > 0) {
                        for (int j = 0; j < jsonArray.length(); j++) {
                            JSONObject jo = jsonArray.optJSONObject(j);
                            activitySpecialServiceDetailsBinding.tvMcellObservation.setVisibility(View.VISIBLE);
                            String obs=jo.optString("overview_details");
                            String modObs=obs.replace("\n","<br>");
                            Log.e("DAPL","MODTEXT--"+modObs);
                            activitySpecialServiceDetailsBinding.tvMcellObservation.setText(Html.fromHtml(modObs));

                        }

                    }
                } else {

                }
            } catch (Exception e) {

            }
        }

    }


    public void showReminderButton(String requestTime) {
        Date requestDate = null;
        Date currentDate = null;
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currDate = sdf.format(c.getTime());
            currentDate = sdf.parse(currDate);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            requestDate = format.parse(requestTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        //in milliseconds
        long diff = currentDate.getTime() - requestDate.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);
        Log.e("DAPL","DATE DIFF:-------------"+diffDays);

       if (diffDays > 0) {
            if (requesterRole.equalsIgnoreCase("IO")) {

                if (requestCategory.equalsIgnoreCase("LI")) {
                    if(io.getDateOfActivation() == null) {
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setOnClickListener(this);
                    }
                }
                else{
                    if(!io.getMCellToPsAction().equalsIgnoreCase("1")){
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setOnClickListener(this);
                    }

                }


            }
            else if (requesterRole.equalsIgnoreCase("OC")) {
                if (requestCategory.equalsIgnoreCase("LI")) {
                    if(oc.getDateOfActivation() == null) {
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setOnClickListener(this);
                    }
                }
                else{
                    if(oc.getMCellToPsAction().equalsIgnoreCase("1")){
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setOnClickListener(this);
                    }

                }
            }
            else if (requesterRole.equalsIgnoreCase("DC")) {
                if (requestCategory.equalsIgnoreCase("LI")) {
                    if(dc.getDateOfActivation() == null) {
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setOnClickListener(this);
                    }
                }
                else{
                    if(dc.getMCellToPsAction().equalsIgnoreCase("1")){
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setOnClickListener(this);
                    }

                }
            }
            else if (requesterRole.equalsIgnoreCase("JTCP")) {
                if (requestCategory.equalsIgnoreCase("LI")) {
                    if(jtcp.getDateOfActivation() == null) {
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setOnClickListener(this);
                    }
                }
                else{
                    if(jtcp.getMCellToPsAction().equalsIgnoreCase("1")){
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setOnClickListener(this);
                    }

                }
            }
            else if (requesterRole.equalsIgnoreCase("ADDLCP")) {
                if (requestCategory.equalsIgnoreCase("LI")) {
                    if(addlcp.getDateOfActivation() == null) {
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setOnClickListener(this);
                    }
                }
                else{
                    if(addlcp.getMCellToPsAction().equalsIgnoreCase("1")){
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setOnClickListener(this);
                    }

                }
            }
            else if (requesterRole.equalsIgnoreCase("CP")) {
                if (requestCategory.equalsIgnoreCase("LI")) {
                    if(cp.getDateOfActivation() == null) {
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setOnClickListener(this);
                    }
                }
                else{
                    if(cp.getMCellToPsAction().equalsIgnoreCase("1")){
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setVisibility(View.VISIBLE);
                        activitySpecialServiceDetailsBinding.bttnReminderSubmit.setOnClickListener(this);
                    }

                }
            }

        }
    }

    public void showStatusBarIO() {

        Log.e("DAPL", "REQUEST TYPE:" + io.getRequestType());
        if (io.getRequestType().equalsIgnoreCase("LI")) {


            activitySpecialServiceDetailsBinding.llLiHolder.setVisibility(View.VISIBLE);
            if(urgentGeneralFlag.equalsIgnoreCase("1")){


                activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.VISIBLE);
            }
            else{


                activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.GONE);
            }
            StatusLIIO();
        } else {
            activitySpecialServiceDetailsBinding.llAllRequestExceptLI.setVisibility(View.VISIBLE);
            StatusOthersIO();

        }


    }


    private void StatusLIIO() {
        if (io.getOcApprovalFlag().equalsIgnoreCase("1") && io.getApprovedByOC().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiOcApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            //activitySpecialServiceDetailsBinding.viewLiOcApprovalCircle.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiOcApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiOc.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiOcApprovalDateTime.setText(io.getApprovedByOCTime());
            activitySpecialServiceDetailsBinding.tvLiOcApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiOcApprovalName.setText(io.getApprovedOCName());
            activitySpecialServiceDetailsBinding.tvLiOcApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiOcApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiOcApprovalMessage.setText("APPROVED BY OC");

        } else if (io.getOcApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiOcApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            // activitySpecialServiceDetailsBinding.viewLiOcApprovalCircle.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiOcApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiOcApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvOcApprovalMessage.setText("APPROVED BY OC");

        } else {
            activitySpecialServiceDetailsBinding.rlLiOcApprovalHolder.setVisibility(View.GONE);

        }
        if (io.getAcApprovalFlag().equalsIgnoreCase("1") && io.getApprovedByAC().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiAcApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiAcApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiAc.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiAcApprovalDateTime.setText(io.getApprovedACTime());
            activitySpecialServiceDetailsBinding.tvLiAcApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiAcApprovalName.setText(io.getApprovedACName());
            activitySpecialServiceDetailsBinding.tvLiAcApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiAcApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiAcApprovalMessage.setText("APPROVED BY AC");
        } else if (io.getAcApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiAcApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiAcApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiAcApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlLiAcApprovalHolder.setVisibility(View.GONE);

        }
        if (io.getDcApprovalFlag().equalsIgnoreCase("1") && io.getApprovedByDC().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDcApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiDcApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiDc.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiDcApprovalDateTime.setText(io.getApprovedByDCTime());
            activitySpecialServiceDetailsBinding.tvLiDcApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDcApprovalName.setText(io.getApprovedDCName());
            activitySpecialServiceDetailsBinding.tvLiDcApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDcApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDcApprovalMessage.setText("APPROVED BY DC");
        } else if (io.getDcApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDcApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiDcApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiDcApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlLiDcApprovalHolder.setVisibility(View.GONE);

        }
        if (io.getJtcpApprovalFlag().equalsIgnoreCase("1") && io.getApprovedJtCp().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiJtcpApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiJtcpApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiJtcp.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalDateTime.setText(io.getApprovedJtCpTime());
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalName.setText(io.getApprovedJtCpName());
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalMessage.setText("APPROVED BY JTCP");
        } else if (io.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiJtcpApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiJtcpApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        } else {
            activitySpecialServiceDetailsBinding.rlLiJtcpApprovalHolder.setVisibility(View.GONE);

        }
        if (io.getMcellActionLi().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiMcellApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiMcell.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalDateTime.setText(io.getMcellActionLiDatetime());
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalName.setText(io.getMcellActionLiBy());
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setText("VERIFIED BY MCELL");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiMcellApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        }
        if (io.getNodalApprovalFlag().equalsIgnoreCase("1") && io.getApprovedNodal().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiNodalApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiNodal.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalDateTime.setText(io.getApprovedNodalTime());
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalName.setText(io.getApprovedNodalName());
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setText("APPROVED BY NODAL OFFICER");
        } else if (io.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiNodalApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlLiNodalApprovalHolder.setVisibility(View.GONE);

        }

        if (io.getDesignatedApprovalFlag().equalsIgnoreCase("1") && io.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiDesignated.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalDateTime.setText(io.getApprovedDesignatedOfficerTime());
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalName.setText(io.getApprovedDesignatedOfficerName());
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setText("APPROVED BY DESIGNATED OFFICER ");
        } else if (io.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlLiDesignatedApprovalHolder.setVisibility(View.GONE);

        }
        String approvedSucessFlag = "";
        if (io.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            if (io.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (io.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            if (io.getApprovedNodal().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (io.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            if (io.getApprovedJtCp().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (io.getDcApprovalFlag().equalsIgnoreCase("1") || io.getDcApprovalFlag().equalsIgnoreCase("0")) {
            if (io.getApprovedByDC().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        }
        if (approvedSucessFlag.equalsIgnoreCase("1") && io.getMcellRead().equalsIgnoreCase("0")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));

        } else if (approvedSucessFlag.equalsIgnoreCase("1") && io.getMcellRead().equalsIgnoreCase("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiMcellSendLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiMcellSend.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiMcellSendDateTime.setText(io.getMcellReadTime());
            activitySpecialServiceDetailsBinding.tvLiMcellSendDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendName.setText(io.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvLiMcellSendName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendMessage.setText("Letter preparation by MCELL");
        }
        if (io.getUrgentGeneralFlag().equals("1")) {
            activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.VISIBLE);
            if (io.getActivated().equals("1")) {
                GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
                drawable2.setColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.viewLiActivatedLine.setBackgroundColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.IvLiActivated.setImageResource(R.drawable.calendar_status_green);
                activitySpecialServiceDetailsBinding.tvLiActivatedDateTime.setText(io.getActivatedTime());
                activitySpecialServiceDetailsBinding.tvLiActivatedDateTime.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedName.setText(io.getActivatedBy());
                activitySpecialServiceDetailsBinding.tvLiActivatedName.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setText("ACTIVATED");
            } else {
                GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
                drawable2.setColor(Color.parseColor("#CECECE"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setTextColor(Color.parseColor("#CECECE"));
            }

        } else {
            activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.GONE);
        }

        if (io.getSendToHs().equals("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiHsSendLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiHsSend.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiHsSendDateTime.setText(io.getSendToHsDatetime());
            activitySpecialServiceDetailsBinding.tvLiHsSendDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendName.setText(io.getSendToHsBy());
            activitySpecialServiceDetailsBinding.tvLiHsSendName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setText("SENT TO HS");
        } else {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setTextColor(Color.parseColor("#CECECE"));
        }
        if (io.getHsMemoNo() == null) {

            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiHsReceivedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiHsReceived.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiHsReceivedDateTime.setText(io.getReceivedHsOrderDatetime());
            activitySpecialServiceDetailsBinding.tvLiHsReceivedDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedName.setText(io.getReceivedHsOrderBy());
            activitySpecialServiceDetailsBinding.tvLiHsReceivedName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setText("RECEIVED HS ORDER");

        }

        if (io.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiProviderSentLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiProvider.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiProviderSentDateTime.setText(io.getProviderReplyTime());
            activitySpecialServiceDetailsBinding.tvLiProviderSentDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentName.setText(io.getProviderReplyBy());
            activitySpecialServiceDetailsBinding.tvLiProviderSentName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setText("SEND TO PROVIDER");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setTextColor(Color.parseColor("#CECECE"));
        }
        if (io.getActivated().equals("1") && io.getDateOfActivation()!=null) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiRequestActiveLine.setBackgroundColor(Color.parseColor("#008000"));
            ;
            activitySpecialServiceDetailsBinding.IvLiRequestActive.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiRequestActiveDateTime.setText(io.getDateOfActivation());
            activitySpecialServiceDetailsBinding.tvLiRequestActiveDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveMessage.setText("REQUEST ACTIVATED");
            activitySpecialServiceDetailsBinding.viewLiInfoFurnishedApprovedEnd.setBackgroundColor(Color.parseColor("#008000"));

        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));

        }


    }

    public void StatusOthersIO() {
        Log.e("DAPL", "getOcApprovalFlag():" + io.getOcApprovalFlag() + "getApprovedByOC():" + io.getApprovedByOC());
        if (io.getOcApprovalFlag().equalsIgnoreCase("1") && io.getApprovedByOC().equalsIgnoreCase("1")) {

            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewOcApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            //activitySpecialServiceDetailsBinding.viewOcApprovedCircle.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewOcApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvOc.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvOcApprovalDateTime.setText(io.getApprovedByOCTime());
            activitySpecialServiceDetailsBinding.tvOcApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvOcApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvOcApprovalName.setText(io.getApprovedOCName());
            activitySpecialServiceDetailsBinding.tvOcApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvOcApprovalMessage.setText("APPROVED BY OC");

        } else if (io.getOcApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewOcApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewOcApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvOcApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        } else {
            activitySpecialServiceDetailsBinding.rlOcApprovalHolder.setVisibility(View.GONE);

        }
        if (io.getAcApprovalFlag().equalsIgnoreCase("1") && io.getApprovedByAC().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewAcApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewAcApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvAc.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvAcApprovalDateTime.setText(io.getApprovedACTime());
            activitySpecialServiceDetailsBinding.tvAcApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvAcApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvAcApprovalName.setText(io.getApprovedACName());
            activitySpecialServiceDetailsBinding.tvAcApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvAcApprovalMessage.setText("APPROVED BY AC");
        } else if (io.getAcApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewAcApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewAcApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvAcApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlAcApprovalHolder.setVisibility(View.GONE);

        }

        if (io.getDcApprovalFlag().equalsIgnoreCase("1") && io.getApprovedByDC().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDcApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewDcApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvDc.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvDcApprovalDateTime.setText(io.getApprovedByDCTime());
            activitySpecialServiceDetailsBinding.tvDcApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDcApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDcApprovalName.setText(io.getApprovedDCName());
            activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY DC");
        } else if (io.getDcApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDcApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewDcApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        } else {
            activitySpecialServiceDetailsBinding.rlDcApprovalHolder.setVisibility(View.GONE);

        }

        if (io.getJtcpApprovalFlag().equalsIgnoreCase("1") && io.getApprovedJtCp().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewJtcpApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewJtcpApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvJtcp.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvJtcpApprovalDateTime.setText(io.getApprovedJtCpTime());
            activitySpecialServiceDetailsBinding.tvJtcpApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalName.setText(io.getApprovedJtCpName());
            activitySpecialServiceDetailsBinding.tvJtcpApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalMessage.setText("APPROVED BY JTCP");
        } else if (io.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewJtcpApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewJtcpApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlJtcpApprovalHolder.setVisibility(View.GONE);

        }
        if (io.getNodalApprovalFlag().equalsIgnoreCase("1") && io.getApprovedNodal().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewNodalApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewNodalApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvNodal.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvNodalApprovalDateTime.setText(io.getApprovedNodalTime());
            activitySpecialServiceDetailsBinding.tvNodalApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalName.setText(io.getApprovedNodalName());
            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setText("APPROVED BY NODAL OFFICER");
        } else if (io.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewNodalApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewNodalApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));

            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlNodalApprovalHolder.setVisibility(View.GONE);

        }

        if (io.getDesignatedApprovalFlag().equalsIgnoreCase("1") && io.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDesignatedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewDesignatedApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvDesignated.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalDateTime.setText(io.getApprovedDesignatedOfficerTime());
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalName.setText(io.getApprovedDesignatedOfficerName());
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setText("APPROVED BY DESIGNATED OFFICER");
        } else if (io.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDesignatedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewDesignatedApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        } else {
            activitySpecialServiceDetailsBinding.rlDesignatedApprovalHolder.setVisibility(View.GONE);

        }

        String approvedSucessFlag = "";
        if (io.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            if (io.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (io.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            if (io.getApprovedNodal().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (io.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            if (io.getApprovedJtCp().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (io.getDcApprovalFlag().equalsIgnoreCase("1") || io.getDcApprovalFlag().equalsIgnoreCase("0")) {
            if (io.getApprovedByDC().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        }
        // approvedSucessFlag="1";
        //Toast.makeText(this, "ApprovedSucessFlag: " + approvedSucessFlag, Toast.LENGTH_SHORT).show();
        if (approvedSucessFlag.equalsIgnoreCase("1") && io.getMcellRead().equalsIgnoreCase("0")) {

            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        } else if (approvedSucessFlag.equalsIgnoreCase("1") && io.getMcellRead().equalsIgnoreCase("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewMcellApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvMcell.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvMcellApprovalDateTime.setText(io.getMcellReadTime());
            activitySpecialServiceDetailsBinding.tvMcellApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setText(io.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setText("MCELL READ");
        } else {
            //activitySpecialServiceDetailsBinding.rlMcellApprovalHolder.setVisibility(View.GONE);


        }

        if (io.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewProviderSeenApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvProviderSeen.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalDateTime.setText(io.getProviderReplyTime());
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalName.setText(io.getProviderReplyBy());
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setText("SEEN BY PROVIDER");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setTextColor(Color.parseColor("#CECECE"));

        }
        if (io.getMCellToPsAction().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedEnd.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvInfoFurnished.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalDateTime.setText(io.getMCellToPsActionTime());
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalName.setText(io.getMCellToPsActionBy());
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setText("INFORMATION FURNISHED");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setTextColor(Color.parseColor("#CECECE"));

        }


    }


    public void showStatusBarOC() {
        if (oc.getRequestType().equalsIgnoreCase("LI")) {
            activitySpecialServiceDetailsBinding.llLiHolder.setVisibility(View.VISIBLE);
            if(urgentGeneralFlag.equalsIgnoreCase("1")){
                activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.VISIBLE);
            }
            else{
                activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.GONE);
            }
            StatusLIOC();
        } else {
            activitySpecialServiceDetailsBinding.llAllRequestExceptLI.setVisibility(View.VISIBLE);
            StatusOthersOC();

        }

    }

    private void StatusLIOC() {
        activitySpecialServiceDetailsBinding.rlLiOcApprovalHolder.setVisibility(View.GONE);

        if (oc.getAcApprovalFlag().equalsIgnoreCase("1") && oc.getApprovedByAC().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiAcApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiAcApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiAc.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiAcApprovalDateTime.setText(oc.getApprovedACTime());
            activitySpecialServiceDetailsBinding.tvLiAcApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiAcApprovalName.setText(oc.getApprovedACName());
            activitySpecialServiceDetailsBinding.tvLiAcApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiAcApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiAcApprovalMessage.setText("APPROVED BY AC");
        } else if (oc.getAcApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiAcApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiAcApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiAcApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlLiAcApprovalHolder.setVisibility(View.GONE);

        }
        if (oc.getDcApprovalFlag().equalsIgnoreCase("1") && oc.getApprovedByDC().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDcApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiDcApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiDc.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiDcApprovalDateTime.setText(oc.getApprovedByDCTime());
            activitySpecialServiceDetailsBinding.tvLiDcApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDcApprovalName.setText(oc.getApprovedDCName());
            activitySpecialServiceDetailsBinding.tvLiDcApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDcApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDcApprovalMessage.setText("APPROVED BY DC");
        } else if (oc.getDcApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDcApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            // activitySpecialServiceDetailsBinding.viewDcApprovedCircle.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiDcApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiDcApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlLiDcApprovalHolder.setVisibility(View.GONE);

        }
        if (oc.getJtcpApprovalFlag().equalsIgnoreCase("1") && oc.getApprovedJtCp().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiJtcpApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiJtcpApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiJtcp.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalDateTime.setText(oc.getApprovedJtCpTime());
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalName.setText(oc.getApprovedJtCpName());
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalMessage.setText("APPROVED BY JTCP");
        } else if (oc.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiJtcpApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiJtcpApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlLiJtcpApprovalHolder.setVisibility(View.GONE);

        }
        if (oc.getMcellActionLi().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiMcellApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiMcell.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalDateTime.setText(oc.getMcellActionLiBy());
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalName.setText(oc.getMcellActionLiDatetime());
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setText("VERIFIED BY MCELL");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiMcellApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        }
        if (oc.getNodalApprovalFlag().equalsIgnoreCase("1") && oc.getApprovedNodal().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiNodalApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiNodal.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalDateTime.setText(oc.getApprovedNodalTime());
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalName.setText(oc.getApprovedNodalName());
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setText("APPROVED BY NODAL OFFICER");
        } else if (oc.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiNodalApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlLiNodalApprovalHolder.setVisibility(View.GONE);

        }

        if (oc.getDesignatedApprovalFlag().equalsIgnoreCase("1") && oc.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiDesignated.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalDateTime.setText(oc.getApprovedDesignatedOfficerTime());
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalName.setText(oc.getApprovedDesignatedOfficerName());
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setText("APPROVED BY DESIGNATED OFFICER");
        } else if (oc.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlLiDesignatedApprovalHolder.setVisibility(View.GONE);

        }
        String approvedSucessFlag = "";
        if (oc.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            if (oc.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (oc.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            if (oc.getApprovedNodal().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (oc.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            if (oc.getApprovedJtCp().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (oc.getDcApprovalFlag().equalsIgnoreCase("1") || oc.getDcApprovalFlag().equalsIgnoreCase("0")) {
            if (oc.getApprovedByDC().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        }
        //Toast.makeText(this, "ApprovedSucessFlag: " + approvedSucessFlag, Toast.LENGTH_SHORT).show();
        if (approvedSucessFlag.equalsIgnoreCase("1") && oc.getMcellRead().equalsIgnoreCase("0")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));

        } else if (approvedSucessFlag.equalsIgnoreCase("1") && oc.getMcellRead().equalsIgnoreCase("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiMcellSendLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiMcellSend.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiMcellSendDateTime.setText(oc.getMcellReadTime());
            activitySpecialServiceDetailsBinding.tvLiMcellSendDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendName.setText(oc.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvLiMcellSendName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendMessage.setText("Letter preparation by MCELL");
        }
        if (oc.getUrgentGeneralFlag().equals("1")) {
            activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.VISIBLE);
            if (oc.getActivated().equals("1")) {
                GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
                drawable2.setColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.viewLiActivatedLine.setBackgroundColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.IvLiActivated.setImageResource(R.drawable.calendar_status_green);
                activitySpecialServiceDetailsBinding.tvLiActivatedDateTime.setText(oc.getActivatedTime());
                activitySpecialServiceDetailsBinding.tvLiActivatedDateTime.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedName.setText(oc.getActivatedBy());
                activitySpecialServiceDetailsBinding.tvLiActivatedName.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setText("ACTIVATED");
            } else {
                GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
                drawable2.setColor(Color.parseColor("#CECECE"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setTextColor(Color.parseColor("#CECECE"));
            }

        } else {
            activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.GONE);
        }

        if (oc.getSendToHs().equals("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiHsSendLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiHsSend.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiHsSendDateTime.setText(oc.getSendToHsDatetime());
            activitySpecialServiceDetailsBinding.tvLiHsSendDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendName.setText(oc.getSendToHsBy());
            activitySpecialServiceDetailsBinding.tvLiHsSendName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setText("SENT TO HS");
        } else {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setTextColor(Color.parseColor("#CECECE"));
        }
        if (oc.getHsMemoNo() == null) {

            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiHsReceivedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiHsReceived.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiHsReceivedDateTime.setText(oc.getReceivedHsOrderDatetime());
            activitySpecialServiceDetailsBinding.tvLiHsReceivedDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedName.setText(oc.getReceivedHsOrderBy());
            activitySpecialServiceDetailsBinding.tvLiHsReceivedName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setText("RECEIVED HS ORDER");

        }

        if (oc.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiProviderSentLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiProvider.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiProviderSentDateTime.setText(oc.getProviderReplyTime());
            activitySpecialServiceDetailsBinding.tvLiProviderSentDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentName.setText(oc.getProviderReplyBy());
            activitySpecialServiceDetailsBinding.tvLiProviderSentName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setText("SEND TO PROVIDER");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setTextColor(Color.parseColor("#CECECE"));
        }
        if (oc.getActivated().equals("1") && oc.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiRequestActiveLine.setBackgroundColor(Color.parseColor("#008000"));
            ;
            activitySpecialServiceDetailsBinding.IvLiRequestActive.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiRequestActiveDateTime.setText(oc.getDateOfActivation());
            activitySpecialServiceDetailsBinding.tvLiRequestActiveDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveMessage.setText("REQUEST ACTIVATED");
            activitySpecialServiceDetailsBinding.viewLiInfoFurnishedApprovedEnd.setBackgroundColor(Color.parseColor("#008000"));
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));

        }


    }

    private void StatusOthersOC() {

        activitySpecialServiceDetailsBinding.rlOcApprovalHolder.setVisibility(View.GONE);

        if (oc.getAcApprovalFlag().equalsIgnoreCase("1") && oc.getApprovedByAC().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewAcApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewAcApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvAc.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvAcApprovalDateTime.setText(oc.getApprovedACTime());
            activitySpecialServiceDetailsBinding.tvAcApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvAcApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvAcApprovalName.setText(oc.getApprovedACName());
            activitySpecialServiceDetailsBinding.tvAcApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvAcApprovalMessage.setText("APPROVED BY AC");
        } else if (oc.getAcApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewAcApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewAcApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvAcApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlAcApprovalHolder.setVisibility(View.GONE);

        }

        if (oc.getDcApprovalFlag().equalsIgnoreCase("1") && oc.getApprovedByDC().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDcApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewDcApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvDc.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvDcApprovalDateTime.setText(oc.getApprovedByDCTime());
            activitySpecialServiceDetailsBinding.tvDcApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDcApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDcApprovalName.setText(oc.getApprovedDCName());
            activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY DC");
        } else if (oc.getDcApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDcApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewDcApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlDcApprovalHolder.setVisibility(View.GONE);

        }

        if (oc.getJtcpApprovalFlag().equalsIgnoreCase("1") && oc.getApprovedJtCp().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewJtcpApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewJtcpApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvJtcp.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvJtcpApprovalDateTime.setText(oc.getApprovedJtCpTime());
            activitySpecialServiceDetailsBinding.tvJtcpApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalName.setText(oc.getApprovedJtCpName());
            activitySpecialServiceDetailsBinding.tvJtcpApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalMessage.setText("APPROVED BY JTCP");
        } else if (oc.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewJtcpApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            // activitySpecialServiceDetailsBinding.viewDcApprovedCircle.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewJtcpApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlJtcpApprovalHolder.setVisibility(View.GONE);

        }
        if (oc.getNodalApprovalFlag().equalsIgnoreCase("1") && oc.getApprovedNodal().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewNodalApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewNodalApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvNodal.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvNodalApprovalDateTime.setText(oc.getApprovedNodalTime());
            activitySpecialServiceDetailsBinding.tvNodalApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalName.setText(oc.getApprovedNodalName());
            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setText("APPROVED BY NODAL OFFICER");
        } else if (oc.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewNodalApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            // activitySpecialServiceDetailsBinding.viewDcApprovedCircle.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewNodalApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlNodalApprovalHolder.setVisibility(View.GONE);

        }

        if (oc.getDesignatedApprovalFlag().equalsIgnoreCase("1") && oc.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDesignatedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewDesignatedApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvDesignated.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalDateTime.setText(oc.getApprovedDesignatedOfficerTime());
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalName.setText(oc.getApprovedDesignatedOfficerName());
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setText("APPROVED BY DESIGNATED OFFICER");
        } else if (oc.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDesignatedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewDesignatedApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlDesignatedApprovalHolder.setVisibility(View.GONE);

        }

        String approvedSucessFlag = "";
        if (oc.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            if (oc.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (oc.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            if (oc.getApprovedNodal().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (oc.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            if (oc.getApprovedJtCp().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (oc.getDcApprovalFlag().equalsIgnoreCase("1") || oc.getDcApprovalFlag().equalsIgnoreCase("0")) {
            if (oc.getApprovedByDC().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        }
        // approvedSucessFlag="1";
        //Toast.makeText(this, "ApprovedSucessFlag: " + approvedSucessFlag, Toast.LENGTH_SHORT).show();
        if (approvedSucessFlag.equalsIgnoreCase("1") && oc.getMcellRead().equalsIgnoreCase("0")) {

            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        } else if (approvedSucessFlag.equalsIgnoreCase("1") && oc.getMcellRead().equalsIgnoreCase("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewMcellApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvMcell.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvMcellApprovalDateTime.setText(oc.getMcellReadTime());
            activitySpecialServiceDetailsBinding.tvMcellApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setText(oc.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setText("MCELL READ");
        } else {
            //activitySpecialServiceDetailsBinding.rlMcellApprovalHolder.setVisibility(View.GONE);


        }

        if (oc.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewProviderSeenApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvProviderSeen.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalDateTime.setText(oc.getProviderReplyTime());
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalName.setText(oc.getProviderReplyBy());
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setText("SEEN BY PROVIDER");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setTextColor(Color.parseColor("#CECECE"));

        }
        if (oc.getMCellToPsAction().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedEnd.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvInfoFurnished.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalDateTime.setText(oc.getMCellToPsActionTime());
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalName.setText(oc.getMCellToPsActionBy());
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setText("INFORMATION FURNISHED");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setTextColor(Color.parseColor("#CECECE"));

        }

    }

    public void showStatusBarAC() {

        if (ac.getRequestType().equalsIgnoreCase("LI")) {
            activitySpecialServiceDetailsBinding.llLiHolder.setVisibility(View.VISIBLE);
            if(urgentGeneralFlag.equalsIgnoreCase("1")){
                activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.VISIBLE);
            }
            else{
                activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.GONE);
            }
            StatusLIAC();
        } else {
            activitySpecialServiceDetailsBinding.llAllRequestExceptLI.setVisibility(View.VISIBLE);
            StatusOthersAC();

        }

    }

    private void StatusLIAC() {
        activitySpecialServiceDetailsBinding.rlLiOcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlLiAcApprovalHolder.setVisibility(View.GONE);

        if (ac.getDcApprovalFlag().equalsIgnoreCase("1") && ac.getApprovedByDC().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDcApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiDcApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiDc.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiDcApprovalDateTime.setText(ac.getApprovedByDCTime());
            activitySpecialServiceDetailsBinding.tvLiDcApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDcApprovalName.setText(ac.getApprovedDCName());
            activitySpecialServiceDetailsBinding.tvLiDcApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDcApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDcApprovalMessage.setText("APPROVED BY DC");
        } else if (ac.getDcApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDcApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            // activitySpecialServiceDetailsBinding.viewDcApprovedCircle.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiDcApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiDcApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlLiDcApprovalHolder.setVisibility(View.GONE);

        }
        if (ac.getJtcpApprovalFlag().equalsIgnoreCase("1") && ac.getApprovedJtCp().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiJtcpApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiJtcpApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiJtcp.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalDateTime.setText(ac.getApprovedJtCpTime());
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalName.setText(ac.getApprovedJtCpName());
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalMessage.setText("APPROVED BY JTCP");
        } else if (ac.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiJtcpApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiJtcpApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlLiJtcpApprovalHolder.setVisibility(View.GONE);

        }
        if (ac.getMcellActionLi().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiMcellApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiMcell.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalDateTime.setText(ac.getMcellActionLiBy());
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalName.setText(ac.getMcellActionLiDatetime());
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setText("VERIFIED BY MCELL");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiMcellApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        }
        if (ac.getNodalApprovalFlag().equalsIgnoreCase("1") && ac.getApprovedNodal().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiNodalApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiNodal.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalDateTime.setText(ac.getApprovedNodalTime());
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalName.setText(ac.getApprovedNodalName());
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setText("APPROVED BY NODAL OFFICER");
        } else if (ac.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiNodalApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlLiNodalApprovalHolder.setVisibility(View.GONE);

        }

        if (ac.getDesignatedApprovalFlag().equalsIgnoreCase("1") && ac.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiDesignated.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalDateTime.setText(ac.getApprovedDesignatedOfficerTime());
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalName.setText(ac.getApprovedDesignatedOfficerName());
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setText("APPROVED BY DESIGNATED OFFICER");
        } else if (ac.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlLiDesignatedApprovalHolder.setVisibility(View.GONE);

        }
        String approvedSucessFlag = "";
        if (ac.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            if (ac.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (ac.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            if (ac.getApprovedNodal().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (ac.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            if (ac.getApprovedJtCp().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (ac.getDcApprovalFlag().equalsIgnoreCase("1") || ac.getDcApprovalFlag().equalsIgnoreCase("0")) {
            if (ac.getApprovedByDC().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        }
        //Toast.makeText(this, "ApprovedSucessFlag: " + approvedSucessFlag, Toast.LENGTH_SHORT).show();
        if (approvedSucessFlag.equalsIgnoreCase("1") && ac.getMcellRead().equalsIgnoreCase("0")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));

        } else if (approvedSucessFlag.equalsIgnoreCase("1") && ac.getMcellRead().equalsIgnoreCase("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiMcellSendLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiMcellSend.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiMcellSendDateTime.setText(ac.getMcellReadTime());
            activitySpecialServiceDetailsBinding.tvLiMcellSendDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendName.setText(ac.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvLiMcellSendName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendMessage.setText("Letter preparation by MCELL");
        }
        if (ac.getUrgentGeneralFlag().equals("1")) {
            activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.VISIBLE);
            if (ac.getActivated().equals("1")) {
                GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
                drawable2.setColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.viewLiActivatedLine.setBackgroundColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.IvLiActivated.setImageResource(R.drawable.calendar_status_green);
                activitySpecialServiceDetailsBinding.tvLiActivatedDateTime.setText(ac.getActivatedTime());
                activitySpecialServiceDetailsBinding.tvLiActivatedDateTime.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedName.setText(ac.getActivatedBy());
                activitySpecialServiceDetailsBinding.tvLiActivatedName.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setText("ACTIVATED");
            } else {
                GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
                drawable2.setColor(Color.parseColor("#CECECE"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setTextColor(Color.parseColor("#CECECE"));
            }

        } else {
            activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.GONE);
        }

        if (ac.getSendToHs().equals("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiHsSendLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiHsSend.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiHsSendDateTime.setText(ac.getSendToHsDatetime());
            activitySpecialServiceDetailsBinding.tvLiHsSendDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendName.setText(ac.getSendToHsBy());
            activitySpecialServiceDetailsBinding.tvLiHsSendName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setText("SENT TO HS");
        } else {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setTextColor(Color.parseColor("#CECECE"));
        }
        if (ac.getHsMemoNo() == null) {

            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiHsReceivedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiHsReceived.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiHsReceivedDateTime.setText(ac.getReceivedHsOrderDatetime());
            activitySpecialServiceDetailsBinding.tvLiHsReceivedDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedName.setText(ac.getReceivedHsOrderBy());
            activitySpecialServiceDetailsBinding.tvLiHsReceivedName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setText("RECEIVED HS ORDER");

        }

        if (ac.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiProviderSentLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiProvider.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiProviderSentDateTime.setText(ac.getProviderReplyTime());
            activitySpecialServiceDetailsBinding.tvLiProviderSentDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentName.setText(ac.getProviderReplyBy());
            activitySpecialServiceDetailsBinding.tvLiProviderSentName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setText("SEND TO PROVIDER");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setTextColor(Color.parseColor("#CECECE"));
        }
        if (ac.getActivated().equals("1") && ac.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiRequestActiveLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiRequestActive.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiRequestActiveDateTime.setText(ac.getDateOfActivation());
            activitySpecialServiceDetailsBinding.tvLiRequestActiveDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveMessage.setText("REQUEST ACTIVATED");
            activitySpecialServiceDetailsBinding.viewLiInfoFurnishedApprovedEnd.setBackgroundColor(Color.parseColor("#008000"));
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));

        }

    }

    public void StatusOthersAC() {

        activitySpecialServiceDetailsBinding.rlOcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlAcApprovalHolder.setVisibility(View.GONE);

        if (ac.getDcApprovalFlag().equalsIgnoreCase("1") && ac.getApprovedByDC().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDcApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewDcApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvDc.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvDcApprovalDateTime.setText(ac.getApprovedByDCTime());
            activitySpecialServiceDetailsBinding.tvDcApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDcApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDcApprovalName.setText(ac.getApprovedDCName());
            activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY DC");
        } else if (ac.getDcApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDcApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            // activitySpecialServiceDetailsBinding.viewDcApprovedCircle.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewDcApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlDcApprovalHolder.setVisibility(View.GONE);

        }

        if (ac.getJtcpApprovalFlag().equalsIgnoreCase("1") && ac.getApprovedJtCp().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewJtcpApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewJtcpApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvJtcp.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvJtcpApprovalDateTime.setText(ac.getApprovedJtCpTime());
            activitySpecialServiceDetailsBinding.tvJtcpApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalName.setText(ac.getApprovedJtCpName());
            activitySpecialServiceDetailsBinding.tvJtcpApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalMessage.setText("APPROVED BY JTCP");
        } else if (ac.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewJtcpApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewJtcpApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlJtcpApprovalHolder.setVisibility(View.GONE);

        }
        if (ac.getNodalApprovalFlag().equalsIgnoreCase("1") && ac.getApprovedNodal().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewNodalApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewNodalApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvNodal.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvNodalApprovalDateTime.setText(ac.getApprovedNodalTime());
            activitySpecialServiceDetailsBinding.tvNodalApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalName.setText(ac.getApprovedNodalName());
            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setText("APPROVED BY NODAL OFFICER");
        } else if (ac.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewNodalApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewNodalApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlNodalApprovalHolder.setVisibility(View.GONE);

        }

        if (ac.getDesignatedApprovalFlag().equalsIgnoreCase("1") && ac.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDesignatedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewDesignatedApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvDesignated.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalDateTime.setText(ac.getApprovedDesignatedOfficerTime());
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalName.setText(ac.getApprovedDesignatedOfficerName());
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setText("APPROVED BY DESIGNATED OFFICER");
        } else if (ac.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDesignatedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewDesignatedApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        } else {
            activitySpecialServiceDetailsBinding.rlDesignatedApprovalHolder.setVisibility(View.GONE);

        }

        String approvedSucessFlag = "";
        if (ac.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            if (ac.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (ac.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            if (ac.getApprovedNodal().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (ac.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            if (ac.getApprovedJtCp().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (ac.getDcApprovalFlag().equalsIgnoreCase("1") || ac.getDcApprovalFlag().equalsIgnoreCase("0")) {
            if (ac.getApprovedByDC().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        }
        // approvedSucessFlag="1";
        //Toast.makeText(this, "ApprovedSucessFlag: " + approvedSucessFlag, Toast.LENGTH_SHORT).show();
        if (approvedSucessFlag.equalsIgnoreCase("1") && ac.getMcellRead().equalsIgnoreCase("0")) {

            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        } else if (approvedSucessFlag.equalsIgnoreCase("1") && ac.getMcellRead().equalsIgnoreCase("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewMcellApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvMcell.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvMcellApprovalDateTime.setText(ac.getMcellReadTime());
            activitySpecialServiceDetailsBinding.tvMcellApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setText(ac.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setText("MCELL READ");
        } else {
            //activitySpecialServiceDetailsBinding.rlMcellApprovalHolder.setVisibility(View.GONE);


        }

        if (ac.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewProviderSeenApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvProviderSeen.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalDateTime.setText(ac.getProviderReplyTime());
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalName.setText(ac.getProviderReplyBy());
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setText("SEEN BY PROVIDER");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setTextColor(Color.parseColor("#CECECE"));

        }
        if (ac.getMCellToPsAction().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedEnd.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvInfoFurnished.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalDateTime.setText(ac.getMCellToPsActionTime());
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalName.setText(ac.getMCellToPsActionBy());
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setText("INFORMATION FURNISHED");

        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setTextColor(Color.parseColor("#CECECE"));

        }
    }

    public void showStatusBarDC() {
        if (dc.getRequestType().equalsIgnoreCase("LI")) {
            activitySpecialServiceDetailsBinding.llLiHolder.setVisibility(View.VISIBLE);
            if(urgentGeneralFlag.equalsIgnoreCase("1")){
                activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.VISIBLE);
            }
            else{
                activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.GONE);
            }
            StatusLIDC();
        } else {
            activitySpecialServiceDetailsBinding.llAllRequestExceptLI.setVisibility(View.VISIBLE);
            StatusOthersDC();

        }

    }

    private void StatusLIDC() {

        activitySpecialServiceDetailsBinding.rlLiOcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlLiAcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlLiDcApprovalHolder.setVisibility(View.GONE);

        if (dc.getJtcpApprovalFlag().equalsIgnoreCase("1") && dc.getApprovedJtCp().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiJtcpApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiJtcpApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiJtcp.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalDateTime.setText(dc.getApprovedJtCpTime());
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalName.setText(dc.getApprovedJtCpName());
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalMessage.setText("APPROVED BY JTCP");
        } else if (dc.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiJtcpApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiJtcpApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiJtcpApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlLiJtcpApprovalHolder.setVisibility(View.GONE);

        }
        if (dc.getMcellActionLi().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiMcellApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiMcell.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalDateTime.setText(dc.getMcellActionLiDatetime());
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalName.setText(dc.getMcellActionLiBy());
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setText("VERIFIED BY MCELL");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiMcellApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        }
        if (dc.getNodalApprovalFlag().equalsIgnoreCase("1") && dc.getApprovedNodal().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiNodalApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiNodal.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalDateTime.setText(dc.getApprovedNodalTime());
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalName.setText(dc.getApprovedNodalName());
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setText("APPROVED BY NODAL OFFICER");
        } else if (dc.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiNodalApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        } else {
            activitySpecialServiceDetailsBinding.rlLiNodalApprovalHolder.setVisibility(View.GONE);

        }

        if (dc.getDesignatedApprovalFlag().equalsIgnoreCase("1") && dc.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiDesignated.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalDateTime.setText(dc.getApprovedDesignatedOfficerTime());
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalName.setText(dc.getApprovedDesignatedOfficerName());
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setText("APPROVED BY DESIGNATED OFFICER");
        } else if (dc.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlLiDesignatedApprovalHolder.setVisibility(View.GONE);

        }
        String approvedSucessFlag = "";
        if (dc.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            if (dc.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (dc.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            if (dc.getApprovedNodal().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (dc.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            if (dc.getApprovedJtCp().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (dc.getDcApprovalFlag().equalsIgnoreCase("1") || dc.getDcApprovalFlag().equalsIgnoreCase("0")) {
            if (dc.getApprovedByDC().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        }
        if (approvedSucessFlag.equalsIgnoreCase("1") && dc.getMcellRead().equalsIgnoreCase("0")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));

        } else if (approvedSucessFlag.equalsIgnoreCase("1") && dc.getMcellRead().equalsIgnoreCase("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiMcellSendLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiMcellSend.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiMcellSendDateTime.setText(dc.getMcellReadTime());
            activitySpecialServiceDetailsBinding.tvLiMcellSendDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendName.setText(dc.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvLiMcellSendName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendMessage.setText("Letter preparation by MCELL");
        }
        if (dc.getUrgentGeneralFlag().equals("1")) {
            activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.VISIBLE);
            if (dc.getActivated().equals("1")) {
                GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
                drawable2.setColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.viewLiActivatedLine.setBackgroundColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.IvLiActivated.setImageResource(R.drawable.calendar_status_green);
                activitySpecialServiceDetailsBinding.tvLiActivatedDateTime.setText(dc.getActivated());
                activitySpecialServiceDetailsBinding.tvLiActivatedDateTime.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedName.setText(dc.getMcellReadBy());
                activitySpecialServiceDetailsBinding.tvLiActivatedName.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setText("ACTIVATED");
            } else {
                GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
                drawable2.setColor(Color.parseColor("#CECECE"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setTextColor(Color.parseColor("#CECECE"));
            }

        } else {
            activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.GONE);
        }

        if (dc.getSendToHs().equals("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiHsSendLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiHsSend.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiHsSendDateTime.setText(dc.getSendToHsDatetime());
            activitySpecialServiceDetailsBinding.tvLiHsSendDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendName.setText(dc.getSendToHsBy());
            activitySpecialServiceDetailsBinding.tvLiHsSendName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setText("SENT TO HS");

        } else {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setTextColor(Color.parseColor("#CECECE"));
        }
        if (dc.getHsMemoNo() == null) {

            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiHsReceivedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiHsReceived.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiHsReceivedDateTime.setText(dc.getActivated());
            activitySpecialServiceDetailsBinding.tvLiHsReceivedDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedName.setText(dc.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvLiHsReceivedName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setText("RECEIVED HS ORDER");

        }

        if (dc.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiProviderSentLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiProvider.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiProviderSentDateTime.setText(dc.getProviderReplyTime());
            activitySpecialServiceDetailsBinding.tvLiProviderSentDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentName.setText(dc.getProviderReplyBy());
            activitySpecialServiceDetailsBinding.tvLiProviderSentName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setText("SEND TO PROVIDER");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setTextColor(Color.parseColor("#CECECE"));
        }
        if (dc.getActivated().equals("1") && dc.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiRequestActiveLine.setBackgroundColor(Color.parseColor("#008000"));
            ;
            activitySpecialServiceDetailsBinding.IvLiRequestActive.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiRequestActiveDateTime.setText(dc.getDateOfActivation());
            activitySpecialServiceDetailsBinding.tvLiRequestActiveDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveName.setText("");
            activitySpecialServiceDetailsBinding.tvLiRequestActiveName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveMessage.setText("REQUEST ACTIVATED");
            activitySpecialServiceDetailsBinding.viewLiInfoFurnishedApprovedEnd.setBackgroundColor(Color.parseColor("#008000"));
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));

        }


    }

    private void StatusOthersDC() {

        activitySpecialServiceDetailsBinding.rlOcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlAcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlDcApprovalHolder.setVisibility(View.GONE);
        if (dc.getJtcpApprovalFlag().equalsIgnoreCase("1") && dc.getApprovedJtCp().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewJtcpApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewJtcpApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvJtcp.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvJtcpApprovalDateTime.setText(dc.getApprovedJtCpTime());
            activitySpecialServiceDetailsBinding.tvJtcpApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalName.setText(dc.getApprovedJtCpName());
            activitySpecialServiceDetailsBinding.tvJtcpApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalMessage.setText("APPROVED BY JTCP");
        } else if (dc.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewJtcpApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewJtcpApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvJtcpApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlJtcpApprovalHolder.setVisibility(View.GONE);

        }
        if (dc.getNodalApprovalFlag().equalsIgnoreCase("1") && dc.getApprovedNodal().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewNodalApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewNodalApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvNodal.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvNodalApprovalDateTime.setText(dc.getApprovedNodalTime());
            activitySpecialServiceDetailsBinding.tvNodalApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalName.setText(dc.getApprovedNodalName());
            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setText("APPROVED BY NODAL OFFICER");
        } else if (dc.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewNodalApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewNodalApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlNodalApprovalHolder.setVisibility(View.GONE);

        }

        if (dc.getDesignatedApprovalFlag().equalsIgnoreCase("1") && dc.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDesignatedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewDesignatedApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvDesignated.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalDateTime.setText(dc.getApprovedDesignatedOfficerTime());
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalName.setText(dc.getApprovedDesignatedOfficerName());
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setText("APPROVED BY DESIGNATED OFFICER");
        } else if (dc.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDesignatedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewDesignatedApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlDesignatedApprovalHolder.setVisibility(View.GONE);

        }

        String approvedSucessFlag = "";
        if (dc.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            if (dc.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (dc.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            if (dc.getApprovedNodal().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (dc.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            if (dc.getApprovedJtCp().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (dc.getDcApprovalFlag().equalsIgnoreCase("1") || dc.getDcApprovalFlag().equalsIgnoreCase("0")) {
            if (dc.getApprovedByDC().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        }
        // approvedSucessFlag="1";
        //Toast.makeText(this, "ApprovedSucessFlag: " + approvedSucessFlag, Toast.LENGTH_SHORT).show();
        if (approvedSucessFlag.equalsIgnoreCase("1") && dc.getMcellRead().equalsIgnoreCase("0")) {

            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        } else if (approvedSucessFlag.equalsIgnoreCase("1") && dc.getMcellRead().equalsIgnoreCase("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewMcellApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvMcell.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvMcellApprovalDateTime.setText(dc.getMcellReadTime());
            activitySpecialServiceDetailsBinding.tvMcellApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setText(dc.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setText("MCELL READ");
        } else {
            //activitySpecialServiceDetailsBinding.rlMcellApprovalHolder.setVisibility(View.GONE);


        }

        if (dc.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewProviderSeenApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvProviderSeen.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalDateTime.setText(dc.getProviderReplyTime());
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalName.setText(dc.getProviderReplyBy());
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setText("SEEN BY PROVIDER");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setTextColor(Color.parseColor("#CECECE"));

        }
        if (dc.getMCellToPsAction().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedEnd.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvInfoFurnished.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalDateTime.setText(dc.getMCellToPsActionTime());
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalName.setText(dc.getMCellToPsActionBy());
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setText("INFORMATION FURNISHED");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setTextColor(Color.parseColor("#CECECE"));

        }


    }

    public void showStatusBarJTCP() {
        if (jtcp.getRequestType().equalsIgnoreCase("LI")) {
            activitySpecialServiceDetailsBinding.llLiHolder.setVisibility(View.VISIBLE);
            if(urgentGeneralFlag.equalsIgnoreCase("1")){
                activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.VISIBLE);
            }
            else{
                activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.GONE);
            }
            StatusLIJTCP();
        } else {
            activitySpecialServiceDetailsBinding.llAllRequestExceptLI.setVisibility(View.VISIBLE);
            StatusOthersJTCP();

        }

    }

    private void StatusLIJTCP() {
        activitySpecialServiceDetailsBinding.rlLiOcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlLiAcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlLiDcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlLiJtcpApprovalHolder.setVisibility(View.GONE);


        if (jtcp.getMcellActionLi().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiMcellApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiMcell.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalDateTime.setText(jtcp.getApprovedByDCTime());
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalName.setText(jtcp.getApprovedDCName());
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setText("VERIFIED BY MCELL");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiMcellApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        }
        if (jtcp.getNodalApprovalFlag().equalsIgnoreCase("1") && jtcp.getApprovedNodal().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiNodalApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiNodal.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalDateTime.setText(jtcp.getApprovedNodalTime());
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalName.setText(jtcp.getApprovedNodalName());
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setText("APPROVED BY NODAL OFFICER");
        } else if (jtcp.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiNodalApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlLiNodalApprovalHolder.setVisibility(View.GONE);

        }

        if (jtcp.getDesignatedApprovalFlag().equalsIgnoreCase("1") && jtcp.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiDesignated.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalDateTime.setText(jtcp.getApprovedDesignatedOfficerTime());
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalName.setText(jtcp.getApprovedDesignatedOfficerName());
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setText("APPROVED BY DESIGNATED OFFICER");
        } else if (jtcp.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlLiDesignatedApprovalHolder.setVisibility(View.GONE);

        }
        String approvedSucessFlag = "";
        if (jtcp.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            if (jtcp.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (jtcp.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            if (jtcp.getApprovedNodal().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (jtcp.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            if (jtcp.getApprovedJtCp().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (jtcp.getDcApprovalFlag().equalsIgnoreCase("1") || jtcp.getDcApprovalFlag().equalsIgnoreCase("0")) {
            if (jtcp.getApprovedByDC().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        }
        //Toast.makeText(this, "ApprovedSucessFlag: " + approvedSucessFlag, Toast.LENGTH_SHORT).show();
        if (approvedSucessFlag.equalsIgnoreCase("1") && jtcp.getMcellRead().equalsIgnoreCase("0")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));

        } else if (approvedSucessFlag.equalsIgnoreCase("1") && jtcp.getMcellRead().equalsIgnoreCase("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiMcellSendLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiMcellSend.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiMcellSendDateTime.setText(jtcp.getMcellReadTime());
            activitySpecialServiceDetailsBinding.tvLiMcellSendDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendName.setText(jtcp.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvLiMcellSendName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendMessage.setText("Letter preparation by MCELL");
        }
        if (jtcp.getUrgentGeneralFlag().equals("1")) {
            activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.VISIBLE);
            if (jtcp.getActivated().equals("1")) {
                GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
                drawable2.setColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.viewLiActivatedLine.setBackgroundColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.IvLiActivated.setImageResource(R.drawable.calendar_status_green);
                activitySpecialServiceDetailsBinding.tvLiActivatedDateTime.setText(jtcp.getActivated());
                activitySpecialServiceDetailsBinding.tvLiActivatedDateTime.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedName.setText(jtcp.getActivatedBy());
                activitySpecialServiceDetailsBinding.tvLiActivatedName.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setText("ACTIVATED");
            } else {
                GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
                drawable2.setColor(Color.parseColor("#CECECE"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setTextColor(Color.parseColor("#CECECE"));
            }

        } else {
            activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.GONE);
        }

        if (jtcp.getSendToHs().equals("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiHsSendLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiHsSend.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiHsSendDateTime.setText(jtcp.getActivated());
            activitySpecialServiceDetailsBinding.tvLiHsSendDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendName.setText(jtcp.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvLiHsSendName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setText("SENT TO HS");
        } else {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setTextColor(Color.parseColor("#CECECE"));
        }
        if (jtcp.getHsMemoNo() == null) {

            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiHsReceivedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiHsReceived.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiHsReceivedDateTime.setText(jtcp.getReceivedHsOrderDatetime());
            activitySpecialServiceDetailsBinding.tvLiHsReceivedDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedName.setText(jtcp.getReceivedHsOrderBy());
            activitySpecialServiceDetailsBinding.tvLiHsReceivedName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setText("RECEIVED HS ORDER");


        }

        if (jtcp.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiProviderSentLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiProvider.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiProviderSentDateTime.setText(jtcp.getProviderReplyTime());
            activitySpecialServiceDetailsBinding.tvLiProviderSentDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentName.setText(jtcp.getProviderReplyBy());
            activitySpecialServiceDetailsBinding.tvLiProviderSentName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setText("SEND TO PROVIDER");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setTextColor(Color.parseColor("#CECECE"));
        }
        if (jtcp.getActivated().equals("1") && jtcp.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiRequestActiveLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiRequestActive.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiRequestActiveDateTime.setText(jtcp.getDateOfActivation());
            activitySpecialServiceDetailsBinding.tvLiRequestActiveDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveName.setText("");
            activitySpecialServiceDetailsBinding.tvLiRequestActiveName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveMessage.setText("REQUEST ACTIVATED");
            activitySpecialServiceDetailsBinding.viewLiInfoFurnishedApprovedEnd.setBackgroundColor(Color.parseColor("#008000"));
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));

        }

    }


    private void StatusOthersJTCP() {

        activitySpecialServiceDetailsBinding.rlOcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlDcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlJtcpApprovalHolder.setVisibility(View.GONE);


        if (jtcp.getNodalApprovalFlag().equalsIgnoreCase("1") && jtcp.getApprovedNodal().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewNodalApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewNodalApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvNodal.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvNodalApprovalDateTime.setText(jtcp.getApprovedNodalTime());
            activitySpecialServiceDetailsBinding.tvNodalApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalName.setText(jtcp.getApprovedNodalName());
            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setText("APPROVED BY NODAL OFFICER");
        } else if (jtcp.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewNodalApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewNodalApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvNodalApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlNodalApprovalHolder.setVisibility(View.GONE);

        }

        if (jtcp.getDesignatedApprovalFlag().equalsIgnoreCase("1") && jtcp.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDesignatedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewDesignatedApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvDesignated.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalDateTime.setText(jtcp.getApprovedDesignatedOfficerTime());
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalName.setText(jtcp.getApprovedDesignatedOfficerName());
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setText("APPROVED BY DESIGNATED OFFICER");
        } else if (jtcp.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewDesignatedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewDesignatedApprovedLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvDesignatedApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        } else {
            activitySpecialServiceDetailsBinding.rlDesignatedApprovalHolder.setVisibility(View.GONE);

        }

        String approvedSucessFlag = "";
        if (jtcp.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            if (jtcp.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (jtcp.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            if (jtcp.getApprovedNodal().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (jtcp.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            if (jtcp.getApprovedJtCp().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (jtcp.getDcApprovalFlag().equalsIgnoreCase("1") || jtcp.getDcApprovalFlag().equalsIgnoreCase("0")) {
            if (jtcp.getApprovedByDC().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        }
        // approvedSucessFlag="1";
        //Toast.makeText(this, "ApprovedSucessFlag: " + approvedSucessFlag, Toast.LENGTH_SHORT).show();
        if (approvedSucessFlag.equalsIgnoreCase("1") && jtcp.getMcellRead().equalsIgnoreCase("0")) {

            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        } else if (approvedSucessFlag.equalsIgnoreCase("1") && jtcp.getMcellRead().equalsIgnoreCase("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewMcellApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvMcell.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvMcellApprovalDateTime.setText(jtcp.getMcellReadTime());
            activitySpecialServiceDetailsBinding.tvMcellApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setText(jtcp.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setText("MCELL READ");
        } else {
            //activitySpecialServiceDetailsBinding.rlMcellApprovalHolder.setVisibility(View.GONE);


        }

        if (jtcp.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewProviderSeenApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvProviderSeen.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalDateTime.setText(jtcp.getProviderReplyTime());
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalName.setText(jtcp.getProviderReplyBy());
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setText("SEEN BY PROVIDER");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setTextColor(Color.parseColor("#CECECE"));

        }
        if (jtcp.getMCellToPsAction().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedEnd.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvInfoFurnished.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalDateTime.setText(jtcp.getMCellToPsActionTime());
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalName.setText(jtcp.getMCellToPsActionBy());
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setText("INFORMATION FURNISHED");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setTextColor(Color.parseColor("#CECECE"));

        }


    }

    public void showStatusBarADDLCP() {
        if (addlcp.getRequestType().equalsIgnoreCase("LI")) {
            activitySpecialServiceDetailsBinding.llLiHolder.setVisibility(View.VISIBLE);
            if(urgentGeneralFlag.equalsIgnoreCase("1")){
                activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.VISIBLE);
            }
            else{
                activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.GONE);
            }
            StatusLIADDLCP();
        } else {
            activitySpecialServiceDetailsBinding.llAllRequestExceptLI.setVisibility(View.VISIBLE);
            StatusOthersADDLCP();

        }

    }

    private void StatusLIADDLCP() {
        activitySpecialServiceDetailsBinding.rlLiOcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlLiAcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlLiDcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlLiJtcpApprovalHolder.setVisibility(View.GONE);


        if (addlcp.getMcellActionLi().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiMcellApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiMcell.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalDateTime.setText(addlcp.getMcellActionLiDatetime());
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalName.setText(addlcp.getMcellActionLiBy());
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setText("VERIFIED BY MCELL");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiMcellApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        }
        if (addlcp.getNodalApprovalFlag().equalsIgnoreCase("1") && addlcp.getApprovedNodal().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiNodalApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiNodal.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalDateTime.setText(addlcp.getApprovedNodalTime());
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalName.setText(addlcp.getApprovedNodalName());
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setText("APPROVED BY NODAL OFFICER");
        } else if (addlcp.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiNodalApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlLiNodalApprovalHolder.setVisibility(View.GONE);

        }

        if (addlcp.getDesignatedApprovalFlag().equalsIgnoreCase("1") && addlcp.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiDesignated.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalDateTime.setText(addlcp.getApprovedDesignatedOfficerTime());
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalName.setText(addlcp.getApprovedDesignatedOfficerName());
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setText("APPROVED BY DESIGNATED OFFICER");
        } else if (addlcp.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlLiDesignatedApprovalHolder.setVisibility(View.GONE);

        }
        String approvedSucessFlag = "";
        if (addlcp.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            if (addlcp.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (addlcp.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            if (addlcp.getApprovedNodal().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (addlcp.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            if (addlcp.getApprovedJtCp().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (addlcp.getDcApprovalFlag().equalsIgnoreCase("1") || addlcp.getDcApprovalFlag().equalsIgnoreCase("0")) {
            if (addlcp.getApprovedByDC().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        }
        //Toast.makeText(this, "ApprovedSucessFlag: " + approvedSucessFlag, Toast.LENGTH_SHORT).show();
        if (approvedSucessFlag.equalsIgnoreCase("1") && addlcp.getMcellRead().equalsIgnoreCase("0")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));

        } else if (approvedSucessFlag.equalsIgnoreCase("1") && addlcp.getMcellRead().equalsIgnoreCase("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiMcellSendLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiMcellSend.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiMcellSendDateTime.setText(addlcp.getMcellReadTime());
            activitySpecialServiceDetailsBinding.tvLiMcellSendDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendName.setText(addlcp.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvLiMcellSendName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendMessage.setText("Letter preparation by MCELL");
        }
        if (addlcp.getUrgentGeneralFlag().equals("1")) {
            activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.VISIBLE);
            if (addlcp.getActivated().equals("1")) {
                GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
                drawable2.setColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.viewLiActivatedLine.setBackgroundColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.IvLiActivated.setImageResource(R.drawable.calendar_status_green);
                activitySpecialServiceDetailsBinding.tvLiActivatedDateTime.setText(addlcp.getActivated());
                activitySpecialServiceDetailsBinding.tvLiActivatedDateTime.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedName.setText(addlcp.getMcellReadBy());
                activitySpecialServiceDetailsBinding.tvLiActivatedName.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setText("ACTIVATED");
            } else {
                GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
                drawable2.setColor(Color.parseColor("#CECECE"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setTextColor(Color.parseColor("#CECECE"));
            }

        } else {
            activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.GONE);
        }

        if (addlcp.getSendToHs().equals("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiHsSendLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiHsSend.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiHsSendDateTime.setText(addlcp.getSendToHsDatetime());
            activitySpecialServiceDetailsBinding.tvLiHsSendDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendName.setText(addlcp.getSendToHsBy());
            activitySpecialServiceDetailsBinding.tvLiHsSendName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setText("SENT TO HS");

        } else {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setTextColor(Color.parseColor("#CECECE"));
        }
        if (addlcp.getHsMemoNo() == null) {

            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiHsReceivedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiHsReceived.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiHsReceivedDateTime.setText(addlcp.getActivated());
            activitySpecialServiceDetailsBinding.tvLiHsReceivedDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedName.setText(addlcp.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvLiHsReceivedName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setText("RECEIVED HS ORDER");

        }

        if (addlcp.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiProviderSentLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiProvider.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiProviderSentDateTime.setText(addlcp.getProviderReplyTime());
            activitySpecialServiceDetailsBinding.tvLiProviderSentDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentName.setText(addlcp.getProviderReplyBy());
            activitySpecialServiceDetailsBinding.tvLiProviderSentName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setText("SEND TO PROVIDER");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setTextColor(Color.parseColor("#CECECE"));
        }
        if (addlcp.getActivated().equals("1") && addlcp.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiRequestActiveLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiRequestActive.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiRequestActiveDateTime.setText(addlcp.getDateOfActivation());
            activitySpecialServiceDetailsBinding.tvLiRequestActiveDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveName.setText("");
            activitySpecialServiceDetailsBinding.tvLiRequestActiveName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveMessage.setText("REQUEST ACTIVATED");
            activitySpecialServiceDetailsBinding.viewLiInfoFurnishedApprovedEnd.setBackgroundColor(Color.parseColor("#008000"));
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));

        }


    }

    private void StatusOthersADDLCP() {


        activitySpecialServiceDetailsBinding.rlOcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlDcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlJtcpApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlDesignatedApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlNodalApprovalHolder.setVisibility(View.GONE);


        if (addlcp.getMcellRead().equalsIgnoreCase("1")) {
         /*   GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        } else if (approvedSucessFlag.equalsIgnoreCase("1") && jtcp.getMcellRead().equalsIgnoreCase("1")) {*/
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewMcellApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvMcell.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvMcellApprovalDateTime.setText(addlcp.getMcellReadTime());
            activitySpecialServiceDetailsBinding.tvMcellApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setText(addlcp.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setText("Letter preparation by MCELL");
        } else {
            //activitySpecialServiceDetailsBinding.rlMcellApprovalHolder.setVisibility(View.GONE);
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setTextColor(Color.parseColor("#CECECE"));


        }

        if (addlcp.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewProviderSeenApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvProviderSeen.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalDateTime.setText(addlcp.getProviderReplyTime());
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalName.setText(addlcp.getProviderReplyBy());
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setText("SEEN BY PROVIDER");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setTextColor(Color.parseColor("#CECECE"));

        }
        if (addlcp.getMCellToPsAction().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedEnd.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvInfoFurnished.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalDateTime.setText(addlcp.getMCellToPsActionTime());
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalName.setText(addlcp.getMCellToPsActionBy());
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setText("INFORMATION FURNISHED");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setTextColor(Color.parseColor("#CECECE"));

        }
    }

    public void showStatusBarCP() {

        if (cp.getRequestType().equalsIgnoreCase("LI")) {
            activitySpecialServiceDetailsBinding.llLiHolder.setVisibility(View.VISIBLE);
            if(urgentGeneralFlag.equalsIgnoreCase("1")){
                activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.VISIBLE);
            }
            else{
                activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.GONE);
            }
            StatusLICP();
        } else {
            activitySpecialServiceDetailsBinding.llAllRequestExceptLI.setVisibility(View.VISIBLE);
            StatusOthersCP();

        }

    }

    private void StatusLICP() {
        activitySpecialServiceDetailsBinding.rlLiOcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlLiAcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlLiDcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlLiJtcpApprovalHolder.setVisibility(View.GONE);


        if (cp.getMcellActionLi().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiMcellApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiMcell.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalDateTime.setText(cp.getMcellActionLiDatetime());
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalName.setText(cp.getMcellActionLiBy());
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setText("VERIFIED BY MCELL");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiMcellApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiMcellApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        }
        if (cp.getNodalApprovalFlag().equalsIgnoreCase("1") && cp.getApprovedNodal().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiNodalApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiNodal.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalDateTime.setText(cp.getApprovedNodalTime());
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalName.setText(cp.getApprovedNodalName());
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setText("APPROVED BY NODAL OFFICER");
        } else if (cp.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiNodalApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiNodalApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiNodalApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
            //activitySpecialServiceDetailsBinding.tvDcApprovalMessage.setText("APPROVED BY AC");
        } else {
            activitySpecialServiceDetailsBinding.rlLiNodalApprovalHolder.setVisibility(View.GONE);

        }

        if (cp.getDesignatedApprovalFlag().equalsIgnoreCase("1") && cp.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiDesignated.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalDateTime.setText(cp.getApprovedDesignatedOfficerTime());
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalName.setText(cp.getApprovedDesignatedOfficerName());
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setText("APPROVED BY DESIGNATED OFFICER");
        } else if (cp.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.viewLiDesignatedApprovalLine.setBackgroundColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiDesignatedApprovalMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            activitySpecialServiceDetailsBinding.rlLiDesignatedApprovalHolder.setVisibility(View.GONE);

        }
        String approvedSucessFlag = "";
        if (cp.getDesignatedApprovalFlag().equalsIgnoreCase("1")) {
            if (cp.getApprovedDesignatedOficer().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (cp.getNodalApprovalFlag().equalsIgnoreCase("1")) {
            if (cp.getApprovedNodal().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (cp.getJtcpApprovalFlag().equalsIgnoreCase("1")) {
            if (cp.getApprovedJtCp().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        } else if (cp.getDcApprovalFlag().equalsIgnoreCase("1") || cp.getDcApprovalFlag().equalsIgnoreCase("0")) {
            if (cp.getApprovedByDC().equalsIgnoreCase("1")) {
                approvedSucessFlag = "1";
            } else {
                approvedSucessFlag = "0";
            }
        }
        //Toast.makeText(this, "ApprovedSucessFlag: " + approvedSucessFlag, Toast.LENGTH_SHORT).show();
        if (approvedSucessFlag.equalsIgnoreCase("1") && cp.getMcellRead().equalsIgnoreCase("0")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));

        } else if (approvedSucessFlag.equalsIgnoreCase("1") && cp.getMcellRead().equalsIgnoreCase("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiMcellSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiMcellSendLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiMcellSend.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiMcellSendDateTime.setText(cp.getMcellReadTime());
            activitySpecialServiceDetailsBinding.tvLiMcellSendDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendName.setText(cp.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvLiMcellSendName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiMcellSendMessage.setText("Letter preparation by MCELL");
        }
        if (cp.getUrgentGeneralFlag().equals("1")) {
            activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.VISIBLE);
            if (cp.getActivated().equals("1")) {
                GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
                drawable2.setColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.viewLiActivatedLine.setBackgroundColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.IvLiActivated.setImageResource(R.drawable.calendar_status_green);
                activitySpecialServiceDetailsBinding.tvLiActivatedDateTime.setText(cp.getActivated());
                activitySpecialServiceDetailsBinding.tvLiActivatedDateTime.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedName.setText(cp.getMcellReadBy());
                activitySpecialServiceDetailsBinding.tvLiActivatedName.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setTextColor(Color.parseColor("#008000"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setText("ACTIVATED");
            } else {
                GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiActivatedCircle.getBackground();
                drawable2.setColor(Color.parseColor("#CECECE"));
                activitySpecialServiceDetailsBinding.tvLiActivatedMessage.setTextColor(Color.parseColor("#CECECE"));
            }

        } else {
            activitySpecialServiceDetailsBinding.rlLiActivatedHolder.setVisibility(View.GONE);
        }

        if (cp.getSendToHs().equals("1")) {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiHsSendLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiHsSend.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiHsSendDateTime.setText(cp.getSendToHsDatetime());
            activitySpecialServiceDetailsBinding.tvLiHsSendDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendName.setText(cp.getSendToHsBy());
            activitySpecialServiceDetailsBinding.tvLiHsSendName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setText("SENT TO HS");

        } else {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsSendCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiHsSendMessage.setTextColor(Color.parseColor("#CECECE"));
        }
        if (cp.getHsMemoNo() == null) {

            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setTextColor(Color.parseColor("#CECECE"));
        } else {
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiHsReceivedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiHsReceivedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiHsReceived.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiHsReceivedDateTime.setText(cp.getActivated());
            activitySpecialServiceDetailsBinding.tvLiHsReceivedDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedName.setText(cp.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvLiHsReceivedName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiHsReceivedMessage.setText("RECEIVED HS ORDER");

        }

        if (cp.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiProviderSentLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvLiProvider.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiProviderSentDateTime.setText(cp.getProviderReplyTime());
            activitySpecialServiceDetailsBinding.tvLiProviderSentDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentName.setText(cp.getProviderReplyBy());
            activitySpecialServiceDetailsBinding.tvLiProviderSentName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setText("SEND TO PROVIDER");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiProviderSentCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvLiProviderSentMessage.setTextColor(Color.parseColor("#CECECE"));
        }
        if (cp.getActivated().equals("1") && cp.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewLiRequestActiveLine.setBackgroundColor(Color.parseColor("#008000"));
            ;
            activitySpecialServiceDetailsBinding.IvLiRequestActive.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvLiRequestActiveDateTime.setText(cp.getDateOfActivation());
            activitySpecialServiceDetailsBinding.tvLiRequestActiveDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveName.setText("");
            activitySpecialServiceDetailsBinding.tvLiRequestActiveName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvLiRequestActiveMessage.setText("REQUEST ACTIVATED");
            activitySpecialServiceDetailsBinding.viewLiInfoFurnishedApprovedEnd.setBackgroundColor(Color.parseColor("#008000"));
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewLiRequestActiveCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));

        }


    }

    private void StatusOthersCP() {


        activitySpecialServiceDetailsBinding.rlOcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlDcApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlJtcpApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlDesignatedApprovalHolder.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.rlNodalApprovalHolder.setVisibility(View.GONE);


        if (cp.getMcellRead().equalsIgnoreCase("1")) {
         /*   GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setTextColor(Color.parseColor("#CECECE"));

        } else if (approvedSucessFlag.equalsIgnoreCase("1") && jtcp.getMcellRead().equalsIgnoreCase("1")) {*/
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewMcellApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvMcell.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvMcellApprovalDateTime.setText(cp.getMcellReadTime());
            activitySpecialServiceDetailsBinding.tvMcellApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setText(cp.getMcellReadBy());
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalMessage.setText("MCELL READ");
        } else {
            //activitySpecialServiceDetailsBinding.rlMcellApprovalHolder.setVisibility(View.GONE);
            GradientDrawable drawable2 = (GradientDrawable) activitySpecialServiceDetailsBinding.viewMcellApprovedCircle.getBackground();
            drawable2.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvMcellApprovalName.setTextColor(Color.parseColor("#CECECE"));


        }

        if (cp.getProviderReply().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewProviderSeenApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvProviderSeen.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalDateTime.setText(cp.getProviderReplyTime());
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalName.setText(cp.getProviderReplyBy());
            activitySpecialServiceDetailsBinding.tvProviderSeenApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setText("SEEN BY PROVIDER");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewProviderSeenApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvProviderSeenMessage.setTextColor(Color.parseColor("#CECECE"));

        }
        if (cp.getMCellToPsAction().equalsIgnoreCase("1")) {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedLine.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedEnd.setBackgroundColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.IvInfoFurnished.setImageResource(R.drawable.calendar_status_green);
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalDateTime.setText(cp.getMCellToPsActionTime());
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalDateTime.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalName.setText(cp.getMCellToPsActionBy());
            activitySpecialServiceDetailsBinding.tvInfoFurnishedApprovalName.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setTextColor(Color.parseColor("#008000"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setText("INFORMATION FURNISHED");
        } else {
            GradientDrawable drawable = (GradientDrawable) activitySpecialServiceDetailsBinding.viewInfoFurnishedApprovedCircle.getBackground();
            drawable.setColor(Color.parseColor("#CECECE"));
            activitySpecialServiceDetailsBinding.tvInfoFurnishedMessage.setTextColor(Color.parseColor("#CECECE"));

        }


    }


    public void setDetails(String requestType, List<Detail> details,String nodalApprovalDone ) {

        nodalApprovalDoneString=nodalApprovalDone;
        if (details.size() > 0) {
            layoutManager = new LinearLayoutManager(this);
            activitySpecialServiceDetailsBinding.detailsRv.setLayoutManager(layoutManager);
            SpecialServiceDetailsAdapter specialServiceDetailsAdapter = new SpecialServiceDetailsAdapter(this, details, requestType, false, alldisconnectionFlag,nodalApprovalDone);
            specialServiceDetailsAdapter.setOnPartialDisconnectionListener(this);
            specialServiceDetailsAdapter.setOnSpecialServiceDesignatedActionClickListener(this);
            activitySpecialServiceDetailsBinding.detailsRv.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
            activitySpecialServiceDetailsBinding.detailsRv.setAdapter(specialServiceDetailsAdapter);
        } else {
            activitySpecialServiceDetailsBinding.RlDetailsInfo.setVisibility(View.GONE);

        }

    }

    public void setNotification(List<NotificationDetail> notificationdetails) {
        if (notificationdetails.size() > 0) {
            layoutManagerReminder = new LinearLayoutManager(this);
            activitySpecialServiceDetailsBinding.reminderRv.setLayoutManager(layoutManagerReminder);
            SpecialSeviceReminderAdapter specialSeviceReminderAdapter = new SpecialSeviceReminderAdapter(this, notificationdetails);
            // activitySpecialServiceDetailsBinding.reminderRv.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
            activitySpecialServiceDetailsBinding.reminderRv.setAdapter(specialSeviceReminderAdapter);
        } else {
            activitySpecialServiceDetailsBinding.RlCommentHeading.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.Rb_general:
                if (activitySpecialServiceDetailsBinding.RbGeneral.isChecked()) {
                  urgentGeneralFlag="0";
                }
                break;
            case R.id.Rb_urgent:
                if (activitySpecialServiceDetailsBinding.RbUrgent.isChecked()) {
                    urgentGeneralFlag="1";
                }
                break;

            case R.id.tv_grant_request:
                updateStatus();
                break;
            case R.id.tv_skip_request:
                final android.app.AlertDialog ad = new android.app.AlertDialog.Builder(this).create();
                // Setting Dialog Title
                ad.setTitle("Info");

                // Setting Dialog Message
                ad.setMessage("Are you sure that you want to skip this approval?");

                // Setting alert dialog icon
                ad.setIcon((true) ? R.drawable.success : R.drawable.fail);


                ad.setButton(DialogInterface.BUTTON_POSITIVE,
                        "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (ad != null && ad.isShowing()) {
                                    ad.dismiss();
                                }

                                skipStatus();

                            }
                        });
                ad.setButton(DialogInterface.BUTTON_NEGATIVE,
                        "CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (ad != null && ad.isShowing()) {
                                    ad.dismiss();
                                }


                            }
                        });

                // Showing Alert Message
                ad.show();
                ad.setCanceledOnTouchOutside(false);
                break;
            case R.id.tv_edit_request:
                if (requesterRole.equalsIgnoreCase("IO")) {
                    if(io.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                        Intent it = new Intent(this, SpecialServiceActivity.class);
                        it.putExtra("REQUEST_TYPE", "LI_PARTIAL_SAVE");
                        it.putExtra("REQUEST_MAASTER_ID", io.getId());
                        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(it);
                    }
                    else{
                        Utility.showAlertDialog(this,Constants.ERROR,"Sorry you can complete your request only",false);
                    }

                }
                else if (requesterRole.equalsIgnoreCase("OC")) {
                    if(oc.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                        Intent it = new Intent(this, SpecialServiceActivity.class);
                        it.putExtra("REQUEST_TYPE", "LI_PARTIAL_SAVE");
                        it.putExtra("REQUEST_MAASTER_ID", oc.getId());
                        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(it);
                    }
                    else{
                        Utility.showAlertDialog(this,Constants.ERROR,"Sorry you can complete your request only",false);
                    }

                }
                else if (requesterRole.equalsIgnoreCase("DC")) {
                    if(dc.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                        Intent it = new Intent(this, SpecialServiceActivity.class);
                        it.putExtra("REQUEST_TYPE", "LI_PARTIAL_SAVE");
                        it.putExtra("REQUEST_MAASTER_ID", dc.getId());
                        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(it);
                    }
                    else{
                        Utility.showAlertDialog(this,Constants.ERROR,"Sorry you can complete your request only",false);
                    }


                }
                else if (requesterRole.equalsIgnoreCase("JTCP")) {
                    if(jtcp.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                        Intent it = new Intent(this, SpecialServiceActivity.class);
                        it.putExtra("REQUEST_TYPE", "LI_PARTIAL_SAVE");
                        it.putExtra("REQUEST_MAASTER_ID", jtcp.getId());
                        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(it);
                    }
                    else{
                        Utility.showAlertDialog(this,Constants.ERROR,"Sorry you can complete your request only",false);
                    }

                }
                else if (requesterRole.equalsIgnoreCase("ADDLCP")) {
                    if(addlcp.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                        Intent it = new Intent(this, SpecialServiceActivity.class);
                        it.putExtra("REQUEST_TYPE", "LI_PARTIAL_SAVE");
                        it.putExtra("REQUEST_MAASTER_ID", addlcp.getId());
                        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(it);
                    }
                    else{
                        Utility.showAlertDialog(this,Constants.ERROR,"Sorry you can complete your request only",false);
                    }

                }
                else if (requesterRole.equalsIgnoreCase("CP")) {
                    if(cp.getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
                        Intent it = new Intent(this, SpecialServiceActivity.class);
                        it.putExtra("REQUEST_TYPE", "LI_PARTIAL_SAVE");
                        it.putExtra("REQUEST_MAASTER_ID", cp.getId());
                        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(it);
                    }
                    else{
                        Utility.showAlertDialog(this,Constants.ERROR,"Sorry you can complete your request only",false);
                    }

                }

                break;
            case R.id.tv_revoke_request:
                revokerequest();
                break;
            case R.id.edit_IV:
                setAlertDialogOption();
                break;
            case R.id.tv_caseraferencevalue:
                String psCode="",caseNo="",caseYear="";
                if (requesterRole.equalsIgnoreCase("IO")) {
                    if(io.getCaseRef().contains("_")) {
                        String someString =io.getCaseRef();
                        char someChar = '_';
                        int count = 0;
                        for (int i = 0; i < someString.length(); i++) {
                            if (someString.charAt(i) == someChar) {
                                count++;
                            }
                        }
                        if (count == 2) {

                            String[] parts =io.getCaseRef().split("_");
                            psCode=parts[0];
                            caseYear=parts[1];
                            caseNo=parts[2];

                        }

                    }


                }
                else if (requesterRole.equalsIgnoreCase("OC")) {
                    if(oc.getCaseRef().contains("_")) {
                        String someString =oc.getCaseRef();
                        char someChar = '_';
                        int count = 0;
                        for (int i = 0; i < someString.length(); i++) {
                            if (someString.charAt(i) == someChar) {
                                count++;
                            }
                        }
                        if (count == 2) {
                            String[] parts =oc.getCaseRef().split("_");
                            psCode=parts[0];
                            caseYear=parts[1];
                            caseNo=parts[2];

                        }

                    }


                }
                else if (requesterRole.equalsIgnoreCase("AC")) {
                    if(ac.getCaseRef().contains("_")) {
                        String someString =ac.getCaseRef();
                        char someChar = '_';
                        int count = 0;
                        for (int i = 0; i < someString.length(); i++) {
                            if (someString.charAt(i) == someChar) {
                                count++;
                            }
                        }
                        if (count == 2) {

                            String[] parts =ac.getCaseRef().split("_");
                            psCode=parts[0];
                            caseYear=parts[1];
                            caseNo=parts[2];

                        }

                    }



                }
                else if (requesterRole.equalsIgnoreCase("DC")) {
                    if(dc.getCaseRef().contains("_")) {
                        String someString =dc.getCaseRef();
                        char someChar = '_';
                        int count = 0;
                        for (int i = 0; i < someString.length(); i++) {
                            if (someString.charAt(i) == someChar) {
                                count++;
                            }
                        }
                        if (count == 2) {

                            String[] parts =dc.getCaseRef().split("_");
                            psCode=parts[0];
                            caseYear=parts[1];
                            caseNo=parts[2];




                        }

                    }


                }
                else if (requesterRole.equalsIgnoreCase("JTCP")) {
                    if(jtcp.getCaseRef().contains("_")) {
                        String someString =jtcp.getCaseRef();
                        char someChar = '_';
                        int count = 0;
                        for (int i = 0; i < someString.length(); i++) {
                            if (someString.charAt(i) == someChar) {
                                count++;
                            }
                        }
                        if (count == 2) {

                            String[] parts =jtcp.getCaseRef().split("_");
                            psCode=parts[0];
                            caseYear=parts[1];
                            caseNo=parts[2];


                        }

                    }

                }
                else if (requesterRole.equalsIgnoreCase("ADDLCP")) {
                    if(addlcp.getCaseRef().contains("_")) {
                        String someString =addlcp.getCaseRef();
                        char someChar = '_';
                        int count = 0;
                        for (int i = 0; i < someString.length(); i++) {
                            if (someString.charAt(i) == someChar) {
                                count++;
                            }
                        }
                        if (count == 2) {

                            String[] parts =addlcp.getCaseRef().split("_");
                            psCode=parts[0];
                            caseYear=parts[1];
                            caseNo=parts[2];

                        }

                    }

                }
                else if (requesterRole.equalsIgnoreCase("CP")) {
                    if(cp.getCaseRef().contains("_")) {
                        String someString =cp.getCaseRef();
                        char someChar = '_';
                        int count = 0;
                        for (int i = 0; i < someString.length(); i++) {
                            if (someString.charAt(i) == someChar) {
                                count++;
                            }
                        }
                        if (count == 2) {

                            String[] parts =cp.getCaseRef().split("_");
                            psCode=parts[0];
                            caseYear=parts[1];
                            caseNo=parts[2];



                        }

                    }


                }

                Intent it = new Intent(this, CaseSearchFIRDetailsActivity.class);
                it.putExtra("FROM_PAGE", "SpecialServiceDetailsActivity");
                it.putExtra("ITEM_NAME", headerTextFir);
                it.putExtra("HEADER_VALUE", headerTextFir);
                it.putExtra("PS_CODE", psCode);
                it.putExtra("CASE_NO", caseNo);
                it.putExtra("CASE_YR", caseYear);
                it.putExtra("POSITION", "0");
                startActivity(it);
                break;
            case R.id.tv_requestingOffvalue:
                String officerPhone = "";
                if (requesterRole.equalsIgnoreCase("IO")) {
                    officerPhone=io.getPhoneNumber();
                }
                else if (requesterRole.equalsIgnoreCase("OC")) {
                    officerPhone=oc.getPhoneNumber();
                }
                else if (requesterRole.equalsIgnoreCase("AC")) {
                    officerPhone=ac.getPhoneNumber();
                }
                else if (requesterRole.equalsIgnoreCase("DC")) {
                    officerPhone=dc.getPhoneNumber();
                }
                else if (requesterRole.equalsIgnoreCase("JTCP")) {
                    officerPhone=jtcp.getPhoneNumber();
                }
                else if (requesterRole.equalsIgnoreCase("ADDLCP")) {
                    officerPhone=addlcp.getPhoneNumber();
                }
                else if (requesterRole.equalsIgnoreCase("CP")) {
                    officerPhone=cp.getPhoneNumber();
                }
                Log.e("DAPL","OFFICER NUMBER:----"+officerPhone);
                Intent itphone=new Intent(Intent.ACTION_DIAL);
                itphone.setData(Uri.parse("tel:" + officerPhone));
                startActivity(itphone);
                break;
            case R.id.bttn_reconnection_submit:
                submitReconnectionRequest();
                break;
            case R.id.bttn_disconnection_submit:
                submitDisconnectionRequest();
                break;
            case R.id.Cb_all_disconnection:
                boolean flag = true;
                if (activitySpecialServiceDetailsBinding.CbAllDisconnection.isChecked()) {
                    allSelectFlag = true;
                    alldisconnectionFlag = true;
                    setSpecialServiceAdapter(flag, alldisconnectionFlag);
                    try {

                        // String getRecId = "";
                        if (requesterRole.equalsIgnoreCase("SEARCH")) {
                            for (int S = 0; S < result.getDetails().size(); S++) {
                                JSONObject jObj = new JSONObject();
                                jObj.put("id", io.getDetails().get(S).getRecordId());
                                jsonArrayData.put(jObj);
                            }
                        }

                        if (requesterRole.equalsIgnoreCase("IO")) {
                            for (int IO = 0; IO < io.getDetails().size(); IO++) {
                                JSONObject jObj = new JSONObject();
                                jObj.put("id", io.getDetails().get(IO).getRecordId());
                                jsonArrayData.put(jObj);
                            }
                        } else if (requesterRole.equalsIgnoreCase("OC")) {
                            for (int OC = 0; OC < oc.getDetails().size(); OC++) {
                                JSONObject jObj = new JSONObject();
                                jObj.put("id", oc.getDetails().get(OC).getRecordId());
                                jsonArrayData.put(jObj);
                            }
                        } else if (requesterRole.equalsIgnoreCase("DC")) {
                            for (int DC = 0; DC < dc.getDetails().size(); DC++) {
                                JSONObject jObj = new JSONObject();
                                jObj.put("id", dc.getDetails().get(DC).getRecordId());
                                jsonArrayData.put(jObj);
                            }
                        } else if (requesterRole.equalsIgnoreCase("JTCP")) {
                            for (int JTCP = 0; JTCP < jtcp.getDetails().size(); JTCP++) {
                                JSONObject jObj = new JSONObject();
                                jObj.put("id", jtcp.getDetails().get(JTCP).getRecordId());
                                jsonArrayData.put(jObj);
                            }
                        } else if (requesterRole.equalsIgnoreCase("ADDLCP")) {
                            for (int ADDLCP = 0; ADDLCP < addlcp.getDetails().size(); ADDLCP++) {
                                JSONObject jObj = new JSONObject();
                                String getRecId = addlcp.getDetails().get(ADDLCP).getRecordId();
                                jObj.put("id", getRecId);
                                jsonArrayData.put(jObj);
                            }
                        } else if (requesterRole.equalsIgnoreCase("CP")) {
                            for (int CP = 0; CP < cp.getDetails().size(); CP++) {
                                JSONObject jObj = new JSONObject();
                                String getRecId = cp.getDetails().get(CP).getRecordId();
                                jObj.put("id", getRecId);
                                jsonArrayData.put(jObj);
                            }
                        }


                    } catch (Exception e) {

                    }
                } else {
                    allSelectFlag = false;
                    alldisconnectionFlag = false;
                    setSpecialServiceAdapter(flag, alldisconnectionFlag);
                    jsonArrayData = new JSONArray();
                }
                break;
            case R.id.bttn_reminder_submit:
                setReminderToMcell();
                break;
            case R.id.bttn_nodal_comment_submit:
                String nodal_suggestion = activitySpecialServiceDetailsBinding.etNodalComment.getText().toString();
                if (nodal_suggestion.isEmpty()) {
                    Utility.showAlertDialog(this, Constants.ERROR, "Please enter your suggestion", false);
                } else {
                    setNodalSuggestion(nodal_suggestion);
                }
                break;


        }

    }

    public void setRequestProcessType(String flag_type) {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_SPECIAL_SERVICE_REQUEST_PROCESS_TYPE);
        taskManager.setSpecialServiceRequestProcessType(true);
        String[] keys = {"name", "user_rank", "master_id", "flag_type"};
        String[] values = {Utility.getUserInfo(this).getName(), Utility.getUserInfo(this).getUserRank(), requestID, flag_type};
        taskManager.doStartTask(keys, values, true, true);

    }

    public void parseSpecialServiceRequestProcessType(String response) {

        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optString("status").equals("1")) {
                    Utility.showAlertDialog(this, Constants.SUCCESS, jsonObject.optString("message"), true);
                } else {
                    Utility.showAlertDialog(this, Constants.ERROR, jsonObject.optString("message"), true);
                }
            } catch (Exception e) {

            }
        }

    }

    public void setAlertDialogOption() {
        final CharSequence[] choice = {"Disconnection", "Reconnection"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.myDialog);
        builder.setTitle("Select Type");
        builder.setSingleChoiceItems(choice, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (choice[which] == "Disconnection") {
                    type = 1;
                } else if (choice[which] == "Reconnection") {
                    type = 2;
                }

            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (type == 0) {
                    Toast.makeText(SpecialServiceDetailsActivity.this, "Select One Choice", Toast.LENGTH_SHORT).show();
                } else if (type == 1) {
                    partial_disconnection();
                } else if (type == 2) {
                    //complete_disconnection();
                    call_for_outcome_monitoringcell();
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public void partial_disconnection() {
        boolean flag = true;
        activitySpecialServiceDetailsBinding.llLastOutcomeMonitoring.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.bttnDisconnectionSubmit.setVisibility(View.VISIBLE);
        activitySpecialServiceDetailsBinding.CbAllDisconnection.setVisibility(View.VISIBLE);
        activitySpecialServiceDetailsBinding.CbAllDisconnection.setOnClickListener(this);
        activitySpecialServiceDetailsBinding.bttnDisconnectionSubmit.setText("DISCONNECTION");
        setSpecialServiceAdapter(flag, alldisconnectionFlag);

    }

    public void setSpecialServiceAdapter(boolean flag, boolean alldisconnectionFlag) {

        if (requesterRole.equalsIgnoreCase("IO")) {
            SpecialServiceDetailsAdapter specialServiceDetailsAdapter = new SpecialServiceDetailsAdapter(this, io.getDetails(), io.getRequestType(), flag, alldisconnectionFlag,nodalApprovalDoneString);
            specialServiceDetailsAdapter.setOnPartialDisconnectionListener(this);
            activitySpecialServiceDetailsBinding.detailsRv.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
            activitySpecialServiceDetailsBinding.detailsRv.setAdapter(specialServiceDetailsAdapter);
            specialServiceDetailsAdapter.notifyDataSetChanged();
        }
        if (requesterRole.equalsIgnoreCase("OC")) {
            SpecialServiceDetailsAdapter specialServiceDetailsAdapter = new SpecialServiceDetailsAdapter(this, oc.getDetails(), oc.getRequestType(), flag, alldisconnectionFlag,nodalApprovalDoneString);
            specialServiceDetailsAdapter.setOnPartialDisconnectionListener(this);
            activitySpecialServiceDetailsBinding.detailsRv.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
            activitySpecialServiceDetailsBinding.detailsRv.setAdapter(specialServiceDetailsAdapter);
            specialServiceDetailsAdapter.notifyDataSetChanged();
        }
        if (requesterRole.equalsIgnoreCase("DC")) {
            SpecialServiceDetailsAdapter specialServiceDetailsAdapter = new SpecialServiceDetailsAdapter(this, dc.getDetails(), dc.getRequestType(), flag, alldisconnectionFlag,nodalApprovalDoneString);
            specialServiceDetailsAdapter.setOnPartialDisconnectionListener(this);
            activitySpecialServiceDetailsBinding.detailsRv.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
            activitySpecialServiceDetailsBinding.detailsRv.setAdapter(specialServiceDetailsAdapter);
            specialServiceDetailsAdapter.notifyDataSetChanged();
        }
        if (requesterRole.equalsIgnoreCase("JTCP")) {
            SpecialServiceDetailsAdapter specialServiceDetailsAdapter = new SpecialServiceDetailsAdapter(this, jtcp.getDetails(), jtcp.getRequestType(), flag, alldisconnectionFlag,nodalApprovalDoneString);
            specialServiceDetailsAdapter.setOnPartialDisconnectionListener(this);
            activitySpecialServiceDetailsBinding.detailsRv.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
            activitySpecialServiceDetailsBinding.detailsRv.setAdapter(specialServiceDetailsAdapter);
            specialServiceDetailsAdapter.notifyDataSetChanged();
        }

        if (requesterRole.equalsIgnoreCase("ADDLCP")) {
            SpecialServiceDetailsAdapter specialServiceDetailsAdapter = new SpecialServiceDetailsAdapter(this, addlcp.getDetails(), addlcp.getRequestType(), flag, alldisconnectionFlag,nodalApprovalDoneString);
            specialServiceDetailsAdapter.setOnPartialDisconnectionListener(this);
            activitySpecialServiceDetailsBinding.detailsRv.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
            activitySpecialServiceDetailsBinding.detailsRv.setAdapter(specialServiceDetailsAdapter);
            specialServiceDetailsAdapter.notifyDataSetChanged();
        }
        if (requesterRole.equalsIgnoreCase("CP")) {
            SpecialServiceDetailsAdapter specialServiceDetailsAdapter = new SpecialServiceDetailsAdapter(this, cp.getDetails(), cp.getRequestType(), flag, alldisconnectionFlag,nodalApprovalDoneString);
            specialServiceDetailsAdapter.setOnPartialDisconnectionListener(this);
            activitySpecialServiceDetailsBinding.detailsRv.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
            activitySpecialServiceDetailsBinding.detailsRv.setAdapter(specialServiceDetailsAdapter);
            specialServiceDetailsAdapter.notifyDataSetChanged();
        }
    }

    public void complete_disconnection() {
        activitySpecialServiceDetailsBinding.llLastOutcomeMonitoring.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.bttnDisconnectionSubmit.setVisibility(View.VISIBLE);
        activitySpecialServiceDetailsBinding.bttnDisconnectionSubmit.setText("COMPLETE DISCONNECTION");
        boolean flag = false;
        setSpecialServiceAdapter(flag, alldisconnectionFlag);
    }

    public void setReminderToMcell() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_REMINDER_TO_MCELL);
        taskManager.setReminderToMcell(true);
        if (requesterRole.equalsIgnoreCase("SEARCH")) {
            String[] keys = {"request_id", "user_name"};
            String[] values = {result.getId(), Utility.getUserInfo(this).getName()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("IO")) {
            String[] keys = {"request_id", "user_name"};
            String[] values = {io.getId(), Utility.getUserInfo(this).getName()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("OC")) {
            String[] keys = {"request_id", "user_name"};
            String[] values = {oc.getId(), Utility.getUserInfo(this).getName()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("DC")) {
            String[] keys = {"request_id", "user_name"};
            String[] values = {dc.getId(), Utility.getUserInfo(this).getName()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("JTCP")) {
            String[] keys = {"request_id", "user_name"};
            String[] values = {jtcp.getId(), Utility.getUserInfo(this).getName()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("ADDLCP")) {
            String[] keys = {"request_id", "user_name"};
            String[] values = {addlcp.getId(), Utility.getUserInfo(this).getName()};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("CP")) {
            String[] keys = {"request_id", "user_name"};
            String[] values = {cp.getId(), Utility.getUserInfo(this).getName()};
            taskManager.doStartTask(keys, values, true, true);
        }
    }

    public void parseReminderToMcellResult(String response) {
        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optString("status").equals("1")) {
                    Utility.showAlertDialog(this, Constants.SUCCESS, jsonObject.optString("message"), true);
                } else {
                    Utility.showAlertDialog(this, Constants.ERROR, jsonObject.optString("message"), true);
                }
            } catch (Exception e) {

            }
        }
    }

    public void setNodalSuggestion(String suggestion) {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_LI_REQUEST_NODAL_SUGGESTION);
        taskManager.setLINodalSuggestion(true);
        if (requesterRole.equalsIgnoreCase("IO")) {
            String[] keys = {"request_id", "suggested_name", "suggestion_details"};
            String[] values = {io.getId(), Utility.getUserInfo(this).getName(), suggestion};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("OC")) {
            String[] keys = {"request_id", "suggested_name", "suggestion_details"};
            String[] values = {oc.getId(), Utility.getUserInfo(this).getName(), suggestion};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("DC")) {
            String[] keys = {"request_id", "suggested_name", "suggestion_details"};
            String[] values = {dc.getId(), Utility.getUserInfo(this).getName(), suggestion};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("JTCP")) {
            String[] keys = {"request_id", "suggested_name", "suggestion_details"};
            String[] values = {jtcp.getId(), Utility.getUserInfo(this).getName(), suggestion};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("ADDLCP")) {
            String[] keys = {"request_id", "suggested_name", "suggestion_details"};
            String[] values = {addlcp.getId(), Utility.getUserInfo(this).getName(), suggestion};
            taskManager.doStartTask(keys, values, true, true);
        }
        if (requesterRole.equalsIgnoreCase("CP")) {
            String[] keys = {"request_id", "suggested_name", "suggestion_details"};
            String[] values = {cp.getId(), Utility.getUserInfo(this).getName(), suggestion};
            taskManager.doStartTask(keys, values, true, true);
        }
    }

    public void parseNodalSuggestionSavedResult(String response) {
        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optString("status").equals("1")) {
                    activitySpecialServiceDetailsBinding.etNodalComment.setText(" ");
                    JSONArray jsonArray = jsonObject.optJSONArray("result");

                    if (jsonArray.length() > 0) {
                        for (int j = 0; j < jsonArray.length(); j++) {
                            JSONObject jo = jsonArray.optJSONObject(j);
                            SuggestionModel suggestionModel = new SuggestionModel();
                            suggestionModel.setSuggestionId(jo.optString("suggestion_id"));
                            suggestionModel.setRequestId(jo.optString("request_id"));
                            suggestionModel.setSuggestionDetails(jo.optString("suggestion_details"));
                            suggestionModel.setSuggestionBy(jo.optString("suggestion_by"));
                            suggestionModel.setSuggestedTime(jo.optString("suggested_time"));
                            arraylistSugg.add(suggestionModel);

                        }
                        populateSavedComment(arraylistSugg);

                    }
                } else {
                    Utility.showAlertDialog(this, Constants.ERROR, jsonObject.optString("message"), true);
                }
            } catch (Exception e) {

            }
        }

    }

    private void populateSavedComment(ArrayList<SuggestionModel> arrayListNodalSuggestion) {
        specialServiceNodalSuggestionAdapter = new SpecialServiceNodalSuggestionAdapter(this, arrayListNodalSuggestion);
        activitySpecialServiceDetailsBinding.nodalCommentRv.setAdapter(specialServiceNodalSuggestionAdapter);
        specialServiceNodalSuggestionAdapter.notifyDataSetChanged();
    }

    public void call_for_outcome_monitoringcell() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_LI_RECONNECTION_JUSTIFICATION_PHONE);
        taskManager.setLIReconnectionJustification(true);
        String[] keys = {};
        String[] values = {};
        taskManager.doStartTask(keys, values, true, true);
    }

    public void parseLIReconnectionJustificationResult(String response) {
        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optString("status").equals("1")) {

                    JSONArray justification_arr = jsonObject.optJSONArray("justification");

                    if (justification_arr.length() > 0) {
                        int justTitleCount = justification_arr.length() + 1;
                        int justTemplateCount = justification_arr.length() + 1;

                        justificationTitle = new String[justTitleCount];
                        justificationTemplate = new String[justTemplateCount];
                        justificationTitle[0] = "Select Justification Type";
                        justificationTemplate[0] = "";
                        int j = 1;
                        for (int i = 0; i < justification_arr.length(); i++) {

                            JSONObject justObject = (JSONObject) justification_arr.get(i);
                            justificationTitle[j] = justObject.optString("title");
                            justificationTemplate[j] = justObject.optString("subject");
                            j++;
                        }
                    }


                    createJustLayout();

                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    public void submitReconnectionRequest() {
        if (
                activitySpecialServiceDetailsBinding.spTvJustificationType.getSelectedItem().toString().equalsIgnoreCase("Select Justiication Type")) {
            Utility.showAlertDialog(this, " Error ", "Please select Justification type ", false);
            return;

        } else if (activitySpecialServiceDetailsBinding.etJustification.getText().toString().isEmpty()) {

            Utility.showAlertDialog(this, " Error ", "Please enter Justification ", false);
            return;

        } else if (activitySpecialServiceDetailsBinding.etOutcomeMonitoringCell.getText().toString().isEmpty()) {

            Utility.showAlertDialog(this, " Error ", "Please enter out come of monitoring ", false);
            return;

        } else {
            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_LI_RECONNECTION);
            taskManager.setLIReconnectionCreate(true);
            try {
                if (allSelectFlag) {

                    jsonArrayData = new JSONArray();
                    try {

                        // String getRecId = "";
                        if (requesterRole.equalsIgnoreCase("SEARCH")) {
                            for (int S = 0; S < result.getDetails().size(); S++) {
                                JSONObject jObj = new JSONObject();
                                jObj.put("id", result.getDetails().get(S).getRecordId());
                                jsonArrayData.put(jObj);
                            }
                        }
                        if (requesterRole.equalsIgnoreCase("IO")) {
                            for (int IO = 0; IO < io.getDetails().size(); IO++) {
                                JSONObject jObj = new JSONObject();
                                jObj.put("id", io.getDetails().get(IO).getRecordId());
                                jsonArrayData.put(jObj);
                            }
                        } else if (requesterRole.equalsIgnoreCase("OC")) {
                            for (int OC = 0; OC < oc.getDetails().size(); OC++) {
                                JSONObject jObj = new JSONObject();
                                jObj.put("id", oc.getDetails().get(OC).getRecordId());
                                jsonArrayData.put(jObj);
                            }
                        } else if (requesterRole.equalsIgnoreCase("DC")) {
                            for (int DC = 0; DC < oc.getDetails().size(); DC++) {
                                JSONObject jObj = new JSONObject();
                                jObj.put("id", dc.getDetails().get(DC).getRecordId());
                                jsonArrayData.put(jObj);
                            }
                        } else if (requesterRole.equalsIgnoreCase("JTCP")) {
                            for (int JTCP = 0; JTCP < jtcp.getDetails().size(); JTCP++) {
                                JSONObject jObj = new JSONObject();
                                jObj.put("id", jtcp.getDetails().get(JTCP).getRecordId());
                                jsonArrayData.put(jObj);
                            }
                        } else if (requesterRole.equalsIgnoreCase("ADDLCP")) {
                            for (int ADDLCP = 0; ADDLCP < addlcp.getDetails().size(); ADDLCP++) {
                                JSONObject jObj = new JSONObject();
                                String getRecId = addlcp.getDetails().get(ADDLCP).getRecordId();
                                jObj.put("id", getRecId);
                                jsonArrayData.put(jObj);
                            }
                        } else if (requesterRole.equalsIgnoreCase("CP")) {
                            for (int CP = 0; CP < cp.getDetails().size(); CP++) {
                                JSONObject jObj = new JSONObject();
                                String getRecId = cp.getDetails().get(CP).getRecordId();
                                jObj.put("id", getRecId);
                                jsonArrayData.put(jObj);
                            }
                        }


                    } catch (Exception e) {

                    }


                } else {
                    jsonArrayData = new JSONArray();
                    for (int i = 0; i < arrayListPartialDisconnection.size(); i++) {
                        JSONObject jObj = new JSONObject();
                        jObj.put("id", arrayListPartialDisconnection.get(i));
                        jsonArrayData.put(jObj);
                    }

                    arrayListPartialDisconnection.clear();
                }


            } catch (Exception e) {

            }
            if(jsonArrayData.length()>0) {
                if (requesterRole.equalsIgnoreCase("SEARCH")) {
                    String[] keys = {"request_id", "just_title", "just_subject", "outcome_mcell", "data_array"};
                    String[] values = {result.getId(), activitySpecialServiceDetailsBinding.spTvJustificationType.getSelectedItem().toString(),
                            activitySpecialServiceDetailsBinding.etJustification.getText().toString(), activitySpecialServiceDetailsBinding.etOutcomeMonitoringCell.getText().toString(), jsonArrayData.toString()};
                    taskManager.doStartTask(keys, values, true, true);

                }
                if (requesterRole.equalsIgnoreCase("IO")) {
                    String[] keys = {"request_id", "just_title", "just_subject", "outcome_mcell", "data_array"};
                    String[] values = {io.getId(), activitySpecialServiceDetailsBinding.spTvJustificationType.getSelectedItem().toString(),
                            activitySpecialServiceDetailsBinding.etJustification.getText().toString(), activitySpecialServiceDetailsBinding.etOutcomeMonitoringCell.getText().toString(), jsonArrayData.toString()};
                    taskManager.doStartTask(keys, values, true, true);

                }
                if (requesterRole.equalsIgnoreCase("OC")) {
                    String[] keys = {"request_id", "just_title", "just_subject", "outcome_mcell", "data_array"};
                    String[] values = {oc.getId(), activitySpecialServiceDetailsBinding.spTvJustificationType.getSelectedItem().toString(),
                            activitySpecialServiceDetailsBinding.etJustification.getText().toString(), activitySpecialServiceDetailsBinding.etOutcomeMonitoringCell.getText().toString(), jsonArrayData.toString()};
                    taskManager.doStartTask(keys, values, true, true);

                }
                if (requesterRole.equalsIgnoreCase("DC")) {
                    String[] keys = {"request_id", "just_title", "just_subject", "outcome_mcell", "data_array"};
                    String[] values = {dc.getId(), activitySpecialServiceDetailsBinding.spTvJustificationType.getSelectedItem().toString(),
                            activitySpecialServiceDetailsBinding.etJustification.getText().toString(), activitySpecialServiceDetailsBinding.etOutcomeMonitoringCell.getText().toString(), jsonArrayData.toString()};
                    taskManager.doStartTask(keys, values, true, true);

                }
                if (requesterRole.equalsIgnoreCase("JTCP")) {
                    String[] keys = {"request_id", "just_title", "just_subject", "outcome_mcell", "data_array"};
                    String[] values = {jtcp.getId(), activitySpecialServiceDetailsBinding.spTvJustificationType.getSelectedItem().toString(),
                            activitySpecialServiceDetailsBinding.etJustification.getText().toString(), activitySpecialServiceDetailsBinding.etOutcomeMonitoringCell.getText().toString(), jsonArrayData.toString()};
                    taskManager.doStartTask(keys, values, true, true);

                }
                if (requesterRole.equalsIgnoreCase("ADDLCP")) {
                    String[] keys = {"request_id", "just_title", "just_subject", "outcome_mcell", "data_array"};
                    String[] values = {addlcp.getId(), activitySpecialServiceDetailsBinding.spTvJustificationType.getSelectedItem().toString(),
                            activitySpecialServiceDetailsBinding.etJustification.getText().toString(), activitySpecialServiceDetailsBinding.etOutcomeMonitoringCell.getText().toString(), jsonArrayData.toString()};
                    taskManager.doStartTask(keys, values, true, true);

                }
                if (requesterRole.equalsIgnoreCase("CP")) {
                    String[] keys = {"request_id", "just_title", "just_subject", "outcome_mcell", "data_array"};
                    String[] values = {cp.getId(), activitySpecialServiceDetailsBinding.spTvJustificationType.getSelectedItem().toString(),
                            activitySpecialServiceDetailsBinding.etJustification.getText().toString(), activitySpecialServiceDetailsBinding.etOutcomeMonitoringCell.getText().toString(), jsonArrayData.toString()};
                    taskManager.doStartTask(keys, values, true, true);
                }
            }else{
                Utility.showAlertDialog(this,Constants.ERROR,"Please select number for Reconnection",false);

            }


        }

    }

    public void submitDisconnectionRequest() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_LI_DISCONNECTION);
        taskManager.setLIDisconnectionCreate(true);
        try {
            if (allSelectFlag) {

                jsonArrayData = new JSONArray();
                try {
                    if (requesterRole.equalsIgnoreCase("SEARCH")) {
                        for (int S = 0; S < result.getDetails().size(); S++) {
                            JSONObject jObj = new JSONObject();
                            jObj.put("id", result.getDetails().get(S).getRecordId());
                            jsonArrayData.put(jObj);
                        }
                    }

                    // String getRecId = "";
                    if (requesterRole.equalsIgnoreCase("IO")) {
                        for (int IO = 0; IO < io.getDetails().size(); IO++) {
                            JSONObject jObj = new JSONObject();
                            jObj.put("id", io.getDetails().get(IO).getRecordId());
                            jsonArrayData.put(jObj);
                        }
                    } else if (requesterRole.equalsIgnoreCase("OC")) {
                        for (int OC = 0; OC < oc.getDetails().size(); OC++) {
                            JSONObject jObj = new JSONObject();
                            jObj.put("id", oc.getDetails().get(OC).getRecordId());
                            jsonArrayData.put(jObj);
                        }
                    } else if (requesterRole.equalsIgnoreCase("DC")) {
                        for (int DC = 0; DC < dc.getDetails().size(); DC++) {
                            JSONObject jObj = new JSONObject();
                            jObj.put("id", dc.getDetails().get(DC).getRecordId());
                            jsonArrayData.put(jObj);
                        }
                    } else if (requesterRole.equalsIgnoreCase("JTCP")) {
                        for (int JTCP = 0; JTCP < jtcp.getDetails().size(); JTCP++) {
                            JSONObject jObj = new JSONObject();
                            jObj.put("id", jtcp.getDetails().get(JTCP).getRecordId());
                            jsonArrayData.put(jObj);
                        }
                    } else if (requesterRole.equalsIgnoreCase("ADDLCP")) {
                        for (int ADDLCP = 0; ADDLCP < addlcp.getDetails().size(); ADDLCP++) {
                            JSONObject jObj = new JSONObject();
                            String getRecId = addlcp.getDetails().get(ADDLCP).getRecordId();
                            jObj.put("id", getRecId);
                            jsonArrayData.put(jObj);
                        }
                    } else if (requesterRole.equalsIgnoreCase("CP")) {
                        for (int CP = 0; CP < cp.getDetails().size(); CP++) {
                            JSONObject jObj = new JSONObject();
                            String getRecId = cp.getDetails().get(CP).getRecordId();
                            jObj.put("id", getRecId);
                            jsonArrayData.put(jObj);
                        }
                    }


                } catch (Exception e) {

                }


            } else {
                jsonArrayData = new JSONArray();
                for (int i = 0; i < arrayListPartialDisconnection.size(); i++) {
                    JSONObject jObj = new JSONObject();
                    jObj.put("id", arrayListPartialDisconnection.get(i));
                    jsonArrayData.put(jObj);
                }

                arrayListPartialDisconnection.clear();
            }


        } catch (Exception e) {

        }

        if(jsonArrayData.length()>0) {
            if (requesterRole.equalsIgnoreCase("SEARCH")) {
                String[] keys = {"request_id", "data_array"};
                String[] values = {result.getId(), jsonArrayData.toString()};
                taskManager.doStartTask(keys, values, true, true);

            }

            if (requesterRole.equalsIgnoreCase("IO")) {
                String[] keys = {"request_id", "data_array"};
                String[] values = {io.getId(), jsonArrayData.toString()};
                taskManager.doStartTask(keys, values, true, true);

            }
            if (requesterRole.equalsIgnoreCase("OC")) {
                String[] keys = {"request_id", "data_array"};
                String[] values = {oc.getId(), jsonArrayData.toString()};
                taskManager.doStartTask(keys, values, true, true);

            }
            if (requesterRole.equalsIgnoreCase("DC")) {
                String[] keys = {"request_id", "data_array"};
                String[] values = {dc.getId(), jsonArrayData.toString()};
                taskManager.doStartTask(keys, values, true, true);

            }
            if (requesterRole.equalsIgnoreCase("JTCP")) {
                String[] keys = {"request_id", "data_array"};
                String[] values = {jtcp.getId(), jsonArrayData.toString()};
                taskManager.doStartTask(keys, values, true, true);

            }
            if (requesterRole.equalsIgnoreCase("ADDLCP")) {
                String[] keys = {"request_id", "data_array"};
                String[] values = {addlcp.getId(), jsonArrayData.toString()};
                taskManager.doStartTask(keys, values, true, true);

            }
            if (requesterRole.equalsIgnoreCase("CP")) {
                String[] keys = {"request_id", "data_array"};
                String[] values = {cp.getId(), jsonArrayData.toString()};
                taskManager.doStartTask(keys, values, true, true);
            }
        }else {
            Utility.showAlertDialog(this,Constants.ERROR,"Please select numner for disconnection",false);
        }


    }

    public void parseLIDisconnectionCreateResult(String response) {
        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optString("status").equals("1")) {
                    activitySpecialServiceDetailsBinding.CbAllDisconnection.setChecked(false);
                    Utility.showAlertDialog(this, Constants.SUCCESS, jsonObject.optString("message"), true);
                    boolean flag = true;
                    allSelectFlag = false;
                    alldisconnectionFlag = false;
                    setSpecialServiceAdapter(flag, alldisconnectionFlag);

                } else {
                    Utility.showAlertDialog(this, Constants.ERROR, jsonObject.optString("message"), false);
                }
            } catch (Exception e) {

            }
        }

    }

    public void selectAllcreatejsonArray() {

        if (requesterRole.equalsIgnoreCase("IO")) {
            for (int IO = 0; IO < io.getDetails().size(); IO++) {
                allselectArraylist.add(io.getDetails().get(IO).getRecordId());

            }
        } else if (requesterRole.equalsIgnoreCase("OC")) {
            for (int OC = 0; OC < oc.getDetails().size(); OC++) {
                allselectArraylist.add(oc.getDetails().get(OC).getRecordId());
            }
        } else if (requesterRole.equalsIgnoreCase("DC")) {
            for (int DC = 0; DC < dc.getDetails().size(); DC++) {
                allselectArraylist.add(dc.getDetails().get(DC).getRecordId());
            }
        } else if (requesterRole.equalsIgnoreCase("JTCP")) {
            for (int JTCP = 0; JTCP < jtcp.getDetails().size(); JTCP++) {
                allselectArraylist.add(jtcp.getDetails().get(JTCP).getRecordId());
            }
        } else if (requesterRole.equalsIgnoreCase("ADDLCP")) {
            for (int ADDLCP = 0; ADDLCP < addlcp.getDetails().size(); ADDLCP++) {
                allselectArraylist.add(addlcp.getDetails().get(ADDLCP).getRecordId());
            }
        } else if (requesterRole.equalsIgnoreCase("CP")) {
            for (int CP = 0; CP < cp.getDetails().size(); CP++) {
                allselectArraylist.add(cp.getDetails().get(CP).getRecordId());
            }
        }


    }

    public void parseLIReconnectionCreateResult(String response) {
        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optString("status").equals("1")) {
                    activitySpecialServiceDetailsBinding.CbAllDisconnection.setChecked(false);
                    Utility.showAlertDialog(this, Constants.SUCCESS, jsonObject.optString("message"), true);
                    boolean flag = true;
                    allSelectFlag = false;
                    alldisconnectionFlag = false;
                    setSpecialServiceAdapter(flag, alldisconnectionFlag);

                } else {
                    Utility.showAlertDialog(this, Constants.ERROR, jsonObject.optString("message"), false);
                }
            } catch (Exception e) {

            }
        }
    }


    public void createJustLayout() {
        activitySpecialServiceDetailsBinding.bttnDisconnectionSubmit.setVisibility(View.GONE);
        activitySpecialServiceDetailsBinding.llLastOutcomeMonitoring.setVisibility(View.VISIBLE);
        activitySpecialServiceDetailsBinding.CbAllDisconnection.setVisibility(View.VISIBLE);
        activitySpecialServiceDetailsBinding.CbAllDisconnection.setOnClickListener(this);

        activitySpecialServiceDetailsBinding.spTvJustificationType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                activitySpecialServiceDetailsBinding.etJustification.setText(justificationTemplate[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, justificationTitle);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        activitySpecialServiceDetailsBinding.spTvJustificationType.setAdapter(arrayAdapter);
        boolean flag = true;
        setSpecialServiceAdapter(flag, alldisconnectionFlag);
    }

    public void updateStatus() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_SPECIAL_SERVICE_SELF_APPROVAL);
        taskManager.setSpecilServiceUpdate(true);
        String[] keys = {"name", "user_id", "master_id", "div_code", "ps_code", "rank", "device_imei","urgent_general_flag"};
        String[] values = {Utility.getUserInfo(this).getName(), Utility.getUserInfo(this).getUserId(), requestID, Utility.getUserInfo(this).getUserDivisionCode(), Utility.getUserInfo(this).getUserPscode(), Utility.getUserInfo(this).getUserRank(), Utility.getImiNO(this),urgentGeneralFlag};
        taskManager.doStartTask(keys, values, true, true);
    }

    public void parseSpecialServiceUpdateResult(String response) {
        Log.e("DAPL", response);
        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optString("status").equals("1")) {
                    activitySpecialServiceDetailsBinding.tvStatusvalue.setText(jsonObject.optString("result"));
                    activitySpecialServiceDetailsBinding.tvGrantRequest.setVisibility(View.GONE);
                    showAlertDialog(this,Constants.SUCCESS,jsonObject.optString("message"),true);

                } else {
                    Utility.showAlertDialog(this, Constants.ERROR, jsonObject.optString("message"), false);
                }
            } catch (Exception e) {

            }
        } else {
            Utility.showAlertDialog(this, Constants.ERROR, Constants.ERROR_EXCEPTION_MSG, true);
        }

    }

    public void skipStatus() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_SPECIAL_SERVICE_SELF_SKIP);
        taskManager.setSpecilServiceSkipped(true);
        String[] keys = {"name", "user_id", "master_id", "div_code", "ps_code", "rank", "device_imei","urgent_general_flag"};
        String[] values = {Utility.getUserInfo(this).getName(), Utility.getUserInfo(this).getUserId(), requestID, Utility.getUserInfo(this).getUserDivisionCode(), Utility.getUserInfo(this).getUserPscode(), Utility.getUserInfo(this).getUserRank(), Utility.getImiNO(this),urgentGeneralFlag};
        taskManager.doStartTask(keys, values, true, true);
    }

    public void parseSpecialServiceItemSkipedResult(String response) {
        Log.d("DAPL", "RESPONSE:---------" + response);
        Log.e("DAPL", response);
        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optString("status").equals("1")) {
                    urgentGeneralFlag=jsonObject.optString("urgentGeneral_isTRue");
                    activitySpecialServiceDetailsBinding.tvStatusvalue.setText(jsonObject.optString("result"));
                    activitySpecialServiceDetailsBinding.tvSkipRequest.setVisibility(View.GONE);
                    showAlertDialog(this, Constants.SUCCESS, jsonObject.optString("message"), true);

                } else {
                    Utility.showAlertDialog(this, Constants.ERROR, jsonObject.optString("message"), false);
                }
            } catch (Exception e) {

            }
        } else {
            Utility.showAlertDialog(this, Constants.ERROR, Constants.ERROR_EXCEPTION_MSG, true);
        }

    }

    public void revokerequest() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_SPECIAL_SERVICE_REVOKE);
        taskManager.setRequetRevoked(true);
        String[] keys = {"name", "user_id", "master_id", "rank", "device_imei", "remarks","draft_save","complete_save"};
        String[] values = {Utility.getUserInfo(this).getName(), Utility.getUserInfo(this).getUserId(), requestID, Utility.getUserInfo(this).getUserRank(), Utility.getImiNO(this), regretremarks, draftSaveFlag, completeSaveFlag};
        taskManager.doStartTask(keys, values, true, true);
    }

    public void parseRequetRevokedReult(String response) {
        Log.d("DAPL", "RESPONSE:---------" + response);
        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optString("status").equals("1")) {
                    //Utility.showAlertDialog(this, Constants.SUCCESS, jsonObject.optString("message"), true);
                    activitySpecialServiceDetailsBinding.tvRevokeRequest.setVisibility(View.GONE);
                    showAlertDialog(this, Constants.SUCCESS, jsonObject.optString("message"), true);


                } else {
                    Utility.showAlertDialog(this, Constants.ERROR, jsonObject.optString("message"), false);
                }
            } catch (Exception e) {

            }
        } else {
            Utility.showAlertDialog(this, Constants.ERROR, Constants.ERROR_EXCEPTION_MSG, false);
        }

    }


    @Override
    public void OnPartialDisconnectionItemClick(int position, boolean itemflag) {
        // Toast.makeText(this, "Position: " + position + " checked", Toast.LENGTH_SHORT).show();
        if (itemflag) {


            if (requesterRole.equalsIgnoreCase("IO")) {
                String RecId = io.getDetails().get(position).getRecordId();
                arrayListPartialDisconnection.add(RecId);
            } else if (requesterRole.equalsIgnoreCase("OC")) {
                String RecId = oc.getDetails().get(position).getRecordId();
                arrayListPartialDisconnection.add(RecId);
            } else if (requesterRole.equalsIgnoreCase("DC")) {
                String RecId = dc.getDetails().get(position).getRecordId();
                arrayListPartialDisconnection.add(RecId);
            } else if (requesterRole.equalsIgnoreCase("JTCP")) {
                String RecId = jtcp.getDetails().get(position).getRecordId();
                arrayListPartialDisconnection.add(RecId);
            } else if (requesterRole.equalsIgnoreCase("ADDLCP")) {
                String RecId = addlcp.getDetails().get(position).getRecordId();
                arrayListPartialDisconnection.add(RecId);
            } else if (requesterRole.equalsIgnoreCase("CP")) {
                String RecId = cp.getDetails().get(position).getRecordId();
                arrayListPartialDisconnection.add(RecId);
            }

        } else {

            if (requesterRole.equalsIgnoreCase("IO")) {
                String RecId = io.getDetails().get(position).getRecordId();
                arrayListPartialDisconnection.remove(RecId);
            } else if (requesterRole.equalsIgnoreCase("OC")) {
                String RecId = oc.getDetails().get(position).getRecordId();
                arrayListPartialDisconnection.remove(RecId);
            } else if (requesterRole.equalsIgnoreCase("DC")) {
                String RecId = dc.getDetails().get(position).getRecordId();
                arrayListPartialDisconnection.remove(RecId);
            } else if (requesterRole.equalsIgnoreCase("JTCP")) {
                String RecId = jtcp.getDetails().get(position).getRecordId();
                arrayListPartialDisconnection.remove(RecId);
            } else if (requesterRole.equalsIgnoreCase("ADDLCP")) {
                String RecId = addlcp.getDetails().get(position).getRecordId();
                arrayListPartialDisconnection.remove(RecId);
            } else if (requesterRole.equalsIgnoreCase("CP")) {
                String RecId = cp.getDetails().get(position).getRecordId();
                arrayListPartialDisconnection.remove(RecId);
            }
            // String[] array = (String[]) arrayListPartialDisconnection.toArray(new String[arrayListPartialDisconnection.size()]);


        }


    }

    @Override
    public void onSpecialServiceDesignatedItemClick(int position, String type) {
        if (type.equals("parallalconnectivity")) {

            callSpecialServiceItemDesignatedAction("2", position);

        } else if (type.equals("partlyrevoked")) {

            callSpecialServiceItemDesignatedAction("1", position);
        } else if (type.equals("reconnectivity")) {

            callSpecialServiceItemDesignatedAction("3", position);
        }
    }

    private void callSpecialServiceItemDesignatedAction(String flag_type, int position) {
        String RecId = "";
        if (requesterRole.equalsIgnoreCase("IO")) {
            RecId = io.getDetails().get(position).getRecordId();

        } else if (requesterRole.equalsIgnoreCase("OC")) {
            RecId = oc.getDetails().get(position).getRecordId();

        } else if (requesterRole.equalsIgnoreCase("DC")) {
            RecId = dc.getDetails().get(position).getRecordId();

        } else if (requesterRole.equalsIgnoreCase("JTCP")) {
            RecId = jtcp.getDetails().get(position).getRecordId();

        } else if (requesterRole.equalsIgnoreCase("ADDLCP")) {
            RecId = addlcp.getDetails().get(position).getRecordId();

        } else if (requesterRole.equalsIgnoreCase("CP")) {
            RecId = cp.getDetails().get(position).getRecordId();

        }
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_SPECIAL_SERVICE_LI_DETAILS_STATUS_CHANGE);
        taskManager.setSpecialServiceLIDetailsStatusChange(true);
        String[] keys = {"flag_type", "li_details_id", "user_name", "rank", "user_device_imei"};
        String[] values = {flag_type, RecId, Utility.getUserInfo(this).getName(), "DESIGNATED OFFICER", Utility.getImiNO(this)};
        taskManager.doStartTask(keys, values, true, true);

    }

    public void parseSpecialServiceLIDetailsStatusChangeResult(String response) {

        Log.d("DAPL", "RESPONSE:---------" + response);
        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optString("status").equals("1")) {
                    showAlertDialog(this, Constants.SUCCESS, jsonObject.optString("message"), true);

                } else {
                    Utility.showAlertDialog(this, Constants.ERROR, jsonObject.optString("message"), false);
                }
            } catch (Exception e) {

            }
        } else {
            Utility.showAlertDialog(this, Constants.ERROR, Constants.ERROR_EXCEPTION_MSG, true);
        }


    }
    public void showAlertDialog(final Context context, String title, String message, final Boolean status) {
        final android.app.AlertDialog ad = new android.app.AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);


        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }
                        if(isRankChanged){
                            UserInfo userInfo=Utility.getUserInfo(SpecialServiceDetailsActivity.this);
                            userInfo.setUserRank(unModifiedRank);
                            Utility.setUserInfo(SpecialServiceDetailsActivity.this,userInfo);

                        }

                         finish();


                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }
}
