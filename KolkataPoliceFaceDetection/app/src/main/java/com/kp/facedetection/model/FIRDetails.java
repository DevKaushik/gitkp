package com.kp.facedetection.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 06-07-2016.
 */
public class FIRDetails implements Serializable {

    private String name="";
    private String alias="";
    private String address="";
    private String category="";
    private String io="";
    private String under_section="";
    private String crimeNo="";
    private String crimeYear="";
    private String crsGeneralId="";
    private String ttcriminalid="";
    private String ps_code="";
    private String ps_name="";
    private String case_ref="";
    private String case_date="";
    private String complaint="";
    private String complaint_address="";
    private String gd_details="";
    private String brief_fact="";
    private String court="";
    private String cgr_details="";
    private String warrant="";
    private String ac_comment="";
    private String dc_comment="";
    private String oc_comment="";
    private String fir_status="";
    private String pdf_url = "";
    private String pmReport_pdf_url = "";
    private String poLat = "";
    private String poLong = "";
    private String caseNo = "";
    private String caseYr = "";
    private String ps = "";
    private String crimeCategory = "";
    private String psNameAsCode = "";
    private String firYear = "";
    private String pmNo = "";
    private String pmYear = "";

    public String getPmNo() {
        return pmNo;
    }

    public void setPmNo(String pmNo) {
        this.pmNo = pmNo;
    }

    public String getPmYear() {
        return pmYear;
    }

    public void setPmYear(String pmYear) {
        this.pmYear = pmYear;
    }

    public String getPdf_url() {
        return pdf_url;
    }

    public void setPdf_url(String pdf_url) {
        this.pdf_url = pdf_url;
    }

    public String getPmReport_pdf_url() {
        return pmReport_pdf_url;
    }

    public void setPmReport_pdf_url(String pmReport_pdf_url) {
        this.pmReport_pdf_url = pmReport_pdf_url;
    }

    public String getFir_status() {
        return fir_status;
    }

    public void setFir_status(String fir_status) {
        this.fir_status = fir_status;
    }

    public String getOc_comment() {
        return oc_comment;
    }

    public void setOc_comment(String oc_comment) {
        this.oc_comment = oc_comment;
    }

    public String getDc_comment() {
        return dc_comment;
    }

    public void setDc_comment(String dc_comment) {
        this.dc_comment = dc_comment;
    }

    public String getAc_comment() {
        return ac_comment;
    }

    public void setAc_comment(String ac_comment) {
        this.ac_comment = ac_comment;
    }


    private List<FIROtherAccusedDetail> firOtherAccusedDetailList=new ArrayList<FIROtherAccusedDetail>();
    private List<FIRUnderSectionDetails> firUnderSectionDetailsList = new ArrayList<FIRUnderSectionDetails>();
    private List<ChargeSheetDetails> chargeSheetDetailsList = new ArrayList<ChargeSheetDetails>();
    private List<FIRWarrantDetails> warrantDetailsList = new ArrayList<FIRWarrantDetails>();
    private List<CriminalSearchPRStatus> prsStatusList = new ArrayList<CriminalSearchPRStatus>();
    private List<CaseTransferDetails> CaseTransferList=new ArrayList<CaseTransferDetails>();

    public List<CaseTransferDetails> getCaseTransferList() {
        return CaseTransferList;
    }

    public void setCaseTransferList(CaseTransferDetails obj) {
        CaseTransferList.add(obj);
    }

    public List<CriminalSearchPRStatus> getPrsStatusList() {
        return prsStatusList;
    }

    public void setPrsStatusList(CriminalSearchPRStatus obj) {
        prsStatusList.add(obj);
    }

    public List<ChargeSheetDetails> getChargeSheetDetailsList() {
        return chargeSheetDetailsList;
    }

    public void setChargeSheetDetailsList(ChargeSheetDetails obj) {
        chargeSheetDetailsList.add(obj);
    }

    public List<FIRWarrantDetails> getWarrantDetailsList() {
        return warrantDetailsList;
    }

    public void setWarrantDetailsList(FIRWarrantDetails obj) {
        warrantDetailsList.add(obj);

    }

    public List<FIRUnderSectionDetails> getFirUnderSectionDetailsList() {
        return firUnderSectionDetailsList;
    }

    public void setFirUnderSectionDetailsList(FIRUnderSectionDetails obj) {
        firUnderSectionDetailsList.add(obj);
    }

    public List<FIROtherAccusedDetail> getFirOtherAccusedDetailList() {
        return firOtherAccusedDetailList;
    }

    public void setFirOtherAccusedDetailList(FIROtherAccusedDetail obj) {
        firOtherAccusedDetailList.add(obj);
    }

    public String getComplaint() {
        return complaint;
    }

    public void setComplaint(String complaint) {
        this.complaint = complaint;
    }

    public String getGd_details() {
        return gd_details;
    }

    public void setGd_details(String gd_details) {
        this.gd_details = gd_details;
    }

    public String getBrief_fact() {
        return brief_fact;
    }

    public void setBrief_fact(String brief_fact) {
        this.brief_fact = brief_fact;
    }

    public String getCourt() {
        return court;
    }

    public void setCourt(String court) {
        this.court = court;
    }

    public String getCgr_details() {
        return cgr_details;
    }

    public void setCgr_details(String cgr_details) {
        this.cgr_details = cgr_details;
    }

    public String getWarrant() {
        return warrant;
    }

    public void setWarrant(String warrant) {
        this.warrant = warrant;
    }



    public String getCase_date() {
        return case_date;
    }

    public void setCase_date(String case_date) {
        this.case_date = case_date;
    }

    public String getCase_ref() {
        return case_ref;
    }

    public void setCase_ref(String case_ref) {
        this.case_ref = case_ref;
    }

    public String getPs_code() {
        return ps_code;
    }

    public void setPs_code(String ps_code) {
        this.ps_code = ps_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIo() {
        return io;
    }

    public void setIo(String io) {
        this.io = io;
    }

    public String getUnder_section() {
        return under_section;
    }

    public void setUnder_section(String under_section) {
        this.under_section = under_section;
    }

    public String getCrimeNo() {
        return crimeNo;
    }

    public void setCrimeNo(String crimeNo) {
        this.crimeNo = crimeNo;
    }

    public String getCrimeYear() {
        return crimeYear;
    }

    public void setCrimeYear(String crimeYear) {
        this.crimeYear = crimeYear;
    }

    public String getCrsGeneralId() {
        return crsGeneralId;
    }

    public void setCrsGeneralId(String crsGeneralId) {
        this.crsGeneralId = crsGeneralId;
    }

    public String getTtcriminalid() {
        return ttcriminalid;
    }

    public void setTtcriminalid(String ttcriminalid) {
        this.ttcriminalid = ttcriminalid;
    }

    public String getPs_name() {
        return ps_name;
    }

    public void setPs_name(String ps_name) {
        this.ps_name = ps_name;
    }

    public String getPoLat() {
        return poLat;
    }

    public void setPoLat(String poLat) {
        this.poLat = poLat;
    }

    public String getPoLong() {
        return poLong;
    }

    public void setPoLong(String poLong) {
        this.poLong = poLong;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }

    public String getPsNameAsCode() {
        return psNameAsCode;
    }

    public void setPsNameAsCode(String psNameAsCode) {
        this.psNameAsCode = psNameAsCode;
    }

    public String getCaseYr() {
        return caseYr;
    }

    public void setCaseYr(String caseYr) {
        this.caseYr = caseYr;
    }

    public String getFirYear() {
        return firYear;
    }

    public void setFirYear(String firYear) {
        this.firYear = firYear;
    }

    public String getComplaint_address() {
        return complaint_address;
    }

    public void setComplaint_address(String complaint_address) {
        this.complaint_address = complaint_address;
    }
}
