package com.kp.facedetection.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 25-07-2016.
 */
public class CaseSearchFIRChargeSheet implements Serializable {

    private String courtOf="";
    private String district="";
    private String ps="";
    private String chgYear="";
    private String firNo="";
    private String firDate="";
    private String frChgNo="";
    private String frChgDate="";
    private String acts="";
    private String actOther="";
    private String frType="";
    private String frtpeDetails="";
    private String frUnOccured="";
    private String frUnOccuredType="";
    private String supplementaryOriginal="";
    private String ioCode="";
    private String dtComplntInformed="";
    private String noAccusedChg="";
    private String noAccusedNotChg="";
    private String actionFrFalse="";
    private String resultLabAnalysis="";
    private String firYr="";
    private String slNo="";
    private String briefFact="";

    private List<CaseSearchWitnessDetails> caseSearchWitnessDetailsList = new ArrayList<CaseSearchWitnessDetails>();
    private List<CaseSearchAccusedDetails> caseSearchAccusedDetailsList = new ArrayList<CaseSearchAccusedDetails>();
    private List<CaseSearchAccusedDetails> caseSearchNotAccusedDetailsList = new ArrayList<CaseSearchAccusedDetails>();
    private List<CaseSearchPropertyDetails> caseSearchPropertyDetailsList = new ArrayList<CaseSearchPropertyDetails>();

    public List<CaseSearchPropertyDetails> getCaseSearchPropertyDetailsList() {
        return caseSearchPropertyDetailsList;
    }

    public void setCaseSearchPropertyDetailsList(CaseSearchPropertyDetails obj) {
        caseSearchPropertyDetailsList.add(obj);
    }


    public List<CaseSearchAccusedDetails> getCaseSearchNotAccusedDetailsList() {
        return caseSearchNotAccusedDetailsList;
    }

    public void setCaseSearchNotAccusedDetailsList(CaseSearchAccusedDetails obj) {
        caseSearchNotAccusedDetailsList.add(obj);
    }

    public List<CaseSearchAccusedDetails> getCaseSearchAccusedDetailsList() {
        return caseSearchAccusedDetailsList;
    }

    public void setCaseSearchAccusedDetailsList(CaseSearchAccusedDetails obj) {
        caseSearchAccusedDetailsList.add(obj);
    }

    public List<CaseSearchWitnessDetails> getCaseSearchWitnessDetailsList() {
        return caseSearchWitnessDetailsList;
    }

    public void setCaseSearchWitnessDetailsList(CaseSearchWitnessDetails obj) {
        caseSearchWitnessDetailsList.add(obj);
    }

    public String getBriefFact() {
        return briefFact;
    }

    public void setBriefFact(String briefFact) {
        this.briefFact = briefFact;
    }

    public String getCourtOf() {
        return courtOf;
    }

    public void setCourtOf(String courtOf) {
        this.courtOf = courtOf;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }

    public String getChgYear() {
        return chgYear;
    }

    public void setChgYear(String chgYear) {
        this.chgYear = chgYear;
    }

    public String getFirNo() {
        return firNo;
    }

    public void setFirNo(String firNo) {
        this.firNo = firNo;
    }

    public String getFirDate() {
        return firDate;
    }

    public void setFirDate(String firDate) {
        this.firDate = firDate;
    }

    public String getFrChgNo() {
        return frChgNo;
    }

    public void setFrChgNo(String frChgNo) {
        this.frChgNo = frChgNo;
    }

    public String getFrChgDate() {
        return frChgDate;
    }

    public void setFrChgDate(String frChgDate) {
        this.frChgDate = frChgDate;
    }

    public String getActs() {
        return acts;
    }

    public void setActs(String acts) {
        this.acts = acts;
    }

    public String getActOther() {
        return actOther;
    }

    public void setActOther(String actOther) {
        this.actOther = actOther;
    }

    public String getFrType() {
        return frType;
    }

    public void setFrType(String frType) {
        this.frType = frType;
    }

    public String getFrtpeDetails() {
        return frtpeDetails;
    }

    public void setFrtpeDetails(String frtpeDetails) {
        this.frtpeDetails = frtpeDetails;
    }

    public String getFrUnOccured() {
        return frUnOccured;
    }

    public void setFrUnOccured(String frUnOccured) {
        this.frUnOccured = frUnOccured;
    }

    public String getFrUnOccuredType() {
        return frUnOccuredType;
    }

    public void setFrUnOccuredType(String frUnOccuredType) {
        this.frUnOccuredType = frUnOccuredType;
    }

    public String getSupplementaryOriginal() {
        return supplementaryOriginal;
    }

    public void setSupplementaryOriginal(String supplementaryOriginal) {
        this.supplementaryOriginal = supplementaryOriginal;
    }

    public String getIoCode() {
        return ioCode;
    }

    public void setIoCode(String ioCode) {
        this.ioCode = ioCode;
    }

    public String getDtComplntInformed() {
        return dtComplntInformed;
    }

    public void setDtComplntInformed(String dtComplntInformed) {
        this.dtComplntInformed = dtComplntInformed;
    }

    public String getNoAccusedChg() {
        return noAccusedChg;
    }

    public void setNoAccusedChg(String noAccusedChg) {
        this.noAccusedChg = noAccusedChg;
    }

    public String getNoAccusedNotChg() {
        return noAccusedNotChg;
    }

    public void setNoAccusedNotChg(String noAccusedNotChg) {
        this.noAccusedNotChg = noAccusedNotChg;
    }

    public String getActionFrFalse() {
        return actionFrFalse;
    }

    public void setActionFrFalse(String actionFrFalse) {
        this.actionFrFalse = actionFrFalse;
    }

    public String getResultLabAnalysis() {
        return resultLabAnalysis;
    }

    public void setResultLabAnalysis(String resultLabAnalysis) {
        this.resultLabAnalysis = resultLabAnalysis;
    }

    public String getFirYr() {
        return firYr;
    }

    public void setFirYr(String firYr) {
        this.firYr = firYr;
    }

    public String getSlNo() {
        return slNo;
    }

    public void setSlNo(String slNo) {
        this.slNo = slNo;
    }
}
