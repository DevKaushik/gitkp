package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnItemClickListenerForChargesheetDue;
import com.kp.facedetection.interfaces.OnItemClickListenerForUnnaturalDeath;
import com.kp.facedetection.model.ChargesheetDueDetails;
import com.kp.facedetection.model.CrimeReviewDetails;

import java.util.ArrayList;

/**
 * Created by DAT-Asset-128 on 04-12-2017.
 */

public class ChargesheetDueListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    Context con;
    String from;
    ArrayList<ChargesheetDueDetails> psList = new ArrayList<>();
    OnItemClickListenerForChargesheetDue onItemClickListenerForChargesheetDue;

    public ChargesheetDueListAdapter(Context con, ArrayList<ChargesheetDueDetails> psList ,String from) {
        this.con = con;
        this.psList = psList;
        this.from = from;
    }
    public void setOnClickListener(OnItemClickListenerForChargesheetDue onItemClickListenerForChargesheetDue){
        this.onItemClickListenerForChargesheetDue=onItemClickListenerForChargesheetDue;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view ;
        if(from.equalsIgnoreCase("CS")) {
            view = inflater.inflate(R.layout.chargesheet_caselist_item_layout, null, false);
            return new MyViewHolderCase(view);


        }
        else {
            view = inflater.inflate(R.layout.chargesheet_pslist_item_layout, null, false);
            return new MyViewHolderPs(view);

        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ChargesheetDueDetails chargesheetDueDetails = psList.get(position);

        if (holder instanceof MyViewHolderPs) {
            ((MyViewHolderPs) holder).bind(position);
             ((MyViewHolderPs) holder).tv_categoryName.setText(chargesheetDueDetails.getPsName());
             ((MyViewHolderPs) holder).tv_count.setText(chargesheetDueDetails.getNoOfCase());

        } else if(holder instanceof MyViewHolderCase){
            ((MyViewHolderCase) holder).bind(position);
            ((MyViewHolderCase)holder).tv_chargesheetDetails.setText(" C/No "+chargesheetDueDetails.getCaseNo()+" dated "+chargesheetDueDetails.getCaseDate() + " u/s "+chargesheetDueDetails.getUnderSection());

        }


    }

    @Override
    public int getItemCount() {
        return psList == null ? 0 : psList.size();
    }
    public  class MyViewHolderPs extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView tv_categoryName, tv_count ;

        public MyViewHolderPs(View itemView) {
            super(itemView);
            tv_categoryName = (TextView) itemView.findViewById(R.id.tv_categoryName);
            tv_count = (TextView) itemView.findViewById(R.id.tv_count);
            //  tv_count.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(onItemClickListenerForChargesheetDue!=null){
                onItemClickListenerForChargesheetDue.onClickChargesheetDue(v,getAdapterPosition());
            }

        }
        public  void bind(int pos) {
        }
    }
    public  class MyViewHolderCase extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView tv_chargesheetDetails;

        public MyViewHolderCase(View itemView) {
            super(itemView);
            tv_chargesheetDetails = (TextView) itemView.findViewById(R.id.tv_chargesheet_case_details);
            tv_chargesheetDetails.setOnClickListener(this);
            //itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(onItemClickListenerForChargesheetDue!=null){
                onItemClickListenerForChargesheetDue.onClickChargesheetDue(v,getAdapterPosition());
            }

        }
        public  void bind(int pos) {
        }
    }

}
