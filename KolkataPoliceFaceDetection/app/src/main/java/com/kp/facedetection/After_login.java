package com.kp.facedetection;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kp.facedetection.fragments.AllDocumentDataSearchFragment;
import com.kp.facedetection.fragments.CriminalSearchFragment;
import com.kp.facedetection.fragments.HighCourtSearchFragment;
import com.kp.facedetection.fragments.MobileStolenFragment;
import com.kp.facedetection.fragments.ModifiedCaseSearchFragment;
import com.kp.facedetection.fragments.ModifiedDocumentsFragment;
import com.kp.facedetection.fragments.PMReportFragment;
import com.kp.facedetection.fragments.WarrantSearchFragment;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by DAT-Asset-40 on 07-12-2015.
 */

public class After_login extends BaseActivity implements View.OnClickListener, Observer {
    TabHost tabHost;
    private String userName = "";
    private String loginNumber = "";
    private LatLng latLng;
    private String appVersion = "";
    TextView chargesheetNotificationCount;
    String notificationCount="";
    String devicetoken="";
    String user_id="";
    String documentTabid="";
    private boolean doubleBackToExitPressedOnce;


    @SuppressLint("NewApi")
    @Override
    public void onBackPressed(){
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        Toast.makeText(this, "Double tap to exit", Toast.LENGTH_SHORT).show();
        this.doubleBackToExitPressedOnce = true;
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
        
    }
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.after_login);
        ObservableObject.getInstance().addObserver(this);

        try {
            user_id = Utility.getUserInfo(this).getUserId();
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
          // devicetoken = GCMRegistrar.getRegistrationId(this);


            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        TabWidget widget = (TabWidget) findViewById(android.R.id.tabs);

        tabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabHost.setup();

        Resources res = getResources();
        Configuration cfg = res.getConfiguration();
        boolean hor = cfg.orientation == Configuration.ORIENTATION_LANDSCAPE;

        if (hor) {
            widget = tabHost.getTabWidget();
            widget.setOrientation(LinearLayout.VERTICAL);
        }


        TabHost.TabSpec spec1 = tabHost.newTabSpec("tab1");
        spec1.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });

        View tabIndicator1 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((TextView) tabIndicator1.findViewById(R.id.tab_title)).setText("All Data Search");
        ((TextView) tabIndicator1.findViewById(R.id.tab_title)).setTextSize(10);
        ((TextView) tabIndicator1.findViewById(R.id.tab_title)).setGravity(Gravity.CENTER);
        ((ImageView) tabIndicator1.findViewById(R.id.tab_icon)).setImageResource(R.drawable.case_search);
        spec1.setIndicator(tabIndicator1);

        tabHost.addTab(spec1);

        /*  --Criminal Search Tab*/
        TabHost.TabSpec spec2 = tabHost.newTabSpec("tab2");
        spec2.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });

        View tabIndicator = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((TextView) tabIndicator.findViewById(R.id.tab_title)).setText("Criminal Search");
        ((TextView) tabIndicator.findViewById(R.id.tab_title)).setTextSize(10);
        ((TextView) tabIndicator.findViewById(R.id.tab_title)).setGravity(Gravity.CENTER);
        ((ImageView) tabIndicator.findViewById(R.id.tab_icon)).setImageResource(R.drawable.detail_search);

        spec2.setIndicator(tabIndicator);
        // spec1.setIndicator("details search",
        // getResources().getDrawable(setBackgroundColor(view.Color.RED));
      /*  if(android.os.Build.VERSION.SDK_INT >= 21){


            spec1.setIndicator("detail Search", getResources().getDrawable(R.drawable.detail_search,getTheme()));
        } else {
            spec1.setIndicator("detail Search", getResources().getDrawable(R.drawable.detail_search));
        }*/
        //spec1.setIndicator("detail search", null);

        tabHost.addTab(spec2);

		/*TabHost.TabSpec spec2 = tabHost.newTabSpec("tab2");
        spec2.setContent(new TabHost.TabContentFactory() {
			public View createTabContent(String tag) {
				return findViewById(R.id.realtabcontent);
			}
		});

		View tabIndicator2 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
		((TextView) tabIndicator2.findViewById(R.id.tab_title)).setText("Image Search");
		((TextView) tabIndicator2.findViewById(R.id.tab_title)).setTextSize(10);
		((TextView) tabIndicator2.findViewById(R.id.tab_title)).setGravity(Gravity.CENTER);
		((ImageView) tabIndicator2.findViewById(R.id.tab_icon)).setImageResource(R.drawable.image_search);
		spec2.setIndicator(tabIndicator2);*/
        /*
        if(android.os.Build.VERSION.SDK_INT >= 21){
            spec2.setIndicator("", getResources().getDrawable(R.drawable.image_search,getTheme()));
        } else {
            spec2.setIndicator("", getResources().getDrawable(R.drawable.image_search));
        }*/


        //tabHost.addTab(spec2);

        /*  --Case Search Tab */
        TabHost.TabSpec spec3 = tabHost.newTabSpec("tab3");
        spec3.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });

        View tabIndicator3 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((TextView) tabIndicator3.findViewById(R.id.tab_title)).setText("Case Search");
        ((TextView) tabIndicator3.findViewById(R.id.tab_title)).setTextSize(10);
        ((TextView) tabIndicator3.findViewById(R.id.tab_title)).setGravity(Gravity.CENTER);
        ((ImageView) tabIndicator3.findViewById(R.id.tab_icon)).setImageResource(R.drawable.case_search);
        spec3.setIndicator(tabIndicator3);

        tabHost.addTab(spec3);


        /*  --Warrant Tab */
        TabHost.TabSpec spec4 = tabHost.newTabSpec("tab4");
        spec4.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });

        View tabIndicator4 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((TextView) tabIndicator4.findViewById(R.id.tab_title)).setText("Warrant Search");
        ((TextView) tabIndicator4.findViewById(R.id.tab_title)).setTextSize(10);
        ((TextView) tabIndicator4.findViewById(R.id.tab_title)).setGravity(Gravity.CENTER);
        ((ImageView) tabIndicator4.findViewById(R.id.tab_icon)).setImageResource(R.drawable.warrant_search);
        spec4.setIndicator(tabIndicator4);

        tabHost.addTab(spec4);

        /* -- Image Search  */
     /*   TabHost.TabSpec spec5 = tabHost.newTabSpec("tab5");
        spec5.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });

        View tabIndicator5 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((TextView) tabIndicator5.findViewById(R.id.tab_title)).setText("Image Search");
        ((TextView) tabIndicator5.findViewById(R.id.tab_title)).setTextSize(10);
        ((TextView) tabIndicator5.findViewById(R.id.tab_title)).setGravity(Gravity.CENTER);
        ((ImageView) tabIndicator5.findViewById(R.id.tab_icon)).setImageResource(R.drawable.image_search);
        spec5.setIndicator(tabIndicator5);

        tabHost.addTab(spec5);*/

        /*  --Document tab  */
        TabHost.TabSpec spec6 = tabHost.newTabSpec("tab6");
        spec6.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });

        View tabIndicator6 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((TextView) tabIndicator6.findViewById(R.id.tab_title)).setText("Documents");
        ((TextView) tabIndicator6.findViewById(R.id.tab_title)).setTextSize(10);
        ((TextView) tabIndicator6.findViewById(R.id.tab_title)).setGravity(Gravity.CENTER);
        ((ImageView) tabIndicator6.findViewById(R.id.tab_icon)).setImageResource(R.drawable.docs_new);
        spec6.setIndicator(tabIndicator6);

        tabHost.addTab(spec6);


        /*  --Lost Mobile  */
        TabHost.TabSpec spec7 = tabHost.newTabSpec("tab7");
        spec7.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });

        View tabIndicator7 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((TextView) tabIndicator7.findViewById(R.id.tab_title)).setText("Lost Mobile");
        ((TextView) tabIndicator7.findViewById(R.id.tab_title)).setTextSize(10);
        ((TextView) tabIndicator7.findViewById(R.id.tab_title)).setGravity(Gravity.CENTER);
        ((ImageView) tabIndicator7.findViewById(R.id.tab_icon)).setImageResource(R.drawable.mobile_stolen);
        spec7.setIndicator(tabIndicator7);

        tabHost.addTab(spec7);


        /*  --PMReport */
        TabHost.TabSpec spec8 = tabHost.newTabSpec("tab8");
        spec8.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });

        View tabIndicator8 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((TextView) tabIndicator8.findViewById(R.id.tab_title)).setText("PM Report");
        ((TextView) tabIndicator8.findViewById(R.id.tab_title)).setTextSize(10);
        ((TextView) tabIndicator8.findViewById(R.id.tab_title)).setGravity(Gravity.CENTER);
        ((ImageView) tabIndicator8.findViewById(R.id.tab_icon)).setImageResource(R.drawable.report_icon);
        spec8.setIndicator(tabIndicator8);

        tabHost.addTab(spec8);

        /*Search Map*/

       /* TabHost.TabSpec spec9 = tabHost.newTabSpec("tab9");
        spec9.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });

        View tabIndicator9 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((TextView) tabIndicator9.findViewById(R.id.tab_title)).setText("MAP");
        ((TextView) tabIndicator9.findViewById(R.id.tab_title)).setTextSize(10);
        ((TextView) tabIndicator9.findViewById(R.id.tab_title)).setGravity(Gravity.CENTER);
        ((ImageView) tabIndicator9.findViewById(R.id.tab_icon)).setImageResource(R.drawable.map);
        spec9.setIndicator(tabIndicator9);

        tabHost.addTab(spec9);*/


        TabHost.TabSpec spec10 = tabHost.newTabSpec("tab10");
        spec10.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });

        View tabIndicator10 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((TextView) tabIndicator10.findViewById(R.id.tab_title)).setText("Cause List");
        ((TextView) tabIndicator10.findViewById(R.id.tab_title)).setTextSize(10);
        ((TextView) tabIndicator10.findViewById(R.id.tab_title)).setGravity(Gravity.CENTER);
        ((ImageView) tabIndicator10.findViewById(R.id.tab_icon)).setImageResource(R.drawable.highcourt);
        spec10.setIndicator(tabIndicator10);

        tabHost.addTab(spec10);

        /*
        * Capture FIR
        * */
        /*TabHost.TabSpec spec11 = tabHost.newTabSpec("tab11");
        spec11.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });

        View tabIndicator11 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((TextView) tabIndicator11.findViewById(R.id.tab_title)).setText("Capture Lat/Long");
        ((TextView) tabIndicator11.findViewById(R.id.tab_title)).setTextSize(10);
        ((TextView) tabIndicator11.findViewById(R.id.tab_title)).setGravity(Gravity.CENTER);
        ((ImageView) tabIndicator11.findViewById(R.id.tab_icon)).setImageResource(R.drawable.refresh_location);
        spec11.setIndicator(tabIndicator11);

        tabHost.addTab(spec11);*/



        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {

                Log.e("Change", "Change");
                for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
                    tabHost.getTabWidget().getChildAt(i)
                            .setBackgroundResource(R.color.color_light_blue); // unselected
                }
                tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab())
                        .setBackgroundResource(R.color.color_deep_blue); // selected

                if (tabId.equals("tab1")) {
                    documentTabid=tabId;
                    getAllSearchSecurityCode();

                }
				else if (tabId.equals("tab2")) {
					pushFragments(tabId, new CriminalSearchFragment(), true, false);
				}
                else if (tabId.equals("tab3")) {
                    pushFragments(tabId, new ModifiedCaseSearchFragment(), true, false);
                } else if (tabId.equals("tab4")) {
                    pushFragments(tabId, new WarrantSearchFragment().newInstance(), true, false);
                }
                /*else if (tabId.equals("tab5")) {
                    pushFragments(tabId, new ModifiedImageSearchFragment().newInstance(), true, false);
                } */
                else if (tabId.equals("tab6")) {
                    documentTabid=tabId;
                    getDocumentSecurityCode(tabId);

                } else if (tabId.equals("tab7")) {
                    pushFragments(tabId, new MobileStolenFragment().newInstance(), true, false);
                } else if (tabId.equals("tab8")) {
                    pushFragments(tabId, new PMReportFragment().newInstance(), true, false);
                } /*else if (tabId.equals("tab9")) {
         a           pushFragments(tabId, new MapSerachFragment().newInstance(), true, false);
                }*/else if (tabId.equals("tab10")) {
                    pushFragments(tabId, new HighCourtSearchFragment().newInstance(), true, false);
                }
                /*else if (tabId.equals("tab11")) {
                    pushFragments(tabId, new CaptureFIRFragment().newInstance(), true, false);
                }*/else {

                }

            }
        });
        //
         tabHost.getTabWidget().getChildAt(0).performClick();
        //tabHost.setCurrentTab(1);
        tabHost.setCurrentTab(0);
        pushFragments("tab1", new AllDocumentDataSearchFragment(), true, false);
        tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab())
                .setBackgroundResource(R.color.color_deep_blue);
        getAllSearchSecurityCode();



    }
    public void getAllSearchSecurityCode()
    {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_DOCUMENT_SESSION_ID);
        taskManager.setSessionForAllSearch(true);
//		String devicetoken = PushUtility.fetchC2DMToken(this);
//      String devicetoken = GCMRegistrar.getRegistrationId(this);
        if(FirebaseInstanceId.getInstance().getToken()!=null) {
            devicetoken = FirebaseInstanceId.getInstance().getToken();
        }
        else{
            devicetoken=Utility.getFCMToken(this);
        }

        String[] keys = {"user_id","device_type", "device_token","imei","login_count"};
       /* String[] values = {et_userid.getText().toString().trim(),
                et_password.getText().toString(), "android", devicetoken,Constants.device_IMEI_No};*///Constants.device_IMEI_No
        String[] values = {user_id,"android",devicetoken,Utility.getImiNO(this),loginNumber};//Utility.getImiNO(this)
        taskManager.doStartTask(keys, values, true, true);


    }

    public void parseAllSearchAunthicatedResult(String response){
        if (response != null && !response.equals("")) {
            try {
                JSONObject jObj = new JSONObject(response);
                // L.e("response---------"+response);
                if (jObj.optString("status").equalsIgnoreCase("1")) {
                    if(jObj.optString("session_id")!=null && !jObj.optString("session_id").equalsIgnoreCase("")&& !jObj.optString("session_id").equalsIgnoreCase("null")) {
                        String session_id = jObj.optString("session_id");
                        //L.e("session_id---------"+session_id);
                        Utility.setDocumentSearchSesionId(After_login.this, session_id);
                        pushFragments("tab1", new AllDocumentDataSearchFragment(), true, false);
                        tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab())
                                .setBackgroundResource(R.color.color_deep_blue);


                    }
                }
                else{
                    showAlertDialog(this," Search Error ",Constants.ERROR_MSG_TO_RELOAD,false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void getDocumentSecurityCode(final String tabId)
    {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_DOCUMENT_SESSION_ID);
        taskManager.setSessionForDocumentSearch(true);
//		String devicetoken = PushUtility.fetchC2DMToken(this);
    //    String devicetoken = GCMRegistrar.getRegistrationId(this);

       // devicetoken = FirebaseInstanceId.getInstance().getToken();
       // Log.e("DAPL","DEVICE TOKEN in Login:----------"+devicetoken);


        if(FirebaseInstanceId.getInstance().getToken()!=null) {
            devicetoken = FirebaseInstanceId.getInstance().getToken();

        }
        else{
            devicetoken=Utility.getFCMToken(this);
        }


        String[] keys = {"user_id","device_type", "device_token","imei","login_count"};
       /* String[] values = {et_userid.getText().toString().trim(),
                et_password.getText().toString(), "android", devicetoken,Constants.device_IMEI_No};*///Constants.device_IMEI_No
        String[] values = {user_id,"android",devicetoken, Utility.getImiNO(this),loginNumber};
        taskManager.doStartTask(keys, values, true, true);


    }
    public void parseDocumentSearchAunthicatedResult(String response){
        if (response != null && !response.equals("")) {
            try {
                JSONObject jObj = new JSONObject(response);
               // L.e("response---------"+response);
               if (jObj.optString("status").equalsIgnoreCase("1")) {
                    if(jObj.optString("session_id")!=null && !jObj.optString("session_id").equalsIgnoreCase("")&& !jObj.optString("session_id").equalsIgnoreCase("null")) {
                        String session_id = jObj.optString("session_id");
                        //L.e("session_id---------"+session_id);
                        Utility.setDocumentSearchSesionId(After_login.this, session_id);
                        pushFragments(documentTabid, new ModifiedDocumentsFragment().newInstance(), true, false);
                    }
                }
                else{
                    Utility.showAlertDialog(this," Search Error ",Constants.ERROR_MSG_TO_RELOAD,false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
    public String readStream(InputStream in) {

        StringBuilder sb = new StringBuilder();

        try {
            Reader reader  = new InputStreamReader(in);
            int data = reader.read();
            while (data != -1) {
                char theChar = (char) data;
                data = reader.read();
                sb.append(theChar);

            }

            reader.close();
        }
        catch (Exception e) {

        }
        return sb.toString();
    }


    public void pushFragments(String tag, Fragment fragment,
                              boolean shouldAnimate, boolean shouldAdd) {

        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        transaction.replace(R.id.realtabcontent, fragment);
        //transaction.add(R.id.realtabcontent, fragment);
        transaction.commit();
    }

    public void onClickPush(View view) {

    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
           /* case R.id.menu_send_push:
                //tabHost.setCurrentTab(2);
                //startActivity(new Intent(this, SendPushActivity.class));
                //	pushFragments("Push", new SendPushActivity(), true, false);
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                // Utility.logout(this);
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
        }

    }

    @Override
    public void update(Observable observable, Object data) {
       // chargesheetNotificationCount.setText(String.valueOf(data));
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        Log.e("After_Login_noti",String.valueOf(count));
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                       /* tabHost.getTabWidget().getChildAt(1).performClick();
                        tabHost.setCurrentTab(1);*/
                        tabHost.getTabWidget().getChildAt(0)
                                .setBackgroundResource(R.color.color_light_blue);
                        tabHost.getTabWidget().getChildAt(1)
                                .setBackgroundResource(R.color.color_deep_blue);
                        pushFragments("tab2", new CriminalSearchFragment(), true, false);

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

}
