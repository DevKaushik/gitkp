package com.kp.facedetection.interfaces;

/**
 * Created by user on 22-05-2018.
 */

public interface OnSpecialServiceDesignatedActionClickListener {
    void onSpecialServiceDesignatedItemClick(int position,String type);
}
