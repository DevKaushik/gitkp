package com.kp.facedetection.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.kp.facedetection.R;
import com.kp.facedetection.imageloader.ImageLoader;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-Asset-131 on 05-07-2016.
 */
public class MyClickableSpan extends ClickableSpan {

    int pos;
    Context ctx;
    private List<String> CRSImageList;

    private List<String> imageExtensionList = new ArrayList<String>();

    ImageLoader imageLoader;

    private boolean imageStatus = false;


    public MyClickableSpan(int position, Context ctx, List<String> CRSImageList) {
        this.pos = position;
        this.ctx = ctx;
        this.CRSImageList = CRSImageList;
        imageLoader = new ImageLoader(ctx);
    }

    @Override
    public void onClick(final View textView) {
        showImagePopup(pos);
    }

    @Override
    public void updateDrawState(final TextPaint textPaint) {
        textPaint.setColor(ContextCompat.getColor(ctx, R.color.colorPrimaryDark));
        textPaint.setUnderlineText(true);
    }

    public void showImagePopup(final int position) {

        imageExtensionList.clear();

        imageExtensionList.add("bmp");
        imageExtensionList.add("gif");
        imageExtensionList.add("jpg");
        imageExtensionList.add("jpeg");
        imageExtensionList.add("png");

        Dialog imageDialog = new Dialog(ctx);
        imageDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View inflatedView;

        LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        inflatedView = layoutInflater.inflate(R.layout.image_layout, null, false);
        imageDialog.setContentView(inflatedView);

        final ImageView iv_popup = (ImageView) inflatedView.findViewById(R.id.iv_popup);


        String original_url = CRSImageList.get(position);
        final String extension = original_url.substring(original_url.lastIndexOf(".") + 1);
        System.out.println("Extension: " + extension);

        imageExtensionList.remove(imageExtensionList.indexOf(extension));
        new Thread(new Runnable() {
            @Override
            public void run() {

                if (imageLoader.getBitmap(CRSImageList.get(position)) != null) {
                    ((Activity) ctx).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            imageLoader.DisplayImage(
                                    CRSImageList.get(position),
                                    iv_popup);

                        }
                    });
                } else {

                    for (int i = 0; i < imageExtensionList.size(); i++) {

                        if (imageLoader.getBitmap(CRSImageList.get(position).replace(extension, imageExtensionList.get(i))) != null) {
                            final int finalI = i;
                            ((Activity) ctx).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    imageLoader.DisplayImage(
                                            CRSImageList.get(position).replace(extension, imageExtensionList.get(finalI)),
                                            iv_popup);

                                }
                            });
                            break;
                        }

                    }
                }

            }
        }).start();

        imageDialog.show();

    }

}