package com.kp.facedetection.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.kp.facedetection.R;
import com.kp.facedetection.SearchResultActivity;
import com.kp.facedetection.adapter.CustomDialogAdapterForCategoryCrimeList;
import com.kp.facedetection.adapter.CustomDialogAdapterForCategoryCrimeList2;
import com.kp.facedetection.adapter.CustomDialogAdapterForModusOperandi;
import com.kp.facedetection.model.AllContentList;
import com.kp.facedetection.model.ClassListCriminal;
import com.kp.facedetection.model.CrimeCategoryList;
import com.kp.facedetection.model.CrimeCategoryList2;
import com.kp.facedetection.model.CriminalDetails;
import com.kp.facedetection.model.CriminalSearchData;
import com.kp.facedetection.model.DivisionList;
import com.kp.facedetection.model.ModusOperandiList;
import com.kp.facedetection.model.PoliceStationList;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class CriminalSearchFragment extends Fragment implements OnClickListener, SearchView.OnQueryTextListener {

	private static HashMap<String, List<String>> map;
	private static HashMap<String, List<String>> mapSubClass;

	View rootLayout;
	EditText et_name, et_alias, et_imei, et_associates, et_address_value, et_briefFact_value, et_Fname, et_age, et_year;
	Button btn_search;
	private Button btn_reset;

	ArrayAdapter<CharSequence> ageAdapter;
	private EditText tv_ps_value;
	private EditText tv_crime_value;
	private EditText tv_subCategory_value;
	private EditText tv_idmark_value;
	private EditText tv_modus_operandi_value;
	private EditText tv_class_value, tv_subClass_value, tv_date_start_value, tv_date_end_value;
	private RelativeLayout advanceShow;
	private ImageView iv_advance;

	private LinearLayout linear_identity_sub_category;
	private LinearLayout linear_subClass;

	public AllContentList allContentList;
	public JSONArray policeStationListArray;
	public JSONArray crimeCategoryListArray;
	public JSONArray divisionListArray;
	public JSONArray modusListArray;
	public JSONArray ioListArray;
	public JSONArray firStatusListArray;
	public JSONArray classListArray;

	protected String[] array_policeStation ;
	protected ArrayList<CharSequence> selectedPoliceStations = new ArrayList<CharSequence>();
	protected ArrayList<CharSequence> selectedPoliceStationsId = new ArrayList<CharSequence>();

	protected String[] array_crimeCategory ;
	protected ArrayList<CharSequence> selectedCrimeCategory = new ArrayList<CharSequence>();

	protected String[] array_identifyCategory ;
	protected ArrayList<CharSequence> selectedIdentifyCategory = new ArrayList<CharSequence>();

	protected String[] array_identifySubCategory ;
	protected ArrayList<CharSequence> selectedIdentifySubCategory = new ArrayList<CharSequence>();
	protected ArrayList<String> customSelectedIdentifySubCategory = new ArrayList<String>();

	protected String[] array_modusOperandi ;
	protected ArrayList<CharSequence> selectedModusOperandi = new ArrayList<CharSequence>();

	protected String[] array_class;
	protected ArrayList<CharSequence> selectedClassList = new ArrayList<CharSequence>();

	protected String[] array_subClass;
	protected ArrayList<CharSequence> selectedSubClass = new ArrayList<CharSequence>();
	protected ArrayList<String> customSelectedSubClass = new ArrayList<String>();

	List<String> policeStationArrayList = new ArrayList<String>();
	public static List<String> identityCategoryList = new ArrayList<String>();
	private List<String> identitySubCategoryList = new ArrayList<String>();
	private List<String> customIdentitySubCategoryList = new ArrayList<String>();
	private List<String> modusOperandiArrayList = new ArrayList<String>();
	public static List<String> classArrayList = new ArrayList<String>();
	private List<String> subClassList = new ArrayList<String>();
	private List<String> customSubClassList = new ArrayList<String>();

	//ArrayList<ModusOperandiList> obj_modusOperandiList;
	ArrayList<PoliceStationList> obj_policeStationList;
	ArrayList<ClassListCriminal> obj_classList;

	private String[] inputMarkArray;
	private String[] inputClassArray;

	private String identity_category_name="";
	private String policeStationString="";
	private String crimeCategoryString="";
	private String identifyMarkCategoryString="";
	private String identifySubCategoryString="";
	private String identifyCategoryString="";
	private String identifyCategorySubCategoryString="";
	private String modusOperandiString="";
    private String classString = "";
	private String newClassString = "";
	private String subClassString = "";
	private String modifiedClassSubClassString = "";

	private String subClassStringShow = "";

	private String customidentifySubCategoryString="";

	CustomDialogAdapterForModusOperandi customDialogAdapterForModusOperandi;
	CustomDialogAdapterForCategoryCrimeList customDialogAdapterForCategoryCrimeList;
	CustomDialogAdapterForCategoryCrimeList2 customDialogAdapterForCategoryCrimeList2;

	ArrayList<CrimeCategoryList> obj_categoryCrimeList;
	//ArrayList<CrimeCategoryList2> obj_categoryCrimeList2;

	private boolean crimeCategory_status=false;
	private boolean modusOperandi_status=false;

	private ListView lv_dialog;
	private List<String> modified_crimeCategoryList = new ArrayList<String>();


	public static ArrayList<CrimeCategoryList2> obj_categoryCrimeList2;
	public static ArrayList<ModusOperandiList> obj_modusOperandiList;

	private List<String> yearList = new ArrayList<String>();
	private String[] yearArray;
	ArrayAdapter<String> yearAdapter;
	private String caseYear="";

	private String[] ageArray= {"5-10", "11-20", "21-30", "31-40", "41-50","51-60","61-70","71-80","81-90","91-100"};
	private ArrayList<String> ageArrayList;
	protected String[] array_age;

	private boolean advanceViewVisible = false;
	String userId ="";
	ImageView toggleButtonSearchType;
	String ownjurisdictionFlag="1";
	boolean search_own_global_flag=false;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		rootLayout = inflater.inflate(R.layout.detail_search_layout_modified, null);
		initView();
		return rootLayout;
	}

	private void initView() {

		fetchAllContent();

		userId= Utility.getUserInfo(getContext()).getUserId();
		toggleButtonSearchType=(ImageView) rootLayout.findViewById(R.id.Tb_search_type);
		toggleButtonSearchType.setOnClickListener(this);
		iv_advance = (ImageView) rootLayout.findViewById(R.id.iv_advance);
		advanceShow = (RelativeLayout) rootLayout.findViewById(R.id.advanceShow);
		advanceShow.setVisibility(View.GONE);

		et_alias = (EditText) rootLayout.findViewById(R.id.et_alias);
		et_associates = (EditText) rootLayout.findViewById(R.id.et_associates);
		//et_imei = (EditText) rootLayout.findViewById(R.id.et_imei);
		et_name = (EditText) rootLayout.findViewById(R.id.et_name);
		et_name.requestFocus();
		et_address_value= (EditText) rootLayout.findViewById(R.id.et_address_value);
		et_briefFact_value= (EditText) rootLayout.findViewById(R.id.et_briefFact_value);
		et_Fname = (EditText) rootLayout.findViewById(R.id.et_Fname);
		et_age = (EditText) rootLayout.findViewById(R.id.et_age);
		et_year = (EditText) rootLayout.findViewById(R.id.et_year);

		tv_idmark_value = (EditText) rootLayout.findViewById(R.id.tv_idmark_value);
		tv_subCategory_value = (EditText) rootLayout.findViewById(R.id.tv_subCategory_value);

		tv_ps_value = (EditText) rootLayout.findViewById(R.id.tv_ps_value);
		tv_crime_value = (EditText) rootLayout.findViewById(R.id.tv_crime_value);
		tv_modus_operandi_value = (EditText) rootLayout.findViewById(R.id.tv_modus_operandi_value);

		tv_class_value = (EditText) rootLayout.findViewById(R.id.tv_class_value);
		tv_subClass_value = (EditText) rootLayout.findViewById(R.id.tv_subClass_value);

		tv_date_start_value = (EditText) rootLayout.findViewById(R.id.tv_date_start_value);
		tv_date_end_value = (EditText) rootLayout.findViewById(R.id.tv_date_end_value);

		linear_identity_sub_category = (LinearLayout)rootLayout.findViewById(R.id.linear_identity_sub_category);
		linear_subClass = (LinearLayout)rootLayout.findViewById(R.id.linear_subClass);

		btn_search = (Button) rootLayout.findViewById(R.id.btn_search);
		btn_reset = (Button) rootLayout.findViewById(R.id.btn_reset);

		btn_search.setOnClickListener(this);
		btn_reset.setOnClickListener(this);

		Constants.buttonEffect(btn_search);
		Constants.buttonEffect(btn_reset);

		et_associates.setFocusable(false);
		et_associates.setFocusableInTouchMode(false); // user touches widget on phone with touch screen
		et_associates.setClickable(false); // user navigates with wheel and selects widget

		backgroundSetForBelowApi();

		Calendar calendar = Calendar.getInstance();
		int current_year = calendar.get(Calendar.YEAR);

        /* Case year Spinner set */
		yearList.clear();

		for(int i=current_year;i>=1980;i--){
			yearList.add(Integer.toString(i));
		}
		yearArray = yearList.toArray(new String[yearList.size()]);

		ageArrayList = new ArrayList<String>(Arrays.asList(ageArray));

		clickEvents();
	}


	private void backgroundSetForBelowApi(){
		if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.LOLLIPOP){
			// Do something for lollipop and above versions
			et_name.setBackgroundResource(R.drawable.text_underline_selector);
			et_alias.setBackgroundResource(R.drawable.text_underline_selector);
			et_address_value.setBackgroundResource(R.drawable.text_underline_selector);
			et_Fname.setBackgroundResource(R.drawable.text_underline_selector);
			et_age.setBackgroundResource(R.drawable.text_underline_selector);
			et_briefFact_value.setBackgroundResource(R.drawable.text_underline_selector);
			tv_crime_value.setBackgroundResource(R.drawable.text_underline_selector);
			tv_ps_value.setBackgroundResource(R.drawable.text_underline_selector);
			tv_idmark_value.setBackgroundResource(R.drawable.text_underline_selector);
			tv_subCategory_value.setBackgroundResource(R.drawable.text_underline_selector);
			tv_modus_operandi_value.setBackgroundResource(R.drawable.text_underline_selector);
			tv_class_value.setBackgroundResource(R.drawable.text_underline_selector);
			tv_subClass_value.setBackgroundResource(R.drawable.text_underline_selector);
			et_year.setBackgroundResource(R.drawable.text_underline_selector);
			tv_date_start_value.setBackgroundResource(R.drawable.text_underline_selector);
			tv_date_end_value.setBackgroundResource(R.drawable.text_underline_selector);
		}else{
			Log.e("Moitri:","API above LOLLIPOP");
		}
	}

	public void parseSearchResult(String jsonStr, String[] keys, String[]  values) {
		try {
			if (jsonStr != null && !jsonStr.equals("")) {

				JSONObject jObj = new JSONObject(jsonStr);
				if (jObj.opt("status").toString().equalsIgnoreCase("1")) {
					String pageno=jObj.opt("pageno").toString();
					String totalResult=jObj.opt("totalresult").toString();


					JSONArray jsonArray = jObj.optJSONArray("result");
					ArrayList<CriminalDetails> criminalList = Utility
							.parseCrimiinalRecords(getActivity(), jsonArray);
					if (criminalList != null && criminalList.size() > 0) {
						Intent intent = new Intent(getActivity(),
								SearchResultActivity.class);
						intent.putExtra(Constants.OBJECT, criminalList);
						intent.putExtra("keys",keys);
						intent.putExtra("value",values);
						intent.putExtra("pageno",pageno);
						intent.putExtra("totalResult", totalResult);
						intent.putExtra("searchCase", "criminalSearch");
						intent.putExtra("crimeCategoryList", obj_categoryCrimeList);
						intent.putExtra("modusOperandiList",obj_modusOperandiList);
						intent.putExtra("policeStation",obj_policeStationList);
						intent.putExtra("modifiedcrimeCategoryList",obj_categoryCrimeList2);
                        //intent.putExtra("classList",classString);
						//intent.putExtra("subClassList",subClassString);

//						System.out.println("size police:"+obj_policeStationList.size());

						CriminalSearchData criminalSearchData = new CriminalSearchData();

						criminalSearchData.setName(et_name.getText().toString().trim());
						//criminalSearchData.setAge(sp_age.getSelectedItem().toString().trim().replace("Select Age Range", ""));
						criminalSearchData.setAge(et_age.getText().toString().trim()/*.replace("Select Age Range", "")*/);
						criminalSearchData.setPs_code(policeStationString);
						criminalSearchData.setPs_name(tv_ps_value.getText().toString().trim()/*.replace("Select Police Stations", "")*/);
						criminalSearchData.setAlias_name(et_alias.getText().toString().trim());
						criminalSearchData.setAddress(et_address_value.getText().toString().trim());
						criminalSearchData.setBriefFact(et_briefFact_value.getText().toString().trim());
						criminalSearchData.setMark_categoryValue(tv_idmark_value.getText().toString().trim()/*.replace("Select a Mark Category","")*/);
						criminalSearchData.setMark_subCategoryValue(tv_subCategory_value.getText().toString().trim()/*.replace("Select a Sub Category","")*/);
						criminalSearchData.setModified_categoryString(identifyCategorySubCategoryString);
						criminalSearchData.setCategoryOfCrimeValue(tv_crime_value.getText().toString().trim()/*.replace("Select a Category","")*/);
						criminalSearchData.setCategoryOfCrimeString(crimeCategoryString);
						criminalSearchData.setModusOperandiValue(tv_modus_operandi_value.getText().toString().trim()/*.replace("Select an Operandi","")*/);
						criminalSearchData.setModusOperandiString(modusOperandiString);

						criminalSearchData.setClassStr(classString);
						criminalSearchData.setClassValue(tv_class_value.getText().toString().trim()/*.replace("Select Class", "")*/);

						criminalSearchData.setSubClassStr(subClassString);
						criminalSearchData.setSubClassValue(tv_subClass_value.getText().toString().trim()/*.replace("Select Sub-Class", "")*/);

						criminalSearchData.setfName(et_Fname.getText().toString().trim());
						//criminalSearchData.setCaseYr(sp_year_value.getSelectedItem().toString().trim().replace("Enter Year",""));
						criminalSearchData.setCaseYr(et_year.getText().toString().trim());
						criminalSearchData.setFromDt(tv_date_start_value.getText().toString().trim());
						criminalSearchData.setToDt(tv_date_end_value.getText().toString().trim());

						intent.putExtra("prev_data",criminalSearchData);

						getActivity().startActivity(intent);
					}else{
						showAlertDialog(getActivity(), Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL , false);
					}

				}
				if(!jObj.optString("message").toString().equalsIgnoreCase("Success")) {
					/*Toast.makeText(getActivity(), jObj.optString("message"),
							Toast.LENGTH_LONG).show();*/
					String message=jObj.optString("message");
					showAlertDialog(getActivity(),Constants.SEARCH_ERROR_TITLE,"Sorry, "+message, false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
		}catch (OutOfMemoryError outOfMemoryError){
			outOfMemoryError.printStackTrace();
			showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
		}

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		    case R.id.Tb_search_type:
				if(!search_own_global_flag){
					toggleButtonSearchType.setImageResource(R.drawable.toggle_right);
					ownjurisdictionFlag="0";
					search_own_global_flag=true;
				}
				else{
					toggleButtonSearchType.setImageResource(R.drawable.toggle_left);
					ownjurisdictionFlag="1";
					search_own_global_flag=false;
				}
				break;
			case R.id.btn_search:

				String startDate = tv_date_start_value.getText().toString().trim();
				String endDate = tv_date_end_value.getText().toString().trim();


				if(!identifyCategoryString.equalsIgnoreCase("") && identifyCategorySubCategoryString.equalsIgnoreCase("")){

					//Toast.makeText(getActivity(),"Please select an identity sub category",Toast.LENGTH_LONG).show();
					showAlertDialog(getActivity(), "Alert !!!", "Please select an identity sub category", false);

				}
				else  if (!endDate.equalsIgnoreCase("") && startDate.equalsIgnoreCase("")) {

					fetchSearchResult();
				} else if (endDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("")) {
					fetchSearchResult();
				} else if (!endDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("")) {
					boolean date_result_flag = DateUtils.dateComparison(startDate, endDate);
					System.out.println("DateComparison: " + date_result_flag);
					if (!date_result_flag) {
						showAlertDialog(getActivity(), "Alert !!!", "Arrest From date can't be greater than Arrest To date", false);
					} else {
						fetchSearchResult();
					}
				}
				else {
					fetchSearchResult();
				}

				break;

			case R.id.btn_reset:

				resetDataFieldValues();
				advanceShow.setVisibility(View.GONE);
				iv_advance.setImageResource(R.drawable.big_dropdown);
				et_name.requestFocus();
				break;

			default:
				break;
		}
	}

	private void fetchSearchResult() {

		// String[]
		// keys={"criminal_name","age","present_position","criminal_aliases_name","imei_no","distinguishes_id_mark","comments","mother_tongue","particulars_of_associates"};
		String age = et_age.getText().toString().trim();
		/*String location = sp_location.getSelectedItem().toString();*/
		String location = tv_ps_value.getText().toString().trim();
		String modus = tv_crime_value.getText().toString().trim();
		String name = et_name.getText().toString().trim();
		String alias = et_alias.getText().toString().trim();
		String address = et_address_value.getText().toString().trim();
		String briefFact = et_briefFact_value.getText().toString().trim();
		/*String imeiNo = et_imei.getText().toString();
		String idMark = *//*et_idmark.getText().toString();*//*"";
		String associate = et_associates.getText().toString();*/
		String fName = et_Fname.getText().toString().trim();
		String caseyr = et_year.getText().toString().trim();
		String fromDt = tv_date_start_value.getText().toString().trim();
		String toDt = tv_date_end_value.getText().toString().trim();


		if (age.toLowerCase().startsWith("select")) {
			age = "";

		}

		if (location.toLowerCase().startsWith("select")) {
			location = "";

		}
		if (modus.toLowerCase().startsWith("select")) {
			modus = "";

		}

		//name_accused,age_accused,address_accused,criminal_aliases_name,accused_for

		if(!name.equalsIgnoreCase("") || !age.equalsIgnoreCase("") || !policeStationString.equalsIgnoreCase("") || !alias.equalsIgnoreCase("")
				|| !crimeCategoryString.equalsIgnoreCase("") || !identifyCategoryString.equalsIgnoreCase("") || !modusOperandiString.equalsIgnoreCase("")
				|| !address.equalsIgnoreCase("") || !briefFact.equalsIgnoreCase("") || !classString.equalsIgnoreCase("") || !subClassStringShow.equalsIgnoreCase("")
				|| !fName.equalsIgnoreCase("") || !fromDt.equalsIgnoreCase("") || !toDt.equalsIgnoreCase("") || !caseyr.equalsIgnoreCase("")){

			if (!Constants.internetOnline(getActivity())) {
				Utility.showAlertDialog(getActivity(), Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
				return;
			}

			TaskManager taskManager = new TaskManager(this);
			taskManager.setMethod(Constants.METHOD_CRIMINAL_SEARCH);
			taskManager.setDetailedSearch(true);
			/*String[] keys = { "name_accused", "age_accused", "policestations",//policestation
					"criminal_aliases_name","crimecategory","identitycategory","identitysubcategory","pageno"*//*"accused_for",*//**//* "imei_no", "distinguishes_id_mark",
				"mother_tongue", "particulars_of_associates"*//* };*/

			String[] keys = { "name_accused", "father_accused", "from_date", "to_date", "case_yr", "policestations",
					"criminal_aliases_name", "pageno", "identitycategory", "crimecategory","moduslist","age_accused", "address", "brief_keyword", "class", "subclass" ,"user_id","own_jurisdiction"};

			/*String[] values = { name, age, policeStationString, alias,crimeCategoryString,identifyMarkCategoryString,identifySubCategoryString,"1" *//*,modus*//**//*imeiNo, idMark,
				language, associate*//* };*/

			String[] values = { name.trim(), fName.trim(), fromDt.trim(), toDt.trim(), caseyr.trim(), policeStationString.trim(), alias.trim(),"1" ,identifyCategorySubCategoryString.trim(),crimeCategoryString.trim(),modusOperandiString.trim(),age.trim(), address.trim(), briefFact.trim(), classString.trim(), subClassString.trim(), userId ,ownjurisdictionFlag};

			taskManager.doStartTask(keys, values, true);

		}
		else {
			showAlertDialog(getActivity(), "Alert !!!", "Please provide atleast one value for search", false);
		}


	}


	private void fetchAllContent(){

		if (!Constants.internetOnline(getActivity())) {
			Utility.showAlertDialog(getActivity(), Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
			return;
		}

		TaskManager taskManager = new TaskManager(this);
		taskManager.setMethod(Constants.METHOD_GETCONTENT);
		taskManager.setAllContent(true);

		String[] keys = {  };
		String[] values = { };
		taskManager.doStartTask(keys, values, true);
	}

	public void parseGetAllContentResponse(String response){

		//System.out.println("Get All Content Result: " + response);

		fetchIdentityCategory();

		if (response != null && !response.equals("")) {

			try {
				Constants.divisionArrayList.clear();
				Constants.crimeCategoryArrayList.clear();
				Constants.policeStationIDArrayList.clear();
				Constants.policeStationNameArrayList.clear();
				//Constants.IOCodeArrayList.clear();
				//Constants.IONameArrayList.clear();
				Constants.firStatusArrayList.clear();

			//	Constants.firStatusArrayList.add("Select a status");

				JSONObject jObj = new JSONObject(response);
				allContentList = new AllContentList();
				if(jObj != null && jObj.optString("status").equalsIgnoreCase("1")){

					allContentList.setStatus(jObj.optString("status"));
					allContentList.setMessage(jObj.optString("message"));

					policeStationListArray = jObj.getJSONObject("result").getJSONArray("policestationlist");
					crimeCategoryListArray = jObj.getJSONObject("result").getJSONArray("crimecategorylist");
					divisionListArray = jObj.getJSONObject("result").getJSONArray("divisionlist");
					modusListArray = jObj.getJSONObject("result").getJSONArray("modlist");
				//	ioListArray = jObj.getJSONObject("result").getJSONArray("iolist");
					firStatusListArray = jObj.getJSONObject("result").getJSONArray("firstatuslist");
					classListArray = jObj.getJSONObject("result").getJSONArray("classList");

					for(int i=0;i<policeStationListArray.length();i++){

						PoliceStationList policeStationList = new PoliceStationList();

						JSONObject row = policeStationListArray.getJSONObject(i);
						policeStationList.setPs_code(row.optString("PSCODE"));
						policeStationList.setPs_name(row.optString("PSNAME"));
						policeStationList.setDiv_code(row.optString("DIVCODE"));
						policeStationList.setOc_name(row.optString("OCNAME"));

						policeStationArrayList.add(row.optString("PSNAME"));

						Constants.policeStationNameArrayList.add(row.optString("PSNAME"));
						Constants.policeStationIDArrayList.add(row.optString("PSCODE"));

						allContentList.setObj_policeStationList(policeStationList);

					}

					for(int j=0;j<crimeCategoryListArray.length();j++){

						CrimeCategoryList crimeCategoryList = new CrimeCategoryList();

						JSONObject row = crimeCategoryListArray.getJSONObject(j);
						crimeCategoryList.setCategory(row.optString("CATEGORY"));
						crimeCategoryList.setCategory_desc(row.optString("CATEGORYDESC"));
						crimeCategoryList.setBroad_category(row.optString("BROAD_CATEGORY"));
						crimeCategoryList.setIPC_SSl(row.optString("IPC_SLL"));
						crimeCategoryList.setSections(row.optString("SECTIONS"));
						crimeCategoryList.setVerified(row.optString("VERIFIED"));
						crimeCategoryList.setFavourite(row.optString("FAVOURITE"));

						Constants.crimeCategoryArrayList.add(row.optString("CATEGORY"));


						allContentList.setObj_categoryCrimeList(crimeCategoryList);

					}

					createModifiedCrimeCategoryList(allContentList.getObj_categoryCrimeList());

					for(int k=0;k<divisionListArray.length();k++){

						DivisionList divisionList = new DivisionList();
						JSONObject row = divisionListArray.getJSONObject(k);
						divisionList.setDiv_name(row.optString("DIVNAME"));
						divisionList.setDiv_code(row.optString("DIVCODE"));

						Constants.divisionArrayList.add(row.optString("DIVNAME"));
						Constants.divCodeArrayList.add(row.optString("DIVCODE"));

						allContentList.setObj_DivisionList(divisionList);

					}

					for(int l=0;l<modusListArray.length();l++){

						ModusOperandiList modusOperandiList = new ModusOperandiList();
						JSONObject row = modusListArray.getJSONObject(l);
						modusOperandiList.setMod_operand(row.optString("MOD_OPER"));
						modusOperandiList.setSelected(false);

						modusOperandiArrayList.add(row.optString("MOD_OPER"));


						allContentList.setObj_modusOperandiList(modusOperandiList);

					}

					/*for(int m=0;m<ioListArray.length();m++){

						IOList ioList = new IOList();
						JSONObject row = ioListArray.getJSONObject(m);

						ioList.setIo_code(row.optString("IOCODE"));
						ioList.setIo_name(row.optString("IONAME"));
						ioList.setActive_flag(row.optString("ACTIVE_FLAG"));
						ioList.setPs(row.optString("PS"));
						ioList.setGf_no(row.optString("GFNO"));

						Constants.IOCodeArrayList.add(row.optString("IOCODE"));
						Constants.IONameArrayList.add(row.optString("IONAME"));

					}*/

					for(int n=0;n<firStatusListArray.length(); n++){

						JSONObject row = firStatusListArray.getJSONObject(n);
						Constants.firStatusArrayList.add(row.optString("FIR_STATUS"));

					}

					for(int m=0; m<classListArray.length(); m++){

						ClassListCriminal classList = new ClassListCriminal();
						JSONObject row = classListArray.getJSONObject(m);
						classList.setClassName(row.optString("CLASS"));
						classArrayList.add(row.optString("CLASS"));

						allContentList.setObj_classList(classList);
					}


					obj_categoryCrimeList=allContentList.getObj_categoryCrimeList();
					obj_modusOperandiList=allContentList.getObj_modusOperandiList();
					obj_policeStationList=allContentList.getObj_policeStationList();
					obj_classList = allContentList.getObj_classList();
				}
				else {
					showAlertDialog(getActivity(), Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
				}


			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		array_policeStation = new String[policeStationArrayList.size()];
		array_policeStation = policeStationArrayList.toArray(array_policeStation);

		array_crimeCategory = new String[Constants.crimeCategoryArrayList.size()];
		array_crimeCategory = Constants.crimeCategoryArrayList.toArray(array_crimeCategory);

		array_modusOperandi = new String[modusOperandiArrayList.size()];
		array_modusOperandi = modusOperandiArrayList.toArray(array_modusOperandi);

		array_class = new String[classArrayList.size()];
		array_class = classArrayList.toArray(array_class);

		array_age = new String[ageArrayList.size()];
		array_age = ageArrayList.toArray(array_age);
	}

	@SuppressLint("ClickableViewAccessibility")
	private void clickEvents(){

		iv_advance.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

                if(advanceViewVisible == true){
                    advanceViewVisible = false;
                }else{
                    advanceViewVisible = true;
                }


				if(advanceViewVisible){
					advanceShow.setVisibility(View.VISIBLE);
					iv_advance.setImageResource(R.drawable.big_up_arrow);
					et_briefFact_value.requestFocus();

				}
				else{
                    advanceShow.setVisibility(View.GONE);
					iv_advance.setImageResource(R.drawable.big_dropdown);
				}
			}
		});

		et_year.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP){
					showCaseYearDialog();
					Log.e("Moitri::","call setOnTouchListener" );
				}

				return false;
			}
		});

		et_age.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP){
					showAgedialog();
					Log.e("Moitri::","call setOnTouchListener" );
				}
				return false;
			}
		});


		tv_ps_value.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP){
					if(policeStationArrayList.size()!=0) {
						showSelectPoliceStationsDialog();
					}
					else {
						showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
					}
				}
				return false;
			}
		});

		tv_crime_value.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP){
					if(Constants.crimeCategoryArrayList.size()!=0){
						//	showSelectCrimeCategoryDialog();
						//tv_crime_value.setText("Select a Category");
						tv_crime_value.setText("");
						crimeCategoryString="";
						crimeCategory_status = true;

						//customDialogAdapterForCategoryCrimeList = new CustomDialogAdapterForCategoryCrimeList(getActivity(),obj_categoryCrimeList);
						customDialogAdapterForCategoryCrimeList2 = new CustomDialogAdapterForCategoryCrimeList2(getActivity(),obj_categoryCrimeList2);
						customDialogForCategoryCrimeList();
					}
					else {
						showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
					}
				}
				return false;
			}
		});

		tv_idmark_value.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					if (identityCategoryList.size() != 0) {
						showSelectIdentifyCategoryDialog();
					} else {
						showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
					}
				}
				return false;
			}
		});

		tv_modus_operandi_value.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP){
					if(modusOperandiArrayList.size()!=0){
						//showModusOperandiDialog();
						//tv_modus_operandi_value.setText("Select an Operandi");
						tv_modus_operandi_value.setText("");
						modusOperandiString="";
						modusOperandi_status = true;
						obj_modusOperandiList = allContentList.getObj_modusOperandiList();
						customDialogAdapterForModusOperandi = new CustomDialogAdapterForModusOperandi(getActivity(),obj_modusOperandiList);
						customDialogForModusOperandi();

					}
					else {
						showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
					}
				}
				return false;
			}
		});

		tv_subCategory_value.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP){
					linear_identity_sub_category.setVisibility(View.VISIBLE);
					tv_subCategory_value.setVisibility(View.VISIBLE);
					//showSelectIdentifySubCategoryDialog();
					//tv_subCategory_value.setText("Select a Sub Category");
					tv_subCategory_value.setText("");
					System.out.println("Identify Category String");
					fetchIdentitySubCategory(identifyCategoryString);
				}
				return false;
			}
		});

		tv_class_value.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP){
					if(classArrayList.size() != 0){
						showSelectClassDialog();
					}
					else{
						showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
					}
				}
				return false;
			}
		});


		tv_subClass_value.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP){
					linear_subClass.setVisibility(View.VISIBLE);
					tv_subClass_value.setVisibility(View.VISIBLE);
					//tv_subClass_value.setText("Select Sub-Class");
					tv_subClass_value.setText("");
					// fetch data
					//fetchSubClass(classString);
					fetchSubClass(newClassString);
				}
				return false;
			}
		});

		tv_date_start_value.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP){
					DateUtils.setDate(getActivity(), tv_date_start_value, true);
				}
				return false;
			}
		});

		tv_date_end_value.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP){
					DateUtils.setDate(getActivity(), tv_date_end_value, true);
				}
				return false;
			}
		});

	}

	 /* This method shows Police Station list in a pop-up */

	protected void showSelectPoliceStationsDialog() {
		boolean[] checkedPoliceStations = new boolean[array_policeStation.length];
		int count = array_policeStation.length;

		for(int i = 0; i < count; i++) {
			checkedPoliceStations[i] = selectedPoliceStations.contains(array_policeStation[i]);
		}

		DialogInterface.OnMultiChoiceClickListener policeStationsDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				if (isChecked) {
					selectedPoliceStations.add(array_policeStation[which]);
					selectedPoliceStationsId.add((CharSequence) allContentList.getObj_policeStationList().get(which).getPs_code());
				} else{
					selectedPoliceStations.remove(array_policeStation[which]);
					selectedPoliceStationsId.remove((CharSequence) allContentList.getObj_policeStationList().get(which).getPs_code());
				}

//				onChangeSelectedPoliceStations();
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Police Stations");
		builder.setMultiChoiceItems(array_policeStation, checkedPoliceStations, policeStationsDialogListener);

		// Set the positive/yes button click listener
		builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Do something when click positive button
				onChangeSelectedPoliceStations();
			}
		});

		// Set the negative/no button click listener
		builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Do something when click the negative button
				//tv_ps_value.setText("Select Police Stations");
				tv_ps_value.setText("");
				policeStationString="";
				selectedPoliceStations.clear();
				selectedPoliceStationsId.clear();
			}
		});


		AlertDialog dialog = builder.create();
		dialog.show();
	}

	protected void onChangeSelectedPoliceStations() {
		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder stringBuilderId = new StringBuilder();

		for(CharSequence policeStation : selectedPoliceStations){
			stringBuilder.append(policeStation + ",");
		}

		for(CharSequence policeStation : selectedPoliceStationsId){
			stringBuilderId.append("\'"+policeStation + "\',");
		}

		if(selectedPoliceStations.size()==0) {
			tv_ps_value.setText("Select Police Stations");
			policeStationString="";
		}
		else {
			tv_ps_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
			policeStationString=stringBuilderId.toString().substring(0,stringBuilderId.toString().length()-1);
		}

	}

	 /* This method shows Crime Category list in a pop-up */

	/*protected void showSelectCrimeCategoryDialog() {
		boolean[] checkedCrimeCategory = new boolean[array_crimeCategory.length];
		int count = array_crimeCategory.length;

		for(int i = 0; i < count; i++)
			checkedCrimeCategory[i] = selectedCrimeCategory.contains(array_crimeCategory[i]);

		DialogInterface.OnMultiChoiceClickListener crimeCategoryDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				if(isChecked)
					selectedCrimeCategory.add(array_crimeCategory[which]);
				else
					selectedCrimeCategory.remove(array_crimeCategory[which]);

				onChangeSelectedCrimeCategory();
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Category of Crime");
		builder.setMultiChoiceItems(array_crimeCategory, checkedCrimeCategory, crimeCategoryDialogListener);

		AlertDialog dialog = builder.create();
		dialog.show();
	}

	protected void onChangeSelectedCrimeCategory() {
		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder stringBuilderCrimeCategory = new StringBuilder();

		for(CharSequence crimeCategory : selectedCrimeCategory) {
			stringBuilder.append(crimeCategory + ",");

			String crimeCategoryModifiedString = stringBuilderCrimeCategory.append("\'"+crimeCategory.toString().toLowerCase() + "\',").toString();

			crimeCategoryString = crimeCategoryModifiedString.substring(0,crimeCategoryModifiedString.length()-1);
		}

		if(selectedCrimeCategory.size()==0) {
			tv_crime_value.setText("Select a Category");
		}
		else {
			tv_crime_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
		}

	}*/

	private void fetchIdentityCategory(){

		if (!Constants.internetOnline(getActivity())) {
			Utility.showAlertDialog(getActivity(), Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
			return;
		}

		TaskManager taskManager = new TaskManager(this);
		taskManager.setMethod(Constants.METHOD_IDENTITY_CATEGORY);
		taskManager.setIdentityCategory(true);

		String[] keys = {  };
		String[] values = { };
		taskManager.doStartTask(keys, values, true);
	}

	public void parseIdentityCategoryResponse(String response) {

		//System.out.println("Identity Category Response: " + response);
		identityCategoryList.clear();

		if (response != null && !response.equals("")) {

			try {
				JSONObject jObj = new JSONObject(response);
				JSONArray identity_resultArray = jObj.getJSONArray("result");

				for(int i=0;i<identity_resultArray.length();i++) {

					identityCategoryList.add(identity_resultArray.optString(i));

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		System.out.println("Identity Category List: " + identityCategoryList);

		array_identifyCategory = new String[identityCategoryList.size()];
		array_identifyCategory = identityCategoryList.toArray(array_identifyCategory);

	}

	private void fetchIdentitySubCategory(String category){

		if (!Constants.internetOnline(getActivity())) {
			Utility.showAlertDialog(getActivity(), Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
			return;
		}

		TaskManager taskManager = new TaskManager(this);
		taskManager.setMethod(Constants.METHOD_IDENTITY_SUB_CATEGORY);
		taskManager.setIdentitySubCategory(true);

		String[] keys = {"categoryname"};
		String[] values = {category.trim()};
		taskManager.doStartTask(keys, values, true);

		inputMarkArray = category.split(",");
		System.out.println("Input_Mark_Array: " + inputMarkArray);

	}

	public void parseIdentitySubCategoryResponse(String response) {

		//System.out.println("Identity Sub Category Response: " + response);

		identitySubCategoryList.clear();
		customIdentitySubCategoryList.clear();

		linear_identity_sub_category.setVisibility(View.VISIBLE);
		tv_subCategory_value.setVisibility(View.VISIBLE);

		if (response != null && !response.equals("")) {



			JSONObject jObj = null;
			try {

				jObj = new JSONObject(response);
				if(jObj != null && jObj.optString("status").equalsIgnoreCase("1")){

					JSONArray identity_sub_category_resultArray = jObj.getJSONArray("result");


					for(int i=0;i<identity_sub_category_resultArray.length();i++) {

						JSONObject row = identity_sub_category_resultArray.getJSONObject(i);
						System.out.println("Input: "+inputMarkArray[i]);
						JSONArray jsonArray = row.getJSONArray(inputMarkArray[i]);

						for(int j=0;j<jsonArray.length();j++){
							identitySubCategoryList.add(jsonArray.optString(j));
							customIdentitySubCategoryList.add(inputMarkArray[i]+"_"+jsonArray.optString(j));
						}
					}
				}
				else {
					showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_DETAIL, false);
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		System.out.println("Identity Sub Category List: " + identitySubCategoryList);
		System.out.println("Identity Sub Category List Size: " + identitySubCategoryList.size());

		System.out.println("Identity Custom Sub Category List: " + customIdentitySubCategoryList);
		System.out.println("Identity Custom Sub Category List Size: " + customIdentitySubCategoryList.size());

		//createJSONForCategorySubCategory();

		array_identifySubCategory = new String[identitySubCategoryList.size()];
		array_identifySubCategory = identitySubCategoryList.toArray(array_identifySubCategory);


		showSelectIdentifySubCategoryDialog();


	}

	/* Identify Sub Category Dialog */

	protected void showSelectIdentifySubCategoryDialog() {

		customSelectedIdentifySubCategory.clear();
		selectedIdentifySubCategory.clear();
		identifySubCategoryString="";

		boolean[] checkedIdentifySubCategory = new boolean[array_identifySubCategory.length];
		int count = array_identifySubCategory.length;

		for(int i = 0; i < count; i++) {
			checkedIdentifySubCategory[i] = selectedIdentifySubCategory.contains(array_identifySubCategory[i]);
		}

		DialogInterface.OnMultiChoiceClickListener identifySubCategoryDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				if (isChecked) {
					selectedIdentifySubCategory.add(array_identifySubCategory[which]);
					customSelectedIdentifySubCategory.add(customIdentitySubCategoryList.get(which));
				} else{
					selectedIdentifySubCategory.remove(array_identifySubCategory[which]);
					customSelectedIdentifySubCategory.remove(customIdentitySubCategoryList.get(which));
				}

//				onChangeSelectedIdentifySubCategory();
			}
		};


		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select a Sub Category");
		builder.setMultiChoiceItems(array_identifySubCategory, checkedIdentifySubCategory, identifySubCategoryDialogListener);

		// Set the positive/yes button click listener
		builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Do something when click positive button
				onChangeSelectedIdentifySubCategory();
			}
		});

		// Set the negative/no button click listener
		builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Do something when click the negative button
				selectedIdentifySubCategory.clear();
				customSelectedIdentifySubCategory.clear();
			}
		});


		/*builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				createJSONForCategorySubCategory();
			}
		});*/

		AlertDialog dialog = builder.create();
		dialog.show();

	}

	protected void onChangeSelectedIdentifySubCategory() {

		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder stringBuilderIdentifySubCategory = new StringBuilder();

		for(CharSequence identifySubCategory : selectedIdentifySubCategory) {

			stringBuilder.append(identifySubCategory + ",");
			String identifySubCategoryModifiedString = stringBuilderIdentifySubCategory.append("\'"+identifySubCategory.toString().toLowerCase() + "\',").toString();
			identifySubCategoryString = identifySubCategoryModifiedString.substring(0,identifySubCategoryModifiedString.length()-1);
		}

		if(selectedIdentifySubCategory.size()==0) {
			//tv_subCategory_value.setText("Select a Sub Category");
			tv_subCategory_value.setText("");
		} else {
			System.out.println("Get Select Sub Category List: " + customSelectedIdentifySubCategory);
			tv_subCategory_value.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
			createJSONForCategorySubCategory();

		}

	}


	/* Identify Category Dialog */

	protected void showSelectIdentifyCategoryDialog(){

		boolean[] checkedIdentifyCategory = new boolean[array_identifyCategory.length];
		int count = array_identifyCategory.length;


		for(int i = 0; i < count; i++) {
			checkedIdentifyCategory[i] = selectedIdentifyCategory.contains(array_identifyCategory[i]);
		}

		DialogInterface.OnMultiChoiceClickListener identifyCategoryDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				if (isChecked) {
					selectedIdentifyCategory.add(array_identifyCategory[which]);
				} else{
					selectedIdentifyCategory.remove(array_identifyCategory[which]);
				}

//				onChangeSelectedIdentifyCategory();
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select a Mark Category");
		builder.setMultiChoiceItems(array_identifyCategory, checkedIdentifyCategory, identifyCategoryDialogListener);

		// Set the positive/yes button click listener
		builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Do something when click positive button
				//tv_subCategory_value.setText("Select a Sub Category");
				tv_subCategory_value.setText("");
				onChangeSelectedIdentifyCategory();
			}
		});

		// Set the negative/no button click listener
		builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Do something when click the negative button
				//tv_idmark_value.setText("Select a Mark Category");
				tv_idmark_value.setText("");
				linear_identity_sub_category.setVisibility(View.GONE);
				tv_subCategory_value.setVisibility(View.GONE);
				identifyCategoryString="";
				selectedIdentifyCategory.clear();
			}
		});

		AlertDialog dialog = builder.create();
		dialog.show();


	}

	protected void onChangeSelectedIdentifyCategory() {

		StringBuilder stringBuilder = new StringBuilder();

		for(CharSequence identifyCategory : selectedIdentifyCategory) {

			String identifyCategoryModifiedString = stringBuilder.append(identifyCategory + ",").toString();
			identifyCategoryString = identifyCategoryModifiedString.substring(0, identifyCategoryModifiedString.length() - 1);

		}

		if(selectedIdentifyCategory.size()==0) {
			//tv_idmark_value.setText("Select a Mark Category");
			tv_idmark_value.setText("");
			linear_identity_sub_category.setVisibility(View.GONE);
			tv_subCategory_value.setVisibility(View.GONE);
			identifyCategoryString="";
		}
		else {
			tv_idmark_value.setText(identifyCategoryString);
			linear_identity_sub_category.setVisibility(View.VISIBLE);
			tv_subCategory_value.setVisibility(View.VISIBLE);
		}

	}

	/* This method shows Modus Operandi list in a pop-up */

	/*protected void showModusOperandiDialog(){

		boolean[] checkedModusOperandi = new boolean[array_modusOperandi.length];
		int count = array_modusOperandi.length;

		for(int i = 0; i < count; i++){

			checkedModusOperandi[i] = selectedModusOperandi.contains(array_modusOperandi[i]);

		}

		DialogInterface.OnMultiChoiceClickListener modusOperandiDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				if(isChecked){
					selectedModusOperandi.add(array_modusOperandi[which]);
				}
				else{
					selectedModusOperandi.remove(array_modusOperandi[which]);
				}


				onChangeSelectedModusOperandi();
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Modus Operandi");
		builder.setMultiChoiceItems(array_modusOperandi, checkedModusOperandi, modusOperandiDialogListener);

		AlertDialog dialog = builder.create();
		dialog.show();

	}


	protected void onChangeSelectedModusOperandi() {

		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder stringBuilderModusOperandi = new StringBuilder();

		for(CharSequence modusOperandi : selectedModusOperandi) {
			stringBuilder.append(modusOperandi + ",");

			String modusOperandiModifiedString = stringBuilderModusOperandi.append("\'"+modusOperandi.toString().toLowerCase() + "\',").toString();

			modusOperandiString = modusOperandiModifiedString.substring(0,modusOperandiModifiedString.length()-1);

		}

		if(selectedModusOperandi.size()==0) {
			tv_modus_operandi_value.setText("Select an Operandi");
			modusOperandiString="";
		}
		else {
			tv_modus_operandi_value.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
		}

	}*/



	private void resetDataFieldValues(){

		System.out.println("Reset button click");

        /* for name part */
		et_name.setText("");


		/* for age part */
		/*ageAdapter = ArrayAdapter.createFromResource(getActivity(),
				R.array.Age_array, R.layout.simple_spinner_item);
		ageAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_age.setAdapter(ageAdapter);
		ageAdapter.notifyDataSetChanged();*/
		et_age.setText("");


		/* for police station part */
		//tv_ps_value.setText("Select Police Stations");
		tv_ps_value.setText("");
		policeStationString="";
		selectedPoliceStations.clear();
		selectedPoliceStationsId.clear();


		/* for alias  part */
		et_alias.setText("");

		/* for address  part */
		et_address_value.setText("");

		/* for briefFact  part */
		et_briefFact_value.setText("");

		/* for identify mark category & sub category part */
		//tv_idmark_value.setText("Select a Mark Category");
		//tv_subCategory_value.setText("Select a Sub Category");
		tv_idmark_value.setText("");
		tv_subCategory_value.setText("");
		identifyCategoryString = "";
		identifyCategorySubCategoryString= "";
		selectedIdentifyCategory.clear();
		linear_identity_sub_category.setVisibility(View.GONE);
		tv_subCategory_value.setVisibility(View.GONE);


		/* for Category of crime part */
		//tv_crime_value.setText("Select a Category");
		tv_crime_value.setText("");
		crimeCategoryString="";
		selectedCrimeCategory.clear();

		 /* for modus operandi part */
		modusOperandiString = "";
		selectedModusOperandi.clear();
		//tv_modus_operandi_value.setText("Select an Operandi");
		tv_modus_operandi_value.setText("");


		/* for assosiate part */
		et_associates.setText("");

		/* for class part */
		//tv_class_value.setText("Select Class");
		tv_class_value.setText("");
        classString="";
		newClassString = "";
        selectedClassList.clear();

		/* for sub class part */
		//tv_subClass_value.setText("Select Sub-Class");
		tv_subClass_value.setText("");
		subClassString = "";
		subClassStringShow = "";
        linear_subClass.setVisibility(View.GONE);
		tv_subClass_value.setVisibility(View.GONE);

		et_Fname.setText("");

		//*  for case year part *//*
		caseYear="";
		et_year.setText("");

		//* for Start Date part *//*
		tv_date_start_value.setText("");

		//* for End Date part *//*
		tv_date_end_value.setText("");
	}



	private  void createJSONForCategorySubCategory(){

		map = new HashMap<String, List<String>>();

		List<String>[] aList= new List[inputMarkArray.length];
		JSONObject objCategorySubcategory = new JSONObject();

		for(int j=0;j<inputMarkArray.length;j++){
			System.out.println("IN: "+inputMarkArray[j]);
		}

		System.out.println("Size: "+customSelectedIdentifySubCategory.size());
		Log.e("custom sub Cat",""+customSelectedIdentifySubCategory.size());
		for(int i=0;i<customSelectedIdentifySubCategory.size();i++){
			String a = customSelectedIdentifySubCategory.get(i);
			insert(a.substring(0, a.indexOf("_")), a.substring(a.indexOf("_") + 1));

			for(int j=0;j<inputMarkArray.length;j++){
				aList[j] = map.get(inputMarkArray[j]);
			}

		}
		for(int j=0;j<inputMarkArray.length;j++){

			System.out.println("List "+j+"  :"+aList[j]);

			JSONArray jsonArray = new JSONArray();
			try {
				for (int k = 0; k < aList[j].size(); k++) {
					jsonArray.put(aList[j].get(k));
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}

			try {
				if(jsonArray.length()>0) {
					objCategorySubcategory.put(inputMarkArray[j], jsonArray);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		identifyCategorySubCategoryString="";
		identifyCategorySubCategoryString = objCategorySubcategory.toString();
		Log.e("Create JSON--","---------------");
		Log.e("CategorySubCategory",identifyCategorySubCategoryString);
		System.out.println("Created JSON For Category and SubCategory: " + identifyCategorySubCategoryString);

	}

	public void insert(String key, String value){
		List<String> list = map.get(key);
		if (list == null){
			list = new ArrayList<String>();
			map.put(key, list);
		}
		list.add(value);
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		/*if (TextUtils.isEmpty(query)) {
			lv_dialog.clearTextFilter();
		} else {
			//lv_dialog.setFilterText(query.toString());
			customDialogAdapterForModusOperandi.filter(query.toString());
		}*/
		return false;
	}

	@Override
	public boolean onQueryTextChange(String newText) {

		/*if(crimeCategory_status){
			customDialogAdapterForCategoryCrimeList.filter(newText.toString());
		}
		else if(modusOperandi_status){
			customDialogAdapterForModusOperandi.filter(newText.toString());
		}*/

		if(crimeCategory_status){
			customDialogAdapterForCategoryCrimeList2.filter(newText.toString());
		}
		else if(modusOperandi_status){
			customDialogAdapterForModusOperandi.filter(newText.toString());
		}

		return true;
	}


	//--------------------------------------------------------------------------------------------------------------//


	                                /* Custom Dialog for Modus Operandi */

	//-------------------------------------------------------------------------------------------------------------//


	private void customDialogForModusOperandi(){

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
		builder.setView(dialogView);

		TextView tv_title = (TextView)dialogView.findViewById(R.id.tv_title);
		Button btn_cancel = (Button)dialogView.findViewById(R.id.btn_cancel);
		Button btn_done = (Button)dialogView.findViewById(R.id.btn_done);
		lv_dialog = (ListView)dialogView.findViewById(R.id.lv_dialog);
		SearchView searchView = (SearchView)dialogView.findViewById(R.id.searchView);


		Constants.changefonts(tv_title, getActivity(), "Calibri Bold.ttf");
		tv_title.setText("Select Modus Operandi");

		customDialogAdapterForModusOperandi.notifyDataSetChanged();
		lv_dialog.setAdapter(customDialogAdapterForModusOperandi);
		//lv_dialog.setScrollingCacheEnabled(false);
		lv_dialog.setTextFilterEnabled(true);

		searchView.setIconifiedByDefault(false);
		searchView.setOnQueryTextListener(CriminalSearchFragment.this);
		searchView.setSubmitButtonEnabled(false);
		searchView.setQueryHint("Search Here");


		final AlertDialog alertDialog = builder.show();

		alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {

				modusOperandi_status = false;

			}
		});

		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				modusOperandi_status = false;

				if(alertDialog != null && alertDialog.isShowing()){
					alertDialog.dismiss();

				}

			}
		});

		btn_done.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				System.out.println("No of Selected Items: "+ CustomDialogAdapterForModusOperandi.selectedItemList.size());
				System.out.println("Selected Items: "+ CustomDialogAdapterForModusOperandi.selectedItemList);

				modusOperandi_status = false;

				onSelectedModusOperandi(CustomDialogAdapterForModusOperandi.selectedItemList);

				if(alertDialog != null && alertDialog.isShowing()){
					alertDialog.dismiss();

				}

			}
		});

	}


	private void onSelectedModusOperandi(List<String> modusOperandiList){

		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder stringBuilderModusOperandi = new StringBuilder();

		for(CharSequence modusOperandi : modusOperandiList) {
			stringBuilder.append(modusOperandi + ",");

			String modusOperandiModifiedString = stringBuilderModusOperandi.append("\'"+modusOperandi.toString().toLowerCase() + "\',").toString();

			modusOperandiString = modusOperandiModifiedString.substring(0,modusOperandiModifiedString.length()-1);

		}

		if(modusOperandiList.size()==0) {
			//tv_modus_operandi_value.setText("Select an Operandi");
			tv_modus_operandi_value.setText("");
			modusOperandiString="";
		}
		else {
			System.out.println("Modus Operandi String: "+modusOperandiString);
			tv_modus_operandi_value.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
		}
	}

	//---------------------------------------------------------------------------------------------------------------//


	                                  /* Custom Dialog for Crime Category */

	//---------------------------------------------------------------------------------------------------------------//

	private void customDialogForCategoryCrimeList(){

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
		builder.setView(dialogView);

		TextView tv_title = (TextView)dialogView.findViewById(R.id.tv_title);
		Button btn_cancel = (Button)dialogView.findViewById(R.id.btn_cancel);
		Button btn_done = (Button)dialogView.findViewById(R.id.btn_done);
		lv_dialog = (ListView)dialogView.findViewById(R.id.lv_dialog);
		SearchView searchView = (SearchView) dialogView.findViewById(R.id.searchView);


		Constants.changefonts(tv_title, getActivity(), "Calibri Bold.ttf");
		tv_title.setText("Select Crime Category");

		/*customDialogAdapterForCategoryCrimeList.notifyDataSetChanged();
		lv_dialog.setAdapter(customDialogAdapterForCategoryCrimeList);*/

		customDialogAdapterForCategoryCrimeList2.notifyDataSetChanged();
		lv_dialog.setAdapter(customDialogAdapterForCategoryCrimeList2);
		//lv_dialog.setScrollingCacheEnabled(false);
		lv_dialog.setTextFilterEnabled(true);

		searchView.setIconifiedByDefault(false);
		searchView.setOnQueryTextListener(CriminalSearchFragment.this);
		searchView.setSubmitButtonEnabled(false);
		searchView.setQueryHint("Search Here");



		final AlertDialog alertDialog = builder.show();

		alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {

				crimeCategory_status = false;
			}
		});

		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if(alertDialog != null && alertDialog.isShowing()){
					alertDialog.dismiss();

				}
				crimeCategory_status = false;

			}
		});

		btn_done.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				/*System.out.println("No of Selected Items: "+ CustomDialogAdapterForCategoryCrimeList.selectedItemList.size());
				System.out.println("Selected Items: "+ CustomDialogAdapterForCategoryCrimeList.selectedItemList);
				crimeCategory_status = false;

				onSelectedCategoryOfCrime(CustomDialogAdapterForCategoryCrimeList.selectedItemList);

				if(alertDialog != null && alertDialog.isShowing()){
					alertDialog.dismiss();

				}*/

				System.out.println("No of Selected Items: "+ CustomDialogAdapterForCategoryCrimeList2.selectedItemList.size());
				System.out.println("Selected Items: "+ CustomDialogAdapterForCategoryCrimeList2.selectedItemList);
				crimeCategory_status = false;

				onSelectedCategoryOfCrime(CustomDialogAdapterForCategoryCrimeList2.selectedItemList);

				if(alertDialog != null && alertDialog.isShowing()){
					alertDialog.dismiss();

				}

			}
		});

	}

	private void onSelectedCategoryOfCrime(List<String> crimeCategoryList){

		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder stringBuilderCrimeCategory = new StringBuilder();

		for(CharSequence crimeCategory : crimeCategoryList) {
			stringBuilder.append(crimeCategory + ",");

			String crimeCategoryModifiedString = stringBuilderCrimeCategory.append("\'"+crimeCategory.toString().toLowerCase() + "\',").toString();

			crimeCategoryString = crimeCategoryModifiedString.substring(0,crimeCategoryModifiedString.length()-1);
		}

		if(crimeCategoryList.size()==0) {
			//tv_crime_value.setText("Select a Category");
			tv_crime_value.setText("");
		}
		else {
			System.out.println("Crime Category String: "+crimeCategoryString);
			tv_crime_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
		}

	}

	//---------------------------------------------------------------------------------------------------------------//

	private void createModifiedCrimeCategoryList(List<CrimeCategoryList> ccList){

		modified_crimeCategoryList.clear();
		System.out.println("CCList Size: "+ccList.size());

		obj_categoryCrimeList2 = new ArrayList<CrimeCategoryList2>();

		List<String> ccFavouriteList = new ArrayList<String>();
		List<String> ccNonFavouriteList = new ArrayList<String>();

		for(int i=0;i<ccList.size();i++){

			if(ccList.get(i).getFavourite().equalsIgnoreCase("Y")){
				ccFavouriteList.add(ccList.get(i).getCategory());
			}
			else{
				ccNonFavouriteList.add(ccList.get(i).getCategory());
			}

		}
		modified_crimeCategoryList.add("Frequently Used");
		modified_crimeCategoryList.addAll(ccFavouriteList);
		modified_crimeCategoryList.add("Others");
		modified_crimeCategoryList.addAll(ccNonFavouriteList);


		System.out.println("CC FAV Size: " + ccFavouriteList.size());
		System.out.println("CC FAV List: "+ccFavouriteList);
		System.out.println("CC Non FAV Size: "+ccNonFavouriteList.size());
		System.out.println("CC Non FAV List: "+ccNonFavouriteList);
		System.out.println("CC Non FAV List: "+modified_crimeCategoryList.size());

		for(int i=0;i<modified_crimeCategoryList.size();i++){

			CrimeCategoryList2 crimeCategoryList2 = new CrimeCategoryList2();
			crimeCategoryList2.setCategory(modified_crimeCategoryList.get(i));

			obj_categoryCrimeList2.add(crimeCategoryList2);
		}

	}

	private void fetchSubClass(String classStr){
		TaskManager taskManager = new TaskManager(this);
		taskManager.setMethod(Constants.METHOD_GET_CLASS_SUBCLASS);
		taskManager.setSubClass(true);

		String[] keys = {"class"};
		String[] values = {classStr.trim()};
		taskManager.doStartTask(keys, values, true);

		inputClassArray = classStr.split(",");

	}

	public void parseSubClassResponse(String response) {
		//System.out.println("Identity Sub Class Response: " + response);

		subClassList.clear();
		customSubClassList.clear();

		linear_subClass.setVisibility(View.VISIBLE);
		tv_subClass_value.setVisibility(View.VISIBLE);

		if (response != null && !response.equals("")) {

			JSONObject jObj = null;
			try {

				jObj = new JSONObject(response);
				if(jObj != null && jObj.optString("status").equalsIgnoreCase("1")) {

					JSONArray sub_class_resultArray = jObj.getJSONArray("result");
					for(int i=0;i<sub_class_resultArray.length();i++) {
						JSONObject row = sub_class_resultArray.getJSONObject(i);
						JSONArray jsonArray = row.getJSONArray(inputClassArray[i]);

						for(int j=0;j<jsonArray.length();j++){
							if(jsonArray.optString(j) != null && !jsonArray.optString(j).equalsIgnoreCase("null")) {
								subClassList.add(jsonArray.optString(j));
								customSubClassList.add(inputClassArray[i] + "_" + jsonArray.optString(j));
							}
						}
					}
				}
				else {
					showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_DETAIL, false);
				}
			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}

		array_subClass = new String [subClassList.size()];
		array_subClass = subClassList.toArray(array_subClass);
		Log.e("subClassList size",subClassList.size()+"");
		Log.e("customSubClassList size",customSubClassList.size()+"");

		showSelectedSubClassDialog();
	}

	 /* This method shows Sub-Class list in a pop-up */

	protected void showSelectedSubClassDialog() {

		customSelectedSubClass.clear();
		selectedSubClass.clear();
		subClassString = "";
		subClassStringShow = "";

		boolean[] checkedSubClass = new boolean[array_subClass.length];
		int count = array_subClass.length;

		for(int i = 0; i < count; i++) {
			checkedSubClass[i] = selectedSubClass.contains(array_subClass[i]);
		}

		DialogInterface.OnMultiChoiceClickListener subClassDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				if (isChecked) {
					selectedSubClass.add(array_subClass[which]);
					customSelectedSubClass.add(customSubClassList.get(which));
				} else{
					selectedSubClass.remove(array_subClass[which]);
					customSelectedSubClass.remove(customSubClassList.get(which));
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Sub-Class");
		builder.setMultiChoiceItems(array_subClass, checkedSubClass, subClassDialogListener);

		// Set the positive/yes button click listener
		builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Do something when click positive button
				onChangeSelectedSubClass();
			}
		});

		// Set the negative/no button click listener
		builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Do something when click the negative button
				customSelectedSubClass.clear();
				selectedSubClass.clear();
			}
		});

		AlertDialog dialog = builder.create();
		dialog.show();
	}

	protected void onChangeSelectedSubClass() {
		StringBuilder stringBuilderSubClass = new StringBuilder();
		StringBuilder stringBuilderSubClassShow = new StringBuilder();

		Log.e("SubClass",selectedSubClass.toString());
		Log.e("SubClass size",selectedSubClass.size()+"");
		for(CharSequence subClass : selectedSubClass) {

			String subClassModifiedString = stringBuilderSubClass.append(subClass.toString()+ ",").toString();
			subClassStringShow = subClassModifiedString.substring(0,subClassModifiedString.length()-1);

			String subClassModifiedStringShow = stringBuilderSubClassShow.append("\'"+subClass.toString()+ "\',").toString();
			subClassString = subClassModifiedStringShow.substring(0,subClassModifiedStringShow.length()-1);
		}
		if(selectedSubClass.size()==0) {
			//tv_subClass_value.setText("Select Sub-Class");
			tv_subClass_value.setText("");
		} else {
			tv_subClass_value.setText(subClassStringShow);
			//createJSONForSubClass();
		}
	}


	private  void createJSONForSubClass(){

		mapSubClass = new HashMap<String, List<String>>();
		List<String>[] subCList= new List[inputClassArray.length];
		JSONObject objSubClass = new JSONObject();

		for(int j=0;j<inputClassArray.length;j++){
			System.out.println("IN: "+inputClassArray[j]);
		}

		for(int i=0;i<customSelectedSubClass.size();i++){
			String a = customSelectedSubClass.get(i);
			insertSubClass(a.substring(0, a.indexOf("_")), a.substring(a.indexOf("_") + 1));

			for(int j=0;j<inputClassArray.length;j++){
				subCList[j] = mapSubClass.get(inputClassArray[j]);
			}

			for(int j=0;j<inputClassArray.length;j++){
				JSONArray jsonArray = new JSONArray();
				try {
					for (int k = 0; k < subCList[j].size(); k++) {
						jsonArray.put(subCList[j].get(k));
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
				try {
					if(jsonArray.length()>0) {
						objSubClass.put(inputClassArray[j], jsonArray);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			modifiedClassSubClassString="";
			modifiedClassSubClassString = objSubClass.toString();
		}
	}

	public void insertSubClass(String key, String value){
		List<String> list = mapSubClass.get(key);
		if (list == null){
			list = new ArrayList<String>();
			mapSubClass.put(key, list);
		}
		list.add(value);
	}


     /* This method shows Class list in a pop-up */

    protected void showSelectClassDialog() {
        boolean[] checkedClass = new boolean[array_class.length];
        int count = array_class.length;

        for(int i = 0; i < count; i++) {
            checkedClass[i] = selectedClassList.contains(array_class[i]);
        }

        DialogInterface.OnMultiChoiceClickListener classDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedClassList.add(array_class[which]);
                } else{
                    selectedClassList.remove(array_class[which]);
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Class");
        builder.setMultiChoiceItems(array_class, checkedClass, classDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                //tv_subClass_value.setText("Select Sub-Class");
                tv_subClass_value.setText("");
                onChangeSelectedClass();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                //tv_class_value.setText("Select Class");
                tv_class_value.setText("");
				linear_subClass.setVisibility(View.GONE);
				tv_subClass_value.setVisibility(View.GONE);
                classString="";
				newClassString = "";
                selectedClassList.clear();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedClass() {
        StringBuilder stringBuilder = new StringBuilder();
		StringBuilder stringBuilderClass = new StringBuilder();

		Log.e("Selected Class List",selectedClassList.toString());

        for(CharSequence classValue : selectedClassList){
			String classModifiedString = stringBuilderClass.append("\'"+classValue + "\',").toString();
			String newClassModifiedString = stringBuilder.append(classValue + ",").toString();
			//Log.e("Class Modified String",classModifiedString);
			classString = classModifiedString.substring(0,classModifiedString.length() - 1);
			newClassString = newClassModifiedString.substring(0,newClassModifiedString.length() - 1);
        }

        if(selectedClassList.size()==0) {
            //tv_class_value.setText("Select Class");
            tv_class_value.setText("");
            linear_subClass.setVisibility(View.GONE);
            tv_subClass_value.setVisibility(View.GONE);
            classString="";
			newClassString = "";
        }
        else {
            //tv_class_value.setText(classString);
			Log.e("Class String",classString);
			tv_class_value.setText(newClassString);
            linear_subClass.setVisibility(View.VISIBLE);
            tv_subClass_value.setVisibility(View.VISIBLE);
        }
    }


	/**
	 * Function to display simple Alert Dialog
	 * @param context - application context
	 * @param title - alert dialog title
	 * @param message - alert message
	 * @param status - success/failure (used to set icon)
	 * */
	private void showAlertDialog(Context context, String title, String message, Boolean status) {
		final AlertDialog ad = new AlertDialog.Builder(context).create();

		// Setting Dialog Title
		ad.setTitle(title);

		// Setting Dialog Message
		ad.setMessage(message);

		// Setting alert dialog icon
		ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

		// Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

		ad.setButton(DialogInterface.BUTTON_POSITIVE,
				"OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {

						if (ad != null && ad.isShowing()) {
							ad.dismiss();
						}

					}
				});

		// Showing Alert Message
		ad.show();
		ad.setCanceledOnTouchOutside(false);
	}


	private void showAgedialog() {

		if (ageArrayList.size() > 0) {
			final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.myDialog);
			builder.setTitle("Select Age Range")
					.setItems(R.array.Age_array_new, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// The 'which' argument contains the index position
							// of the selected item
							et_age.setText(ageArrayList.get(which));
						}
					});

			android.support.v7.app.AlertDialog dialog = builder.create();
			dialog.show();
		}
	}

	private void showCaseYearDialog() {

		if (yearList.size() > 0) {
			final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.myDialog);
			builder.setTitle("Select Year")
					.setItems(yearArray, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// The 'which' argument contains the index position
							// of the selected item
							et_year.setText(yearArray[which]);
						}
					});

			android.support.v7.app.AlertDialog dialog = builder.create();
			dialog.show();
		}
	}
}
