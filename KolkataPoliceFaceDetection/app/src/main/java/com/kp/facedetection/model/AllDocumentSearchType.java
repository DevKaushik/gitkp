package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by user on 27-02-2018.
 */

public class AllDocumentSearchType implements Serializable {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
