package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kp.facedetection.asynctasks.AsynctaskForDownloadPDF;
import com.kp.facedetection.interfaces.OnPDFDownload;
import com.kp.facedetection.model.PMReportSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class PMReportSearchDetailsActivity extends BaseActivity implements OnPDFDownload, View.OnClickListener, Observer {

    private String userName = "";
    private String user_Id = "";
    private String loginNumber = "";
    private String appVersion = "";

    private TextView tv_heading;
    private TextView tv_watermark;
    private TextView tv_NameValue, tv_inquestNoValue, tv_inquestDtValue, tv_pmNoValue, tv_pmDtValue,
            tv_genderValue, tv_ageValue, tv_stationNameValue, tv_firNoValue, tv_opNameValue,
            tv_psNameValue, tv_sentDtValue, tv_firDateValue, tv_firPSCodeValue;


    private TextView tv_pdf, tv_criminalPdf;
    private ImageView iv_pdf, iv_criminalPdf;

    private List<PMReportSearchDetails> pmReportSearchDetailsList;
    private PMReportSearchDetails pmReportSearchDetails;
    int positionValue;

    private boolean isPM_PDF;

    private String pdf_download_url = "";
    private String existing_pdf_path = "";

    private String criminal_pdf_download_url = "";
    private String existing_criminal_pdf_path = "";

    private String holderName = "";
    private String holderInquestNo = "";
    private String holderInquestDt = "";
    private String holderPmNo = "";
    private String holderPmYear = "";
    private String holderPmDt = "";
    private String holderAge = "";
    private String holderGender = "";
    private String holderStationName = "";
    private String holderfirNo = "";
    private String holderfirYear = "";
    private String holderfirDate = "";
    private String holderfirPS = "";
    private String holderOpName = "";
    private String holderPsName = "";
    private String holderPsCode = "";
    private String holderSntDt = "";



    public static final String KEY_POSITION = "key_position";
    public static final String KEY_PM_LIST_OBJ = "pm_list_obj";

    private boolean isFIR_PDF=false;
    private boolean isDownloadPDF=false;
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pmreport_search_details);
        ObservableObject.getInstance().addObserver(this);
        initialization();

        clickEvents();
    }

    private void initialization() {

        Bundle bundle = getIntent().getExtras();
        //pmReportSearchDetailsList = bundle.getParcelableArrayList("PMREPORT_SEARCH_RESULT");
        positionValue = getIntent().getExtras().getInt(KEY_POSITION);
        pmReportSearchDetails = bundle.getParcelable(KEY_PM_LIST_OBJ);

        pmReportSearchDetailsList = new ArrayList<PMReportSearchDetails>();

        pmReportSearchDetailsList.add(pmReportSearchDetails);

        //tv_pdf = (TextView) findViewById(R.id.tv_pdf);
        //tv_criminalPdf = (TextView) findViewById(R.id.tv_criminalPdf);

        iv_pdf = (ImageView) findViewById(R.id.iv_pdf);
        iv_criminalPdf = (ImageView) findViewById(R.id.iv_criminalPdf);

        tv_NameValue = (TextView) findViewById(R.id.tv_NameValue);
        tv_inquestNoValue = (TextView) findViewById(R.id.tv_inquestNoValue);
        tv_inquestDtValue = (TextView) findViewById(R.id.tv_inquestDtValue);
        tv_pmNoValue = (TextView) findViewById(R.id.tv_pmNoValue);
        tv_pmDtValue = (TextView) findViewById(R.id.tv_pmDtValue);
        tv_genderValue = (TextView) findViewById(R.id.tv_genderValue);
        tv_ageValue = (TextView) findViewById(R.id.tv_ageValue);
        tv_stationNameValue = (TextView) findViewById(R.id.tv_stationNameValue);
        tv_firNoValue = (TextView) findViewById(R.id.tv_firNoValue);
        tv_firDateValue = (TextView) findViewById(R.id.tv_firDateValue);
        tv_opNameValue = (TextView) findViewById(R.id.tv_opNameValue);

        tv_psNameValue = (TextView) findViewById(R.id.tv_psNameValue);
        tv_firPSCodeValue = (TextView) findViewById(R.id.tv_firPSCodeValue);
        tv_sentDtValue = (TextView) findViewById(R.id.tv_sentDtValue);



        tv_heading = (TextView) findViewById(R.id.tv_heading);
        tv_watermark = (TextView) findViewById(R.id.tv_watermark);

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
            user_Id = Utility.getUserInfo(this).getUserId();



            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);


        if (pmReportSearchDetailsList.get(positionValue).getPdf_url() != null && !pmReportSearchDetailsList.get(positionValue).getPdf_url().equalsIgnoreCase("")) {

            pdf_download_url = pmReportSearchDetailsList.get(positionValue).getPdf_url();
            System.out.println("PDF_URL " + pdf_download_url);
            //tv_pdf.setText("PM Report: View");
            iv_pdf.setVisibility(View.VISIBLE);
        } else {
            //tv_pdf.setText("PM Report: File not available");
            //Utility.setCustomTextAppearance(getApplicationContext(), tv_pdf, R.style.textAppearanceSmallBold);
            //tv_pdf.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_black));
            iv_pdf.setVisibility(View.GONE);
        }

        if (pmReportSearchDetailsList.get(positionValue).getCriminal_pdf_url() != null && !pmReportSearchDetailsList.get(positionValue).getCriminal_pdf_url().equalsIgnoreCase("")) {
            criminal_pdf_download_url = pmReportSearchDetailsList.get(positionValue).getCriminal_pdf_url();
            System.out.println("CRIMINAL_PDF_URL " + criminal_pdf_download_url);
            //tv_criminalPdf.setText("FIR: View");
            iv_criminalPdf.setVisibility(View.VISIBLE);
        } else {
            //tv_criminalPdf.setText("FIR: File not available");
            //Utility.setCustomTextAppearance(getApplicationContext(), tv_criminalPdf, R.style.textAppearanceSmallBold);
            //tv_criminalPdf.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_black));
            iv_criminalPdf.setVisibility(View.GONE);
        }

        if (pmReportSearchDetailsList.get(positionValue).getNameDeceased() != null) {
            if (pmReportSearchDetailsList.get(positionValue).getNameDeceased().toString().equalsIgnoreCase("N"))
                holderName = pmReportSearchDetailsList.get(positionValue).getNameDeceased().toString().replace("N", "NA").trim();
            else
                holderName = pmReportSearchDetailsList.get(positionValue).getNameDeceased().trim();
        }


        if (pmReportSearchDetailsList.get(positionValue).getInquestNo() != null)
            holderInquestNo = pmReportSearchDetailsList.get(positionValue).getInquestNo().trim();
        if (pmReportSearchDetailsList.get(positionValue).getPsCode() != null)
            holderPsCode = pmReportSearchDetailsList.get(positionValue).getPsCode().trim();
        if (pmReportSearchDetailsList.get(positionValue).getInquestDt() != null)
            holderInquestDt = pmReportSearchDetailsList.get(positionValue).getInquestDt().trim();


        if (pmReportSearchDetailsList.get(positionValue).getPmNo() != null)
            holderPmNo = pmReportSearchDetailsList.get(positionValue).getPmNo().trim();

        if (pmReportSearchDetailsList.get(positionValue).getPmYr() != null)
            holderPmYear = pmReportSearchDetailsList.get(positionValue).getPmYr().trim();
        if (pmReportSearchDetailsList.get(positionValue).getPmDt() != null)
            holderPmDt = pmReportSearchDetailsList.get(positionValue).getPmDt().trim();

        if (pmReportSearchDetailsList.get(positionValue).getGender() != null)
            holderGender = pmReportSearchDetailsList.get(positionValue).getGender().trim();

        if (pmReportSearchDetailsList.get(positionValue).getAgeVal() != null)
            holderAge = pmReportSearchDetailsList.get(positionValue).getAgeVal().trim();

        if (pmReportSearchDetailsList.get(positionValue).getAgeIndicator() != null)
            holderAge = holderAge + " " + pmReportSearchDetailsList.get(positionValue).getAgeIndicator().trim();

        if (pmReportSearchDetailsList.get(positionValue).getFirNo() != null) {
            if (pmReportSearchDetailsList.get(positionValue).getFirNo().toString().equalsIgnoreCase("N"))
                holderfirNo = pmReportSearchDetailsList.get(positionValue).getFirNo().replace("N", "NA").trim();
            else
                holderfirNo = pmReportSearchDetailsList.get(positionValue).getFirNo().trim();
        }
        if (pmReportSearchDetailsList.get(positionValue).getFirYr() != null)
            holderfirYear = pmReportSearchDetailsList.get(positionValue).getFirYr().trim();


        if (pmReportSearchDetailsList.get(positionValue).getFirDate() != null && !pmReportSearchDetailsList.get(positionValue).getFirDate().equalsIgnoreCase("null")
                && !pmReportSearchDetailsList.get(positionValue).getFirDate().equalsIgnoreCase("")) {
            if (pmReportSearchDetailsList.get(positionValue).getFirDate().toString().equalsIgnoreCase("N"))
                holderfirDate = pmReportSearchDetailsList.get(positionValue).getFirDate().replace("N", "NA").trim();
            else
                holderfirDate = pmReportSearchDetailsList.get(positionValue).getFirDate().trim();
        }

        if (pmReportSearchDetailsList.get(positionValue).getFirPS() != null) {
            if (pmReportSearchDetailsList.get(positionValue).getFirPS().toString().equalsIgnoreCase("N"))
                holderfirPS = pmReportSearchDetailsList.get(positionValue).getFirPS().replace("N", "NA").trim();
            else
                holderfirPS = pmReportSearchDetailsList.get(positionValue).getFirPS().trim();
        }

        if (pmReportSearchDetailsList.get(positionValue).getOpName() != null)
            holderOpName = pmReportSearchDetailsList.get(positionValue).getOpName().trim();

        if (pmReportSearchDetailsList.get(positionValue).getStationName() != null)
            holderStationName = pmReportSearchDetailsList.get(positionValue).getStationName().trim();


        if (pmReportSearchDetailsList.get(positionValue).getPsName() != null)
            holderPsName = pmReportSearchDetailsList.get(positionValue).getPsName().trim();

        if (pmReportSearchDetailsList.get(positionValue).getSent_on() != null)
            holderSntDt = pmReportSearchDetailsList.get(positionValue).getSent_on().trim();


        if (pmReportSearchDetailsList.get(positionValue).getStationName() != null) {
            if (pmReportSearchDetailsList.get(positionValue).getStationName().toString().equalsIgnoreCase("N"))
                holderStationName = pmReportSearchDetailsList.get(positionValue).getStationName().replace("N", "NA").trim();
            else
                holderStationName = pmReportSearchDetailsList.get(positionValue).getStationName().trim();
        }


        tv_NameValue.setText(holderName);
        tv_inquestNoValue.setText(holderInquestNo);
        tv_inquestDtValue.setText(holderInquestDt);
        tv_pmNoValue.setText(holderPmNo);
        tv_pmDtValue.setText(holderPmDt);
        tv_ageValue.setText(holderAge);
        tv_genderValue.setText(holderGender);
        tv_firNoValue.setText(holderfirNo);
        tv_opNameValue.setText(holderOpName);
        tv_stationNameValue.setText(holderStationName);
        tv_psNameValue.setText(holderPsName);
        tv_sentDtValue.setText(holderSntDt);
        tv_firDateValue.setText(holderfirDate);
        tv_firPSCodeValue.setText(holderfirPS);


        Constants.changefonts(tv_NameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_inquestNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_inquestDtValue, this, "Calibri.ttf");
        Constants.changefonts(tv_pmNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_pmDtValue, this, "Calibri.ttf");
        Constants.changefonts(tv_ageValue, this, "Calibri.ttf");
        Constants.changefonts(tv_genderValue, this, "Calibri.ttf");
        Constants.changefonts(tv_firNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_opNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_stationNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_psNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_sentDtValue, this, "Calibri.ttf");
        Constants.changefonts(tv_firDateValue, this, "Calibri.ttf");
        Constants.changefonts(tv_firPSCodeValue, this, "Calibri.ttf");


        Constants.changefonts(tv_heading, this, "Calibri Bold.ttf");

        /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);
    }

    private void clickEvents() {

        iv_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFIR_PDF = false;
                isDownloadPDF =true;

                Intent intent = new Intent(PMReportSearchDetailsActivity.this, PDFLoadActivity.class);
                intent.putExtra("PDF_DOWNLOAD_URL",pdf_download_url);
                intent.putExtra("DOWNLOAD_PDF", isDownloadPDF);
                intent.putExtra("REPORT_TYPE", isFIR_PDF);
                intent.putExtra("USER_ID", user_Id);
                intent.putExtra("PS_CODE", holderPsCode);
                intent.putExtra("PM_NO", holderPmNo);
                intent.putExtra("PM_YEAR", holderPmYear);

                startActivity(intent);

               /* isPM_PDF = true;
                if (pdf_download_url.length() > 0) {

                    if (existing_pdf_path.length() == 0) {
                        downloadPDFData(pdf_download_url);
                    } else {
                        showPDF(existing_pdf_path);
                    }
                }*/
            }
        });

        iv_criminalPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFIR_PDF = true;
                isDownloadPDF =true;
                Intent intent = new Intent(PMReportSearchDetailsActivity.this, PDFLoadActivity.class);
                intent.putExtra("PDF_DOWNLOAD_URL",criminal_pdf_download_url);
                intent.putExtra("DOWNLOAD_PDF", isDownloadPDF);
                intent.putExtra("REPORT_TYPE", isFIR_PDF);
                intent.putExtra("USER_ID", user_Id);
                intent.putExtra("PS_CODE", holderPsCode);
                intent.putExtra("CASE_NO", holderfirNo);
                intent.putExtra("CASE_YEAR", holderfirYear);
                startActivity(intent);

                /*isPM_PDF = false;
                if (criminal_pdf_download_url.length() > 0) {
                    if (existing_criminal_pdf_path.length() == 0) {
                        downloadPDFData(criminal_pdf_download_url);
                    } else {
                        showPDF(existing_criminal_pdf_path);
                    }
                }*/
            }
        });

    }


    private void downloadPDFData(String pdf_url) {

        if (Constants.internetOnline(PMReportSearchDetailsActivity.this)) {

            AsynctaskForDownloadPDF asynctaskForDownloadPDF = new AsynctaskForDownloadPDF(PMReportSearchDetailsActivity.this, pdf_url, isPM_PDF);
            asynctaskForDownloadPDF.execute();
            asynctaskForDownloadPDF.setDelegate(PMReportSearchDetailsActivity.this);
        } else {

            Utility.showAlertDialog(PMReportSearchDetailsActivity.this, "Connectivity Problem", "Sorry, internet connection not found", false);

        }

    }

    @Override
    public void onPDFDownloadSuccess(String filePath, boolean forWhich) {

        if (forWhich) {
            // for PM PDF
            existing_pdf_path = filePath;
            Log.e("OnPDFDownloadSuccess", filePath);
            tv_pdf.setText("PM Report: View");
            showPDF(filePath);

        } else {
            // for criminal PDF
            existing_criminal_pdf_path = filePath;
            Log.e("OnPDFDownloadSuccess_C", filePath);
            tv_criminalPdf.setText("FIR: View");
            showPDF(filePath);
        }
    }

    @Override
    public void onPDFDownloadError(String error) {

        Log.e("OnPDFDownloadError", error);

        PMReportSearchDetailsActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                Utility.showToast(PMReportSearchDetailsActivity.this, Constants.ERROR_EXCEPTION_MSG, "long");
            }
        });


    }

    private void showPDF(String filePath) {

        File file = new File(filePath);
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file), "application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something

            Utility.showAlertDialog(PMReportSearchDetailsActivity.this, "PDF Reader Problem", "Your device doesn't have PDF reader. Download a PDF reader app from play store", false);

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
