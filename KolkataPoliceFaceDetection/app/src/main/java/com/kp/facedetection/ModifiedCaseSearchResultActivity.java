package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.kp.facedetection.adapter.ModifiedCaseSearchAdapter;
import com.kp.facedetection.interfaces.OnMapIconChangeListener;
import com.kp.facedetection.interfaces.OnMapIconClickListener;
import com.kp.facedetection.model.CaseSearchDetails;
import com.kp.facedetection.model.CaseSearchFIRAllAccused;
import com.kp.facedetection.model.CaseSearchFIRDetails;
import com.kp.facedetection.model.CaseTransferDetails;
import com.kp.facedetection.model.CriminalDetails;
import com.kp.facedetection.model.LatLongDistance;
import com.kp.facedetection.singletone_classes.CommunicationViews;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by DAT-165 on 22-07-2016.
 */
public class ModifiedCaseSearchResultActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, LocationListener,GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks,OnMapIconChangeListener,OnMapIconClickListener, Observer {

    private TextView tv_resultCount;
    private ListView lv_caseSearchList;
    private Button btnLoadMore;

    private String[] keys;
    private String[] values;
    private String pageno;
    private String totalResult="";
    private String searchCase = "";
    private String userId = "";
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private TextView tv_watermark;

    ArrayList<CriminalDetails> criminalList;
    ModifiedCaseSearchAdapter caseSearchAdapter;

    List<CaseSearchDetails> caseSearchDetailsList;
    private ImageView iv_showMap;

    private String[] key_map;
    private String[] value_map;

    private String[] keyMap_parse;
    private String[] valueMap_parse;
    private String totlaResultMap = "";
    private String pagenoMap = "";
    private ArrayList<CaseSearchDetails> mapAllList = new ArrayList<CaseSearchDetails>();
    private ArrayList<LatLongDistance> latLongList = new ArrayList<LatLongDistance>();
    KPFaceDetectionApplication kpFaceDetectionApplication;
    private List<CaseSearchFIRDetails> caseSearchFIRDetailsList = new ArrayList<CaseSearchFIRDetails>();
    private List<CaseSearchFIRDetails> caseSearchFIRDetailsListFinal = new ArrayList<CaseSearchFIRDetails>();


    public int mapCount = 0;
    int clickedPos;

    // Flag for GPS status
    boolean isGPSEnabled = false;

    // Flag for network status
    boolean isNetworkEnabled = false;

    // Flag for GPS status
    boolean canGetLocation = false;

    Location location; // Location
    double latitude; // Latitude
    double longitude; // Longitude

    // Declaring a Location Manager
    protected LocationManager locationManager;
    //
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    public static final int MAP_ADDRESS_REQUEST_CODE = 123;
    private static final int LOCATION_REQUEST = 1414;
    private static final int ENABLE_GPS_REQUEST = 1515;

    protected String latitudeVal = "", longitudeVal = "";

    private String ps_code = "";
    private String case_no = "";
    private String case_yr = "";
    private String user_Id = "";
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changed_case_search_result_layout);
        ObservableObject.getInstance().addObserver(this);

        initView();

        clickEvents();

        try {
            userId = Utility.getUserInfo(this).getUserId();
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);


        keys = getIntent().getStringArrayExtra("keys");
        values = getIntent().getStringArrayExtra("value");
        pageno = getIntent().getStringExtra("pageno");
        totalResult = getIntent().getStringExtra("totalResult");
        caseSearchDetailsList = (List<CaseSearchDetails>) getIntent().getSerializableExtra("MODIFIED_CASE_SEARCH_LIST");

        key_map = getIntent().getStringArrayExtra("key_map");
        value_map = getIntent().getStringArrayExtra("value_map");

        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }

        caseSearchAdapter = new ModifiedCaseSearchAdapter(this,caseSearchDetailsList);
        caseSearchAdapter.setModifiedCaseSearchAdapter(ModifiedCaseSearchResultActivity.this);
        lv_caseSearchList.setAdapter(caseSearchAdapter);
        caseSearchAdapter.notifyDataSetChanged();

        if (Integer.parseInt(totalResult) > 20) {

            // Adding Load More button to listview at bottom
            lv_caseSearchList.addFooterView(btnLoadMore);
        }

        lv_caseSearchList.setOnItemClickListener(this);

        /*  Set user name as Watermark  */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-55);
        setAddressLatLong();

    }



    private void initView() {

        tv_resultCount=(TextView)findViewById(R.id.tv_resultCount);
        lv_caseSearchList=(ListView)findViewById(R.id.lv_caseSearchList);
        iv_showMap = (ImageView)findViewById(R.id.iv_showMap);
        iv_showMap.setVisibility(View.VISIBLE);

        // LoadMore button
        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");

        tv_watermark = (TextView)findViewById(R.id.tv_watermark);

        CommunicationViews.getInstance().setOnMapIconChangeListener(this);

    }
    private void setAddressLatLong() {

        initLocationPermission();
        initLocationManager();
    }
    private void initLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            initGoogleApiClient();
            initLocationRequest();

        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
        }
    }
    private void initGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(ModifiedCaseSearchResultActivity.this)
                    // The next two lines tell the new client that “this” current class will handle connection stuff
                    //.addConnectionCallbacks(this)
                    //.enableAutoManage(getActivity(), 0 /* clientId */, this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    //fourth line adds the LocationServices API endpoint from GooglePlayServices
                    .addApi(Places.GEO_DATA_API)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    private void initLocationManager() {
        locationManager = (LocationManager) ModifiedCaseSearchResultActivity.this.getSystemService(Context.LOCATION_SERVICE);
    }

    private void initLocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1000); // 1 second, in milliseconds
    }



    private void clickEvents(){

        iv_showMap.setOnClickListener(this);

        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // Starting a new async task
                fetchCaseSearchResultPagination();
            }
        });

    }

    private void fetchCaseSearchResultPagination() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_MODIFIED_CASE_SEARCH);
        taskManager.setModifedCaseSearchPagination(true);

        values[8] = (Integer.parseInt(pageno) + 1) + "";

        taskManager.doStartTask(keys, values, true);
    }

    public void parseModifiedCaseSearchResultPagination(String result, String[] keys, String[] values) {

        //System.out.println("ParseModifiedCaseSearchResult: " + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("pageno").toString();
                   // totalResult = jObj.opt("totalresult").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseCaseSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(ModifiedCaseSearchResultActivity.this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }


            } catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(ModifiedCaseSearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);

            }

        }

    }

    private void parseCaseSearchResponse(JSONArray result_array){
        L.e(result_array);
        for(int i=0;i<result_array.length();i++){

            JSONObject obj = null;
            CaseSearchFIRDetails caseSearchFIRDetails = new CaseSearchFIRDetails();
            CaseSearchFIRAllAccused caseSearchFIRAllAccused = new CaseSearchFIRAllAccused();
            CaseSearchDetails caseSearchDetails = new CaseSearchDetails();


            try {

                obj = result_array.getJSONObject(i);

                if(!obj.optString("ROWNUMBER").equalsIgnoreCase("null"))
                    caseSearchDetails.setRowNumber(obj.optString("ROWNUMBER"));
                if(!obj.optString("PSCODE").equalsIgnoreCase("null"))
                    caseSearchDetails.setPsCode(obj.optString("PSCODE"));
                   // caseSearchFIRAllAccused.setPsCase(obj.optString("PSCODE"));
                   // caseSearchFIRDetails.setCaseSearchFIRAllAccusedList(caseSearchFIRAllAccused);
                if(!obj.optString("PS").equalsIgnoreCase("null"))
                    caseSearchDetails.setPs(obj.optString("PS"));

                if(!obj.optString("CASENO").equalsIgnoreCase("null")) {
                    caseSearchDetails.setCaseNo(obj.optString("CASENO"));
                    caseSearchFIRDetails.setCaseNo(obj.optString("CASENO"));
                }
                if(!obj.optString("CASEDATE").equalsIgnoreCase("null"))
                    caseSearchDetails.setCaseDate(obj.optString("CASEDATE"));
                if(!obj.optString("UNDER_SECTION").equalsIgnoreCase("null"))
                    caseSearchDetails.setUnderSection(obj.optString("UNDER_SECTION"));
                if(!obj.optString("CATEGORY").equalsIgnoreCase("null")) {
                    caseSearchDetails.setCategory(obj.optString("CATEGORY"));
                    caseSearchFIRDetails.setCaseNo(obj.optString("CATEGORY"));
                }
                if(!obj.optString("CASE_YR").equalsIgnoreCase("null"))
                    caseSearchDetails.setCaseYr(obj.optString("CASE_YR"));
                   caseSearchFIRDetails.setCaseYr(obj.optString("CASE_YR"));

                if(!obj.optString("FIR_STATUS").equalsIgnoreCase("null"))
                    caseSearchDetails.setFirStatus(obj.optString("FIR_STATUS"));

                if(!obj.optString("OCCUR_TIMING").equalsIgnoreCase("null"))
                    caseSearchDetails.setOccurTime(obj.optString("OCCUR_TIMING"));
                if(!obj.optString("MOD_OPER").equalsIgnoreCase("null"))
                    caseSearchDetails.setModOper(obj.optString("MOD_OPER"));
                if(!obj.optString("PO_LAT").equalsIgnoreCase("null") && !obj.optString("PO_LAT").equalsIgnoreCase("") && obj.optString("PO_LAT") != null) {
                    caseSearchDetails.setPoLat(obj.optString("PO_LAT"));
                    caseSearchFIRDetails.setPoLat(obj.optString("PO_LAT"));
                }
                if(!obj.optString("PO_LONG").equalsIgnoreCase("null") && !obj.optString("PO_LONG").equalsIgnoreCase("") && obj.optString("PO_LONG") != null) {
                    caseSearchDetails.setPoLong(obj.optString("PO_LONG"));
                    caseSearchFIRDetails.setPoLong(obj.optString("PO_LONG"));
                }


                JSONArray briefMatch_array = obj.getJSONArray("brief_match");
                if(briefMatch_array.length() > 0){
                    List<String> briefMatch_list = new ArrayList<>();
                    for(int j=0;j<briefMatch_array.length();j++){
                        briefMatch_list.add(briefMatch_array.optString(j));
                    }
                    caseSearchDetails.setBriefMatch_list(briefMatch_list);
                }

                JSONArray case_transfer_array = obj.getJSONArray("CASE_TRANSFER");
                for (int m = 0; m < case_transfer_array.length(); m++) {

                    JSONObject JSON_obj = null;
                    try {
                        JSON_obj = case_transfer_array.getJSONObject(m);
                        CaseTransferDetails caseTransferDetails = new CaseTransferDetails();

                        if (!JSON_obj.optString("FROM_PS").equalsIgnoreCase("null"))
                            caseTransferDetails.setFromPS(JSON_obj.optString("FROM_PS"));

                        if (!JSON_obj.optString("TO_PS").equalsIgnoreCase("null"))
                            caseTransferDetails.setToPS(JSON_obj.optString("TO_PS"));

                        if (!JSON_obj.optString("FROM_IONAME").equalsIgnoreCase("null"))
                            caseTransferDetails.setFromIoName(JSON_obj.optString("FROM_IONAME"));

                        if (!JSON_obj.optString("TO_IONAME").equalsIgnoreCase("null"))
                            caseTransferDetails.setToIoName(JSON_obj.optString("TO_IONAME"));

                        if (!JSON_obj.optString("TO_UNIT").equalsIgnoreCase("null"))
                            caseTransferDetails.setToUnit(JSON_obj.optString("TO_UNIT"));

                        if (!JSON_obj.optString("CASENO").equalsIgnoreCase("null"))
                            caseTransferDetails.setCaseNo(JSON_obj.optString("CASENO"));

                        if (!JSON_obj.optString("CASE_YR").equalsIgnoreCase("null"))
                            caseTransferDetails.setCaseYr(JSON_obj.optString("CASE_YR"));

                        if (!JSON_obj.optString("DT_TRANSFER").equalsIgnoreCase("null"))
                            caseTransferDetails.setTranferDt(JSON_obj.optString("DT_TRANSFER"));

                        if (!JSON_obj.optString("REMARKS").equalsIgnoreCase("null"))
                            caseTransferDetails.setRemarks(JSON_obj.optString("REMARKS"));

                        if (JSON_obj.optString("PSNAME") != null && !JSON_obj.optString("PSNAME").equalsIgnoreCase("null") && !JSON_obj.optString("PSNAME").equalsIgnoreCase(""))
                            caseTransferDetails.setPsName(JSON_obj.optString("PSNAME"));

                        caseSearchDetails.setCaseSearchCaseTransferList(caseTransferDetails);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                caseSearchDetailsList.add(caseSearchDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        int currentPosition = lv_caseSearchList.getFirstVisiblePosition();
        int lastVisiblePosition = lv_caseSearchList.getLastVisiblePosition();

        // Setting new scroll position
        lv_caseSearchList.setSelectionFromTop(currentPosition + 1, 0);

        if((Integer.parseInt(totalResult)-lastVisiblePosition)<20){
            lv_caseSearchList.removeFooterView(btnLoadMore);
        }

    }

    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     * */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        TextView tv_caseName = (TextView) view.findViewById(R.id.tv_caseName);

        String headerValue = caseSearchDetailsList.get(position).getPs()+" C/No. "+caseSearchDetailsList.get(position).getCaseNo()+" dated "+caseSearchDetailsList.get(position).getCaseDate();
        String ps_code = caseSearchDetailsList.get(position).getPsCode();
        String case_no = caseSearchDetailsList.get(position).getCaseNo();
        String case_yr = caseSearchDetailsList.get(position).getCaseYr();

        Intent in = new Intent(ModifiedCaseSearchResultActivity.this,CaseSearchFIRDetailsActivity.class);
        in.putExtra("ITEM_NAME", tv_caseName.getText().toString().trim());
        in.putExtra("HEADER_VALUE", headerValue);
        in.putExtra("PS_CODE",ps_code);
        in.putExtra("CASE_NO", case_no);
        in.putExtra("CASE_YR", case_yr);
        in.putExtra("PS_NAME", "");
        in.putExtra("POSITION",position+"");
        in.putExtra("FROM_PAGE","ModifiedCaseSearchResultActivity");

        startActivity(in);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }


    public void disableView(View v) {

        v.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0; i < vg.getChildCount(); i++) {
                View child = vg.getChildAt(i);
                disableView(child);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_showMap:

                fetchMapView();
                break;
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }


    private void fetchMapView() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_MAP_VIEW);
        taskManager.setMapViewCase(true);

        taskManager.doStartTask(key_map, value_map, true);
    }

    public void parseMapViewCase(String result, String[] keysMap, String[] valuesMap) {

        //System.out.println("parseMapViewCase: " + result);

        if (result != null && !result.equals("")) {
            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keyMap_parse = keysMap;
                    this.valueMap_parse = valuesMap;
                    pagenoMap = jObj.opt("page").toString();
                    totlaResultMap = jObj.opt("count").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseMapViewAllResponse(resultArray);
                }
                else {
                    showAlertDialog(ModifiedCaseSearchResultActivity.this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_NO_LOCATION_FOUND_MSG, false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(ModifiedCaseSearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }

    }


    private void parseMapViewAllResponse(JSONArray result_array) {

        kpFaceDetectionApplication = KPFaceDetectionApplication.getApplication();
        mapAllList.clear();
        latLongList.clear();
        mapCount = 0;

        for (int i = 0; i < result_array.length(); i++) {

            JSONObject obj = null;
            try {
                obj = result_array.getJSONObject(i);

                LatLongDistance latLongDistance = new LatLongDistance();
                CaseSearchDetails caseMapDetails = new CaseSearchDetails();

                if (!obj.optString("PS").equalsIgnoreCase("null") && !obj.optString("PS").equalsIgnoreCase("") && obj.optString("PS") != null){
                    caseMapDetails.setPs(obj.optString("PS"));
                }

                if (!obj.optString("UNDER_SECTION").equalsIgnoreCase("null") && !obj.optString("UNDER_SECTION").equalsIgnoreCase("") && obj.optString("UNDER_SECTION") != null){
                    caseMapDetails.setUnderSection(obj.optString("UNDER_SECTION"));
                }

                if (!obj.optString("CASEDATE").equalsIgnoreCase("null") && !obj.optString("CASEDATE").equalsIgnoreCase("") && obj.optString("CASEDATE") != null){
                    caseMapDetails.setCaseDate(obj.optString("CASEDATE"));
                }

                if (!obj.optString("CASE_YR").equalsIgnoreCase("null"))
                    caseMapDetails.setCaseYr(obj.optString("CASE_YR"));

                if (!obj.optString("CASENO").equalsIgnoreCase("null"))
                    caseMapDetails.setCaseNo(obj.optString("CASENO"));

                if (!obj.optString("PSCODE").equalsIgnoreCase("null"))
                    caseMapDetails.setPsCode(obj.optString("PSCODE"));

                if (!obj.optString("CATEGORY").equalsIgnoreCase("null"))
                    caseMapDetails.setCategory(obj.optString("CATEGORY"));

                if (!obj.optString("PO_LAT").equalsIgnoreCase("null") && obj.optString("PO_LAT") != null && !obj.optString("PO_LAT").equalsIgnoreCase("")) {
                    caseMapDetails.setPoLat(obj.optString("PO_LAT"));                    //latLongList.(obj.optString("PO_LAT"));

                }

                if (!obj.optString("PO_LONG").equalsIgnoreCase("null") && obj.optString("PO_LONG") != null && !obj.optString("PO_LONG").equalsIgnoreCase("")) {
                    caseMapDetails.setPoLong(obj.optString("PO_LONG"));
                    //latLongList.add(obj.optString("PO_LONG"));
                }

                if (!obj.optString("COLOR_CODE").equalsIgnoreCase("null"))
                    caseMapDetails.setColorCode(obj.optString("COLOR_CODE"));

                mapAllList.add(caseMapDetails);


                if(!obj.optString("PO_LAT").equalsIgnoreCase("") && !obj.optString("PO_LAT").equalsIgnoreCase("null") && obj.optString("PO_LAT") != null){
                    latLongDistance.setLatitude(obj.optString("PO_LAT"));
                    //Log.e("Lat- CaseSearch",obj.optString("PO_LAT"));
                    mapCount++;

                }

                latLongList.add(latLongDistance);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }



        if(mapCount > 0){

            Intent intent = new Intent(ModifiedCaseSearchResultActivity.this, CaseMapSearchDetailsActivity.class);
            intent.putExtra("keysMap", keyMap_parse);
            intent.putExtra("valueMap", valueMap_parse);
            intent.putExtra("totalResultMap", totlaResultMap);
            intent.putExtra("pagenoMap", pagenoMap);
            intent.putExtra("MAPCOUNT",mapCount);

            // when huge data need to intent using parcellable can not possible, required setter getter class in Application class
            kpFaceDetectionApplication.setCaseMapAllList(mapAllList);

            startActivity(intent);
        }
        else{
            Log.e("Latlong list Size: ",latLongList.size()+"");
            showAlertDialog(ModifiedCaseSearchResultActivity.this, " Search Error!!! ", "Map not avaialable", false);
        }
    }


    @Override
    public void mapIconChange(int pos, String po_lat, String po_long) {
        caseSearchDetailsList.get(pos).setPoLat(po_lat);
        caseSearchDetailsList.get(pos).setPoLong(po_long);
        caseSearchAdapter.notifyDataSetChanged();
    }

    @Override
    public void onMapClick(int pos) {
        caseSearchFIRDetailsList.clear();
        clickedPos=pos;
        L.e("Map button clicked"+pos);
        CaseSearchFIRDetails caseSearchFIRDetails = new CaseSearchFIRDetails();
        CaseSearchFIRAllAccused caseSearchFIRAllAccused = new CaseSearchFIRAllAccused();
        caseSearchFIRDetails.setCaseNo(caseSearchDetailsList.get(pos).getCaseNo());
        L.e("caseNo-----------"+caseSearchDetailsList.get(pos).getCaseNo());
        caseSearchFIRDetails.setCaseYr(caseSearchDetailsList.get(pos).getCaseYr());
        caseSearchFIRDetails.setFirYr(caseSearchDetailsList.get(pos).getCaseYr());
        L.e("caseYear-----------"+caseSearchDetailsList.get(pos).getCaseYr());
        caseSearchFIRDetails.setCategory(caseSearchDetailsList.get(pos).getCategory());
        caseSearchFIRDetails.setPoLat(caseSearchDetailsList.get(pos).getPoLat());
        caseSearchFIRDetails.setPoLong(caseSearchDetailsList.get(pos).getPoLong());
        caseSearchFIRAllAccused.setPsCase(caseSearchDetailsList.get(pos).getPsCode());
        caseSearchFIRDetails.setCaseSearchFIRAllAccusedList(caseSearchFIRAllAccused);
        caseSearchFIRDetailsList.add(caseSearchFIRDetails);

        L.e("Result:---------" + "Lat:" + caseSearchFIRDetailsList.get(0).getPoLat() + " Long: " + caseSearchFIRDetailsList.get(0).getPoLong());
        if(caseSearchFIRDetailsList.get(0).getPoLat() == null || caseSearchFIRDetailsList.get(0).getPoLat().equals("null")||caseSearchFIRDetailsList.get(0).getPoLat().equals("")){

            showAlertForProceed(pos);
        }
        else {
            Intent intent = new Intent(ModifiedCaseSearchResultActivity.this, CaseCrimeMapActivity.class);
            System.out.println("Result:" + "Lat:" + caseSearchFIRDetailsList.get(0).getPoLat() + " Long: " + caseSearchFIRDetailsList.get(0).getPoLong());
            intent.putExtra("MAP_SEARCH", (Serializable) caseSearchFIRDetailsList);
            startActivity(intent);
        }

    }
   /* public Location getCurrentLocation() {

        try {
            locationManager = (LocationManager) this
                    .getSystemService(LOCATION_SERVICE);


            // Getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // Getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // No network provider is enabled
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            0, 0, this);
                    Log.d("Network", "Network");

                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }

                // If GPS enabled, get latitude/longitude using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }

            latitudeVal = latitude + "";
            longitudeVal = longitude + "";
            Log.e("Lat  -  Long: ", latitudeVal + " - " + longitudeVal);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }*/
    private void showAlertForProceed(final int position) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ModifiedCaseSearchResultActivity.this);
        // alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setTitle("Alert!");
        alertDialog.setMessage("Do you want to capture lat/long of current location and tag it to the P. O.?");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                fetchedLatLongSet(position);
                //update Map icon in ModifiedCaseSearchResultActivity using Singletone class
              //  CommunicationViews.getInstance().changeMapIcon(position, latitudeVal, longitudeVal);
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        alertDialog.show();
    }
    private void fetchedLatLongSet(int pos) {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CAPTURE_LATONG);
        taskManager.setCaptureLatLongList(true);
        ps_code=caseSearchDetailsList.get(pos).getPsCode();
        case_no=caseSearchDetailsList.get(pos).getCaseNo();
        case_yr=caseSearchDetailsList.get(pos).getCaseYr();
        user_Id = Utility.getUserInfo(this).getUserId();
       // case_no=caseSearchDetailsList.get(pos).
        keys = new String[]{"ps", "case_no", "case_yr", "lat", "long", "userId"};
        values = new String[]{ps_code.trim(), case_no.trim(), case_yr.trim(), latitudeVal.trim(), longitudeVal.trim(), user_Id.trim()};
        taskManager.doStartTask(keys, values, true);
    }
    public void parseCapturedLatLong(String result, String[] keys, String[] values) {

        //System.out.println("parseCapturedLatLong: " + result);

        if (result != null && !result.equals("")) {


            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;

                    JSONObject result_obj = jObj.getJSONObject("result");
                    L.e("Lat & Lang setting Adapter:"+"Lat:"+result_obj.optString("PO_LAT")+"Lang:"+result_obj.optString("PO_LONG"));
                    caseSearchDetailsList.get(clickedPos).setPoLat(result_obj.optString("PO_LAT"));
                    caseSearchDetailsList.get(clickedPos).setPoLong(result_obj.optString("PO_LONG"));
                    caseSearchAdapter.notifyDataSetChanged();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(ModifiedCaseSearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
            }
        } else {
            showAlertDialog(ModifiedCaseSearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_LOCATION_NOT_FOUND, false);
        }

    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        initLocationWithGoogleApiClient();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    private void initLocationWithGoogleApiClient() {
        if (ActivityCompat.checkSelfPermission(ModifiedCaseSearchResultActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ModifiedCaseSearchResultActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location != null) {

            latitude = location.getLatitude();
            longitude = location.getLongitude();
            latitudeVal=String.valueOf(latitude);
            longitudeVal=String.valueOf(longitude);

            L.e("Device---------"+"Lat:"+latitudeVal+"Longitude:"+longitudeVal);;



            /*if (address.equalsIgnoreCase("")) {
                //Utility.showAlertDialog(getActivity(), Constants.ERROR_CAPTURE_ALERT_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                return;
            } else {
                tv_address_value.setText(address);
            }*/

        } else {
            L.e("initLocationWithGoogleApiClient else");
            openGpsSettings();
        }
    }
    private void openGpsSettings() {
        L.e("openGpsSettings");

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        // **************************
        builder.setAlwaysShow(true); // this is the key ingredient
        // **************************

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                .checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        L.e("success");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        L.e("RESOLUTION_REQUIRED");
                        // Location settings are not satisfied. But could be
                        // fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling
                            // startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(ModifiedCaseSearchResultActivity.this, ENABLE_GPS_REQUEST);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        L.e("SETTINGS_CHANGE_UNAVAILABLE");
                        // Location settings are not satisfied. However, we have
                        // no way to fix the
                        // settings so we won't show the dialog.
                        break;
                    case LocationSettingsStatusCodes.CANCELED:

                        L.e("CANCELED");

                        break;
                    case LocationSettingsStatusCodes.ERROR:

                        L.e("ERROR");
                        break;
                }
            }
        });
    }


    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }


    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
