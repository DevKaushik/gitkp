package com.kp.facedetection.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DAT-165 on 24-03-2017.
 */

public class CourtCaseCategoryListDetails implements Parcelable {

    private String caseCategory = "";

    public CourtCaseCategoryListDetails(Parcel in) {
        caseCategory = in.readString();
    }

    public static final Creator<CourtCaseCategoryListDetails> CREATOR = new Creator<CourtCaseCategoryListDetails>() {
        @Override
        public CourtCaseCategoryListDetails createFromParcel(Parcel in) {
            return new CourtCaseCategoryListDetails(in);
        }

        @Override
        public CourtCaseCategoryListDetails[] newArray(int size) {
            return new CourtCaseCategoryListDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(caseCategory);
    }

    public String getCaseCategory() {
        return caseCategory;
    }

    public void setCaseCategory(String caseCategory) {
        this.caseCategory = caseCategory;
    }
}
