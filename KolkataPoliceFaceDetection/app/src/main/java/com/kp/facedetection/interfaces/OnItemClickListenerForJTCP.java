package com.kp.facedetection.interfaces;

import android.view.View;

/**
 * Created by user on 15-05-2018.
 */

public interface OnItemClickListenerForJTCP {
    public void  onItemClickForJTCP(View v, int pos);
}
