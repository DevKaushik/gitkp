package com.kp.facedetection.adapter;

/**
 * Created by DAT-Asset-131 on 07-07-2016.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.model.FIROtherAccusedDetail;

import java.util.List;

/**
 * Created by DAT-Asset-131 on 18-05-2016.
 */
public class FIROtherAccussedDetailsAdapter extends BaseAdapter {

    private Context context;
    private List<FIROtherAccusedDetail> firOtherAccusedDetailList;

    public FIROtherAccussedDetailsAdapter(Context context, List<FIROtherAccusedDetail> firOtherAccusedDetailList) {

        this.context = context;
        this.firOtherAccusedDetailList = firOtherAccusedDetailList;
    }

    @Override
    public int getCount() {

        if(firOtherAccusedDetailList.size()>0) {
            return 6;
        }
        else {
            return 0;
        }

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.fir_other_accused_list_item, null);


            holder.tv_SlNo = (TextView) convertView
                    .findViewById(R.id.tv_SlNo);

            holder.tv_accused_details = (TextView) convertView
                    .findViewById(R.id.tv_accused_details);


            Constants.changefonts(holder.tv_SlNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_accused_details, context, "Calibri Bold.ttf");


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


       // String accusedDetails=firOtherAccusedDetailList.get(position).getName()+" ,S/o "+firOtherAccusedDetailList.get(position).getFatherName()/*+" ,"+firOtherAccusedDetailList.get(position).getAddress()*/;
        holder.tv_SlNo.setText(Integer.toString(position + 1));
      //  holder.tv_accused_details.setText(accusedDetails);


        return convertView;

    }

    class ViewHolder {

        TextView tv_SlNo;
        TextView tv_accused_details;

    }

}


