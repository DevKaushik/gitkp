package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;

import java.util.Observable;
import java.util.Observer;

public class VehicleSearchDetailsActivity extends BaseActivity implements View.OnClickListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private TextView tv_heading;

    private TextView tv_mobileNoValue;
    private TextView tv_regNoValue;
    private TextView tv_regDateValue;
    private TextView tv_holderNameValue;
    private TextView tv_fNameValue;
    private TextView tv_presentAddressValue;
    private TextView tv_permanentAddressValue;
    private TextView tv_engNoValue;
    private TextView tv_chasisNoValue;
    private TextView tv_garageAddValue;
    private TextView tv_colorValue;
    private TextView tv_makerValue;
    private TextView tv_clDescValue;

    private String holder_Veh_HolderMobileNo="";
    private String holder_Veh_RegNo="";
    private String holder_Veh_RegDate="";
    private String holder_Veh_HolderName="";
    private String holder_Veh_FatherName="";
    private String holder_Veh_PresentAddress="";
    private String holder_Veh_PermanentAddress="";
    private String holder_Veh_EngineNo="";
    private String holder_Veh_ChasisNo="";
    private String holder_Veh_GarageAddress="";
    private String holder_Veh_Color="";
    private String holder_Veh_Maker="";
    private String holder_Veh_Cldesc="";
    private TextView tv_watermark;
    TextView chargesheetNotificationCount;
    String notificationCount="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehiclesearch_details);
        ObservableObject.getInstance().addObserver(this);

        initialization();
    }

    private void initialization() {

        tv_mobileNoValue = (TextView)findViewById(R.id.tv_mobileNoValue);
        tv_regNoValue = (TextView)findViewById(R.id.tv_regNoValue);
        tv_regDateValue = (TextView)findViewById(R.id.tv_regDateValue);
        tv_holderNameValue = (TextView)findViewById(R.id.tv_holderNameValue);
        tv_fNameValue = (TextView)findViewById(R.id.tv_fNameValue);
        tv_presentAddressValue = (TextView)findViewById(R.id.tv_presentAddressValue);
        tv_permanentAddressValue = (TextView)findViewById(R.id.tv_permanentAddressValue);
        tv_engNoValue = (TextView)findViewById(R.id.tv_engNoValue);
        tv_chasisNoValue = (TextView)findViewById(R.id.tv_chasisNoValue);
        tv_garageAddValue = (TextView)findViewById(R.id.tv_garageAddValue);
        tv_colorValue = (TextView)findViewById(R.id.tv_colorValue);
        tv_makerValue = (TextView)findViewById(R.id.tv_makerValue);
        tv_clDescValue = (TextView)findViewById(R.id.tv_clDescValue);

        tv_heading = (TextView) findViewById(R.id.tv_heading);

        tv_watermark = (TextView)findViewById(R.id.tv_watermark);

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        holder_Veh_HolderMobileNo = getIntent().getStringExtra("Holder_Veh_HolderMobileNo").trim();
        holder_Veh_RegNo = getIntent().getStringExtra("Holder_Veh_RegNo").trim();
        holder_Veh_RegDate = getIntent().getStringExtra("Holder_Veh_RegDate").trim();
        holder_Veh_HolderName = getIntent().getStringExtra("Holder_Veh_HolderName").trim();
        holder_Veh_FatherName = getIntent().getStringExtra("Holder_Veh_FatherName").trim();
        holder_Veh_PresentAddress = getIntent().getStringExtra("Holder_Veh_PresentAddress").trim();
        holder_Veh_PermanentAddress = getIntent().getStringExtra("Holder_Veh_PermanentAddress").trim();
        holder_Veh_EngineNo = getIntent().getStringExtra("Holder_Veh_EngineNo").trim();
        holder_Veh_ChasisNo = getIntent().getStringExtra("Holder_Veh_ChasisNo").trim();
        holder_Veh_GarageAddress = getIntent().getStringExtra("Holder_Veh_GarageAddress").trim();
        holder_Veh_Color = getIntent().getStringExtra("Holder_Veh_Color").trim();
        holder_Veh_Maker = getIntent().getStringExtra("Holder_Veh_Maker").trim();
        holder_Veh_Cldesc = getIntent().getStringExtra("Holder_Veh_Cldesc").trim();

        tv_mobileNoValue.setText(holder_Veh_HolderMobileNo);
        tv_regNoValue.setText(holder_Veh_RegNo);
        tv_regDateValue.setText(holder_Veh_RegDate);
        tv_holderNameValue.setText(holder_Veh_HolderName);
        tv_fNameValue.setText(holder_Veh_FatherName);
        tv_presentAddressValue.setText(holder_Veh_PresentAddress);
        tv_permanentAddressValue.setText(holder_Veh_PermanentAddress);
        tv_engNoValue.setText(holder_Veh_EngineNo);
        tv_chasisNoValue.setText(holder_Veh_ChasisNo);
        tv_garageAddValue.setText(holder_Veh_GarageAddress);
        tv_colorValue.setText(holder_Veh_Color);
        tv_makerValue.setText(holder_Veh_Maker);
        tv_clDescValue.setText(holder_Veh_Cldesc);

        Constants.changefonts(tv_mobileNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_regNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_regDateValue, this, "Calibri.ttf");
        Constants.changefonts(tv_holderNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_fNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_presentAddressValue, this, "Calibri.ttf");
        Constants.changefonts(tv_permanentAddressValue, this, "Calibri.ttf");
        Constants.changefonts(tv_engNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_chasisNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_garageAddValue, this, "Calibri.ttf");
        Constants.changefonts(tv_colorValue, this, "Calibri.ttf");
        Constants.changefonts(tv_makerValue, this, "Calibri.ttf");
        Constants.changefonts(tv_clDescValue, this, "Calibri.ttf");

        Constants.changefonts(tv_heading, this, "Calibri Bold.ttf");

         /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
