package com.kp.facedetection.interfaces;

import android.view.View;

/**
 * Created by user on 26-03-2018.
 */

public interface OnItemClickListenerForADDLCP {
    public void  onItemClickForADDLCP(View v, int pos);
}
