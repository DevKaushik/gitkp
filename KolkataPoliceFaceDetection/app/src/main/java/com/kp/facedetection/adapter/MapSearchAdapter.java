package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.MapSearchDetails;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by DAT-165 on 01-11-2016.
 */
public class MapSearchAdapter extends BaseAdapter {


    private Context context;
    private List<MapSearchDetails> mapSearchDetailsList;

    public MapSearchAdapter(Context context, List<MapSearchDetails> mapSearchDetailsList) {
        this.context = context;
        this.mapSearchDetailsList = mapSearchDetailsList;
    }


    @Override
    public int getCount() {

        int size = (mapSearchDetailsList.size()>0)? mapSearchDetailsList.size():0;
        return size;

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.map_list_item, null);


            holder.tv_slNo=(TextView)convertView.findViewById(R.id.tv_slNo);
            holder.tv_psName=(TextView)convertView.findViewById(R.id.tv_psName);
            holder.tv_caseDetails=(TextView)convertView.findViewById(R.id.tv_caseDetails);
            holder.tv_crimeCat=(TextView)convertView.findViewById(R.id.tv_crimeCat);


            Constants.changefonts(holder.tv_slNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_psName, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_caseDetails, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_crimeCat, context, "Calibri.ttf");

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);

        holder.tv_slNo.setText(sl_no);
        if(mapSearchDetailsList.get(position).getPsName() != null){
            holder.tv_psName.setText("PS Name: "+ mapSearchDetailsList.get(position).getPsName().trim());
        }

        if(mapSearchDetailsList.get(position).getCaseNo() != null && mapSearchDetailsList.get(position).getFirYr() != null){
            holder.tv_caseDetails.setText("Case No: "+mapSearchDetailsList.get(position).getCaseNo().trim() + " of "+ mapSearchDetailsList.get(position).getFirYr());
        }

        if(mapSearchDetailsList.get(position).getCrimeCat() != null){
            holder.tv_crimeCat.setText("Category: "+mapSearchDetailsList.get(position).getCrimeCat().trim());
        }



        return convertView;
    }

    class ViewHolder {

        TextView tv_slNo,tv_psName,tv_caseDetails,tv_crimeCat;

    }
}
