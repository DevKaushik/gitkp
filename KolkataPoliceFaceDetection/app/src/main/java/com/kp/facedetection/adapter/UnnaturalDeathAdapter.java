package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnItemClickListenerForUnnaturalDeath;
import com.kp.facedetection.model.CrimeReviewDetails;

import java.util.ArrayList;

/**
 * Created by DAT-Asset-128 on 28-11-2017.
 */

public class UnnaturalDeathAdapter  extends RecyclerView.Adapter<UnnaturalDeathAdapter.MyViewHolder> {
    Context con;
    ArrayList<CrimeReviewDetails> psList = new ArrayList<>();
    OnItemClickListenerForUnnaturalDeath onItemClickListenerForUnnaturalDeath;

    public UnnaturalDeathAdapter(Context con, ArrayList<CrimeReviewDetails> psList) {
        this.con = con;
        this.psList = psList;
    }
    public void setOnClickListener(OnItemClickListenerForUnnaturalDeath onItemClickListenerForUnnaturalDeath){
        this.onItemClickListenerForUnnaturalDeath=onItemClickListenerForUnnaturalDeath;
    }

    @Override
    public UnnaturalDeathAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.warrant_fail_due_item_layout,null,false);
        return new UnnaturalDeathAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UnnaturalDeathAdapter.MyViewHolder holder, int position) {
        final CrimeReviewDetails crimeReviewDetails = psList.get(position);
        if (crimeReviewDetails.getCrimeCategory() != null && !crimeReviewDetails.getCrimeCategory().equalsIgnoreCase("")
                && !crimeReviewDetails.getCrimeCategory().equalsIgnoreCase("null")) {
            holder.tv_categoryName.setText(crimeReviewDetails.getCrimeCategory());
        }
        if (crimeReviewDetails.getCountOfCrime() != null && !crimeReviewDetails.getCountOfCrime().equalsIgnoreCase("")
                && !crimeReviewDetails.getCountOfCrime().equalsIgnoreCase("null")) {

                holder.tv_count.setText(crimeReviewDetails.getCountOfCrime());
            }


    }

    @Override
    public int getItemCount() {
        return psList == null ? 0 : psList.size();
    }
    public  class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView tv_categoryName, tv_count ;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_categoryName = (TextView) itemView.findViewById(R.id.tv_categoryName);
            tv_count = (TextView) itemView.findViewById(R.id.tv_count);
            //  tv_count.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(onItemClickListenerForUnnaturalDeath!=null){
                onItemClickListenerForUnnaturalDeath.onClick(v,getAdapterPosition());
            }

        }
    }
}
