package com.kp.facedetection;

import android.Manifest;
import android.app.ActionBar;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kp.facedetection.model.FIRDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class CriminalCrimeMapActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener, Observer {

    private GoogleMap mMap;
    private Button bt_prev, bt_next, bt_showColorCategory;

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    List<FIRDetails> mapSearchDetailsList;

    LatLngBounds.Builder builder;
    CameraUpdate cu;
    int position = 0;
    boolean isSingle = false;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    ArrayList<LatLng> listLatLng = new ArrayList<LatLng>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_search_result);
        ObservableObject.getInstance().addObserver(this);

        setUpMap();
        initViews();

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
            System.out.println("UserName: " + userName);

        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        mapSearchDetailsList = (ArrayList<FIRDetails>) getIntent().getSerializableExtra("MAP_SEARCH");
        position = getIntent().getIntExtra("POSITION", 0);
        isSingle = getIntent().getBooleanExtra("isSingle", false);
    }


    private void initViews(){
        bt_next = (Button) findViewById(R.id.bt_next);
        bt_prev = (Button) findViewById(R.id.bt_prev);
        bt_showColorCategory = (Button) findViewById(R.id.bt_showColorCategory);
        bt_next.setVisibility(View.GONE);
        bt_prev.setVisibility(View.GONE);
        bt_showColorCategory.setVisibility(View.GONE);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setUpMap();
    }


    private void setUpMap() {
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Initialize Google Play Services for API 21 and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                //mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);
            }
        } else {
            //mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
        }

        if (!isSingle) {

            listLatLng.clear();
            for (int pos = 0; pos < mapSearchDetailsList.size(); pos++) {

                double latValue = 0.00, lngValue = 0.00;
                if (mapSearchDetailsList.get(pos).getPoLat() != null && !mapSearchDetailsList.get(pos).getPoLat().equalsIgnoreCase("null") && !mapSearchDetailsList.get(pos).getPoLat().equalsIgnoreCase("")) {
                    latValue = Double.parseDouble(mapSearchDetailsList.get(pos).getPoLat().toString());
                }

                if (mapSearchDetailsList.get(pos).getPoLong() != null && !mapSearchDetailsList.get(pos).getPoLong().equalsIgnoreCase("null") && !mapSearchDetailsList.get(pos).getPoLong().equalsIgnoreCase("")) {
                    lngValue = Double.parseDouble(mapSearchDetailsList.get(pos).getPoLong().toString());
                }


                String title = "";
                String snippetText = "";
                String psValue = "";
                String cNoValue = "";

                if ((mapSearchDetailsList.get(pos).getPs() != null) && (!mapSearchDetailsList.get(pos).getPs().equalsIgnoreCase("null")) && (!mapSearchDetailsList.get(pos).getPs().equalsIgnoreCase(""))) {

                    psValue = "SEC - " + mapSearchDetailsList.get(pos).getPs() + ", ";
                }

                if ((mapSearchDetailsList.get(pos).getCaseNo() != null && !mapSearchDetailsList.get(pos).getCaseNo().equalsIgnoreCase("") && !mapSearchDetailsList.get(pos).getCaseNo().equalsIgnoreCase("null"))
                        && (mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCaseYr() != null && !mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCaseYr().equalsIgnoreCase("") && !mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCaseYr().equalsIgnoreCase("null"))) {
                    cNoValue = "C/No: " + mapSearchDetailsList.get(pos).getCaseNo() + " of " + mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCaseYr();
                }


                if ((mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCategory() != null) && (!mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCategory().equalsIgnoreCase("")) && (!mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCategory().equalsIgnoreCase("null"))) {
                    snippetText = "CATEGORY: " + mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCategory();
                }
                title = psValue + cNoValue;

                /* Marker color code chage*/
                String col_code = "#ff3232";


                if (latValue != 0.00 && lngValue != 0.00) {

                    // create the marker
                    createMarker(latValue, lngValue, title, snippetText, col_code);

                    // save all latlong value to arrayList
                    LatLng object = new LatLng(latValue, lngValue);
                    listLatLng.add(object);
                }
            }
        }
        else {

            listLatLng.clear();
            int pos = position;
            double latValue = 0.00, lngValue = 0.00;
            if (mapSearchDetailsList.get(pos).getPoLat() != null && !mapSearchDetailsList.get(pos).getPoLat().equalsIgnoreCase("null") && !mapSearchDetailsList.get(pos).getPoLat().equalsIgnoreCase("")) {
                latValue = Double.parseDouble(mapSearchDetailsList.get(pos).getPoLat().toString());
            }

            if (mapSearchDetailsList.get(pos).getPoLong() != null && !mapSearchDetailsList.get(pos).getPoLong().equalsIgnoreCase("null") && !mapSearchDetailsList.get(pos).getPoLong().equalsIgnoreCase("")) {
                lngValue = Double.parseDouble(mapSearchDetailsList.get(pos).getPoLong().toString());
            }


            String title = "";
            String snippetText = "";
            String psValue = "";
            String cNoValue = "";

            if ((mapSearchDetailsList.get(pos).getPs() != null) && (!mapSearchDetailsList.get(pos).getPs().equalsIgnoreCase("null")) && (!mapSearchDetailsList.get(pos).getPs().equalsIgnoreCase(""))) {

                psValue = "SEC - " + mapSearchDetailsList.get(pos).getPs() + ", ";
            }

            if ((mapSearchDetailsList.get(pos).getCaseNo() != null && !mapSearchDetailsList.get(pos).getCaseNo().equalsIgnoreCase("") && !mapSearchDetailsList.get(pos).getCaseNo().equalsIgnoreCase("null"))
                    && (mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCaseYr() != null && !mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCaseYr().equalsIgnoreCase("") && !mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCaseYr().equalsIgnoreCase("null"))) {
                cNoValue = "C/No: " + mapSearchDetailsList.get(pos).getCaseNo() + " of " + mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCaseYr();
            }

            Log.e("TAG", "Size: " + mapSearchDetailsList.size());
            Log.e("TAG", "Size1: " + mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().size());


            if ((mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCategory() != null) && (!mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCategory().equalsIgnoreCase("")) && (!mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCategory().equalsIgnoreCase("null"))) {
                snippetText = "CATEGORY: " + mapSearchDetailsList.get(pos).getFirUnderSectionDetailsList().get(0).getCategory();
            }
            title = psValue + cNoValue;

            /* Marker color code chage*/
            String col_code = "#ff3232";


            if (latValue != 0.00 && lngValue != 0.00) {

                // create the marker
                createMarker(latValue, lngValue, title, snippetText, col_code);

                // save all latlong value to arrayList
                LatLng object = new LatLng(latValue, lngValue);
                listLatLng.add(object);
            }

        }
        SetZoomlevel(listLatLng);

    }


    //set the marker
    protected Marker createMarker(double latitude, double longitude, String title, String snippetText, String colCode) {

        return mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .title(title)
                .snippet(snippetText)
                .infoWindowAnchor(0.5f, 0.5f)
                .icon(getMarkerIcon(colCode)));
    }


    // method to change marker color
    public BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }


    public void SetZoomlevel(ArrayList<LatLng> listLatLng) {
        if (listLatLng != null && listLatLng.size() == 1) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(listLatLng.get(0), 10));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:

                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                // Utility.logout(this);
                break;
        }
        return true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        ImageLoader.getInstance().destroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
