package com.kp.facedetection.GlobalSearch.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.DLSearchDetails;
import com.kp.facedetection.utility.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 28-07-2016.
 */
public class GlobalDLSearchAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<DLSearchDetails> dlSearchDetailsList;
    private ValueFilter valueFilter;
    private List<DLSearchDetails> mStringFilterList;

    public GlobalDLSearchAdapter(Context context, List<DLSearchDetails> dlSearchDetailsList) {
        this.context = context;
        this.dlSearchDetailsList = dlSearchDetailsList;
        mStringFilterList =  dlSearchDetailsList;

    }


    @Override
    public int getCount() {

        int size = (dlSearchDetailsList.size()>0)? dlSearchDetailsList.size():0;
        return size;

    }

    @Override
    public Object getItem(int position) {
        return dlSearchDetailsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.modified_sdr_search_list_item, null);


            holder.tv_slNo=(TextView)convertView.findViewById(R.id.tv_slNo);
            holder.tv_name=(TextView)convertView.findViewById(R.id.tv_name);
            holder.tv_address=(TextView)convertView.findViewById(R.id.tv_address);
            holder.tv_mobileNo=(TextView)convertView.findViewById(R.id.tv_mobileNo);

            holder.tv_mobileNo.setVisibility(View.GONE);

            Constants.changefonts(holder.tv_slNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_name, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_address, context, "Calibri.ttf");

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);

        holder.tv_slNo.setText(sl_no);
        holder.tv_name.setText("NAME: "+dlSearchDetailsList.get(position).getDl_Name().trim());
        holder.tv_address.setText("ADDRESS: "+dlSearchDetailsList.get(position).getDl_PAddress().trim());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if(valueFilter==null) {

            valueFilter=new ValueFilter();
        }

        return valueFilter;

    }

    class ViewHolder {

        TextView tv_slNo,tv_name,tv_address,tv_mobileNo;


    }
    private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results=new FilterResults();
            if(constraint!=null && constraint.length()>0){
                ArrayList<DLSearchDetails> filterList=new ArrayList<DLSearchDetails>();
                for(int i=0;i<mStringFilterList.size();i++){
                    if((mStringFilterList.get(i).getDl_Name().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {
                        DLSearchDetails dlSearchDetails = new DLSearchDetails();
                        dlSearchDetails.setDl_Name(mStringFilterList.get(i).getDl_Name());
                        dlSearchDetails.setDl_PAddress(mStringFilterList.get(i).getDl_PAddress());
                        filterList.add(dlSearchDetails);
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }else{
                results.count=mStringFilterList.size();
                results.values=mStringFilterList;
            }
            return results;
        }


        //Invoked in the UI thread to publish the filtering results in the user interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            dlSearchDetailsList=(ArrayList<DLSearchDetails>) results.values;
            notifyDataSetChanged();
        }
    }



}
