package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 26-07-2016.
 */
public class CaseSearchPropertyDetails implements Serializable {

    private String propertyDesc="";
    private String psPropRegNo="";

    public String getPropertyDesc() {
        return propertyDesc;
    }

    public void setPropertyDesc(String propertyDesc) {
        this.propertyDesc = propertyDesc;
    }

    public String getPsPropRegNo() {
        return psPropRegNo;
    }

    public void setPsPropRegNo(String psPropRegNo) {
        this.psPropRegNo = psPropRegNo;
    }
}
