package com.kp.facedetection.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import com.kp.facedetection.R;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.model.ModusOperandiList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-Asset-131 on 18-05-2016.
 */
public class CustomDialogAdapterForModusOperandi extends BaseAdapter {

    List<ModusOperandiList> itemList = new ArrayList<ModusOperandiList>();
    List<ModusOperandiList> searchItemList = new ArrayList<ModusOperandiList>();
    private Context context;
    public boolean[] thumbnailsselection;
    public static List<String> selectedItemList= new ArrayList<>();

    public CustomDialogAdapterForModusOperandi(Context c, List<ModusOperandiList> itemList) {

        selectedItemList.clear();
        searchItemList.clear();
        context = c;
        this.itemList = itemList;
        searchItemList.addAll(itemList);

        for(int i=0;i<searchItemList.size();i++){
            searchItemList.get(i).setSelected(false);
        }

        thumbnailsselection = new boolean[searchItemList.size()];

        System.out.println("List Size: "+itemList.size());
    }

    @Override
    public int getCount() {

        if(searchItemList.size()>0) {
            return searchItemList.size();
        }
        else {
            return 0;
        }

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.dialog_item_row, null);


            holder.tv_itemName = (CheckedTextView) convertView
                    .findViewById(R.id.tv_itemName);

            Constants.changefonts(holder.tv_itemName, context, "Calibri.ttf");

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_itemName.setId(position);


        holder.tv_itemName.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub

                CheckedTextView cb = (CheckedTextView) v;
                int id = cb.getId();
                int getPosition = (Integer) cb.getTag();

                if (thumbnailsselection[getPosition]) {
                    cb.setChecked(false);
                    thumbnailsselection[getPosition] = false;
                    selectedItemList.remove(searchItemList.get(getPosition).getMod_operand());
                    searchItemList.get(getPosition).setSelected(false);

                } else {
                    cb.setChecked(true);
                    thumbnailsselection[getPosition] = true;
                    selectedItemList.add(searchItemList.get(getPosition).getMod_operand());
                    searchItemList.get(getPosition).setSelected(true);
                }
            }
        });

        holder.tv_itemName.setTag(position);
        holder.tv_itemName.setText(searchItemList.get(position).getMod_operand());
        holder.tv_itemName.setChecked(searchItemList.get(position).isSelected());
        holder.id = position;

        return convertView;

    }

    class ViewHolder {

        CheckedTextView tv_itemName;
        int id;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        searchItemList.clear();
        if (charText.length() == 0) {
            searchItemList.addAll(itemList);
        } else {
            for (ModusOperandiList item : itemList) {
                if (item.getMod_operand().toLowerCase().contains(charText)) {
                    searchItemList.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }
}

