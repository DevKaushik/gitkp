package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kp.facedetection.adapter.VehicleSearchAdapter;
import com.kp.facedetection.model.VehicleSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class VehicleSearchResultActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, Observer {

    private String[] keys;
    private String[] values;
    private String pageno;
    private String totalResult = "";
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private TextView tv_resultCount;
    private ListView lv_VehicleSearchList;
    private Button btnLoadMore;

    private TextView tv_watermark;

    private List<VehicleSearchDetails> vehicleSearchDetailsList;
    VehicleSearchAdapter vehicleSearchAdapter;
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sdrsearch_result);
        ObservableObject.getInstance().addObserver(this);
        initialization();
        clickEvents();
    }


    private void initialization() {

        tv_resultCount = (TextView) findViewById(R.id.tv_resultCount);
        lv_VehicleSearchList = (ListView) findViewById(R.id.lv_sdrSearchList);

        // LoadMore button
        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        keys = getIntent().getStringArrayExtra("keys");
        values = getIntent().getStringArrayExtra("value");
        pageno = getIntent().getStringExtra("pageno");
        totalResult = getIntent().getStringExtra("totalResult");

        vehicleSearchDetailsList = (List<VehicleSearchDetails>) getIntent().getSerializableExtra("VEHICLE_SEARCH_LIST");

        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }

        vehicleSearchAdapter = new VehicleSearchAdapter(this, vehicleSearchDetailsList);
        lv_VehicleSearchList.setAdapter(vehicleSearchAdapter);
        vehicleSearchAdapter.notifyDataSetChanged();

        if (Integer.parseInt(totalResult) > 20) {

            // Adding Load More button to listview at bottom
            lv_VehicleSearchList.addFooterView(btnLoadMore);
        }

        lv_VehicleSearchList.setOnItemClickListener(this);

        /*  Set user name as Watermark  */
        tv_watermark = (TextView)findViewById(R.id.tv_watermark);
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-55);

    }

    private void clickEvents() {

        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // Starting a new async task
                fetchSDRSearchResultPagination();
            }
        });

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    private void fetchSDRSearchResultPagination() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.MODIFIED_METHOD_VEHICLE_SEARCH);
        taskManager.setDocumentVehicleSearchPaginarion(true);

        values[7] = (Integer.parseInt(pageno) + 1) + "";

        taskManager.doStartTask(keys, values, true, true);
    }

    public void parseDocumentVehicleSearchPagination(String result, String[] keys, String[] values) {

        //System.out.println("Document Vehicle Search Result Pagination: " + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("page").toString();
                   // totalResult = jObj.opt("count").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseVehicleSearchResponse(resultArray);

                } else {
                    showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_NO_RECORD, false);
                }


            } catch (JSONException e) {

                e.printStackTrace();
                showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }


    private void parseVehicleSearchResponse(JSONArray resultArray) {

        for (int i = 0; i < resultArray.length(); i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj = resultArray.getJSONObject(i);
                VehicleSearchDetails vehicleSearchDetails = new VehicleSearchDetails();

                String address = "";
                String p_address = "";

                if (!jsonObj.optString("ROWNUMBER").equalsIgnoreCase("null")) {
                    vehicleSearchDetails.setRol(jsonObj.optString("ROWNUMBER"));
                }

                if (!jsonObj.optString("MOBILE_NO").equalsIgnoreCase("null")) {
                    vehicleSearchDetails.setHolder_mobileNo(jsonObj.optString("MOBILE_NO"));
                }

                if (!jsonObj.optString("REGN_NO").equalsIgnoreCase("null")) {
                    vehicleSearchDetails.setRegn_no(jsonObj.optString("REGN_NO"));
                }

                if (!jsonObj.optString("REGN_DT").equalsIgnoreCase("null")) {
                    vehicleSearchDetails.setRegn_dt(jsonObj.optString("REGN_DT"));
                }

                if (!jsonObj.optString("RTO_CD").equalsIgnoreCase("null")) {
                    vehicleSearchDetails.setRto_cd(jsonObj.optString("RTO_CD"));
                }

                if (!jsonObj.optString("ENG_NO").equalsIgnoreCase("null")) {
                    vehicleSearchDetails.setEng_no(jsonObj.optString("ENG_NO"));
                }

                if (!jsonObj.optString("CHASI_NO").equalsIgnoreCase("null")) {
                    vehicleSearchDetails.setChasis_no(jsonObj.optString("CHASI_NO"));
                }

                if (!jsonObj.optString("O_NAME").equalsIgnoreCase("null")) {
                    vehicleSearchDetails.setO_name(jsonObj.optString("O_NAME"));
                }

                if (!jsonObj.optString("F_NAME").equalsIgnoreCase("null")) {
                    vehicleSearchDetails.setF_name(jsonObj.optString("F_NAME"));
                }

                if (!jsonObj.optString("GARAGE_ADD").equalsIgnoreCase("null")) {
                    vehicleSearchDetails.setGarage_address(jsonObj.optString("GARAGE_ADD"));
                }

                if (!jsonObj.optString("COLOR").equalsIgnoreCase("null")) {
                    vehicleSearchDetails.setColor(jsonObj.optString("COLOR"));
                }

                if (!jsonObj.optString("MAKER_MODEL").equalsIgnoreCase("null")) {
                    vehicleSearchDetails.setMaker_model(jsonObj.optString("MAKER_MODEL"));
                }

                if (!jsonObj.optString("CL_DESC").equalsIgnoreCase("null")) {
                    vehicleSearchDetails.setCl_desc(jsonObj.optString("CL_DESC"));
                }

                if (!jsonObj.optString("ADD1").equalsIgnoreCase("null") && !jsonObj.optString("ADD1").equalsIgnoreCase("")) {
                    address = address + jsonObj.optString("ADD1");
                }
                if (!jsonObj.optString("ADD2").equalsIgnoreCase("null") && !jsonObj.optString("ADD2").equalsIgnoreCase("")) {
                    address = address + ", " + jsonObj.optString("ADD2");
                }
                if (!jsonObj.optString("CITY").equalsIgnoreCase("null") && !jsonObj.optString("CITY").equalsIgnoreCase("")) {
                    address = address + ", " + jsonObj.optString("CITY");
                }
                if (!jsonObj.optString("PINCODE").equalsIgnoreCase("null") && !jsonObj.optString("PINCODE").equalsIgnoreCase("")) {
                    address = address + ", " + jsonObj.optString("PINCODE");
                }

                vehicleSearchDetails.setAddress(address);

                if (!jsonObj.optString("PADD1").equalsIgnoreCase("null") && !jsonObj.optString("PADD1").equalsIgnoreCase("")) {
                    p_address = p_address + jsonObj.optString("PADD1");
                }
                if (!jsonObj.optString("PADD2").equalsIgnoreCase("null") && !jsonObj.optString("PADD2").equalsIgnoreCase("")) {
                    p_address = p_address + ", " + jsonObj.optString("PADD2");
                }
                if (!jsonObj.optString("PPINCODE").equalsIgnoreCase("null") && !jsonObj.optString("PPINCODE").equalsIgnoreCase("")) {
                    p_address = p_address + ", " + jsonObj.optString("PPINCODE");
                }

                vehicleSearchDetails.setP_address(p_address);

                vehicleSearchDetailsList.add(vehicleSearchDetails);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        int currentPosition = lv_VehicleSearchList.getFirstVisiblePosition();
        int lastVisiblePosition = lv_VehicleSearchList.getLastVisiblePosition();

        // Setting new scroll position
        lv_VehicleSearchList.setSelectionFromTop(currentPosition + 1, 0);

        if((Integer.parseInt(totalResult)-lastVisiblePosition)<20){
            lv_VehicleSearchList.removeFooterView(btnLoadMore);
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent in = new Intent(VehicleSearchResultActivity.this, VehicleSearchDetailsActivity.class);
        in.putExtra("Holder_Veh_HolderMobileNo",vehicleSearchDetailsList.get(position).getHolder_mobileNo());
        in.putExtra("Holder_Veh_RegNo",vehicleSearchDetailsList.get(position).getRegn_no());
        in.putExtra("Holder_Veh_RegDate",vehicleSearchDetailsList.get(position).getRegn_dt());
        in.putExtra("Holder_Veh_HolderName",vehicleSearchDetailsList.get(position).getO_name());
        in.putExtra("Holder_Veh_FatherName",vehicleSearchDetailsList.get(position).getF_name());
        in.putExtra("Holder_Veh_PresentAddress",vehicleSearchDetailsList.get(position).getAddress());
        in.putExtra("Holder_Veh_PermanentAddress",vehicleSearchDetailsList.get(position).getP_address());
        in.putExtra("Holder_Veh_EngineNo",vehicleSearchDetailsList.get(position).getEng_no());
        in.putExtra("Holder_Veh_ChasisNo",vehicleSearchDetailsList.get(position).getChasis_no());
        in.putExtra("Holder_Veh_GarageAddress",vehicleSearchDetailsList.get(position).getGarage_address());
        in.putExtra("Holder_Veh_Color",vehicleSearchDetailsList.get(position).getColor());
        in.putExtra("Holder_Veh_Maker",vehicleSearchDetailsList.get(position).getMaker_model());
        in.putExtra("Holder_Veh_Cldesc",vehicleSearchDetailsList.get(position).getCl_desc());

        startActivity(in);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }


    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}

