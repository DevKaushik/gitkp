package com.kp.facedetection.webservices;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import com.kp.facedetection.After_login;
import com.kp.facedetection.AllDocumentDataSearchResultActivity;
import com.kp.facedetection.ArrestTabFromDashboardActivity;
import com.kp.facedetection.BARSearchResultActivity;
import com.kp.facedetection.BarSearchActivity;
import com.kp.facedetection.BaseActivity;
import com.kp.facedetection.CableSearchResultActivity;
import com.kp.facedetection.CaptureLogListActivity;
import com.kp.facedetection.CaseMapSearchDetailsActivity;
import com.kp.facedetection.CaseSearchFIRDetailsActivity;
import com.kp.facedetection.ChargesheetDueCaseListActivity;
import com.kp.facedetection.ChargesheetDuePsListActivity;
import com.kp.facedetection.CrimeReviewActivity;
import com.kp.facedetection.CrimeReviewDetailsActivity;
import com.kp.facedetection.CriminalDetailActivity;
import com.kp.facedetection.DLSearchResultActivity;
import com.kp.facedetection.DashboardActivity;
import com.kp.facedetection.DisposalCaseSearchResultActivity;
import com.kp.facedetection.DisposalMapSearchDetailsActivity;
import com.kp.facedetection.DisposalTabFromDashboardActivity;
import com.kp.facedetection.FIRPSListActivity;
import com.kp.facedetection.FIRTabFromDashboardActivity;
import com.kp.facedetection.FeedbackActivity;
import com.kp.facedetection.FingerprintAuthActivity;
import com.kp.facedetection.GasSearchResultActivity;
import com.kp.facedetection.GlobalSearch.activity.GlobalCableSearchDetailsActivity;
import com.kp.facedetection.GlobalSearch.activity.GlobalDLSearchDetailsActivity;
import com.kp.facedetection.GlobalSearch.activity.GlobalGasSearchDetailsActivity;
import com.kp.facedetection.GlobalSearch.activity.GlobalKMCSearchDetailsActivity;
import com.kp.facedetection.GlobalSearch.activity.GlobalSDRSearchDetailsActivity;
import com.kp.facedetection.GlobalSearch.activity.GlobalVehicleSearchDetailsActivity;
import com.kp.facedetection.HighCourtCaseSearchResultActivity;
import com.kp.facedetection.HotelSearchActivity;
import com.kp.facedetection.HotelSearchResultActivity;
import com.kp.facedetection.KMCSearchResultActivity;
import com.kp.facedetection.LoginActivity;
import com.kp.facedetection.MapActivity;
import com.kp.facedetection.MapSearchDetailsActivity;
import com.kp.facedetection.MapSearchResultActivity;
import com.kp.facedetection.MisActiveUsersActivity;
import com.kp.facedetection.MisActivity;
import com.kp.facedetection.MisOnlineUsersActivity;
import com.kp.facedetection.MobileStolenSearchResultActivity;
import com.kp.facedetection.ModifiedCaseSearchResultActivity;
import com.kp.facedetection.NotificationDetailActivity;
import com.kp.facedetection.PDFLoadActivity;
import com.kp.facedetection.PMReportSearchResultActivity;
import com.kp.facedetection.PreferenceActivity;
import com.kp.facedetection.RTATabFromDashboardActivity;
import com.kp.facedetection.SDRSearchActivity;
import com.kp.facedetection.SDRSearchResultActivity;
import com.kp.facedetection.SearchResultActivity;
import com.kp.facedetection.SpecialServiceActivity;
import com.kp.facedetection.SpecialServiceDetailsActivity;
import com.kp.facedetection.SpecialServiceList;
import com.kp.facedetection.SpecialServiceSearch;
import com.kp.facedetection.UnnaturalDeathDetailsActivity;
import com.kp.facedetection.UnnaturalDeathListActivity;
import com.kp.facedetection.VehicleSearchResultActivity;
import com.kp.facedetection.WarrantExcDetailsActivityNew;
import com.kp.facedetection.WarrantExcTabFromDashboardActivity;
import com.kp.facedetection.WarrantPendingDetailsActivity;
import com.kp.facedetection.WarrantPendingTabFromDashboardActivity;
import com.kp.facedetection.WarrantRecvDetailsActivityNew;
import com.kp.facedetection.WarrantRecvTabFromDashboardActivity;
import com.kp.facedetection.WarrantSearchDetailsActivity;
import com.kp.facedetection.WarrantSearchResultActivity;
import com.kp.facedetection.WarrentFallingDueDetailActivityNew;
import com.kp.facedetection.WarrentFallingDuePsActivity;
import com.kp.facedetection.fragments.AllDocumentDataSearchFragment;
import com.kp.facedetection.fragments.ArrestFragment;
import com.kp.facedetection.fragments.CableDataSearchFragment;
import com.kp.facedetection.fragments.CaptureFIRFragment;
import com.kp.facedetection.fragments.CaseSearchFragment;
import com.kp.facedetection.fragments.CriminalFIRDetailsFragment;
import com.kp.facedetection.fragments.CriminalSearchFragment;
import com.kp.facedetection.fragments.DLSearchFragment;
import com.kp.facedetection.fragments.DashboardArrestOthersFragment;
import com.kp.facedetection.fragments.DashboardArrestSpecificFragment;
import com.kp.facedetection.fragments.DetailsForCaptureFIRFragment;
import com.kp.facedetection.fragments.FIRFragment;
import com.kp.facedetection.fragments.GasDataSearchFragment;
import com.kp.facedetection.fragments.HighCourtSearchFragment;
import com.kp.facedetection.fragments.HotelSearchFragment;
import com.kp.facedetection.fragments.ImageSearchFragment;
import com.kp.facedetection.fragments.KMCDataSearchFragment;
import com.kp.facedetection.fragments.MapSerachFragment;
import com.kp.facedetection.fragments.MobileStolenFragment;
import com.kp.facedetection.fragments.ModifiedArrestFragment;
import com.kp.facedetection.fragments.ModifiedCableDataSearchFragment;
import com.kp.facedetection.fragments.ModifiedCaseSearchFragment;
import com.kp.facedetection.fragments.ModifiedFIRFragment;
import com.kp.facedetection.fragments.ModifiedGasDataSearchFragment;
import com.kp.facedetection.fragments.ModifiedKMCDataSearchFragment;
import com.kp.facedetection.fragments.ModifiedProfileFragment;
import com.kp.facedetection.fragments.ModifiedWarrantFragment;
import com.kp.facedetection.fragments.PMReportFragment;
import com.kp.facedetection.fragments.ProfileFragment;
import com.kp.facedetection.fragments.SDRDataSearchFragment;
import com.kp.facedetection.fragments.SendPushActivity;
import com.kp.facedetection.fragments.VehicleSearchFragment;
import com.kp.facedetection.fragments.WarrantSearchFragment;
import com.kp.facedetection.fragments.WarrentFragment;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;


public class TaskManager implements IResponseCallback {
    private Activity mCurrentActivity;
    private Fragment fragment;
    private String methodname;
    private String[] keys, values;
    private RestClient client;
    //	private boolean isVSBCall;
    private View addLI;
    private View addLIBlackList;
    private View addAllRequest;
    private boolean isInternetConnected;


   /* private boolean isLogin, isDetailedSearch, isDetailedSearchPaging, isUploadImage, isFirDownloadLog, isFetchImageDetail, isForgetPassword, isSendPush, isNotificationDetails, isLogout, isGetContent,isGetContentAllDocumentSearch, isGetContentSpService, isGetContentPref, isIdentityCategory, isIdentifySubCategory, isDivisionWisePoliceStation, isCaseSearch, isCriminalDetailsArrest, isCriminalDetailsWarrent, isCriminalDetailsFIR, isAdvanceCriminalSearch, isIdentifySubCategoryAtSearchResultActivity, isProfileDetails, isModifiedWarrentDetails, isModifiedArrestDetails, isModifiedFIRDetails, isCriminalProfileDescriptiveRole, isCriminalProfileDescriptiveRoleUpdate, isCriminalProfileCRS, isCriminalProfileAssociates, isCriminalModifiedFIRDetails, isCriminalModifiedArrestDetails, isCriminalModifiedWarrantDetails, isModifiedCaseSearch, isModifiedCaseSearchPagination, isModifiedCaseSearchFIRDetails, isWarrarntSearch, isWarrarntSearchPaging, isWarrantSearchItemDeatils, isEditComment, isDocumentDLSearch, isVehicleSearch, isGetGasContent, isGasSearch, isGetKMCContent, isKMCSearch, isGetCableContent, isCableSearch, isSDRSearch, isBARSearch, isHotelSearch, isDocumentAllDataSearch, isDLAllDataRelationalSearch, isVehicleAllDataRelationalSearch, isSDRAllDataRelationalSearch, isCableAllDataRelationalSearch, isKMCAllDataRelationalSearch, isGasAllDataRelationalSearch, isEistingHotelList, isBARSearchPaging, isHotelSearchPaging, isDocumentAllDataSearchPaging, isSDRSearchPaging, isUserForgotPassword, isCaseSearchEditComment, isChangePassword, isPsWiseIOListForCaseSearch, isDivWisePSListForCaseSearch, isDivWisePSListForWarrantSearch, isPsWiseIOListForWarrant, isDocumentDLSearchPagination, isDocumentVehicleSearchPagination, isMobileStolenSearch, isMobileStolenSearchPaging, isMobileStolenStatus, isDocumentGasSearch, isDocumentGasSearchPagination, isDocumentKMCSearch, isDocumentKMCSearchPagination, isDocumentCableSearch, isDocumentCableSearchPagination, isPMReportListData, isPMReportSearch, isPMReportSearchPagination, isGetSubClass, isMapSearch, isMapSearchPagination, isMapView, isMapViewPagination, isCourtCaseListData, isCourtCaseSearch, isCaptureLatLngList, isCourtCaseSearchPaging, isMapViewCase, isMapViewCaseForDisposal, isMapViewCaseForDisposalPagination, isMapViewCasePagination, isSubClassAtSearchResultActivity, isCaptureLatLng, isCaptureLatLngCriminal, isCaptureLatLngCF, isCriminalPicUpdate, isCaseFirLogList, isCriminalFirLogList, isInvestigateUnitStatus, isInvestigateSec, isTempFirCapture, isFeedback, isGeoCode, isPreference, isDashboard, isCrimeReview, isCrimeReviewSearch, isAllFirView,
            isAllFirViewForCase, isAllRtaView, isMapViewCaseFromRTA, isAllSpecificView, isAllSpecificViewPagination, isArrestSpecificViewForCase, isAllOtherView, isAllOtherViewPagination, isArrestOtherViewForCase, isCrimeReviewToCaseSearchList, isFIRAllPSFromCategory, isDisposalsView, isDisposalToCaseSearchList, isDisposalToCaseSearchListPagination, isWaExecView, isWaRecvView, isWaFallDueView, isWaExecDetailViewNBW, isWaExecDetailViewBW, isWaRecvDetailViewNBW, isWaFallDueDetailViewNBW, isWaRecvDetailViewBW, isWaFallDueDetailViewBW,

            isWaPendingView, isWaPendingDetailViewNBW, isWaPendingDetailViewBW, isListCapture, isTagAdd, isTagUnTag, isDeleteCaptureFIR, isGetContentCapture, isListCapturePagination, isUnnaturalDeath, isUnnaturalDeathDetails, isChargesheetDuePs, isChargesheetDueCS, isDocumentSearchAunthicated, isAllSearchAunthicated, isDocumentHotelSearchAunthicated, isSpecialServiceSaved, isSpecialServiceItemUpdated,isSpecialServiceItemSkipped, isSpecialServiceListData, isSpecialServiceRequestAuthenticated, isSpecialServicePasscode, isSpecialServiceLoggerInfo, isSpecialServiceLISDRinfo,isLIReconnectionJustification,isLIReconnectionCreate,isSpecialServiceListSearch,isSpecialServiceAllSDRinfo,isSDRSearchAllState,isReminderToMcell,isLIDisconnectionCreate,isSpecialServiceItemSkiped,isLISearchAllState,isBlackListed,isRequestRevoked,isSpecialServiceRequestProcessType,isSpecialServiceSearchListData,isSpecialServiceLIDetailsStatusChange,isLIPartialSaved,isNodalSuggestionSaved,isNodalSuggestionOnLoading,isMcellSuggestionOnLoading;
*/
    private boolean isLogin, isDetailedSearch, isDetailedSearchPaging, isUploadImage, isFirDownloadLog, isFetchImageDetail, isForgetPassword, isSendPush, isNotificationDetails, isLogout, isGetContent,isGetContentAllDocumentSearch, isGetContentSpService, isGetContentPref, isIdentityCategory, isIdentifySubCategory, isDivisionWisePoliceStation, isCaseSearch, isCriminalDetailsArrest, isCriminalDetailsWarrent, isCriminalDetailsFIR, isAdvanceCriminalSearch, isIdentifySubCategoryAtSearchResultActivity, isProfileDetails, isModifiedWarrentDetails, isModifiedArrestDetails, isModifiedFIRDetails, isCriminalProfileDescriptiveRole, isCriminalProfileDescriptiveRoleUpdate, isCriminalProfileCRS, isCriminalProfileAssociates, isCriminalModifiedFIRDetails, isCriminalModifiedArrestDetails, isCriminalModifiedWarrantDetails, isModifiedCaseSearch, isModifiedCaseSearchPagination, isModifiedCaseSearchFIRDetails, isWarrarntSearch, isWarrarntSearchPaging, isWarrantSearchItemDeatils, isEditComment, isDocumentDLSearch, isVehicleSearch, isGetGasContent, isGasSearch, isGetKMCContent, isKMCSearch, isGetCableContent, isCableSearch, isSDRSearch, isBARSearch, isHotelSearch, isDocumentAllDataSearch, isDLAllDataRelationalSearch, isVehicleAllDataRelationalSearch, isSDRAllDataRelationalSearch, isCableAllDataRelationalSearch, isKMCAllDataRelationalSearch, isGasAllDataRelationalSearch, isEistingHotelList, isBARSearchPaging, isHotelSearchPaging, isDocumentAllDataSearchPaging, isSDRSearchPaging, isUserForgotPassword, isCaseSearchEditComment, isChangePassword, isPsWiseIOListForCaseSearch, isDivWisePSListForCaseSearch, isDivWisePSListForWarrantSearch, isPsWiseIOListForWarrant, isDocumentDLSearchPagination, isDocumentVehicleSearchPagination, isMobileStolenSearch, isMobileStolenSearchPaging, isMobileStolenStatus, isDocumentGasSearch, isDocumentGasSearchPagination, isDocumentKMCSearch, isDocumentKMCSearchPagination, isDocumentCableSearch, isDocumentCableSearchPagination, isPMReportListData, isPMReportSearch, isPMReportSearchPagination, isGetSubClass, isMapSearch, isMapSearchPagination, isMapView, isMapViewPagination, isCourtCaseListData, isCourtCaseSearch, isCaptureLatLngList, isCourtCaseSearchPaging, isMapViewCase, isMapViewCaseForDisposal, isMapViewCaseForDisposalPagination, isMapViewCasePagination, isSubClassAtSearchResultActivity, isCaptureLatLng, isCaptureLatLngCriminal, isCaptureLatLngCF, isCriminalPicUpdate, isCaseFirLogList, isCriminalFirLogList, isInvestigateUnitStatus, isInvestigateSec,isCurrentVer, isTempFirCapture, isFeedback, isGeoCode, isPreference, isDashboard, isCrimeReview, isCrimeReviewSearch, isAllFirView,
            isAllFirViewForCase, isAllRtaView, isMapViewCaseFromRTA, isAllSpecificView, isAllSpecificViewPagination, isArrestSpecificViewForCase, isAllOtherView, isAllOtherViewPagination, isArrestOtherViewForCase, isCrimeReviewToCaseSearchList, isFIRAllPSFromCategory, isDisposalsView, isDisposalToCaseSearchList, isDisposalToCaseSearchListPagination, isWaExecView, isWaRecvView, isWaFallDueView, isWaExecDetailViewNBW, isWaExecDetailViewBW, isWaRecvDetailViewNBW, isWaFallDueDetailViewNBW, isWaRecvDetailViewBW, isWaFallDueDetailViewBW,
            isWaPendingView, isWaPendingDetailViewNBW, isWaPendingDetailViewBW, isListCapture, isTagAdd, isTagUnTag, isDeleteCaptureFIR, isGetContentCapture, isListCapturePagination, isUnnaturalDeath, isUnnaturalDeathDetails, isChargesheetDuePs, isChargesheetDueCS, isDocumentSearchAunthicated, isAllSearchAunthicated, isBarSearchAunthicated,isSDRSearchAunthicated,isDocumentHotelSearchAunthicated, isSpecialServiceSaved, isSpecialServiceItemUpdated, isSpecialServiceListData, isSpecialServiceRequestAuthenticated, isSpecialServicePasscode, isSpecialServiceLoggerInfo, isSpecialServiceLISDRinfo,isLIReconnectionJustification,isLIReconnectionCreate,isSpecialServiceListSearch,isSpecialServiceAllSDRinfo,isSDRSearchAllState,isReminderToMcell,isLIDisconnectionCreate,isSpecialServiceItemSkiped,isLISearchAllState,isBlackListed,isRequestRevoked,isSpecialServiceRequestProcessType,isSpecialServiceSearchListData,isSpecialServiceLIDetailsStatusChange,isLIPartialSaved,isNodalSuggestionSaved,isNodalSuggestionOnLoading,isMisCount,isOnlineUser,isActiveUser,isSpecialServiceItemSkipped,isSpecialServiceAllowUserListData,isMcellSuggestionOnLoading;

            /*isWaPendingView, isWaPendingDetailViewNBW, isWaPendingDetailViewBW, isListCapture, isTagAdd, isTagUnTag, isDeleteCaptureFIR, isGetContentCapture, isListCapturePagination, isUnnaturalDeath, isUnnaturalDeathDetails, isChargesheetDuePs, isChargesheetDueCS, isDocumentSearchAunthicated, isAllSearchAunthicated, isDocumentHotelSearchAunthicated, isSpecialServiceSaved, isSpecialServiceItemUpdated,isSpecialServiceItemSkipped, isSpecialServiceListData, isSpecialServiceAllowUserListData,isSpecialServiceRequestAuthenticated, isSpecialServicePasscode, isSpecialServiceLoggerInfo, isSpecialServiceLISDRinfo,isLIReconnectionJustification,isLIReconnectionCreate,isSpecialServiceListSearch,isSpecialServiceAllSDRinfo,isSDRSearchAllState,isReminderToMcell,isLIDisconnectionCreate,isSpecialServiceItemSkiped,isLISearchAllState,isBlackListed,isRequestRevoked,isSpecialServiceRequestProcessType,isSpecialServiceSearchListData,isSpecialServiceLIDetailsStatusChange,isLIPartialSaved,isNodalSuggestionSaved,isNodalSuggestionOnLoading,isMcellSuggestionOnLoading;*/

    Context mContext;
    private String[] keysImg, valuesImg;

    public TaskManager(Activity activity) {

        this.mCurrentActivity = activity;
        this.mContext = mCurrentActivity;
        isInternetConnected = Constants.internetOnline(activity);
    }

    public TaskManager(Fragment fragment) {
        this.fragment = fragment;
        this.mContext = fragment.getActivity();
        isInternetConnected = Constants.internetOnline(fragment.getActivity());
    }

    public void setMethod(String method) {
        methodname = method;
    }

    public void setLogin(boolean isLogin) {
        this.isLogin = isLogin;
    }

    public void setDetailedSearch(boolean isDetailedSearch) {
        this.isDetailedSearch = isDetailedSearch;
    }

    public void setAdvanceCriminalSearch(boolean isAdvanceCriminalSearch) {
        this.isAdvanceCriminalSearch = isAdvanceCriminalSearch;
    }

    public void setModifedCaseSearch(boolean isModifiedCaseSearch) {
        this.isModifiedCaseSearch = isModifiedCaseSearch;
    }

    public void setMapSearch(boolean isMapSearch) {
        this.isMapSearch = isMapSearch;
    }

    public void setMapSearchPagination(boolean isMapSearchPagination) {
        this.isMapSearchPagination = isMapSearchPagination;
    }

    public void setMapView(boolean isMapView) {
        this.isMapView = isMapView;
    }

    public void setMapViewPagination(boolean isMapViewPagination) {
        this.isMapViewPagination = isMapViewPagination;
    }

    public void setMapViewCase(boolean isMapViewCase) {
        this.isMapViewCase = isMapViewCase;
    }

    public void setMapViewCasePagination(boolean isMapViewCasePagination) {
        this.isMapViewCasePagination = isMapViewCasePagination;
    }

    public void setMapViewCaseForDisposal(boolean isMapViewCaseForDisposal) {
        this.isMapViewCaseForDisposal = isMapViewCaseForDisposal;
    }

    public void setMapViewCaseForDisposalPagination(boolean isMapViewCaseForDisposalPagination) {
        this.isMapViewCaseForDisposalPagination = isMapViewCaseForDisposalPagination;
    }


    public void setWarrantSearch(boolean isWarrarntSearch) {
        this.isWarrarntSearch = isWarrarntSearch;
    }


    public void setCaptureLatLong(boolean isCaptureLatLng) {
        this.isCaptureLatLng = isCaptureLatLng;
    }

    public void setFirDownloadLog(boolean isFirDownloadLog) {
        this.isFirDownloadLog = isFirDownloadLog;
    }


    public void setCaptureLatLongForOF(boolean isCaptureLatLngCriminal) {
        this.isCaptureLatLngCriminal = isCaptureLatLngCriminal;
    }

    public void setCriminalPicUpdate(boolean isCriminalPicUpdate) {
        this.isCriminalPicUpdate = isCriminalPicUpdate;
    }

    public void setCaptureLatLongForCF(boolean isCaptureLatLngCF) {
        this.isCaptureLatLngCF = isCaptureLatLngCF;
    }

    public void setWarrantSearchPagination(boolean isWarrarntSearchPaging) {
        this.isWarrarntSearchPaging = isWarrarntSearchPaging;
    }

    public void setModifedCaseSearchPagination(boolean isModifiedCaseSearchPagination) {
        this.isModifiedCaseSearchPagination = isModifiedCaseSearchPagination;
    }

    public void setModifedCaseSearchFIRDetails(boolean isModifiedCaseSearchFIRDetails) {
        this.isModifiedCaseSearchFIRDetails = isModifiedCaseSearchFIRDetails;
    }

    public void setWarrantSearchItemDetails(boolean isWarrantSearchItemDeatils) {
        this.isWarrantSearchItemDeatils = isWarrantSearchItemDeatils;
    }


    public void setAllContent(boolean isGetContent) {
        this.isGetContent = isGetContent;
    }
    public void setAllContentAllDocumentSearch(boolean isGetContentAllDocumentSearch) {
        this.isGetContentAllDocumentSearch = isGetContentAllDocumentSearch;
    }
    //isGetContentAllDocumentSearch


    public void setAllContentSpService(boolean isGetContentSpService) {
        this.isGetContentSpService = isGetContentSpService;
    }

    public void setSpecialServiceRequestPermission(boolean isSpecialServiceRequestAuthenticated) {
        this.isSpecialServiceRequestAuthenticated = isSpecialServiceRequestAuthenticated;
    }

    public void setAllContentPreference(boolean isGetContentPref) {
        this.isGetContentPref = isGetContentPref;
    }

    public void setAllContentForCaptureLog(boolean isGetContentCapture) {
        this.isGetContentCapture = isGetContentCapture;
    }


    public void setIdentityCategory(boolean isIdentityCategory) {
        this.isIdentityCategory = isIdentityCategory;
    }

    public void setIdentitySubCategory(boolean isIdentifySubCategory) {
        this.isIdentifySubCategory = isIdentifySubCategory;
    }

    public void setSubClass(boolean isGetSubClass) {
        this.isGetSubClass = isGetSubClass;
    }

    public void setPsWiseIOListForCaseSearch(boolean isPsWiseIOListForCaseSearch) {
        this.isPsWiseIOListForCaseSearch = isPsWiseIOListForCaseSearch;
    }

    public void setDivWisePsListForCaseSearch(boolean isDivWisePSListForCaseSearch) {
        this.isDivWisePSListForCaseSearch = isDivWisePSListForCaseSearch;
    }

    public void setDivWisePsListForWarrantSearch(boolean isDivWisePSListForWarrantSearch) {
        this.isDivWisePSListForWarrantSearch = isDivWisePSListForWarrantSearch;
    }


    public void setPsWiseIOListForWarrant(boolean isPsWiseIOListForWarrant) {
        this.isPsWiseIOListForWarrant = isPsWiseIOListForWarrant;
    }


    public void setIdentitySubCategoryForAdvanceSearch(boolean isIdentifySubCategoryAtSearchResultActivity) {
        this.isIdentifySubCategoryAtSearchResultActivity = isIdentifySubCategoryAtSearchResultActivity;
    }

    public void setSubClassForAdvanceSearch(boolean isSubClassAtSearchResultActivity) {
        this.isSubClassAtSearchResultActivity = isSubClassAtSearchResultActivity;
    }

    public void setDivisionWisePoliceStation(boolean isDivisionWisePoliceStation) {
        this.isDivisionWisePoliceStation = isDivisionWisePoliceStation;
    }

    public void setCaseSearch(boolean isCaseSearch) {
        this.isCaseSearch = isCaseSearch;
    }

    public void setCaptureLatLongList(boolean isCaptureLatLngList) {
        this.isCaptureLatLngList = isCaptureLatLngList;
    }

    public void setCriminalDetailsArrest(boolean isCriminalDetailsArrest) {
        this.isCriminalDetailsArrest = isCriminalDetailsArrest;
    }

    public void setModifiedCriminalDetailsArrest(boolean isModifiedArrestDetails) {
        this.isModifiedArrestDetails = isModifiedArrestDetails;
    }

    public void setCriminalDetailsWarrent(boolean isCriminalDetailsWarrent) {
        this.isCriminalDetailsWarrent = isCriminalDetailsWarrent;
    }

    public void setModifiedCriminalDetailsWarrent(boolean isModifiedWarrentDetails) {
        this.isModifiedWarrentDetails = isModifiedWarrentDetails;
    }

    public void setCriminalDetailsFIR(boolean isCriminalDetailsFIR) {
        this.isCriminalDetailsFIR = isCriminalDetailsFIR;
    }

    public void setModifiedCriminalDetailsFIR(boolean isModifiedFIRDetails) {
        this.isModifiedFIRDetails = isModifiedFIRDetails;
    }

    public void setCriminalModifiedFIRDetails(boolean isCriminalModifiedFIRDetails) {
        this.isCriminalModifiedFIRDetails = isCriminalModifiedFIRDetails;
    }

    public void setCriminalModifiedArrestDetails(boolean isCriminalModifiedArrestDetails) {
        this.isCriminalModifiedArrestDetails = isCriminalModifiedArrestDetails;
    }

    public void setCriminalModifiedWarrantDetails(boolean isCriminalModifiedWarrantDetails) {
        this.isCriminalModifiedWarrantDetails = isCriminalModifiedWarrantDetails;
    }


    public void setDetailedSearchPaging(boolean isDetailedSearchPaging) {
        this.isDetailedSearchPaging = isDetailedSearchPaging;
    }

    public void setUploadImage(boolean isUploadImage) {
        this.isUploadImage = isUploadImage;
    }

    public void setFetchImageDetail(boolean isFetchImageDetail) {
        this.isFetchImageDetail = isFetchImageDetail;
    }

    public void setForgetPassword(boolean isForgetPassword) {
        this.isForgetPassword = isForgetPassword;
    }

    public void setChangePassword(boolean isChangePassword) {
        this.isChangePassword = isChangePassword;
    }

    public void setSendPush(boolean isSendPush) {
        this.isSendPush = isSendPush;
    }

    public void setLogout(boolean isLogout) {
        this.isLogout = isLogout;
    }

    public void setNotificationDetails(boolean isNotificationDetails) {
        this.isNotificationDetails = isNotificationDetails;
    }

    public void setProfileDetails(boolean isProfileDetails) {
        this.isProfileDetails = isProfileDetails;
    }

    public void setCriminalProfileDescriptiveRole(boolean isCriminalProfileDescriptiveRole) {
        this.isCriminalProfileDescriptiveRole = isCriminalProfileDescriptiveRole;
    }

    public void setCriminalProfileDescriptiveRoleUpdate(boolean isCriminalProfileDescriptiveRoleUpdate) {
        this.isCriminalProfileDescriptiveRoleUpdate = isCriminalProfileDescriptiveRoleUpdate;
    }

    public void setCriminalProfileCRS(boolean isCriminalProfileCRS) {
        this.isCriminalProfileCRS = isCriminalProfileCRS;
    }

    public void setCriminalProfileAssociates(boolean isCriminalProfileAssociates) {
        this.isCriminalProfileAssociates = isCriminalProfileAssociates;
    }

    public void setEditComment(boolean isEditComment) {
        this.isEditComment = isEditComment;
    }

    public void setDocumentDLSearch(boolean isDocumentDLSearch) {
        this.isDocumentDLSearch = isDocumentDLSearch;
    }

    public void setDocumentDLSearchPaginarion(boolean isDocumentDLSearchPagination) {
        this.isDocumentDLSearchPagination = isDocumentDLSearchPagination;
    }

    public void setVehicleSearch(boolean isVehicleSearch) {
        this.isVehicleSearch = isVehicleSearch;
    }

    public void setDocumentVehicleSearchPaginarion(boolean isDocumentVehicleSearchPagination) {
        this.isDocumentVehicleSearchPagination = isDocumentVehicleSearchPagination;
    }

    public void setGasContent(boolean isGetGasContent) {
        this.isGetGasContent = isGetGasContent;
    }

    public void setGasSearch(boolean isGasSearch) {
        this.isGasSearch = isGasSearch;
    }

    public void setKMCSearch(boolean isKMCSearch) {
        this.isKMCSearch = isKMCSearch;
    }

    public void setKMCContent(boolean isGetKMCContent) {
        this.isGetKMCContent = isGetKMCContent;
    }

    public void setCableContent(boolean isGetCableContent) {
        this.isGetCableContent = isGetCableContent;
    }

    public void setCableSearch(boolean isCableSearch) {
        this.isCableSearch = isCableSearch;
    }

    public void setSDRSearch(boolean isSDRSearch) {
        this.isSDRSearch = isSDRSearch;
    }public void setSDRSearchAllState(boolean isSDRSearchAllState) {
        this.isSDRSearchAllState = isSDRSearchAllState;
    }

    public void setBARSearch(boolean isBARSearch) {
        this.isBARSearch = isBARSearch;
    }

    public void setHotelSearch(boolean isHotelSearch) {
        this.isHotelSearch = isHotelSearch;
    }

    public void setDocumentAllDataSearch(boolean isDocumentAllDataSearch) {
        this.isDocumentAllDataSearch = isDocumentAllDataSearch;
    }

    public void setDLAllDataRelationalSearch(boolean isDLAllDataRelationalSearch) {
        this.isDLAllDataRelationalSearch = isDLAllDataRelationalSearch;
    }

    public void setVehicleAllDataRelationalSearch(boolean isVehicleAllDataRelationalSearch) {
        this.isVehicleAllDataRelationalSearch = isVehicleAllDataRelationalSearch;
    }

    public void setSDRAllDataRelationalSearch(boolean isSDRAllDataRelationalSearch) {
        this.isSDRAllDataRelationalSearch = isSDRAllDataRelationalSearch;
    }

    public void setCableAllDataRelationalSearch(boolean isCableAllDataRelationalSearch) {
        this.isCableAllDataRelationalSearch = isCableAllDataRelationalSearch;
    }

    public void setKMCAllDataRelationalSearch(boolean isKMCAllDataRelationalSearch) {
        this.isKMCAllDataRelationalSearch = isKMCAllDataRelationalSearch;
    }

    public void setGasAllDataRelationalSearch(boolean isGasAllDataRelationalSearch) {
        this.isGasAllDataRelationalSearch = isGasAllDataRelationalSearch;
    }


    public void setExistingHotelList(boolean isEistingHotelList) {
        this.isEistingHotelList = isEistingHotelList;
    }


    public void setSDRSearchPagination(boolean isSDRSearchPaging) {
        this.isSDRSearchPaging = isSDRSearchPaging;
    }
    public void setBARSearchPagination(boolean isBARSearchPaging) {
        this.isBARSearchPaging = isBARSearchPaging;
    }

    public void setHotelSearchPagination(boolean isHotelSearchPaging) {
        this.isHotelSearchPaging = isHotelSearchPaging;
    }

    public void setDocumentAllDataSearchPagination(boolean isDocumentAllDataSearchPaging) {
        this.isDocumentAllDataSearchPaging = isDocumentAllDataSearchPaging;
    }


    public void setUserForgotPassword(boolean isUserForgotPassword) {
        this.isUserForgotPassword = isUserForgotPassword;
    }

    public void setCaseSearchEditComment(boolean isCaseSearchEditComment) {
        this.isCaseSearchEditComment = isCaseSearchEditComment;
    }

    public void setMobileStolenSearch(boolean isMobileStolenSearch) {
        this.isMobileStolenSearch = isMobileStolenSearch;
    }

    public void setMobileStolenSearchPagination(boolean isMobileStolenSearchPaging) {
        this.isMobileStolenSearchPaging = isMobileStolenSearchPaging;
    }

    public void setMobileStolenStatus(boolean isMobileStolenStatus) {
        this.isMobileStolenStatus = isMobileStolenStatus;
    }

    public void setDocumentGasSearch(boolean isDocumentGasSearch) {
        this.isDocumentGasSearch = isDocumentGasSearch;
    }

    public void setDocumentGasSearchPaginarion(boolean isDocumentGasSearchPagination) {
        this.isDocumentGasSearchPagination = isDocumentGasSearchPagination;
    }

    public void setDocumentKMCSearch(boolean isDocumentKMCSearch) {
        this.isDocumentKMCSearch = isDocumentKMCSearch;
    }

    public void setDocumentKMCSearchPagination(boolean isDocumentKMCSearchPagination) {
        this.isDocumentKMCSearchPagination = isDocumentKMCSearchPagination;
    }

    public void setDocumentCableSearch(boolean isDocumentCableSearch) {
        this.isDocumentCableSearch = isDocumentCableSearch;
    }

    public void setDocumentCableSearchPagination(boolean isDocumentCableSearchPagination) {
        this.isDocumentCableSearchPagination = isDocumentCableSearchPagination;
    }

    public void setPMReportListData(boolean isPMReportListData) {
        this.isPMReportListData = isPMReportListData;
    }

    public void setPMReportSearch(boolean isPMReportSearch) {
        this.isPMReportSearch = isPMReportSearch;
    }

    public void setPMReportSearchPagination(boolean isPMReportSearchPagination) {
        this.isPMReportSearchPagination = isPMReportSearchPagination;
    }

    public void setCourtCaseListData(boolean isCourtCaseListData) {
        this.isCourtCaseListData = isCourtCaseListData;
    }

    public void setCourtCaseListSearch(boolean isCourtCaseSearch) {
        this.isCourtCaseSearch = isCourtCaseSearch;
    }

    public void setCourtCaseSearchPagination(boolean isCourtCaseSearchPaging) {
        this.isCourtCaseSearchPaging = isCourtCaseSearchPaging;
    }

    public void setCaseFIRLogList(boolean isCaseFirLogList) {
        this.isCaseFirLogList = isCaseFirLogList;
    }

    public void setCriminalFIRLogList(boolean isCriminalFirLogList) {
        this.isCriminalFirLogList = isCriminalFirLogList;
    }

    public void setInvestigateUnit(boolean isInvestigateUnitStatus) {
        this.isInvestigateUnitStatus = isInvestigateUnitStatus;
    }

    public void setInvestigateSecSearch(boolean isInvestigateSec) {
        this.isInvestigateSec = isInvestigateSec;
    }

    public void setTempCapturedFIR(boolean isTempFirCapture) {
        this.isTempFirCapture = isTempFirCapture;
    }
    public void setMiscount(boolean isMisCount) {
        this.isMisCount = isMisCount;
    }
    public void setOnlineUser(boolean isOnlineUser){
        this.isOnlineUser = isOnlineUser;
    }
    public void setActiveUser(boolean isActiveUser){
        this.isActiveUser = isActiveUser;
    }
    public void setFeedbackMenu(boolean isFeedback) {
        this.isFeedback = isFeedback;
    }
    public void setCurrentVer(boolean isCurrentVer) {
        this.isCurrentVer = isCurrentVer;
    }

    public void setGeoCode(boolean isGeoCode) {
        this.isGeoCode = isGeoCode;
    }

    public void setDashBoard(boolean isDashboard) {
        this.isDashboard = isDashboard;
    }

    public void setPreference(boolean isPreference) {
        this.isPreference = isPreference;
    }

    public void setCrimeReview(boolean isCrimeReview) {
        this.isCrimeReview = isCrimeReview;
    }

    public void setCrimeReviewItemSearch(boolean isCrimeReviewSearch) {
        this.isCrimeReviewSearch = isCrimeReviewSearch;
    }

    public void setCrimeReciewToCaseSearch(boolean isCrimeReviewToCaseSearchList) {
        this.isCrimeReviewToCaseSearchList = isCrimeReviewToCaseSearchList;
    }

    public void setAllFirViewByCategory(boolean isAllFirView) {
        this.isAllFirView = isAllFirView;
    }

    public void setFIRPSItemSearch(boolean isFIRAllPSFromCategory) {
        this.isFIRAllPSFromCategory = isFIRAllPSFromCategory;
    }

    public void setFIRAllViewToCaseSearch(boolean isAllFirViewForCase) {
        this.isAllFirViewForCase = isAllFirViewForCase;
    }

    public void setAllRTAView(boolean isAllRtaView) {
        this.isAllRtaView = isAllRtaView;
    }

    public void setMapViewFromRTA(boolean isMapViewCaseFromRTA) {
        this.isMapViewCaseFromRTA = isMapViewCaseFromRTA;
    }

    public void setAllSpecificArrestView(boolean isAllSpecificView) {
        this.isAllSpecificView = isAllSpecificView;
    }

    public void setAllSpecificArrestViewPagination(boolean isAllSpecificViewPagination) {
        this.isAllSpecificViewPagination = isAllSpecificViewPagination;
    }

    public void setArrestSpecificViewToCaseSearch(boolean isArrestSpecificViewForCase) {
        this.isArrestSpecificViewForCase = isArrestSpecificViewForCase;
    }

    public void setAllOtherArrestView(boolean isAllOtherView) {
        this.isAllOtherView = isAllOtherView;
    }

    public void setAllOtherArrestViewPagination(boolean isAllOtherViewPagination) {
        this.isAllOtherViewPagination = isAllOtherViewPagination;
    }

    public void setArrestOtherViewToCaseSearch(boolean isArrestOtherViewForCase) {
        this.isArrestOtherViewForCase = isArrestOtherViewForCase;
    }

    public void setDisposalsItemSearch(boolean isDisposalsView) {
        this.isDisposalsView = isDisposalsView;
    }

    public void setDisposalToCaseSearch(boolean isDisposalToCaseSearchList) {
        this.isDisposalToCaseSearchList = isDisposalToCaseSearchList;
    }

    public void setDisposalToCaseSearchPagination(boolean isDisposalToCaseSearchListPagination) {
        this.isDisposalToCaseSearchListPagination = isDisposalToCaseSearchListPagination;
    }

    public void setWaExecItemSearch(boolean isWaExecView) {
        this.isWaExecView = isWaExecView;
    }

    public void setWaExecDetailSearchForNBW(boolean isWaExecDetailViewNBW) {
        this.isWaExecDetailViewNBW = isWaExecDetailViewNBW;
    }

    public void setWaExecDetailSearchForBW(boolean isWaExecDetailViewBW) {
        this.isWaExecDetailViewBW = isWaExecDetailViewBW;
    }

    public void setWaRecvItemSearch(boolean isWaRecvView) {
        this.isWaRecvView = isWaRecvView;
    }

    public void setWaFallDueItemSearch(boolean isWaFView) {
        this.isWaFallDueView = isWaFView;
    }

    public void setWaRecvDetailSearchForNBW(boolean isWaRecvDetailViewNBW) {
        this.isWaRecvDetailViewNBW = isWaRecvDetailViewNBW;
    }

    public void setWaFallDueDetailSearchForNBW(boolean isWaFallDueDetailViewNBW) {
        this.isWaFallDueDetailViewNBW = isWaFallDueDetailViewNBW;
    }

    public void setWaRecvDetailSearchForBW(boolean isWaRecvDetailViewBW) {
        this.isWaRecvDetailViewBW = isWaRecvDetailViewBW;
    }

    public void setWaFallDueDetailSearchForBW(boolean isWaFallDueDetailViewBW) {
        this.isWaFallDueDetailViewBW = isWaFallDueDetailViewBW;
    }

    public void setWaPendingItemSearch(boolean isWaPendingView) {
        this.isWaPendingView = isWaPendingView;
    }

    public void setWaPendingDetailSearchForNBW(boolean isWaPendingDetailViewNBW) {
        this.isWaPendingDetailViewNBW = isWaPendingDetailViewNBW;
    }

    public void setWaPendingDetailSearchForBW(boolean isWaPendingDetailViewBW) {
        this.isWaPendingDetailViewBW = isWaPendingDetailViewBW;
    }

    public void setLogListCapturedFIR(boolean isListCapture) {
        this.isListCapture = isListCapture;
    }

    public void setLogListCapturedFIRPagination(boolean isListCapturePagination) {
        this.isListCapturePagination = isListCapturePagination;
    }

    public void setCapturedTagAdd(boolean isTagAdd) {
        this.isTagAdd = isTagAdd;
    }

    public void setCapturedUnTagRecord(boolean isTagUnTag) {
        this.isTagUnTag = isTagUnTag;
    }

    public void setDeleteTempCaptureFIR(boolean isDeleteCaptureFIR) {
        this.isDeleteCaptureFIR = isDeleteCaptureFIR;
    }

    public void setUnnaturalDeath(boolean isUnnaturalDeath) {
        this.isUnnaturalDeath = isUnnaturalDeath;
    }

    public void setUnnaturalDeathDetails(boolean isUnnaturalDeathDetails) {
        this.isUnnaturalDeathDetails = isUnnaturalDeathDetails;
    }

    public void setChargesheetDuePs(boolean isChargesheetDuePs) {
        this.isChargesheetDuePs = isChargesheetDuePs;
    }

    //isChargesheetDueCS
    public void setChargesheetDueCS(boolean isChargesheetDueCS) {
        this.isChargesheetDueCS = isChargesheetDueCS;
    }

    public void setSessionForDocumentSearch(boolean isDocumentSearchAunthicated) {
        this.isDocumentSearchAunthicated = isDocumentSearchAunthicated;
    }

    public void setSessionForAllSearch(boolean isAllSearchAunthicated) {
        this.isAllSearchAunthicated = isAllSearchAunthicated;
    }
    public void setSessionForBarSearch(boolean isBarSearchAunthicated) {
        this.isBarSearchAunthicated = isBarSearchAunthicated;
    }
    public void setSessionForSDRSearch(boolean isSDRSearchAunthicated) {
        this.isSDRSearchAunthicated = isSDRSearchAunthicated;
    }
    //isSDRSearchAunthicated


    public void setSessionForDocumentHotelSearch(boolean isDocumentHotelSearchAunthicated) {
        this.isDocumentHotelSearchAunthicated = isDocumentHotelSearchAunthicated;
    }

    public void setSpecilService(boolean isSpecialServiceSaved) {
        this.isSpecialServiceSaved = isSpecialServiceSaved;
    }

    public void setSpecilServiceUpdate(boolean isSpecialServiceItemUpdated) {
        this.isSpecialServiceItemUpdated = isSpecialServiceItemUpdated;
    }  public void setSpecilServiceSkipped(boolean isSpecialServiceItemSkipped) {
        this.isSpecialServiceItemSkipped = isSpecialServiceItemSkipped;
    }


    public void setSpecilServiceListData(boolean isSpecialServiceListData) {
        this.isSpecialServiceListData = isSpecialServiceListData;
    }  public void setSpecilServiceAllowUserList(boolean isSpecialServiceAllowUserListData) {
        this.isSpecialServiceAllowUserListData = isSpecialServiceAllowUserListData;
    }
    //isSpecialServiceAllowUserListData

    public void setSpecilServicePasscode(boolean isSpecialServicePasscode) {
        this.isSpecialServicePasscode = isSpecialServicePasscode;
    }

    public void setSpecialServiceLoggerInfo(boolean isSpecialServiceLoggerInfo) {
        this.isSpecialServiceLoggerInfo = isSpecialServiceLoggerInfo;
    }

    public void setSpecialServiceLISDRInfo(boolean isSpecialServiceLISDRinfo, View addLIView) {
        this.isSpecialServiceLISDRinfo = isSpecialServiceLISDRinfo;
        addLI = addLIView;
    }
    public void setSpecialServiceAllSDRinfo(boolean isSpecialServiceAllSDRinfo, View addAllRequest) {
        this.isSpecialServiceAllSDRinfo = isSpecialServiceAllSDRinfo;
        this.addAllRequest = addAllRequest;
    }
    public void setLIReconnectionJustification(boolean isLIReconnectionJustification) {
        this.isLIReconnectionJustification = isLIReconnectionJustification;

    }
    public void setLIReconnectionCreate(boolean isLIReconnectionCreate) {
        this.isLIReconnectionCreate = isLIReconnectionCreate;
    }
    public void setSpecialServiceListSearch(boolean isSpecialServiceListSearch) {
        this.isSpecialServiceListSearch = isSpecialServiceListSearch;
    }
    public void setReminderToMcell(boolean isReminderToMcell) {
        this.isReminderToMcell = isReminderToMcell;
    }
    public void setLIDisconnectionCreate(boolean isLIDisconnectionCreate) {
        this.isLIDisconnectionCreate = isLIDisconnectionCreate;
    }
    public void setSpecialServiceItemSkiped(boolean isSpecialServiceItemSkiped) {
        this.isSpecialServiceItemSkiped = isSpecialServiceItemSkiped;
    }
    public void setLISearchAllState(boolean isLISearchAllState) {
        this.isLISearchAllState = isLISearchAllState;
    }public void checkBlackListed(boolean isBlackListed,View addLIBlackList) {
        this.isBlackListed = isBlackListed;
        this.addLIBlackList = addLIBlackList;
    }
    public void setRequetRevoked(boolean isRequestRevoked) {
        this.isRequestRevoked = isRequestRevoked;
    }
    public void setSpecialServiceRequestProcessType(boolean isSpecialServiceRequestProcessType) {
        this.isSpecialServiceRequestProcessType = isSpecialServiceRequestProcessType;
    }
    public void setSpecilServiceSearchListData(boolean isSpecialServiceSearchListData){
        this.isSpecialServiceSearchListData=isSpecialServiceSearchListData;
    }
    public void setSpecialServiceLIDetailsStatusChange(boolean isSpecialServiceLIDetailsStatusChange){
        this.isSpecialServiceLIDetailsStatusChange=isSpecialServiceLIDetailsStatusChange;
    }
    public void setLIPartialSavedRecord(boolean isLIPartialSaved){
        this.isLIPartialSaved = isLIPartialSaved;
    }
    public void setLINodalSuggestion(boolean isNodalSuggestionSaved){
        this.isNodalSuggestionSaved = isNodalSuggestionSaved;
    }
    public void setLINodalSuggestionOnLoadingDetails(boolean isNodalSuggestionOnLoading){
        this.isNodalSuggestionOnLoading=isNodalSuggestionOnLoading;
    }

     public void setLIMcellObservationDetails(boolean isMcellSuggestionOnLoading){
        this.isMcellSuggestionOnLoading=isMcellSuggestionOnLoading;
    }

    //isMcellSuggestionOnLoading

    //isNodalSuggestionOnLoading
    //isNodalSuggestionSaved
    //isSpecialServiceLIDetailsStatusChange
    //isSpecialServiceRequestProcessType
    //isRequetRevoked
    //isBlackListed
   // isLISearchAllState
    //isSpecialServiceItemSkiped
    //isSpecialServiceListSearch
    //isLIReconnectionCreate
    //isSpecialServicePasscode
    //isSpecialServiceLoggerInfo


    public void doStartTask(String[] keys, String[] values, boolean isPost) {// any service call is or VSB or for MDB.If isVSBCall is false that means its am MDB call
        this.keys = keys;
        this.values = values;

        if (!isInternetConnected) {
            Utility.showAlertDialog(mContext, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }
        client = new RestClient(mContext, keys, values, this, methodname, "", isPost);
        client.execute();
    }

    public void doStartTask(String[] keys, String[] values, boolean isPost, boolean direct_url) {// any service call is or VSB or for MDB.If isVSBCall is false that means its am MDB call
        this.keys = keys;
        this.values = values;

        if (!isInternetConnected) {
            Utility.showAlertDialog(mContext, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        client = new RestClient(mContext, keys, values, this, methodname, "", isPost, false, direct_url);
        client.execute();
    }

    public void doStartTask(String[] keys, String[] values, String appendInServieceUrl, boolean isVSBCall, boolean isPost) {// any service call is or VSB or for MDB.If isVSBCall is false that means its am MDB call
        this.keys = keys;
        this.values = values;

        if (!isInternetConnected) {
            Utility.showAlertDialog(mContext, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        client = new RestClient(mContext, keys, values, this, methodname, appendInServieceUrl, isPost);
        client.execute();
    }

    public void doStartTask(boolean isUploadImage, String imagePath, Bitmap bitmap) {
        this.keys = null;
        this.values = null;

        if (!isInternetConnected) {
            Utility.showAlertDialog(mContext, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        client = new RestClient(mContext, this, isUploadImage, imagePath, bitmap);
        client.execute();
    }

    public void doStartTask(String[] keys, String[] values, boolean isUploadImage, boolean isUpdateImage, String imagePath, Bitmap bitmap) {
        this.keysImg = keys;
        this.valuesImg = values;

        if (!isInternetConnected) {
            Utility.showAlertDialog(mContext, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        client = new RestClient(mContext, keysImg, valuesImg, this, isUploadImage, isUpdateImage, imagePath, bitmap);
        client.execute();
    }


    @Override
    public void onSuccess(String response, String[] keys, String[] values) {
        Log.e("Response", "Response " + response);
        if (isLogin) {
            isLogin = false;
            ((LoginActivity) mCurrentActivity).parseLoginResponse(response);
        }else if (isCurrentVer)  {
            isCurrentVer = false;
            ((MisActiveUsersActivity)mCurrentActivity).parseCurrentVersion(response);
        }
        else if (isDetailedSearch) {
            isDetailedSearch = false;
            ((CriminalSearchFragment) fragment).parseSearchResult(response, keys, values);
        } else if (isDetailedSearchPaging) {
            isDetailedSearchPaging = false;
            ((SearchResultActivity) mCurrentActivity).parseSearchResultPaging(response);
        } else if (isUploadImage) {
            this.isUploadImage = false;
            ((ImageSearchFragment) fragment).parseUploadResult(response);
        } else if (isFetchImageDetail) {
            this.isFetchImageDetail = false;
            ((ImageSearchFragment) fragment).parseMatchedImageDetails(response);
        } else if (isForgetPassword) {
            this.isForgetPassword = false;
            ((LoginActivity) mCurrentActivity).parseForgetpasswordResponse(response);
        } else if (isSendPush) {
            this.isSendPush = false;
            ((SendPushActivity) fragment).parseNotificationResponse(response);
        } else if (isNotificationDetails) {
            this.isNotificationDetails = false;
            ((NotificationDetailActivity) mCurrentActivity).parseNotificationDetails(response);
        } else if (isLogout) {
            this.isLogout = false;
            ((BaseActivity) mCurrentActivity).parseLogoutResponse(response);
        } else if (isGetContent) {
            this.isGetContent = false;
            ((CriminalSearchFragment) fragment).parseGetAllContentResponse(response);
        } else if (isGetContentAllDocumentSearch) {
            this.isGetContentAllDocumentSearch = false;
            ((AllDocumentDataSearchFragment)fragment).parseGetAllContentResponse(response);
        }
        else if (isGetContentSpService) {
            this.isGetContentSpService = false;
            ((SpecialServiceActivity) mCurrentActivity).parseGetAllContentResponse(response);
        } else if (isSpecialServiceRequestAuthenticated) {
            this.isSpecialServiceRequestAuthenticated = false;
            ((SpecialServiceActivity) mCurrentActivity).parseSpecialServiceRequestResponse(response);
        } else if (isGetContentPref) {
            this.isGetContentPref = false;
            ((PreferenceActivity) mCurrentActivity).parseGetAllContentResponse(response);
        } else if (isIdentityCategory) {
            this.isIdentityCategory = false;
            ((CriminalSearchFragment) fragment).parseIdentityCategoryResponse(response);
        } else if (isIdentifySubCategory) {
            this.isIdentifySubCategory = false;
            ((CriminalSearchFragment) fragment).parseIdentitySubCategoryResponse(response);
        } else if (isIdentifySubCategoryAtSearchResultActivity) {
            this.isIdentifySubCategoryAtSearchResultActivity = false;
            ((SearchResultActivity) mCurrentActivity).parseIdentitySubCategoryResponse(response);
        } else if (isDivisionWisePoliceStation) {
            this.isDivisionWisePoliceStation = false;
            ((CaseSearchFragment) fragment).parseDivisionWisePoliceStationResponse(response);
        } else if (isCaseSearch) {
            this.isCaseSearch = false;
            ((CaseSearchFragment) fragment).parseCaseSearchResultResponse(response, keys, values);
        } else if (isCriminalDetailsArrest) {
            this.isCriminalDetailsArrest = false;
            ((ArrestFragment) fragment).parseArrestResultResponse(response);
        } else if (isCriminalDetailsWarrent) {
            this.isCriminalDetailsWarrent = false;
            ((WarrentFragment) fragment).parseWarrentResultResponse(response);
        } else if (isCriminalDetailsFIR) {
            this.isCriminalDetailsFIR = false;
            ((FIRFragment) fragment).parseFIRResultResponse(response);
        } else if (isProfileDetails) {
            isProfileDetails = false;
            ((ProfileFragment) fragment).parseProfileDetailsResult(response);
        } else if (isModifiedWarrentDetails) {
            isModifiedWarrentDetails = false;
            ((WarrentFragment) fragment).parseModifiedWarrentResultResponse(response);
        } else if (isModifiedArrestDetails) {
            isModifiedArrestDetails = false;
            ((ArrestFragment) fragment).parseModifiedArrestResultResponse(response);
        } else if (isModifiedFIRDetails) {
            isModifiedFIRDetails = false;
            ((FIRFragment) fragment).parseModifiedFIRResultResponse(response);
        } else if (isAdvanceCriminalSearch) {
            isAdvanceCriminalSearch = false;
            ((SearchResultActivity) mCurrentActivity).parseAdvanceSearchResult(response, keys, values);
        } else if (isCriminalProfileDescriptiveRole) {
            isCriminalProfileDescriptiveRole = false;
            ((ModifiedProfileFragment) fragment).parseCriminalDescriptiveRoleResponse(response);
        } else if (isCriminalProfileDescriptiveRoleUpdate) {
            isCriminalProfileDescriptiveRoleUpdate = false;
            ((ModifiedProfileFragment) fragment).parseCriminalDescriptiveRoleResponseUpdate(response);
        } else if (isCriminalProfileCRS) {
            isCriminalProfileCRS = false;
            ((ModifiedProfileFragment) fragment).parseCriminalCRS(response);
        } else if (isCriminalProfileAssociates) {
            isCriminalProfileAssociates = false;
            ((ModifiedProfileFragment) fragment).parseCriminalAssociates(response);
        } else if (isCriminalModifiedFIRDetails) {
            isCriminalModifiedFIRDetails = false;
            ((ModifiedFIRFragment) fragment).parseCriminalModifiedFIRDetailsResponse(response);
        } else if (isCriminalModifiedArrestDetails) {
            isCriminalModifiedArrestDetails = false;
            ((ModifiedArrestFragment) fragment).parseCriminalModifiedArrestDetailsResponse(response);
        } else if (isCriminalModifiedWarrantDetails) {
            isCriminalModifiedWarrantDetails = false;
            ((ModifiedWarrantFragment) fragment).parseCriminalModifiedWarrantDetailsResponse(response);
        } else if (isModifiedCaseSearch) {
            isModifiedCaseSearch = false;
            ((ModifiedCaseSearchFragment) fragment).parseModifiedCaseSearchResult(response, keys, values);
        } else if (isModifiedCaseSearchPagination) {
            isModifiedCaseSearchPagination = false;
            ((ModifiedCaseSearchResultActivity) mCurrentActivity).parseModifiedCaseSearchResultPagination(response, keys, values);
        } else if (isModifiedCaseSearchFIRDetails) {
            isModifiedCaseSearchFIRDetails = false;
            ((CaseSearchFIRDetailsActivity) mCurrentActivity).parseModifiedCaseSearchFIRDetailsResult(response);
        } else if (isWarrarntSearch) {
            isWarrarntSearch = false;
            ((WarrantSearchFragment) fragment).parseWarrantSearchResultResponse(response, keys, values);
        } else if (isWarrarntSearchPaging) {
            isWarrarntSearchPaging = false;
            ((WarrantSearchResultActivity) mCurrentActivity).parseWarrantSearchResultResponsePagination(response, keys, values);
        } else if (isWarrantSearchItemDeatils) {
            isWarrantSearchItemDeatils = false;
            ((WarrantSearchDetailsActivity) mCurrentActivity).parseWarrantSearchItemDetailsResult(response);
        } else if (isEditComment) {
            isEditComment = false;
            ((CriminalFIRDetailsFragment) fragment).parseEditCommentDataResponse(response);
        } else if (isDocumentDLSearch) {
            isDocumentDLSearch = false;
            ((DLSearchFragment) fragment).parseDocumentDLSearch(response, keys, values);
        } else if (isVehicleSearch) {
            isVehicleSearch = false;
            ((VehicleSearchFragment) fragment).parseVehicleSearch(response, keys, values);
        } else if (isGetGasContent) {
            isVehicleSearch = false;
            ((GasDataSearchFragment) fragment).parseGetGasContentResponse(response);
        } else if (isGasSearch) {
            isGasSearch = false;
            ((GasDataSearchFragment) fragment).parseGasSearch(response);
        } else if (isKMCSearch) {
            isKMCSearch = false;
            ((KMCDataSearchFragment) fragment).parseKMCSearch(response);
        } else if (isGetKMCContent) {
            isGetKMCContent = false;
            ((KMCDataSearchFragment) fragment).parseGetKMCContentResponse(response);
        } else if (isGetCableContent) {
            isGetCableContent = false;
            ((CableDataSearchFragment) fragment).parseGetCableContentResponse(response);
        } else if (isCableSearch) {
            isCableSearch = false;
            ((CableDataSearchFragment) fragment).parseCableSearch(response);
        } else if (isSDRSearch) {
            isSDRSearch = false;
//            ((SDRDataSearchFragment) fragment).parseSDRSearchResultResponse(response, keys, values);
            ((SDRSearchActivity) mCurrentActivity).parseSDRSearchResultResponse(response, keys, values);
        } else if (isSDRSearchAllState) {
            isSDRSearchAllState = false;
            ((SDRSearchActivity) mCurrentActivity).parseSDRSearchAllStateResponse(response);
        }
        else if (isMisCount){
            isMisCount = false;
            ((MisActivity)mCurrentActivity).ParseMisCount(response);
        }
        else if (isOnlineUser){
            isOnlineUser = false;
            ((MisOnlineUsersActivity)mCurrentActivity).ParseOnlineUser(response);
        }
        else if (isActiveUser){
            isActiveUser = false;
            ((MisActiveUsersActivity)mCurrentActivity).ParseActiveUser(response);
        }
            else if (isSDRSearchAllState) {
            isSDRSearchAllState = false;
            ((SDRDataSearchFragment) fragment).parseSDRSearchAllStateResponse(response);
        }else if (isBARSearch) {
            isBARSearch = false;
            //((BarSearchFragment) fragment).parseBARSearchResultResponse(response, keys, values);
            ((BarSearchActivity) mCurrentActivity).parseBARSearchResultResponse(response, keys, values);
        } else if (isHotelSearch) {
            isHotelSearch = false;
            //((HotelSearchFragment) fragment).parseHotelSearchResultResponse(response, keys, values);
            ((HotelSearchActivity) mCurrentActivity).parseHotelSearchResultResponse(response, keys, values);
        } else if (isDocumentAllDataSearch) {
            isDocumentAllDataSearch = false;
            ((AllDocumentDataSearchFragment) fragment).parseAllDocumentDataSearchResultResponse(response, keys, values);
        } else if (isDLAllDataRelationalSearch) {
            isDLAllDataRelationalSearch = false;
            ((GlobalDLSearchDetailsActivity) mCurrentActivity).parseDLAllDataRelationalSearchResultResponse(response, keys, values);
        } else if (isVehicleAllDataRelationalSearch) {
            isVehicleAllDataRelationalSearch = false;
            ((GlobalVehicleSearchDetailsActivity) mCurrentActivity).parseVehicleAllDataRelationalSearchResultResponse(response, keys, values);
        } else if (isSDRAllDataRelationalSearch) {
            isSDRAllDataRelationalSearch = false;
            ((GlobalSDRSearchDetailsActivity) mCurrentActivity).parseSDRAllDataRelationalSearchResultResponse(response, keys, values);
        } else if (isCableAllDataRelationalSearch) {
            isCableAllDataRelationalSearch = false;
            ((GlobalCableSearchDetailsActivity) mCurrentActivity).parseCableAllDataRelationalSearchResultResponse(response, keys, values);
        } else if (isKMCAllDataRelationalSearch) {
            isKMCAllDataRelationalSearch = false;
            ((GlobalKMCSearchDetailsActivity) mCurrentActivity).parseKMCAllDataRelationalSearchResultResponse(response, keys, values);
        } else if (isGasAllDataRelationalSearch) {
            isGasAllDataRelationalSearch = false;
            ((GlobalGasSearchDetailsActivity) mCurrentActivity).parseGasAllDataRelationalSearchResultResponse(response, keys, values);
        } else if (isEistingHotelList) {
            isEistingHotelList = false;
            ((HotelSearchFragment) fragment).parseHotelListResponse(response);
        } else if (isSDRSearchPaging) {
            isSDRSearchPaging = false;
            ((SDRSearchResultActivity) mCurrentActivity).parseSDRSearchResultPaginationResponse(response, keys, values);
        } else if (isBARSearchPaging) {
            isBARSearchPaging = false;
            ((BARSearchResultActivity) mCurrentActivity).parseBARSearchResultPaginationResponse(response, keys, values);
        } else if (isHotelSearchPaging) {
            isHotelSearchPaging = false;
            ((HotelSearchResultActivity) mCurrentActivity).parseHotelSearchResultPaginationResponse(response, keys, values);
        } else if (isDocumentAllDataSearchPaging) {
            isDocumentAllDataSearchPaging = false;
            ((AllDocumentDataSearchResultActivity) mCurrentActivity).parseAllDocumentDataSearchPaginationResponse(response, keys, values);
        } else if (isUserForgotPassword) {
            isUserForgotPassword = false;
            ((LoginActivity) mCurrentActivity).parseUserForgotPasswordResponse(response);
        } else if (isCaseSearchEditComment) {
            isCaseSearchEditComment = false;
            ((CaseSearchFIRDetailsActivity) mCurrentActivity).parseCaseSearchEditCommentDataResponse(response);
        } else if (isChangePassword) {
            isChangePassword = false;
            Utility.parseUserChangePasswordResponse(mContext, response);
        } else if (isPsWiseIOListForCaseSearch) {
            this.isPsWiseIOListForCaseSearch = false;
            ((ModifiedCaseSearchFragment) fragment).parsePSWiseIOForCaseSearchResponse(response);
        } else if (isDivWisePSListForCaseSearch) {
            this.isDivWisePSListForCaseSearch = false;
            ((ModifiedCaseSearchFragment) fragment).parseDivisionWisePoliceStationResponse(response);
        } else if (isDivWisePSListForWarrantSearch) {
            this.isDivWisePSListForWarrantSearch = false;
            ((WarrantSearchFragment) fragment).parseDivisionWisePoliceStationResponse(response);
        } else if (isPsWiseIOListForWarrant) {
            this.isPsWiseIOListForWarrant = false;
            ((WarrantSearchFragment) fragment).parsePSWiseIOForWarrantResponse(response);
        } else if (isDocumentDLSearchPagination) {
            this.isDocumentDLSearchPagination = false;
            ((DLSearchResultActivity) mCurrentActivity).parseDocumentDLSearchPagination(response, keys, values);
        } else if (isDocumentVehicleSearchPagination) {
            this.isDocumentVehicleSearchPagination = false;
            ((VehicleSearchResultActivity) mCurrentActivity).parseDocumentVehicleSearchPagination(response, keys, values);
        } else if (isMobileStolenSearch) {
            isMobileStolenSearch = false;
            ((MobileStolenFragment) fragment).parseMobileStolenSearchResultResponse(response, keys, values);
        } else if (isMobileStolenSearchPaging) {
            this.isMobileStolenSearchPaging = false;
            ((MobileStolenSearchResultActivity) mCurrentActivity).parseMobileStolenSearchPagination(response, keys, values);
        } else if (isMobileStolenStatus) {
            this.isMobileStolenStatus = false;
            ((MobileStolenFragment) fragment).parseGetMobileStolenStatusResponse(response);
        } else if (isDocumentGasSearch) {
            isDocumentGasSearch = false;
            ((ModifiedGasDataSearchFragment) fragment).parseDocumentGasSearch(response, keys, values);
        } else if (isDocumentGasSearchPagination) {
            this.isDocumentGasSearchPagination = false;
            ((GasSearchResultActivity) mCurrentActivity).parseDocumentGasSearchPagination(response, keys, values);
        } else if (isDocumentKMCSearch) {
            isDocumentKMCSearch = false;
            ((ModifiedKMCDataSearchFragment) fragment).parseDocumentKMCSearch(response, keys, values);
        } else if (isDocumentKMCSearchPagination) {
            this.isDocumentKMCSearchPagination = false;
            ((KMCSearchResultActivity) mCurrentActivity).parseDocumentKMCSearchPagination(response, keys, values);
        } else if (isDocumentCableSearch) {
            isDocumentCableSearch = false;
            ((ModifiedCableDataSearchFragment) fragment).parseDocumentCableSearch(response, keys, values);
        } else if (isDocumentCableSearchPagination) {
            this.isDocumentCableSearchPagination = false;
            ((CableSearchResultActivity) mCurrentActivity).parseDocumentCableSearchPagination(response, keys, values);
        } else if (isPMReportListData) {
            this.isPMReportListData = false;
            ((PMReportFragment) fragment).parseGetPMReportListDataResponse(response);
        } else if (isPMReportSearch) {
            this.isPMReportSearch = false;
            ((PMReportFragment) fragment).parsePMReportSearchResultResponse(response, keys, values);
        } else if (isPMReportSearchPagination) {
            this.isPMReportSearchPagination = false;
            ((PMReportSearchResultActivity) mCurrentActivity).parsePMReportSearchPagination(response, keys, values);
        } else if (isGetSubClass) {
            this.isGetSubClass = false;
            ((CriminalSearchFragment) fragment).parseSubClassResponse(response);
        } else if (isMapSearch) {
            isMapSearch = false;
            ((MapSerachFragment) fragment).parseMapSearchResult(response, keys, values);
        } else if (isCourtCaseListData) {
            this.isCourtCaseListData = false;
            ((HighCourtSearchFragment) fragment).parseGetCourtCaseListDataResponse(response);
        } else if (isCourtCaseSearch) {
            isCourtCaseSearch = false;
            ((HighCourtSearchFragment) fragment).parseCourtCaseSearchResultResponse(response, keys, values);
        } else if (isCourtCaseSearchPaging) {
            this.isCourtCaseSearchPaging = false;
            ((HighCourtCaseSearchResultActivity) mCurrentActivity).parseCourtCaseSearchPagination(response, keys, values);
        } else if (isMapSearchPagination) {
            this.isMapSearchPagination = false;
            ((MapSearchResultActivity) mCurrentActivity).parseMapSearchPagination(response, keys, values);
        } else if (isMapView) {
            this.isMapView = false;
            ((MapSearchResultActivity) mCurrentActivity).parseMapViewAll(response, keys, values);
        } else if (isMapViewPagination) {
            this.isMapViewPagination = false;
            ((MapSearchDetailsActivity) mCurrentActivity).parseMapViewAllPagination(response, keys, values);
        } else if (isMapViewCase) {
            this.isMapViewCase = false;
            ((ModifiedCaseSearchResultActivity) mCurrentActivity).parseMapViewCase(response, keys, values);
        } else if (isMapViewCasePagination) {
            this.isMapViewCasePagination = false;
            ((CaseMapSearchDetailsActivity) mCurrentActivity).parseMapViewCasePagination(response, keys, values);
        } else if (isSubClassAtSearchResultActivity) {
            this.isSubClassAtSearchResultActivity = false;
            ((SearchResultActivity) mCurrentActivity).parseSubClassResponse(response);
        } else if (isCaptureLatLng) {
            this.isCaptureLatLng = false;
            ((CaseSearchFIRDetailsActivity) mCurrentActivity).parseCapturedLatLong(response, keys, values);
        } else if (isCaptureLatLngList) {
            this.isCaptureLatLngList = false;
            ((ModifiedCaseSearchResultActivity) mCurrentActivity).parseCapturedLatLong(response, keys, values);
        } else if (isCaptureLatLngCriminal) {
            isCaptureLatLngCriminal = false;
            ((CriminalFIRDetailsFragment) fragment).parseCapturedLatLongOFModifiedFIRDetailsResponse(response);
        } else if (isCaptureLatLngCF) {
            isCaptureLatLngCF = false;
            ((CriminalFIRDetailsFragment) fragment).parseCapturedLatLongCFModifiedFIRDetailsResponse(response);
        } else if (isCaseFirLogList) {
            isCaseFirLogList = false;
            ((CaseSearchFIRDetailsActivity) mCurrentActivity).parseCaseSearchFIRLogListResponse(response, keys, values);
        }/*else if (isCriminalFirLogList) {
            isCriminalFirLogList = false;
			((ModifiedFIRFragment) fragment).parseCriminalFIRLogListResponse(response);
		}*/ else if (isFirDownloadLog) {
            isFirDownloadLog = false;
            ((PDFLoadActivity) mCurrentActivity).parseFirDownloadLog(response);
        } else if (isCriminalFirLogList) {
            isCriminalFirLogList = false;
            ((CriminalFIRDetailsFragment) fragment).parseCriminalFIRLogListResponse(response);
        } else if (isInvestigateUnitStatus) {
            this.isInvestigateUnitStatus = false;
            ((ModifiedCaseSearchFragment) fragment).parseGetInvestigateUnitResponse(response);
        } else if (isInvestigateSec) {
            this.isInvestigateSec = false;
            ((ModifiedCaseSearchFragment) fragment).parseInvestigateSectionResponse(response);
        } else if (isTempFirCapture) {
            this.isTempFirCapture = false;
            ((CaptureFIRFragment) fragment).parseCaptureFIRResponse(response);
        } else if (isFeedback) {
            this.isFeedback = false;
            ((FeedbackActivity) mCurrentActivity).parseFeedbackResponse(response);
        } else if (isCriminalPicUpdate) {
            this.isCriminalPicUpdate = false;
            ((CriminalDetailActivity) mCurrentActivity).parseCriminalPicUpdateResponse(response);
        } else if (isGeoCode) {
            this.isGeoCode = false;
            ((MapActivity) mCurrentActivity).parseGeoCodeResponse(response);
        } else if (isDashboard) {
            this.isDashboard = false;
            ((DashboardActivity) mCurrentActivity).parseDashboardResponse(response);
        } else if (isCrimeReview) {
            this.isCrimeReview = false;
            ((CrimeReviewActivity) mCurrentActivity).parseCrimeReviewResponse(response);
        } else if (isCrimeReviewSearch) {
            isCrimeReviewSearch = false;
            ((CrimeReviewDetailsActivity) mCurrentActivity).parseCrimeReviewCaseSearchResult(response, keys, values);
        } else if (isAllFirView) {
            isAllFirView = false;
            ((FIRTabFromDashboardActivity) mCurrentActivity).parseAllFirViewSearchResult(response);
        } else if (isAllRtaView) {
            isAllRtaView = false;
            ((RTATabFromDashboardActivity) mCurrentActivity).parseAllRTAViewSearchResult(response);
        } else if (isMapViewCaseFromRTA) {
            isMapViewCaseFromRTA = false;
            ((RTATabFromDashboardActivity) mCurrentActivity).parseAllRTAViewMapViewResult(response, keys, values);
        } else if (isAllSpecificView) {
            isAllSpecificView = false;
            ((ArrestTabFromDashboardActivity) mCurrentActivity).parseAllSpecificArrestViewResult(response, keys, values);
        } else if (isAllSpecificViewPagination) {
            this.isAllSpecificViewPagination = false;
            ((DashboardArrestSpecificFragment) fragment).parseArrestSpecificSearchPagination(response);
        } else if (isAllOtherView) {
            isAllOtherView = false;
            ((ArrestTabFromDashboardActivity) mCurrentActivity).parseAllOtherArrestViewResult(response);
        } else if (isAllOtherViewPagination) {
            this.isAllOtherViewPagination = false;
            ((DashboardArrestOthersFragment) fragment).parseArrestOtherSearchPagination(response);
        } else if (isCrimeReviewToCaseSearchList) {
            isCrimeReviewToCaseSearchList = false;
            ((CrimeReviewDetailsActivity) mCurrentActivity).parseCrimeReviewToCaseSearchResult(response, keys, values);
        } else if (isFIRAllPSFromCategory) {
            isFIRAllPSFromCategory = false;
            ((FIRPSListActivity) mCurrentActivity).parseFIRCategoryToPSResult(response, keys, values);
        } else if (isAllFirViewForCase) {
            isAllFirViewForCase = false;
            ((FIRPSListActivity) mCurrentActivity).parseFIRToCaseSearchResult(response, keys, values);
        } else if (isDisposalsView) {
            isDisposalsView = false;
            ((DisposalTabFromDashboardActivity) mCurrentActivity).parseDisposalsPSResult(response, keys, values);
        } else if (isDisposalToCaseSearchList) {
            isDisposalToCaseSearchList = false;
            ((DisposalTabFromDashboardActivity) mCurrentActivity).parseDisposalsToCaseSearchResult(response, keys, values);
        } else if (isDisposalToCaseSearchListPagination) {
            isDisposalToCaseSearchListPagination = false;
            ((DisposalCaseSearchResultActivity) mCurrentActivity).parseDisposalsToCaseSearchResultPagination(response, keys, values);
        } else if (isWaExecView) {
            isWaExecView = false;
            ((WarrantExcTabFromDashboardActivity) mCurrentActivity).parseWarrantExecPSResult(response, keys, values);
        } else if (isWaRecvView) {
            isWaRecvView = false;
            ((WarrantRecvTabFromDashboardActivity) mCurrentActivity).parseWarrantRecvPSResult(response, keys, values);
        } else if (isWaFallDueView) {
            isWaFallDueView = false;
            ((WarrentFallingDuePsActivity) mCurrentActivity).parseWarrantRecvPSResult(response, keys, values);
        } else if (isWaExecDetailViewNBW) {
            isWaExecDetailViewNBW = false;
            ((WarrantExcDetailsActivityNew) mCurrentActivity).parseWarrantExecDetailsResultForNBW(response, keys, values);
        } else if (isWaExecDetailViewBW) {
            isWaExecDetailViewBW = false;
            ((WarrantExcDetailsActivityNew) mCurrentActivity).parseWarrantExecDetailsResultForBW(response, keys, values);
        } else if (isWaRecvDetailViewNBW) {
            isWaRecvDetailViewNBW = false;
            ((WarrantRecvDetailsActivityNew) mCurrentActivity).parseWarrantRecvDetailsResultForNBW(response);
        } else if (isWaFallDueDetailViewNBW) {
            isWaFallDueDetailViewNBW = false;
            ((WarrentFallingDueDetailActivityNew) mCurrentActivity).parseWarrantRecvDetailsResultForNBW(response);
        } else if (isWaRecvDetailViewBW) {
            isWaRecvDetailViewBW = false;
            ((WarrantRecvDetailsActivityNew) mCurrentActivity).parseWarrantRecvDetailsResultForBW(response);
        } else if (isWaFallDueDetailViewBW) {
            isWaFallDueDetailViewBW = false;
            ((WarrentFallingDueDetailActivityNew) mCurrentActivity).parseWarrantRecvDetailsResultForBW(response);
        } else if (isArrestSpecificViewForCase) {
            isArrestSpecificViewForCase = false;
            ((DashboardArrestSpecificFragment) fragment).parseArrestSpecificDetailsForCase(response);
        } else if (isArrestOtherViewForCase) {
            isArrestOtherViewForCase = false;
            ((DashboardArrestOthersFragment) fragment).parseArrestOtherDetailsForCase(response);
        } else if (isMapViewCaseForDisposal) {
            this.isMapViewCaseForDisposal = false;
            ((DisposalCaseSearchResultActivity) mCurrentActivity).parseMapViewCaseForDisposal(response, keys, values);
        } else if (isMapViewCaseForDisposalPagination) {
            this.isMapViewCaseForDisposalPagination = false;
            ((DisposalMapSearchDetailsActivity) mCurrentActivity).parseMapViewCaseForDisposalPagination(response, keys, values);
        } else if (isWaPendingView) {
            isWaPendingView = false;
            ((WarrantPendingTabFromDashboardActivity) mCurrentActivity).parseWarrantPendingPSResult(response, keys, values);
        } else if (isWaPendingDetailViewNBW) {
            isWaPendingDetailViewNBW = false;
            ((WarrantPendingDetailsActivity) mCurrentActivity).parseWarrantPendingResultForNBW(response, keys, values);
        } else if (isWaPendingDetailViewBW) {
            isWaPendingDetailViewBW = false;
            ((WarrantPendingDetailsActivity) mCurrentActivity).parseWarrantPendingResultForBW(response, keys, values);
        } else if (isListCapture) {
            this.isListCapture = false;
            ((CaptureFIRFragment) fragment).parseLogListCaptureResponse(response);
        } else if (isTagAdd) {
            this.isTagAdd = false;
            ((DetailsForCaptureFIRFragment) fragment).parseCaptureLogTaggedResponse(response);
        } else if (isGetContentCapture) {
            this.isGetContentCapture = false;
            ((CaptureFIRFragment) fragment).parseGetAllContentInCaptureFragmentResponse(response);
        } else if (isListCapturePagination) {
            this.isListCapturePagination = false;
            ((CaptureLogListActivity) mCurrentActivity).parseLogListCapturePaginationResponse(response);
        } else if (isTagUnTag) {
            this.isTagUnTag = false;
            ((CaptureLogListActivity) mCurrentActivity).parseCaptureLogUnTagResponse(response);
        } else if (isDeleteCaptureFIR) {
            this.isDeleteCaptureFIR = false;
            ((CaptureLogListActivity) mCurrentActivity).parseCaptureTempLogDeleteResponse(response);
        } else if (isUnnaturalDeath) {
            this.isUnnaturalDeath = false;
            ((UnnaturalDeathListActivity) mCurrentActivity).parseUnnaturalDeathResponse(response);
        } else if (isUnnaturalDeathDetails) {
            this.isUnnaturalDeathDetails = false;
            ((UnnaturalDeathDetailsActivity) mCurrentActivity).parseUnnaturalDeathDetailsResponse(response);
        } else if (isChargesheetDuePs) {
            this.isChargesheetDuePs = false;
            ((ChargesheetDuePsListActivity) mCurrentActivity).parseChargeSheetDuePsResult(response);
        } else if (isChargesheetDueCS) {
            this.isChargesheetDueCS = false;
            ((ChargesheetDueCaseListActivity) mCurrentActivity).parseChargeSheetDueCaseResult(response);
        } else if (isDocumentSearchAunthicated) {
            this.isDocumentSearchAunthicated = false;
            ((After_login) mCurrentActivity).parseDocumentSearchAunthicatedResult(response);
//            ((DashboardActivity) mCurrentActivity).parseDocumentSearchAunthicatedResultDashboard(response);
        } else if (isDocumentSearchAunthicated) {
            this.isDocumentSearchAunthicated = false;
//            ((After_login) mCurrentActivity).parseDocumentSearchAunthicatedResult(response);
            ((DashboardActivity) mCurrentActivity).parseDocumentSearchAunthicatedResultDashboard(response);
        } else if (isAllSearchAunthicated) {
            this.isAllSearchAunthicated = false;
            ((After_login) mCurrentActivity).parseAllSearchAunthicatedResult(response);
        } else if (isAllSearchAunthicated) {
            this.isAllSearchAunthicated = false;
            ((DashboardActivity) mCurrentActivity).parseAllSearchAunthicatedResultDash(response);
        }else if (isBarSearchAunthicated) {
            this.isBarSearchAunthicated = false;
            ((BarSearchActivity) mCurrentActivity).parseBarSearchAunthicatedResult(response);
        }
        else if (isSDRSearchAunthicated) {
            this.isSDRSearchAunthicated = false;
            ((SDRSearchActivity) mCurrentActivity).parseSDRSearchAunthicatedResult(response);
        }


        //isSDRSearchAunthicated
        else if (isDocumentHotelSearchAunthicated) {
            this.isDocumentHotelSearchAunthicated = false;
//            ((ModifiedDocumentsFragment) fragment).parseDocumentHotelSearchAunthicatedResult(response);
            ((DashboardActivity) mCurrentActivity).parseDocumentHotelSearchAunthicatedResult(response);
        } else if (isSpecialServiceSaved) {
            this.isSpecialServiceSaved = false;
            ((SpecialServiceActivity) mCurrentActivity).parseSpecialServiceSavedResult(response);
        } else if (isSpecialServiceItemUpdated) {
            this.isSpecialServiceItemUpdated = false;
            ((SpecialServiceDetailsActivity) mCurrentActivity).parseSpecialServiceUpdateResult(response);
        } else if (isSpecialServiceItemSkipped) {
            this.isSpecialServiceItemSkipped = false;
            ((SpecialServiceDetailsActivity) mCurrentActivity).parseSpecialServiceItemSkipedResult(response);
        } else if (isSpecialServiceListData) {
            this.isSpecialServiceListData = false;
            ((SpecialServiceList) mCurrentActivity).parseSpecialServiceListResult(response);
        }  else if (isSpecialServiceAllowUserListData) {
            this.isSpecialServiceAllowUserListData = false;
            ((SpecialServiceList) mCurrentActivity).parseSpecialServiceAllowUserListResult(response);
        }
        else if (isSpecialServicePasscode) {
            this.isSpecialServicePasscode = false;
            ((FingerprintAuthActivity) mCurrentActivity).parseSpecialServicePasscodeResult(response);
        } else if (isSpecialServiceLoggerInfo) {
            this.isSpecialServiceLoggerInfo = false;
            ((SpecialServiceActivity) mCurrentActivity).parseSpecialServiceLoggerInfoResult(response);
        } else if (isSpecialServiceLISDRinfo) {
            this.isSpecialServiceLISDRinfo = false;
            ((SpecialServiceActivity) mCurrentActivity).parseSpecialServiceLISDRinfoResult(response,addLI);
        }else if (isSpecialServiceAllSDRinfo) {
            this.isSpecialServiceAllSDRinfo = false;
            ((SpecialServiceActivity) mCurrentActivity).parseSpecialServiceAllSDRinfoResult(response,addAllRequest);
        }else if (isLIReconnectionJustification) {
            this.isLIReconnectionJustification = false;
            ((SpecialServiceDetailsActivity) mCurrentActivity).parseLIReconnectionJustificationResult(response);
        }else if (isLIReconnectionCreate) {
            this.isLIReconnectionCreate = false;
            ((SpecialServiceDetailsActivity) mCurrentActivity).parseLIReconnectionCreateResult(response);
        }else if (isReminderToMcell) {
            this.isReminderToMcell = false;
            ((SpecialServiceDetailsActivity) mCurrentActivity).parseReminderToMcellResult(response);
        }else if (isLIDisconnectionCreate) {
            this.isLIDisconnectionCreate = false;
            ((SpecialServiceDetailsActivity) mCurrentActivity).parseLIDisconnectionCreateResult(response);
        }else if (isSpecialServiceItemSkiped) {
            this.isSpecialServiceItemSkiped = false;
            ((SpecialServiceDetailsActivity) mCurrentActivity).parseSpecialServiceItemSkipedResult(response);
        }else if (isLISearchAllState) {
            this.isLISearchAllState = false;
            ((SpecialServiceActivity) mCurrentActivity).parseLISearchAllStateResult(response);
        }else if (isBlackListed) {
            this.isBlackListed = false;
            ((SpecialServiceActivity) mCurrentActivity).parsecheckBlackListedReult(response,addLIBlackList);
        }else if (isRequestRevoked) {
            this.isRequestRevoked = false;
            ((SpecialServiceDetailsActivity) mCurrentActivity).parseRequetRevokedReult(response);
        }else if (isSpecialServiceRequestProcessType) {
            this.isSpecialServiceRequestProcessType = false;
            ((SpecialServiceDetailsActivity) mCurrentActivity).parseSpecialServiceRequestProcessType(response);
        }else if (isSpecialServiceSearchListData) {
            this.isSpecialServiceSearchListData = false;
            ((SpecialServiceList) mCurrentActivity).parseSpecialServiceListResult(response);
        }
        else if (isSpecialServiceLIDetailsStatusChange) {
            this.isSpecialServiceLIDetailsStatusChange = false;
            ((SpecialServiceDetailsActivity) mCurrentActivity).parseSpecialServiceLIDetailsStatusChangeResult(response);
        }
        else if (isLIPartialSaved) {
            this.isLIPartialSaved = false;
            ((SpecialServiceActivity) mCurrentActivity).parseLIPartialSavedResult(response);
        }
        else if (isNodalSuggestionSaved) {
            this.isNodalSuggestionSaved = false;
            ((SpecialServiceDetailsActivity) mCurrentActivity).parseNodalSuggestionSavedResult(response);
        } else if (isNodalSuggestionOnLoading) {
            this.isNodalSuggestionOnLoading = false;
            ((SpecialServiceDetailsActivity) mCurrentActivity).parseNodalSuggestionSavedResult(response);

        }else if (isMcellSuggestionOnLoading) {
            this.isMcellSuggestionOnLoading = false;
            ((SpecialServiceDetailsActivity) mCurrentActivity).parseMcellObservationResult(response);
        }


        }

        //isNodalSuggestionOnLoading
         //isNodalSuggestionSaved
        //isLIPartialSaved
           //isSpecialServiceSearchListData
         //isSpecialServiceRequestProcessType
         //isRequestRevoked
         //isSpecialServiceItemSkiped
        //isLIDisconnectionCreate
           //isReminderToMcell
         //isSpecialServiceListSearch
        //isSpecialServiceLISDRinfo
        //isLIReconnectionJustification



    @Override
    public void onFailure(String response) {
        //Toast.makeText(mContext, "Some error encountered!", Toast.LENGTH_LONG).show();
        Utility.showAlertDialog(mContext, "Error!!!", Constants.ERROR_MSG_TO_RELOAD, false);
    }

}
