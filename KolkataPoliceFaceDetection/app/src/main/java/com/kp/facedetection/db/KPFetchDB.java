package com.kp.facedetection.db;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-Asset-131 on 27-09-2016.
 */
public class KPFetchDB {

    static KPHelperDB dbhlpr_obj;
    static Cursor cursor;

    public static String getAppInfoTableCount(Context mContext) {


        dbhlpr_obj = new KPHelperDB(mContext);
        try {
            dbhlpr_obj.openDataBase();

        } catch (SQLException e) {

            e.printStackTrace();
        }

        String query ="select  * from app_info ";
        cursor = dbhlpr_obj.MyDB().rawQuery(
                query, null);
        int count = cursor.getCount();
        cursor.close();
        dbhlpr_obj.close();

        try {
            dbhlpr_obj.MyDB().close();
        } catch (Exception e) {
            e.printStackTrace();
        }



        return ""+count;
    }

    public static List<String> getInstalledAppVersionList(Context mContext){

        List<String> recentAppVersionList = new ArrayList<String>();

        dbhlpr_obj = new KPHelperDB(mContext);
        try {
            dbhlpr_obj.openDataBase();

        } catch (SQLException e) {

            e.printStackTrace();
        }

        String query = "select * from app_info order by time_stamp desc limit 10";

        cursor = dbhlpr_obj.MyDB().rawQuery(
                query, null);

        if (cursor.moveToFirst()) {
            do {

                recentAppVersionList.add(cursor.getString(cursor.getColumnIndex("app_version")));

            } while (cursor.moveToNext());
        }

        try {
            dbhlpr_obj.MyDB().close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Data Fetched Successfully");

        return recentAppVersionList;

    }

}
