package com.kp.facedetection.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 13-06-2017.
 */

public class SpecificArrestDetails implements Parcelable {

    private String arrestDate = "";
    private String psName = "";
    private String arrestAgainst = "";
    private String arrestee = "";
    private String aliasName ="";
    private String gender ="";
    private String age ="";
    private String fatherName = "";
    private String address = "";
    private String caseRef ="";
    private String crimeHead = "";
    private String caseDate = "";
    private String underSection ="";
    private String psCode = "";
    private String caseYr = "";
    private String UNIT = "";
    private String PS = "";
    //private String PICTURE_URL = "";
    private List<String> picture_list = new ArrayList<>();

    public String getPsCode() {
        return psCode;
    }

    public void setPsCode(String psCode) {
        this.psCode = psCode;
    }

    public String getCaseYr() {
        return caseYr;
    }

    public void setCaseYr(String caseYr) {
        this.caseYr = caseYr;
    }

    public String getArrestDate() {
        return arrestDate;
    }

    public void setArrestDate(String arrestDate) {
        this.arrestDate = arrestDate;
    }

    public String getPsName() {
        return psName;
    }

    public void setPsName(String psName) {
        this.psName = psName;
    }

    public String getArrestAgainst() {
        return arrestAgainst;
    }

    public void setArrestAgainst(String arrestAgainst) {
        this.arrestAgainst = arrestAgainst;
    }

    public String getArrestee() {
        return arrestee;
    }

    public void setArrestee(String arrestee) {
        this.arrestee = arrestee;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCaseRef() {
        return caseRef;
    }

    public void setCaseRef(String caseRef) {
        this.caseRef = caseRef;
    }

    public String getCrimeHead() {
        return crimeHead;
    }

    public void setCrimeHead(String crimeHead) {
        this.crimeHead = crimeHead;
    }

    public String getCaseDate() {
        return caseDate;
    }

    public void setCaseDate(String caseDate) {
        this.caseDate = caseDate;
    }

    public String getUnderSection() {
        return underSection;
    }

    public void setUnderSection(String underSection) {
        this.underSection = underSection;
    }

    public SpecificArrestDetails(Parcel in) {
        arrestDate = in.readString();
        psName = in.readString();
        arrestAgainst = in.readString();
        arrestee = in.readString();
        aliasName = in.readString();
        gender = in.readString();
        age = in.readString();
        fatherName = in.readString();
        address = in.readString();
        caseRef = in.readString();
        crimeHead = in.readString();
        caseDate = in.readString();
        underSection = in.readString();
        psCode = in.readString();
        caseYr = in.readString();
        UNIT = in.readString();
        PS = in.readString();
        //PICTURE_URL = in.readString();
    }

    public static final Creator<SpecificArrestDetails> CREATOR = new Creator<SpecificArrestDetails>() {
        @Override
        public SpecificArrestDetails createFromParcel(Parcel in) {
            return new SpecificArrestDetails(in);
        }

        @Override
        public SpecificArrestDetails[] newArray(int size) {
            return new SpecificArrestDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(arrestDate);
        dest.writeString(psName);
        dest.writeString(arrestAgainst);
        dest.writeString(arrestee);
        dest.writeString(aliasName);
        dest.writeString(gender);
        dest.writeString(age);
        dest.writeString(fatherName);
        dest.writeString(address);
        dest.writeString(caseRef);
        dest.writeString(crimeHead);
        dest.writeString(caseDate);
        dest.writeString(underSection);
        dest.writeString(psCode);
        dest.writeString(caseYr);
        dest.writeString(UNIT);
        dest.writeString(PS);
        //dest.writeString(PICTURE_URL);
    }

    public String getUNIT() {
        return UNIT;
    }

    public void setUNIT(String UNIT) {
        this.UNIT = UNIT;
    }

    public String getPS() {
        return PS;
    }

    public void setPS(String PS) {
        this.PS = PS;
    }

    public List<String> getPicture_list() {
        return picture_list;
    }

    public void setPicture_list(List<String> picture_list) {
        this.picture_list = picture_list;
    }
}
