package com.kp.facedetection.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kp.facedetection.CaseMapSearchDetailsActivity;
import com.kp.facedetection.CaseSearchFIRDetailsActivity;
import com.kp.facedetection.FIRPSListActivity;
import com.kp.facedetection.ModifiedCaseSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.adapter.ArrestSpecificListRecyclerAdapter;
import com.kp.facedetection.interfaces.OnArrestItemListener;
import com.kp.facedetection.model.CaseSearchDetails;
import com.kp.facedetection.model.SpecificArrestDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class DashboardArrestSpecificFragment extends Fragment implements View.OnClickListener, OnArrestItemListener {

    private RecyclerView recycler_arrestSpecificValue;
    private Button btnLoadMore;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrestSpecificListRecyclerAdapter specificAdapter;
    private TextView tv_noSepecificData;
    private View iv_dividerShow;

    ArrayList<SpecificArrestDetails> specificArrestDetailsArrayList = new ArrayList<>();

    int total;
    String[] keys;
    String[] values;

    String pageNo;
    private String caseRef = "";
    private String psCode = "";
    private String caseYear = "";
    private String psName = "";
    private String arrestDate = "";
    private String caseDate = "";
    private String underSec = "";
    private String itemName = "";
    private String headerShow = "";
    private int position;


    public static DashboardArrestSpecificFragment newInstance() {

        DashboardArrestSpecificFragment DLSearchFragment = new DashboardArrestSpecificFragment();
        return DLSearchFragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.specific_arrest_layout, container, false);
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        iv_dividerShow = view.findViewById(R.id.iv_dividerShow);

        recycler_arrestSpecificValue = (RecyclerView) view.findViewById(R.id.recycler_arrestSpecificValue);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_arrestSpecificValue.setLayoutManager(mLayoutManager);
        recycler_arrestSpecificValue.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        btnLoadMore = (Button) view.findViewById(R.id.bt_loadMore);
        tv_noSepecificData = (TextView) view.findViewById(R.id.tv_showNODetail);
    }


    public void addAllSpecificData(ArrayList<SpecificArrestDetails> specificArrestDetailsArrayList, String countResult, String pageNo, String[] keys, String[] values) {
        this.specificArrestDetailsArrayList = specificArrestDetailsArrayList;

        specificAdapter = new ArrestSpecificListRecyclerAdapter(getActivity(), specificArrestDetailsArrayList);
        recycler_arrestSpecificValue.setAdapter(specificAdapter);
        specificAdapter.notifyDataSetChanged();

        // item click call
        specificAdapter.setArrestItemClickListener(this);

        total = Integer.parseInt(countResult);
        this.keys = keys;
        this.values = values;
        this.pageNo = pageNo;

        if (total > 20) {
            btnLoadMore.setVisibility(View.VISIBLE);
        } else {
            btnLoadMore.setVisibility(View.GONE);
        }
        btnLoadMore.setOnClickListener(this);
    }


    public void setTextViewVisibleForSpecific(){
        if (specificArrestDetailsArrayList.size() > 0 ){
            tv_noSepecificData.setVisibility(View.GONE);
            iv_dividerShow.setVisibility(View.VISIBLE);

        }else{
            iv_dividerShow.setVisibility(View.GONE);
            tv_noSepecificData.setVisibility(View.VISIBLE);
            tv_noSepecificData.setText("No specific arrest detail found.");
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_loadMore:
                fetchSpecificDataPagination();
                break;
        }
    }

    private void fetchSpecificDataPagination() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_SPECIFIC_ARREST_VIEW);
        taskManager.setAllSpecificArrestViewPagination(true);

        values[1] = (Integer.parseInt(pageNo) + 1) + "";

        taskManager.doStartTask(keys, values, true);
    }

    public void parseArrestSpecificSearchPagination(String response) {
        //System.out.println("parseArrestSpecificSearchPagination" + response);

        if (response != null && !response.equals("")) {
            try {
                JSONObject jObj = new JSONObject(response);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    //totalResult = jObj.opt("totalresult").toString();
                    pageNo = jObj.opt("pageno").toString();
                    JSONArray resultArray = jObj.optJSONArray("result");
                    parseAllSpecificArrestResponse(resultArray);
                } else {
                    Utility.showAlertDialog(getActivity(), Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }


    private void parseAllSpecificArrestResponse(JSONArray resultArray) {

        for (int i = 0; i < resultArray.length(); i++) {

            SpecificArrestDetails specificArrestDetails = new SpecificArrestDetails(Parcel.obtain());
            try {
                JSONObject jObj = resultArray.getJSONObject(i);
                if (jObj.optString("ADDRESS") != null && !jObj.optString("ADDRESS").equalsIgnoreCase("null") && !jObj.optString("ADDRESS").equalsIgnoreCase("")) {
                    specificArrestDetails.setAddress(jObj.optString("ADDRESS"));
                }

                if(jObj.optString("AGE") != null && !jObj.optString("AGE").equalsIgnoreCase("null") && !jObj.optString("AGE").equalsIgnoreCase("") && !jObj.optString("AGE").equalsIgnoreCase("NA")){
                    specificArrestDetails.setAge(jObj.optString("AGE"));
                }else{
                    specificArrestDetails.setAge("");
                }

                if (jObj.optString("ALIASNAME") != null && !jObj.optString("ALIASNAME").equalsIgnoreCase("null") && !jObj.optString("ALIASNAME").equalsIgnoreCase("")
                        && !jObj.optString("ALIASNAME").equalsIgnoreCase("NA") && !jObj.optString("ALIASNAME").equalsIgnoreCase("NIL")) {
                    specificArrestDetails.setAliasName(jObj.optString("ALIASNAME"));
                }
                if (jObj.optString("ARRAGAINST") != null && !jObj.optString("ARRAGAINST").equalsIgnoreCase("null") && !jObj.optString("ARRAGAINST").equalsIgnoreCase("")) {
                    specificArrestDetails.setArrestAgainst(jObj.optString("ARRAGAINST"));
                }
                if (jObj.optString("ARRESTEE") != null && !jObj.optString("ARRESTEE").equalsIgnoreCase("null") && !jObj.optString("ARRESTEE").equalsIgnoreCase("")) {
                    specificArrestDetails.setArrestee(jObj.optString("ARRESTEE"));
                }
                if (jObj.optString("ARREST_DATE") != null && !jObj.optString("ARREST_DATE").equalsIgnoreCase("null") && !jObj.optString("ARREST_DATE").equalsIgnoreCase("")) {
                    specificArrestDetails.setArrestDate(jObj.optString("ARREST_DATE"));
                }
                if (jObj.optString("CASEDATE") != null && !jObj.optString("CASEDATE").equalsIgnoreCase("null") && !jObj.optString("CASEDATE").equalsIgnoreCase("")) {
                    specificArrestDetails.setCaseDate(jObj.optString("CASEDATE"));
                }
                if (jObj.optString("CASEREF") != null && !jObj.optString("CASEREF").equalsIgnoreCase("null") && !jObj.optString("CASEREF").equalsIgnoreCase("")) {
                    specificArrestDetails.setCaseRef(jObj.optString("CASEREF"));
                }
                if (jObj.optString("CRIME_HEAD") != null && !jObj.optString("CRIME_HEAD").equalsIgnoreCase("null") && !jObj.optString("CRIME_HEAD").equalsIgnoreCase("")) {
                    specificArrestDetails.setCrimeHead(jObj.optString("CRIME_HEAD"));
                }
                if (jObj.optString("FATHER_HUSBAND") != null && !jObj.optString("FATHER_HUSBAND").equalsIgnoreCase("null") && !jObj.optString("FATHER_HUSBAND").equalsIgnoreCase("")) {
                    specificArrestDetails.setFatherName(jObj.optString("FATHER_HUSBAND"));
                }
                if (jObj.optString("PSNAME") != null && !jObj.optString("PSNAME").equalsIgnoreCase("null") && !jObj.optString("PSNAME").equalsIgnoreCase("")) {
                    specificArrestDetails.setPsName(jObj.optString("PSNAME"));
                }
                if (jObj.optString("SEX") != null && !jObj.optString("SEX").equalsIgnoreCase("null") && !jObj.optString("SEX").equalsIgnoreCase("") && !jObj.optString("SEX").equalsIgnoreCase("NA")) {
                    specificArrestDetails.setGender(jObj.optString("SEX"));
                }
                if (jObj.optString("UNDER_SECTION") != null && !jObj.optString("UNDER_SECTION").equalsIgnoreCase("null") && !jObj.optString("UNDER_SECTION").equalsIgnoreCase("")) {
                    specificArrestDetails.setUnderSection(jObj.optString("UNDER_SECTION"));
                }
                if(jObj.optString("UNIT") != null && !jObj.optString("UNIT").equalsIgnoreCase("null") && !jObj.optString("UNIT").equalsIgnoreCase("")){
                    specificArrestDetails.setUNIT(jObj.optString("UNIT"));
                }
                if(jObj.optString("PS") != null && !jObj.optString("PS").equalsIgnoreCase("null") && !jObj.optString("PS").equalsIgnoreCase("")){
                    specificArrestDetails.setPS(jObj.optString("PS"));
                }

                if(jObj.optString("PSCODE") != null && !jObj.optString("PSCODE").equalsIgnoreCase("null") && !jObj.optString("PSCODE").equalsIgnoreCase("")){
                    specificArrestDetails.setPsCode(jObj.optString("PSCODE"));
                }
                if(jObj.optString("CASE_YR") != null && !jObj.optString("CASE_YR").equalsIgnoreCase("null") && !jObj.optString("CASE_YR").equalsIgnoreCase("")){
                    specificArrestDetails.setCaseYr(jObj.optString("CASE_YR"));
                }


                JSONArray pic_url_array = jObj.optJSONArray("PICTURE_URL");
                List<String> pic_list = new ArrayList<>();
                if(pic_url_array != null) {
                    for (int k = 0; k < pic_url_array.length(); k++) {
                        pic_list.add(pic_url_array.optString(k));
                    }
                    specificArrestDetails.setPicture_list(pic_list);
                }

                specificArrestDetailsArrayList.add(specificArrestDetails);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        specificAdapter.notifyDataSetChanged();
        if (specificArrestDetailsArrayList.size() >= total) {
            btnLoadMore.setVisibility(View.GONE);
        }
    }

    @Override
    public void onArrestItemClick(View v, int pos) {

        SpecificArrestDetails specificArrestDetails = specificArrestDetailsArrayList.get(pos);

        if(specificArrestDetails.getPsName() != null && !specificArrestDetails.getPsName().equalsIgnoreCase("") && !specificArrestDetails.getPsName().equalsIgnoreCase("null")){
            psName = specificArrestDetails.getPsName().toString().trim();
        }
        if(specificArrestDetails.getArrestDate() != null && !specificArrestDetails.getArrestDate().equalsIgnoreCase("") && !specificArrestDetails.getArrestDate().equalsIgnoreCase("null")){
            arrestDate =" dated "+ specificArrestDetails.getArrestDate().toString().trim();
        }
        if(specificArrestDetails.getCaseDate() != null && !specificArrestDetails.getCaseDate().equalsIgnoreCase("") && !specificArrestDetails.getCaseDate().equalsIgnoreCase("null")){
            caseDate =" dated "+ specificArrestDetails.getCaseDate().toString().trim();
        }
        if(specificArrestDetails.getUnderSection() != null && !specificArrestDetails.getUnderSection().equalsIgnoreCase("") && !specificArrestDetails.getUnderSection().equalsIgnoreCase("null")){
            underSec = " u/s "+ specificArrestDetails.getUnderSection().toString().trim();
        }

        if(specificArrestDetails.getCaseRef() != null && !specificArrestDetails.getCaseRef().equalsIgnoreCase("") && !specificArrestDetails.getCaseRef().equalsIgnoreCase("null")){
            caseRef = specificArrestDetails.getCaseRef().toString().trim();
        }
        if(specificArrestDetails.getPsCode() != null && !specificArrestDetails.getPsCode().equalsIgnoreCase("") && !specificArrestDetails.getPsCode().equalsIgnoreCase("null")){
            psCode = specificArrestDetails.getPsCode().toString().trim();
        }
        if(specificArrestDetails.getCaseYr() != null && !specificArrestDetails.getCaseYr().equalsIgnoreCase("") && !specificArrestDetails.getCaseYr().equalsIgnoreCase("null")){
            caseYear = specificArrestDetails.getCaseYr().toString().trim();
        }

//        itemName = psName+" C/No. "+caseRef + arrestDate + underSec;
        itemName = psName+" C/No. "+caseRef + caseDate + underSec;
//        headerShow =psName+" C/No. "+caseRef + arrestDate;
        headerShow =psName+" C/No. "+caseRef + caseDate;
        position = pos;

        if(!caseRef.equalsIgnoreCase("") && !caseYear.equalsIgnoreCase("") && !psCode.equalsIgnoreCase("")){

            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_MODIFIED_CASE_SEARCH_FIR_DETAILS);
            taskManager.setArrestSpecificViewToCaseSearch(true);

            String[] keys = { "pscode", "caseno", "caseyr"};
            String[] values = {psCode.trim(), caseRef.trim(), caseYear.trim()};

            taskManager.doStartTask(keys, values, true);
        }
        else{
            Utility.showAlertDialog(getActivity(),Constants.ERROR_DETAILS_TITLE,Constants.ERROR_MSG_DETAIL,false);
        }

    }

    public  void parseArrestSpecificDetailsForCase(String result){
        //System.out.println("parseArrestSpecificDetailsForCase"+result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    Intent in = new Intent(getActivity(),CaseSearchFIRDetailsActivity.class);
                    in.putExtra("ITEM_NAME",itemName);
                    in.putExtra("HEADER_VALUE", headerShow);
                    in.putExtra("PS_CODE",psCode);
                    in.putExtra("CASE_NO", caseRef);
                    in.putExtra("CASE_YR", caseYear);
                    in.putExtra("POSITION",position+"");
                    in.putExtra("FROM_PAGE","DashboardArrestSpecificFragment");

                    getActivity().startActivity(in);
                }
                else{
                    Utility.showAlertDialog(getActivity(),Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL,false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(getActivity(),Constants.SEARCH_ERROR_TITLE,Constants.ERROR_EXCEPTION_MSG,false);
            }
        }
    }


}