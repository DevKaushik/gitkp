package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-Asset-131 on 13-07-2016.
 */
public class FIRUnderSectionDetails implements Serializable {

    private String under_section="";
    private String us_class = "";
    private String category = "";
    private String caseYr = "";
    private String psCode = "";
    private String caseNo = "";

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUs_class() {
        return us_class;
    }

    public void setUs_class(String us_class) {
        this.us_class = us_class;
    }

    public String getUnder_section() {
        return under_section;
    }

    public void setUnder_section(String under_section) {
        this.under_section = under_section;
    }

    public String getCaseYr() {
        return caseYr;
    }

    public void setCaseYr(String caseYr) {
        this.caseYr = caseYr;
    }

    public String getPsCode() {
        return psCode;
    }

    public void setPsCode(String psCode) {
        this.psCode = psCode;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }
}
