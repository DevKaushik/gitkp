package com.kp.facedetection.asynctasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.kp.facedetection.interfaces.OnSendSMSResult;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Kaushik on 21-09-2016.
 */
public class AsynctaskForSendSMS extends AsyncTask<Void, Void, Void> {


    private ProgressDialog dialog;
    private Context context;
    private String ph_no;
    private String current_pswd;
    OnSendSMSResult delegate;
    private String responsebody;

    public void setDelegate(OnSendSMSResult delegate) {
        this.delegate = delegate;
    }

    public AsynctaskForSendSMS(Context context,String ph_no,String current_pswd){

        this.context = context;
        this.ph_no = ph_no;
        this.current_pswd = current_pswd;
        dialog = new ProgressDialog(context);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        try {
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {

        String sms_gateway_url = Constants.SMS_GATEWAY_API+ph_no+"&text=Your+password+has+been+reset+to+"+current_pswd+".%0AYou+are+advised+to+use+this+in+the+next+login.+%0A%0ACRS+APP"+"&route=Informative&type=text&datetime="+Utility.currentDateTime();

        try {
            URL url = new URL(sms_gateway_url);
            Log.e("URL",sms_gateway_url);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(30000);
            con.setReadTimeout(30000);
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            Log.e("Response Code",""+responseCode);

            if(responseCode == HttpURLConnection.HTTP_OK){
                String readStream = Utility.readStream(con.getInputStream());
                System.out.println(readStream);
                responsebody = readStream;
                Log.e("Response", responsebody);

                if(responsebody.toUpperCase().contains("ERROR")){
                    delegate.onSendSMSResultError("Some error encountered, Please try again.");
                }
                else{
                    delegate.onSendSMSResultSuccess(responsebody);
                }

            } else {
                delegate.onSendSMSResultError("Some error encountered, Please try again.");
            }

            con.disconnect();

        } catch (IOException e) {

            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {

        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }

    }
}
