package com.kp.facedetection;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class MisActiveUsersActivity extends MisActivity {
    ArrayList<MisActiveUsersDataModel> msActiveDataModel;
    ListView listView;
    private static MisActiveCustomAdapter adapter;
    String CURRVER;
    SearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_active_users);
        searchView=(SearchView)findViewById(R.id.searchView);
        listView=(ListView)findViewById(R.id.activelist);

        msActiveDataModel= new ArrayList<>();
        appversion();
        activeuserApiCall();

//        ParseOnlineUser();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                misDataModel misDataModel=  msDataModel.get(position);


            }
        });
    }
    private void appversion(){
        TaskManager taskManager = new TaskManager(MisActiveUsersActivity.this);
        taskManager.setMethod(Constants.METHOD_CURRENT_VERSION);
        taskManager.setCurrentVer(true);

        String[] keys = {};
        String[] values = {};
        taskManager.doStartTask(keys,values,true);
    }

    public void parseCurrentVersion(String result){
        if (result != null && !result.equals("")) {
            try {
                JSONObject jObj = new JSONObject(result);

                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {
                    JSONObject o = jObj.getJSONObject("result");
                    CURRVER = o.getString("CURRVER");
                }
                else{
                    Utility.showAlertDialog(MisActiveUsersActivity.this,Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL,false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(MisActiveUsersActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
            }
        }


    private void activeuserApiCall() {
        TaskManager taskManager = new TaskManager(MisActiveUsersActivity.this);
        taskManager.setMethod(Constants.METHOD_ACTIVE_USERS);
        taskManager.setActiveUser(true);

        String[] keys = {};
        String[] values = {};
        taskManager.doStartTask(keys, values, true);
    }

    public void ParseActiveUser(String result) {
        if (result != null && !result.equals("")) {
            Log.e("ACTIVEUSERSTOTAL",result);
            try {
                JSONObject jObj = new JSONObject(result);

                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {
//                    JSONObject o = jObj.getJSONObject("result");
                    JSONArray resultArray=jObj.getJSONArray("result");
                    parseActiveUserData(resultArray);
                }
                else{
                    Utility.showAlertDialog(MisActiveUsersActivity.this,Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL,false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(MisActiveUsersActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }


    private void parseActiveUserData(JSONArray resultArray) {
        JSONObject jsonObj = null;

        for (int i = 0; i < resultArray.length(); i++) {

            try {
                jsonObj = resultArray.getJSONObject(i);

                String USER_NAME = jsonObj.getString("USER_NAME");
                String PSNAME = jsonObj.getString("PSNAME");
                String USER_RANK = jsonObj.getString("USER_RANK");
                String APP_VERSION = jsonObj.getString("APP_VERSION");
                String USER_LASTLOGIN = jsonObj.getString("USER_LASTLOGIN");
                String LOGIN_TIME = jsonObj.getString("LOGIN_TIME");

                Log.e("LIST", USER_NAME + PSNAME + USER_RANK + APP_VERSION + USER_LASTLOGIN + LOGIN_TIME);
                msActiveDataModel.add(new MisActiveUsersDataModel(USER_NAME,PSNAME,USER_RANK,APP_VERSION,USER_LASTLOGIN,LOGIN_TIME,CURRVER));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter= new MisActiveCustomAdapter(msActiveDataModel,getApplicationContext());

        listView.setAdapter(adapter);

        listView.setTextFilterEnabled(true);

    }

}
