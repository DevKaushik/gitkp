package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-Asset-131 on 22-07-2016.
 */
public class FIRWarranteesDetails implements Serializable {

    private String sl_no="";
    private String warrantee_name="";
    private String warrantee_address="";

    public String getSl_no() {
        return sl_no;
    }

    public void setSl_no(String sl_no) {
        this.sl_no = sl_no;
    }

    public String getWarrantee_address() {
        return warrantee_address;
    }

    public void setWarrantee_address(String warrantee_address) {
        this.warrantee_address = warrantee_address;
    }

    public String getWarrantee_name() {
        return warrantee_name;
    }

    public void setWarrantee_name(String warrantee_name) {
        this.warrantee_name = warrantee_name;
    }

}
