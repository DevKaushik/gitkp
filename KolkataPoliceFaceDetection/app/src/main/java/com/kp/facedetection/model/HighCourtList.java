package com.kp.facedetection.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by DAT-165 on 24-03-2017.
 */

public class HighCourtList implements Serializable {

    private String status = "";
    private String message = "";
    private ArrayList<CourtRoomListDetails> obj_courtRoomList = new ArrayList<CourtRoomListDetails>();
    private ArrayList<CourtTypeListDetails> obj_courtTypeList = new ArrayList<CourtTypeListDetails>();
    private ArrayList<CourtCaseCategoryListDetails> obj_caseCategoryList = new ArrayList<CourtCaseCategoryListDetails>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CourtRoomListDetails> getObj_courtRoomList() {
        return obj_courtRoomList;
    }

    public void setObj_courtRoomList(CourtRoomListDetails obj) {
        obj_courtRoomList.add(obj);
    }

    public ArrayList<CourtTypeListDetails> getObj_courtTypeList() {
        return obj_courtTypeList;
    }

    public void setObj_courtTypeList(CourtTypeListDetails obj) {
        obj_courtTypeList.add(obj);
    }

    public ArrayList<CourtCaseCategoryListDetails> getObj_caseCategoryList() {
        return obj_caseCategoryList;
    }

    public void setObj_caseCategoryList(CourtCaseCategoryListDetails obj) {
        obj_caseCategoryList.add(obj);
    }
}
