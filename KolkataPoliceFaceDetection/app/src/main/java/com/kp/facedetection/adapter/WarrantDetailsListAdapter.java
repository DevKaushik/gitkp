package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.WarrantDetails;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by DAT-165 on 14-07-2016.
 */
public class WarrantDetailsListAdapter extends BaseAdapter {

    private Context context;
    private List<WarrantDetails> warrantDetailsList;

    public WarrantDetailsListAdapter(Context context,List<WarrantDetails> warrantDetailsList) {

        this.context = context;
        this.warrantDetailsList = warrantDetailsList;
    }

    @Override
    public int getCount() {

        if(warrantDetailsList.size()>0) {
            return warrantDetailsList.size();
        }
        else {
            return 0;
        }

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.layout_fir_details_item_row, null);


            holder.tv_SlNo = (TextView) convertView
                    .findViewById(R.id.tv_SlNo);

            holder.tv_WarrantName = (TextView) convertView
                    .findViewById(R.id.tv_FirName);

            holder.tv_status = (TextView) convertView
                    .findViewById(R.id.tv_status);

            Constants.changefonts(holder.tv_SlNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_WarrantName, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_status, context, "Calibri.ttf");

            holder.tv_WarrantName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String warrantName="Warrant No. "+warrantDetailsList.get(position).getWaNo()+" Dated "+warrantDetailsList.get(position).getWaIssueDate()+" of Sec. "+warrantDetailsList.get(position).getUnderSection();
        holder.tv_SlNo.setText(Integer.toString(position + 1));


        SpannableString mySpannableString = new SpannableString(warrantName);
        mySpannableString.setSpan(new UnderlineSpan(), 0, mySpannableString.length(), 0);
        holder.tv_WarrantName.setText(mySpannableString);

        holder.tv_status.setText(warrantDetailsList.get(position).getWaStatus());


        return convertView;
    }

    class ViewHolder {

        TextView tv_SlNo;
        TextView tv_WarrantName;
        TextView tv_status;

    }
}
