package com.kp.facedetection.adapter;

/**
 * Created by DAT-Asset-131 on 31-05-2016.
 */
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.utility.Constants;

public class ExpandableListAdapterForWarrent extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;
    private TextView tv_key;
    private TextView tv_value;
    private TextView tv_name;
    private TextView tv_address;
    private ImageView iv_searchResultImage;
    private RelativeLayout relative_profile;
    private RelativeLayout relative_associate;


    public ExpandableListAdapterForWarrent(Context context, List<String> listDataHeader,
                                           HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;

    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);
        final String groupText = (String) getGroup(groupPosition);

        if (convertView == null) {

            convertView = View.inflate(_context,R.layout.layout_criminal_details_item_row, null);
        }

        tv_key = (TextView) convertView.findViewById(R.id.tv_key);
        tv_value = (TextView) convertView.findViewById(R.id.tv_value);
        tv_name = (TextView) convertView.findViewById(R.id.tv_name);
        tv_address = (TextView) convertView.findViewById(R.id.tv_address);
        iv_searchResultImage = (ImageView) convertView.findViewById(R.id.iv_searchResultImage);
        relative_profile = (RelativeLayout) convertView.findViewById(R.id.relative_profile);
        relative_associate = (RelativeLayout) convertView.findViewById(R.id.relative_associate);

        relative_associate.setVisibility(View.GONE);

        String original_key = childText.substring(0,childText.indexOf("^"));
        String modified_key =  original_key.substring(0, 1).toUpperCase() + original_key.substring(1)+":";
        String value = childText.substring(childText.indexOf("^")+1);

        Constants.changefonts(tv_key, _context, "Calibri Bold.ttf");
        Constants.changefonts(tv_value, _context, "Calibri.ttf");

        tv_key.setText(modified_key);
        tv_value.setText(value);



        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
