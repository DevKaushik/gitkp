package com.kp.facedetection;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.adapter.ChargesheetDueListAdapter;
import com.kp.facedetection.adapter.UnnaturalDeathAdapter;
import com.kp.facedetection.interfaces.OnItemClickListenerForChargesheetDue;
import com.kp.facedetection.interfaces.OnItemClickListenerForUnnaturalDeath;
import com.kp.facedetection.model.CaseSearchFIRDetails;
import com.kp.facedetection.model.ChargesheetDueDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class ChargesheetDueCaseListActivity extends BaseActivity implements OnItemClickListenerForChargesheetDue, View.OnClickListener, Observer {
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";
    private String user_id = "";
    private String selectedDate="";
    private String selected_ps="";
    private String selected_ps_name="";
    private String selected_crime="";
    RecyclerView chargeSheetRecyclerView;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<ChargesheetDueDetails> chargesheetDueDetailsArrayList =new ArrayList<>();
    ChargesheetDueListAdapter chargesheetDueListAdapter;
    String from="CS";
    String role="";
    TextView chargesheetNotificationCount,tv_heading_charge_sheet_due,tv_no_record;
    int pageNo=1;
    String fromPage="";
    String headerText="";
    String notificationCount="";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chargesheet_due_list);
        ObservableObject.getInstance().addObserver(this);
        role = Utility.getUserInfo(this).getUserDesignation();
        fromPage = getIntent().getStringExtra("FROM_PAGE");
        setToolBar();
        initView();
    }
    public void setToolBar(){
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
            //role = Utility.getUserInfo(this).getUserDesignation();
           // role="DC";

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }
    public void initView() {
        tv_heading_charge_sheet_due=(TextView)findViewById(R.id.tv_heading_charge_sheet_due);
        if(fromPage.equalsIgnoreCase("DashboardActivity")) {
            selectedDate = getIntent().getExtras().getString("SELECTED_DATE_CS");
            selected_ps = getIntent().getExtras().getString("SELECTED_PS_CS");
            selected_ps_name = getIntent().getExtras().getString("SELECTED_PS_NAME_CS");
            selected_crime = getIntent().getExtras().getString("SELECTED_CRIME_CS");
            tv_heading_charge_sheet_due.setText(selected_ps_name);
        }
        else if(fromPage.equalsIgnoreCase("BaseActivity"))
        {
            user_id= getIntent().getExtras().getString("SELECTED_USER_ID");
            selected_crime = getIntent().getExtras().getString("SELECTED_CRIME_CS");
        }

        tv_no_record=(TextView)findViewById(R.id.tv_no_record);
        chargeSheetRecyclerView=(RecyclerView)findViewById(R.id.rv_chargesheet_due_case);
        layoutManager=new LinearLayoutManager(this);
        chargeSheetRecyclerView.setLayoutManager(layoutManager);
        chargeSheetRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));

        chargesheetDueListAdapter=new ChargesheetDueListAdapter(this,chargesheetDueDetailsArrayList,from);
        chargeSheetRecyclerView.setAdapter(chargesheetDueListAdapter);
        chargesheetDueListAdapter.setOnClickListener(this);
        getAllchargeSheetCase();

    }
    public  void   getAllchargeSheetCase(){
        TaskManager taskManager = new TaskManager(this);
        if(fromPage.equalsIgnoreCase("DashboardActivity")) {
            taskManager.setMethod(Constants.METHOD_CHARGESHEET_CS_DETAILS);
            taskManager.setChargesheetDueCS(true);
            String[] keys = {"currDate", "pageno", "ps", "category"};
            String[] values = {selectedDate.trim(), String.valueOf(pageNo).trim(), selected_ps.trim(), selected_crime.trim()};
            taskManager.doStartTask(keys, values, true);
        }
        else if(fromPage.equalsIgnoreCase("BaseActivity")){
            taskManager.setMethod(Constants.METHOD_CHARGESHEET_CS_DETAILS_NOTIFICATION);
            taskManager.setChargesheetDueCS(true);
            String[] keys = {"user_id","category"};
            String[] values = {user_id.trim(),selected_crime.trim()};
            taskManager.doStartTask(keys, values, true);

        }
    }
    public void parseChargeSheetDueCaseResult(String response) {
        //System.out.println("ParseCSresult" + response);

        if (response != null && !response.equals("")) {

            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.opt("status").toString().equalsIgnoreCase("1")) {
                    JSONArray result = jobj.optJSONArray("result");
                    parseCSResponse(result);
                } else {
                  //  Utility.showAlertDialog(ChargesheetDueCaseListActivity.this, Constants.ERROR_DETAILS_TITLE, jobj.optString("message"), false);
                    tv_no_record.setVisibility(View.VISIBLE);
                    tv_no_record.setText("No Chargesheet Due");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(ChargesheetDueCaseListActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }
    private void parseCSResponse(JSONArray resultArray) {
        for (int i = 0; i < resultArray.length(); i++) {
            ChargesheetDueDetails psDetails = new ChargesheetDueDetails();
            JSONObject object = resultArray.optJSONObject(i);
            psDetails.setCaseNo(object.optString("CASENO"));
            psDetails.setCaseDate(object.optString("CASEDATE"));
            psDetails.setUnderSection(object.optString("UNDER_SECTION"));
            psDetails.setPsName(object.optString("PSNAME"));
            psDetails.setPsCode(object.optString("PS"));
            psDetails.setCaseYear(object.optString("FIR_YR"));
            chargesheetDueDetailsArrayList.add(psDetails);
        }
        if(fromPage.equalsIgnoreCase("BaseActivity") && resultArray.length()>0) {
            tv_heading_charge_sheet_due.setText(chargesheetDueDetailsArrayList.get(0).getPsName());
        }
        chargesheetDueListAdapter.notifyDataSetChanged();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.charseet_due_notification:
                if(role.equalsIgnoreCase(Constants.USER_ROLE_DC)) {
                    getChargesheetdetailsAsperRole(this);
                    finish();
                }
        }

    }

    @Override
    public void onClickChargesheetDue(View view, int position) {
        if (!Constants.internetOnline(this)) {
            Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }
        headerText="";
        L.e("CS CLICKED POSITION"+position);
        /*Intent in = new Intent(ChargesheetDueCaseListActivity.this, CaseSearchFIRDetailsActivity.class);
        in.putExtra("SELECTED_DATE_CS", selectedDate);
        in.putExtra("SELECTED_PS_CS", chargesheetDueDetailsArrayList.get(position).getPsCode());
        in.putExtra("SELECTED_CRIME_CS", selected_crime);
        startActivity(in);*/
        if(chargesheetDueDetailsArrayList.get(position).getPsName()!=null && !chargesheetDueDetailsArrayList.get(position).getPsName().equalsIgnoreCase("")&& !chargesheetDueDetailsArrayList.get(position).getPsName().equalsIgnoreCase("null")) {
            headerText = headerText + chargesheetDueDetailsArrayList.get(position).getPsName();
        }
        if(chargesheetDueDetailsArrayList.get(position).getCaseNo()!=null && !chargesheetDueDetailsArrayList.get(position).getCaseNo().equalsIgnoreCase("")&& !chargesheetDueDetailsArrayList.get(position).getCaseNo().equalsIgnoreCase("null")) {
            headerText = headerText +  " C/No. "+chargesheetDueDetailsArrayList.get(position).getCaseNo();
        }
        if(chargesheetDueDetailsArrayList.get(position).getCaseDate()!=null && !chargesheetDueDetailsArrayList.get(position).getCaseDate().equalsIgnoreCase("")&& !chargesheetDueDetailsArrayList.get(position).getCaseDate().equalsIgnoreCase("null")) {
            headerText = headerText +  " dated "+chargesheetDueDetailsArrayList.get(position).getCaseDate();
        }
        if(chargesheetDueDetailsArrayList.get(position).getUnderSection()!=null && !chargesheetDueDetailsArrayList.get(position).getUnderSection().equalsIgnoreCase("")&& !chargesheetDueDetailsArrayList.get(position).getUnderSection().equalsIgnoreCase("null")) {
            headerText = headerText +  " u/s "+chargesheetDueDetailsArrayList.get(position).getUnderSection();
        }
        Intent it = new Intent(ChargesheetDueCaseListActivity.this, CaseSearchFIRDetailsActivity.class);
        it.putExtra("FROM_PAGE", "ChargesheetDueCaseListActivity");
        it.putExtra("ITEM_NAME", headerText);
        it.putExtra("HEADER_VALUE", headerText);
        it.putExtra("PS_CODE", chargesheetDueDetailsArrayList.get(position).getPsCode());
        it.putExtra("CASE_NO", chargesheetDueDetailsArrayList.get(position).getCaseNo());
        it.putExtra("CASE_YR", chargesheetDueDetailsArrayList.get(position).getCaseYear());
        it.putExtra("POSITION", pageNo);
       // it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        //finish();
        startActivity(it);


    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }
    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
