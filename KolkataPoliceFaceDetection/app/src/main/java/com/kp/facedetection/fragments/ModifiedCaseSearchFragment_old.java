package com.kp.facedetection.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import com.kp.facedetection.ModifiedCaseSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.adapter.CustomDialogAdapterForCategoryCrimeList2;
import com.kp.facedetection.adapter.CustomDialogAdapterForModusOperandi;
import com.kp.facedetection.adapter.CustomFIRStatusAdapter;
import com.kp.facedetection.model.AllContentList;
import com.kp.facedetection.model.CaseSearchDetails;
import com.kp.facedetection.model.DivisionList;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by DAT-165 on 22-07-2016.
 */
public class ModifiedCaseSearchFragment_old extends Fragment implements SearchView.OnQueryTextListener,AdapterView.OnItemSelectedListener{

    protected String[] array_policeStation ;
    protected ArrayList<CharSequence> selectedPoliceStations = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedPoliceStationsId = new ArrayList<CharSequence>();
    private String policeStationString="";

    public AllContentList allContentList;

    ArrayAdapter<CharSequence> crimeTimeAdapter;
    private TextView tv_modus_operandi;
    private TextView tv_time_crime_value;
    private TextView tv_ps_value;
    private TextView tv_date_start_value;
    private TextView tv_date_end_value;
    private TextView tv_category_crime_value;
    private EditText et_case_value;
    private Spinner sp_crimeTimeValue;
    private Spinner sp_caseYear_value;
    private EditText et_complaint_value;
    private TextView tv_io_value;
    private TextView tv_divison_value;
    private Spinner sp_status_value;
    private EditText et_briefFact_value;
    private TextView tv_Status;

    private TextView tv_investingUnit_value, tv_investingSec_value;

    private ScrollView scrollView1;

    private Button btn_Case_search;
    private Button btn_reset;

    private int timeSetStatus = 0;


    protected String[] array_crimeCategory;

    private boolean crimeCategory_status = false;
    private boolean modusOperandi_status = false;

    CustomDialogAdapterForCategoryCrimeList2 customDialogAdapterForCategoryCrimeList2;
    CustomDialogAdapterForModusOperandi customDialogAdapterForModusOperandi;

    private List<String> caseYearList = new ArrayList<String>();
    ArrayAdapter<String> caseYearAdapter;

    private ListView lv_dialog;

    //various String for search

    private String caseYear="";
    private String case_no="";
    private String date_from="";
    private String date_to="";
    private String crime_time="";
    private String crimeCategoryString = "";
    private String modusOperandiString = "";

    private String pageno="";
    private String totalResult="";
    private String[] keys;
    private String[] values;

    private List<CaseSearchDetails> caseSearchDetailsList = new ArrayList<CaseSearchDetails>();

    protected ArrayList<String>  selectedInvestigateUnit = new ArrayList<String>();
    protected ArrayList<String>  selectedInvestigateUnitCode = new ArrayList<String>();
    private String[] array_investigateUnit;
    private String invUnitString = "";

    private String[] array_investigateSection;
    protected ArrayList<String> invSectionNameList;
    protected ArrayList<String> invSectionCodeList;
    protected ArrayList<String> selectedInvSectionName;
    protected ArrayList<String> selectedInvSectionCode;
    private String invSectionString = "";

    protected String[] array_division ;
    protected ArrayList<CharSequence> selectedDivision = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedDivisionsCode = new ArrayList<CharSequence>();
    private String divisionString="";

    //protected String[] array_io ;

    private String[] psWiseIOArray;
    protected ArrayList<String> psWiseIONameList;// = new ArrayList<String>();
    protected ArrayList<String> psWiseIOCodeList;// = new ArrayList<String>();

    protected ArrayList<String> selectedIOName;// = new ArrayList<String>();
    protected ArrayList<String> selectedIOCode;// = new ArrayList<String>();
    private String ioString="";

    protected String[] array_FIRstatus ;
    private String fir_status="";
    //private ArrayAdapter<String> statusAdapter;

    CustomFIRStatusAdapter customFIRStatusAdapter;

    public boolean ioAfterPS = false;

    private String[] key_map;
    private String[] value_map;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.modified_case_search_layout, container, false);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        tv_ps_value = (TextView) view.findViewById(R.id.tv_ps_value);
        et_case_value = (EditText) view.findViewById(R.id.et_case_value);
        sp_caseYear_value = (Spinner) view.findViewById(R.id.sp_caseYear_value);
        tv_category_crime_value = (TextView) view.findViewById(R.id.tv_category_crime_value);
        tv_date_start_value = (TextView) view.findViewById(R.id.tv_date_start_value);
        tv_date_end_value = (TextView) view.findViewById(R.id.tv_date_end_value);
        sp_crimeTimeValue = (Spinner) view.findViewById(R.id.sp_crimeTimeValue);
        tv_modus_operandi = (TextView) view.findViewById(R.id.tv_modus_operandi);
        et_complaint_value = (EditText) view.findViewById(R.id.et_complaint_value);
        tv_io_value = (TextView) view.findViewById(R.id.tv_io_value);
        tv_divison_value = (TextView) view.findViewById(R.id.tv_divison_value);
        sp_status_value=(Spinner) view.findViewById(R.id.sp_status_value);
        et_briefFact_value= (EditText) view.findViewById(R.id.et_briefFact_value);

        tv_investingUnit_value = (TextView)view.findViewById(R.id.tv_investingUnit_value);
        tv_investingSec_value = (TextView)view.findViewById(R.id.tv_investingSec_value);

        scrollView1 = (ScrollView) view.findViewById(R.id.scrollView1);


        btn_Case_search = (Button) view.findViewById(R.id.btn_Case_search);
        btn_reset = (Button) view.findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_Case_search);
        Constants.buttonEffect(btn_reset);



        array_policeStation = new String[Constants.policeStationNameArrayList.size()];
        array_policeStation = Constants.policeStationNameArrayList.toArray(array_policeStation);

        array_crimeCategory = new String[Constants.crimeCategoryArrayList.size()];
        array_crimeCategory = Constants.crimeCategoryArrayList.toArray(array_crimeCategory);

        array_division=new String[Constants.divisionArrayList.size()];
        array_division=Constants.divisionArrayList.toArray(array_division);

        /*array_io=new String[Constants.IONameArrayList.size()];
        array_io=Constants.IONameArrayList.toArray(array_io);*/

        array_FIRstatus=new String[Constants.firStatusArrayList.size()];
        array_FIRstatus=Constants.firStatusArrayList.toArray(array_FIRstatus);

        Calendar calendar = Calendar.getInstance();
        int current_year = calendar.get(Calendar.YEAR);

        /* Case year Spinner set */
        caseYearList.clear();
        caseYearList.add("Enter Case Year");

        for(int i=current_year;i>=1980;i--){
            caseYearList.add(Integer.toString(i));
        }


        caseYearAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_custom_textcolor, caseYearList);

        caseYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_caseYear_value.setAdapter(caseYearAdapter);

        caseYearAdapter.notifyDataSetChanged();
        sp_caseYear_value.setSelection(0);


        sp_caseYear_value.setOnItemSelectedListener(this);

        /** spiner set to crime time*/
        crimeTimeAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.CrimeTime_Array, R.layout.simple_spinner_item);
        crimeTimeAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_crimeTimeValue.setAdapter(crimeTimeAdapter);

        sp_crimeTimeValue.setSelection(0);

        fetchInvestigateUnit();

        initStatusSpinner();

        clickEvents();

    }

    private void fetchInvestigateUnit(){

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_DIVISIONlIST);
        taskManager.setInvestigateUnit(true);

        String[] keys = {  };
        String[] values = { };
        taskManager.doStartTask(keys, values, true);

    }

    public void parseGetInvestigateUnitResponse(String response) {

        //System.out.println("parseGetInvestigateUnitResponse: "+ response);
        if(response != null && !response.equals("")){

            JSONObject jObj = null;
            try {

                jObj = new JSONObject(response);
                if(jObj != null && jObj.optString("status").equalsIgnoreCase("1")){
                    Constants.investigateUnitCodeArrayList.clear();
                    Constants.investigateUnitNameArrayList.clear();

                    JSONArray jsonAry = jObj.getJSONArray("result");

                    for(int i = 0; i<jsonAry.length(); i++){
                        JSONObject investingDiv_obj = jsonAry.getJSONObject(i);
                        DivisionList divList = new DivisionList();
                        if(investingDiv_obj.optString("DIVCODE") != null && !investingDiv_obj.optString("DIVCODE").equalsIgnoreCase("") && !investingDiv_obj.optString("DIVCODE").equalsIgnoreCase("null")){
                            divList.setDiv_code(investingDiv_obj.optString("DIVCODE"));
                            Constants.investigateUnitCodeArrayList.add(investingDiv_obj.optString("DIVCODE"));
                        }

                        if(investingDiv_obj.optString("DIVNAME") != null && !investingDiv_obj.optString("DIVNAME").equalsIgnoreCase("") && !investingDiv_obj.optString("DIVNAME").equalsIgnoreCase("null")){
                            divList.setDiv_name(investingDiv_obj.optString("DIVNAME"));
                            Constants.investigateUnitNameArrayList.add(investingDiv_obj.optString("DIVNAME"));
                        }
                    }

                    array_investigateUnit = new String[Constants.investigateUnitNameArrayList.size()];
                    array_investigateUnit = Constants.investigateUnitNameArrayList.toArray(array_investigateUnit);

                }

            }
            catch(JSONException e){
                e.printStackTrace();
            }

        }

    }


    private void clickEvents() {

         /* Police stations Click Event */

        tv_ps_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.policeStationNameArrayList.size()>0) {
                    tv_ps_value.setError(null);
                    showSelectPoliceStationsDialog();


                }
                else {
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }

            }
        });
         /* Crime Category Click Event */

        tv_category_crime_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Constants.crimeCategoryArrayList.size() > 0) {
                    //	showSelectCrimeCategoryDialog();
                    tv_category_crime_value.setText("Select a Category");
                    crimeCategoryString = "";
                    crimeCategory_status = true;
                    System.out.println("obj_categoryCrimeList2 size :" + CriminalSearchFragment.obj_categoryCrimeList2.size());

                    //customDialogAdapterForCategoryCrimeList = new CustomDialogAdapterForCategoryCrimeList(getActivity(),obj_categoryCrimeList);
                    customDialogAdapterForCategoryCrimeList2 = new CustomDialogAdapterForCategoryCrimeList2(getActivity(), CriminalSearchFragment.obj_categoryCrimeList2);
                    customDialogForCategoryCrimeList();
                } else {

                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }


            }
        });


        /* Start Date field click event */

        tv_date_start_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DateUtils.setDate(getActivity(), tv_date_start_value, true);
            }
        });

        /* End Date field click event */

        tv_date_end_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DateUtils.setDate(getActivity(), tv_date_end_value, true);
            }
        });


        /* Modus Operandi Click Event */

        tv_modus_operandi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CriminalSearchFragment.obj_modusOperandiList.size() > 0) {
                    //showModusOperandiDialog();
                    tv_modus_operandi.setText("Select an Operandi");
                    modusOperandiString = "";
                    modusOperandi_status = true;
                    //obj_modusOperandiList = allContentList.getObj_modusOperandiList();
                    System.out.println("Modus size:" + CriminalSearchFragment.obj_modusOperandiList.size());
                    customDialogAdapterForModusOperandi = new CustomDialogAdapterForModusOperandi(getActivity(), CriminalSearchFragment.obj_modusOperandiList);
                    customDialogForModusOperandi();

                } else {
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }
            }
        });




        btn_Case_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String startDate = tv_date_start_value.getText().toString().trim();
                String endDate = tv_date_end_value.getText().toString().trim();


                if (!endDate.equalsIgnoreCase("") && startDate.equalsIgnoreCase("")) {
                    showAlertDialog(getActivity(), " Search Error!!! ", "Please provide both dates", false);
                } else if (endDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("")) {
                    showAlertDialog(getActivity(), " Search Error!!! ", "Please provide both dates", false);
                } else if (!endDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("")) {
                    boolean date_result_flag = DateUtils.dateComparison(startDate, endDate);
                    System.out.println("DateComparison: " + date_result_flag);
                    if (!date_result_flag) {
                        showAlertDialog(getActivity(), " Search Error!!! ", "From date can't be greater than To date", false);
                    } else {
                        fetchCaseSearchResult();
                    }
                }else if(ioAfterPS == true && ioString.equalsIgnoreCase("")){

                    showAlertDialog(getActivity(), " Error!!! ", "Please select IO", false);
                }
                else {
                    fetchCaseSearchResult();
                }

            }
        });


        tv_divison_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.divisionArrayList.size()>0) {

                    Utility.setLogMessage("Division size","" + Constants.divisionArrayList.size());
                    showSelectDivisionDialog();
                }
                else {
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }

            }
        });

        tv_io_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!policeStationString.equalsIgnoreCase("")){

                    tv_io_value.setError(null);
                    fetchIOName(policeStationString);
                    ioAfterPS = false;
                }
                else{
                    //Toast.makeText(getActivity(),"Please select police stations",Toast.LENGTH_LONG).show();
                    ioAfterPS = true;
                    showAlertDialog(getActivity(), "Error!!! ", "Please select police stations", false);
                }
            }
        });


        tv_investingUnit_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.investigateUnitNameArrayList.size() > 0){
                    showInvestigateUnitDialog();
                }
                else{
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }
            }
        });


        tv_investingSec_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!invUnitString.equalsIgnoreCase("")){
                    fetchInvestigateSec(invUnitString);
                }
                else{
                    showAlertDialog(getActivity(),"Error!!! ","Please select investigating unit", false);
                }
            }
        });

          /* Resetting all fields to original state */

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resetDataFieldValues();

            }
        });

    }


    /*
    *  parsing date for Investigating Section part
    *
    * */
    private void fetchInvestigateSec(String invUnitString){

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_DIVISIONWISE_POLICESTATION);
        taskManager.setInvestigateSecSearch(true);

        String[] keys = {"divcode"};
        String[] values = {invUnitString.trim()};
        taskManager.doStartTask(keys, values, true);
    }

    public void parseInvestigateSectionResponse(String response) {

        //System.out.println("parseInvestigateSectionResponse :"+ response);

        if(response != null && !response.equals("")) {

            JSONObject jObj = null;
            try {

                jObj = new JSONObject(response);
                if(jObj != null && jObj.optString("status").equalsIgnoreCase("1")){

                    invSectionNameList = new ArrayList<String>();
                    invSectionCodeList = new ArrayList<String>();

                    JSONArray resultAry = jObj.getJSONArray("result");

                    for(int i=0; i<resultAry.length(); i++){

                        JSONObject invSecList = resultAry.getJSONObject(i);
                        if(invSecList.optString("PSNAME") != null && !invSecList.optString("PSNAME").equalsIgnoreCase("") && !invSecList.optString("PSNAME").equalsIgnoreCase("null")){
                            invSectionNameList.add(invSecList.optString("PSNAME"));
                        }
                        if(invSecList.optString("PSCODE") != null && !invSecList.optString("PSCODE").equalsIgnoreCase("") && !invSecList.optString("PSCODE").equalsIgnoreCase("null")){
                            invSectionCodeList.add(invSecList.optString("PSCODE"));
                        }
                    }

                    showInvestigateSectionDialog();
                }
                else{
                    showAlertDialog(getActivity(), " Error!!! ", "No data found", false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }


    /*
    *  Investigating section part  dialog show
    *
    * */
    private void showInvestigateSectionDialog(){

        selectedInvSectionName = new ArrayList<String>();
        selectedInvSectionCode = new ArrayList<String>();

        array_investigateSection = new String[invSectionNameList.size()];
        array_investigateSection = invSectionNameList.toArray(array_investigateSection);

        boolean[] checkedInvSection = new boolean[array_investigateSection.length];
        int count = array_investigateSection.length;

        for(int i = 0; i < count; i++) {
            checkedInvSection[i] = selectedInvSectionName.contains(array_investigateSection[i]);
        }

        DialogInterface.OnMultiChoiceClickListener invSectionDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedInvSectionName.add(array_investigateSection[which]);
                    selectedInvSectionCode.add((String)invSectionCodeList.get(which));

                } else{
                    selectedInvSectionName.remove(array_investigateSection[which]);
                    selectedInvSectionCode.remove((String) invSectionCodeList.get(which));

                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Investigating Section");
        builder.setMultiChoiceItems(array_investigateSection, checkedInvSection, invSectionDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedInvSection();
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_investingSec_value.setText("Select Investigating Section");
                invSectionString ="";
                selectedInvSectionName.clear();
                selectedInvSectionCode.clear();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    protected void onChangeSelectedInvSection() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for(String invSection : selectedInvSectionName){
            stringBuilder.append(invSection + ",");
        }

        for(String invSection : selectedInvSectionCode){
            stringBuilderId.append("\'"+invSection + "\',");
        }

        if(selectedInvSectionName.size()==0) {
            tv_investingSec_value.setText("Select Investigating Section");
            invSectionString="";
        }
        else {
            tv_investingSec_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
            invSectionString=stringBuilderId.toString().substring(0,stringBuilderId.toString().length()-1);
            Log.e("Tag:","Tag7 : "+invSectionString);
        }

    }


    /*
    *  show Investigating Unit part
    * */

    protected void showInvestigateUnitDialog(){

        selectedInvestigateUnit.clear();
        selectedInvestigateUnitCode.clear();

        boolean[] checkedInvUnit = new boolean[array_investigateUnit.length];
        int count  = array_investigateUnit.length;

        for (int i = 0; i< count; i++){
            checkedInvUnit[i] = selectedInvestigateUnit.contains(array_investigateUnit);
        }

        DialogInterface.OnMultiChoiceClickListener invUnitDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                if(isChecked){
                    selectedInvestigateUnit.add(array_investigateUnit[which]);
                    selectedInvestigateUnitCode.add((String) Constants.investigateUnitCodeArrayList.get(which));
                }
                else{
                    selectedInvestigateUnit.remove(array_investigateUnit[which]);
                    selectedInvestigateUnitCode.remove((String) Constants.investigateUnitCodeArrayList.get(which));
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Investigating Unit");
        builder.setMultiChoiceItems(array_investigateUnit,checkedInvUnit,invUnitDialogListener);

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onChangeSelectedInvUnit();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tv_investingUnit_value.setText("Select Investigating Unit");
                invUnitString = "";
                selectedInvestigateUnit.clear();
                selectedInvestigateUnitCode.clear();

                tv_investingSec_value.setText("Select Investigating Section");
                invSectionString = "";
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    protected void onChangeSelectedInvUnit() {

        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for(String invUnit : selectedInvestigateUnit){
            stringBuilder.append(invUnit + ",");
        }

        for(String invUnit : selectedInvestigateUnitCode){
            stringBuilderId.append("\'"+invUnit + "\',");
        }

        if(selectedInvestigateUnit.size()==0) {
            tv_investingUnit_value.setText("Select Investigating Unit");
            invUnitString="";
        }
        else {
            tv_investingUnit_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
            invUnitString=stringBuilderId.toString().substring(0,stringBuilderId.toString().length()-1);
            Log.e("Tag:","Tag6 : "+invUnitString);
        }

    }


    protected void showSelectDivisionDialog() {

        boolean[] checkedDivisions = new boolean[array_division.length];
        int count = array_division.length;

        for(int i = 0; i < count; i++) {
            checkedDivisions[i] = selectedDivision.contains(array_division[i]);
        }

        DialogInterface.OnMultiChoiceClickListener divisionDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedDivision.add(array_division[which]);
                    selectedDivisionsCode.add((CharSequence) Constants.divCodeArrayList.get(which));
                } else{
                    selectedDivision.remove(array_division[which]);
                    selectedDivisionsCode.remove((CharSequence) Constants.divCodeArrayList.get(which));
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Division");
        builder.setMultiChoiceItems(array_division, checkedDivisions, divisionDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedDivisions();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_divison_value.setText("Select Division");
                divisionString="";
                selectedDivision.clear();
                selectedDivisionsCode.clear();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedDivisions() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for(CharSequence division : selectedDivision){
            stringBuilder.append(division + ",");
        }

        for(CharSequence division : selectedDivisionsCode){
            stringBuilderId.append("\'"+division + "\',");
        }

        if(selectedDivision.size()==0) {
            tv_divison_value.setText("Select Division");
            divisionString="";
        }
        else {
            tv_divison_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
            divisionString=stringBuilderId.toString().substring(0,stringBuilderId.toString().length()-1);
        }

    }

    private void fetchIOName(String policeStationString){

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_PS_WISE_IO);
        taskManager.setPsWiseIOListForCaseSearch(true);

        String[] keys = {"ps"};
        String[] values = {policeStationString.trim()};
        taskManager.doStartTask(keys, values, true);
    }

    public void parsePSWiseIOForCaseSearchResponse(String response){

        //System.out.println("PS wise IO name: "+ response);

        if(response != null && !response.equals("")){

            JSONObject jobj = null;
            try{

                jobj = new JSONObject(response);
                if(jobj != null && jobj.optString("status").equalsIgnoreCase("1")){


                    JSONArray ps_wise_io_array = jobj.getJSONArray("result");

                    psWiseIONameList = new ArrayList<String>();
                    psWiseIOCodeList = new ArrayList<String>();

                    for(int i=0; i<ps_wise_io_array.length(); i++){

                        JSONObject io_list = ps_wise_io_array.getJSONObject(i);
                        psWiseIONameList.add(io_list.optString("IONAME"));
                        psWiseIOCodeList.add(io_list.optString("IOCODE"));

                    }
                }else{
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }

            }catch (JSONException e){
                e.printStackTrace();
            }

            showSelectIODialog();

        }
    }

    protected void showSelectIODialog() {

        selectedIOName = new ArrayList<String>();
        selectedIOCode = new ArrayList<String>();

        psWiseIOArray=new String[psWiseIONameList.size()];
        psWiseIOArray=psWiseIONameList.toArray(psWiseIOArray);

        boolean[] checkedIO = new boolean[psWiseIOArray.length];
        int count = psWiseIOArray.length;

        for(int i = 0; i < count; i++) {
            checkedIO[i] = selectedIOName.contains(psWiseIOArray[i]);
        }

        DialogInterface.OnMultiChoiceClickListener ioDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedIOName.add(psWiseIOArray[which]);
                    selectedIOCode.add((String)psWiseIOCodeList .get(which));

                } else{
                    selectedIOName.remove(psWiseIOArray[which]);
                    selectedIOCode.remove((String) psWiseIOCodeList.get(which));

                }
//				onChangeSelectedPoliceStations();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select IO");
        builder.setMultiChoiceItems(psWiseIOArray, checkedIO, ioDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedIO();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_io_value.setText("Select IO");
                ioString="";
                selectedIOName.clear();
                selectedIOCode.clear();
                ioAfterPS = true;
                tv_io_value.setError("");
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedIO() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        /*stringBuilderNew is used for setting data to tv_io_value*/
        StringBuilder stringBuilderNew = new StringBuilder();

        for(CharSequence io : selectedIOName){
            //stringBuilder.append(io + ",");
            stringBuilder.append("\'"+io + "\',");
            stringBuilderNew.append(io + ",");
        }

        for(CharSequence io : selectedIOCode){
            stringBuilderId.append("\'"+io + "\',");
        }

        if(selectedIOName.size()==0) {
            tv_io_value.setText("Select IO");
            ioString="";
            ioAfterPS = true;
            tv_io_value.setError("");
        }
        else {
            //tv_io_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
            //ioString=stringBuilderId.toString().substring(0,stringBuilderId.toString().length()-1);
            tv_io_value.setText(stringBuilderNew.toString().substring(0,stringBuilderNew.toString().length()-1));
            ioString=stringBuilder.toString().substring(0,stringBuilder.toString().length()-1);
        }

    }


    	 /* This method shows Police Station list in a pop-up */

    protected void showSelectPoliceStationsDialog() {
        boolean[] checkedPoliceStations = new boolean[array_policeStation.length];
        int count = array_policeStation.length;

        for(int i = 0; i < count; i++) {
            checkedPoliceStations[i] = selectedPoliceStations.contains(array_policeStation[i]);
        }

        DialogInterface.OnMultiChoiceClickListener policeStationsDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedPoliceStations.add(array_policeStation[which]);
                    selectedPoliceStationsId.add((CharSequence) Constants.policeStationIDArrayList.get(which));
                } else{
                    selectedPoliceStations.remove(array_policeStation[which]);
                    selectedPoliceStationsId.remove((CharSequence) Constants.policeStationIDArrayList.get(which));
                }

            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Police Stations");
        builder.setMultiChoiceItems(array_policeStation, checkedPoliceStations, policeStationsDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button

                onChangeSelectedPoliceStations();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_ps_value.setText("Select Police Stations");
                policeStationString="";
                selectedPoliceStations.clear();
                selectedPoliceStationsId.clear();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedPoliceStations() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        if(ioAfterPS){

            scrollView1.scrollTo(0,scrollView1.getBottom());
            tv_io_value.requestFocus();
            tv_io_value.setError("");
        }

        for(CharSequence policeStation : selectedPoliceStations){
            stringBuilder.append(policeStation + ",");
        }

        for(CharSequence policeStation : selectedPoliceStationsId){
            stringBuilderId.append("\'"+policeStation + "\',");
        }

        if(selectedPoliceStations.size()==0) {
            tv_ps_value.setText("Select Police Stations");
            policeStationString="";
        }
        else {
            tv_ps_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
            policeStationString=stringBuilderId.toString().substring(0,stringBuilderId.toString().length()-1);
        }

    }

    //---------------------------------------------------------------------------------------------------------------//


	                                  /* Custom Dialog for Crime Category */

    //---------------------------------------------------------------------------------------------------------------//

    private void customDialogForCategoryCrimeList(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);

        TextView tv_title = (TextView)dialogView.findViewById(R.id.tv_title);
        Button btn_cancel = (Button)dialogView.findViewById(R.id.btn_cancel);
        Button btn_done = (Button)dialogView.findViewById(R.id.btn_done);
        lv_dialog = (ListView)dialogView.findViewById(R.id.lv_dialog);
        SearchView searchView = (SearchView) dialogView.findViewById(R.id.searchView);


        Constants.changefonts(tv_title, getActivity(), "Calibri Bold.ttf");
        tv_title.setText("Select Crime Category");

		/*customDialogAdapterForCategoryCrimeList.notifyDataSetChanged();
		lv_dialog.setAdapter(customDialogAdapterForCategoryCrimeList);*/

        customDialogAdapterForCategoryCrimeList2.notifyDataSetChanged();
        lv_dialog.setAdapter(customDialogAdapterForCategoryCrimeList2);
        //lv_dialog.setScrollingCacheEnabled(false);
        lv_dialog.setTextFilterEnabled(true);

        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(ModifiedCaseSearchFragment_old.this);
        searchView.setSubmitButtonEnabled(false);
        searchView.setQueryHint("Search Here");



        final AlertDialog alertDialog = builder.show();

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                crimeCategory_status = false;
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(alertDialog != null && alertDialog.isShowing()){
                    alertDialog.dismiss();

                }
                crimeCategory_status = false;

            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("No of Selected Items: "+ CustomDialogAdapterForCategoryCrimeList2.selectedItemList.size());
                System.out.println("Selected Items: "+ CustomDialogAdapterForCategoryCrimeList2.selectedItemList);
                crimeCategory_status = false;

                onSelectedCategoryOfCrime(CustomDialogAdapterForCategoryCrimeList2.selectedItemList);

                if(alertDialog != null && alertDialog.isShowing()){
                    alertDialog.dismiss();

                }

            }
        });

    }

    private void onSelectedCategoryOfCrime(List<String> crimeCategoryList){

        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderCrimeCategory = new StringBuilder();

        for(CharSequence crimeCategory : crimeCategoryList) {
            stringBuilder.append(crimeCategory + ",");

            String crimeCategoryModifiedString = stringBuilderCrimeCategory.append("\'"+crimeCategory.toString() + "\',").toString();

            crimeCategoryString = crimeCategoryModifiedString.substring(0,crimeCategoryModifiedString.length()-1);
        }

        if(crimeCategoryList.size()==0) {
            tv_category_crime_value.setText("Select a Category");
        }
        else {
            System.out.println("Crime Category String: "+crimeCategoryString);
            tv_category_crime_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
        }

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(crimeCategory_status){
            customDialogAdapterForCategoryCrimeList2.filter(newText.toString());
        }
        else if(modusOperandi_status){
            customDialogAdapterForModusOperandi.filter(newText.toString());
        }

        return true;
    }

    //--------------------------------------------------------------------------------------------------------------//


	                                /* Custom Dialog for Modus Operandi */

    //-------------------------------------------------------------------------------------------------------------//


    private void customDialogForModusOperandi(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);

        TextView tv_title = (TextView)dialogView.findViewById(R.id.tv_title);
        Button btn_cancel = (Button)dialogView.findViewById(R.id.btn_cancel);
        Button btn_done = (Button)dialogView.findViewById(R.id.btn_done);
        lv_dialog = (ListView)dialogView.findViewById(R.id.lv_dialog);
        SearchView searchView = (SearchView)dialogView.findViewById(R.id.searchView);


        Constants.changefonts(tv_title, getActivity(), "Calibri Bold.ttf");
        tv_title.setText("Select Modus Operandi");

        customDialogAdapterForModusOperandi.notifyDataSetChanged();
        lv_dialog.setAdapter(customDialogAdapterForModusOperandi);
        //lv_dialog.setScrollingCacheEnabled(false);
        lv_dialog.setTextFilterEnabled(true);

        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(ModifiedCaseSearchFragment_old.this);
        searchView.setSubmitButtonEnabled(false);
        searchView.setQueryHint("Search Here");


        final AlertDialog alertDialog = builder.show();

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                modusOperandi_status = false;

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                modusOperandi_status = false;

                if(alertDialog != null && alertDialog.isShowing()){
                    alertDialog.dismiss();

                }

            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("No of Selected Items: "+ CustomDialogAdapterForModusOperandi.selectedItemList.size());
                System.out.println("Selected Items: "+ CustomDialogAdapterForModusOperandi.selectedItemList);

                modusOperandi_status = false;

                onSelectedModusOperandi(CustomDialogAdapterForModusOperandi.selectedItemList);

                if(alertDialog != null && alertDialog.isShowing()){
                    alertDialog.dismiss();

                }

            }
        });

    }


    private void onSelectedModusOperandi(List<String> modusOperandiList){


        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderModusOperandi = new StringBuilder();

        for(CharSequence modusOperandi : modusOperandiList) {
            stringBuilder.append(modusOperandi + ",");

            String modusOperandiModifiedString = stringBuilderModusOperandi.append("\'"+modusOperandi.toString() + "\',").toString();

            modusOperandiString = modusOperandiModifiedString.substring(0,modusOperandiModifiedString.length()-1);

        }

        if(modusOperandiList.size()==0) {
            tv_modus_operandi.setText("Select an Operandi");
            modusOperandiString="";
        }
        else {
            System.out.println("Modus Operandi String: "+modusOperandiString);
            tv_modus_operandi.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        caseYear = sp_caseYear_value.getSelectedItem().toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

        sp_caseYear_value.setSelection(0);

    }

    private void fetchCaseSearchResult() {

        String case_no=et_case_value.getText().toString().trim();
        String caseYear=sp_caseYear_value.getSelectedItem().toString().trim().replace("Enter Case Year", "");
        String date_from=tv_date_start_value.getText().toString().trim();
        String date_to=tv_date_end_value.getText().toString().trim();
        String crime_time="";
        String status = fir_status.trim().replace("Select a status","");

        String complainant=et_complaint_value.getText().toString().trim();
        String brief_fact = et_briefFact_value.getText().toString().trim().replace("Enter Brief Fact","");


        if(!sp_crimeTimeValue.getSelectedItem().toString().trim().replace("Select time of crime","").equalsIgnoreCase("")){
            crime_time = "\'"+ sp_crimeTimeValue.getSelectedItem().toString().trim().replace("Select time of crime","") +"\'";
        }

        if(!policeStationString.equalsIgnoreCase("") || !case_no.equalsIgnoreCase("")
                || !caseYear.equalsIgnoreCase("") || !date_from.equalsIgnoreCase("") || !date_to.equalsIgnoreCase("")
                || !crimeCategoryString.equalsIgnoreCase("") || !modusOperandiString.equalsIgnoreCase("") || !crime_time.equalsIgnoreCase("")
                || !complainant.equalsIgnoreCase("") || !ioString.equalsIgnoreCase("") || !divisionString.equalsIgnoreCase("")
                || !brief_fact.equalsIgnoreCase("")  || !status.equalsIgnoreCase("") || !invUnitString.equalsIgnoreCase("") || !invSectionString.equalsIgnoreCase("")){

            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_MODIFIED_CASE_SEARCH);
            taskManager.setModifedCaseSearch(true);

            String newStatus="";
            if(status!=null && !status.equalsIgnoreCase("")) {
                if(status.contains("'")){
                    status=status.replace("'","");
                }
                newStatus = "'" + status + "'";
            }

            String[] keys = { "caseno", "caseyear",
                    "datefrom", "dateto", "policestations", "crimecategory","crimetime","mod_oper","pageno", "complaint", "io", "divisions", "brief_keyword","status", "inv_unit", "inv_section"};

            String[] values = { case_no, caseYear, date_from, date_to ,policeStationString, crimeCategoryString, crime_time, modusOperandiString, "1", complainant, ioString, divisionString, brief_fact, newStatus, invUnitString, invSectionString };

            key_map = new String[]{"div", "ps", "crime_cat", "from_date", "to_date", "caseno", "caseyear", "crimetime", "mod_oper", "complaint", "status", "io", "brief_keyword", "pageno", "inv_unit", "inv_section"};
            value_map = new String[]{divisionString, policeStationString, crimeCategoryString, date_from, date_to, case_no, caseYear, crime_time, modusOperandiString, complainant, newStatus, ioString, brief_fact,"1", invUnitString, invSectionString };

            taskManager.doStartTask(keys, values, true);

        }else{

            showAlertDialog(getActivity(), " Search Error!!! ", "Please provide atleast one value for search", false);

        }

    }

    public void parseModifiedCaseSearchResult(String result, String[] keys, String[] values) {

        //System.out.println("ParseModifiedCaseSearchResult: " + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("pageno").toString();
                    totalResult = jObj.opt("totalresult").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseCaseSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(getActivity(),Constants.SEARCH_ERROR_TITLE,Constants.ERROR_MSG_DETAIL,false);
                }


            } catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
            }

        }

    }

    private void parseCaseSearchResponse(JSONArray result_array){

        caseSearchDetailsList.clear();

        for(int i=0;i<result_array.length();i++){

            JSONObject obj = null;

            try {

                obj = result_array.getJSONObject(i);

                CaseSearchDetails caseSearchDetails = new CaseSearchDetails();

                if(!obj.optString("ROWNUMBER").equalsIgnoreCase("null"))
                    caseSearchDetails.setRowNumber(obj.optString("ROWNUMBER"));
                if(!obj.optString("PSCODE").equalsIgnoreCase("null"))
                    caseSearchDetails.setPsCode(obj.optString("PSCODE"));
                if(!obj.optString("PS").equalsIgnoreCase("null"))
                    caseSearchDetails.setPs(obj.optString("PS"));
                if(!obj.optString("CASENO").equalsIgnoreCase("null"))
                    caseSearchDetails.setCaseNo(obj.optString("CASENO"));
                if(!obj.optString("CASEDATE").equalsIgnoreCase("null"))
                    caseSearchDetails.setCaseDate(obj.optString("CASEDATE"));
                if(!obj.optString("UNDER_SECTION").equalsIgnoreCase("null"))
                    caseSearchDetails.setUnderSection(obj.optString("UNDER_SECTION"));
                if(!obj.optString("CATEGORY").equalsIgnoreCase("null"))
                    caseSearchDetails.setCategory(obj.optString("CATEGORY"));
                if(!obj.optString("CASE_YR").equalsIgnoreCase("null"))
                    caseSearchDetails.setCaseYr(obj.optString("CASE_YR"));
                if(!obj.optString("FIR_STATUS").equalsIgnoreCase("null"))
                    caseSearchDetails.setFirStatus(obj.optString("FIR_STATUS"));

                if(!obj.optString("OCCUR_TIMING").equalsIgnoreCase("null"))
                    caseSearchDetails.setOccurTime(obj.optString("OCCUR_TIMING"));
                if(!obj.optString("MOD_OPER").equalsIgnoreCase("null"))
                    caseSearchDetails.setModOper(obj.optString("MOD_OPER"));
                if(!obj.optString("PO_LAT").equalsIgnoreCase("null") && !obj.optString("PO_LAT").equalsIgnoreCase("") && obj.optString("PO_LAT") != null)
                    caseSearchDetails.setPoLat(obj.optString("PO_LAT"));
                if(!obj.optString("PO_LONG").equalsIgnoreCase("null") && !obj.optString("PO_LONG").equalsIgnoreCase("") && obj.optString("PO_LONG") != null)
                    caseSearchDetails.setPoLong(obj.optString("PO_LONG"));

                JSONArray briefMatch_array = obj.getJSONArray("brief_match");
                if(briefMatch_array.length() > 0){
                    List<String> briefMatch_list = new ArrayList<>();
                    for(int j=0;j<briefMatch_array.length();j++){
                        briefMatch_list.add(briefMatch_array.optString(j));
                    }
                    caseSearchDetails.setBriefMatch_list(briefMatch_list);
                }

                caseSearchDetailsList.add(caseSearchDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Intent intent=new Intent(getActivity(), ModifiedCaseSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putExtra("key_map",key_map);
        intent.putExtra("value_map",value_map);
        intent.putExtra("MODIFIED_CASE_SEARCH_LIST", (Serializable) caseSearchDetailsList);
        startActivity(intent);

    }

    private void resetDataFieldValues(){

        //* for police stations part*//*
        tv_ps_value.setText("Select Police Stations");
        policeStationString="";
        selectedPoliceStations.clear();
        selectedPoliceStationsId.clear();
        tv_ps_value.setError(null);

        //* for Division part*//*
        tv_divison_value.setText("Select Divisions");
        divisionString="";
        selectedDivision.clear();
        selectedDivisionsCode.clear();

        //* for IO part*//*
        tv_io_value.setText("Select IO");
        ioString="";
        tv_io_value.setError(null);


        //* for case no part*//*
        et_case_value.setText("");

        //*  for case year part *//*
        sp_caseYear_value.setSelection(0);
        caseYear="";

        //* for Start Date part *//*
        tv_date_start_value.setText("");

        //* for End Date part *//*
        tv_date_end_value.setText("");

        //* for Category of crime part*//*
        tv_category_crime_value.setText("Select a Category");
        crimeCategoryString="";


        //*  for modus operandi part *//*
        modusOperandiString = "";
        tv_modus_operandi.setText("Select an Operandi");

        //*  for time of crime part *//*
        sp_crimeTimeValue.setSelection(0);


        //*  for Complainant part *//*
        et_complaint_value.setText("");

        //*  for BriefFact part *//*
        et_briefFact_value.setText("");

        //*  for Status part *//*
        sp_status_value.setSelection(0);

        ioAfterPS = false;

        tv_investingUnit_value.setText("Select Investigating Unit");
        invUnitString="";
        selectedInvestigateUnit.clear();
        selectedInvestigateUnitCode.clear();

        tv_investingSec_value.setText("Select Investigating Section");
        invSectionString="";
        /*selectedInvSectionName.clear();
        selectedInvSectionCode.clear();*/
    }

    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     * */
    private void showAlertDialog(Context context, String title, final String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();

                            if(message.equalsIgnoreCase("Please provide atleast one value for search")){
                                tv_ps_value.setError(null);
                            }
                            else if(!policeStationString.equalsIgnoreCase("") && ioString.equalsIgnoreCase("")){
                                tv_ps_value.setError(null);
                            }
                            else if(ioString.equalsIgnoreCase("") && ioAfterPS == true){
                                scrollView1.scrollTo(tv_io_value.getScrollX(),tv_ps_value.getScrollY());
                                tv_ps_value.requestFocus();
                                tv_ps_value.setError("");
                                //ioAfterPS = false;
                            }
                            /*if(ioString.equalsIgnoreCase("") && ioAfterPS == true && !policeStationString.equalsIgnoreCase("")){
                                *//*scrollView1.scrollTo(tv_io_value.getScrollX(),tv_ps_value.getScrollY());
                                tv_ps_value.requestFocus();
                                tv_ps_value.setError("");
                                //ioAfterPS = false;*//*
                                fetchCaseSearchResult();
                            }*/
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    private void initStatusSpinner(){

        customFIRStatusAdapter=new CustomFIRStatusAdapter(getActivity(),array_FIRstatus);
        sp_status_value.setAdapter(customFIRStatusAdapter);

        //customFIRStatusAdapter.notifyDataSetChanged();

        sp_status_value.setSelection(0);

        sp_status_value.setOnItemSelectedListener(this);

        sp_status_value.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ((TextView) view.findViewById(R.id.tv_itemName)).setTextColor(ContextCompat.getColor(getActivity(),R.color.color_black));
                fir_status = Constants.firStatusArrayList.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                sp_status_value.setSelection(0);
                fir_status = Constants.firStatusArrayList.get(0);

            }
        });

    }


}
