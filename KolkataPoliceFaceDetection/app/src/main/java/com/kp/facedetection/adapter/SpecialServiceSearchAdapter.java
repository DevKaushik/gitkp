package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnSpecialServiceSearchListListener;
import com.kp.facedetection.model.Result;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;

import java.util.List;

/**
 * Created by user on 30-04-2018.
 */

public class SpecialServiceSearchAdapter extends RecyclerView.Adapter<SpecialServiceSearchAdapter.ViewHolder>{
    List<Result> dataResult;
    OnSpecialServiceSearchListListener onSpecialServiceSearchListListener;
    Context mContext;
    public SpecialServiceSearchAdapter(Context context, List<Result> dataResult) {
        this.mContext = context;
        this.dataResult = dataResult;
    }
    public  void setSpecialServiceSearchListListener(OnSpecialServiceSearchListListener onSpecialServiceSearchListListener){
        this.onSpecialServiceSearchListListener=onSpecialServiceSearchListListener;
    }

    @Override
    public SpecialServiceSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.special_services_inflater, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SpecialServiceSearchAdapter.ViewHolder holder, int position) {
        holder.name_TV.setText(dataResult.get(position).getOfficerName());
        holder.caseRef_TV.setText(dataResult.get(position).getCaseRef());
        holder.reqType_TV.setText(dataResult.get(position).getRequestType());
        if(dataResult.get(position).getReconnection().equalsIgnoreCase("1")){
            holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[0]);
        }
        else if(dataResult.get(position).getDisconnection().equalsIgnoreCase("1")){
            holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[1]);
        }  else {
            holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2]);
        }

        holder.div_TV.setText(dataResult.get(position).getDiv()+"/"+dataResult.get(position).getPs());
        String date_time=dataResult.get(position).getRequestTime();
        String date="";
        if(date_time.contains(" ")) {
            String[] dateTimeArray= date_time.split(" ");
            date=dateTimeArray[0];
            String formatedDate= DateUtils.changeDateFormat(date);
            holder.date_time.setText(formatedDate);
        }

        holder.status.setBackgroundColor(Color.parseColor(dataResult.get(position).getStatusColour()));
        holder.status.setText(dataResult.get(position).getStatusMsg());

        Constants.changefonts(holder.name_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.caseRef_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.reqType_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.div_request_subtype_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.div_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.status_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.date_time_label, mContext,"Calibri Bold.ttf");

        Constants.changefonts(holder.name_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.caseRef_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.reqType_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.div_request_subtype_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.div_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.status, mContext, "Calibri.ttf");
        Constants.changefonts(holder.date_time, mContext, "Calibri.ttf");

    }

    @Override
    public int getItemCount() {
        return (dataResult != null ? dataResult.size() : 0);
    }
    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView caseRef_TV,name_TV,reqType_TV,div_TV,status,div_request_subtype_TV,date_time;
        TextView caseRef_TV_label,name_TV_label,reqType_TV_label,div_TV_label,status_label,div_request_subtype_label,date_time_label;
        LinearLayout ll_special_service_request_item;


        public ViewHolder(View itemView) {
            super(itemView);
            ll_special_service_request_item=(LinearLayout)itemView.findViewById(R.id.ll_special_service_request_item);
            name_TV = (TextView) itemView.findViewById(R.id.name_TV);
            caseRef_TV = (TextView) itemView.findViewById(R.id.caseRef_TV);
            reqType_TV = (TextView) itemView.findViewById(R.id.reqType_TV);
            div_request_subtype_TV=(TextView) itemView.findViewById(R.id.div_request_subtype_TV);
            div_request_subtype_label=(TextView)itemView.findViewById(R.id.div_request_subtype_label);
            div_TV =(TextView) itemView.findViewById(R.id.div_TV);
            status = (TextView) itemView.findViewById(R.id.status);
            date_time = (TextView) itemView.findViewById(R.id.date_time);
            caseRef_TV_label=(TextView) itemView.findViewById(R.id.caseRef_TV_label);
            name_TV_label=(TextView) itemView.findViewById(R.id.name_TV_label);
            reqType_TV_label=(TextView) itemView.findViewById(R.id.reqType_TV_label);
            div_TV_label=(TextView) itemView.findViewById(R.id.div_TV_label);
            status_label=(TextView) itemView.findViewById(R.id.status_label);
            date_time_label=(TextView) itemView.findViewById(R.id.date_time_label);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onSpecialServiceSearchListListener.onSpecialServiceSearchListItemListener(getLayoutPosition());
        }
    }
}
