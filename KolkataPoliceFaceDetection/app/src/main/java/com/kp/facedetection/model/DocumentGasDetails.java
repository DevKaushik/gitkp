package com.kp.facedetection.model;

/**
 * Created by DAT-165 on 10-09-2016.
 */
public class DocumentGasDetails {

    private String gasId="";
    private String gasName="";
    private String gasAlias="";

    public String getGasId() {
        return gasId;
    }

    public void setGasId(String gasId) {
        this.gasId = gasId;
    }

    public String getGasName() {
        return gasName;
    }

    public void setGasName(String gasName) {
        this.gasName = gasName;
    }

    public String getGasAlias() {
        return gasAlias;
    }

    public void setGasAlias(String gasAlias) {
        this.gasAlias = gasAlias;
    }
}
