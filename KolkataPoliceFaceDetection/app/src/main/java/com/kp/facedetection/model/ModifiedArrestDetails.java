package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 14-07-2016.
 */
public class ModifiedArrestDetails implements Serializable {

    private String aliasName="";
    private String fatherOrHusbandName="";
    private String age="";
    private String sex="";
    private String arrestedOn="";
    private String arrestedBy="";
    private String placeOfArrest="";
    private String underSection="";
    private String categoryOfCrime="";
    private String caseReference="";
    private String psName="";
    private String caseYear="";
    private String caseDate="";


    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getArrestedOn() {
        return arrestedOn;
    }

    public void setArrestedOn(String arrestedOn) {
        this.arrestedOn = arrestedOn;
    }

    public String getArrestedBy() {
        return arrestedBy;
    }

    public void setArrestedBy(String arrestedBy) {
        this.arrestedBy = arrestedBy;
    }

    public String getPlaceOfArrest() {
        return placeOfArrest;
    }

    public void setPlaceOfArrest(String placeOfArrest) {
        this.placeOfArrest = placeOfArrest;
    }

    public String getUnderSection() {
        return underSection;
    }

    public void setUnderSection(String underSection) {
        this.underSection = underSection;
    }

    public String getCategoryOfCrime() {
        return categoryOfCrime;
    }

    public void setCategoryOfCrime(String categoryOfCrime) {
        this.categoryOfCrime = categoryOfCrime;
    }

    public String getCaseReference() {
        return caseReference;
    }

    public void setCaseReference(String caseReference) {
        this.caseReference = caseReference;
    }

    public String getFatherOrHusbandName() {
        return fatherOrHusbandName;
    }

    public void setFatherOrHusbandName(String fatherOrHusbandName) {
        this.fatherOrHusbandName = fatherOrHusbandName;
    }

    public String getPsName() {
        return psName;
    }

    public void setPsName(String psName) {
        this.psName = psName;
    }

    public String getCaseYear() {
        return caseYear;
    }

    public void setCaseYear(String caseYear) {
        this.caseYear = caseYear;
    }

    public String getCaseDate() {
        return caseDate;
    }

    public void setCaseDate(String caseDate) {
        this.caseDate = caseDate;
    }
}
