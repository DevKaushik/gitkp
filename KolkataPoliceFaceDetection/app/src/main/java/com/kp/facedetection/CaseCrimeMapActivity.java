package com.kp.facedetection;

import android.Manifest;
import android.app.ActionBar;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kp.facedetection.model.CaseSearchFIRDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class CaseCrimeMapActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener, Observer {

    GoogleMap mMap, mDivMap, mTPMap, mPubMap;
    private Button bt_prev, bt_next,bt_showColorCategory;

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    String jsonString;
    ArrayList<HashMap<String, String>> featureList ;
    ArrayList<HashMap<String, String>> featureTP ;
    ArrayList<HashMap<String, String>> featurePublic ;

    ImageView iv_plus, iv_minus;
    ImageView fab;
    String f1 = "" ,f2 = "" ,f3 = "";
    double radi = 50.00;
    double lati, longi, p_lat, p_long;
    Marker MarkerDiv, MarkerTp, MarkerPub;
    MarkerOptions markerDiv,markerTp,markerPub;
    List<CaseSearchFIRDetails>mapSearchDetailsList;

    LatLngBounds.Builder builder;
    CameraUpdate cu;
    TextView chargesheetNotificationCount;
    String notificationCount="";
    Circle circle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_search_result);
        ObservableObject.getInstance().addObserver(this);

        featureList = new ArrayList<>();
        featureTP= new ArrayList<>();
        featurePublic = new ArrayList<>();

        iv_plus = (ImageView)findViewById(R.id.iv_plus);
        iv_minus = (ImageView)findViewById(R.id.iv_minus);

        setUpMap();
        divcamparser();
        tpcamparser();
        pubcamparser();


        initViews();

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
            mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
        mapSearchDetailsList = (ArrayList<CaseSearchFIRDetails>)getIntent().getSerializableExtra("MAP_SEARCH");
        L.e("Case no:-----" + mapSearchDetailsList.get(0).getCaseNo()+"Case Year:----" + mapSearchDetailsList.get(0).getCaseYr());

    }

    private void pubcamparser() {
        loadJSONFromRawPUB();
        if (jsonString != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonString);
                JSONArray featurestp = jsonObj.getJSONArray("features");
                for (int i = 0; i < featurestp.length(); i++) {
                    Log.e("COUNT",String.valueOf(featurestp.length()));
                    JSONObject c = featurestp.getJSONObject(i);

                    JSONObject propert = c.getJSONObject("properties");
                    String name = propert.getString("location");
                    String psname = propert.getString("ps");
                    String div = propert.getString("division");

                    JSONObject geomet = c.getJSONObject("geometry");
                    String type = geomet.getString("type");
                    String tempcordi = geomet.getString("coordinates");
                    String temptwo_cordi = tempcordi.replace( "[", "" );
                    String cordi = temptwo_cordi.replace( "]", "" );
                    String[] separated = cordi.split(",");
                    String  lat = separated[0].trim();
                    String  longi = separated[1].trim();

                    HashMap<String, String> feature = new HashMap<>();

                    feature.put("Name", name);
                    feature.put("psname", psname);
                    feature.put("div", div);
                    feature.put("type", type);
                    feature.put("lat", lat);
                    feature.put("long", longi);

                    // adding contact to contact list
                    featurePublic.add(feature);
                }
                Log.e("TOTAL", String.valueOf(featurePublic.size()));
            } catch (final JSONException e) {
                Log.e("RESULT", "Json parsing error: " + e.getMessage());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Json parsing error: " + e.getMessage(),
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }
        } else {
            Log.e("NORESULT", "Couldn't get json from server.");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(),
                            "Couldn't get json from server. Check LogCat for possible errors!",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
        }
    }

    private void loadJSONFromRawPUB() {
        InputStream is = getResources().openRawResource(R.raw.publiccamerapoint);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        jsonString = writer.toString();
        Log.e("RESULT", jsonString);
    }

    private void tpcamparser() {
        loadJSONFromRawTP();
        if (jsonString != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonString);
                JSONArray featurestp = jsonObj.getJSONArray("features");
                for (int i = 0; i < featurestp.length(); i++) {
                    Log.e("COUNT",String.valueOf(featurestp.length()));
                    JSONObject c = featurestp.getJSONObject(i);

                    JSONObject propert = c.getJSONObject("properties");
                    String name = propert.getString("location");
                    String psname = propert.getString("ps_name");
                    String gdname = propert.getString("guard_name");
                    String div = propert.getString("division");

                    JSONObject geomet = c.getJSONObject("geometry");
                    String type = geomet.getString("type");
                    String tempcordi = geomet.getString("coordinates");
                    String temptwo_cordi = tempcordi.replace( "[", "" );
                    String cordi = temptwo_cordi.replace( "]", "" );
                    String[] separated = cordi.split(",");
                    String  lat = separated[0].trim();
                    String  longi = separated[1].trim();

                    HashMap<String, String> feature = new HashMap<>();

                    feature.put("Name", name);
                    feature.put("psname", psname);
                    feature.put("div", div);
                    feature.put("gurd", gdname);
                    feature.put("type", type);
                    feature.put("lat", lat);
                    feature.put("long", longi);

                    // adding contact to contact list
                    featureTP.add(feature);
                }
                Log.e("TOTAL", String.valueOf(featureList.size()));
            } catch (final JSONException e) {
                Log.e("RESULT", "Json parsing error: " + e.getMessage());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Json parsing error: " + e.getMessage(),
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });
            }
        } else {
            Log.e("NORESULT", "Couldn't get json from server.");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(),
                            "Couldn't get json from server. Check LogCat for possible errors!",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
        }
    }

    private void loadJSONFromRawTP() {
        InputStream is = getResources().openRawResource(R.raw.tpcamerapoint);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        jsonString = writer.toString();
        Log.e("RESULT", jsonString);
    }

    private void divcamparser() {
        loadJSONFromRaw();
        if (jsonString != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonString);
                JSONArray features = jsonObj.getJSONArray("features");
                for (int i = 0; i < features.length(); i++) {
                    Log.e("COUNT",String.valueOf(features.length()));
                    JSONObject c = features.getJSONObject(i);

                    JSONObject propert = c.getJSONObject("properties");
                    String name = propert.getString("Name");
                    String psname = propert.getString("psname");
                    String div = propert.getString("div");

                    JSONObject geomet = c.getJSONObject("geometry");
                    String type = geomet.getString("type");
                    String tempcordi = geomet.getString("coordinates");
                    String temptwo_cordi = tempcordi.replace( "[", "" );
                    String cordi = temptwo_cordi.replace( "]", "" );
                    String[] separated = cordi.split(",");
                    String  lat = separated[0].trim();
                    String  longi = separated[1].trim();

                    HashMap<String, String> feature = new HashMap<>();

                    feature.put("Name", name);
                    feature.put("psname", psname);
                    feature.put("div", div);
                    feature.put("type", type);
                    feature.put("lat", lat);
                    feature.put("long", longi);

                    // adding contact to contact list
                    featureList.add(feature);
                }
                Log.e("TOTAL", String.valueOf(featureList.size()));
            } catch (final JSONException e) {
                Log.e("RESULT", "Json parsing error: " + e.getMessage());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Json parsing error: " + e.getMessage(),
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });
            }
        } else {
            Log.e("NORESULT", "Couldn't get json from server.");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(),
                            "Couldn't get json from server. Check LogCat for possible errors!",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
        }
    }

    private void loadJSONFromRaw() {
        InputStream is = getResources().openRawResource(R.raw.divisionalcamera);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        jsonString = writer.toString();
        Log.e("RESULT", jsonString);
    }


    private void initViews(){
        bt_next=(Button) findViewById(R.id.bt_next);
        bt_prev = (Button)findViewById(R.id.bt_prev);
        bt_showColorCategory = (Button) findViewById(R.id.bt_showColorCategory);
        bt_next.setVisibility(View.GONE);
        bt_prev.setVisibility(View.GONE);
        bt_showColorCategory.setVisibility(View.GONE);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setUpMap();
    }


    private void setUpMap() {
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mDivMap = googleMap;
        mTPMap = googleMap;
        mPubMap = googleMap;
        iv_minus.setClickable(false);

        //Initialize Google Play Services for API 21 and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                //mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);
            }
        } else {
            //mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
        }


        ArrayList<LatLng>listLatLng = new ArrayList<LatLng>();

        for (int pos = 0; pos < mapSearchDetailsList.size(); pos++) {

            double latValue = 0.00, lngValue = 0.00;
            if (mapSearchDetailsList.get(pos).getPoLat() != null && !mapSearchDetailsList.get(pos).getPoLat().equalsIgnoreCase("null") && !mapSearchDetailsList.get(pos).getPoLat().equalsIgnoreCase("")) {
                latValue = Double.parseDouble(mapSearchDetailsList.get(pos).getPoLat());
            }

            if (mapSearchDetailsList.get(pos).getPoLong() != null && !mapSearchDetailsList.get(pos).getPoLong().equalsIgnoreCase("null") && !mapSearchDetailsList.get(pos).getPoLong().equalsIgnoreCase("")) {
                lngValue = Double.parseDouble(mapSearchDetailsList.get(pos).getPoLong());
            }


            String title = "";
            String snippetText = "";
            String psValue = "";
            String cNoValue = "";

            if ((mapSearchDetailsList.get(pos).getCaseSearchFIRAllAccusedList().get(0).getPsCase() != null) && (!mapSearchDetailsList.get(pos).getCaseSearchFIRAllAccusedList().get(0).getPsCase().equalsIgnoreCase("null")) && (!mapSearchDetailsList.get(pos).getCaseSearchFIRAllAccusedList().get(0).getPsCase().equalsIgnoreCase(""))){

                psValue = "SEC - " + mapSearchDetailsList.get(pos).getCaseSearchFIRAllAccusedList().get(0).getPsCase() + ", ";
            }

            if ((mapSearchDetailsList.get(pos).getCaseNo() != null && !mapSearchDetailsList.get(pos).getCaseNo().equalsIgnoreCase("") && !mapSearchDetailsList.get(pos).getCaseNo().equalsIgnoreCase("null"))
                    && (mapSearchDetailsList.get(pos).getFirYr() != null && !mapSearchDetailsList.get(pos).getFirYr().equalsIgnoreCase("") && !mapSearchDetailsList.get(pos).getFirYr().equalsIgnoreCase("null"))) {
                L.e("caseNo2-----------"+mapSearchDetailsList.get(pos).getCaseNo()+"caseYear2-----------"+mapSearchDetailsList.get(pos).getCaseYr());
                cNoValue = "C/No: " + mapSearchDetailsList.get(pos).getCaseNo() + " of " + mapSearchDetailsList.get(pos).getFirYr();
            }


            if ((mapSearchDetailsList.get(pos).getCategory() != null) && (!mapSearchDetailsList.get(pos).getCategory().equalsIgnoreCase("")) && (!mapSearchDetailsList.get(pos).getCategory().equalsIgnoreCase("null"))) {
                snippetText = "CATEGORY: " + mapSearchDetailsList.get(pos).getCategory();
            }

            title = psValue + cNoValue;

            /* Marker color code chage*/
            String col_code = "#ff3232";


            if(latValue != 0.00 && lngValue != 0.00) {
                // create the marker
                createMarker(latValue, lngValue, title, snippetText, col_code);

                // save all latlong value to arrayList
                LatLng object = new LatLng(latValue, lngValue);
                p_lat = latValue;
                p_long = lngValue;
                lati = latValue;
                longi = lngValue;
                listLatLng.add(object);
            }

        }

        SetZoomlevel(listLatLng);

        fab = (ImageView) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupDialog();
            }
        });

        circlefence();


        iv_plus.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Log.e("PLUS","RAD"+ radi);
                radi = radi + 50.00;
                circle.remove();
//                circlefence();
                iv_minus.setClickable(true);
                if (radi>=50000.00){
                    iv_plus.setClickable(false);
                }
                mDivMap.clear();
                if(f1 == "true") {
                    setUpMap();
                    divcamMarker();
                }
                if (f2 == "true"){
                    setUpMap();
                    tpcameraMarker();
                }
                if (f3 == "true"){
                    setUpMap();
                    pubcameraMarker();
                }
                circlefence();
            }
        });
        iv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radi = radi - 50;
                circle.remove();
                Log.e("MINUS","RAD"+ radi);
                iv_plus.setClickable(true);
                if (radi<=50.00) {
                    iv_minus.setClickable(false);
                }
                mDivMap.clear();
                if(f1 == "true") {
                    setUpMap();
                    divcamMarker();
                }
                if (f2 == "true"){
                    setUpMap();
                    tpcameraMarker();
                }
                if (f3 == "true"){
                    setUpMap();
                    pubcameraMarker();
                }
                circlefence();
            }
        });

    }

    private void popupDialog() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.custom_alertdialog, null);
        final Switch sw_div_cam = (Switch) alertLayout.findViewById(R.id.switch1);
        final Switch sw_tp_cam = (Switch) alertLayout.findViewById(R.id.switch2);
        final Switch sw_pub_cam = (Switch) alertLayout.findViewById(R.id.switch3);
        if (f1 == "true"){sw_div_cam.setChecked(true);}
        else if(f1 == "false"){sw_div_cam.setChecked(false);}
        if (f2 == "true"){sw_tp_cam.setChecked(true);}
        else if(f2 == "false"){sw_tp_cam.setChecked(false);}
        if (f3 == "true"){sw_pub_cam.setChecked(true);}
        else if(f3 == "false"){sw_pub_cam.setChecked(false);}

        sw_div_cam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    f1 = String.valueOf(isChecked);
                    divcamMarker();
                } else {
                    f1 = String.valueOf(isChecked);
                    mDivMap.clear();
                    if (f2 == "true" && f3 == "true") {
                        setUpMap();
                        tpcameraMarker();
                        pubcameraMarker();
                    }else if (f2 == "true"){
                        setUpMap();
                        tpcameraMarker();
                    }else if(f3 == "true"){
                        setUpMap();
                        pubcameraMarker();
                    }else {
                        setUpMap();
                    }
                }
            }
        });

        sw_tp_cam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    f2 = String.valueOf(isChecked);
                    tpcameraMarker();
                } else {
                    f2 = String.valueOf(isChecked);
//                    MarkerTp.remove();
                    mTPMap.clear();
                    if (f3 == "true" && f1 == "true") {
                        setUpMap();
                        divcamMarker();
                        pubcameraMarker();
                    }else if (f3 == "true"){
                        setUpMap();
                        divcamMarker();
                    }else if(f1 == "true"){
                        setUpMap();
                        pubcameraMarker();
                    }else {
                        setUpMap();
                    }
                }
            }
        });

        sw_pub_cam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    f3 = String.valueOf(isChecked);
                    pubcameraMarker();
                } else {
                    f3 = String.valueOf(isChecked);
                    mPubMap.clear();
                    if (f1 == "true" && f2 == "true") {
                        setUpMap();
                        tpcameraMarker();
                        divcamMarker();
                    }else if (f1 == "true"){
                        setUpMap();
                        divcamMarker();
                    }else if(f2 == "true"){
                        setUpMap();
                        tpcameraMarker();
                    }else {
                        setUpMap();
                    }

                }
            }
        });
        AlertDialog.Builder alert = new AlertDialog.Builder(CaseCrimeMapActivity.this,R.style.MyAlertDialogStyle);
        alert.setTitle("Layers");
        alert.setView(alertLayout);
        alert.setCancelable(false);
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                setUpMap();
            }
        });
        alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void pubcameraMarker() {
        for (int m = 0; m < featurePublic.size(); m++) {
            String t_name = String.valueOf(featurePublic.get(m).get("Name"));
            Double t_lat = Double.valueOf(String.valueOf(featurePublic.get(m).get("lat")));
            Double t_long = Double.valueOf(String.valueOf(featurePublic.get(m).get("long")));
            String t_ps = String.valueOf(featurePublic.get(m).get("psname"));
            String t_div = String.valueOf(featurePublic.get(m).get("div"));
            Log.e("ZERO",t_name);
            Log.e("ONE",String.valueOf(t_lat));
            Log.e("TWO",String.valueOf(t_long));
            Log.e("THREE",t_ps);
            Log.e("FOUR",t_div);

            if (radi>=distance(t_lat,t_long)) {
                markerPub = new MarkerOptions().position(new LatLng(t_long, t_lat ))
                        .title(t_name)
                        .snippet(t_ps + "[" + t_div +"]")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pubcam));
                MarkerPub = mPubMap.addMarker(markerPub);
//                mPubMap.addMarker(markerPub);
            }
        }
    }

    private void tpcameraMarker() {
        for (int m = 0; m < featureTP.size(); m++) {
            String t_name = String.valueOf(featureTP.get(m).get("Name"));
            Double t_lat = Double.valueOf(String.valueOf(featureTP.get(m).get("lat")));
            Double t_long = Double.valueOf(String.valueOf(featureTP.get(m).get("long")));
            String t_ps = String.valueOf(featureTP.get(m).get("psname"));
            String t_div = String.valueOf(featureTP.get(m).get("div"));
            String t_grd = String.valueOf(featureTP.get(m).get("gurd"));
            Log.e("ZERO",t_name);
            Log.e("ONE",String.valueOf(t_lat));
            Log.e("TWO",String.valueOf(t_long));
            Log.e("THREE",t_ps);
            Log.e("FOUR",t_div);
            Log.e("FIVE",t_grd);
//            double radi = 200.00;
            double dis = distance(t_lat,t_long);
            if (radi>=dis) {
                markerTp = new MarkerOptions().position(new LatLng(t_long, t_lat))
                        .title(t_name)
                        .snippet(t_ps + "[" + t_div + "]" + "[" + t_grd + "]")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.tpcam));
                MarkerTp = mTPMap.addMarker(markerTp);
//                mTPMap.addMarker(markerTp);
//                MarkerTp = mTPMap.addMarker(markerTp);
            }
        }
    }

    private void divcamMarker() {
        for (int i = 0; i < featureList.size(); i++) {
            String c_name = String.valueOf(featureList.get(i).get("Name"));
            Double c_lat = Double.valueOf(String.valueOf(featureList.get(i).get("lat")));
            Double c_long = Double.valueOf(String.valueOf(featureList.get(i).get("long")));
            String c_ps = String.valueOf(featureList.get(i).get("psname"));
            String c_div = String.valueOf(featureList.get(i).get("div"));
            Log.e("ZERO",c_name);
            Log.e("ONE",String.valueOf(c_lat));
            Log.e("TWO",String.valueOf(c_long));
            Log.e("THREE",c_ps);
            Log.e("FOUR",c_div);
//            double radi = 200.00;

            ///////// c_lat = Longitude; c_long = Latitude///////////////
            if (radi>=distance(c_lat,c_long)) {
                markerDiv = new MarkerOptions().position(new LatLng(c_long, c_lat))
                        .title(c_name)
                        .snippet(c_ps + "(" + c_div + ")")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.divcam));
                MarkerDiv = mDivMap.addMarker(markerDiv);
//                mDivMap.addMarker(markerDiv);
                mDivMap.addMarker(markerDiv);
            }
        }
    }

    public double distance (double clong, double clat) {

        System.out.println("center lat = " + lati + " center long = " + longi);
        System.out.println("cam lat = " + clat + " cam long = " + clong);

        double ycoord = Math.abs (lati - clat);
        double xcoord = Math.abs (longi- clong);

        double radY = ycoord*(Math.PI/180);
        double radX = xcoord*(Math.PI/180);

        System.out.println("ycoord = " + ycoord);
        System.out.println("xcoord = " + xcoord);

        double a = (Math.sin(radY/2)* Math.sin(radY/2)) + Math.cos(lati*(Math.PI/180))* Math.cos(clat*(Math.PI/180)) * (Math.sin(radX/2)* Math.sin(radX/2));
        double c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = 6371 *c;

        d = d*1000;

//        double distance = Math.sqrt((ycoord)*(ycoord) +(xcoord)*(xcoord));
        System.out.println("distance is = " + d);
        return d;
    }


    //set the marker
    protected Marker createMarker(double latitude, double longitude, String title, String snippetText, String colCode) {

        return mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .title(title)
                .snippet(snippetText)
                .infoWindowAnchor(0.5f, 0.5f)
                .icon(getMarkerIcon(colCode)));
    }


    // method to change marker color
    public BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }


    public void  SetZoomlevel(ArrayList<LatLng> listLatLng) {
        if (listLatLng != null && listLatLng.size() == 1) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(listLatLng.get(0), 17));
        }
    }



    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:

                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                // Utility.logout(this);
                break;
        }
        return true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        ImageLoader.getInstance().destroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }
    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }

    private void circlefence() {

        circle = mMap.addCircle(new CircleOptions()
                .center(new LatLng(p_lat, p_long))
                .radius(radi)
                .strokeColor(Color.RED)
                .strokeWidth(4f));
    }
}
