package com.kp.facedetection;

import android.Manifest;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kp.facedetection.adapter.MapColorWithCategoryAdapter;
import com.kp.facedetection.model.MapSearchDetails;
import com.kp.facedetection.model.ShowColorWithCategory;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import static com.kp.facedetection.utility.Utility.showAlertDialog;

public class MapSearchDetailsActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener, Observer {

    private GoogleMap mMap;
    private Button bt_prev, bt_next, bt_showColorCategory;

    private String[] keys;
    private String[] values;
    private String pageno;
    private String totalResult;

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    List<MapSearchDetails> mapSearchDetailsList;
    private ArrayList<MapSearchDetails> mapAllList = new ArrayList<MapSearchDetails>();
    KPFaceDetectionApplication kpFaceDetectionApplication;

    LatLngBounds.Builder builder;
    CameraUpdate cu;

    ArrayList<ShowColorWithCategory> uniqueColorCategoryList = null;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_search_result);
        ObservableObject.getInstance().addObserver(this);

        setUpMap();

        bt_prev = (Button) findViewById(R.id.bt_prev);
        bt_next = (Button) findViewById(R.id.bt_next);
        bt_showColorCategory = (Button) findViewById(R.id.bt_showColorCategory);


        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);


        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);


        keys = getIntent().getStringArrayExtra("keysMap");
        values = getIntent().getStringArrayExtra("valueMap");
        pageno = getIntent().getStringExtra("pagenoMap");
        totalResult = getIntent().getStringExtra("totalResultMap");

       /* Bundle bundle = getIntent().getExtras();
        mapSearchDetailsList = bundle.getParcelableArrayList("MAP_SEARCH");*/

        mapSearchDetailsList = KPFaceDetectionApplication.getApplication().getMapAllListData();
        System.out.println("mapSearchDetailsList size "+ mapSearchDetailsList.size());

        bt_next.setOnClickListener(this);
        bt_prev.setOnClickListener(this);
        bt_showColorCategory.setOnClickListener(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setUpMap();
    }


    private void setUpMap() {
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Initialize Google Play Services for API 21 and above
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                //mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.getUiSettings().setMapToolbarEnabled(false);
            }
        } else {
            //mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setMapToolbarEnabled(false);
        }

        loadMap();

    }


    private void loadMap(){

        ArrayList<ShowColorWithCategory> colorCategoryList = new ArrayList<ShowColorWithCategory>();

        List<Marker> markersList = new ArrayList<Marker>();
        Log.e("MapList size-- LoadMap", "pageNo"+pageno+" - "+ mapSearchDetailsList.size());
        for (int pos = 0; pos < mapSearchDetailsList.size(); pos++) {


            double latValue = 0.00, lngValue = 0.00;
            if (mapSearchDetailsList.get(pos).getPoLat() != null && !mapSearchDetailsList.get(pos).getPoLat().equalsIgnoreCase("") && !mapSearchDetailsList.get(pos).getPoLat().equalsIgnoreCase("null")) {
                latValue = Double.parseDouble(mapSearchDetailsList.get(pos).getPoLat());
                //Log.e("Lat: -- ",mapSearchDetailsList.get(pos).getPoLat());

            }

            if (mapSearchDetailsList.get(pos).getPoLong() != null && !mapSearchDetailsList.get(pos).getPoLong().equalsIgnoreCase("") && !mapSearchDetailsList.get(pos).getPoLong().equalsIgnoreCase("null")) {
                lngValue = Double.parseDouble(mapSearchDetailsList.get(pos).getPoLong());
                //Log.e("Long: -- ",mapSearchDetailsList.get(pos).getPoLong());
            }


            String title = "";
            String snippetText = "";
            String psValue = "";
            String cNoValue = "";

            if (mapSearchDetailsList.get(pos).getPsCode() != null && !mapSearchDetailsList.get(pos).getPsCode().equalsIgnoreCase("") && !mapSearchDetailsList.get(pos).getPsCode().equalsIgnoreCase("null")) {

                psValue = "SEC- " + mapSearchDetailsList.get(pos).getPsCode() + ", ";
            }

            if ((mapSearchDetailsList.get(pos).getCaseNo() != null && !mapSearchDetailsList.get(pos).getCaseNo().equalsIgnoreCase("null") && !mapSearchDetailsList.get(pos).getCaseNo().equalsIgnoreCase(""))
                    && (mapSearchDetailsList.get(pos).getFirYr() != null && !mapSearchDetailsList.get(pos).getFirYr().equalsIgnoreCase("null") && !mapSearchDetailsList.get(pos).getFirYr().equalsIgnoreCase(""))) {
                cNoValue = "C/No: " + mapSearchDetailsList.get(pos).getCaseNo() + " of " + mapSearchDetailsList.get(pos).getFirYr();
            }


            if (mapSearchDetailsList.get(pos).getCrimeCat() != null && !mapSearchDetailsList.get(pos).getCrimeCat().equalsIgnoreCase("") && !mapSearchDetailsList.get(pos).getCrimeCat().equalsIgnoreCase("null") ) {
                snippetText = "CATEGORY: " + mapSearchDetailsList.get(pos).getCrimeCat();
            }

            title = psValue + cNoValue;


            /* Marker color code chage*/
            String col_code = "";
            if (mapSearchDetailsList.get(pos).getColorCode() == null || mapSearchDetailsList.get(pos).getColorCode().charAt(0) != '#'
                    || mapSearchDetailsList.get(pos).getColorCode().length() > 9
                    || mapSearchDetailsList.get(pos).getColorCode().substring(1, mapSearchDetailsList.get(pos).getColorCode().length()).contains("#")) {
                col_code = "#ff3232";
                Log.e("Col- category","Col- "+ mapSearchDetailsList.get(pos).getColorCode() + "  Category "+ mapSearchDetailsList.get(pos).getCrimeCat() );
            }
            else{
                col_code = mapSearchDetailsList.get(pos).getColorCode();
            }

            // create the marker and set to markerList
            if(latValue != 0.00 && lngValue !=0.00) {
                markersList.add(createMarker(latValue, lngValue, title, snippetText, col_code));
            }


            /*create arraylist of category */
            ShowColorWithCategory showColorWithCategory= new ShowColorWithCategory(Parcel.obtain());

            if (mapSearchDetailsList.get(pos).getCrimeCat() != null && !mapSearchDetailsList.get(pos).getCrimeCat().equalsIgnoreCase("") && !mapSearchDetailsList.get(pos).getCrimeCat().equalsIgnoreCase("null") ) {
                showColorWithCategory.setCategory(mapSearchDetailsList.get(pos).getCrimeCat());
            }

            String col_codeDisplay = "";
            if (mapSearchDetailsList.get(pos).getColorCode() == null || mapSearchDetailsList.get(pos).getColorCode().charAt(0) != '#'
                    || mapSearchDetailsList.get(pos).getColorCode().length() > 9
                    || mapSearchDetailsList.get(pos).getColorCode().substring(1, mapSearchDetailsList.get(pos).getColorCode().length()).contains("#")) {
                col_codeDisplay = "#ff3232";
            }
            else{
                col_codeDisplay = mapSearchDetailsList.get(pos).getColorCode();
            }
            showColorWithCategory.setColor(col_codeDisplay);

            colorCategoryList.add(showColorWithCategory);
        }

        Set<ShowColorWithCategory> setColorList = new HashSet<>(colorCategoryList);
        //create a new List from the Set
        uniqueColorCategoryList = new ArrayList<>(setColorList);
        Log.e("Unique Color list Size", uniqueColorCategoryList.size()+"");
        for(int i = 0; i<uniqueColorCategoryList.size(); i++){
            Log.e("Col - Cat",uniqueColorCategoryList.get(i).getColor() +" - "+ uniqueColorCategoryList.get(i).getCategory());
        }



        /**create for loop for get the latLngbuilder from the marker list*/
        builder = new LatLngBounds.Builder();
        for (Marker m : markersList) {
            builder.include(m.getPosition());
        }

        /**initialize the padding for map boundary*/
        int padding = 50;
        /**create the bounds from latlngBuilder to set into map camera*/
        LatLngBounds bounds = builder.build();
        /**create the camera with bounds and padding to set into map*/
        cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        /**call the map call back to know map is loaded or not*/
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                /**set animated zoom camera into map*/
                mMap.animateCamera(cu);

            }
        });
    }


    //set the marker
    protected Marker createMarker(double latitude, double longitude, String title, String snippetText, String colCode) {

        return mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .title(title)
                .snippet(snippetText)
                .infoWindowAnchor(0.5f, 0.5f)
                /*.icon(getMarkerIcon(colCode))*/
                .icon(BitmapDescriptorFactory.fromBitmap(changeBitmapColor(BitmapFactory.decodeResource(getResources(),R.drawable.marker),Color.parseColor(colCode)))));
    }


    // method to change marker color
    public BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        Log.e("color - hsv :", color+ " - "+ hsv[0]);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
           /* case R.id.menu_send_push:

                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                // Utility.logout(this);
                break;
        }
        return true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        ImageLoader.getInstance().destroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.bt_next:

                loadNextDataForMap();
                break;

            case R.id.bt_prev:

                loadPrevDataForMap();
                break;

            case R.id.bt_showColorCategory:
                showColorCategoryLabel();
                break;
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }



    private void loadNextDataForMap() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_MAP_VIEW);
        taskManager.setMapViewPagination(true);

        values[5] = (Integer.parseInt(pageno) + 1) + "";
        taskManager.doStartTask(keys, values, true);
    }


    private void loadPrevDataForMap(){

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_MAP_VIEW);
        taskManager.setMapViewPagination(true);

        values[5] = (Integer.parseInt(pageno) - 1) + "";
        taskManager.doStartTask(keys, values, true);
    }


    public void parseMapViewAllPagination(String result, String[] keys, String[] values) {

        //System.out.println("parseMapViewAllPagination: " + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("page").toString();
                    totalResult = jObj.opt("count").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseMapViewAllResponse(resultArray);

                } else {

                    showAlertDialog(MapSearchDetailsActivity.this, Constants.ERROR_DETAILS_TITLE, "Sorry! No location available.", false);
                }

            }
            catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(MapSearchDetailsActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }

    }


    private void parseMapViewAllResponse(JSONArray result_array) {

        kpFaceDetectionApplication = KPFaceDetectionApplication.getApplication();

        mapSearchDetailsList.clear();
        mMap.clear();

        for (int i = 0; i < result_array.length(); i++) {

            JSONObject obj = null;

            try {

                obj = result_array.getJSONObject(i);

                MapSearchDetails mapDetails = new MapSearchDetails(Parcel.obtain());

                if (!obj.optString("FIR_YR").equalsIgnoreCase("null"))
                    mapDetails.setFirYr(obj.optString("FIR_YR"));

                if (!obj.optString("CASENO").equalsIgnoreCase("null"))
                    mapDetails.setCaseNo(obj.optString("CASENO"));

                if (!obj.optString("PS").equalsIgnoreCase("null"))
                    mapDetails.setPsCode(obj.optString("PS"));

                if (!obj.optString("CATEGORY").equalsIgnoreCase("null"))
                    mapDetails.setCrimeCat(obj.optString("CATEGORY"));

                if (!obj.optString("PO_LAT").equalsIgnoreCase("null"))
                    mapDetails.setPoLat(obj.optString("PO_LAT"));

                if (!obj.optString("PO_LONG").equalsIgnoreCase("null"))
                    mapDetails.setPoLong(obj.optString("PO_LONG"));

                if (!obj.optString("COLOR_CODE").equalsIgnoreCase("null"))
                    mapDetails.setColorCode(obj.optString("COLOR_CODE"));

                mapSearchDetailsList.add(mapDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        loadMap();
    }


    private void showColorCategoryLabel(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(MapSearchDetailsActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.custom_color_category, null);
        dialog.setView(convertView);

        ListView lv_colCatList = (ListView) convertView.findViewById(R.id.lv_colCatList);

        MapColorWithCategoryAdapter adapter = new MapColorWithCategoryAdapter(MapSearchDetailsActivity.this, uniqueColorCategoryList);
        lv_colCatList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        dialog.show();

    }


    private Bitmap changeBitmapColor(Bitmap bitmap, int color) {
        Paint paint = new Paint();
        paint.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN));
        Bitmap bmp2 = bitmap.copy(bitmap.getConfig(), true);
        Canvas canvas = new Canvas(bmp2);
        canvas.drawBitmap(bmp2, 0, 0, paint);
        return bmp2;
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
