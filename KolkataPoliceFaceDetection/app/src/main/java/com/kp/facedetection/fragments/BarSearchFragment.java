package com.kp.facedetection.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.google.android.gcm.GCMRegistrar;
import com.kp.facedetection.BARSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.SDRSearchResultActivity;
import com.kp.facedetection.interfaces.OnShowAlertForProceedResult;
import com.kp.facedetection.model.BARDetails;
import com.kp.facedetection.model.SDRDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class BarSearchFragment extends Fragment implements OnShowAlertForProceedResult {

    private EditText et_holderName,et_address;
    String holder_name ="",holder_address="";
    private String pageno="";
    private String totalResult="";
    private String[] keys;
    private String[] values;
    String userId="";
    private Button btn_Search, btn_reset;
    String devicetoken ="";
    String auth_key="";
    ArrayList<BARDetails> barDetailsList ;
    public BarSearchFragment() {
        // Required empty public constructor
    }
    public static BarSearchFragment newInstance() {

        BarSearchFragment barSearchFragment = new BarSearchFragment();
        return barSearchFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bar_search, container, false);
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userId= Utility.getUserInfo(getContext()).getUserId();
        et_holderName = (EditText)view.findViewById(R.id.et_holderName);
        et_address = (EditText)view.findViewById(R.id.et_address);
        btn_Search = (Button) view.findViewById(R.id.btn_Search);
        btn_reset = (Button) view.findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_Search);
        Constants.buttonEffect(btn_reset);
        clickEvents();

    }
    private void clickEvents(){



        btn_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder_name = et_holderName.getText().toString().trim();
                holder_address = et_address.getText().toString().trim();

                if (!holder_name.equalsIgnoreCase("") || !holder_address.equalsIgnoreCase("")) {
                    Utility utility = new Utility();
                    utility.setDelegate(BarSearchFragment.this);
                    Utility.showAlertForProceed(getActivity());
                } else {

                    showAlertDialog(getActivity(), " Search Error!!! ", "Please provide at least one value for search", false);

                }



            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_holderName.setText("");
                et_address.setText("");

            }
        });

    }

    @Override
    public void onShowAlertForProceedResultSuccess(String success) {
        fetchSearchResult(holder_name,holder_address);
    }
    private void fetchSearchResult(String holder_name,String holder_address) {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_BAR_SEARCH);
        taskManager.setBARSearch(true);
        try{
            devicetoken = GCMRegistrar.getRegistrationId(getActivity());
            auth_key=Utility.getDocumentSearchSesionId(getActivity());
        }
        catch (Exception e){

        }
        //user_id,device_token,device_type,imei,auth_key,name,address,pageno
        String[] keys = {"name", "address","pageno","user_id","device_token","device_type","imei","auth_key"};

        String[] values = {holder_name, holder_address,"1",userId,devicetoken,"android",Utility.getImiNO(getActivity()),auth_key};

        taskManager.doStartTask(keys, values, true,true);


    }

    @Override
    public void onShowAlertForProceedResultError(String error) {

    }
    public void parseBARSearchResultResponse(String result, String[] keys, String[] values){
        if(result != null && !result.equals("")){

            try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.opt("status").toString().equalsIgnoreCase("1")){

                    this.keys=keys;
                    this.values=values;
                    pageno=jObj.opt("page").toString();
                    totalResult=jObj.opt("count").toString();

                    JSONArray resultArray=jObj.getJSONArray("result");

                    parseBARSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(getActivity()," Search Error ",jObj.optString("message"),false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                Utility.showToast(getActivity(), "Some error has been encountered", "long");
            }
        }
    }
    private void parseBARSearchResponse(JSONArray resultArray) {
        barDetailsList = new ArrayList<BARDetails>();
        for(int i=0;i<resultArray.length();i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj=resultArray.getJSONObject(i);

                BARDetails barDetails = new BARDetails();

                if(!jsonObj.optString("name").equalsIgnoreCase("null") && !jsonObj.optString("name").equalsIgnoreCase("")){
                    barDetails.setName(jsonObj.optString("name"));
                }
                if(!jsonObj.optString("address").equalsIgnoreCase("null") && !jsonObj.optString("address").equalsIgnoreCase("")){
                    barDetails.setAddress(jsonObj.optString("address"));
                }




                barDetailsList.add(barDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Intent intent=new Intent(getActivity(), BARSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putExtra("BAR_SEARCH_LIST", (Serializable) barDetailsList);
        startActivity(intent);

    }
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

}
