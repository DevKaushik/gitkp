package com.kp.facedetection;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

public class KPApplication extends Application {
	private static Context mContext;
	private Activity mCurrentActivity;
	static KPApplication mInstance;
	
	public void onCreate() {
		super.onCreate();
		this.mContext = this;
		mInstance = this;
	}

	public static Context getContext() {
		return mContext;
	}

	public Activity getCurrentActivity() {
		return mCurrentActivity;
	}
	public void setCurrentActivity(Activity activity) {
		mCurrentActivity=activity;
	}
	public static KPApplication getApplication() {
		return mInstance;
	}
}