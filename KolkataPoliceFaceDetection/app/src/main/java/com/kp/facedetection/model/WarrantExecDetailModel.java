package com.kp.facedetection.model;

import java.util.ArrayList;

/**
 * Created by DAT-165 on 28-06-2017.
 */

public class WarrantExecDetailModel {
    String rowNo;
    String casePS;
    String firYear;
    String returnableDtaeToCourt;
    String waNo;
    String psRcvDate;
    String actionDate;
    String underSec;
    String waStatus;
    String waType;
    String psCode;
    String waSlNo;
    String waCaseNo;
    String waYear;
    String slNo;
    String issueCourt;
    String officerToServe;
    String officerName;
    String processNo;
    String waCat;
    String colorStatus;
    String waDate;

    public String getWaDate() {
        return waDate;
    }

    public void setWaDate(String waDate) {
        this.waDate = waDate;
    }

    public String getColorStatus() {
        return colorStatus;
    }

    public void setColorStatus(String colorStatus) {
        this.colorStatus = colorStatus;
    }

    ArrayList<WarrantiesDetailOfDashboardWarrantModel> warrantiesList = new ArrayList<>();
    private ArrayList<ModifiedWarrantDetailsModel> warrantNerList = new ArrayList<>();
    private ArrayList<ModifiedWarrantDetailsModel> warrantProcessList = new ArrayList<>();
    private ArrayList<ModifiedWarrantDetailsModel> warrantPropertiesList = new ArrayList<>();



    public String getRowNo() {
        return rowNo;
    }

    public void setRowNo(String rowNo) {
        this.rowNo = rowNo;
    }

    public String getCasePS() {
        return casePS;
    }

    public void setCasePS(String casePS) {
        this.casePS = casePS;
    }

    public String getFirYear() {
        return firYear;
    }

    public void setFirYear(String firYear) {
        this.firYear = firYear;
    }

    public String getReturnableDtaeToCourt() {
        return returnableDtaeToCourt;
    }

    public void setReturnableDtaeToCourt(String returnableDtaeToCourt) {
        this.returnableDtaeToCourt = returnableDtaeToCourt;
    }

    public String getWaNo() {
        return waNo;
    }

    public void setWaNo(String waNo) {
        this.waNo = waNo;
    }

    public String getPsRcvDate() {
        return psRcvDate;
    }

    public void setPsRcvDate(String psRcvDate) {
        this.psRcvDate = psRcvDate;
    }

    public String getActionDate() {
        return actionDate;
    }

    public void setActionDate(String actionDate) {
        this.actionDate = actionDate;
    }

    public String getUnderSec() {
        return underSec;
    }

    public void setUnderSec(String underSec) {
        this.underSec = underSec;
    }

    public String getWaStatus() {
        return waStatus;
    }

    public void setWaStatus(String waStatus) {
        this.waStatus = waStatus;
    }

    public String getWaType() {
        return waType;
    }

    public void setWaType(String waType) {
        this.waType = waType;
    }

    public String getPsCode() {
        return psCode;
    }

    public void setPsCode(String psCode) {
        this.psCode = psCode;
    }

    public String getWaSlNo() {
        return waSlNo;
    }

    public void setWaSlNo(String waSlNo) {
        this.waSlNo = waSlNo;
    }

    public String getWaCaseNo() {
        return waCaseNo;
    }

    public void setWaCaseNo(String waCaseNo) {
        this.waCaseNo = waCaseNo;
    }

    public String getWaYear() {
        return waYear;
    }

    public void setWaYear(String waYear) {
        this.waYear = waYear;
    }

    public String getSlNo() {
        return slNo;
    }

    public void setSlNo(String slNo) {
        this.slNo = slNo;
    }

    public String getIssueCourt() {
        return issueCourt;
    }

    public void setIssueCourt(String issueCourt) {
        this.issueCourt = issueCourt;
    }

    public String getOfficerToServe() {
        return officerToServe;
    }

    public void setOfficerToServe(String officerToServe) {
        this.officerToServe = officerToServe;
    }

    public String getOfficerName() {
        return officerName;
    }

    public void setOfficerName(String officerName) {
        this.officerName = officerName;
    }

    public String getProcessNo() {
        return processNo;
    }

    public void setProcessNo(String processNo) {
        this.processNo = processNo;
    }

    public ArrayList<WarrantiesDetailOfDashboardWarrantModel> getWarranties() {
        return warrantiesList;
    }

    public void setWarranties(WarrantiesDetailOfDashboardWarrantModel warrantiesObj) {
        warrantiesList.add(warrantiesObj);
    }
    public ArrayList<ModifiedWarrantDetailsModel> getWarrantNerList() {
        return warrantNerList;
    }

    public void setWarrantNerList(ModifiedWarrantDetailsModel  warrantNerObj) {
        warrantNerList.add(warrantNerObj);
    }

    public ArrayList<ModifiedWarrantDetailsModel> getWarrantProcessList() {
        return warrantProcessList;
    }

    public void setWarrantProcessList(ModifiedWarrantDetailsModel  warrantProcessObj) {
        warrantProcessList.add(warrantProcessObj);
    }
    public ArrayList<ModifiedWarrantDetailsModel> getWarrantPropertiesList() {
        return warrantPropertiesList;
    }

    public void setWarrantPropertiesList(ModifiedWarrantDetailsModel  warrantPropertiesObj) {
        warrantPropertiesList.add(warrantPropertiesObj);
    }
    public String getWaCat() {
        return waCat;
    }

    public void setWaCat(String waCat) {
        this.waCat = waCat;
    }
}
