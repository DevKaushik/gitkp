package com.kp.facedetection.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 22-07-2016.
 */
public class CaseSearchDetails implements Serializable {

    private String rowNumber="";
    private String psCode="";
    private String ps="";
    private String caseNo="";
    private String caseDate="";
    private String underSection="";
    private String category="";
    private String caseYr="";
    private String firStatus="";
    private String occurTime="";
    private String modOper="";
    private String sl_no="";
    private String wa_sl_no="";
    private String wa_yr="";
    private String poLat = "";
    private String poLong = "";
    private String colorCode = "";

    private List<String> briefMatch_list = new ArrayList<>();
    private List<CaseTransferDetails> caseSearchCaseTransferList=new ArrayList<CaseTransferDetails>();

    public List<String> getBriefMatch_list() {
        return briefMatch_list;
    }

    public void setBriefMatch_list(List<String> briefMatch_list) {
        this.briefMatch_list = briefMatch_list;
    }

    public String getWa_yr() {
        return wa_yr;
    }

    public void setWa_yr(String wa_yr) {
        this.wa_yr = wa_yr;
    }

    public String getWa_sl_no() {
        return wa_sl_no;
    }

    public void setWa_sl_no(String wa_sl_no) {
        this.wa_sl_no = wa_sl_no;
    }

    public String getSl_no() {
        return sl_no;
    }

    public void setSl_no(String sl_no) {
        this.sl_no = sl_no;
    }



    public String getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(String rowNumber) {
        this.rowNumber = rowNumber;
    }

    public String getPsCode() {
        return psCode;
    }

    public void setPsCode(String psCode) {
        this.psCode = psCode;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getCaseDate() {
        return caseDate;
    }

    public void setCaseDate(String caseDate) {
        this.caseDate = caseDate;
    }

    public String getUnderSection() {
        return underSection;
    }

    public void setUnderSection(String underSection) {
        this.underSection = underSection;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCaseYr() {
        return caseYr;
    }

    public void setCaseYr(String caseYr) {
        this.caseYr = caseYr;
    }

    public String getFirStatus() {
        return firStatus;
    }

    public void setFirStatus(String firStatus) {
        this.firStatus = firStatus;
    }

    public String getOccurTime() {
        return occurTime;
    }

    public void setOccurTime(String occurTime) {
        this.occurTime = occurTime;
    }

    public String getModOper() {
        return modOper;
    }

    public void setModOper(String modOper) {
        this.modOper = modOper;
    }

    public String getPoLat() {
        return poLat;
    }

    public void setPoLat(String poLat) {
        this.poLat = poLat;
    }

    public String getPoLong() {
        return poLong;
    }

    public void setPoLong(String poLong) {
        this.poLong = poLong;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public List<CaseTransferDetails> getCaseSearchCaseTransferList() {
        return caseSearchCaseTransferList;
    }

    public void setCaseSearchCaseTransferList(CaseTransferDetails obj) {
        caseSearchCaseTransferList.add(obj);
    }
}
