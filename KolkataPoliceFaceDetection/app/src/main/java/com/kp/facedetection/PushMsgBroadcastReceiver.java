package com.kp.facedetection;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

//com.kp.facedetection.PushMsgBroadcastReceiver
public class PushMsgBroadcastReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.e("Message", "Message");
		System.out.println("intent : facedetection.PushMsgBroadcastReceiver " + intent);
		Bundle extras = intent.getExtras();
		String subject = extras.getString("subject");
		String from = extras.getString("from");
		showNotification(context,subject,from);
	}

	private void showNotification(Context context,String subject,String from) {
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		int notifyID = 1;
		
		NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(context)
	    .setContentTitle("New Message From : " + from)
	    .setContentText("Subject: " + subject)
	    .setSmallIcon(R.drawable.ic_launcher);
		
		Notification notification = mNotifyBuilder.build();
		Intent notificationIntent = new Intent(context, SplashActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
		notification.contentIntent = contentIntent;
		
		 mNotificationManager.notify(
		            notifyID,
		            notification);
	
	}

}
