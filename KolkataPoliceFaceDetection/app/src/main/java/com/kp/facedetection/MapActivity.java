package com.kp.facedetection;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kp.facedetection.adapter.PlaceAutocompleteAdapter;
import com.kp.facedetection.utility.AlertDialogManager;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Observable;
import java.util.Observer;

public class MapActivity extends BaseActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMapLongClickListener, View.OnClickListener, LocationListener, android.location.LocationListener, GoogleApiClient.ConnectionCallbacks, AlertDialogManager.OnDialogClickListener, GoogleMap.OnMapClickListener, Observer {

    public static final String LAT_LONG = "lat_lng";
    public static final String CHOSEN_ADDRESS = "chosen_address";
    private static final int LOCATION_REQUEST = 111;
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final int ENABLE_GPS_REQUEST = 222;

    private AutoCompleteTextView autoCompleteTextView;
    private PlaceAutocompleteAdapter mAdapter;

    private GoogleMap map;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LocationManager locationManager;

    private LatLng pointedLatLng = null, currentLatLng = null;
    private Marker pointedMarker = null, currentMarker = null;

    private boolean isCurrentLocationShownOneTime = false;
    private boolean isAddressAvailableFromPrevActivity = false;
    private boolean isOpenSettingsCalled = false;
    private String userName, loginNumber;
    private String appVersion = "";
    TextView chargesheetNotificationCount;
    String notificationCount="";


    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                places.release();
                return;
            }

            Place place;
            try {
                place = places.get(0);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            if (place == null)
                return;


            pointedLatLng = place.getLatLng();
            autoCompleteTextView.setSelection(autoCompleteTextView.getText().toString().trim().length());

            String address = autoCompleteTextView.getText().toString().trim();


            setPointedMarker(address);

            L.e("ResultCallback");

            places.release();
        }
    };


    private AdapterView.OnItemClickListener placeAutoCompleteListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final AutocompletePrediction item = mAdapter.getItem(position);

            if (item == null) {
                return;
            }

            final String placeId = item.getPlaceId();
            //final CharSequence primaryText = item.getPrimaryText(null);

            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modified_capture_firmap_layout);
        ObservableObject.getInstance().addObserver(this);

        initViews();
        initToolBar();
        initLocationPermission();
        initLocationManager();
        getDataFromIntent();
    }

    private void getDataFromIntent() {
        autoCompleteTextView.setText(getIntent().getExtras().getString(CHOSEN_ADDRESS, ""));
        autoCompleteTextView.setSelection(autoCompleteTextView.getText().toString().trim().length());
        pointedLatLng = getIntent().getExtras().getParcelable(LAT_LONG);
        if (pointedLatLng != null) {
            isAddressAvailableFromPrevActivity = true;
        }
    }

    private void initLocationManager() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }

    private void initLocationPermission() {
        if (ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            initMap();

            initGoogleApiClient();

            initLocationRequest();
        } else {
            ActivityCompat.requestPermissions(MapActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
        }
    }

    private void initLocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1000); // 1 second, in milliseconds
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initToolBar() {


        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initPlaceAutocompleteWithGoogleApiClient() {
        LatLngBounds BOUNDS = new LatLngBounds(new LatLng(22, 88), new LatLng(22, 88));
        mAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, BOUNDS, null);
        autoCompleteTextView.setOnItemClickListener(placeAutoCompleteListener);
        autoCompleteTextView.setAdapter(mAdapter);
    }

    private void initViews() {
        Button btDone = (Button) findViewById(R.id.bt_done);
        btDone.setOnClickListener(this);
        Constants.buttonEffect(btDone);

        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.tv_auto_address);
        ImageView ivCurrentLocation = (ImageView) findViewById(R.id.iv_current_location);
        ivCurrentLocation.setOnClickListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_REQUEST:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    initMap();

                    initGoogleApiClient();

                    initLocationRequest();

                } else {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(MapActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                        AlertDialogManager alertDialogManager = new AlertDialogManager();
                        alertDialogManager.showDialog(MapActivity.this, "Alert", "You have to enable location permission to continue.", "OK", "CANCEL", false);
                        alertDialogManager.setDialogClickListener(this);

                    } else {
                        initLocationPermission();
                        Utility.showToast(MapActivity.this, "Please allow location permission to continue", "long");
                    }
                }

                break;
        }
    }

    private void initGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    // The next two lines tell the new client that “this” current class will handle connection stuff
                    //.addConnectionCallbacks(this)
                    .enableAutoManage(this, 0 /* clientId */, this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    //fourth line adds the LocationServices API endpoint from GooglePlayServices
                    .addApi(Places.GEO_DATA_API)
                    .addApi(LocationServices.API)
                    .build();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        //Disconnect from API onPause()
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        //map.setOnMapLongClickListener(this);
        map.setOnMapClickListener(this);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        map.setMyLocationEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setRotateGesturesEnabled(true);


        View locationButton = ((View) findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 30);


        if (isAddressAvailableFromPrevActivity) {
            L.e("onMapReady isAddressAvailableFromPrevActivity");
            setPointedMarker(autoCompleteTextView.getText().toString().trim());
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    /*
             * Google Play services can resolve some errors it detects.
             * If the error has a resolution, try sending an Intent to
             * start a Google Play services activity that can resolve
             * error.
             */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
            L.e("Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }



    @Override
    public void onMapLongClick(LatLng latLng) {

        //
        //Avani

        //
        geoCodeApi(MapActivity.this, latLng);
        this.pointedLatLng = latLng;
        setPointedMarker(null);
    }

    private void geoCodeApi(Activity activity, LatLng latLng) {
        TaskManager taskManager = new TaskManager(this);
        String geoCodingUrl = Constants.GEO_CODING_URL;
        geoCodingUrl = geoCodingUrl.replace("TANAY_LAT", String.valueOf(latLng.latitude))
                .replace("TANAY_LON", String.valueOf(latLng.longitude))
                .replace("GEO_API_KEY", activity.getString(R.string.google_maps_key));

        L.e(geoCodingUrl);
        taskManager.setMethod(geoCodingUrl);
        taskManager.setGeoCode(true);
        String[] keys = {};
        String[] values = {};
        taskManager.doStartTask(keys, values, true, true);
    }

    private void setPointedMarker(String markerText) {
        if (getCurrentFocus() != null) {
            Utility.hideKeyBoard(getCurrentFocus());
        }

        if (pointedMarker != null) {
            pointedMarker.remove();
        }

        LatLng latlng = new LatLng(pointedLatLng.latitude, pointedLatLng.longitude);
        pointedMarker = map.addMarker(new MarkerOptions()
                .position(latlng).title(markerText == null ? "Selected pointedMarker" : markerText)
        );
        map.setMyLocationEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLng(latlng));
        map.animateCamera(CameraUpdateFactory.zoomIn());
        map.animateCamera(CameraUpdateFactory.zoomTo(15), 1000, null);
    }


    public void parseGeoCodeResponse(String response) {

        try {
            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.optJSONArray("results").length() != 0) {
                autoCompleteTextView.setText(jsonObject.optJSONArray("results").optJSONObject(0).optString("formatted_address"));
                autoCompleteTextView.setSelection(autoCompleteTextView.getText().toString().length());
                pointedMarker.setTitle(autoCompleteTextView.getText().toString().trim());
            } else {
                //Error
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (isOpenSettingsCalled) {
            initLocationPermission();
            isOpenSettingsCalled = false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_done:

                Intent intent = new Intent();
                intent.putExtra(LAT_LONG, pointedLatLng);
                intent.putExtra(CHOSEN_ADDRESS, autoCompleteTextView.getText().toString().trim());
                if (TextUtils.isEmpty(autoCompleteTextView.getText().toString()) || pointedLatLng == null) {
                    //setResult(RESULT_CANCELED, null);
                    //onBackPressed();
                    Utility.showToast(MapActivity.this, "Please enter an address", "short");
                } else {
                    setResult(RESULT_OK, intent);
                    onBackPressed();
                }
                break;

            case R.id.iv_current_location:

                if (currentLatLng != null) {
                    map.setMyLocationEnabled(true);
                    map.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
                    map.animateCamera(CameraUpdateFactory.zoomIn());
                    map.animateCamera(CameraUpdateFactory.zoomTo(15), 1000, null);
                } else {
                    if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                        Utility.showToast(MapActivity.this, "Please wait until we get your current location", "short");
                    } else {
                        Utility.showToast(MapActivity.this, "Please enable location", "short");
                    }
                }


                break;
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        initLocationWithGoogleApiClient();
        initPlaceAutocompleteWithGoogleApiClient();
    }

    private void initLocationWithGoogleApiClient() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        if (location != null) {

            if (!isAddressAvailableFromPrevActivity) {
                isCurrentLocationShownOneTime = true;
                L.e("Current Location: " + location.getLatitude() + "  " + location.getLongitude());

                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                this.currentLatLng = latLng;
                map.setMyLocationEnabled(true);
                map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                map.animateCamera(CameraUpdateFactory.zoomIn());
                map.animateCamera(CameraUpdateFactory.zoomTo(15), 1000, null);

                geoCodeApi(MapActivity.this, latLng);
                this.pointedLatLng = latLng;
                setPointedMarker(null);

                if (currentMarker != null) {
                    currentMarker.remove();
                }
            }
            //currentMarker = map.addMarkeraddMarker(new MarkerOptions().position(pointedLatLng).title("Current Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location)));
        } else {
            L.e("initLocationWithGoogleApiClient else");
            openGpsSettings();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        L.e("GPS STATUS 1 onProviderEnabled");
        if (ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            initGoogleApiClient();
            initLocationRequest();
        }

    }

    @Override
    public void onProviderDisabled(String provider) {
        L.e("GPS STATUS 2 onProviderDisabled");
        //openGpsSettings();
        Utility.showToast(MapActivity.this, "Please enable location", "short");
    }

    @Override
    public void onLocationChanged(Location location) {

        if (!isCurrentLocationShownOneTime) {
            if (!isAddressAvailableFromPrevActivity) {
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                this.pointedLatLng = latLng;
                geoCodeApi(MapActivity.this, latLng);
                setPointedMarker(null);

                map.setMyLocationEnabled(true);
                map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                map.animateCamera(CameraUpdateFactory.zoomIn());
                map.animateCamera(CameraUpdateFactory.zoomTo(15), 1000, null);
            }
        }

        isCurrentLocationShownOneTime = true;
        currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());

        L.e("onLocationChanged");
        if (currentMarker != null) {
            currentMarker.remove();
        }


        //currentMarker = map.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("Current Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location)));

        /*LatLng pointedLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        map.moveCamera(CameraUpdateFactory.newLatLng(pointedLatLng));
        map.animateCamera(CameraUpdateFactory.zoomIn());
        map.animateCamera(CameraUpdateFactory.zoomTo(15), 1000, null);*/
    }

    @Override
    public void onDialogPositiveClick() {
        isOpenSettingsCalled = true;
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    public void onDialogNegativeClick() {
        onBackPressed();
    }

    private void openGpsSettings() {
        L.e("openGpsSettings");

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        // **************************
        builder.setAlwaysShow(true); // this is the key ingredient
        // **************************

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                .checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        L.e("success");
                        //getLocationNow();
                        // All location settings are satisfied. The client can
                        // initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        L.e("RESOLUTION_REQUIRED");
                        // Location settings are not satisfied. But could be
                        // fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling
                            // startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MapActivity.this, ENABLE_GPS_REQUEST);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        L.e("SETTINGS_CHANGE_UNAVAILABLE");
                        // Location settings are not satisfied. However, we have
                        // no way to fix the
                        // settings so we won't show the dialog.
                        break;
                    case LocationSettingsStatusCodes.CANCELED:

                        L.e("CANCELED");

                        break;
                    case LocationSettingsStatusCodes.ERROR:

                        L.e("ERROR");
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ENABLE_GPS_REQUEST:

                if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    Utility.showToast(MapActivity.this, "Please enable location", "short");
                    //openGpsSettings();
                }


                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0, this);

                break;
        }
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:

                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                // Utility.logout(this);
                break;
        }
        return true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                mGoogleApiClient.stopAutoManage(this);
                mGoogleApiClient.disconnect();
            }
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        geoCodeApi(MapActivity.this, latLng);
        this.pointedLatLng = latLng;
        setPointedMarker(null);
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }


    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}