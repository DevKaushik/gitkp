package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-Asset-131 on 13-07-2016.
 */
public class ChargeSheetDetails implements Serializable{

    private String courtOf="";
    private String frType="";
    private String actsSection="";
    private String suppltmentaryOriginal="";
    private String briefFacts="";
    private String charge_sheet_details="";
    private String fir_chgNo="";
    private String fir_date="";

    public String getFir_chgNo() {
        return fir_chgNo;
    }

    public void setFir_chgNo(String fir_chgNo) {
        this.fir_chgNo = fir_chgNo;
    }

    public String getFir_date() {
        return fir_date;
    }

    public void setFir_date(String fir_date) {
        this.fir_date = fir_date;
    }

    public String getCharge_sheet_details() {
        return charge_sheet_details;
    }

    public void setCharge_sheet_details(String charge_sheet_details) {
        this.charge_sheet_details = charge_sheet_details;
    }



    public String getCourtOf() {
        return courtOf;
    }

    public void setCourtOf(String courtOf) {
        this.courtOf = courtOf;
    }

    public String getFrType() {
        return frType;
    }

    public void setFrType(String frType) {
        this.frType = frType;
    }

    public String getActsSection() {
        return actsSection;
    }

    public void setActsSection(String actsSection) {
        this.actsSection = actsSection;
    }

    public String getSuppltmentaryOriginal() {
        return suppltmentaryOriginal;
    }

    public void setSuppltmentaryOriginal(String suppltmentaryOriginal) {
        this.suppltmentaryOriginal = suppltmentaryOriginal;
    }

    public String getBriefFacts() {
        return briefFacts;
    }

    public void setBriefFacts(String briefFacts) {
        this.briefFacts = briefFacts;
    }
}
