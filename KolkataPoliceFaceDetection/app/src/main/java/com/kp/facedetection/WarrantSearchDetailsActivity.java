package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kp.facedetection.adapter.WarrantModiedListAdapter;
import com.kp.facedetection.model.FIRWarranteesDetails;
import com.kp.facedetection.model.ModifiedWarrantDetailsModel;
import com.kp.facedetection.model.WarrantSearchItemDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by DAT-165 on 29-07-2016.
 */
public class WarrantSearchDetailsActivity extends BaseActivity implements View.OnClickListener, Observer {

    /**
     * views of WARRANT popup window
     */
    private TextView tv_issuingCourtValue;
    private TextView tv_processNoValue;
    private TextView tv_typeValue;
    private TextView tv_returnableDateValue;
    private TextView tv_IOValue;
    private TextView tv_presentStatusValue;
    private TextView tv_remarksValue;
    private TextView tv_caseRefValue;
    private TextView tv_warrantNo;
    private TextView tv_itemName;
    private LinearLayout linear_caseRef;
    private LinearLayout linear_warrantees;
    private TextView tv_watermark;

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String warrantDetails = "";
    private String ps_code = "";
    private String wa_sl_no = "";
    private String wa_yr = "";
    private String sl_no = "";
    private int wa_position;

    ListView modifiedWarrantListView;
    ArrayList<ModifiedWarrantDetailsModel> modifiedWarrantDetailsesArray=new ArrayList<>();
    WarrantModiedListAdapter warrantModiedListAdapter;
    private List<WarrantSearchItemDetails> warrantSearchItemList;
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.warrant_popup_modified);
        ObservableObject.getInstance().addObserver(this);

        initialization();

    }

    private void initialization() {

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        warrantDetails = getIntent().getStringExtra("WARRANT_DETAILS");
        sl_no = getIntent().getStringExtra("SL_NO");
        wa_sl_no = getIntent().getStringExtra("WA_SL_NO");
        wa_yr = getIntent().getStringExtra("WA_YR");
        ps_code = getIntent().getStringExtra("PS_CODE");
        wa_position = getIntent().getIntExtra("WA_POSITION", 0);

        initView();

        /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-55);
    }

    private void initView() {

       /* tv_issuingCourtValue = (TextView) findViewById(R.id.tv_issuingCourtValue);
        tv_processNoValue = (TextView) findViewById(R.id.tv_processNoValue);
        tv_typeValue = (TextView) findViewById(R.id.tv_typeValue);
        tv_returnableDateValue = (TextView) findViewById(R.id.tv_returnableDateValue);
        tv_IOValue = (TextView) findViewById(R.id.tv_IOValue);
        tv_presentStatusValue = (TextView) findViewById(R.id.tv_presentStatusValue);
        tv_remarksValue = (TextView) findViewById(R.id.tv_remarksValue);
        tv_caseRefValue = (TextView) findViewById(R.id.tv_caseRefValue);
*/
        tv_warrantNo = (TextView) findViewById(R.id.tv_warrantNo);
        tv_itemName = (TextView) findViewById(R.id.tv_itemName);


       // linear_warrantees = (LinearLayout) findViewById(R.id.linear_warrantees);
        modifiedWarrantListView=(ListView)findViewById(R.id.modifiedWarrantListview);
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup)inflater.inflate(R.layout.warrant_popup_modified_header, null, false);
        linear_caseRef = (LinearLayout)header.findViewById(R.id.linear_caseRef);
        tv_issuingCourtValue = (TextView)header.findViewById(R.id.tv_issuingCourtValue);
        tv_processNoValue = (TextView)header.findViewById(R.id.tv_processNoValue);
        tv_typeValue = (TextView)header.findViewById(R.id.tv_typeValue);
        tv_returnableDateValue = (TextView)header.findViewById(R.id.tv_returnableDateValue);
        tv_IOValue = (TextView)header.findViewById(R.id.tv_IOValue);
        tv_presentStatusValue = (TextView)header.findViewById(R.id.tv_presentStatusValue);
        tv_remarksValue = (TextView)header.findViewById(R.id.tv_remarksValue);
        tv_caseRefValue = (TextView)header.findViewById(R.id.tv_caseRefValue);
        modifiedWarrantListView.addHeaderView(header, null, false);
      /*  setListViewHeightBasedOnChildren(modifiedWarrantListView);
        modifiedWarrantListView.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });*/
       /* modifiedWarrantListView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });*/

        linear_caseRef.setVisibility(View.VISIBLE);
        tv_warrantNo.setVisibility(View.VISIBLE);

        Utility.changefonts(tv_itemName, this, "Calibri Bold.ttf");
        Utility.changefonts(tv_warrantNo, this, "Calibri Bold.ttf");
        //Utility.changefonts(tv_presentStatusValue, this, "Calibri Bold.ttf");
        Utility.changefonts(tv_caseRefValue, this, "Calibri Bold.ttf");


        tv_warrantNo.setText(warrantDetails);

        tv_watermark = (TextView) findViewById(R.id.tv_watermark);

        getWarrantSearchDetailsResult();
        warrantModiedListAdapter=new WarrantModiedListAdapter(this,modifiedWarrantDetailsesArray);
        modifiedWarrantListView.setAdapter(warrantModiedListAdapter);
       // setListViewHeightBasedOnChildren(modifiedWarrantListView);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        ImageLoader.getInstance().destroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }


    private void getWarrantSearchDetailsResult() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_WARRANT_SEARCH_ITEM_DETAILS);
        taskManager.setWarrantSearchItemDetails(true);

        String[] keys = {"ps", "slno", "wa_slno", "wa_year"};
        String[] values = {ps_code.trim(), sl_no.trim(), wa_sl_no.trim(), wa_yr.trim()};
        //String[] values = {"MTZ", "B-024/16", "24", "2016"};

        taskManager.doStartTask(keys, values, true);

    }

    public void parseWarrantSearchItemDetailsResult(String result) {

       // System.out.println("parseWarrantSearchItemDetailsResult: " + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    JSONObject resultObj = jObj.getJSONObject("result");

                    parseWarrantSearchDetailsResponse(resultObj);

                } else {
                    showAlertDialog(this, Constants.ERROR_DETAILS_TITLE, jObj.optString("message"), false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }

        }
    }

    private void parseWarrantSearchDetailsResponse(JSONObject obj) {
        modifiedWarrantDetailsesArray.clear();

        warrantSearchItemList = new ArrayList<WarrantSearchItemDetails>();

        WarrantSearchItemDetails warrantSearchItemDetails = new WarrantSearchItemDetails();

        if (!obj.optString("ps").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setPs(obj.optString("ps"));

        if (!obj.optString("slno").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setSl_no(obj.optString("slno"));

        if (!obj.optString("wa_slno").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setWa_slno(obj.optString("wa_slno"));

        if (!obj.optString("yr").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setYr(obj.optString("yr"));

        if (!obj.optString("WANO").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setWarrant_no(obj.optString("WANO"));

        if (!obj.optString("CASEPS").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setCase_ps(obj.optString("CASEPS"));

        if (!obj.optString("CASE_POLICE_STN").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setCase_police_station(obj.optString("CASE_POLICE_STN"));

        if (!obj.optString("CASENO").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setCase_no(obj.optString("CASENO"));

        if (!obj.optString("PSNAME").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setPs_name(obj.optString("PSNAME"));

        if (!obj.optString("FIR_YR").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setFir_yr(obj.optString("FIR_YR"));

        if (!obj.optString("PS_RECV_DATE").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setPs_recv_date(obj.optString("PS_RECV_DATE"));

        if (!obj.optString("UNDER_SECTIONS").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setUnder_sections(obj.optString("UNDER_SECTIONS"));

        if (!obj.optString("ISSUE_COURT").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setIssue_court(obj.optString("ISSUE_COURT"));

        if (!obj.optString("PROCESS_NO").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setProcess_no(obj.optString("PROCESS_NO"));

        if (!obj.optString("WATYPE").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setWarrant_type(obj.optString("WATYPE"));

        if (!obj.optString("RETURNABLE_DATE_TO_COURT").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setReturnable_date_to_court(obj.optString("RETURNABLE_DATE_TO_COURT"));

        if (!obj.optString("IONAME").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setIo_name(obj.optString("IONAME"));

        if (!obj.optString("REMARKS").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setRemarks(obj.optString("REMARKS"));

        if (!obj.optString("WA_STATUS").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setWarrant_status(obj.optString("WA_STATUS"));

        if (!obj.optString("ACTION_DATE").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setAction_date(obj.optString("ACTION_DATE"));
        if (!obj.optString("fail_normal").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setColor_status(obj.optString("fail_normal"));
        if (!obj.optString("casedate").equalsIgnoreCase("null"))
            warrantSearchItemDetails.setCase_date(obj.optString("casedate"));


       try {

            JSONArray warrantees_array = obj.optJSONArray("WARRANTEES");

            if (warrantees_array.length() > 0) {

                for (int i = 0; i < warrantees_array.length(); i++) {

                    FIRWarranteesDetails firWarranteesDetails = new FIRWarranteesDetails();

                    JSONObject warranteesObj = warrantees_array.getJSONObject(i);

                    if (warranteesObj.optString("SLNO") != null && !warranteesObj.optString("SLNO").equalsIgnoreCase("") && !warranteesObj.optString("SLNO").equalsIgnoreCase("null")) {
                        firWarranteesDetails.setSl_no(warranteesObj.optString("SLNO"));
                    }
                    if (warranteesObj.optString("NAME") != null && !warranteesObj.optString("NAME").equalsIgnoreCase("") && !warranteesObj.optString("NAME").equalsIgnoreCase("null")) {
                        firWarranteesDetails.setWarrantee_name(warranteesObj.optString("NAME"));
                    }
                    if (warranteesObj.optString("ADDRESS") != null && !warranteesObj.optString("ADDRESS").equalsIgnoreCase("") && !warranteesObj.optString("ADDRESS").equalsIgnoreCase("null")) {
                        firWarranteesDetails.setWarrantee_address(warranteesObj.optString("ADDRESS"));
                    }

                    warrantSearchItemDetails.setFirWarranteesDetailsList(firWarranteesDetails);

                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try{
            if(obj.has("WARRANTEES"))
            {
                JSONArray modified_warrant_details_array_warrantees = obj.optJSONArray("WARRANTEES");
                if(modified_warrant_details_array_warrantees.length()>0){
                    ModifiedWarrantDetailsModel modifiedWarrantDetailsModel_warrantees_header=new ModifiedWarrantDetailsModel();
                    modifiedWarrantDetailsModel_warrantees_header.setWarrantObjType("Header");
                    modifiedWarrantDetailsModel_warrantees_header.setWarrantHeaderType("WARRANTEES");
                    modifiedWarrantDetailsesArray.add(modifiedWarrantDetailsModel_warrantees_header);
                    for(int i=0;i<modified_warrant_details_array_warrantees.length();i++){
                        JSONObject modified_warrant_details_array_warrantees_obj = modified_warrant_details_array_warrantees.optJSONObject(i);
                        ModifiedWarrantDetailsModel modifiedWarrantDetailsModel_warrantees=new ModifiedWarrantDetailsModel();
                        modifiedWarrantDetailsModel_warrantees.setWarrantObjType("WARRANTEES");
                        String detail = "";
                        if (modified_warrant_details_array_warrantees_obj.optString("SLNO") != null && !modified_warrant_details_array_warrantees_obj.optString("SLNO").equalsIgnoreCase("") && !modified_warrant_details_array_warrantees_obj.optString("SLNO").equalsIgnoreCase("null")) {
                            detail=modified_warrant_details_array_warrantees_obj.optString("SLNO")+".";
                        }
                        if (modified_warrant_details_array_warrantees_obj.optString("NAME") != null && !modified_warrant_details_array_warrantees_obj.optString("NAME").equalsIgnoreCase("") && !modified_warrant_details_array_warrantees_obj.optString("NAME").equalsIgnoreCase("null")) {
                            detail = detail + modified_warrant_details_array_warrantees_obj.optString("NAME");
                        }
                        if (modified_warrant_details_array_warrantees_obj.optString("ADDRESS") != null && !modified_warrant_details_array_warrantees_obj.optString("ADDRESS").equalsIgnoreCase("") && !modified_warrant_details_array_warrantees_obj.optString("ADDRESS").equalsIgnoreCase("null")) {
                           detail = detail +","+ modified_warrant_details_array_warrantees_obj.optString("ADDRESS");
                        }
                        modifiedWarrantDetailsModel_warrantees.setWarranteeDetails(detail);
                       // modifiedWarrantDetailsesArray.add(modifiedWarrantDetailsModel_ner);
                        modifiedWarrantDetailsesArray.add(modifiedWarrantDetailsModel_warrantees);
                    }
                }

            }
            if(obj.has("NON_EXEC_REPORT"))
            {
                JSONArray modified_warrant_details_array_ner = obj.optJSONArray("NON_EXEC_REPORT");
                if(modified_warrant_details_array_ner.length()>0){
                    ModifiedWarrantDetailsModel modifiedWarrantDetailsModel_ner_header=new ModifiedWarrantDetailsModel();
                    modifiedWarrantDetailsModel_ner_header.setWarrantObjType("Header");
                    modifiedWarrantDetailsModel_ner_header.setWarrantHeaderType("WA_NER");
                    modifiedWarrantDetailsesArray.add(modifiedWarrantDetailsModel_ner_header);
                    for(int i=0;i<modified_warrant_details_array_ner.length();i++){
                        JSONObject modified_warrant_details_array_ner_obj = modified_warrant_details_array_ner.optJSONObject(i);
                        ModifiedWarrantDetailsModel modifiedWarrantDetailsModel_ner=new ModifiedWarrantDetailsModel();
                        modifiedWarrantDetailsModel_ner.setWarrantObjType("WA_NER");
                        if(modified_warrant_details_array_ner_obj.optString("NERDATE") != null && !modified_warrant_details_array_ner_obj.optString("NERDATE").equalsIgnoreCase("") && !modified_warrant_details_array_ner_obj.optString("NERDATE").equalsIgnoreCase("null"))
                           modifiedWarrantDetailsModel_ner.setNerDate(modified_warrant_details_array_ner_obj.optString("NERDATE"));
                        else
                            modifiedWarrantDetailsModel_ner.setNerDate("NA");
                        if(modified_warrant_details_array_ner_obj.optString("WITH_WA") != null && !modified_warrant_details_array_ner_obj.optString("WITH_WA").equalsIgnoreCase("") && !modified_warrant_details_array_ner_obj.optString("WITH_WA").equalsIgnoreCase("null"))
                            modifiedWarrantDetailsModel_ner.setWithWarrant(modified_warrant_details_array_ner_obj.optString("WITH_WA"));
                        else
                            modifiedWarrantDetailsModel_ner.setWithWarrant("NA");
                        if(modified_warrant_details_array_ner_obj.optString("REMARKS") != null && !modified_warrant_details_array_ner_obj.optString("REMARKS").equalsIgnoreCase("") && !modified_warrant_details_array_ner_obj.optString("REMARKS").equalsIgnoreCase("null"))
                            modifiedWarrantDetailsModel_ner.setNerRemarks(modified_warrant_details_array_ner_obj.optString("REMARKS"));
                        else
                            modifiedWarrantDetailsModel_ner.setNerRemarks("NA");
                        modifiedWarrantDetailsesArray.add(modifiedWarrantDetailsModel_ner);
                    }
                }

            }
            if(obj.has("PROCESS_DETAILS"))
            {
                JSONArray modified_warrant_details_array_process = obj.optJSONArray("PROCESS_DETAILS");
                if(modified_warrant_details_array_process.length()>0){
                    ModifiedWarrantDetailsModel modifiedWarrantDetailsModel_process_header=new ModifiedWarrantDetailsModel();
                    modifiedWarrantDetailsModel_process_header.setWarrantObjType("Header");
                    modifiedWarrantDetailsModel_process_header.setWarrantHeaderType("WA_PROCESSES");
                    modifiedWarrantDetailsesArray.add(modifiedWarrantDetailsModel_process_header);
                    for(int i=0;i<modified_warrant_details_array_process.length();i++){
                        JSONObject modified_warrant_details_array_process_obj = modified_warrant_details_array_process.optJSONObject(i);
                        ModifiedWarrantDetailsModel modifiedWarrantDetailsModel_process=new ModifiedWarrantDetailsModel();
                        modifiedWarrantDetailsModel_process.setWarrantObjType("WA_PROCESSES");
                        if(modified_warrant_details_array_process_obj.optString("PROCESS_NO") != null && !modified_warrant_details_array_process_obj.optString("PROCESS_NO").equalsIgnoreCase("") && !modified_warrant_details_array_process_obj.optString("PROCESS_NO").equalsIgnoreCase("null"))
                            modifiedWarrantDetailsModel_process.setProcessNo(modified_warrant_details_array_process_obj.optString("PROCESS_NO"));
                         else
                            modifiedWarrantDetailsModel_process.setProcessNo("NA");
                        if(modified_warrant_details_array_process_obj.optString("PROCESS_TYPE") != null && !modified_warrant_details_array_process_obj.optString("PROCESS_TYPE").equalsIgnoreCase("") && !modified_warrant_details_array_process_obj.optString("PROCESS_TYPE").equalsIgnoreCase("null"))
                            modifiedWarrantDetailsModel_process.setProcessType(modified_warrant_details_array_process_obj.optString("PROCESS_TYPE"));
                         else
                            modifiedWarrantDetailsModel_process.setProcessType("NA");
                        if(modified_warrant_details_array_process_obj.optString("REQUEST_DATE") != null && !modified_warrant_details_array_process_obj.optString("REQUEST_DATE").equalsIgnoreCase("") && !modified_warrant_details_array_process_obj.optString("REQUEST_DATE").equalsIgnoreCase("null"))
                            modifiedWarrantDetailsModel_process.setProcessRequestDate(modified_warrant_details_array_process_obj.optString("REQUEST_DATE"));
                         else
                            modifiedWarrantDetailsModel_process.setProcessRequestDate("NA");
                        if(modified_warrant_details_array_process_obj.optString("COURT_APPVD") != null && !modified_warrant_details_array_process_obj.optString("COURT_APPVD").equalsIgnoreCase("") && !modified_warrant_details_array_process_obj.optString("COURT_APPVD").equalsIgnoreCase("null"))
                            modifiedWarrantDetailsModel_process.setProcessCourtAppr(modified_warrant_details_array_process_obj.optString("COURT_APPVD"));
                         else
                            modifiedWarrantDetailsModel_process.setProcessCourtAppr("NA");
                        if(modified_warrant_details_array_process_obj.optString("MEMO_NO") != null && !modified_warrant_details_array_process_obj.optString("MEMO_NO").equalsIgnoreCase("") && !modified_warrant_details_array_process_obj.optString("MEMO_NO").equalsIgnoreCase("null"))
                             modifiedWarrantDetailsModel_process.setProcessMemoNo(modified_warrant_details_array_process_obj.optString("MEMO_NO"));
                        else
                            modifiedWarrantDetailsModel_process.setProcessMemoNo("NA");

                        if(modified_warrant_details_array_process_obj.optString("MEMO_DT") != null && !modified_warrant_details_array_process_obj.optString("MEMO_DT").equalsIgnoreCase("") && !modified_warrant_details_array_process_obj.optString("MEMO_DT").equalsIgnoreCase("null"))
                             modifiedWarrantDetailsModel_process.setProcessMemodate(modified_warrant_details_array_process_obj.optString("MEMO_DT"));
                        else
                            modifiedWarrantDetailsModel_process.setProcessMemodate("NA");

                        if(modified_warrant_details_array_process_obj.optString("EXECUTED") != null && !modified_warrant_details_array_process_obj.optString("EXECUTED").equalsIgnoreCase("") && !modified_warrant_details_array_process_obj.optString("EXECUTED").equalsIgnoreCase("null"))
                             modifiedWarrantDetailsModel_process.setProcessExecuted(modified_warrant_details_array_process_obj.optString("EXECUTED"));
                        else
                            modifiedWarrantDetailsModel_process.setProcessExecuted("NA");

                        if(modified_warrant_details_array_process_obj.optString("EXECUTION_DT") != null && !modified_warrant_details_array_process_obj.optString("EXECUTION_DT").equalsIgnoreCase("") && !modified_warrant_details_array_process_obj.optString("EXECUTION_DT").equalsIgnoreCase("null"))
                             modifiedWarrantDetailsModel_process.setProcessExecutedDate(modified_warrant_details_array_process_obj.optString("EXECUTION_DT"));
                         else
                            modifiedWarrantDetailsModel_process.setProcessExecutedDate("NA");

                        if(modified_warrant_details_array_process_obj.optString("REMARKS") != null && !modified_warrant_details_array_process_obj.optString("REMARKS").equalsIgnoreCase("") && !modified_warrant_details_array_process_obj.optString("REMARKS").equalsIgnoreCase("null"))
                             modifiedWarrantDetailsModel_process.setProcessRemarks(modified_warrant_details_array_process_obj.optString("REMARKS"));
                         else
                        {
                            modifiedWarrantDetailsModel_process.setProcessRemarks("NA");
                        }
                        modifiedWarrantDetailsesArray.add(modifiedWarrantDetailsModel_process);
                    }
               }

            }
            if(obj.has("PROPERTY_DETAILS"))
            {
                JSONArray modified_warrant_details_array_properties = obj.optJSONArray("PROPERTY_DETAILS");
                if(modified_warrant_details_array_properties.length()>0){
                    ModifiedWarrantDetailsModel modifiedWarrantDetailsModel_properties_header=new ModifiedWarrantDetailsModel();
                    modifiedWarrantDetailsModel_properties_header.setWarrantObjType("Header");
                    modifiedWarrantDetailsModel_properties_header.setWarrantHeaderType("WA_PROPERTIES");
                    modifiedWarrantDetailsesArray.add(modifiedWarrantDetailsModel_properties_header);
                    for(int i=0;i<modified_warrant_details_array_properties.length();i++){
                        JSONObject modified_warrant_details_array_properties_obj = modified_warrant_details_array_properties.optJSONObject(i);
                        ModifiedWarrantDetailsModel modifiedWarrantDetailsModel_properties=new ModifiedWarrantDetailsModel();
                        modifiedWarrantDetailsModel_properties.setWarrantObjType("WA_PROPERTIES");
                        if(modified_warrant_details_array_properties_obj.optString("SLNO") != null && !modified_warrant_details_array_properties_obj.optString("SLNO").equalsIgnoreCase("") && !modified_warrant_details_array_properties_obj.optString("SLNO").equalsIgnoreCase("null"))
                            modifiedWarrantDetailsModel_properties.setPropertiesSlNo(modified_warrant_details_array_properties_obj.optString("SLNO"));
                         else
                            modifiedWarrantDetailsModel_properties.setPropertiesSlNo("NA");
                        if(modified_warrant_details_array_properties_obj.optString("ARTICLE") != null && !modified_warrant_details_array_properties_obj.optString("ARTICLE").equalsIgnoreCase("") && !modified_warrant_details_array_properties_obj.optString("ARTICLE").equalsIgnoreCase("null"))
                            modifiedWarrantDetailsModel_properties.setPropertiesArticle(modified_warrant_details_array_properties_obj.optString("ARTICLE"));
                        else
                            modifiedWarrantDetailsModel_properties.setPropertiesArticle("NA");
                        if(modified_warrant_details_array_properties_obj.optString("VALUATION") != null && !modified_warrant_details_array_properties_obj.optString("VALUATION").equalsIgnoreCase("") && !modified_warrant_details_array_properties_obj.optString("VALUATION").equalsIgnoreCase("null"))
                            modifiedWarrantDetailsModel_properties.setPropertiesValuation(modified_warrant_details_array_properties_obj.optString("VALUATION"));
                        else
                            modifiedWarrantDetailsModel_properties.setPropertiesValuation("NA");
                        modifiedWarrantDetailsesArray.add(modifiedWarrantDetailsModel_properties);
                    }
                }

            }


        }catch(Exception ex){
            ex.printStackTrace();
        }


        warrantSearchItemList.add(warrantSearchItemDetails);

        setData(warrantSearchItemList);
        warrantModiedListAdapter.notifyDataSetChanged();
        //setListViewHeightBasedOnChildren(modifiedWarrantListView);

    }

    private void setData(final List<WarrantSearchItemDetails> warrantSearchItemList) {

        this.warrantSearchItemList = warrantSearchItemList;
        String warrantName = "";

        if (!warrantSearchItemList.get(0).getPs_name().equals("")) {
            warrantName = warrantSearchItemList.get(0).getPs_name();
        }
        if (!warrantSearchItemList.get(0).getCase_no().equals("")) {
            warrantName = warrantName + " C/No. " + warrantSearchItemList.get(0).getCase_no();
        }
        if (!warrantSearchItemList.get(0).getAction_date().equals("")) {
            warrantName = warrantName + " dated " + warrantSearchItemList.get(0).getAction_date();
        }


        tv_itemName.setText(warrantName);

        /** start of case reference set*/

        String caseRef = "";
        if (!warrantSearchItemList.get(0).getCase_police_station().equals("")) {
            caseRef = warrantSearchItemList.get(0).getCase_police_station();
        }
        if (!warrantSearchItemList.get(0).getCase_no().equals("")) {
            caseRef = caseRef + " C/No. " + warrantSearchItemList.get(0).getCase_no();
        }
        if (!warrantSearchItemList.get(0).getUnder_sections().equals("")) {
            caseRef = caseRef + " u/s " + warrantSearchItemList.get(0).getUnder_sections();
        }
        if (!warrantSearchItemList.get(0).getCase_date().equals("")) {
            caseRef = caseRef + " of " + warrantSearchItemList.get(0).getCase_date();
        }
        else if(!warrantSearchItemList.get(0).getFir_yr().equals("")){
            caseRef = caseRef + " of " + warrantSearchItemList.get(0).getFir_yr();
        }

        /** end of case reference set*/

        Log.e("TAG1", warrantSearchItemList.get(0).getCase_ps().replace("SEC-", ""));
        Log.e("TAG2", warrantSearchItemList.get(0).getCase_no());
        Log.e("TAG3", warrantSearchItemList.get(0).getFir_yr());

        if (warrantSearchItemList.get(0).getCase_ps().replace("SEC-", "").equalsIgnoreCase("") || warrantSearchItemList.get(0).getCase_no().equalsIgnoreCase("") || warrantSearchItemList.get(0).getFir_yr().equalsIgnoreCase("")) {

            tv_caseRefValue.setTextColor(ContextCompat.getColor(this, R.color.edit_text_hint_color));
            tv_caseRefValue.setText(caseRef);
            tv_caseRefValue.setTextColor(ContextCompat.getColor(this, R.color.color_light_blue));
            tv_caseRefValue.setEnabled(false);
        } else {
            /** Spannable effect */

            tv_caseRefValue.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            SpannableString caseRefString = new SpannableString(caseRef);
            caseRefString.setSpan(new UnderlineSpan(), 0, caseRefString.length(), 0);
            tv_caseRefValue.setText(caseRefString);
        }


        if (tv_caseRefValue.getText().toString().trim().length() > 0) {

            tv_caseRefValue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String ps_code = warrantSearchItemList.get(0).getCase_ps().replace("SEC-", "");
                    String case_no = warrantSearchItemList.get(0).getCase_no();
                    String case_yr = warrantSearchItemList.get(0).getFir_yr();
                    String ps_name = warrantSearchItemList.get(0).getPs_name();

                    if (ps_code.equalsIgnoreCase("") || case_no.equalsIgnoreCase("") || case_yr.equalsIgnoreCase("")) {

                        //tv_caseRefValue.setEnabled(false);
                        Utility.showAlertDialog(WarrantSearchDetailsActivity.this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                    } else {

                        Intent in = new Intent(WarrantSearchDetailsActivity.this, CaseSearchFIRDetailsActivity.class);
                        in.putExtra("ITEM_NAME", tv_caseRefValue.getText().toString().trim());
                        in.putExtra("HEADER_VALUE", tv_caseRefValue.getText().toString().trim());
                        in.putExtra("PS_CODE", ps_code);
                        in.putExtra("CASE_NO", case_no);
                        in.putExtra("CASE_YR", case_yr);
                        in.putExtra("PS_NAME", ps_name);
                        in.putExtra("POSITION", wa_position);
                        in.putExtra("FROM_PAGE", "WarrantSearchDetailsActivity");
                        startActivity(in);
                    }

                }
            });

        } else {
            tv_caseRefValue.setText("NA");
            tv_caseRefValue.setTextColor(ContextCompat.getColor(this, R.color.color_light_blue));
        }


        if (!warrantSearchItemList.get(0).getIssue_court().equals("")) {
            tv_issuingCourtValue.setText(warrantSearchItemList.get(0).getIssue_court());
        } else {
            tv_issuingCourtValue.setText("NA");
        }

        if (!warrantSearchItemList.get(0).getProcess_no().equals("")) {
            tv_processNoValue.setText(warrantSearchItemList.get(0).getProcess_no());
        } else {
            tv_processNoValue.setText("NA");
        }

        if (!warrantSearchItemList.get(0).getWarrant_type().equals("")) {
            tv_typeValue.setText(warrantSearchItemList.get(0).getWarrant_type());
        } else {
            tv_typeValue.setText("NA");
        }

        if (!warrantSearchItemList.get(0).getReturnable_date_to_court().equals("")) {
            //  tv_returnableDateValue.setText(warrantSearchItemList.get(0).getReturnable_date_to_court());
            if (!warrantSearchItemList.get(0).getColor_status().equalsIgnoreCase("null")) {
                DateUtils.overDueDay(this, warrantSearchItemList.get(0).getReturnable_date_to_court(), tv_returnableDateValue, warrantSearchItemList.get(0).getColor_status());
            } else {
                tv_returnableDateValue.setText(warrantSearchItemList.get(0).getReturnable_date_to_court());
            }
           /* if (!warrantSearchItemList.get(0).getWarrant_status().equals("") && warrantSearchItemList.get(0).getWarrant_status().equalsIgnoreCase("Executed")) {
                tv_returnableDateValue.setText(warrantSearchItemList.get(0).getReturnable_date_to_court());
                tv_returnableDateValue.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
            } else {

                if (day > 0) {
                    if (day == 1) {
                        tv_returnableDateValue.setText(warrantSearchItemList.get(0).getReturnable_date_to_court() + "( Overdue by " + String.valueOf(day) + "day )");
                    } else {
                        tv_returnableDateValue.setText(warrantSearchItemList.get(0).getReturnable_date_to_court() + "( Overdue by " + String.valueOf(day) + "days )");
                    }
                } else {
                    tv_returnableDateValue.setText(warrantSearchItemList.get(0).getReturnable_date_to_court());
                }
            }

*/
        } else {
            tv_returnableDateValue.setText("NA");
        }

        if (!warrantSearchItemList.get(0).getIo_name().equals("")) {
            tv_IOValue.setText(warrantSearchItemList.get(0).getIo_name());
        } else {
            tv_IOValue.setText("NA");
        }

        if (!warrantSearchItemList.get(0).getWarrant_status().equals("")) {

            //tv_presentStatusValue.setText(warrantSearchItemList.get(0).getWarrant_status());
            if (!warrantSearchItemList.get(0).getColor_status().equals("null")) {
                DateUtils.setStatusColor(this, tv_presentStatusValue, warrantSearchItemList.get(0).getColor_status(), warrantSearchItemList.get(0).getWarrant_status());

            } else {
                tv_presentStatusValue.setText(warrantSearchItemList.get(0).getWarrant_status());
            }
        } else {
            tv_presentStatusValue.setText("NA");
        }

        if (!warrantSearchItemList.get(0).getRemarks().equals("")) {
            tv_remarksValue.setText(warrantSearchItemList.get(0).getRemarks());
        } else {
            tv_remarksValue.setText("NA");
        }


       /* if (warrantSearchItemList.get(0).getFirWarranteesDetailsList().size() > 0) {

            int warranteesListSize = warrantSearchItemList.get(0).getFirWarranteesDetailsList().size();

            for (int i = 0; i < warranteesListSize; i++) {

               *//* LayoutInflater inflater = (LayoutInflater) this
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View v = inflater.inflate(R.layout.fir_warrant_list_item, null);

                RelativeLayout relative_warrantDetails = (RelativeLayout) v.findViewById(R.id.relative_warrantDetails);
                relative_warrantDetails.setId(i);
                TextView tv_SlNo = (TextView) v.findViewById(R.id.tv_SlNo);
                TextView tv_warranteesName = (TextView) v.findViewById(R.id.tv_warranteesName);
                TextView tv_warranteesAddress = (TextView) v.findViewById(R.id.tv_warranteesAddress);

                tv_SlNo.setText(warrantSearchItemList.get(0).getFirWarranteesDetailsList().get(i).getSl_no());
                tv_warranteesName.setText(warrantSearchItemList.get(0).getFirWarranteesDetailsList().get(i).getWarrantee_name());
                tv_warranteesAddress.setText(warrantSearchItemList.get(0).getFirWarranteesDetailsList().get(i).getWarrantee_address());*//*

                TextView warrantiesNameAddressDetail = new TextView(WarrantSearchDetailsActivity.this);
                String detail = "";
                if (!warrantSearchItemList.get(0).getFirWarranteesDetailsList().get(i).getSl_no().equalsIgnoreCase(""))
                    detail = warrantSearchItemList.get(0).getFirWarranteesDetailsList().get(i).getSl_no() + ". ";
                if (!warrantSearchItemList.get(0).getFirWarranteesDetailsList().get(i).getWarrantee_name().equalsIgnoreCase(""))
                    detail = detail + warrantSearchItemList.get(0).getFirWarranteesDetailsList().get(i).getWarrantee_name() + ", ";
                if (!warrantSearchItemList.get(0).getFirWarranteesDetailsList().get(i).getWarrantee_address().equalsIgnoreCase(""))
                    detail = detail + warrantSearchItemList.get(0).getFirWarranteesDetailsList().get(i).getWarrantee_address();

                warrantiesNameAddressDetail.setText(detail );
                warrantiesNameAddressDetail.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                linear_warrantees.addView(warrantiesNameAddressDetail);
            }

        } else {

            TextView tv_warrantees = new TextView(this);
            tv_warrantees.setText("  No data available");
            tv_warrantees.setTextColor(ContextCompat.getColor(this, R.color.color_black));
            Utility.changefonts(tv_warrantees, this, "Calibri.ttf");

            linear_warrantees.addView(tv_warrantees);
            linear_warrantees.setGravity(Gravity.CENTER);
        }*/

    }


    /**
     * Function to display simple Alert Dialog
     *
     * @param context - application context
     * @param title   - alert dialog title
     * @param message - alert message
     * @param status  - success/failure (used to set icon)
     */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, RecyclerView.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }
    }

    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
