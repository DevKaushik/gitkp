package com.kp.facedetection.utility;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class Constants {

	public static String device_IMEI_No="";
	public static String device_token="";

	//public static List<String> policeStationArrayList = new ArrayList<String>();
	public static List<String> crimeCategoryArrayList = new ArrayList<String>();
	public static List<String> divisionArrayList = new ArrayList<String>();
	public static List<String> divCodeArrayList = new ArrayList<String>();
	public static List<String> policeStationNameArrayList = new ArrayList<String>();
	public static List<String> policeStationIDArrayList = new ArrayList<String>();
	public static List<String> IOCodeArrayList = new ArrayList<String>();
	public static List<String> IONameArrayList = new ArrayList<String>();
	public static List<String> firStatusArrayList = new ArrayList<String>();
	public static List<String> crimeTimeArrayList = new ArrayList<String>();

	public static List<String> investigateUnitCodeArrayList = new ArrayList<String>();
	public static List<String> investigateUnitNameArrayList = new ArrayList<String>();

	public static Long prev_timeStamp;
	public static String prev_timeStampString="";
	public static BaseActivity baseActivity;
	public static long time_diff;
	public static int resume_count=0;
	public static int logout_interval = 7200;//2hr
	public static final String [] PURPOSE_OF_VISIT={"Business","Conference","Employment","Education","Leisure/Holiday","Marrraige Reception","Medical/Health","Religion/Pilgrimage","Sports","Transit","Visit Friends","Official","Others","Tour"};
	public static final String [] ALL_DOCUMENT_SEARCH_TYPE={"DL SEARCH","VEHICLE SEARCH","SDR SEARCH","CABLE SEARCH","KMC SEARCH","GAS SEARCH"};
	public static final String[] SPECIAL_SERVICE_ARRAY= {"CDR","SDR","IMEI TRACK","TOWER LOCATION","IPDR","ILD","CALL DUMP","CAF","IP RESOLUTION","LI","MNP","Recharge Details","CELL-ID CHART"};
	public static final String[] SPECIAL_SERVICE_VALUE= {"1","2","3","4","5","6","7","8","9","10","11","12","13"};
	public static final String[] SPECIAL_SERVICE_REQUEST_STATUS_ARRAY= {"Waiting for OC Approval","Waiting for AC Approval","Waiting for DC Approval","Sent to monitoring cell","Yet To Be Send To Provider","Sent To Provider","Process Completed"};
    public static final String[] SPECIAL_SERVICE_REQUEST_STATUS_ARRAY_VALUE= {"1","2","3","4","5","6","7"};
	public static final String[] SPECIAL_SERVICE_REQUEST_SUBTYPE= {"Reconnection","Disconnection","New Request"};


    //public static final String API_URL="http://54.169.73.48/index.php/api/"; //old
	//public static final String API_URL="http://182.71.240.209/crimebabu/api/"; // changed on 31/07/2017
	  public static final String API_URL="http://182.71.240.211/crimebabuapp/api/"; // live old
	//public static final String API_URL="http://182.71.240.212/crsapp/api/";//new current KP Database
	//public static final String API_URL="http://182.71.240.209/crimebabu_app/api/"; //new database

	public static final String GEO_CODING_URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=TANAY_LAT,TANAY_LON&key=GEO_API_KEY";

	//for document fragment
	public static final String DOCUMENT_SEARCH_URL="http://182.71.240.209/cb_advanced";

	//	http://54.169.73.48/index.php/api/picturesearchbyid  //old
	//public static final String METHOD_AUTHENTICATE="userAuthenticate"; //old
	public static final String METHOD_AUTHENTICATE="userLogin";  //new
	//public static final String METHOD_AUTHENTICATE2= "userLoginv2";  //new
	public static final String METHOD_AUTHENTICATE2= "userLoginv3";  //new

	public static final String METHOD_COUNT_MIS_ONLINE_USERS="misCount";
	public static final String METHOD_ONLINE_USERS="OnlineUsersList";
	public static final String METHOD_ACTIVE_USERS="activeUsersList";
	public static final String METHOD_CRIMINAL_SEARCH=/*"criminalSearch"*//*"criminalSearchv3"*/"criminalSearchv4";
	public static final String METHOD_CRIMINAL_SEARCH_PAGING=/*"criminalSearch"*//*"criminalSearchv3"*/"criminalSearchv4";//need to change
	public static final String METHOD_FORGET_PASSWORD="forgetPassword";
	public static final String OBJECT="OBJECT";
	public static final String NOTIFICATION_ID="NOTIFICATION_ID";
	public static final String METHOD_PICTURE_SEARCH="picturesearchbyid";
	public static final String IMAGE_BASE_URL="";//http://54.169.73.48/uploads/criminal_pic/
	public static final String METHOD_SEND_PUSH="sendpushAndroid";
	public static final String METHOD_NOTIFICATION_DETAILS="notificationDetails";
	public static final String METHOD_LOGOUT="userLogout";
	public static final String METHOD_GETCONTENT="allcontentlist";
	public static final String METHOD_IDENTITY_CATEGORY="identitycategory";
	public static final String METHOD_IDENTITY_SUB_CATEGORY="identitysubcategory";
	public static final String METHOD_DIVISIONWISE_POLICESTATION="divisionwisePolicestation";
	public static final String METHOD_CASE_SEARCH="caseSearch";
	public static final String METHOD_CASE_SEARCH_PAGING="caseSearch";
	public static final String METHOD_CRIMINAL_DETAILS="criminalallDetails";
	public static final String METHOD_CRIMINAL_PROFILE_DETAILS="criminalprofileDetails";
	public static final String METHOD_CRIMINAL_WARRENT_DETAILS="criminalMultipleWarrantDetails";
	public static final String METHOD_CRIMINAL_ARREST_DETAILS="criminalMultipleArrestDetails";
	public static final String METHOD_CRIMINAL_FIR_DETAILS="criminalFIRDetails";
	public static final String METHOD_CRIMINAL_PROFILE_DESCRIPTIVE_ROLE="criminalprofileDescriptiverolev4";
	//public static final String METHOD_CRIMINAL_PROFILE_DESCRIPTIVE_ROLE="http://182.71.240.209/crimebabu/api/criminalprofileDescriptiverolev4";
	//public static final String METHOD_CRIMINAL_PROFILE_DESCRIPTIVE_ROLE_UPDATE="http://182.71.240.209/crimebabu/api/updateDescriptiveRole";
	public static final String METHOD_CRIMINAL_PROFILE_DESCRIPTIVE_ROLE_UPDATE="updateDescriptiveRole";

	public static final String METHOD_CRIMINAL_PROFILE_CRS="criminalprofilecrsReferencev4";
	//public static final String METHOD_CRIMINAL_PROFILE_CRS="http://182.71.240.209/crimebabu/api/criminalprofilecrsReferencev4";
	public static final String METHOD_CRIMINAL_PROFILE_ASSOCIATES="criminalprofileAssociatesv4";
	//public static final String METHOD_CRIMINAL_PROFILE_ASSOCIATES="http://182.71.240.209/crimebabu/api/criminalprofileAssociatesv4";
	public static final String METHOD_CRIMINAL_MODIFIED_FIR_DETAILS="criminalFIRListv4";
	//public static final String METHOD_CRIMINAL_MODIFIED_FIR_DETAILS="http://182.71.240.209/crimebabu/api/criminalFIRListv4";

	public static final String METHOD_CRIMINAL_MODIFIED_ARREST_DETAILS="arrestDetailsv4";
	//public static final String METHOD_CRIMINAL_MODIFIED_ARREST_DETAILS="http://182.71.240.209/crimebabu/api/arrestDetailsv4";

	public static final String METHOD_CRIMINAL_MODIFIED_WARRANT_DETAILS="warrantDetailsv4";
	//public static final String METHOD_CRIMINAL_MODIFIED_WARRANT_DETAILS="http://182.71.240.209/crimebabu/api/warrantDetailsv4";

	public static final String METHOD_MODIFIED_CASE_SEARCH="caseSearchv4";
	//public static final String METHOD_MODIFIED_CASE_SEARCH_FIR_DETAILS="http://182.71.240.209/crimebabu/api/caseSeachFirDetailsv4";
	public static final String METHOD_MODIFIED_CASE_SEARCH_FIR_DETAILS="caseSeachFirDetailsv5";
	public static final String METHOD_MODIFIED_CASE_SEARCH_FIR_DETAILS_FROM_UNNATURAL_DEATH="UnnaturalFirDetails";

	public static final String METHOD_UPDATE_FIR_DOWNLOADLOG="firDownloadlog";
	public static final String METHOD_UPDATE_PM_DOWNLOADLOG="pmDownloadlog";
	public static final String METHOD_WARRANT_SEARCH="warrantSearchMobV4";
	public static final String METHOD_WARRANT_SEARCH_ITEM_DETAILS="warrantSearchDetailsMobV4";
	public static final String METHOD_POST_COMMENT="postComment";
	public static final String METHOD_CASE_FIRLOGLIST="FirLogList";
	//public static final String METHOD_ALL_DOCUMENT_SEARCH = "http://192.168.1.10/findmehere/api/multipleSearch";
	public static final String METHOD_ALL_DOCUMENT_SEARCH = "https://kpinfohub.in/tb/api/multipleSearch";//all document search
	public static final String METHOD_ALL_DOCUMENT_RELATIONAL_SEARCH = "https://kpinfohub.in/tb/api/relationSearch";//all document relation search
    //http://192.168.1.246/mcli/api/form_entry
	public static final String METHOD_SPECIAL_SEARCH= "https://kpinfohub.in/mcli/Api/form_entry_new";//Special search
	//public static final String METHOD_SPECIAL_SERVICE_LIST= "http://192.168.1.246/mcli/api/view_list";//Special search

	//public static final String METHOD_SPECIAL_SERVICE_LIST= "https://kpinfohub.in/mcli/Api_V2/view_list";//Special Service
	public static final String METHOD_SPECIAL_SERVICE_LIST= "https://kpinfohub.in/mcli/Api/pendingView2";//Special Service
	public static final String METHOD_SPECIAL_SERVICE_SEARCH_LIST= "https://kpinfohub.in/mcli/Api/searchResultsV2";//Special Service

	public static final String METHOD_CURRENT_VERSION = "getCurrVer";

	public static final String METHOD_SPECIAL_SERVICE_ALLOW_USER_LIST= "http://182.71.240.211/crimebabuapp/api/SVCallowuserlist";



	//public static final String METHOD_SPECIAL_SERVICE_SEARCH_LIST= "https://kpinfohub.in/mcli/Api/searchResults";//Special Service




	public static final String METHOD_SPECIAL_SERVICE_REQUEST_PROCESS_TYPE= "https://kpinfohub.in/mcli/Api/set_urgent_general_flag";
	public static final String METHOD_SPECIAL_SERVICE_SELF_APPROVAL= "https://kpinfohub.in/mcli/Api/self_approval";
	public static final String METHOD_SPECIAL_SERVICE_SELF_SKIP= "https://kpinfohub.in/mcli/Api/skipped_approval";
	public static final String METHOD_SPECIAL_SERVICE_REVOKE= "https://kpinfohub.in/mcli/Api/regret_request";
	public static final String METHOD_SPECIAL_SERVICE_LI_DETAILS_STATUS_CHANGE= "https://kpinfohub.in/mcli/Api/li_details_status_change";

	//public static final String METHOD_SPECIAL_SERVICE_OC_APPROVAL= "https://kpinfohub.in/mcli/Api/OC_approval";//Special Service
	//public static final String METHOD_SPECIAL_SERVICE_DC_APPROVAL= "https://kpinfohub.in/mcli/Api/DC_approval";//Special Service
	public static final String METHOD_SPECIAL_SERVICE_REQUEST_APPROVAL= "http://182.71.240.211/crimebabuapp/api/special_service_request_permissionsv2";//Special Service
	public static final String METHOD_SPECIAL_SERVICE_USER_PASSCODE= "http://182.71.240.211/crimebabuapp/api/spl_pass_service";//Special Service sp_user_code
	public static final String METHOD_SPECIAL_SERVICE_LI_DATA= "https://kpinfohub.in/mcli/Api/logger_group_info";//
	public static final String METHOD_LI_SDR_DETAILS_AGAINST_PHONE= "https://kpinfohub.in/tb/Api/checkPhoneNumber";//
	public static final String METHOD_LI_PHONE_BLACKLIST= "https://kpinfohub.in/mcli/Api/checkBlackList";//
	public static final String METHOD_LI_PARTIAL_SAVE= "https://kpinfohub.in/mcli/Api/get_record_draft";
	public static final String METHOD_LI_RECONNECTION_JUSTIFICATION_PHONE= "https://kpinfohub.in/mcli/Api_V2/get_reconnection_just_details";//Special Service
	public static final String METHOD_LI_RECONNECTION= "https://kpinfohub.in/mcli/Api_V2/create_reconnection";//Special Service LI reconnection
	public static final String METHOD_LI_DISCONNECTION= "https://kpinfohub.in/mcli/Api_V2/create_disconnection";//Special Service LI disconnection
	public static final String METHOD_REMINDER_TO_MCELL= "https://kpinfohub.in/mcli/Api/setReminder";//Special Service reminder
	public static final String METHOD_LI_REQUEST_NODAL_SUGGESTION= "https://kpinfohub.in/mcli/Api/saveSuggestion";//Special Service nodal suggestion
	public static final String METHOD_LI_REQUEST_NODAL_SUGGESTION_ONLOAD_DETAILS= "https://kpinfohub.in/mcli/Api/getSuggestion";

    public static final String METHOD_LI_MCELL_OBSERBATION_DETAILS= "https://kpinfohub.in/mcli/Api/getObservations";






	//public static final String METHOD_DOCUMENT_DL_SEARCH = "http://182.71.240.209/fmh_dapt/api/dl_data";
	public static final String METHOD_DOCUMENT_HOTEL_SESSION_ID = "http://182.71.240.212:81/HotelApi/genkey.php";
	//public static final String METHOD_DOCUMENT_SESSION_ID = "http://192.168.1.10/findmehere/api/get_session_id";
	public static final String METHOD_DOCUMENT_SESSION_ID = "https://kpinfohub.in/tb/api/get_session_id";//api for session id of document
	public static final String METHOD_DOCUMENT_DL_SEARCH = "http://182.71.240.211/fmh_dapt/api/dl_dataV3";
//	public static final String MODIFIED_METHOD_DOCUMENT_DL_SEARCH = "http://192.168.1.10/findmehere/api/dl_data_multi_search";
	public static final String MODIFIED_METHOD_DOCUMENT_DL_SEARCH = "https://kpinfohub.in/tb/api/dl_data_multi_search";// new api for dl
   // public static final String METHOD_VEHICLE_SEARCH = "http://182.71.240.209/fmh_dapt/api/vehicle_data";
    public static final String METHOD_VEHICLE_SEARCH = "http://182.71.240.211/fmh_dapt/api/vehicle_dataV3";
	//public static final String MODIFIED_METHOD_VEHICLE_SEARCH = "http://192.168.1.10/findmehere/api/vehicle_data_multi_search";
	public static final String MODIFIED_METHOD_VEHICLE_SEARCH = "https://kpinfohub.in/tb/api/vehicle_data_multi_search";// new api for vehicle

	public static final String METHOD_GET_GAS_SEARCH_CONTENT = "http://182.71.240.211/fmh_dapt/api/get_gas_map";
	public static final String METHOD_GAS_SEARCH = "http://182.71.240.211/fmh_dapt/api/gas_data";
	public static final String METHOD_DOCUMENT_GAS_SEARCH = "http://182.71.240.211/fmh_dapt/api/gas_dataV3";
	//public static final String MODIFIED_METHOD_DOCUMENT_GAS_SEARCH = "http://192.168.1.10/findmehere/api/gas_data_multi_search";
    public static final String MODIFIED_METHOD_DOCUMENT_GAS_SEARCH = "https://kpinfohub.in/tb/api/gas_data_multi_search"; // new api for gas
	public static final String METHOD_GET_KMC_SEARCH_CONTENT = "http://182.71.240.211/fmh_dapt/api/get_kmc_map";
	public static final String METHOD_KMC_SEARCH = "http://182.71.240.211/fmh_dapt/api/kmc_data";
	public static final String MODIFIED_METHOD_KMC_SEARCH = "http://192.168.1.10/findmehere/api/kmc_data_multi_search";

	public static final String METHOD_DOCUMENT_KMC_SEARCH = "http://182.71.240.211/fmh_dapt/api/kmc_dataV3";
   //public static final String MODIFIED_METHOD_DOCUMENT_KMC_SEARCH = "http://192.168.1.10/findmehere/api/kmc_data_multi_search";
	public static final String MODIFIED_METHOD_DOCUMENT_KMC_SEARCH = "https://kpinfohub.in/tb/api/kmc_data_multi_search"; // new api for kmc

	public static final String METHOD_GET_CABLE_SEARCH_CONTENT = "http://182.71.240.211/fmh_dapt/api/get_cable_map";
	public static final String METHOD_CABLE_SEARCH = "http://182.71.240.211/fmh_dapt/api/cable_data";
	public static final String METHOD_DOCUMENT_CABLE_SEARCH = "http://182.71.240.211/fmh_dapt/api/cable_dataV3";
	//public static final String MODIFIED_METHOD_DOCUMENT_CABLE_SEARCH = "http://192.168.1.10/findmehere/api/cabel_data_multi_search";
	public static final String MODIFIED_METHOD_DOCUMENT_CABLE_SEARCH = "https://kpinfohub.in/tb/api/cabel_data_multi_search";// new api for cable
	public static final String METHOD_UPDATE_APPVERSION="updateAppVersion";
	//public static final String METHOD_SDR_SEARCH="http://182.71.240.209/fmh_dapt/api/sdr_data";
	public static final String METHOD_SDR_SEARCH="http://182.71.240.211/fmh_dapt/api/sdr_datav1";
	//public static final String MODIFIED_METHOD_SDR_SEARCH="http://192.168.1.10/findmehere/api/sdr_data_multi_search";
	public static final String MODIFIED_METHOD_SDR_SEARCH="https://kpinfohub.in/tb/api/sdr_data_multi_search";// new api for sdr
	public static final String MODIFIED_METHOD_SDR_SEARCH_ALL_STATE="https://kpinfohub.in/tb/api/getStateListOpen";// new api for sdr

	//public static final String METHOD_BAR_SEARCH="http://192.168.1.10/findmehere/api/bar_data_multi_search";
	public static final String METHOD_BAR_SEARCH="https://kpinfohub.in/tb/api/bar_data_multi_search";//new api for bar
	public static final String IMAGE_URL_NATIONAL="http://182.71.240.212/kpab/FormA/Photographs/";
	public static final String IMAGE_URL_FOREIGN="http://182.71.240.212/kpab/FormB/Photographs/";
	public static final String METHOD_HOTEL_SEARCH="http://182.71.240.212:81/HotelApi/index.php";// new api for hotel
	public static final String METHOD_GET_ALL_HOTEL_LIST="http://182.71.240.212:81/HotelApi/getHotels.php";

	public static final String METHOD_FORGOT_PSWD="forgotUserPass";
	public static final String SMS_GATEWAY_API="http://sms.pinnsafe.in/api/api_http.php?username=egovernance&password=Eg0v@kpd&senderid=KPPIIN&to=";
	public static final String METHOD_CHANGE_PASSWORD="change_password";
	public static final String METHOD_DIV_WISE_PS="divisionwisePolicestation";
	public static final String METHOD_PS_WISE_IO="PSwiseIOlist";
	public static final String METHOD_MOBILE_STOLEN_SEARCH="mobileLost";
	public static final String METHOD_MOBILE_STOLEN_STATUS="getMissingMobileStatus";
	//public static final String METHOD_LIST_FOR_PMREPORT="list_for_pmreport";
	public static final String METHOD_LIST_FOR_PMREPORT="http://182.71.240.211/crimebabuapp/api/list_for_pmreport";
	//public static final String METHOD_PMREPORT_SEARCH="pm_report";
	public static final String METHOD_PMREPORT_SEARCH="http://182.71.240.211/crimebabuapp/api/pm_report";
	public static final String METHOD_GET_CLASS_SUBCLASS="getClassSubclass";
	public static final String METHOD_MAP_SEARCH="getMapData";
	//public static final String METHOD_MAP_VIEW="getMapDataV2";
	public static final String METHOD_MAP_VIEW="getMapDataV3";
	public static final String METHOD_CAPTURE_LATONG="CaptureLatLong";
	//public static final String METHOD_CRIMINAL_PIC_UPDATE ="CriminalPicUpdate";
	public static final String METHOD_CRIMINAL_PIC_UPDATE =API_URL+"criminalPicUpdload";
	//public static final String METHOD_CRIMINAL_PIC_UPDATE ="http://crimebabu.kolkatapolice.org/imageupload/criminalPicUpdload";
	public static final String METHOD_PIC_UPLOAD = "http://182.71.240.209/crimebabu/api/CriminalPicUpdate";
	public static final String METHOD_COURT_CASE_CONTENT="http://182.71.240.211/crimebabuapp/api/getCourtContentList";
	public static final String METHOD_COURT_CASE_LIST_SEARCH="CourtCaseList";
	public static final String METHOD_ALL_DIVISIONlIST = "allDivisionList";
	public static final String METHOD_TEMP_FIR_CAPTURE = "tempFirCapture";
	public static final String METHOD_FEEDBACK = "feedback";
	public static final String METHOD_DASHBOARD = "dashboard";
	public static final String METHOD_CRIME_REVIEW = "crimeReviewv1";
	public static final String METHOD_CRIME_REVIEW_BY_PS = "crimeReviewByPS";
	public static final String METHOD_ALL_FIR_VIEW_BY_CATEGORY = "allFirViewByCategory";
	public static final String METHOD_ALL_FIR_VIEW = "allFirView1";
	public static final String METHOD_ALL_RTA_VIEW = "allRTAView";
	public static final String METHOD_ALL_SPECIFIC_ARREST_VIEW = "allSpecificArrestView";
	public static final String METHOD_ALL_OTHER_ARREST_VIEW = "allOtherArrestView";
	public static final String METHOD_ALL_DISPOSALS = "allDisposals";
	public static final String METHOD_ALL_WA_EXEC = "allWAExec";
	public static final String METHOD_ALL_WA_EXEC_DETAILS = "allWAExecDetails";
	public static final String METHOD_ALL_WA_RECV = "allWARecv";
	public static final String METHOD_ALL_WA_FALL = "allWAFail";
    public static final String METHOD_ALL_WA_DUE = "allWANotExec";
	public static final String METHOD_ALL_WA_RECV_DETAILS = "allWARecvDetails";
	public static final String METHOD_ALL_WA_FALL_DETAILS = "allWAFailDetails";
	public static final String METHOD_ALL_WA_DUE_DETAILS = "allWANotExecDetails";
	public static final String METHOD_CASE_SEARCH_FROM_DASHBOARD="caseSearchFromDashboard";
	public static final String METHOD_MAP_VIEW_DISPOSAL="getMapDataV4";
	public static final String METHOD_ALL_WA_PENDING = "allWAPending";
	public static final String METHOD_ALL_WA_PENDING_DETAILS = "allWAPendingDetails";
	public static final String METHOD_LIST_CAPTURE = "listCapture";
	public static final String METHOD_CAPTURE_ADD_TO_TAG = "addToTag";
	public static final String METHOD_UNTAG_RECORD = "untagRecord";
	public static final String METHOD_DELETE_TEMP_FIR = "deleteTempFir";
	public static final String METHOD_UNNATURAL_DEATH_LIST = "allunnaturalDeaths";
	public static final String METHOD_UNNATURAL_DEATH_DETAILS = "allUnnaturalDeathDetails";
	public static final String METHOD_CHARGESHEET_NOTIFICATION_UPDATE = "notification";
	public static final String METHOD_CHARGESHEET_PS_LIST = "allCSDue";
	public static final String METHOD_CHARGESHEET_CS_DETAILS = "allCSDueDetails";
	public static final String METHOD_CHARGESHEET_CS_DETAILS_NOTIFICATION = "notificationIO_OC";
	public static final String METHOD_CHARGESHEET_PS_LIST_NOTIFICATION = "notificationDC";

	public static final String USER_ROLE_ORS = "0";
	public static final String USER_ROLE_IO = "1";
	public static final String USER_ROLE_OC = "2";
	public static final String USER_ROLE_DC = "3";

	public static final String WARRENT_TYPE_FAIL = "1";
	public static final String WARRENT_TYPE_DUE = "2";



	public static String documentSearchDLResponse = null;

	public static final int GALLERY_INTENT_CALLED = 11;
	public static final int CAPTURE_INTENT_CALLED = 13;
	public static final int GALLERY_KITKAT_INTENT_CALLED = 12;
	public static final int MEDIA_TYPE_IMAGE = 10;

	public static final int PERMISSION_REQUEST = 1001;

	public static boolean isShowWhatsNew = false;

	// All Error msg text
	public static final String SUCCESS = "Data Updated Successfully";
	public static final String ERROR_TITLE = "Connectivity Error !!!";
	public static final String ERROR = "Error !!!";
	public static final String ERROR_PRV_CRIMINAL_NO = "Provisional criminal number not found.Please try again later";

	public static final String UPDATION_ERROR = "Sorry! Failed to update";
	public static final String EXPANDABLE_VIEW_ERROR = "Please open the Descriptive Role";
	public static final String DESCRIPTIVE_ROLE_EDIT_MESSAGE = "Tap on individual features to store values and tap update icon to save";


	public static final String ERROR_MSG = "Sorry! Internet connection not found";
	public static final String ERROR_MSG_SPLASH = "Please Check Internet Connection";
	public static final String ERROR_DETAILS_TITLE = "Details Error !!!";
	public static final String ERROR_MSG_DETAIL = "Sorry! No details found";
	public static final String ERROR_PDF_DOWNLOAD_MSG = "Sorry! Report download failed.Please try again";

	public static final String ERROR_EVENT_DETAIL = "Sorry! No event details found";
	public static final String ERROR_MSG_CASE_LOG = "Sorry! No case log found";
	public static final String ERROR_MSG_DOWNLOAD_LOG = "Sorry! No FIR download log found";
	public static final String ERROR_MSG_NO_RECORD = "Sorry! No record found";
	public static final String ERROR_MSG_NO_CASE_YEAR = "Sorry! No case year found";
	public static final String ERROR_MSG_NO_UNREAD_NOTIFICATION = "No unread notification found";



	public static final String SEARCH_ERROR_TITLE = "Search Error !!!";
	public static final String ERROR_MSG_TO_RELOAD = "Something wrong! Please reload the page";
	public static final String ERROR_EXCEPTION_MSG = "Something wrong! Please try again later";
	public static final String ERROR_MAP_MSG = "Sorry! Information not available";
	public static final String ERROR_LOCATION_NOT_FOUND = "Sorry! Current location not found";//"Sorry, can not fetch current location.";
	public static final String ERROR_MSG_PREVIEW_NOT_FOUND = "Sorry! Preview not available";
	public static final String ERROR_CAPTURE_LOG_MSG = "Something wrong! Please try again";
	public static final String SUCCESS_CAPTURE_LOG_TITLE = "Result !!!";
	public static final String SUCCESS_CAPTURE_LOG_MSG = "Successfully captured";
	public static final String ERROR_MANDATE_FIELD_MSG = "Please enter value for all fields";
	public static final String ERROR_CAPTURE_ALERT_TITLE = "Alert !!!";
	public static final String ERROR_CAPTURE_INFO_TITLE = "Information !!!";

	public static final String ERROR_CAPTURE_ALERT_MSG = "Sorry! Fir details are not exist";
	public static final String ERROR_NO_LOCATION_FOUND_MSG = "Sorry! No location is available";
	public static final String DELETE_CAPTURE_LOG_ITEM = "Record is deleted";


	// Status text
    public static final String STATUS_INVESTIGATION_IN_PROGRESS = "INVESTIGATION IN PROGRESS";
    public static final String STATUS_PENDING = "Pending";
    public static final String STATUS_FINAL_REPORT_NON_COGNIZABLE = "FINAL REPORT - NON-COGNIZABLE";
    public static final String STATUS_FR_NON_COG = "FR-Non-cog";

    public static final String STATUS_CHARGESHEETED = "CHARGESHEETED";
    public static final String STATUS_CS = "C/S";

    public static final String STATUS_FINAL_REPORT_CIVIL = "FINAL REPORT - CIVIL IN NATURE";
    public static final String STATUS_FR_CIVIL = "FR-CIVIL";

    public static final String STATUS_FINAL_REPORT_TRUE = "FINAL REPORT - TRUE";
    public static final String STATUS_FRT = "FRT";

    public static final String STATUS_TRANSFERRED_TO_OTHER_UNIT = "TRANSFERRED TO OTHER UNIT";
    public static final String STATUS_TRFD = "Trfd. to Other Unit";

    public static final String STATUS_FINAL_REPORT_FALSE = "FINAL REPORT - FALSE";
    public static final String STATUS_FR_FALSE = "FR-False";

	public static void changefonts(TextView textViewpre_font,
								   Context mContext, String font) {

		Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/" + font);

		try {
			textViewpre_font.setTypeface(tf);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void buttonEffect(View button){
		button.setOnTouchListener(new View.OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: {
						//v.getBackground().setColorFilter(0xe02353B1, PorterDuff.Mode.SRC_ATOP);
						v.getBackground().setColorFilter(0xe0030D39, PorterDuff.Mode.SRC_ATOP);
						v.invalidate();
						break;
					}
					case MotionEvent.ACTION_UP: {
						v.getBackground().clearColorFilter();
						v.invalidate();
						break;
					}
				}
				return false;
			}
		});
	}

	public static void loginButtonEffect(View button){
		button.setOnTouchListener(new View.OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: {
						v.getBackground().setColorFilter(0xA6030D27, PorterDuff.Mode.SRC_ATOP);
						v.invalidate();
						break;
					}
					case MotionEvent.ACTION_UP: {
						v.getBackground().clearColorFilter();
						v.invalidate();
						break;
					}
				}
				return false;
			}
		});
	}

	public static boolean internetOnline(Context c) {

		boolean status = false;

		ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (activeNetwork != null) { // connected to the internet
			if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
				// connected to wifi
				status = true;
			} else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
				// connected to the mobile provider's data plan
				status = true;
			}
		} else {
			// not connected to the internet
			status = false;
		}

		return status;
	}

}
