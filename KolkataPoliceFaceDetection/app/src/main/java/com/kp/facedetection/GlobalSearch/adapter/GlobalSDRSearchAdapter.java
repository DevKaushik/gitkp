package com.kp.facedetection.GlobalSearch.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.SDRDetails;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by DAT-165 on 28-07-2016.
 */
public class GlobalSDRSearchAdapter extends BaseAdapter {

    private Context context;
    private List<SDRDetails> sdrDetailsList;

    public GlobalSDRSearchAdapter(Context context, List<SDRDetails> sdrDetailsList) {
        this.context = context;
        this.sdrDetailsList = sdrDetailsList;
    }


    @Override
    public int getCount() {

        int size = (sdrDetailsList.size()>0)? sdrDetailsList.size():0;
        return size;

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.modified_sdr_search_list_item, null);


            holder.tv_slNo=(TextView)convertView.findViewById(R.id.tv_slNo);
            holder.tv_name=(TextView)convertView.findViewById(R.id.tv_name);
            holder.tv_id_number=(TextView)convertView.findViewById(R.id.tv_id_number);
            holder.tv_id_number.setVisibility(View.VISIBLE);
            holder.tv_id_type=(TextView)convertView.findViewById(R.id.tv_id_type);
            holder.tv_id_type.setVisibility(View.VISIBLE);
            holder.tv_address=(TextView)convertView.findViewById(R.id.tv_address);
            holder.tv_mobileNo=(TextView)convertView.findViewById(R.id.tv_mobileNo);

            Constants.changefonts(holder.tv_slNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_name, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_id_number, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_id_type, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_address, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_mobileNo, context, "Calibri.ttf");

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);

        holder.tv_slNo.setText(sl_no);
        holder.tv_name.setText("NAME: "+sdrDetailsList.get(position).getName().trim());
        holder.tv_id_number.setText("ID NUMBER: "+sdrDetailsList.get(position).getId_Number().trim());
        holder.tv_id_type.setText("ID_TYPE: "+sdrDetailsList.get(position).getId_type().trim());
        holder.tv_address.setText("ADDRESS: "+sdrDetailsList.get(position).getPresent_address().trim());
        holder.tv_mobileNo.setText("MOBILE NO: "+sdrDetailsList.get(position).getMobNo().trim());

        return convertView;
    }

    class ViewHolder {

        TextView tv_slNo,tv_name,tv_address,tv_mobileNo,tv_id_number,tv_id_type;

    }
}
