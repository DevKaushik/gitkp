package com.kp.facedetection.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DAT-165 on 12-04-2017.
 */

public class ShowColorWithCategory implements Parcelable{

    private String color = "";
    private String category = "";

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ShowColorWithCategory)) return false;

        ShowColorWithCategory that = (ShowColorWithCategory) o;

        if (!color.equals(that.color)) return false;
        return category.equals(that.category);

    }

    @Override
    public int hashCode() {
        int result = color.hashCode();
        result = 31 * result + category.hashCode();
        return result;
    }



    public ShowColorWithCategory(Parcel in) {
        color = in.readString();
        category = in.readString();
    }

    public static final Creator<ShowColorWithCategory> CREATOR = new Creator<ShowColorWithCategory>() {
        @Override
        public ShowColorWithCategory createFromParcel(Parcel in) {
            return new ShowColorWithCategory(in);
        }

        @Override
        public ShowColorWithCategory[] newArray(int size) {
            return new ShowColorWithCategory[size];
        }
    };

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(color);
        dest.writeString(category);
    }
}
