package com.kp.facedetection.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.kp.facedetection.R;
import com.kp.facedetection.VehicleSearchResultActivity;
import com.kp.facedetection.model.VehicleSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kaushik on 10-09-2016.
 */
public class VehicleSearchFragment extends Fragment {


    private TextView tv_notAuthorized;
    private RelativeLayout relative_vehicle_main;
    private Button btn_vehicle_serach, btn_reset;
    private EditText et_vehicle_number, et_owner_name,
            et_vehicle_chasis_no, et_vehicle_address, et_vehicle_colour, et_vehicle_make, et_vehicle_pincode, et_holder_mobile_no, et_vehicle_engine_no;

    private List<VehicleSearchDetails> vehicleSearchDetailsList;

    private String pageno="";
    private String totalResult="";
    private String[] keys;
    private String[] values;
    String userId="";
    String devicetoken ="";
    String auth_key="";

    public static VehicleSearchFragment newInstance() {

        VehicleSearchFragment vehicleSearchFragment = new VehicleSearchFragment();
        return vehicleSearchFragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_vehicle_search, container, false);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        userId= Utility.getUserInfo(getContext()).getUserId();
        relative_vehicle_main = (RelativeLayout) view.findViewById(R.id.relative_vehicle_main);
        tv_notAuthorized = (TextView) view.findViewById(R.id.tv_notAuthorized);

        btn_vehicle_serach = (Button) view.findViewById(R.id.btn_vehicle_serach);
        btn_reset = (Button) view.findViewById(R.id.btn_vehicle_reset);

        Constants.buttonEffect(btn_vehicle_serach);
        Constants.buttonEffect(btn_reset);

        et_vehicle_number = (EditText) view.findViewById(R.id.et_vehicle_number);
        et_owner_name = (EditText) view.findViewById(R.id.et_owner_name);
        et_vehicle_chasis_no = (EditText) view.findViewById(R.id.et_vehicle_chasis_no);
        et_vehicle_address = (EditText) view.findViewById(R.id.et_vehicle_address);
        et_vehicle_colour = (EditText) view.findViewById(R.id.et_vehicle_colour);
        et_vehicle_make = (EditText) view.findViewById(R.id.et_vehicle_make);
        et_vehicle_pincode = (EditText) view.findViewById(R.id.et_vehicle_pincode);
        et_holder_mobile_no = (EditText) view.findViewById(R.id.et_holder_mobile_no);
        et_vehicle_engine_no = (EditText) view.findViewById(R.id.et_vehicle_engine_no);

        try {
            if (Utility.getUserInfo(getActivity()).getVehicle_allow().equalsIgnoreCase("1")) {
                relative_vehicle_main.setVisibility(View.VISIBLE);
                tv_notAuthorized.setVisibility(View.GONE);
            } else {
                relative_vehicle_main.setVisibility(View.GONE);
                tv_notAuthorized.setVisibility(View.VISIBLE);
                Constants.changefonts(tv_notAuthorized, getActivity(), "Calibri Bold.ttf");
            }
        }catch(NullPointerException e){
            e.printStackTrace();
            relative_vehicle_main.setVisibility(View.GONE);
            tv_notAuthorized.setVisibility(View.VISIBLE);
            Constants.changefonts(tv_notAuthorized, getActivity(), "Calibri Bold.ttf");
        }


        clickEvents();
    }

    private void clickEvents() {

        /* search button click event */

        btn_vehicle_serach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //fetchSearchResult();
                String vehicle_number = et_vehicle_number.getText().toString().trim();
                String owner_name = et_owner_name.getText().toString().trim();
                String vehicle_chasis_no = et_vehicle_chasis_no.getText().toString().trim();
                String vehicle_address = et_vehicle_address.getText().toString().trim();
                String vehicle_colour = et_vehicle_colour.getText().toString().trim();
                String vehicle_make = et_vehicle_make.getText().toString().trim();
                String vehicle_pincode = et_vehicle_pincode.getText().toString().trim();
                String vehicle_engineNo = et_vehicle_engine_no.getText().toString().trim();
                String holder_mobileNo = et_holder_mobile_no.getText().toString().trim();


                if (!vehicle_number.equalsIgnoreCase("") || !owner_name.equalsIgnoreCase("")
                        || !vehicle_chasis_no.equalsIgnoreCase("") || !vehicle_address.equalsIgnoreCase("") || !vehicle_colour.equalsIgnoreCase("")
                        || !vehicle_make.equalsIgnoreCase("") || !vehicle_pincode.equalsIgnoreCase("") || !vehicle_engineNo.equalsIgnoreCase("")
                        || !holder_mobileNo.equalsIgnoreCase("")) {

                    showAlertForProceed();
                } else {

                    showAlertDialog(getActivity(), " Search Error!!! ", "Please provide atleast one value for search", false);

                }

            }
        });

         /* reset button click event */

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_vehicle_number.setText("");
                et_owner_name.setText("");
                et_vehicle_chasis_no.setText("");
                et_vehicle_address.setText("");
                et_vehicle_colour.setText("");
                et_vehicle_make.setText("");
                et_vehicle_pincode.setText("");
                et_vehicle_engine_no.setText("");
                et_holder_mobile_no.setText("");

            }
        });

    }

    private void showAlertForProceed() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setTitle("Alert!");
        alertDialog.setMessage("It will take considerable time, proceed");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                fetchSearchResult();
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        alertDialog.show();
    }

    private void fetchSearchResult() {

        String vehicle_number = et_vehicle_number.getText().toString().trim();
        String owner_name = et_owner_name.getText().toString().trim();
        String vehicle_chasis_no = et_vehicle_chasis_no.getText().toString().trim();
        String vehicle_address = et_vehicle_address.getText().toString().trim();
        String vehicle_colour = et_vehicle_colour.getText().toString().trim();
        String vehicle_make = et_vehicle_make.getText().toString().trim();
        String vehicle_pincode = et_vehicle_pincode.getText().toString().trim();
        String vehicle_engineNo = et_vehicle_engine_no.getText().toString().trim();
        String holder_mobileNo = et_holder_mobile_no.getText().toString().trim();
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.MODIFIED_METHOD_VEHICLE_SEARCH);
        taskManager.setVehicleSearch(true);
        try{
            devicetoken = GCMRegistrar.getRegistrationId(getActivity());
            auth_key=Utility.getDocumentSearchSesionId(getActivity());
        }
        catch (Exception e){

        }

        String[] keys = {"veh_no", "veh_owner", "veh_chasis", "veh_address", "veh_color", "veh_make", "veh_pincode", "pageno", "eng_no", "holder_mobile","user_id","device_type", "device_token","imei","auth_key"};

        String[] values = {vehicle_number.trim(), owner_name.trim(), vehicle_chasis_no.trim(), vehicle_address.trim(), vehicle_colour.trim(), vehicle_make.trim(), vehicle_pincode.trim(),"1", vehicle_engineNo.trim(), holder_mobileNo.trim(), userId,"android", devicetoken,Utility.getImiNO(getActivity()),auth_key};

        taskManager.doStartTask(keys, values, true, true);

    }

    /**
     * Function to display simple Alert Dialog
     *
     * @param context - application context
     * @param title   - alert dialog title
     * @param message - alert message
     * @param status  - success/failure (used to set icon)
     */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    public void parseVehicleSearch(String result,String[] keys, String[] values) {

        //System.out.println("Vehicle Search Result: " + result);

        if(result != null && !result.equals("")){

            try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.optString("status").equalsIgnoreCase("1")){

                    this.keys=keys;
                    this.values=values;
                    pageno=jObj.optString("page");
                    totalResult=jObj.optString("count");

                    JSONArray resultArray=jObj.getJSONArray("result");

                    parseVehicleSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(getActivity()," Search Error ",jObj.optString("message"),false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                showAlertDialog(getActivity(), " Search Error!!! ", "Some error has been encountered", false);
            }
        }

    }

    private void parseVehicleSearchResponse(JSONArray resultArray){

        vehicleSearchDetailsList = new ArrayList<VehicleSearchDetails>();

        for(int i=0;i<resultArray.length();i++){

            JSONObject jsonObj=null;

            try {
                jsonObj=resultArray.getJSONObject(i);
                VehicleSearchDetails vehicleSearchDetails = new VehicleSearchDetails();

                String address = "";
                String p_address = "";

                if(!jsonObj.optString("ROWNUMBER").equalsIgnoreCase("null")){
                    vehicleSearchDetails.setRol(jsonObj.optString("ROWNUMBER"));
                }

                if(!jsonObj.optString("MOBILE_NO").equalsIgnoreCase("null")){
                    vehicleSearchDetails.setHolder_mobileNo(jsonObj.optString("MOBILE_NO"));
                }

                if(!jsonObj.optString("REGN_NO").equalsIgnoreCase("null")){
                    vehicleSearchDetails.setRegn_no(jsonObj.optString("REGN_NO"));
                }

                if(!jsonObj.optString("REGN_DT").equalsIgnoreCase("null")){
                    vehicleSearchDetails.setRegn_dt(jsonObj.optString("REGN_DT"));
                }

                if(!jsonObj.optString("RTO_CD").equalsIgnoreCase("null")){
                    vehicleSearchDetails.setRto_cd(jsonObj.optString("RTO_CD"));
                }

                if(!jsonObj.optString("ENG_NO").equalsIgnoreCase("null")){
                    vehicleSearchDetails.setEng_no(jsonObj.optString("ENG_NO"));
                }

                if(!jsonObj.optString("CHASI_NO").equalsIgnoreCase("null")){
                    vehicleSearchDetails.setChasis_no(jsonObj.optString("CHASI_NO"));
                }

                if(!jsonObj.optString("O_NAME").equalsIgnoreCase("null")){
                    vehicleSearchDetails.setO_name(jsonObj.optString("O_NAME"));
                }

                if(!jsonObj.optString("F_NAME").equalsIgnoreCase("null")){
                    vehicleSearchDetails.setF_name(jsonObj.optString("F_NAME"));
                }

                if(!jsonObj.optString("GARAGE_ADD").equalsIgnoreCase("null")){
                    vehicleSearchDetails.setGarage_address(jsonObj.optString("GARAGE_ADD"));
                }

                if(!jsonObj.optString("COLOR").equalsIgnoreCase("null")){
                    vehicleSearchDetails.setColor(jsonObj.optString("COLOR"));
                }

                if(!jsonObj.optString("MAKER_MODEL").equalsIgnoreCase("null")){
                    vehicleSearchDetails.setMaker_model(jsonObj.optString("MAKER_MODEL"));
                }

                if(!jsonObj.optString("CL_DESC").equalsIgnoreCase("null")){
                    vehicleSearchDetails.setCl_desc(jsonObj.optString("CL_DESC"));
                }

                if(!jsonObj.optString("ADD1").equalsIgnoreCase("null") && !jsonObj.optString("ADD1").equalsIgnoreCase("")){
                    address = address + jsonObj.optString("ADD1");
                }
                if(!jsonObj.optString("ADD2").equalsIgnoreCase("null") && !jsonObj.optString("ADD2").equalsIgnoreCase("")){
                    address = address + ", "+ jsonObj.optString("ADD2");
                }
                if(!jsonObj.optString("CITY").equalsIgnoreCase("null") && !jsonObj.optString("CITY").equalsIgnoreCase("")){
                    address = address + ", "+ jsonObj.optString("CITY");
                }
                if(!jsonObj.optString("PINCODE").equalsIgnoreCase("null") && !jsonObj.optString("PINCODE").equalsIgnoreCase("")){
                    address = address + ", "+ jsonObj.optString("PINCODE");
                }

                vehicleSearchDetails.setAddress(address);

                if(!jsonObj.optString("PADD1").equalsIgnoreCase("null") && !jsonObj.optString("PADD1").equalsIgnoreCase("")){
                    p_address = p_address + jsonObj.optString("PADD1");
                }
                if(!jsonObj.optString("PADD2").equalsIgnoreCase("null") && !jsonObj.optString("PADD2").equalsIgnoreCase("")){
                    p_address = p_address + ", "+ jsonObj.optString("PADD2");
                }
                if(!jsonObj.optString("PPINCODE").equalsIgnoreCase("null") && !jsonObj.optString("PPINCODE").equalsIgnoreCase("")){
                    p_address = p_address + ", "+ jsonObj.optString("PPINCODE");
                }

                vehicleSearchDetails.setP_address(p_address);

                vehicleSearchDetailsList.add(vehicleSearchDetails);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        Intent intent=new Intent(getActivity(), VehicleSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putExtra("VEHICLE_SEARCH_LIST", (Serializable) vehicleSearchDetailsList);
        startActivity(intent);

    }

}