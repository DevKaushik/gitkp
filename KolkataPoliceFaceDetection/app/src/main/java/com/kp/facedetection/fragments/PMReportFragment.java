package com.kp.facedetection.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.kp.facedetection.PMReportSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.model.DoneByListForPMReport;
import com.kp.facedetection.model.PMReportList;
import com.kp.facedetection.model.PMReportSearchDetails;
import com.kp.facedetection.model.StationListForPMReport;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class PMReportFragment extends Fragment implements View.OnClickListener {

    private TextView tv_psNameValue;
    private TextView tv_firPSCodeValue;
    private TextView tv_StationCodeValue;
    private TextView tv_doneByValue;

    private EditText et_inquestNoValue;
    private EditText et_inquestYr;
    private EditText et_pmNo;
    private EditText et_pmYear;
    private EditText et_nameDeceased;
    private EditText et_firNo;
    private EditText et_firYear;
    private EditText et_gender;

    private Spinner sp_AgeVal;
    private Spinner sp_AgeIndicator;

    private Button btn_pmReportSearch;
    private Button btn_reset;

    private String userId = "";
    private String pageno = "";
    private String totalResult = "";

    private String[] keys;
    private String[] values;

    //--- variables for PS
    protected String[] array_policeStation;
    protected ArrayList<CharSequence> selectedPoliceStations = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedPoliceStationsId = new ArrayList<CharSequence>();
    private String policeStationString = "";

    //--- variables for FIR_PS
    protected String[] array_policeStation_forFIR;
    protected ArrayList<CharSequence> selectedPoliceStationsForFIR = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedPoliceStationsIdForFIR = new ArrayList<CharSequence>();
    private String policeStationStringForFIR = "";

    //--- variables for StationCode
    protected String[] array_Station;
    protected ArrayList<CharSequence> selectedStations = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedStationsId = new ArrayList<CharSequence>();
    private String stationString = "";

    //--- variables for DoneByList
    protected String[] array_DoneBy;
    protected ArrayList<CharSequence> selectedDoneBy = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedDoneById = new ArrayList<CharSequence>();
    private String doneByString = "";

    ArrayAdapter<CharSequence> ageAdapter;
    ArrayAdapter<CharSequence> ageIndicatorAdapter;

    public JSONArray jsonArray_stationCodeList_PMReport;
    public JSONArray jsonArray_doneByList_PMReport;
    public PMReportList pmReportList;

    List<String> pmReportStationArrayList = new ArrayList<String>();
    List<String> pmReportStationCodeArrayList = new ArrayList<String>();
    ArrayList<StationListForPMReport> obj_StationList;

    List<String> pmReportDoneByArrayList = new ArrayList<String>();
    List<String> pmReportDoneByCodeArrayList = new ArrayList<String>();
    ArrayList<DoneByListForPMReport> obj_DoneByList;

    private List<PMReportSearchDetails> pmReportSearchDetailsList;
    ImageView toggleButtonSearchType;
    String ownjurisdictionFlag="1";
    boolean search_own_global_flag=false;


    public PMReportFragment() {
        // Required empty public constructor
    }

    public static PMReportFragment newInstance() {
        PMReportFragment pmReportFragment = new PMReportFragment();
        return pmReportFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pmreport, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        userId= Utility.getUserInfo(getContext()).getUserId();
        toggleButtonSearchType=(ImageView)view.findViewById(R.id.Tb_search_type);
        toggleButtonSearchType.setOnClickListener(this);
        tv_psNameValue = (TextView) view.findViewById(R.id.tv_psNameValue);
        tv_firPSCodeValue = (TextView) view.findViewById(R.id.tv_firPSCodeValue);
        tv_StationCodeValue = (TextView) view.findViewById(R.id.tv_StationCodeValue);
        tv_doneByValue = (TextView) view.findViewById(R.id.tv_doneByValue);

        et_inquestNoValue = (EditText) view.findViewById(R.id.et_inquestNoValue);
        et_inquestYr = (EditText) view.findViewById(R.id.et_inquestYr);
        et_pmNo = (EditText) view.findViewById(R.id.et_pmNo);
        et_pmYear = (EditText) view.findViewById(R.id.et_pmYear);
        et_nameDeceased = (EditText) view.findViewById(R.id.et_nameDeceased);
        et_firNo = (EditText) view.findViewById(R.id.et_firNo);
        et_firYear = (EditText) view.findViewById(R.id.et_firYear);
        et_gender = (EditText) view.findViewById(R.id.et_gender);

        sp_AgeVal = (Spinner) view.findViewById(R.id.sp_AgeVal);
        sp_AgeIndicator = (Spinner) view.findViewById(R.id.sp_AgeIndicator);

        btn_pmReportSearch = (Button) view.findViewById(R.id.btn_pmReportSearch);
        btn_reset = (Button) view.findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_pmReportSearch);
        Constants.buttonEffect(btn_reset);

        array_policeStation = new String[Constants.policeStationNameArrayList.size()];
        array_policeStation = Constants.policeStationNameArrayList.toArray(array_policeStation);

        array_policeStation_forFIR = new String[Constants.policeStationNameArrayList.size()];
        array_policeStation_forFIR = Constants.policeStationNameArrayList.toArray(array_policeStation_forFIR);

        //--- Set Age Value to Age Spinner
        ageAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Age_array, R.layout.simple_spinner_item);
        ageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_AgeVal.setAdapter(ageAdapter);
        sp_AgeVal.setSelection(0);

        //--- Set Age Indicator Value to AgeIndicator Spinner
        ageIndicatorAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.AgeIndicator_array, R.layout.simple_spinner_item);
        ageIndicatorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_AgeIndicator.setAdapter(ageIndicatorAdapter);
        sp_AgeIndicator.setSelection(0);

        getPMReportListData();

        clickEvents();
    }

    private void clickEvents() {

        //--- Police stations Click Event --
        tv_psNameValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Constants.policeStationNameArrayList.size() > 0) {
                    tv_psNameValue.setError(null);
                    showSelectPoliceStationsDialog();
                } else {
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }

            }
        });

        //--- Police stations Click Event --
        tv_firPSCodeValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Constants.policeStationNameArrayList.size() > 0) {
                    tv_firPSCodeValue.setError(null);
                    showSelectPoliceStationsDialogForFIR();
                } else {
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }

            }
        });

        //---StationCode Click Event
        tv_StationCodeValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pmReportStationArrayList.size() != 0) {
                    showSelectStationCodeDialog();
                } else {
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }
            }
        });

        //--- DoneBy Click Event
        tv_doneByValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pmReportDoneByArrayList.size() != 0) {
                    showSelectDoneByDialog();
                } else {
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }
            }
        });


        btn_pmReportSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchPMReportSearchResult();
            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDataFieldValues();
            }
        });
    }


    //--- This method shows Police Station list in a pop-up --
    protected void showSelectPoliceStationsDialog() {
        boolean[] checkedPoliceStations = new boolean[array_policeStation.length];
        int count = array_policeStation.length;

        for (int i = 0; i < count; i++) {
            checkedPoliceStations[i] = selectedPoliceStations.contains(array_policeStation[i]);
        }

        DialogInterface.OnMultiChoiceClickListener policeStationsDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedPoliceStations.add(array_policeStation[which]);
                    selectedPoliceStationsId.add((CharSequence) Constants.policeStationIDArrayList.get(which));
                } else {
                    selectedPoliceStations.remove(array_policeStation[which]);
                    selectedPoliceStationsId.remove((CharSequence) Constants.policeStationIDArrayList.get(which));
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Police Stations");
        builder.setMultiChoiceItems(array_policeStation, checkedPoliceStations, policeStationsDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedPoliceStations();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_psNameValue.setText("Select Police Stations");
                policeStationString = "";
                selectedPoliceStations.clear();
                selectedPoliceStationsId.clear();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }


    protected void onChangeSelectedPoliceStations() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for (CharSequence policeStation : selectedPoliceStations) {
            stringBuilder.append(policeStation + ",");
        }

        for (CharSequence policeStation : selectedPoliceStationsId) {
            stringBuilderId.append("\'" + policeStation + "\',");
        }

        if (selectedPoliceStations.size() == 0) {
            tv_psNameValue.setText("Select Police Stations");
            policeStationString = "";
        } else {
            tv_psNameValue.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
            policeStationString = stringBuilderId.toString().substring(0, stringBuilderId.toString().length() - 1);
        }
    }


    //--- This method shows Police Station list in a pop-up for FIR--
    protected void showSelectPoliceStationsDialogForFIR() {
        boolean[] checkedPoliceStations = new boolean[array_policeStation.length];
        int count = array_policeStation_forFIR.length;

        for (int i = 0; i < count; i++) {
            checkedPoliceStations[i] = selectedPoliceStationsForFIR.contains(array_policeStation_forFIR[i]);
        }

        DialogInterface.OnMultiChoiceClickListener policeStationsDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedPoliceStationsForFIR.add(array_policeStation_forFIR[which]);
                    selectedPoliceStationsIdForFIR.add((CharSequence) Constants.policeStationIDArrayList.get(which));
                } else {
                    selectedPoliceStationsForFIR.remove(array_policeStation_forFIR[which]);
                    selectedPoliceStationsIdForFIR.remove((CharSequence) Constants.policeStationIDArrayList.get(which));
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Police Stations");
        builder.setMultiChoiceItems(array_policeStation_forFIR, checkedPoliceStations, policeStationsDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedPoliceStationsForFIR();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_firPSCodeValue.setText("Select Police Stations");
                policeStationStringForFIR = "";
                selectedPoliceStationsForFIR.clear();
                selectedPoliceStationsIdForFIR.clear();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }


    protected void onChangeSelectedPoliceStationsForFIR() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for (CharSequence policeStation : selectedPoliceStationsForFIR) {
            stringBuilder.append(policeStation + ",");
        }

        for (CharSequence policeStation : selectedPoliceStationsIdForFIR) {
            stringBuilderId.append("\'" + policeStation + "\',");
        }

        if (selectedPoliceStationsForFIR.size() == 0) {
            tv_firPSCodeValue.setText("Select Police Stations");
            policeStationStringForFIR = "";
        } else {
            tv_firPSCodeValue.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
            policeStationStringForFIR = stringBuilderId.toString().substring(0, stringBuilderId.toString().length() - 1);
        }
    }


    private void resetDataFieldValues() {

        //--- for police stations part
        tv_psNameValue.setText("Select Police Stations");
        policeStationString = "";
        selectedPoliceStations.clear();
        selectedPoliceStationsId.clear();
        tv_psNameValue.setError(null);

        //--- for stations part
        tv_StationCodeValue.setText("Select Stations");
        stationString = "";
        selectedStations.clear();
        selectedStationsId.clear();
        tv_StationCodeValue.setError(null);

        //--- for doneBy part
        tv_doneByValue.setText("Select");
        doneByString = "";
        selectedDoneBy.clear();
        selectedDoneById.clear();
        tv_doneByValue.setError(null);

        //--- for FIR police stations part
        tv_firPSCodeValue.setText("Select Police Stations");
        policeStationStringForFIR = "";
        selectedPoliceStationsForFIR.clear();
        selectedPoliceStationsIdForFIR.clear();
        tv_firPSCodeValue.setError(null);

        et_inquestNoValue.setText("");
        et_inquestYr.setText("");
        et_pmNo.setText("");
        et_pmYear.setText("");
        et_nameDeceased.setText("");
        et_gender.setText("");
        et_firNo.setText("");
        et_firYear.setText("");

        sp_AgeVal.setSelection(0);
        sp_AgeIndicator.setSelection(0);
    }


    //---  Function to display simple Alert Dialog --
    private void showAlertDialog(Context context, String title, final String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    private void getPMReportListData() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_LIST_FOR_PMREPORT);
        taskManager.setPMReportListData(true);

        String[] keys = {};
        String[] values = {};
        taskManager.doStartTask(keys, values, true, true);
    }

    public void parseGetPMReportListDataResponse(String response) {
        //System.out.println("PM Report List Data: " + response);

        if (response != null && !response.equals("")) {

            JSONObject jObj = null;
            try {

                jObj = new JSONObject(response);
                pmReportList = new PMReportList();

                if (jObj != null && jObj.optString("status").equalsIgnoreCase("1")) {

                    pmReportList.setStatus(jObj.optString("status"));
                    pmReportList.setMessage(jObj.optString("message"));

                    jsonArray_stationCodeList_PMReport = jObj.optJSONObject("result").getJSONArray("station_code_list");
                    jsonArray_doneByList_PMReport = jObj.optJSONObject("result").getJSONArray("done_by_list");

                    for (int i = 0; i < jsonArray_stationCodeList_PMReport.length(); i++) {

                        StationListForPMReport stationListForPMReport = new StationListForPMReport(Parcel.obtain());
                        JSONObject row = jsonArray_stationCodeList_PMReport.getJSONObject(i);
                        stationListForPMReport.setStation_code(row.optString("STATIONCODE"));
                        stationListForPMReport.setStation_name(row.optString("STATION"));
                        stationListForPMReport.setStation_inchargeCode(row.optString("INCHCODE"));
                        stationListForPMReport.setStation_inchargeName(row.optString("STATION_INCHARGE"));

                        pmReportStationArrayList.add(row.optString("STATION"));
                        pmReportStationCodeArrayList.add(row.optString("STATIONCODE"));

                        pmReportList.setObj_StationList(stationListForPMReport);
                    }

                    for (int j = 0; j < jsonArray_doneByList_PMReport.length(); j++) {

                        DoneByListForPMReport doneByListForPMReport = new DoneByListForPMReport(Parcel.obtain());
                        JSONObject row = jsonArray_doneByList_PMReport.getJSONObject(j);
                        doneByListForPMReport.setDoneBy_op_code(row.optString("OPCODE"));
                        doneByListForPMReport.setDoneBy_op_name(row.optString("OPNAME"));
                        doneByListForPMReport.setDoneBy_station_code(row.optString("STATIONCODE"));

                        pmReportDoneByArrayList.add(row.optString("OPNAME"));
                        pmReportDoneByCodeArrayList.add(row.optString("OPCODE"));

                        pmReportList.setObj_doneByList(doneByListForPMReport);
                    }

                    obj_StationList = pmReportList.getObj_StationList();
                    obj_DoneByList = pmReportList.getObj_doneByList();
                } else {
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        array_Station = new String[pmReportStationArrayList.size()];
        array_Station = pmReportStationArrayList.toArray(array_Station);

        array_DoneBy = new String[pmReportDoneByCodeArrayList.size()];
        array_DoneBy = pmReportDoneByArrayList.toArray(array_DoneBy);

    }

    protected void showSelectStationCodeDialog() {
        boolean[] checkedStations = new boolean[array_Station.length];
        int count = array_Station.length;

        for (int i = 0; i < count; i++) {
            checkedStations[i] = selectedStations.contains(array_Station[i]);
        }

        DialogInterface.OnMultiChoiceClickListener stationsDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedStations.add(array_Station[which]);
                    selectedStationsId.add((CharSequence) pmReportList.getObj_StationList().get(which).getStation_code());
                } else {
                    selectedPoliceStations.remove(array_Station[which]);
                    selectedPoliceStationsId.remove((CharSequence) pmReportList.getObj_StationList().get(which).getStation_code());
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Stations");
        builder.setMultiChoiceItems(array_Station, checkedStations, stationsDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedStations();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_StationCodeValue.setText("Select Stations");
                stationString = "";
                selectedStations.clear();
                selectedStationsId.clear();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedStations() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for (CharSequence station : selectedStations) {
            stringBuilder.append(station + ",");
        }

        for (CharSequence station : selectedStationsId) {
            //stringBuilderId.append("\'"+station + "\',");
            stringBuilderId.append(station + ",");
        }

        if (selectedStations.size() == 0) {
            tv_StationCodeValue.setText("Select Stations");
            stationString = "";
        } else {
            tv_StationCodeValue.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
            stationString = stringBuilderId.toString().substring(0, stringBuilderId.toString().length() - 1);
        }
    }


    protected void showSelectDoneByDialog() {
        boolean[] checkedDoneBy = new boolean[array_DoneBy.length];
        int count = array_DoneBy.length;

        for (int i = 0; i < count; i++) {
            checkedDoneBy[i] = selectedDoneBy.contains(array_DoneBy[i]);
        }

        DialogInterface.OnMultiChoiceClickListener doneByDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedDoneBy.add(array_DoneBy[which]);
                    selectedDoneById.add((CharSequence) pmReportList.getObj_doneByList().get(which).getDoneBy_op_code());
                } else {
                    selectedDoneBy.remove(array_DoneBy[which]);
                    selectedDoneById.remove((CharSequence) pmReportList.getObj_doneByList().get(which).getDoneBy_op_code());
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select");
        builder.setMultiChoiceItems(array_DoneBy, checkedDoneBy, doneByDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedDoneBy();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_doneByValue.setText("Select");
                doneByString = "";
                selectedDoneBy.clear();
                selectedDoneById.clear();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedDoneBy() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for (CharSequence doneBy : selectedDoneBy) {
            stringBuilder.append(doneBy + ",");
        }

        for (CharSequence doneBy : selectedDoneById) {
            //stringBuilderId.append("\'"+doneBy + "\',");
            stringBuilderId.append(doneBy + ",");
        }

        if (selectedDoneBy.size() == 0) {
            tv_doneByValue.setText("Select");
            doneByString = "";
        } else {
            tv_doneByValue.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
            doneByString = stringBuilderId.toString().substring(0, stringBuilderId.toString().length() - 1);
        }
    }


    private void fetchPMReportSearchResult() {

        String inquestNo = et_inquestNoValue.getText().toString().trim().replace("Inquest No", "");
        String inquestYr = et_inquestYr.getText().toString().trim().replace("Inquest Year", "");
        String pmNo = et_pmNo.getText().toString().trim().replace("PM No", "");
        String pmYr = et_pmYear.getText().toString().trim().replace("PM Year", "");
        String name = et_nameDeceased.getText().toString().trim().replace("Enter Name Deceased", "");
        String gender = et_gender.getText().toString().trim().replace("Enter Gender", "");
        String firNo = et_firNo.getText().toString().trim().replace("FIR No", "");
        String firYr = et_firYear.getText().toString().trim().replace("FIR Year", "");
        String ageVal = sp_AgeVal.getSelectedItem().toString().trim().replace("Select Age Range", "");
        String ageIndicator = sp_AgeIndicator.getSelectedItem().toString().trim().replace("Select Age Indicator", "");

        if (!policeStationString.equalsIgnoreCase("") || !inquestNo.equalsIgnoreCase("") || !inquestYr.equalsIgnoreCase("") ||
                !pmNo.equalsIgnoreCase("") || !pmYr.equalsIgnoreCase("") || !name.equalsIgnoreCase("") || !gender.equalsIgnoreCase("") ||
                !firNo.equalsIgnoreCase("") || !firYr.equalsIgnoreCase("") || !ageVal.equalsIgnoreCase("") || !ageIndicator.equalsIgnoreCase("") ||
                !policeStationStringForFIR.equalsIgnoreCase("") || !stationString.equalsIgnoreCase("") || !doneByString.equalsIgnoreCase("")) {

            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_PMREPORT_SEARCH);
            taskManager.setPMReportSearch(true);

            String[] keys = {"ps", "inquest_no", "pm_no", "inquest_yr", "pm_yr", "name_deceased", "sex", "age_val", "age_indicator", "station_code", "done_by", "fir_no", "fir_ps", "fir_yr", "pageno", "user_id","own_jurisdiction"};
            String[] values = {policeStationString.trim(), inquestNo.trim(), pmNo.trim(), inquestYr.trim(), pmYr.trim(), name.trim(), gender.trim(), ageVal.trim(), ageIndicator.trim(), stationString.trim(), doneByString.trim(), firNo.trim(), policeStationStringForFIR.trim(), firYr.trim(), "1",userId,ownjurisdictionFlag};
            taskManager.doStartTask(keys, values, true,true);
        } else {
            showAlertDialog(getActivity(), "Alert !!!", "Please provide atleast one value for search", false);
        }
    }

    public void parsePMReportSearchResultResponse(String result, String[] keys, String[] values) {
        //System.out.println("parsePMReportSearchResultResponse: " + result);

        if (result != null && !result.equals("")) {
            try {
                JSONObject jobj = new JSONObject(result);
                if (jobj.optString("status").equalsIgnoreCase("1")) {
                    this.keys = keys;
                    this.values = values;
                    pageno = jobj.opt("page").toString();
                    totalResult = jobj.opt("count").toString();

                    JSONArray resultArray = jobj.getJSONArray("result");
                    parsePMReportSearchResponse(resultArray);
                }
                else{
                    showAlertDialog(getActivity(),Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL,false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }


    private void parsePMReportSearchResponse(JSONArray resultArray){

        pmReportSearchDetailsList = new ArrayList<PMReportSearchDetails>();
        for(int i = 0 ; i<resultArray.length(); i++){

            JSONObject jsonObj = null;
            try{
                jsonObj = resultArray.getJSONObject(i);
                PMReportSearchDetails pmReportSearchDetails = new PMReportSearchDetails(Parcel.obtain());

                if(!jsonObj.optString("PSCODE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPsCode(jsonObj.optString("PSCODE"));
                }

                if(!jsonObj.optString("PSNAME").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPsName(jsonObj.optString("PSNAME"));
                }

                if(!jsonObj.optString("INQUESTNO").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setInquestNo(jsonObj.optString("INQUESTNO"));
                }

                if(!jsonObj.optString("INQUESTDT").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setInquestDt(jsonObj.optString("INQUESTDT"));
                }

                if(!jsonObj.optString("INQUESTYR").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setInquestYr(jsonObj.optString("INQUESTYR"));
                }

                if(!jsonObj.optString("PMNO").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPmNo(jsonObj.optString("PMNO"));
                }

                if(!jsonObj.optString("PMDT").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPmDt(jsonObj.optString("PMDT"));
                }

                if(!jsonObj.optString("PMYEAR").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPmYr(jsonObj.optString("PMYEAR"));
                }

                if(!jsonObj.optString("NAME_DECEASED").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setNameDeceased(jsonObj.optString("NAME_DECEASED"));
                }

                if(!jsonObj.optString("SEX").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setGender(jsonObj.optString("SEX"));
                }

                if(!jsonObj.optString("AGEVAL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setAgeVal(jsonObj.optString("AGEVAL"));
                }

                if(!jsonObj.optString("AGE_INDICATOR").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setAgeIndicator(jsonObj.optString("AGE_INDICATOR"));
                }

                if(!jsonObj.optString("DTARRVDEADHOUSE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setDeadHouse(jsonObj.optString("DTARRVDEADHOUSE"));
                }

                if(!jsonObj.optString("STATIONCODE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setStationCode(jsonObj.optString("STATIONCODE"));
                }

                if(!jsonObj.optString("STATION").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setStationName(jsonObj.optString("STATION"));
                }

                if(!jsonObj.optString("DONEBY").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setDoneBy(jsonObj.optString("DONEBY"));
                }

                if(!jsonObj.optString("FIRNO").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setFirNo(jsonObj.optString("FIRNO"));
                }

                if(!jsonObj.optString("FIRYEAR").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setFirYr(jsonObj.optString("FIRYEAR"));
                }

                if(!jsonObj.optString("FIRPSCODE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setFirPS(jsonObj.optString("FIRPSCODE"));
                }

                if(!jsonObj.optString("FIRDATE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setFirDate(jsonObj.optString("FIRDATE"));
                }

                if(!jsonObj.optString("OPNAME").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setOpName(jsonObj.optString("OPNAME"));
                }

                if(!jsonObj.optString("RELIGION").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setReligion(jsonObj.optString("RELIGION"));
                }

                if(!jsonObj.optString("WHENCE_BROUGHT").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setWhen_ce_brought(jsonObj.optString("WHENCE_BROUGHT"));
                }

                if(!jsonObj.optString("NAME_CONST_RELATIVES").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setName_const_relatives(jsonObj.optString("NAME_CONST_RELATIVES"));
                }

                if(!jsonObj.optString("DTDESPATCHDBR").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setDate_dispatch(jsonObj.optString("DTDESPATCHDBR"));
                }

                if(!jsonObj.optString("DTARRVDEADHOUSE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setDate_arrive(jsonObj.optString("DTARRVDEADHOUSE"));
                }

                if(!jsonObj.optString("DTEXAM").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setDate_exam(jsonObj.optString("DTEXAM"));
                }

                if(!jsonObj.optString("INFOBYPOLICE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setInfo_by_police(jsonObj.optString("INFOBYPOLICE"));
                }

                if(!jsonObj.optString("INDENT_BEF_MO").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setIndent_bef_no(jsonObj.optString("INDENT_BEF_MO"));
                }

                if(!jsonObj.optString("LENGTH_FT").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setLength_ft(jsonObj.optString("LENGTH_FT"));
                }

                if(!jsonObj.optString("LENGTH_INCH").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setLength_inch(jsonObj.optString("LENGTH_INCH"));
                }

                if(!jsonObj.optString("SUBJECT_CONDITION").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setSubject_condition(jsonObj.optString("SUBJECT_CONDITION"));
                }

                if(!jsonObj.optString("WOUNDS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setWounds(jsonObj.optString("WOUNDS"));
                }

                if(!jsonObj.optString("BRUISES").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setBruises(jsonObj.optString("BRUISES"));
                }

                if(!jsonObj.optString("LIGATURE_MARKS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setLigature_marks(jsonObj.optString("LIGATURE_MARKS"));
                }

                if(!jsonObj.optString("SCALP_SKULL_VERTEBRAE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setSculp_skull_vertebrae(jsonObj.optString("SCALP_SKULL_VERTEBRAE"));
                }

                if(!jsonObj.optString("MEMBRANE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMembrane(jsonObj.optString("MEMBRANE"));
                }

                if(!jsonObj.optString("BRAIN_SPCORD").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setBrain_sp_cord(jsonObj.optString("BRAIN_SPCORD"));
                }

                if(!jsonObj.optString("WALLS_RIBS_CARTILAGE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setWalls_ribs_cartilage(jsonObj.optString("WALLS_RIBS_CARTILAGE"));
                }

                if(!jsonObj.optString("LARYNX_TRACHEA").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setLaryncs(jsonObj.optString("LARYNX_TRACHEA"));
                }

                if(!jsonObj.optString("RIGHT_LUNG").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setRight_lung(jsonObj.optString("RIGHT_LUNG"));
                }

                if(!jsonObj.optString("LEFT_LUNG").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setLeft_lung(jsonObj.optString("LEFT_LUNG"));
                }

                if(!jsonObj.optString("PERICARDIUM").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPericardium(jsonObj.optString("PERICARDIUM"));
                }

                if(!jsonObj.optString("HEART").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setHeart(jsonObj.optString("HEART"));
                }

                if(!jsonObj.optString("VESSELS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setVessels(jsonObj.optString("VESSELS"));
                }

                if(!jsonObj.optString("PERITONEUM").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPeritoneum(jsonObj.optString("PERITONEUM"));
                }

                if(!jsonObj.optString("MOUTH_PHARYNX_ESOPHAGUS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setEsophagus(jsonObj.optString("MOUTH_PHARYNX_ESOPHAGUS"));
                }

                if(!jsonObj.optString("STOMACH").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setStomach(jsonObj.optString("STOMACH"));
                }

                if(!jsonObj.optString("SMALL_INTESTINE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setSmall_intestine(jsonObj.optString("SMALL_INTESTINE"));
                }

                if(!jsonObj.optString("LARGE_INTESTINE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setLarge_intestine(jsonObj.optString("LARGE_INTESTINE"));
                }

                if(!jsonObj.optString("LIVER").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setLiver(jsonObj.optString("LIVER"));
                }

                if(!jsonObj.optString("SPLEEN").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setSpleen(jsonObj.optString("SPLEEN"));
                }

                if(!jsonObj.optString("KIDNEYS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setKidneys(jsonObj.optString("KIDNEYS"));
                }

                if(!jsonObj.optString("BLADDER").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setBladder(jsonObj.optString("BLADDER"));
                }

                if(!jsonObj.optString("GENERATION_EXTERNAL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setGeneration_external(jsonObj.optString("GENERATION_EXTERNAL"));
                }

                if(!jsonObj.optString("GENERATION_INTERNAL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setGeneration_internal(jsonObj.optString("GENERATION_INTERNAL"));
                }

                if(!jsonObj.optString("MBJ_INJURY").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMbj_injury(jsonObj.optString("MBJ_INJURY"));
                }

                if(!jsonObj.optString("MBJ_DISEASE_DEFORMITY").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMbj_disease_deformity(jsonObj.optString("MBJ_DISEASE_DEFORMITY"));
                }

                if(!jsonObj.optString("MBJ_FRACTURE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMbj_fracture(jsonObj.optString("MBJ_FRACTURE"));
                }

                if(!jsonObj.optString("MBJ_DISLOCATION").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMbj_delocation(jsonObj.optString("MBJ_DISLOCATION"));
                }

                if(!jsonObj.optString("ADDITIONAL_REMARKS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setAdditional_remarks(jsonObj.optString("ADDITIONAL_REMARKS"));
                }

                if(!jsonObj.optString("OPINION_MO").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setOption_mo(jsonObj.optString("OPINION_MO"));
                }

                if(!jsonObj.optString("OPINION_CS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setOption_cs(jsonObj.optString("OPINION_CS"));
                }

                if(!jsonObj.optString("MODIUSER").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setModi_user(jsonObj.optString("MODIUSER"));
                }

                if(!jsonObj.optString("MODIDT").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setModi_dt(jsonObj.optString("MODIDT"));
                }

                if(!jsonObj.optString("SUBMITTED").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setSubmitted(jsonObj.optString("SUBMITTED"));
                }

                if(!jsonObj.optString("MP_VISCERAE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_viscerae(jsonObj.optString("MP_VISCERAE"));
                }

                if(!jsonObj.optString("MP_BLOOD").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_blood(jsonObj.optString("MP_BLOOD"));
                }

                if(!jsonObj.optString("MP_SCALP_HAIR").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_scalp_hair(jsonObj.optString("MP_SCALP_HAIR"));
                }

                if(!jsonObj.optString("MP_NAIL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_nails(jsonObj.optString("MP_NAIL"));
                }

                if(!jsonObj.optString("MP_APPARELS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_apparels(jsonObj.optString("MP_APPARELS"));
                }

                if(!jsonObj.optString("MP_LIGATURE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_ligature(jsonObj.optString("MP_LIGATURE"));
                }

                if(!jsonObj.optString("MP_FOREIGN").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_foreign(jsonObj.optString("MP_FOREIGN"));
                }

                if(!jsonObj.optString("MP_SKIN").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_skin(jsonObj.optString("MP_SKIN"));
                }

                if(!jsonObj.optString("MP_VAGINAL_ANAL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_vaginal_anal(jsonObj.optString("MP_VAGINAL_ANAL"));
                }

                if(!jsonObj.optString("MP_OTHERS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_others(jsonObj.optString("MP_OTHERS"));
                }

                if(!jsonObj.optString("MP_OTHERS_DETAIL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_other_details(jsonObj.optString("MP_OTHERS_DETAIL"));
                }

                if(!jsonObj.optString("SENT_ON").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setSent_on(jsonObj.optString("SENT_ON"));
                }

                if(!jsonObj.optString("STN_BILL_NO").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setStation_billNo(jsonObj.optString("STN_BILL_NO"));
                }

                if(!jsonObj.optString("STN_BILL_DT").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setStation_billDt(jsonObj.optString("STN_BILL_DT"));
                }

                if(!jsonObj.optString("RELEASED").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setReleased(jsonObj.optString("RELEASED"));
                }

                if(!jsonObj.optString("NEFTDONE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setNeft_done(jsonObj.optString("NEFTDONE"));
                }

                if(!jsonObj.optString("NEFTDATE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setNeft_dt(jsonObj.optString("NEFTDATE"));
                }

                if(!jsonObj.optString("REFDBY").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setRef_by(jsonObj.optString("REFDBY"));
                }

                if(!jsonObj.optString("REFNO").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setRef_no(jsonObj.optString("REFNO"));
                }

                if(!jsonObj.optString("REFDT").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setRef_date(jsonObj.optString("REFDT"));
                }

                if(!jsonObj.optString("REMARKS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setRemarks(jsonObj.optString("REMARKS"));
                }

                if(!jsonObj.optString("TRNID").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setTranId(jsonObj.optString("TRNID"));
                }

                if(!jsonObj.optString("FILESIZE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setFile_size(jsonObj.optString("FILESIZE"));
                }

                if(!jsonObj.optString("MIMETYPE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMime_type(jsonObj.optString("MIMETYPE"));
                }

                if(!jsonObj.optString("FILENAME").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setFile_name(jsonObj.optString("FILENAME"));
                }

                if(!jsonObj.optString("PDF_URL").equalsIgnoreCase("null") ){
                    pmReportSearchDetails.setPdf_url(jsonObj.optString("PDF_URL"));
                }

                if(!jsonObj.optString("CRIMINAL_PDF_URL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setCriminal_pdf_url(jsonObj.optString("CRIMINAL_PDF_URL"));
                }


                pmReportSearchDetailsList.add(pmReportSearchDetails);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Intent intent = new Intent(getActivity(), PMReportSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putExtra("PM_REPORT_SEARCH_LIST", (ArrayList<? extends Parcelable>) pmReportSearchDetailsList);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Tb_search_type:
                if(!search_own_global_flag){
                    toggleButtonSearchType.setImageResource(R.drawable.toggle_right);
                    ownjurisdictionFlag="0";
                    search_own_global_flag=true;
                }
                else{
                    toggleButtonSearchType.setImageResource(R.drawable.toggle_left);
                    ownjurisdictionFlag="1";
                    search_own_global_flag=false;
                }

                break;
        }
    }
}
