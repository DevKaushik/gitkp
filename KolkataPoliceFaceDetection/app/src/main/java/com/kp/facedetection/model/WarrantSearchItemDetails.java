package com.kp.facedetection.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-Asset-131 on 29-07-2016.
 */
public class WarrantSearchItemDetails implements Serializable {

    private String ps="";
    private String sl_no="";
    private String wa_slno="";
    private String yr="";
    private String warrant_no="";
    private String case_ps="";
    private String case_police_station="";
    private String case_no="";
    private String ps_name="";
    private String fir_yr="";
    private String ps_recv_date="";
    private String under_sections="";
    private String issue_court="";
    private String process_no="";
    private String warrant_type="";
    private String returnable_date_to_court="";
    private String io_name="";
    private String remarks="";
    private String warrant_status="";
    private String action_date="";
    private String color_status="";
    private String case_date="";

    public String getCase_date() {
        return case_date;
    }

    public void setCase_date(String case_date) {
        this.case_date = case_date;
    }

    public String getColor_status() {
        return color_status;
    }

    public void setColor_status(String color_status) {
        this.color_status = color_status;
    }

    List<FIRWarranteesDetails> firWarranteesDetailsList = new ArrayList<FIRWarranteesDetails>();


    public List<FIRWarranteesDetails> getFirWarranteesDetailsList() {
        return firWarranteesDetailsList;
    }

    public void setFirWarranteesDetailsList(FIRWarranteesDetails obj) {
        firWarranteesDetailsList.add(obj);
    }

    public String getWa_slno() {
        return wa_slno;
    }

    public void setWa_slno(String wa_slno) {
        this.wa_slno = wa_slno;
    }

    public String getYr() {
        return yr;
    }

    public void setYr(String yr) {
        this.yr = yr;
    }

    public String getWarrant_no() {
        return warrant_no;
    }

    public void setWarrant_no(String warrant_no) {
        this.warrant_no = warrant_no;
    }

    public String getCase_ps() {
        return case_ps;
    }

    public void setCase_ps(String case_ps) {
        this.case_ps = case_ps;
    }

    public String getCase_police_station() {
        return case_police_station;
    }

    public void setCase_police_station(String case_police_station) {
        this.case_police_station = case_police_station;
    }

    public String getCase_no() {
        return case_no;
    }

    public void setCase_no(String case_no) {
        this.case_no = case_no;
    }

    public String getPs_name() {
        return ps_name;
    }

    public void setPs_name(String ps_name) {
        this.ps_name = ps_name;
    }

    public String getFir_yr() {
        return fir_yr;
    }

    public void setFir_yr(String fir_yr) {
        this.fir_yr = fir_yr;
    }

    public String getPs_recv_date() {
        return ps_recv_date;
    }

    public void setPs_recv_date(String ps_recv_date) {
        this.ps_recv_date = ps_recv_date;
    }

    public String getUnder_sections() {
        return under_sections;
    }

    public void setUnder_sections(String under_sections) {
        this.under_sections = under_sections;
    }

    public String getIssue_court() {
        return issue_court;
    }

    public void setIssue_court(String issue_court) {
        this.issue_court = issue_court;
    }

    public String getProcess_no() {
        return process_no;
    }

    public void setProcess_no(String process_no) {
        this.process_no = process_no;
    }

    public String getWarrant_type() {
        return warrant_type;
    }

    public void setWarrant_type(String warrant_type) {
        this.warrant_type = warrant_type;
    }

    public String getReturnable_date_to_court() {
        return returnable_date_to_court;
    }

    public void setReturnable_date_to_court(String returnable_date_to_court) {
        this.returnable_date_to_court = returnable_date_to_court;
    }

    public String getIo_name() {
        return io_name;
    }

    public void setIo_name(String io_name) {
        this.io_name = io_name;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getWarrant_status() {
        return warrant_status;
    }

    public void setWarrant_status(String warrant_status) {
        this.warrant_status = warrant_status;
    }

    public String getAction_date() {
        return action_date;
    }

    public void setAction_date(String action_date) {
        this.action_date = action_date;
    }

    public String getSl_no() {
        return sl_no;
    }

    public void setSl_no(String sl_no) {
        this.sl_no = sl_no;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }


}
