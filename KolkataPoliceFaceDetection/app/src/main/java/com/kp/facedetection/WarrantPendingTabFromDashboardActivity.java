package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.adapter.PSListForAllDashboardTabAdapter;
import com.kp.facedetection.interfaces.OnItemClickListenerForAllFIRView;
import com.kp.facedetection.model.CrimeReviewDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WarrantPendingTabFromDashboardActivity extends BaseActivity implements OnItemClickListenerForAllFIRView, View.OnClickListener {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String selectedDate;
    private String selected_div;
    private String selected_ps;

    private RecyclerView recycler_psList;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<CrimeReviewDetails> psList = new ArrayList<>();
    private PSListForAllDashboardTabAdapter psListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firtab_from_dashboard_layout);

        setToolBar();
        initViews();
    }

    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }


    private void initViews() {
        selectedDate = getIntent().getExtras().getString("SELECTED_DATE_WA_PENDING");
        selected_div = getIntent().getExtras().getString("SELECTED_DIV_WA_PENDING");
        selected_ps = getIntent().getExtras().getString("SELECTED_PS_WA_PENDING");

        recycler_psList = (RecyclerView) findViewById(R.id.recycler_psList);
        mLayoutManager = new LinearLayoutManager(this);
        recycler_psList.setLayoutManager(mLayoutManager);
        //recycler_psList.setItemAnimator(new DefaultItemAnimator());
        recycler_psList.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));

        allWarrantPendingPSListCall();

        psListAdapter = new PSListForAllDashboardTabAdapter(this, psList);
        recycler_psList.setAdapter(psListAdapter);
        psListAdapter.setClickListener(this);
    }


    private void allWarrantPendingPSListCall() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_WA_PENDING);
        taskManager.setWaPendingItemSearch(true);

        String[] keys = {"currDate","div","ps"};
        String[] values = {selectedDate.trim(), selected_div.trim(), selected_ps.trim()};
        taskManager.doStartTask(keys, values, true);
    }


    public void parseWarrantPendingPSResult(String response, String[] keys, String[] values) {
        //System.out.println("parseWarrantExecPSResult" + response);

        if (response != null && !response.equals("")) {

            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.opt("status").toString().equalsIgnoreCase("1")) {
                    JSONArray result = jobj.optJSONArray("result");
                    parsePSResponse(result);
                } else {
                    Utility.showAlertDialog(WarrantPendingTabFromDashboardActivity.this, " Search Error ", Constants.ERROR_MSG_DETAIL, false);
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(WarrantPendingTabFromDashboardActivity.this, " Search Error!!! ", Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }

    private void parsePSResponse(JSONArray resultArray) {

        psList.clear();
        for (int i = 0; i < resultArray.length(); i++) {
            CrimeReviewDetails psDetails = new CrimeReviewDetails(Parcel.obtain());
            JSONObject object = resultArray.optJSONObject(i);
            psDetails.setCode(object.optString("PSCODE"));
            psDetails.setCrimeCategory(object.optString("PSNAME"));
            psDetails.setCountOfCrime(object.optString("COUNT"));

            psList.add(psDetails);
        }
        psListAdapter.notifyDataSetChanged();
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View view, int position) {

        CrimeReviewDetails details = psList.get(position);
        Intent intent = new Intent(WarrantPendingTabFromDashboardActivity.this, WarrantPendingDetailsActivity.class);
        intent.putExtra("PS_CODE", details.getCode());
        intent.putExtra("SELECT_DATE", selectedDate);
        intent.putExtra("SELECT_DIV", selected_div);
        intent.putExtra("WA_POSITION",position);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }
}
