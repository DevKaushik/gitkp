package com.kp.facedetection;

import android.*;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.FingerprintHandler;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONObject;

import java.io.IOException;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import static javax.crypto.Cipher.getInstance;


/*import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;*/

public class FingerprintAuthActivity extends BaseActivity implements View.OnClickListener {
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";
    TextView chargesheetNotificationCount;
    String notificationCount = "";
    private TextView errortextView, desctextView;
    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "CRS";
    private Cipher cipher;
    private String passcode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerprint_auth);
        setToolBar();
        initViews();
    }

    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout) mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this, charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount = (TextView) mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount = Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }


    private void initViews() {

        errortextView = (TextView) findViewById(R.id.errorText);
        desctextView = (TextView) findViewById(R.id.desc);
        Constants.changefonts(errortextView, this, "Calibri Bold.ttf");
        Constants.changefonts(desctextView, this, "Calibri Bold.ttf");
       // Toast.makeText(this, "Build.VERSION" + Build.VERSION.SDK_INT, Toast.LENGTH_SHORT).show();
        if (Build.VERSION.SDK_INT > 22) {
            try {

                KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
                FingerprintManagerCompat  fingerprintManager = FingerprintManagerCompat.from(getApplicationContext());;

                if (!fingerprintManager.isHardwareDetected()) {
                    /*
                     * An error message will be displayed if the device does not contain the fingerprint hardware.
                     * However if you plan to implement a default authentication method,
                     * you can redirect the user to a default authentication activity from here.
                     * Example:
                     * Intent intent = new Intent(this, DefaultAuthenticationActivity.class);
                     * startActivity(intent);
                     */
                    showAlertDialog(this, "Error!!", "Your Device does not have a Fingerprint Sensor.", false);


                } else {
                    // Checks whether fingerprint permission is set on manifest
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                        errortextView.setText("Fingerprint authentication permission not enabled");
                    } else {
                        // Check whether at least one fingerprint is registered
                        if (!fingerprintManager.hasEnrolledFingerprints()) {
                            errortextView.setText("Register at least one fingerprint in Settings");
                        } else {
                            // Checks whether lock screen security is enabled or not
                            if (!keyguardManager.isKeyguardSecure()) {
                                errortextView.setText("Lock screen security not enabled in Settings");
                            } else {
                                generateKey();

                                if (cipherInit()) {
                                    FingerprintManagerCompat.CryptoObject cryptoObject = new FingerprintManagerCompat.CryptoObject(cipher);
                                    FingerprintHandler helper = new FingerprintHandler(this);
                                    helper.startAuth(fingerprintManager, cryptoObject);
                                }

                            }
                        }
                    }
                }
            } catch (Exception e) {
                showAlertDialog(this, "Error!!", "App does not support this device Fingerprint Sensor checking.", false);
            }
        } else {
            showAlertDialog(this, "Error!!", "App does not support this device Fingerprint Sensor checking.", false);

        }
    }




    @RequiresApi(api = Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }


        javax.crypto.KeyGenerator keyGenerator;
        try {
            keyGenerator = javax.crypto.KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }


        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }


        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (Exception e) {    //KeyPermanentlyInvalidatedException e
            return false;
        } /*catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }*/
    }


    @Override
    public void onClick(View v) {

    }

    public void updateNotificationCount(String notificationCount) {

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        } else {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }

    public void showAlertDialog(final Context context, String title, String message, final Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);


        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }
                        Log.e("Jay", "Rank:" + Utility.getUserInfo(context).getUserRank());
                        Log.e("Jay", "Divcode:" + Utility.getUserInfo(context).getUserDivisionCode());


                        if (!status) {
                            dialogInterface.dismiss();
                            callApiforUserPasscode();

                        }


                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    public void callApiforUserPasscode() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_SPECIAL_SERVICE_USER_PASSCODE);
        taskManager.setSpecilServicePasscode(true);

        String[] keys = {"user_id"};

        String[] values = {Utility.getUserInfo(this).getUserId()};
        taskManager.doStartTask(keys, values, true, true);

    }

    public void parseSpecialServicePasscodeResult(String result) {
        if (result != null && !result.equals("")) {
            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {
                    JSONObject res = jObj.optJSONObject("result");
                    passcode = res.optString("SPC_PASS");
                    createprocessDialog2(passcode);
                } else {
                    Toast.makeText(FingerprintAuthActivity.this, "Failed to generate passcode", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {

            }


        }
    }

    public void createprocessDialog2(final String passcode) {
        final LinearLayout layout = new LinearLayout(this);
        LayoutInflater.from(this).inflate(R.layout.descriptive_height_dialog_layout, layout);
        // Prefill the dialog content
        final EditText edText = (EditText) layout.findViewById(R.id.ed_height);
        edText.setText("");
        // Create the dialog (without showing)
        final AlertDialog d = new AlertDialog.Builder(this, R.style.CustomDialogTheme)
                .setPositiveButton("CHECK", null)
                .setTitle("Enter Passcode")
                .setView(layout)
                .create();
         // This is handy for re-adjusting the size of the dialog when a keyboard is shown
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        // MUST call show before using AlertDialog.getButton(), otherwise you'll get null returned
        d.show();
        // Override the button's on-click so it doesn't close by default
        d.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edText.getText().toString().trim().isEmpty()) {
                    Toast.makeText(FingerprintAuthActivity.this, "Please enter a value", Toast.LENGTH_SHORT).show();
                    return;
                } else if (edText.getText().toString().length() < 8) {
                    Toast.makeText(FingerprintAuthActivity.this, "Passcode should be atleast 8 digit", Toast.LENGTH_SHORT).show();
                    return;
                } else if (edText.getText().toString().trim().equalsIgnoreCase(passcode)) {
                    d.dismiss();
                    Intent it = new Intent(FingerprintAuthActivity.this, SpecialServiceList.class);
                    startActivity(it);

                } else {
                    //Toast.makeText(FingerprintAuthActivity.this, "Passcode should be atleast 8 digit", Toast.LENGTH_SHORT).show();
                    errortextView.setText("Wrong passcode .Go back and Try again ");
                    d.dismiss();
                }


            }

        });
    }

}
