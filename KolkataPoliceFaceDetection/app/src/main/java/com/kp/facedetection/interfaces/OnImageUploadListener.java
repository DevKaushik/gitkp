package com.kp.facedetection.interfaces;

/**
 * Created by DAT-165 on 26-05-2017.
 */

public interface OnImageUploadListener {
    void imageUpload(int position, String url);
}
