package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-Asset-131 on 18-03-2016.
 */
public class CrimeCategoryList implements Serializable {

    private String category = "";
    private String category_desc="";
    private String broad_category = "";
    private String IPC_SSl = "";
    private String sections="";
    private String verified = "";
    private String favourite = "";
    private  boolean selected = false;


    public String getFavourite() {
        return favourite;
    }

    public void setFavourite(String favourite) {
        this.favourite = favourite;
    }


    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory_desc() {
        return category_desc;
    }

    public void setCategory_desc(String category_desc) {
        this.category_desc = category_desc;
    }

    public String getBroad_category() {
        return broad_category;
    }

    public void setBroad_category(String broad_category) {
        this.broad_category = broad_category;
    }

    public String getIPC_SSl() {
        return IPC_SSl;
    }

    public void setIPC_SSl(String IPC_SSl) {
        this.IPC_SSl = IPC_SSl;
    }

    public String getSections() {
        return sections;
    }

    public void setSections(String sections) {
        this.sections = sections;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }
}
