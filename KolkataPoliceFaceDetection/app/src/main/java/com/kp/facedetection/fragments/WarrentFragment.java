package com.kp.facedetection.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.adapter.ExpandableListAdapterForWarrent;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by DAT-Asset-145 on 06-05-2016.
 */
public class WarrentFragment extends Fragment {

    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String WA_YEAR = "WA_YEAR";
    public static final String WASLNO = "WASLNO";
    public static final String PS_CODE = "PS_CODE";
    private String wasl_no="";
    private String wa_year="";
    private String ps_code="";
    private String details="";
    private String TAG="WarrentFragment";

   /* private TextView tv_details;

    private RelativeLayout relative_details;*/

    private ExpandableListView lvExp;
    private TextView tv_noData;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    ExpandableListAdapterForWarrent listAdapter;
    private int mPage;

    public static WarrentFragment newInstance(int page,String ps_code,String wa_year,String wasl_no ) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        args.putString(PS_CODE, ps_code);
        args.putString(WA_YEAR, wa_year);
        args.putString(WASLNO, wasl_no);
        WarrentFragment wfragment = new WarrentFragment();
        wfragment.setArguments(args);
        return wfragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        ps_code = getArguments().getString(PS_CODE).replace("PSCODE: ", "");
        wasl_no =  getArguments().getString(WASLNO).replace("WASLNO: ","");
        wa_year = getArguments().getString(WA_YEAR).replace("WA_YEAR: ","");
    }

    // Inflate the fragment layout we defined above for this fragment
    // Set the associated text for the title
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //View view = inflater.inflate(R.layout.fragment_details, container, false);
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

       /* tv_details = (TextView) view.findViewById(R.id.tv_detail);

        relative_details = (RelativeLayout)view.findViewById(R.id.relative_details);*/

        //Utility.changefonts(tv_noData, getActivity(), "Calibri.ttf");

        //getWarentDetails(ps_code, wa_year, wasl_no);


        tv_noData = (TextView) view.findViewById(R.id.tv_noData);
        lvExp = (ExpandableListView) view.findViewById(R.id.lvExp);

        //Utility.changefonts(tv_noData, getActivity(), "Calibri.ttf");

        tv_noData.setVisibility(View.GONE);
        lvExp.setVisibility(View.GONE);

      //  getModifiedWarrentDetails(ps_code, wa_year, wasl_no);

    }

    private void getWarentDetails(String ps_code, String wa_year, String wasl_no){

        this.ps_code = ps_code;
        this.wa_year = wa_year;
        this.wasl_no = wasl_no;

        System.out.println(TAG + " PSCODE: " + ps_code);
        System.out.println(TAG+" WA_YEAR: "+wa_year);
        System.out.println(TAG + " WASLNO: " + wasl_no);

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_DETAILS);
        taskManager.setCriminalDetailsWarrent(true);

        String[] keys = { "flag","pscode","wa_year","waslno" };
      //  String[] values = { "W","K2" ,"2012","44" };

        String[] values = { "W", ps_code.trim(),wa_year.trim(),wasl_no.trim() };

        taskManager.doStartTask(keys, values, true);
    }

    public void parseWarrentResultResponse(String response) {

        //System.out.println("parseWarrentResultResponse: " + response);

        /*if (response != null && !response.equals("")) {

            try {
                JSONObject jObj = new JSONObject(response);
                if(jObj.optString("status").equalsIgnoreCase("1")){

                    relative_details.setVisibility(View.VISIBLE);
                    tv_noData.setVisibility(View.GONE);

                    JSONArray result = jObj.getJSONArray("result");
                    JSONObject criminal_details = result.getJSONObject(0);
                    createTextResults(criminal_details);

                }
                else{
                   // Toast.makeText(getActivity(), "Sorry, no warrant details found", Toast.LENGTH_LONG).show();
                    relative_details.setVisibility(View.GONE);
                    tv_noData.setVisibility(View.VISIBLE);
                    tv_noData.setText("Sorry, no warrant details found");


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        else {
            Toast.makeText(getActivity(), "Some error has been encountered", Toast.LENGTH_LONG).show();
        }*/

    }

    private void createTextResults(JSONObject criminal_details) {

        System.out.println("OBJ: "+criminal_details);

        if (criminal_details.optString("SLNO") != null && !criminal_details.optString("SLNO").equalsIgnoreCase("null"))
            details = "SLNO : " + criminal_details.optString("SLNO");
        if (criminal_details.optString("SLDATE") != null && !criminal_details.optString("SLDATE").equalsIgnoreCase("null"))
            details = details + "\n\n"+ "SLDATE : " + criminal_details.optString("SLDATE");
        if (criminal_details.optString("WATYPE") != null && !criminal_details.optString("WATYPE").equalsIgnoreCase("null"))
            details = details + "\n\n"+"WATYPE : " + criminal_details.optString("WATYPE");
        if (criminal_details.optString("WANO") != null && !criminal_details.optString("WANO").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "WANO : " + criminal_details.optString("WANO");
        if (criminal_details.optString("WAISSUEDATE") != null && !criminal_details.optString("WAISSUEDATE").equalsIgnoreCase("null"))
            details = details + "\n\n"+"WAISSUEDATE : " + criminal_details.optString("WAISSUEDATE");

        if (criminal_details.optString("PS") != null && !criminal_details.optString("PS").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "PS : " + criminal_details.optString("PS");

        if (criminal_details.optString("CASENO") != null && !criminal_details.optString("CASENO").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "CASENO : " + criminal_details.optString("CASENO");

        if (criminal_details.optString("UNDER_SECTIONS") != null && !criminal_details.optString("UNDER_SECTIONS").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "UNDER_SECTIONS : " + criminal_details.optString("UNDER_SECTIONS");

        if (criminal_details.optString("ISSUE_COURT") != null && !criminal_details.optString("ISSUE_COURT").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "ISSUE_COURT : " + criminal_details.optString("ISSUE_COURT");

        if (criminal_details.optString("PS_RECV_DATE") != null && !criminal_details.optString("PS_RECV_DATE").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "PS_RECV_DATE : " + criminal_details.optString("PS_RECV_DATE");

        if (criminal_details.optString("OFFICER_TO_SERVE") != null && !criminal_details.optString("OFFICER_TO_SERVE").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "OFFICER_TO_SERVE : " + criminal_details.optString("OFFICER_TO_SERVE");

        if (criminal_details.optString("RETURNABLE_DATE_TO_COURT") != null && !criminal_details.optString("RETURNABLE_DATE_TO_COURT").equalsIgnoreCase("null"))
            details = details + "\n\n"+"RETURNABLE_DATE_TO_COURT : " + criminal_details.optString("RETURNABLE_DATE_TO_COURT");

        if (criminal_details.optString("DTRETURN_TO_COURT") != null && !criminal_details.optString("DTRETURN_TO_COURT").equalsIgnoreCase("null"))
            details = details + "\n\n"+"DTRETURN_TO_COURT : " + criminal_details.optString("DTRETURN_TO_COURT");

        if (criminal_details.optString("WACAT") != null && !criminal_details.optString("WACAT").equalsIgnoreCase("null"))
            details = details + "\n\n"+"WACAT : " + criminal_details.optString("WACAT");

        if (criminal_details.optString("WASUBTYPE") != null && !criminal_details.optString("WASUBTYPE").equalsIgnoreCase("null"))
            details = details + "\n\n"+"WASUBTYPE : " + criminal_details.optString("WASUBTYPE");

        if (criminal_details.optString("WA_STATUS") != null && !criminal_details.optString("WA_STATUS").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "WA_STATUS : " + criminal_details.optString("WA_STATUS");

        if (criminal_details.optString("ACTION_DATE") != null && !criminal_details.optString("ACTION_DATE").equalsIgnoreCase("null"))
            details = details + "\n\n"+"ACTION_DATE : " + criminal_details.optString("ACTION_DATE");

        if (criminal_details.optString("FIR_YR") != null && !criminal_details.optString("FIR_YR").equalsIgnoreCase("null"))
            details = details + "\n\n"+"FIR_YR : " + criminal_details.optString("FIR_YR");

        if (criminal_details.optString("PROCESS_NO") != null && !criminal_details.optString("PROCESS_NO").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PROCESS_NO : " + criminal_details.optString("PROCESS_NO");

        if (criminal_details.optString("CASEPS") != null && !criminal_details.optString("CASEPS").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "CASEPS : " + criminal_details.optString("CASEPS");

        if (criminal_details.optString("REMARKS") != null && !criminal_details.optString("REMARKS").equalsIgnoreCase("null"))
            details = details + "\n\n"+"REMARKS : " + criminal_details.optString("REMARKS");

        if (criminal_details.optString("WA_YEAR") != null && !criminal_details.optString("WA_YEAR").equalsIgnoreCase("null"))
            details = details + "\n\n"+"WA_YEAR : " + criminal_details.optString("WA_YEAR");

        if (criminal_details.optString("WA_SLNO") != null && !criminal_details.optString("WA_SLNO").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "WA_SLNO : " + criminal_details.optString("WA_SLNO");

        if (criminal_details.optString("NAME") != null && !criminal_details.optString("NAME").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "NAME : " + criminal_details.optString("NAME");

        if (criminal_details.optString("FATHER_NAME") != null && !criminal_details.optString("FATHER_NAME").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "FATHER_NAME : " + criminal_details.optString("FATHER_NAME");

        if (criminal_details.optString("ADDRESS") != null && !criminal_details.optString("ADDRESS").equalsIgnoreCase("null"))
            details = details + "\n\n"+"ADDRESS : " + criminal_details.optString("ADDRESS");

        if (criminal_details.optString("WASLNO") != null && !criminal_details.optString("WASLNO").equalsIgnoreCase("null"))
            details = details + "\n\n"+"WASLNO : " + criminal_details.optString("WASLNO");

       // tv_details.setText(details);

    }

    private void getModifiedWarrentDetails(String ps_code, String wa_year, String wasl_no){

        this.ps_code = ps_code;
        this.wa_year = wa_year;
        this.wasl_no = wasl_no;

        System.out.println(TAG + " PSCODE: " + ps_code);
        System.out.println(TAG+" WA_YEAR: "+wa_year);
        System.out.println(TAG + " WASLNO: " + wasl_no);

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_WARRENT_DETAILS);
        taskManager.setModifiedCriminalDetailsWarrent(true);

        String[] keys = {"pscode","wa_year","waslno" };
        //String[] values = {"K2" ,"2012","44" };

        String[] values = { ps_code.trim(),wa_year.trim(),wasl_no.trim() };

        taskManager.doStartTask(keys, values, true);
    }


    public void parseModifiedWarrentResultResponse(String response) {

        //System.out.println("parseModifiedWarrentResultResponse: " + response);

        if (response != null && !response.equals("")) {

            try {
                JSONObject jObj = new JSONObject(response);
                if(jObj.optString("status").equalsIgnoreCase("1")){

                    tv_noData.setVisibility(View.GONE);
                    lvExp.setVisibility(View.VISIBLE);

                    JSONObject result = jObj.getJSONObject("result");
                    parseJSONObject(result);


                }
                else{
                    tv_noData.setVisibility(View.VISIBLE);
                    lvExp.setVisibility(View.GONE);
                    tv_noData.setText("Sorry, no warrent details found");
                   // Utility.showToast(getActivity(), "Sorry, no warrent details found", "long");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        else {
            Utility.showToast(getActivity(), Constants.ERROR_MSG_TO_RELOAD, "long");
        }
    }

    private void parseJSONObject(JSONObject obj){

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

       if(obj.length()>0){

           listDataHeader.add("Warrent No: "+obj.optString("WA_SLNO"));

           Iterator iterator_obj = obj.keys();
           List<String> list_obj = new ArrayList<>();

           while(iterator_obj.hasNext()){
               String key = (String)iterator_obj.next();

               if(!obj.optString(key).equalsIgnoreCase("null")) {

                   String value = obj.optString(key);
                   list_obj.add(key.replace("_"," ") + "^" + value);
               }

           }

           listDataChild.put(listDataHeader.get(listDataHeader.indexOf("Warrent No: "+obj.optString("WA_SLNO"))), list_obj);

       }

        listAdapter = new ExpandableListAdapterForWarrent(getActivity(), listDataHeader, listDataChild);

        // setting list adapter
        lvExp.setAdapter(listAdapter);

    }

}
