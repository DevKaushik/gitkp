package com.kp.facedetection.fragments;

import android.content.Context;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.kp.facedetection.R;
import com.kp.facedetection.adapter.FIRDetailsListAdapter;
import com.kp.facedetection.interfaces.OnFIRDetailsEventItemListener;
import com.kp.facedetection.interfaces.OnFIRDetailsNameListener;
import com.kp.facedetection.model.CaseTransferDetails;
import com.kp.facedetection.model.ChargeSheetDetails;
import com.kp.facedetection.model.CriminalDetails;
import com.kp.facedetection.model.CriminalSearchPRStatus;
import com.kp.facedetection.model.FIRDetails;
import com.kp.facedetection.model.FIROtherAccusedDetail;
import com.kp.facedetection.model.FIRUnderSectionDetails;
import com.kp.facedetection.model.FIRWarrantDetails;
import com.kp.facedetection.model.FIRWarranteesDetails;
import com.kp.facedetection.model.FirLogListDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-Asset-131 on 06-07-2016.
 */
public class ModifiedFIRFragment extends Fragment implements OnFIRDetailsNameListener, OnFIRDetailsEventItemListener {

    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String FLAG = "FLAG";
    public static final String CRIME_NO = "CRIME_NO";
    public static final String CRIME_YR = "CRIME_YR";
    public static final String CASE_NO = "CASE_NO";
    public static final String CASE_YR = "CASE_YR";
    public static final String SL_NO = "SL_NO";
    public static final String PS_CODE = "PS_CODE";
    public static final String CRIMINAL_NAME = "CRIMINAL_NAME";
    public static final String PROV_CRM_NO = "PROV_CRM_NO";


    private int mPage;
    private String flag;
    private String crime_no = "";
    private String crime_year = "";
    private String case_no = "";
    private String case_year = "";
    private String sl_no = "";
    private String ps_code = "";
    private String criminal_name = "";
    private String prov_crm_no="";


    private ListView lv_fir;
    private TextView tv_SlNo;
    private TextView tv_FirName;
    private TextView tv_status;
    private TextView tv_eventLog;
    private TextView tv_noData;

    private LinearLayout linear_main;

    LinearLayout linear_EventDetails;
    View inflatedViewFIREvent;
    private PopupWindow popWindowForEventLog;

    private List<FIRDetails> firDetailsList = new ArrayList<FIRDetails>();
    FIRDetailsListAdapter firDetailsListAdapter;


    private String csHeaderString = "Chargesheet No. ";


    private ArrayList<CriminalDetails> otherAccusedList;

    private String userName = "";
    private String user_Id = "";

    private LinearLayout linear_loadCriminalDetails;

    public static ModifiedFIRFragment newInstance(int page, String flag, String crime_no, String crime_year, String case_no, String case_year, String ps_code, String sl_no, String criminal_name ,String prov_crm_no) {

        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        args.putString(FLAG, flag);
        args.putString(CRIME_NO, crime_no);
        args.putString(CRIME_YR, crime_year);
        args.putString(PS_CODE, ps_code);
        args.putString(CASE_NO, case_no);
        args.putString(CASE_YR, case_year);
        args.putString(SL_NO, sl_no);
        args.putString(CRIMINAL_NAME, criminal_name);
        args.putString(PROV_CRM_NO, prov_crm_no);
        ModifiedFIRFragment fragment = new ModifiedFIRFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        flag = getArguments().getString(FLAG);
        crime_no = getArguments().getString(CRIME_NO).replace("CRIMENO: ", "");
        crime_year = getArguments().getString(CRIME_YR).replace("CRIMEYEAR: ", "");
        ps_code = getArguments().getString(PS_CODE).replace("PSCODE: ", "");
        case_no = getArguments().getString(CASE_NO).replace("CASENO: ", "");
        case_year = getArguments().getString(CASE_YR).replace("CASEYR: ", "");
        sl_no = getArguments().getString(SL_NO).replace("SLNO: ", "");
        criminal_name = getArguments().getString(CRIMINAL_NAME);

        prov_crm_no =  getArguments().getString(PROV_CRM_NO);// getArguments().getString(PROV_CRM_NO).replace("Criminal No^","");
        if(prov_crm_no.contains("PROV_CRM_NO:")){
            prov_crm_no=prov_crm_no.replace("PROV_CRM_NO:","");
        }

        try {
            userName = Utility.getUserInfo(getActivity()).getName();
            user_Id = Utility.getUserInfo(getActivity()).getUserId();

        } catch (Exception e) {
            e.printStackTrace();
        }


        System.out.println("--------------------------FIR-----------------------------");
        System.out.println("Flag: " + flag);
        System.out.println("Crime No: " + crime_no);
        System.out.println("Crime Yr: " + crime_year);
        System.out.println("PS Code: " + ps_code);
        System.out.println("Case No: " + case_no);
        System.out.println("Case Yr: " + case_year);
        System.out.println("SL No: " + case_year);
        System.out.println("Criminal Name: " + criminal_name);
        System.out.println("--------------------------FIR-----------------------------");


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fir_modified, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        lv_fir = (ListView) view.findViewById(R.id.lv_fir);
        tv_SlNo = (TextView) view.findViewById(R.id.tv_SlNo);
        tv_FirName = (TextView) view.findViewById(R.id.tv_FirName);
        tv_status = (TextView) view.findViewById(R.id.tv_status);
        tv_noData = (TextView) view.findViewById(R.id.tv_noData);
        tv_eventLog = (TextView) view.findViewById(R.id.tv_eventLog);
        linear_main = (LinearLayout) view.findViewById(R.id.linear_main);

        linear_main.setVisibility(View.GONE);
        tv_noData.setVisibility(View.GONE);


        tv_SlNo.setText("SL.No");
        tv_FirName.setText("FIR(S)");
        tv_status.setText("Status");
        tv_eventLog.setText("Event");

        tv_status.setTextSize(14);

        tv_FirName.setGravity(Gravity.CENTER);
        Constants.changefonts(tv_SlNo, getActivity(), "Calibri.ttf");
        Constants.changefonts(tv_FirName, getActivity(), "Calibri Bold.ttf");
        Constants.changefonts(tv_status, getActivity(), "Calibri.ttf");
        Constants.changefonts(tv_eventLog, getActivity(), "Calibri.ttf");
        Constants.changefonts(tv_noData, getActivity(), "Calibri.ttf");

        getFIRData();


    }

    private void getFIRData() {

        Log.e("FLAG", "GET FIR DATA CALLING " + flag);

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_MODIFIED_FIR_DETAILS);
        taskManager.setCriminalModifiedFIRDetails(true);

        if (flag.equalsIgnoreCase("OC")) {

            String[] keys = {"flag", "crimeno", "crimeyear"};
            String[] values = {flag.trim(), crime_no.trim(), crime_year.trim()};
            // String[] values = {"OC","1019", "1986" };

            taskManager.doStartTask(keys, values, true );
        } else if (flag.equalsIgnoreCase("OF")) {

            if (!crime_no.equalsIgnoreCase("") && !crime_year.equalsIgnoreCase("")) {

                String[] keys = {"flag", "crimeno", "crimeyear" ,"prov_crm_no"};
                String[] values = {flag, crime_no, crime_year , prov_crm_no};

                taskManager.doStartTask(keys, values, true );


            } else {

                String[] keys = {"flag", "caseno", "case_yr", "pscode", "slno" ,"prov_crm_no"};
                String[] values = {flag, case_no, case_year, ps_code, sl_no , prov_crm_no};

                taskManager.doStartTask(keys, values, true );

            }

        } else if (flag.equalsIgnoreCase("CF")) {

            if (!crime_no.equalsIgnoreCase("") && !crime_year.equalsIgnoreCase("")) {

                String[] keys = {"flag", "crimeno", "crimeyear"};
                String[] values = {flag, crime_no, crime_year};

                taskManager.doStartTask(keys, values, true);

            }

        } else {

            //new added

            if (!crime_no.equalsIgnoreCase("") && !crime_year.equalsIgnoreCase("")) {

                String[] keys = {"flag", "crimeno", "crimeyear" ,"prov_crm_no"};
                String[] values = {"OF", crime_no, crime_year , prov_crm_no};

                taskManager.doStartTask(keys, values, true );


            } else {

                String[] keys = {"flag", "caseno", "case_yr", "pscode", "slno" ,"prov_crm_no"};
                String[] values = {"OF", case_no, case_year, ps_code, sl_no ,prov_crm_no};

                taskManager.doStartTask(keys, values, true );

            }



           /* linear_main.setVisibility(View.GONE);
            tv_noData.setVisibility(View.VISIBLE);
            tv_noData.setText("Sorry, no fir details found");*/
        }

    }

    public void parseCriminalModifiedFIRDetailsResponse(String response) {

        System.out.println("ParseCriminalModifiedFIRDetailsResponse: " + response);

        if (response != null && !response.equals("")) {

            try {
                JSONObject jObj = new JSONObject(response);
                if (jObj.optString("status").equalsIgnoreCase("1")) {

                    linear_main.setVisibility(View.VISIBLE);
                    tv_noData.setVisibility(View.GONE);

                      if(flag.equalsIgnoreCase(""))
                      {
                          flag="OF";
                      }
                    //-------------------------------- For OC Flag -------------------------------------------//

                    if (flag.equalsIgnoreCase("OC")) {

                        JSONArray result_array = jObj.getJSONArray("result");
                        if (result_array.length() > 0) {
                            parseJSONArrayForOCFlag(result_array);

                        } else {
                            System.out.println("No data found in result_array");

                        }
                    }

                    //------------------------------ For OF Flag ----------------------------------------------//


                    else if (flag.equalsIgnoreCase("OF")) {

                        JSONArray result_array = jObj.getJSONArray("result");
                        if (result_array.length() > 0) {
                            parseJSonArrayForOFFlag(result_array);

                        } else {
                            System.out.println("No data found in result_array");

                        }
                    }
                    //---------------------------- For CF Flag -----------------------------------------------//

                    else if (flag.equalsIgnoreCase("CF")) {

                        JSONArray result_array = jObj.getJSONArray("result");
                        if (result_array.length() > 0) {
                            parseJSONArrayForCFFlag(result_array);

                        } else {
                            System.out.println("No data found in result_array");

                        }
                    }


                } else {
                    linear_main.setVisibility(View.GONE);
                    tv_noData.setVisibility(View.VISIBLE);
                    tv_noData.setText("Sorry, no fir details found");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                linear_main.setVisibility(View.GONE);
                tv_noData.setVisibility(View.VISIBLE);
                tv_noData.setText("Sorry, no fir details found");
            }

        } else {
            Utility.showToast(getActivity(), Constants.ERROR_MSG_TO_RELOAD, "short");
        }
    }


    private void parseJSONArrayForOCFlag(JSONArray result_array) {

        FIRDetails firDetails;
        FIROtherAccusedDetail firOtherAccusedDetail;
        CriminalSearchPRStatus criminalSearchPRStatus;
        firDetailsList.clear();


        for (int i = 0; i < result_array.length(); i++) {

            String alias = "";
            String underSection = "";
            String acts = " ";
            String category = "";
            String io = "";
            String ps_code = "";

            firDetails = new FIRDetails();

            try {
                JSONObject obj = result_array.getJSONObject(i);

                if (obj.optString("PSNAME_AS_CODE") != null && !obj.optString("PSNAME_AS_CODE").equalsIgnoreCase("") && !obj.optString("PSNAME_AS_CODE").equalsIgnoreCase("null")) {
                    firDetails.setPsNameAsCode(obj.optString("PSNAME_AS_CODE"));
                }

                if (obj.optString("CRIMENO") != null && !obj.optString("CRIMENO").equalsIgnoreCase("") && !obj.optString("CRIMENO").equalsIgnoreCase("null")) {
                    firDetails.setCrimeNo(obj.optString("CRIMENO"));
                }

                firDetails.setName(obj.optString("NAME"));

                if (obj.optString("ANAME1") != null && !obj.optString("ANAME1").equalsIgnoreCase("") && !obj.optString("ANAME1").equalsIgnoreCase("null")) {
                    alias = alias + obj.optString("ANAME1");
                }
                if (obj.optString("ANAME2") != null && !obj.optString("ANAME2").equalsIgnoreCase("") && !obj.optString("ANAME2").equalsIgnoreCase("null")) {
                    alias = alias + "/ " + obj.optString("ANAME2");
                }
                if (obj.optString("ANAME3") != null && !obj.optString("ANAME3").equalsIgnoreCase("") && !obj.optString("ANAME3").equalsIgnoreCase("null")) {
                    alias = alias + "/ " + obj.optString("ANAME3");
                }
                if (obj.optString("ANAME4") != null && !obj.optString("ANAME4").equalsIgnoreCase("") && !obj.optString("ANAME4").equalsIgnoreCase("null")) {
                    alias = alias + "/ " + obj.optString("ANAME4");
                }
                if (obj.optString("SEC11") != null && !obj.optString("SEC11").equalsIgnoreCase("") && !obj.optString("SEC11").equalsIgnoreCase("null")) {
                    underSection = obj.optString("SEC11");
                }
                if (obj.optString("SEC12") != null && !obj.optString("SEC12").equalsIgnoreCase("") && !obj.optString("SEC12").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC12");
                }
                if (obj.optString("SEC13") != null && !obj.optString("SEC13").equalsIgnoreCase("") && !obj.optString("SEC13").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC13");
                }
                if (obj.optString("SEC14") != null && !obj.optString("SEC14").equalsIgnoreCase("") && !obj.optString("SEC14").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC14");
                }
                if (obj.optString("SEC15") != null && !obj.optString("SEC15").equalsIgnoreCase("") && !obj.optString("SEC15").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC15");
                }
                if (obj.optString("SEC16") != null && !obj.optString("SEC16").equalsIgnoreCase("") && !obj.optString("SEC16").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC16");
                }
                if (obj.optString("SEC21") != null && !obj.optString("SEC21").equalsIgnoreCase("") && !obj.optString("SEC21").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC21");
                }
                if (obj.optString("SEC22") != null && !obj.optString("SEC22").equalsIgnoreCase("") && !obj.optString("SEC22").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC22");
                }
                if (obj.optString("SEC23") != null && !obj.optString("SEC23").equalsIgnoreCase("") && !obj.optString("SEC23").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC23");
                }
                if (obj.optString("SEC24") != null && !obj.optString("SEC24").equalsIgnoreCase("") && !obj.optString("SEC24").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC24");
                }
                if (obj.optString("SEC25") != null && !obj.optString("SEC25").equalsIgnoreCase("") && !obj.optString("SEC25").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC25");
                }
                if (obj.optString("SEC26") != null && !obj.optString("SEC26").equalsIgnoreCase("") && !obj.optString("SEC26").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC26");
                }
                if (obj.optString("SEC31") != null && !obj.optString("SEC31").equalsIgnoreCase("") && !obj.optString("SEC31").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC31");
                }
                if (obj.optString("SEC32") != null && !obj.optString("SEC32").equalsIgnoreCase("") && !obj.optString("SEC32").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC32");
                }
                if (obj.optString("SEC33") != null && !obj.optString("SEC33").equalsIgnoreCase("") && !obj.optString("SEC33").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC33");
                }
                if (obj.optString("SEC34") != null && !obj.optString("SEC34").equalsIgnoreCase("") && !obj.optString("SEC34").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC34");
                }
                if (obj.optString("SEC35") != null && !obj.optString("SEC35").equalsIgnoreCase("") && !obj.optString("SEC35").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC35");
                }
                if (obj.optString("SEC36") != null && !obj.optString("SEC36").equalsIgnoreCase("") && !obj.optString("SEC36").equalsIgnoreCase("null")) {
                    underSection = underSection + "/" + obj.optString("SEC36");
                }
                if (obj.optString("ACTS1") != null && !obj.optString("ACTS1").equalsIgnoreCase("") && !obj.optString("ACTS1").equalsIgnoreCase("null")) {
                    acts = acts + obj.optString("ACTS1");
                }
                if (obj.optString("ACTS2") != null && !obj.optString("ACTS2").equalsIgnoreCase("") && !obj.optString("ACTS2").equalsIgnoreCase("null")) {
                    acts = acts + "/" + obj.optString("ACTS2");
                }
                if (obj.optString("ACTS3") != null && !obj.optString("ACTS3").equalsIgnoreCase("") && !obj.optString("ACTS3").equalsIgnoreCase("null")) {
                    acts = acts + "/" + obj.optString("ACTS3");
                }
                if (obj.optString("ACTS4") != null && !obj.optString("ACTS4").equalsIgnoreCase("") && !obj.optString("ACTS4").equalsIgnoreCase("null")) {
                    acts = acts + "/" + obj.optString("ACTS4");
                }
                if (obj.optString("CLASS") != null && !obj.optString("CLASS").equalsIgnoreCase("") && !obj.optString("CLASS").equalsIgnoreCase("null")) {
                    category = obj.optString("CLASS");
                }
                if (obj.optString("IO") != null && !obj.optString("IO").equalsIgnoreCase("") && !obj.optString("IO").equalsIgnoreCase("null")) {
                    io = obj.optString("IO");
                }
                if (obj.optString("PSCODE") != null && !obj.optString("PSCODE").equalsIgnoreCase("") && !obj.optString("PSCODE").equalsIgnoreCase("null")) {
                    ps_code = obj.optString("PSCODE");
                }


                firDetails.setAlias(alias);
                firDetails.setUnder_section(underSection + acts);
                firDetails.setPs_code(ps_code);

                if (obj.optString("CASEREF") != null && !obj.optString("CASEREF").equalsIgnoreCase("") && !obj.optString("CASEREF").equalsIgnoreCase("null")) {
                    firDetails.setCase_ref(obj.optString("CASEREF"));
                }

                if (obj.optString("CASEDATE") != null && !obj.optString("CASEDATE").equalsIgnoreCase("") && !obj.optString("CASEDATE").equalsIgnoreCase("null")) {
                    firDetails.setCase_date(obj.optString("CASEDATE"));
                }

                firDetails.setCategory(category);
                firDetails.setIo(io);


                firDetails.setPdf_url(obj.optString("pdf_url"));
                firDetails.setPmReport_pdf_url(obj.optString("pmreport_pdf_url"));

                firDetails.setPoLat(obj.optString("PO_LAT"));
                firDetails.setPoLong(obj.optString("PO_LONG"));

                JSONArray other_accused_array = obj.getJSONArray("other_accused");

                for (int j = 0; j < other_accused_array.length(); j++) {

                    firOtherAccusedDetail = new FIROtherAccusedDetail();

                    String accusedAddress = "";
                    String accusedAliasName = "";


                    JSONObject obj_other_accused = other_accused_array.getJSONObject(j);

                    firOtherAccusedDetail.setName(obj_other_accused.optString("NAME"));
                    firOtherAccusedDetail.setFatherName(obj_other_accused.optString("F_NAME"));


                    if (obj_other_accused.optString("PSROAD_PSPLACE") != null && !obj_other_accused.optString("PSROAD_PSPLACE").equalsIgnoreCase("") && !obj_other_accused.optString("PSROAD_PSPLACE").equalsIgnoreCase("null")) {
                        accusedAddress = obj_other_accused.optString("PSROAD_PSPLACE");
                    }
                    if (obj_other_accused.optString("PRPS_KPJ") != null && !obj_other_accused.optString("PRPS_KPJ").equalsIgnoreCase("") && !obj_other_accused.optString("PRPS_KPJ").equalsIgnoreCase("null")) {
                        accusedAddress = accusedAddress + "," + obj_other_accused.optString("PRPS_KPJ");
                    }
                    if (obj_other_accused.optString("PRPS_OUT") != null && !obj_other_accused.optString("PRPS_OUT").equalsIgnoreCase("") && !obj_other_accused.optString("PRPS_OUT").equalsIgnoreCase("null")) {
                        accusedAddress = accusedAddress + "," + obj_other_accused.optString("PRPS_OUT");
                    }
                    if (obj_other_accused.optString("PSDIST") != null && !obj_other_accused.optString("PSDIST").equalsIgnoreCase("") && !obj_other_accused.optString("PSDIST").equalsIgnoreCase("null")) {
                        accusedAddress = accusedAddress + "," + obj_other_accused.optString("PSDIST");
                    }
                    if (obj_other_accused.optString("PSDIST_OUT") != null && !obj_other_accused.optString("PSDIST_OUT").equalsIgnoreCase("") && !obj_other_accused.optString("PSDIST_OUT").equalsIgnoreCase("null")) {
                        accusedAddress = accusedAddress + "," + obj_other_accused.optString("PSDIST_OUT");
                    }
                    if (obj_other_accused.optString("PSPIN") != null && !obj_other_accused.optString("PSPIN").equalsIgnoreCase("") && !obj_other_accused.optString("PSPIN").equalsIgnoreCase("null")) {
                        accusedAddress = accusedAddress + "," + obj_other_accused.optString("PSPIN");
                    }
                    if (obj_other_accused.optString("PSSTATE") != null && !obj_other_accused.optString("PSSTATE").equalsIgnoreCase("") && !obj_other_accused.optString("PSSTATE").equalsIgnoreCase("null")) {
                        accusedAddress = accusedAddress + "," + obj_other_accused.optString("PSSTATE");
                    }

                    firOtherAccusedDetail.setAddress(accusedAddress);


                    if (obj_other_accused.optString("ANAME1") != null && !obj_other_accused.optString("ANAME1").equalsIgnoreCase("") && !obj_other_accused.optString("ANAME1").equalsIgnoreCase("null")) {
                        accusedAliasName = "@ " + obj_other_accused.optString("ANAME1");
                    }
                    if (obj_other_accused.optString("ANAME2") != null && !obj_other_accused.optString("ANAME2").equalsIgnoreCase("") && !obj_other_accused.optString("ANAME2").equalsIgnoreCase("null")) {
                        accusedAliasName = accusedAliasName + "/" + obj_other_accused.optString("ANAME2");
                    }
                    if (obj_other_accused.optString("ANAME3") != null && !obj_other_accused.optString("ANAME3").equalsIgnoreCase("") && !obj_other_accused.optString("ANAME3").equalsIgnoreCase("null")) {
                        accusedAliasName = accusedAliasName + "/" + obj_other_accused.optString("ANAME3");
                    }
                    if (obj_other_accused.optString("ANAME4") != null && !obj_other_accused.optString("ANAME4").equalsIgnoreCase("") && !obj_other_accused.optString("ANAME4").equalsIgnoreCase("null")) {
                        accusedAliasName = accusedAliasName + "/" + obj_other_accused.optString("ANAME4");
                    }

                    firOtherAccusedDetail.setAlias(accusedAliasName);

                    otherAccusedList = Utility
                            .parseCrimiinalAssociateRecords(getActivity(), other_accused_array);
                    Log.e("TAG", "Other details:1 " + otherAccusedList.get(0).getNameAccused());

                    firOtherAccusedDetail.setOtherAccusedListItemData(otherAccusedList);

                    firDetails.setFirOtherAccusedDetailList(firOtherAccusedDetail);
                }

                JSONArray pr_status_array = obj.getJSONArray("pr_status");

                for (int l = 0; l < pr_status_array.length(); l++) {

                    criminalSearchPRStatus = new CriminalSearchPRStatus();

                    JSONObject obj_pr_status = pr_status_array.getJSONObject(l);

                    criminalSearchPRStatus.setEvenDetails(obj_pr_status.optString("EVENT_DETAILS"));
                    criminalSearchPRStatus.setDateRecorded(obj_pr_status.optString("DATE_RECORDED"));
                    criminalSearchPRStatus.setPrsStatus(obj_pr_status.optString("PRS_STATUS"));
                    criminalSearchPRStatus.setDateTill(obj_pr_status.optString("DATE_TILL"));
                    criminalSearchPRStatus.setEvent_SlNo(obj_pr_status.optString("EVENT_SLNO"));

                    firDetails.setPrsStatusList(criminalSearchPRStatus);

                }

                    /* Case Transfer data parse */

                JSONArray case_transfer_array = obj.getJSONArray("CASE_TRANSFER");

                for (int m = 0; m < case_transfer_array.length(); m++) {

                    JSONObject JSON_obj = null;
                    try {
                        JSON_obj = case_transfer_array.getJSONObject(m);
                        CaseTransferDetails caseTransferDetails = new CaseTransferDetails();

                        if (!JSON_obj.optString("FROM_PS").equalsIgnoreCase("null"))
                            caseTransferDetails.setFromPS(JSON_obj.optString("FROM_PS"));

                        if (!JSON_obj.optString("TO_PS").equalsIgnoreCase("null"))
                            caseTransferDetails.setToPS(JSON_obj.optString("TO_PS"));

                        if (!JSON_obj.optString("FROM_IONAME").equalsIgnoreCase("null"))
                            caseTransferDetails.setFromIoName(JSON_obj.optString("FROM_IONAME"));

                        if (!JSON_obj.optString("TO_IONAME").equalsIgnoreCase("null"))
                            caseTransferDetails.setToIoName(JSON_obj.optString("TO_IONAME"));

                        if (!JSON_obj.optString("TO_UNIT").equalsIgnoreCase("null"))
                            caseTransferDetails.setToUnit(JSON_obj.optString("TO_UNIT"));

                        if (!JSON_obj.optString("CASENO").equalsIgnoreCase("null"))
                            caseTransferDetails.setCaseNo(JSON_obj.optString("CASENO"));

                        if (!JSON_obj.optString("CASE_YR").equalsIgnoreCase("null"))
                            caseTransferDetails.setCaseYr(JSON_obj.optString("CASE_YR"));

                        if (!JSON_obj.optString("DT_TRANSFER").equalsIgnoreCase("null"))
                            caseTransferDetails.setTranferDt(JSON_obj.optString("DT_TRANSFER"));

                        if (!JSON_obj.optString("REMARKS").equalsIgnoreCase("null"))
                            caseTransferDetails.setRemarks(JSON_obj.optString("REMARKS"));

                        if (JSON_obj.optString("PSNAME") != null && !JSON_obj.optString("PSNAME").equalsIgnoreCase("null") && !JSON_obj.optString("PSNAME").equalsIgnoreCase(""))
                            caseTransferDetails.setPsName(JSON_obj.optString("PSNAME"));

                        firDetails.setCaseTransferList(caseTransferDetails);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                firDetailsList.add(firDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        firDetailsListAdapter = new FIRDetailsListAdapter(getActivity(), firDetailsList, "OC");
        lv_fir.setAdapter(firDetailsListAdapter);
        firDetailsListAdapter.notifyDataSetChanged();
        //lv_fir.setOnItemClickListener(this);
        firDetailsListAdapter.setFIRNameTapListener(this);
        firDetailsListAdapter.setFIREventLogTapListener(this);

    }





    private void parseJSonArrayForOFFlag(JSONArray result_array) {

        FIRDetails firDetails;
        FIROtherAccusedDetail firOtherAccusedDetail;
        CriminalSearchPRStatus criminalSearchPRStatus;

        firDetailsList.clear();

        for (int i = 0; i < result_array.length(); i++) {

            firDetails = new FIRDetails();

            try {

                String gde_details = "";
                String ps_name = "";

                JSONObject obj = result_array.getJSONObject(i);

                firDetails.setName(obj.optString("NAME_ACCUSED"));

                firDetails.setPs_code(obj.optString("PSCODE") + " PS");

                firDetails.setPdf_url(obj.optString("pdf_url"));
                firDetails.setPmReport_pdf_url(obj.optString("pmreport_pdf_url"));
                firDetails.setCase_ref(obj.optString("CASENO"));
                firDetails.setCase_date(obj.optString("CASEDATE"));
                firDetails.setIo(obj.optString("IO"));
                firDetails.setComplaint(obj.optString("COMPLAINANT"));
                firDetails.setComplaint_address(obj.optString("COMPLAINANT_ADDR"));
                firDetails.setAc_comment(obj.optString("AC_COMMENT"));
                firDetails.setDc_comment(obj.optString("DC_COMMENT"));
                firDetails.setOc_comment(obj.optString("OC_COMMENT"));

                firDetails.setPoLat(obj.optString("PO_LAT"));
                firDetails.setPoLong(obj.optString("PO_LONG"));


                if(obj.optString("PS") != null && !obj.optString("PS").equalsIgnoreCase("") && !obj.optString("PS").equalsIgnoreCase("null")){
                    firDetails.setPs(obj.optString("PS"));
                }

                if (obj.optString("CASENO") != null && !obj.optString("CASENO").equalsIgnoreCase("") && !obj.optString("CASENO").equalsIgnoreCase("null")) {
                    firDetails.setCaseNo(obj.optString("CASENO"));
                }

                if (obj.optString("FIR_YR") != null && !obj.optString("FIR_YR").equalsIgnoreCase("") && !obj.optString("FIR_YR").equalsIgnoreCase("null")) {
                    firDetails.setFirYear(obj.optString("FIR_YR"));
                }

                if (obj.optString("CASE_YR") != null && !obj.optString("CASE_YR").equalsIgnoreCase("") && !obj.optString("CASE_YR").equalsIgnoreCase("null")) {
                    firDetails.setCaseYr(obj.optString("CASE_YR"));
                }



                if (!obj.optString("FIR_STATUS").equalsIgnoreCase("null"))
                    firDetails.setFir_status(obj.optString("FIR_STATUS"));


                if (obj.optString("GDENO") != null && !obj.optString("GDENO").equalsIgnoreCase("") && !obj.optString("GDENO").equalsIgnoreCase("null")) {
                    gde_details = gde_details + obj.optString("GDENO");
                }

                if (obj.optString("GDEDATE") != null && !obj.optString("GDEDATE").equalsIgnoreCase("") && !obj.optString("GDEDATE").equalsIgnoreCase("null")) {
                    gde_details = gde_details + " dt. " + obj.optString("GDEDATE");
                }
                if (obj.optString("PSNAME") != null && !obj.optString("PSNAME").equalsIgnoreCase("") && !obj.optString("PSNAME").equalsIgnoreCase("null")) {
                    ps_name = obj.optString("PSNAME");
                }

                firDetails.setPs_name(ps_name);

                firDetails.setGd_details(gde_details);

                if (!obj.optString("BRIEF_FACT").equalsIgnoreCase("null"))
                    firDetails.setBrief_fact(obj.optString("BRIEF_FACT"));

                if (!obj.optString("GR_C_NO").equalsIgnoreCase("null"))
                    firDetails.setCgr_details("CGR No. " + obj.optString("GR_C_NO"));

                if (!obj.optString("COURTNAME").equalsIgnoreCase("null"))
                    firDetails.setCourt(obj.optString("COURTNAME"));


                JSONArray under_section_array = obj.getJSONArray("under_sections");

                for (int j = 0; j < under_section_array.length(); j++) {

                    FIRUnderSectionDetails firUnderSectionDetails = new FIRUnderSectionDetails();

                    JSONObject under_section_obj = under_section_array.getJSONObject(j);

                    firUnderSectionDetails.setUnder_section(under_section_obj.optString("UNDER_SECTION"));
                    firUnderSectionDetails.setCategory(under_section_obj.optString("CATEGORY"));

                    if (under_section_obj.optString("CASE_YR") != null && !under_section_obj.optString("CASE_YR").equalsIgnoreCase("") && !under_section_obj.optString("CASE_YR").equalsIgnoreCase("null")) {
                        firUnderSectionDetails.setCaseYr(under_section_obj.optString("CASE_YR"));
                    }

                    if (under_section_obj.optString("PS") != null && !under_section_obj.optString("PS").equalsIgnoreCase("") && !under_section_obj.optString("PS").equalsIgnoreCase("null")) {
                        firUnderSectionDetails.setPsCode(under_section_obj.optString("PS"));
                    }

                    if (under_section_obj.optString("CASENO") != null && !under_section_obj.optString("CASENO").equalsIgnoreCase("") && !under_section_obj.optString("CASENO").equalsIgnoreCase("null")) {
                        firUnderSectionDetails.setCaseNo(under_section_obj.optString("CASENO"));
                    }

                    firDetails.setFirUnderSectionDetailsList(firUnderSectionDetails);
                }


                JSONArray other_accused_array = obj.getJSONArray("other_accused");

                for (int k = 0; k < other_accused_array.length(); k++) {

                    firOtherAccusedDetail = new FIROtherAccusedDetail();
                    String accusedAliasName = "";

                    JSONObject obj_other_accused = other_accused_array.getJSONObject(k);

                    firOtherAccusedDetail.setName(obj_other_accused.optString("NAME_ACCUSED"));

                    if (obj_other_accused.optString("ALIAS1") != null && !obj_other_accused.optString("ALIAS1").equalsIgnoreCase("") && !obj_other_accused.optString("ALIAS1").equalsIgnoreCase("null")) {
                        accusedAliasName = "@ " + obj_other_accused.optString("ALIAS1");
                    }
                    if (obj_other_accused.optString("ALIAS2") != null && !obj_other_accused.optString("ALIAS2").equalsIgnoreCase("") && !obj_other_accused.optString("ALIAS2").equalsIgnoreCase("null")) {
                        accusedAliasName = accusedAliasName + "/" + obj_other_accused.optString("ALIAS2");
                    }
                    if (obj_other_accused.optString("ALIAS3") != null && !obj_other_accused.optString("ALIAS3").equalsIgnoreCase("") && !obj_other_accused.optString("ALIAS3").equalsIgnoreCase("null")) {
                        accusedAliasName = accusedAliasName + "/" + obj_other_accused.optString("ALIAS3");
                    }

                    firOtherAccusedDetail.setAlias(accusedAliasName);
                    firOtherAccusedDetail.setFatherName(obj_other_accused.optString("FATHER_ACCUSED"));
                    firOtherAccusedDetail.setAddress(obj_other_accused.optString("ADDR_ACCUSED"));

                    otherAccusedList = Utility
                            .parseCrimiinalAssociateRecords(getActivity(), other_accused_array);

                    Log.e("TAG", "Other details: " + otherAccusedList.get(0).getAliasName());
                    firOtherAccusedDetail.setOtherAccusedListItemData(otherAccusedList);

                    firDetails.setFirOtherAccusedDetailList(firOtherAccusedDetail);
                }

                JSONArray pr_status_array = obj.getJSONArray("pr_status");

                for (int l = 0; l < pr_status_array.length(); l++) {

                    criminalSearchPRStatus = new CriminalSearchPRStatus();

                    JSONObject obj_pr_status = pr_status_array.getJSONObject(l);

                    criminalSearchPRStatus.setEvenDetails(obj_pr_status.optString("EVENT_DETAILS"));
                    criminalSearchPRStatus.setDateRecorded(obj_pr_status.optString("DATE_RECORDED"));
                    criminalSearchPRStatus.setPrsStatus(obj_pr_status.optString("PRS_STATUS"));
                    criminalSearchPRStatus.setDateTill(obj_pr_status.optString("DATE_TILL"));
                    criminalSearchPRStatus.setEvent_SlNo(obj_pr_status.optString("EVENT_SLNO"));

                    firDetails.setPrsStatusList(criminalSearchPRStatus);

                }

                 /* Case Transfer data parse */

                JSONArray case_transfer_array = obj.getJSONArray("CASE_TRANSFER");

                for (int m = 0; m < case_transfer_array.length(); m++) {

                    JSONObject JSON_obj = null;
                    try {
                        JSON_obj = case_transfer_array.getJSONObject(m);
                        CaseTransferDetails caseTransferDetails = new CaseTransferDetails();

                        if (!JSON_obj.optString("FROM_PS").equalsIgnoreCase("null"))
                            caseTransferDetails.setFromPS(JSON_obj.optString("FROM_PS"));

                        if (!JSON_obj.optString("TO_PS").equalsIgnoreCase("null"))
                            caseTransferDetails.setToPS(JSON_obj.optString("TO_PS"));

                        if (!JSON_obj.optString("FROM_IONAME").equalsIgnoreCase("null"))
                            caseTransferDetails.setFromIoName(JSON_obj.optString("FROM_IONAME"));

                        if (!JSON_obj.optString("TO_IONAME").equalsIgnoreCase("null"))
                            caseTransferDetails.setToIoName(JSON_obj.optString("TO_IONAME"));

                        if (!JSON_obj.optString("TO_UNIT").equalsIgnoreCase("null"))
                            caseTransferDetails.setToUnit(JSON_obj.optString("TO_UNIT"));

                        if (!JSON_obj.optString("CASENO").equalsIgnoreCase("null"))
                            caseTransferDetails.setCaseNo(JSON_obj.optString("CASENO"));

                        if (!JSON_obj.optString("CASE_YR").equalsIgnoreCase("null"))
                            caseTransferDetails.setCaseYr(JSON_obj.optString("CASE_YR"));

                        if (!JSON_obj.optString("DT_TRANSFER").equalsIgnoreCase("null"))
                            caseTransferDetails.setTranferDt(JSON_obj.optString("DT_TRANSFER"));

                        if (!JSON_obj.optString("REMARKS").equalsIgnoreCase("null"))
                            caseTransferDetails.setRemarks(JSON_obj.optString("REMARKS"));

                        if (JSON_obj.optString("PSNAME") != null && !JSON_obj.optString("PSNAME").equalsIgnoreCase("null") && !JSON_obj.optString("PSNAME").equalsIgnoreCase(""))
                            caseTransferDetails.setPsName(JSON_obj.optString("PSNAME"));

                        firDetails.setCaseTransferList(caseTransferDetails);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


                JSONObject charge_sheet_obj = obj.getJSONObject("charge_sheet_details");

                String court_details = "";
                String charge_sheet_details = "";
                String actsSection = "";
                String frType = "";
                String suppltmentaryOriginal = "";
                String briefFacts = "";
                String ps_code = "";


                ChargeSheetDetails chargeSheetDetails = new ChargeSheetDetails();

                if (charge_sheet_obj.optString("COURT_OF") != null && !charge_sheet_obj.optString("COURT_OF").equalsIgnoreCase("") && !charge_sheet_obj.optString("COURT_OF").equalsIgnoreCase("null")) {
                    court_details = court_details + charge_sheet_obj.optString("COURT_OF");
                }

                if (charge_sheet_obj.optString("DISTRICT") != null && !charge_sheet_obj.optString("DISTRICT").equalsIgnoreCase("") && !charge_sheet_obj.optString("DISTRICT").equalsIgnoreCase("null")) {
                    court_details = court_details + " " + charge_sheet_obj.optString("DISTRICT");
                }

                chargeSheetDetails.setCourtOf(court_details);

                if (charge_sheet_obj.optString("FR_CHGNO") != null && !charge_sheet_obj.optString("FR_CHGNO").equalsIgnoreCase("") && !charge_sheet_obj.optString("FR_CHGNO").equalsIgnoreCase("null")) {
                    charge_sheet_details = "CS " + charge_sheet_obj.optString("FR_CHGNO");
                    csHeaderString = csHeaderString + " C-" + charge_sheet_obj.optString("FR_CHGNO");
                    chargeSheetDetails.setFir_chgNo(charge_sheet_obj.optString("FR_CHGNO"));
                }

                if (charge_sheet_obj.optString("FR_CHGDATE") != null && !charge_sheet_obj.optString("FR_CHGDATE").equalsIgnoreCase("") && !charge_sheet_obj.optString("FR_CHGDATE").equalsIgnoreCase("null")) {
                    charge_sheet_details = charge_sheet_details + " on " + charge_sheet_obj.optString("FR_CHGDATE");
                    csHeaderString = csHeaderString + " Dated " + charge_sheet_obj.optString("FR_CHGDATE");
                    chargeSheetDetails.setFir_date(charge_sheet_obj.optString("FR_CHGDATE"));
                }
                if (charge_sheet_obj.optString("FRTYPE") != null && !charge_sheet_obj.optString("FRTYPE").equalsIgnoreCase("") && !charge_sheet_obj.optString("FRTYPE").equalsIgnoreCase("null")) {
                    frType = charge_sheet_obj.optString("FRTYPE");
                }
                if (charge_sheet_obj.optString("SUPPLEMENTARY_ORIGINAL") != null && !charge_sheet_obj.optString("SUPPLEMENTARY_ORIGINAL").equalsIgnoreCase("") && !charge_sheet_obj.optString("SUPPLEMENTARY_ORIGINAL").equalsIgnoreCase("null")) {
                    suppltmentaryOriginal = charge_sheet_obj.optString("SUPPLEMENTARY_ORIGINAL");
                }
                if (charge_sheet_obj.optString("BRIEF_FACTS") != null && !charge_sheet_obj.optString("BRIEF_FACTS").equalsIgnoreCase("") && !charge_sheet_obj.optString("BRIEF_FACTS").equalsIgnoreCase("null")) {
                    briefFacts = charge_sheet_obj.optString("BRIEF_FACTS");
                }
                if (charge_sheet_obj.optString("ACT1") != null && !charge_sheet_obj.optString("ACT1").equalsIgnoreCase("") && !charge_sheet_obj.optString("ACT1").equalsIgnoreCase("null")) {
                    actsSection = charge_sheet_obj.optString("ACT1");
                }
                if (charge_sheet_obj.optString("ACT2") != null && !charge_sheet_obj.optString("ACT2").equalsIgnoreCase("") && !charge_sheet_obj.optString("ACT2").equalsIgnoreCase("null")) {
                    actsSection = actsSection + "/" + charge_sheet_obj.optString("ACT2");
                }
                if (charge_sheet_obj.optString("ACT3") != null && !charge_sheet_obj.optString("ACT3").equalsIgnoreCase("") && !charge_sheet_obj.optString("ACT3").equalsIgnoreCase("null")) {
                    actsSection = actsSection + "/" + charge_sheet_obj.optString("ACT3");
                }


                chargeSheetDetails.setCharge_sheet_details(charge_sheet_details);
                chargeSheetDetails.setCourtOf(court_details);
                chargeSheetDetails.setFrType(frType);
                chargeSheetDetails.setSuppltmentaryOriginal(suppltmentaryOriginal);
                chargeSheetDetails.setBriefFacts(briefFacts);
                chargeSheetDetails.setActsSection(actsSection);


                firDetails.setChargeSheetDetailsList(chargeSheetDetails);

                JSONObject warrant_obj = obj.optJSONObject("warrant_details");
                if (warrant_obj != null ) {

                    String slNo = "";
                    String slDate = "";
                    String waType = "";
                    String waNo = "";
                    String waIssueDate = "";
                    String ps = "";
                    String psName = "";
                    String caseNo = "";
                    String underSections = "";
                    String issueCourt = "";
                    String psRecvDate = "";
                    String officerToServe = "";
                    String returnableDateToCourt = "";
                    String dtReturnToCourt = "";
                    String waCat = "";
                    String waSubType = "";
                    String waStatus = "";
                    String actionDate = "";
                    String firYr = "";
                    String processNo = "";
                    String casePs = "";
                    String remarks = "";
                    String waYear = "";
                    String waSlNo = "";

                    FIRWarrantDetails firWarrantDetails = new FIRWarrantDetails();

                    if (warrant_obj.optString("SLNO") != null && !warrant_obj.optString("SLNO").equalsIgnoreCase("") && !warrant_obj.optString("SLNO").equalsIgnoreCase("null")) {
                        slNo = warrant_obj.optString("SLNO");
                    }
                    if (warrant_obj.optString("SLDATE") != null && !warrant_obj.optString("SLDATE").equalsIgnoreCase("") && !warrant_obj.optString("SLDATE").equalsIgnoreCase("null")) {
                        slDate = warrant_obj.optString("SLDATE");
                    }
                    if (warrant_obj.optString("WATYPE") != null && !warrant_obj.optString("WATYPE").equalsIgnoreCase("") && !warrant_obj.optString("WATYPE").equalsIgnoreCase("null")) {
                        waType = warrant_obj.optString("WATYPE");
                    }
                    if (warrant_obj.optString("WANO") != null && !warrant_obj.optString("WANO").equalsIgnoreCase("") && !warrant_obj.optString("WANO").equalsIgnoreCase("null")) {
                        waNo = warrant_obj.optString("WANO");
                    }
                    if (warrant_obj.optString("WAISSUEDATE") != null && !warrant_obj.optString("WAISSUEDATE").equalsIgnoreCase("") && !warrant_obj.optString("WAISSUEDATE").equalsIgnoreCase("null")) {
                        waIssueDate = warrant_obj.optString("WAISSUEDATE");
                    }
                    if (warrant_obj.optString("PS") != null && !warrant_obj.optString("PS").equalsIgnoreCase("") && !warrant_obj.optString("PS").equalsIgnoreCase("null")) {
                        ps = warrant_obj.optString("PS");
                    }
                    if (warrant_obj.optString("PSNAME") != null && !warrant_obj.optString("PSNAME").equalsIgnoreCase("") && !warrant_obj.optString("PSNAME").equalsIgnoreCase("null")) {
                        psName = warrant_obj.optString("PSNAME");
                    }
                    if (warrant_obj.optString("CASENO") != null && !warrant_obj.optString("CASENO").equalsIgnoreCase("") && !warrant_obj.optString("CASENO").equalsIgnoreCase("null")) {
                        caseNo = warrant_obj.optString("CASENO");
                    }
                    if (warrant_obj.optString("UNDER_SECTIONS") != null && !warrant_obj.optString("UNDER_SECTIONS").equalsIgnoreCase("") && !warrant_obj.optString("UNDER_SECTIONS").equalsIgnoreCase("null")) {
                        underSections = warrant_obj.optString("UNDER_SECTIONS");
                    }
                    if (warrant_obj.optString("ISSUE_COURT") != null && !warrant_obj.optString("ISSUE_COURT").equalsIgnoreCase("") && !warrant_obj.optString("ISSUE_COURT").equalsIgnoreCase("null")) {
                        issueCourt = warrant_obj.optString("ISSUE_COURT");
                    }
                    if (warrant_obj.optString("PS_RECV_DATE") != null && !warrant_obj.optString("PS_RECV_DATE").equalsIgnoreCase("") && !warrant_obj.optString("PS_RECV_DATE").equalsIgnoreCase("null")) {
                        psRecvDate = warrant_obj.optString("PS_RECV_DATE");
                    }
                    if (warrant_obj.optString("OFFICER_TO_SERVE") != null && !warrant_obj.optString("OFFICER_TO_SERVE").equalsIgnoreCase("") && !warrant_obj.optString("OFFICER_TO_SERVE").equalsIgnoreCase("null")) {
                        officerToServe = warrant_obj.optString("OFFICER_TO_SERVE");
                    }
                    if (warrant_obj.optString("RETURNABLE_DATE_TO_COURT") != null && !warrant_obj.optString("RETURNABLE_DATE_TO_COURT").equalsIgnoreCase("") && !warrant_obj.optString("RETURNABLE_DATE_TO_COURT").equalsIgnoreCase("null")) {
                        returnableDateToCourt = warrant_obj.optString("RETURNABLE_DATE_TO_COURT");
                    }
                    if (warrant_obj.optString("DTRETURN_TO_COURT") != null && !warrant_obj.optString("DTRETURN_TO_COURT").equalsIgnoreCase("") && !warrant_obj.optString("DTRETURN_TO_COURT").equalsIgnoreCase("null")) {
                        dtReturnToCourt = warrant_obj.optString("DTRETURN_TO_COURT");
                    }
                    if (warrant_obj.optString("WACAT") != null && !warrant_obj.optString("WACAT").equalsIgnoreCase("") && !warrant_obj.optString("WACAT").equalsIgnoreCase("null")) {
                        waCat = warrant_obj.optString("WACAT");
                    }
                    if (warrant_obj.optString("WASUBTYPE") != null && !warrant_obj.optString("WASUBTYPE").equalsIgnoreCase("") && !warrant_obj.optString("WASUBTYPE").equalsIgnoreCase("null")) {
                        waSubType = warrant_obj.optString("WASUBTYPE");
                    }
                    if (warrant_obj.optString("WA_STATUS") != null && !warrant_obj.optString("WA_STATUS").equalsIgnoreCase("") && !warrant_obj.optString("WA_STATUS").equalsIgnoreCase("null")) {
                        waStatus = warrant_obj.optString("WA_STATUS");
                    }
                    if (warrant_obj.optString("ACTION_DATE") != null && !warrant_obj.optString("ACTION_DATE").equalsIgnoreCase("") && !warrant_obj.optString("ACTION_DATE").equalsIgnoreCase("null")) {
                        actionDate = warrant_obj.optString("ACTION_DATE");
                    }
                    if (warrant_obj.optString("FIR_YR") != null && !warrant_obj.optString("FIR_YR").equalsIgnoreCase("") && !warrant_obj.optString("FIR_YR").equalsIgnoreCase("null")) {
                        firYr = warrant_obj.optString("FIR_YR");
                    }
                    if (warrant_obj.optString("PROCESS_NO") != null && !warrant_obj.optString("PROCESS_NO").equalsIgnoreCase("") && !warrant_obj.optString("PROCESS_NO").equalsIgnoreCase("null")) {
                        processNo = warrant_obj.optString("PROCESS_NO");
                    }
                    if (warrant_obj.optString("CASEPS") != null && !warrant_obj.optString("CASEPS").equalsIgnoreCase("") && !warrant_obj.optString("CASEPS").equalsIgnoreCase("null")) {
                        casePs = warrant_obj.optString("CASEPS");
                    }
                    if (warrant_obj.optString("REMARKS") != null && !warrant_obj.optString("REMARKS").equalsIgnoreCase("") && !warrant_obj.optString("REMARKS").equalsIgnoreCase("null")) {
                        remarks = warrant_obj.optString("REMARKS");
                    }
                    if (warrant_obj.optString("WA_YEAR") != null && !warrant_obj.optString("WA_YEAR").equalsIgnoreCase("") && !warrant_obj.optString("WA_YEAR").equalsIgnoreCase("null")) {
                        waYear = warrant_obj.optString("WA_YEAR");
                    }
                    if (warrant_obj.optString("WA_SLNO") != null && !warrant_obj.optString("WA_SLNO").equalsIgnoreCase("") && !warrant_obj.optString("WA_SLNO").equalsIgnoreCase("null")) {
                        waSlNo = warrant_obj.optString("WA_SLNO");
                    }

                    try {
                         if(warrant_obj.has("warrantees")){
                        JSONArray firWarranteesArray = warrant_obj.optJSONArray("warrantees");

                        if (firWarranteesArray.length() > 0) {

                            for (int j = 0; j < firWarranteesArray.length(); j++) {

                                FIRWarranteesDetails firWarranteesDetails = new FIRWarranteesDetails();

                                JSONObject warranteesObj = firWarranteesArray.getJSONObject(j);

                                if (warranteesObj.optString("SLNO") != null && !warranteesObj.optString("SLNO").equalsIgnoreCase("") && !warranteesObj.optString("SLNO").equalsIgnoreCase("null")) {
                                    firWarranteesDetails.setSl_no(warranteesObj.optString("SLNO"));
                                }
                                if (warranteesObj.optString("NAME") != null && !warranteesObj.optString("NAME").equalsIgnoreCase("") && !warranteesObj.optString("NAME").equalsIgnoreCase("null")) {
                                    firWarranteesDetails.setWarrantee_name(warranteesObj.optString("NAME"));
                                }
                                if (warranteesObj.optString("ADDRESS") != null && !warranteesObj.optString("ADDRESS").equalsIgnoreCase("") && !warranteesObj.optString("ADDRESS").equalsIgnoreCase("null")) {
                                    firWarranteesDetails.setWarrantee_address(warranteesObj.optString("ADDRESS"));
                                }

                                firWarrantDetails.setFirWarranteesDetailsList(firWarranteesDetails);

                            }

                        }
                        firDetails.setWarrantDetailsList(firWarrantDetails);
                    }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    firWarrantDetails.setSlNo(slNo);
                    firWarrantDetails.setSlDate(slDate);
                    firWarrantDetails.setWaType(waType);
                    firWarrantDetails.setWaNo(waNo);
                    firWarrantDetails.setWaIssueDate(waIssueDate);
                    firWarrantDetails.setPs(ps);
                    firWarrantDetails.setPs_name(psName);
                    firWarrantDetails.setCaseNo(caseNo);
                    firWarrantDetails.setUnderSections(underSections);
                    firWarrantDetails.setIssueCourt(issueCourt);
                    firWarrantDetails.setPsRecvDate(psRecvDate);
                    firWarrantDetails.setOfficerToServe(officerToServe);
                    firWarrantDetails.setReturnableDateToCourt(returnableDateToCourt);
                    firWarrantDetails.setDtReturnToCourt(dtReturnToCourt);
                    firWarrantDetails.setWaCat(waCat);
                    firWarrantDetails.setWaSubType(waSubType);
                    firWarrantDetails.setWaStatus(waStatus);
                    firWarrantDetails.setActionDate(actionDate);
                    firWarrantDetails.setFirYr(firYr);
                    firWarrantDetails.setProcessNo(processNo);
                    firWarrantDetails.setCasePs(casePs);
                    firWarrantDetails.setRemarks(remarks);
                    firWarrantDetails.setWaYear(waYear);
                    firWarrantDetails.setWaSlNo(waSlNo);


                }


                firDetailsList.add(firDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        firDetailsListAdapter = new FIRDetailsListAdapter(getActivity(), firDetailsList, "OF");
        lv_fir.setAdapter(firDetailsListAdapter);
        firDetailsListAdapter.notifyDataSetChanged();
        //lv_fir.setOnItemClickListener(this);
        firDetailsListAdapter.setFIRNameTapListener(this);
        firDetailsListAdapter.setFIREventLogTapListener(this);

    }


    private void parseJSONArrayForCFFlag(JSONArray result_array) {

        System.out.println("_________________________________________________________________________________");

        FIRDetails firDetails;
        FIROtherAccusedDetail firOtherAccusedDetail;
        CriminalSearchPRStatus criminalSearchPRStatus;
        firDetailsList.clear();


        for (int i = 0; i < result_array.length(); i++) {

            firDetails = new FIRDetails();

            try {

                JSONObject obj = result_array.getJSONObject(i);

                String key_flag = obj.optString("keyflag");

                System.out.println("KEY_Flag: " + key_flag);
                Log.e("TAG", "VALUE" + obj.toString());

                if (key_flag.equalsIgnoreCase("F")) {

                    // F ---> means OF Key
                    String gde_details = "";
                    String ps_name = "";

                    firDetails.setName(obj.optString("NAME_ACCUSED"));
                    firDetails.setPdf_url(obj.optString("pdf_url"));
                    firDetails.setPmReport_pdf_url(obj.optString("pmreport_pdf_url"));
                    firDetails.setPs_code(obj.optString("PSCODE") + " PS");
                    firDetails.setCase_ref(obj.optString("CASENO"));
                    firDetails.setCase_date(obj.optString("CASEDATE"));
                    firDetails.setIo(obj.optString("IO"));
                    firDetails.setComplaint(obj.optString("COMPLAINANT"));
                    firDetails.setComplaint_address(obj.optString("COMPLAINANT_ADDR"));
                    firDetails.setAc_comment(obj.optString("AC_COMMENT"));
                    firDetails.setDc_comment(obj.optString("DC_COMMENT"));
                    firDetails.setOc_comment(obj.optString("OC_COMMENT"));

                    firDetails.setPoLat(obj.optString("PO_LAT"));
                    firDetails.setPoLong(obj.optString("PO_LONG"));

                    if (obj.optString("PS") != null && !obj.optString("PS").equalsIgnoreCase("") && !obj.optString("PS").equalsIgnoreCase("null")) {
                        firDetails.setPs(obj.optString("PS"));
                    }

                    if (obj.optString("CASENO") != null && !obj.optString("CASENO").equalsIgnoreCase("") && !obj.optString("CASENO").equalsIgnoreCase("null")) {
                        firDetails.setCaseNo(obj.optString("CASENO"));
                    }

                    if (obj.optString("FIR_YR") != null && !obj.optString("FIR_YR").equalsIgnoreCase("") && !obj.optString("FIR_YR").equalsIgnoreCase("null")) {
                        firDetails.setFirYear(obj.optString("FIR_YR"));
                    }

                    if (obj.optString("CASE_YR") != null && !obj.optString("CASE_YR").equalsIgnoreCase("") && !obj.optString("CASE_YR").equalsIgnoreCase("null")) {
                        firDetails.setCaseYr(obj.optString("CASE_YR"));
                    }


                    if (!obj.optString("FIR_STATUS").equalsIgnoreCase("null"))
                        firDetails.setFir_status(obj.optString("FIR_STATUS"));


                    if (obj.optString("PSNAME") != null && !obj.optString("PSNAME").equalsIgnoreCase("") && !obj.optString("PSNAME").equalsIgnoreCase("null")) {
                        ps_name = obj.optString("PSNAME");
                    }
                    Log.e("Tag", "PS_NAME" + ps_name);
                    firDetails.setPs_name(ps_name);

                    if (obj.optString("GDENO") != null && !obj.optString("GDENO").equalsIgnoreCase("") && !obj.optString("GDENO").equalsIgnoreCase("null")) {
                        gde_details = gde_details + obj.optString("GDENO");
                    }

                    if (obj.optString("GDEDATE") != null && !obj.optString("GDEDATE").equalsIgnoreCase("") && !obj.optString("GDEDATE").equalsIgnoreCase("null")) {
                        gde_details = gde_details + " dt. " + obj.optString("GDEDATE");
                    }

                    firDetails.setGd_details(gde_details);

                    if (!obj.optString("BRIEF_FACT").equalsIgnoreCase("null"))
                        firDetails.setBrief_fact(obj.optString("BRIEF_FACT"));

                    if (!obj.optString("GR_C_NO").equalsIgnoreCase("null"))
                        firDetails.setCgr_details("CGR No. " + obj.optString("GR_C_NO"));

                    if (!obj.optString("COURTNAME").equalsIgnoreCase("null"))
                        firDetails.setCourt(obj.optString("COURTNAME"));

                    JSONArray under_section_array = obj.getJSONArray("under_sections");

                    for (int j = 0; j < under_section_array.length(); j++) {

                        FIRUnderSectionDetails firUnderSectionDetails = new FIRUnderSectionDetails();

                        JSONObject under_section_obj = under_section_array.getJSONObject(j);

                        firUnderSectionDetails.setUnder_section(under_section_obj.optString("UNDER_SECTION"));
                        firUnderSectionDetails.setCategory(under_section_obj.optString("CATEGORY"));
                        //firUnderSectionDetails.setCaseYr(under_section_obj.optString("CASE_YR"));

                        if (under_section_obj.optString("CASE_YR") != null && !under_section_obj.optString("CASE_YR").equalsIgnoreCase("") && !under_section_obj.optString("CASE_YR").equalsIgnoreCase("null")) {
                            firUnderSectionDetails.setCaseYr(under_section_obj.optString("CASE_YR"));
                        }

                        if (under_section_obj.optString("PS") != null && !under_section_obj.optString("PS").equalsIgnoreCase("") && !under_section_obj.optString("PS").equalsIgnoreCase("null")) {
                            firUnderSectionDetails.setPsCode(under_section_obj.optString("PS"));
                        }

                        if (under_section_obj.optString("CASENO") != null && !under_section_obj.optString("CASENO").equalsIgnoreCase("") && !under_section_obj.optString("CASENO").equalsIgnoreCase("null")) {
                            firUnderSectionDetails.setCaseNo(under_section_obj.optString("CASENO"));
                        }

                        firDetails.setFirUnderSectionDetailsList(firUnderSectionDetails);
                    }


                    JSONArray other_accused_array = obj.getJSONArray("other_accused");

                    for (int k = 0; k < other_accused_array.length(); k++) {

                        firOtherAccusedDetail = new FIROtherAccusedDetail();
                        String accusedAliasName = "";

                        JSONObject obj_other_accused = other_accused_array.getJSONObject(k);

                        firOtherAccusedDetail.setName(obj_other_accused.optString("NAME_ACCUSED"));

                        if (obj_other_accused.optString("ALIAS1") != null && !obj_other_accused.optString("ALIAS1").equalsIgnoreCase("") && !obj_other_accused.optString("ALIAS1").equalsIgnoreCase("null")) {
                            accusedAliasName = "@ " + obj_other_accused.optString("ALIAS1");
                        }
                        if (obj_other_accused.optString("ALIAS2") != null && !obj_other_accused.optString("ALIAS2").equalsIgnoreCase("") && !obj_other_accused.optString("ALIAS2").equalsIgnoreCase("null")) {
                            accusedAliasName = accusedAliasName + "/" + obj_other_accused.optString("ALIAS2");
                        }
                        if (obj_other_accused.optString("ALIAS3") != null && !obj_other_accused.optString("ALIAS3").equalsIgnoreCase("") && !obj_other_accused.optString("ALIAS3").equalsIgnoreCase("null")) {
                            accusedAliasName = accusedAliasName + "/" + obj_other_accused.optString("ALIAS3");
                        }

                        firOtherAccusedDetail.setAlias(accusedAliasName);
                        firOtherAccusedDetail.setFatherName(obj_other_accused.optString("FATHER_ACCUSED"));
                        firOtherAccusedDetail.setAddress(obj_other_accused.optString("ADDR_ACCUSED"));

                        otherAccusedList = Utility
                                .parseCrimiinalAssociateRecords(getActivity(), other_accused_array);


                        firOtherAccusedDetail.setOtherAccusedListItemData(otherAccusedList);

                        firDetails.setFirOtherAccusedDetailList(firOtherAccusedDetail);

                    }


                    JSONArray pr_status_array = obj.getJSONArray("pr_status");

                    for (int l = 0; l < pr_status_array.length(); l++) {

                        criminalSearchPRStatus = new CriminalSearchPRStatus();

                        JSONObject obj_pr_status = pr_status_array.getJSONObject(l);

                        criminalSearchPRStatus.setEvenDetails(obj_pr_status.optString("EVENT_DETAILS"));
                        criminalSearchPRStatus.setDateRecorded(obj_pr_status.optString("DATE_RECORDED"));
                        criminalSearchPRStatus.setPrsStatus(obj_pr_status.optString("PRS_STATUS"));
                        criminalSearchPRStatus.setDateTill(obj_pr_status.optString("DATE_TILL"));
                        criminalSearchPRStatus.setEvent_SlNo(obj_pr_status.optString("EVENT_SLNO"));

                        firDetails.setPrsStatusList(criminalSearchPRStatus);

                    }

                      /* Case Transfer data parse */

                    JSONArray case_transfer_array = obj.getJSONArray("CASE_TRANSFER");

                    for (int m = 0; m < case_transfer_array.length(); m++) {

                        JSONObject JSON_obj = null;
                        try {
                            JSON_obj = case_transfer_array.getJSONObject(m);
                            CaseTransferDetails caseTransferDetails = new CaseTransferDetails();

                            if (!JSON_obj.optString("FROM_PS").equalsIgnoreCase("null"))
                                caseTransferDetails.setFromPS(JSON_obj.optString("FROM_PS"));

                            if (!JSON_obj.optString("TO_PS").equalsIgnoreCase("null"))
                                caseTransferDetails.setToPS(JSON_obj.optString("TO_PS"));

                            if (!JSON_obj.optString("FROM_IONAME").equalsIgnoreCase("null"))
                                caseTransferDetails.setFromIoName(JSON_obj.optString("FROM_IONAME"));

                            if (!JSON_obj.optString("TO_IONAME").equalsIgnoreCase("null"))
                                caseTransferDetails.setToIoName(JSON_obj.optString("TO_IONAME"));

                            if (!JSON_obj.optString("TO_UNIT").equalsIgnoreCase("null"))
                                caseTransferDetails.setToUnit(JSON_obj.optString("TO_UNIT"));

                            if (!JSON_obj.optString("CASENO").equalsIgnoreCase("null"))
                                caseTransferDetails.setCaseNo(JSON_obj.optString("CASENO"));

                            if (!JSON_obj.optString("CASE_YR").equalsIgnoreCase("null"))
                                caseTransferDetails.setCaseYr(JSON_obj.optString("CASE_YR"));

                            if (!JSON_obj.optString("DT_TRANSFER").equalsIgnoreCase("null"))
                                caseTransferDetails.setTranferDt(JSON_obj.optString("DT_TRANSFER"));

                            if (!JSON_obj.optString("REMARKS").equalsIgnoreCase("null"))
                                caseTransferDetails.setRemarks(JSON_obj.optString("REMARKS"));

                            if (JSON_obj.optString("PSNAME") != null && !JSON_obj.optString("PSNAME").equalsIgnoreCase("null") && !JSON_obj.optString("PSNAME").equalsIgnoreCase(""))
                                caseTransferDetails.setPsName(JSON_obj.optString("PSNAME"));

                            firDetails.setCaseTransferList(caseTransferDetails);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                    JSONObject charge_sheet_obj = obj.getJSONObject("charge_sheet_details");

                    String court_details = "";
                    String charge_sheet_details = "";
                    String actsSection = "";
                    String frType = "";
                    String suppltmentaryOriginal = "";
                    String briefFacts = "";


                    ChargeSheetDetails chargeSheetDetails = new ChargeSheetDetails();

                    if (charge_sheet_obj.optString("COURT_OF") != null && !charge_sheet_obj.optString("COURT_OF").equalsIgnoreCase("") && !charge_sheet_obj.optString("COURT_OF").equalsIgnoreCase("null")) {
                        court_details = court_details + charge_sheet_obj.optString("COURT_OF");
                    }

                    if (charge_sheet_obj.optString("DISTRICT") != null && !charge_sheet_obj.optString("DISTRICT").equalsIgnoreCase("") && !charge_sheet_obj.optString("DISTRICT").equalsIgnoreCase("null")) {
                        court_details = court_details + " " + charge_sheet_obj.optString("DISTRICT");
                    }

                    chargeSheetDetails.setCourtOf(court_details);

                    /*if(charge_sheet_obj.optString("FR_CHGNO")!=null && !charge_sheet_obj.optString("FR_CHGNO").equalsIgnoreCase("")&& !charge_sheet_obj.optString("FR_CHGNO").equalsIgnoreCase("null")){
                        charge_sheet_details= charge_sheet_obj.optString("FR_CHGNO");
                        csHeaderString = csHeaderString +charge_sheet_obj.optString("FR_CHGNO");
                    }

                    if(charge_sheet_obj.optString("FR_CHGDATE")!=null && !charge_sheet_obj.optString("FR_CHGDATE").equalsIgnoreCase("")&& !charge_sheet_obj.optString("FR_CHGDATE").equalsIgnoreCase("null")){
                        charge_sheet_details= charge_sheet_details + " on "+charge_sheet_obj.optString("FR_CHGDATE");
                        csHeaderString = csHeaderString + " Dated "+charge_sheet_obj.optString("FR_CHGDATE");
                    }*/

                    if (charge_sheet_obj.optString("FR_CHGNO") != null && !charge_sheet_obj.optString("FR_CHGNO").equalsIgnoreCase("") && !charge_sheet_obj.optString("FR_CHGNO").equalsIgnoreCase("null")) {
                        charge_sheet_details = charge_sheet_obj.optString("FR_CHGNO");
                        csHeaderString = csHeaderString + charge_sheet_obj.optString("FR_CHGNO");
                        chargeSheetDetails.setFir_chgNo(charge_sheet_obj.optString("FR_CHGNO"));
                    }

                    if (charge_sheet_obj.optString("FR_CHGDATE") != null && !charge_sheet_obj.optString("FR_CHGDATE").equalsIgnoreCase("") && !charge_sheet_obj.optString("FR_CHGDATE").equalsIgnoreCase("null")) {
                        charge_sheet_details = charge_sheet_details + " on " + charge_sheet_obj.optString("FR_CHGDATE");
                        csHeaderString = csHeaderString + " Dated " + charge_sheet_obj.optString("FR_CHGDATE");
                        chargeSheetDetails.setFir_date(charge_sheet_obj.optString("FR_CHGDATE"));
                    }

                    if (charge_sheet_obj.optString("FRTYPE") != null && !charge_sheet_obj.optString("FRTYPE").equalsIgnoreCase("") && !charge_sheet_obj.optString("FRTYPE").equalsIgnoreCase("null")) {
                        frType = charge_sheet_obj.optString("FRTYPE");
                    }
                    if (charge_sheet_obj.optString("SUPPLEMENTARY_ORIGINAL") != null && !charge_sheet_obj.optString("SUPPLEMENTARY_ORIGINAL").equalsIgnoreCase("") && !charge_sheet_obj.optString("SUPPLEMENTARY_ORIGINAL").equalsIgnoreCase("null")) {
                        suppltmentaryOriginal = charge_sheet_obj.optString("SUPPLEMENTARY_ORIGINAL");
                    }
                    if (charge_sheet_obj.optString("BRIEF_FACTS") != null && !charge_sheet_obj.optString("BRIEF_FACTS").equalsIgnoreCase("") && !charge_sheet_obj.optString("BRIEF_FACTS").equalsIgnoreCase("null")) {
                        briefFacts = charge_sheet_obj.optString("BRIEF_FACTS");
                    }
                    if (charge_sheet_obj.optString("ACT1") != null && !charge_sheet_obj.optString("ACT1").equalsIgnoreCase("") && !charge_sheet_obj.optString("ACT1").equalsIgnoreCase("null")) {
                        actsSection = charge_sheet_obj.optString("ACT1");
                    }
                    if (charge_sheet_obj.optString("ACT2") != null && !charge_sheet_obj.optString("ACT2").equalsIgnoreCase("") && !charge_sheet_obj.optString("ACT2").equalsIgnoreCase("null")) {
                        actsSection = actsSection + "/" + charge_sheet_obj.optString("ACT2");
                    }
                    if (charge_sheet_obj.optString("ACT3") != null && !charge_sheet_obj.optString("ACT3").equalsIgnoreCase("") && !charge_sheet_obj.optString("ACT3").equalsIgnoreCase("null")) {
                        actsSection = actsSection + "/" + charge_sheet_obj.optString("ACT3");
                    }


                    chargeSheetDetails.setCharge_sheet_details(charge_sheet_details);
                    chargeSheetDetails.setCourtOf(court_details);
                    chargeSheetDetails.setFrType(frType);
                    chargeSheetDetails.setSuppltmentaryOriginal(suppltmentaryOriginal);
                    chargeSheetDetails.setBriefFacts(briefFacts);
                    chargeSheetDetails.setActsSection(actsSection);

                    firDetails.setChargeSheetDetailsList(chargeSheetDetails);

                    JSONObject warrant_obj = obj.getJSONObject("warrant_details");

                    if (warrant_obj != null) {

                        String slNo = "";
                        String slDate = "";
                        String waType = "";
                        String waNo = "";
                        String waIssueDate = "";
                        String ps = "";
                        String psName = "";
                        String caseNo = "";
                        String underSections = "";
                        String issueCourt = "";
                        String psRecvDate = "";
                        String officerToServe = "";
                        String returnableDateToCourt = "";
                        String dtReturnToCourt = "";
                        String waCat = "";
                        String waSubType = "";
                        String waStatus = "";
                        String actionDate = "";
                        String firYr = "";
                        String processNo = "";
                        String casePs = "";
                        String remarks = "";
                        String waYear = "";
                        String waSlNo = "";

                        FIRWarrantDetails firWarrantDetails = new FIRWarrantDetails();

                        if (warrant_obj.optString("SLNO") != null && !warrant_obj.optString("SLNO").equalsIgnoreCase("") && !warrant_obj.optString("SLNO").equalsIgnoreCase("null")) {
                            slNo = warrant_obj.optString("SLNO");
                        }
                        if (warrant_obj.optString("SLDATE") != null && !warrant_obj.optString("SLDATE").equalsIgnoreCase("") && !warrant_obj.optString("SLDATE").equalsIgnoreCase("null")) {
                            slDate = warrant_obj.optString("SLDATE");
                        }
                        if (warrant_obj.optString("WATYPE") != null && !warrant_obj.optString("WATYPE").equalsIgnoreCase("") && !warrant_obj.optString("WATYPE").equalsIgnoreCase("null")) {
                            waType = warrant_obj.optString("WATYPE");
                        }
                        if (warrant_obj.optString("WANO") != null && !warrant_obj.optString("WANO").equalsIgnoreCase("") && !warrant_obj.optString("WANO").equalsIgnoreCase("null")) {
                            waNo = warrant_obj.optString("WANO");
                        }
                        if (warrant_obj.optString("WAISSUEDATE") != null && !warrant_obj.optString("WAISSUEDATE").equalsIgnoreCase("") && !warrant_obj.optString("WAISSUEDATE").equalsIgnoreCase("null")) {
                            waIssueDate = warrant_obj.optString("WAISSUEDATE");
                        }
                        if (warrant_obj.optString("PS") != null && !warrant_obj.optString("PS").equalsIgnoreCase("") && !warrant_obj.optString("PS").equalsIgnoreCase("null")) {
                            ps = warrant_obj.optString("PS");
                        }
                        if (warrant_obj.optString("PSNAME") != null && !warrant_obj.optString("PSNAME").equalsIgnoreCase("") && !warrant_obj.optString("PSNAME").equalsIgnoreCase("null")) {
                            psName = warrant_obj.optString("PSNAME");
                        }
                        if (warrant_obj.optString("CASENO") != null && !warrant_obj.optString("CASENO").equalsIgnoreCase("") && !warrant_obj.optString("CASENO").equalsIgnoreCase("null")) {
                            caseNo = warrant_obj.optString("CASENO");
                        }
                        if (warrant_obj.optString("UNDER_SECTIONS") != null && !warrant_obj.optString("UNDER_SECTIONS").equalsIgnoreCase("") && !warrant_obj.optString("UNDER_SECTIONS").equalsIgnoreCase("null")) {
                            underSections = warrant_obj.optString("UNDER_SECTIONS");
                        }
                        if (warrant_obj.optString("ISSUE_COURT") != null && !warrant_obj.optString("ISSUE_COURT").equalsIgnoreCase("") && !warrant_obj.optString("ISSUE_COURT").equalsIgnoreCase("null")) {
                            issueCourt = warrant_obj.optString("ISSUE_COURT");
                        }
                        if (warrant_obj.optString("PS_RECV_DATE") != null && !warrant_obj.optString("PS_RECV_DATE").equalsIgnoreCase("") && !warrant_obj.optString("PS_RECV_DATE").equalsIgnoreCase("null")) {
                            psRecvDate = warrant_obj.optString("PS_RECV_DATE");
                        }
                        if (warrant_obj.optString("OFFICER_TO_SERVE") != null && !warrant_obj.optString("OFFICER_TO_SERVE").equalsIgnoreCase("") && !warrant_obj.optString("OFFICER_TO_SERVE").equalsIgnoreCase("null")) {
                            officerToServe = warrant_obj.optString("OFFICER_TO_SERVE");
                        }
                        if (warrant_obj.optString("RETURNABLE_DATE_TO_COURT") != null && !warrant_obj.optString("RETURNABLE_DATE_TO_COURT").equalsIgnoreCase("") && !warrant_obj.optString("RETURNABLE_DATE_TO_COURT").equalsIgnoreCase("null")) {
                            returnableDateToCourt = warrant_obj.optString("RETURNABLE_DATE_TO_COURT");
                        }
                        if (warrant_obj.optString("DTRETURN_TO_COURT") != null && !warrant_obj.optString("DTRETURN_TO_COURT").equalsIgnoreCase("") && !warrant_obj.optString("DTRETURN_TO_COURT").equalsIgnoreCase("null")) {
                            dtReturnToCourt = warrant_obj.optString("DTRETURN_TO_COURT");
                        }
                        if (warrant_obj.optString("WACAT") != null && !warrant_obj.optString("WACAT").equalsIgnoreCase("") && !warrant_obj.optString("WACAT").equalsIgnoreCase("null")) {
                            waCat = warrant_obj.optString("WACAT");
                        }
                        if (warrant_obj.optString("WASUBTYPE") != null && !warrant_obj.optString("WASUBTYPE").equalsIgnoreCase("") && !warrant_obj.optString("WASUBTYPE").equalsIgnoreCase("null")) {
                            waSubType = warrant_obj.optString("WASUBTYPE");
                        }
                        if (warrant_obj.optString("WA_STATUS") != null && !warrant_obj.optString("WA_STATUS").equalsIgnoreCase("") && !warrant_obj.optString("WA_STATUS").equalsIgnoreCase("null")) {
                            waStatus = warrant_obj.optString("WA_STATUS");
                        }
                        if (warrant_obj.optString("ACTION_DATE") != null && !warrant_obj.optString("ACTION_DATE").equalsIgnoreCase("") && !warrant_obj.optString("ACTION_DATE").equalsIgnoreCase("null")) {
                            actionDate = warrant_obj.optString("ACTION_DATE");
                        }
                        if (warrant_obj.optString("FIR_YR") != null && !warrant_obj.optString("FIR_YR").equalsIgnoreCase("") && !warrant_obj.optString("FIR_YR").equalsIgnoreCase("null")) {
                            firYr = warrant_obj.optString("FIR_YR");
                        }
                        if (warrant_obj.optString("PROCESS_NO") != null && !warrant_obj.optString("PROCESS_NO").equalsIgnoreCase("") && !warrant_obj.optString("PROCESS_NO").equalsIgnoreCase("null")) {
                            processNo = warrant_obj.optString("PROCESS_NO");
                        }
                        if (warrant_obj.optString("CASEPS") != null && !warrant_obj.optString("CASEPS").equalsIgnoreCase("") && !warrant_obj.optString("CASEPS").equalsIgnoreCase("null")) {
                            casePs = warrant_obj.optString("CASEPS");
                        }
                        if (warrant_obj.optString("REMARKS") != null && !warrant_obj.optString("REMARKS").equalsIgnoreCase("") && !warrant_obj.optString("REMARKS").equalsIgnoreCase("null")) {
                            remarks = warrant_obj.optString("REMARKS");
                        }
                        if (warrant_obj.optString("WA_YEAR") != null && !warrant_obj.optString("WA_YEAR").equalsIgnoreCase("") && !warrant_obj.optString("WA_YEAR").equalsIgnoreCase("null")) {
                            waYear = warrant_obj.optString("WA_YEAR");
                        }
                        if (warrant_obj.optString("WA_SLNO") != null && !warrant_obj.optString("WA_SLNO").equalsIgnoreCase("") && !warrant_obj.optString("WA_SLNO").equalsIgnoreCase("null")) {
                            waSlNo = warrant_obj.optString("WA_SLNO");
                        }

                        try {

                            JSONArray firWarranteesArray = warrant_obj.getJSONArray("warrantees");

                            if (firWarranteesArray.length() > 0) {

                                for (int j = 0; j < firWarranteesArray.length(); j++) {

                                    FIRWarranteesDetails firWarranteesDetails = new FIRWarranteesDetails();

                                    JSONObject warranteesObj = firWarranteesArray.getJSONObject(j);

                                    if (warranteesObj.optString("SLNO") != null && !warranteesObj.optString("SLNO").equalsIgnoreCase("") && !warranteesObj.optString("SLNO").equalsIgnoreCase("null")) {
                                        firWarranteesDetails.setSl_no(warranteesObj.optString("SLNO"));
                                    }
                                    if (warranteesObj.optString("NAME") != null && !warranteesObj.optString("NAME").equalsIgnoreCase("") && !warranteesObj.optString("NAME").equalsIgnoreCase("null")) {
                                        firWarranteesDetails.setWarrantee_name(warranteesObj.optString("NAME"));
                                    }
                                    if (warranteesObj.optString("ADDRESS") != null && !warranteesObj.optString("ADDRESS").equalsIgnoreCase("") && !warranteesObj.optString("ADDRESS").equalsIgnoreCase("null")) {
                                        firWarranteesDetails.setWarrantee_address(warranteesObj.optString("ADDRESS"));
                                    }

                                    firWarrantDetails.setFirWarranteesDetailsList(firWarranteesDetails);

                                }

                            }
                            firDetails.setWarrantDetailsList(firWarrantDetails);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        firWarrantDetails.setSlNo(slNo);
                        firWarrantDetails.setSlDate(slDate);
                        firWarrantDetails.setWaType(waType);
                        firWarrantDetails.setWaNo(waNo);
                        firWarrantDetails.setWaIssueDate(waIssueDate);
                        firWarrantDetails.setPs(ps);
                        firWarrantDetails.setPs_name(psName);
                        firWarrantDetails.setCaseNo(caseNo);
                        firWarrantDetails.setUnderSections(underSections);
                        firWarrantDetails.setIssueCourt(issueCourt);
                        firWarrantDetails.setPsRecvDate(psRecvDate);
                        firWarrantDetails.setOfficerToServe(officerToServe);
                        firWarrantDetails.setReturnableDateToCourt(returnableDateToCourt);
                        firWarrantDetails.setDtReturnToCourt(dtReturnToCourt);
                        firWarrantDetails.setWaCat(waCat);
                        firWarrantDetails.setWaSubType(waSubType);
                        firWarrantDetails.setWaStatus(waStatus);
                        firWarrantDetails.setActionDate(actionDate);
                        firWarrantDetails.setFirYr(firYr);
                        firWarrantDetails.setProcessNo(processNo);
                        firWarrantDetails.setCasePs(casePs);
                        firWarrantDetails.setRemarks(remarks);
                        firWarrantDetails.setWaYear(waYear);
                        firWarrantDetails.setWaSlNo(waSlNo);


                    }


                } else if (key_flag.equalsIgnoreCase("C")) {

                    // C ---> means OC Key

                    String alias = "";
                    String underSection = "";
                    String acts = " ";
                    String category = "";
                    String io = "";

                    if (obj.optString("PSNAME_AS_CODE") != null && !obj.optString("PSNAME_AS_CODE").equalsIgnoreCase("") && !obj.optString("PSNAME_AS_CODE").equalsIgnoreCase("null")) {
                        firDetails.setPsNameAsCode(obj.optString("PSNAME_AS_CODE"));
                    }

                    if (obj.optString("CRIMENO") != null && !obj.optString("CRIMENO").equalsIgnoreCase("") && !obj.optString("CRIMENO").equalsIgnoreCase("null")) {
                        firDetails.setCrimeNo(obj.optString("CRIMENO"));
                    }

                    firDetails.setName(obj.optString("NAME"));

                    if (obj.optString("ANAME1") != null && !obj.optString("ANAME1").equalsIgnoreCase("") && !obj.optString("ANAME1").equalsIgnoreCase("null")) {
                        alias = alias + obj.optString("ANAME1");
                    }
                    if (obj.optString("ANAME2") != null && !obj.optString("ANAME2").equalsIgnoreCase("") && !obj.optString("ANAME2").equalsIgnoreCase("null")) {
                        alias = alias + "/ " + obj.optString("ANAME2");
                    }
                    if (obj.optString("ANAME3") != null && !obj.optString("ANAME3").equalsIgnoreCase("") && !obj.optString("ANAME3").equalsIgnoreCase("null")) {
                        alias = alias + "/ " + obj.optString("ANAME3");
                    }
                    if (obj.optString("ANAME4") != null && !obj.optString("ANAME4").equalsIgnoreCase("") && !obj.optString("ANAME4").equalsIgnoreCase("null")) {
                        alias = alias + "/ " + obj.optString("ANAME4");
                    }
                    if (obj.optString("SEC11") != null && !obj.optString("SEC11").equalsIgnoreCase("") && !obj.optString("SEC11").equalsIgnoreCase("null")) {
                        underSection = obj.optString("SEC11");
                    }
                    if (obj.optString("SEC12") != null && !obj.optString("SEC12").equalsIgnoreCase("") && !obj.optString("SEC12").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC12");
                    }
                    if (obj.optString("SEC13") != null && !obj.optString("SEC13").equalsIgnoreCase("") && !obj.optString("SEC13").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC13");
                    }
                    if (obj.optString("SEC14") != null && !obj.optString("SEC14").equalsIgnoreCase("") && !obj.optString("SEC14").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC14");
                    }
                    if (obj.optString("SEC15") != null && !obj.optString("SEC15").equalsIgnoreCase("") && !obj.optString("SEC15").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC15");
                    }
                    if (obj.optString("SEC16") != null && !obj.optString("SEC16").equalsIgnoreCase("") && !obj.optString("SEC16").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC16");
                    }
                    if (obj.optString("SEC21") != null && !obj.optString("SEC21").equalsIgnoreCase("") && !obj.optString("SEC21").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC21");
                    }
                    if (obj.optString("SEC22") != null && !obj.optString("SEC22").equalsIgnoreCase("") && !obj.optString("SEC22").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC22");
                    }
                    if (obj.optString("SEC23") != null && !obj.optString("SEC23").equalsIgnoreCase("") && !obj.optString("SEC23").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC23");
                    }
                    if (obj.optString("SEC24") != null && !obj.optString("SEC24").equalsIgnoreCase("") && !obj.optString("SEC24").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC24");
                    }
                    if (obj.optString("SEC25") != null && !obj.optString("SEC25").equalsIgnoreCase("") && !obj.optString("SEC25").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC25");
                    }
                    if (obj.optString("SEC26") != null && !obj.optString("SEC26").equalsIgnoreCase("") && !obj.optString("SEC26").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC26");
                    }
                    if (obj.optString("SEC31") != null && !obj.optString("SEC31").equalsIgnoreCase("") && !obj.optString("SEC31").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC31");
                    }
                    if (obj.optString("SEC32") != null && !obj.optString("SEC32").equalsIgnoreCase("") && !obj.optString("SEC32").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC32");
                    }
                    if (obj.optString("SEC33") != null && !obj.optString("SEC33").equalsIgnoreCase("") && !obj.optString("SEC33").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC33");
                    }
                    if (obj.optString("SEC34") != null && !obj.optString("SEC34").equalsIgnoreCase("") && !obj.optString("SEC34").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC34");
                    }
                    if (obj.optString("SEC35") != null && !obj.optString("SEC35").equalsIgnoreCase("") && !obj.optString("SEC35").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC35");
                    }
                    if (obj.optString("SEC36") != null && !obj.optString("SEC36").equalsIgnoreCase("") && !obj.optString("SEC36").equalsIgnoreCase("null")) {
                        underSection = underSection + "/" + obj.optString("SEC36");
                    }
                    if (obj.optString("ACTS1") != null && !obj.optString("ACTS1").equalsIgnoreCase("") && !obj.optString("ACTS1").equalsIgnoreCase("null")) {
                        acts = acts + obj.optString("ACTS1");
                    }
                    if (obj.optString("ACTS2") != null && !obj.optString("ACTS2").equalsIgnoreCase("") && !obj.optString("ACTS2").equalsIgnoreCase("null")) {
                        acts = acts + "/" + obj.optString("ACTS2");
                    }
                    if (obj.optString("ACTS3") != null && !obj.optString("ACTS3").equalsIgnoreCase("") && !obj.optString("ACTS3").equalsIgnoreCase("null")) {
                        acts = acts + "/" + obj.optString("ACTS3");
                    }
                    if (obj.optString("ACTS4") != null && !obj.optString("ACTS4").equalsIgnoreCase("") && !obj.optString("ACTS4").equalsIgnoreCase("null")) {
                        acts = acts + "/" + obj.optString("ACTS4");
                    }
                    if (obj.optString("CLASS") != null && !obj.optString("CLASS").equalsIgnoreCase("") && !obj.optString("CLASS").equalsIgnoreCase("null")) {
                        category = obj.optString("CLASS");
                    }
                    if (obj.optString("IO") != null && !obj.optString("IO").equalsIgnoreCase("") && !obj.optString("IO").equalsIgnoreCase("null")) {
                        io = obj.optString("IO");
                    }


                    firDetails.setAlias(alias);
                    firDetails.setUnder_section(underSection + acts);
                    firDetails.setPs_code(obj.optString("PSCODE") + " PS");

                    if (obj.optString("CASEREF") != null && !obj.optString("CASEREF").equalsIgnoreCase("") && !obj.optString("CASEREF").equalsIgnoreCase("null")) {
                        firDetails.setCase_ref(obj.optString("CASEREF"));
                    }


                    firDetails.setCase_date(obj.optString("CASEDATE"));
                    firDetails.setCategory(category);
                    firDetails.setIo(io);
                    firDetails.setPdf_url(obj.optString("pdf_url"));
                    firDetails.setPmReport_pdf_url(obj.optString("pmreport_pdf_url"));

                    JSONArray other_accused_array = obj.getJSONArray("other_accused");

                    for (int j = 0; j < other_accused_array.length(); j++) {

                        firOtherAccusedDetail = new FIROtherAccusedDetail();

                        String accusedAddress = "";
                        String accusedAliasName = "";

                        JSONObject obj_other_accused = other_accused_array.getJSONObject(j);

                        firOtherAccusedDetail.setName(obj_other_accused.optString("NAME"));
                        firOtherAccusedDetail.setFatherName(obj_other_accused.optString("F_NAME"));


                        if (obj_other_accused.optString("PSROAD_PSPLACE") != null && !obj_other_accused.optString("PSROAD_PSPLACE").equalsIgnoreCase("") && !obj_other_accused.optString("PSROAD_PSPLACE").equalsIgnoreCase("null")) {
                            accusedAddress = obj_other_accused.optString("PSROAD_PSPLACE");
                        }
                        if (obj_other_accused.optString("PRPS_KPJ") != null && !obj_other_accused.optString("PRPS_KPJ").equalsIgnoreCase("") && !obj_other_accused.optString("PRPS_KPJ").equalsIgnoreCase("null")) {
                            accusedAddress = accusedAddress + "," + obj_other_accused.optString("PRPS_KPJ");
                        }
                        if (obj_other_accused.optString("PRPS_OUT") != null && !obj_other_accused.optString("PRPS_OUT").equalsIgnoreCase("") && !obj_other_accused.optString("PRPS_OUT").equalsIgnoreCase("null")) {
                            accusedAddress = accusedAddress + "," + obj_other_accused.optString("PRPS_OUT");
                        }
                        if (obj_other_accused.optString("PSDIST") != null && !obj_other_accused.optString("PSDIST").equalsIgnoreCase("") && !obj_other_accused.optString("PSDIST").equalsIgnoreCase("null")) {
                            accusedAddress = accusedAddress + "," + obj_other_accused.optString("PSDIST");
                        }
                        if (obj_other_accused.optString("PSDIST_OUT") != null && !obj_other_accused.optString("PSDIST_OUT").equalsIgnoreCase("") && !obj_other_accused.optString("PSDIST_OUT").equalsIgnoreCase("null")) {
                            accusedAddress = accusedAddress + "," + obj_other_accused.optString("PSDIST_OUT");
                        }
                        if (obj_other_accused.optString("PSPIN") != null && !obj_other_accused.optString("PSPIN").equalsIgnoreCase("") && !obj_other_accused.optString("PSPIN").equalsIgnoreCase("null")) {
                            accusedAddress = accusedAddress + "," + obj_other_accused.optString("PSPIN");
                        }
                        if (obj_other_accused.optString("PSSTATE") != null && !obj_other_accused.optString("PSSTATE").equalsIgnoreCase("") && !obj_other_accused.optString("PSSTATE").equalsIgnoreCase("null")) {
                            accusedAddress = accusedAddress + "," + obj_other_accused.optString("PSSTATE");
                        }

                        firOtherAccusedDetail.setAddress(accusedAddress);


                        if (obj_other_accused.optString("ANAME1") != null && !obj_other_accused.optString("ANAME1").equalsIgnoreCase("") && !obj_other_accused.optString("ANAME1").equalsIgnoreCase("null")) {
                            accusedAliasName = "@ " + obj_other_accused.optString("ANAME1");
                        }
                        if (obj_other_accused.optString("ANAME2") != null && !obj_other_accused.optString("ANAME2").equalsIgnoreCase("") && !obj_other_accused.optString("ANAME2").equalsIgnoreCase("null")) {
                            accusedAliasName = accusedAliasName + "/" + obj_other_accused.optString("ANAME2");
                        }
                        if (obj_other_accused.optString("ANAME3") != null && !obj_other_accused.optString("ANAME3").equalsIgnoreCase("") && !obj_other_accused.optString("ANAME3").equalsIgnoreCase("null")) {
                            accusedAliasName = accusedAliasName + "/" + obj_other_accused.optString("ANAME3");
                        }
                        if (obj_other_accused.optString("ANAME4") != null && !obj_other_accused.optString("ANAME4").equalsIgnoreCase("") && !obj_other_accused.optString("ANAME4").equalsIgnoreCase("null")) {
                            accusedAliasName = accusedAliasName + "/" + obj_other_accused.optString("ANAME4");
                        }

                        firOtherAccusedDetail.setAlias(accusedAliasName);

                        otherAccusedList = Utility
                                .parseCrimiinalAssociateRecords(getActivity(), other_accused_array);


                        firOtherAccusedDetail.setOtherAccusedListItemData(otherAccusedList);

                        firDetails.setFirOtherAccusedDetailList(firOtherAccusedDetail);
                    }

                    otherAccusedList = Utility
                            .parseCrimiinalAssociateRecords(getActivity(), other_accused_array);

                    JSONArray pr_status_array = obj.getJSONArray("pr_status");

                    for (int l = 0; l < pr_status_array.length(); l++) {

                        criminalSearchPRStatus = new CriminalSearchPRStatus();

                        JSONObject obj_pr_status = pr_status_array.getJSONObject(l);

                        criminalSearchPRStatus.setEvenDetails(obj_pr_status.optString("EVENT_DETAILS"));
                        criminalSearchPRStatus.setDateRecorded(obj_pr_status.optString("DATE_RECORDED"));
                        criminalSearchPRStatus.setPrsStatus(obj_pr_status.optString("PRS_STATUS"));
                        criminalSearchPRStatus.setDateTill(obj_pr_status.optString("DATE_TILL"));
                        criminalSearchPRStatus.setEvent_SlNo(obj_pr_status.optString("EVENT_SLNO"));

                        firDetails.setPrsStatusList(criminalSearchPRStatus);

                    }

                      /* Case Transfer data parse */

                    JSONArray case_transfer_array = obj.getJSONArray("CASE_TRANSFER");

                    for (int m = 0; m < case_transfer_array.length(); m++) {

                        JSONObject JSON_obj = null;
                        try {
                            JSON_obj = case_transfer_array.getJSONObject(m);
                            CaseTransferDetails caseTransferDetails = new CaseTransferDetails();

                            if (!JSON_obj.optString("FROM_PS").equalsIgnoreCase("null"))
                                caseTransferDetails.setFromPS(JSON_obj.optString("FROM_PS"));

                            if (!JSON_obj.optString("TO_PS").equalsIgnoreCase("null"))
                                caseTransferDetails.setToPS(JSON_obj.optString("TO_PS"));

                            if (!JSON_obj.optString("FROM_IONAME").equalsIgnoreCase("null"))
                                caseTransferDetails.setFromIoName(JSON_obj.optString("FROM_IONAME"));

                            if (!JSON_obj.optString("TO_IONAME").equalsIgnoreCase("null"))
                                caseTransferDetails.setToIoName(JSON_obj.optString("TO_IONAME"));

                            if (!JSON_obj.optString("TO_UNIT").equalsIgnoreCase("null"))
                                caseTransferDetails.setToUnit(JSON_obj.optString("TO_UNIT"));

                            if (!JSON_obj.optString("CASENO").equalsIgnoreCase("null"))
                                caseTransferDetails.setCaseNo(JSON_obj.optString("CASENO"));

                            if (!JSON_obj.optString("CASE_YR").equalsIgnoreCase("null"))
                                caseTransferDetails.setCaseYr(JSON_obj.optString("CASE_YR"));

                            if (!JSON_obj.optString("DT_TRANSFER").equalsIgnoreCase("null"))
                                caseTransferDetails.setTranferDt(JSON_obj.optString("DT_TRANSFER"));

                            if (!JSON_obj.optString("REMARKS").equalsIgnoreCase("null"))
                                caseTransferDetails.setRemarks(JSON_obj.optString("REMARKS"));

                            firDetails.setCaseTransferList(caseTransferDetails);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                } else {

                }

                firDetailsList.add(firDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        firDetailsListAdapter = new FIRDetailsListAdapter(getActivity(), firDetailsList, "CF");
        lv_fir.setAdapter(firDetailsListAdapter);
        firDetailsListAdapter.notifyDataSetChanged();

        firDetailsListAdapter.setFIRNameTapListener(this);
        firDetailsListAdapter.setFIREventLogTapListener(this);
    }






    @Override
    public void onFirNameClick(View view, int position) {

        Log.e("MOITRI::","MOITRI MOITRI MOITRI");

        CriminalFIRDetailsFragment criminalFIRFragment = new CriminalFIRDetailsFragment();

        FIRDetails firDetails_model = firDetailsList.get(position);

        String itemName = firDetailsList.get(position).getPs_name() + " C/No." + firDetailsList.get(position).getCase_ref() + " Dated " + firDetailsList.get(position).getCase_date();
        String flag_value = "";
        if(flag.equalsIgnoreCase("OC"))
            flag_value = "OC";
        else if(flag.equalsIgnoreCase("OF"))
            flag_value = "OF";
        else if(flag.equalsIgnoreCase("CF"))
            flag_value = "CF";

        Bundle bundle = new Bundle();
        bundle.putString("PS_CODE",ps_code);
        bundle.putString("CASE_NO",case_no);
        bundle.putString("CASE_YEAR",case_year);
        bundle.putString("PM_NO",firDetailsList.get(position).getPmNo());
        bundle.putString("PM_YEAR",firDetailsList.get(position).getPmYear());
        bundle.putString("CRIMINAL_NAME",criminal_name);
        bundle.putString("FLAG_VALUE",flag_value);
        bundle.putString("ITEM_NAME_SHOW",itemName);
        bundle.putInt("ITEM_POSITION",position);
        bundle.putSerializable("FIR_DETAILS_LIST", firDetails_model);
        criminalFIRFragment.setArguments(bundle);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.bottom_up, R.anim.bottom_down);

        linear_loadCriminalDetails = (LinearLayout) getActivity().findViewById(R.id.linear_loadCriminalDetails);
        linear_loadCriminalDetails.setVisibility(View.VISIBLE);

        fragmentTransaction.replace(R.id.linear_loadCriminalDetails, criminalFIRFragment,CriminalFIRDetailsFragment.TAG);
        fragmentTransaction.commit();

    }


    @Override
    public void onFirEventLogClick(View view, int position) {

        LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflatedViewFIREvent = layoutInflater.inflate(R.layout.log_layout,null,false);

        linear_EventDetails = (LinearLayout)inflatedViewFIREvent.findViewById(R.id.linear_caseTranfer);
        final TextView tv_eventLog = (TextView)inflatedViewFIREvent.findViewById(R.id.tv_showCaseTransfer);

        if(firDetailsList.get(position).getName() != null && !firDetailsList.get(position).getName().equalsIgnoreCase("null") && !firDetailsList.get(position).getName().equalsIgnoreCase("")){
            tv_eventLog.setText("Log of "+ firDetailsList.get(position).getName());
        }else {
            tv_eventLog.setText("Event Details");
        }


        if (firDetailsList.get(position).getPrsStatusList().size() > 0) {

            int sl_no = 0;
            // add view to the layout
            int length = firDetailsList.get(position).getPrsStatusList().size();

            for (int i = 0; i < length; i++) {

                sl_no++;
                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View v = inflater.inflate(R.layout.fir_eventdetails_list_item, null);

                TextView tv_slNo = (TextView) v.findViewById(R.id.tv_slNo);
                TextView tv_eventDetails = (TextView) v.findViewById(R.id.tv_eventDetails);
                TextView tv_dateRecorded = (TextView) v.findViewById(R.id.tv_dateRecorded);
                TextView tv_prsStatus = (TextView) v.findViewById(R.id.tv_prsStatus);
                TextView tv_showTillDate = (TextView) v.findViewById(R.id.tv_showTillDate);



                if(firDetailsList.get(position).getPrsStatusList().get(i).getEvent_SlNo() != null &&
                        !firDetailsList.get(position).getPrsStatusList().get(i).getEvent_SlNo().equalsIgnoreCase("null") &&
                        !firDetailsList.get(position).getPrsStatusList().get(i).getEvent_SlNo().equalsIgnoreCase("")) {

                    tv_slNo.setText(firDetailsList.get(position).getPrsStatusList().get(i).getEvent_SlNo());
                }
                else{

                    tv_slNo.setText("" + sl_no);
                }

                if(firDetailsList.get(position).getPrsStatusList().get(i).getEvenDetails() != null &&
                        !firDetailsList.get(position).getPrsStatusList().get(i).getEvenDetails().equalsIgnoreCase("null") &&
                        !firDetailsList.get(position).getPrsStatusList().get(i).getEvenDetails().equalsIgnoreCase("")) {
                    tv_eventDetails.setText(firDetailsList.get(position).getPrsStatusList().get(i).getEvenDetails());
                }

                if(firDetailsList.get(position).getPrsStatusList().get(i).getDateRecorded() != null &&
                        !firDetailsList.get(position).getPrsStatusList().get(i).getDateRecorded().equalsIgnoreCase("null") &&
                        !firDetailsList.get(position).getPrsStatusList().get(i).getDateRecorded().equalsIgnoreCase("")) {
                    tv_dateRecorded.setText(firDetailsList.get(position).getPrsStatusList().get(i).getDateRecorded());
                }

                if(firDetailsList.get(position).getPrsStatusList().get(i).getPrsStatus() != null &&
                        !firDetailsList.get(position).getPrsStatusList().get(i).getPrsStatus().equalsIgnoreCase("null") &&
                        !firDetailsList.get(position).getPrsStatusList().get(i).getPrsStatus().equalsIgnoreCase("")) {
                    tv_prsStatus.setText(firDetailsList.get(position).getPrsStatusList().get(i).getPrsStatus());
                }

                if(firDetailsList.get(position).getPrsStatusList().get(i).getDateTill() != null &&
                        !firDetailsList.get(position).getPrsStatusList().get(i).getDateTill().equalsIgnoreCase("null") &&
                        !firDetailsList.get(position).getPrsStatusList().get(i).getDateTill().equalsIgnoreCase("")) {
                    tv_showTillDate.setText(firDetailsList.get(position).getPrsStatusList().get(i).getDateTill());
                }

                tv_slNo.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_light_blue));
                tv_eventDetails.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_light_blue));
                tv_dateRecorded.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_light_blue));
                tv_prsStatus.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_light_blue));
                tv_showTillDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_light_blue));

                linear_EventDetails.addView(v);
            }
            openPopupWindowForEventLog();

        } else {

            Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EVENT_DETAIL, false);
        }
    }


    private void openPopupWindowForEventLog(){

        linear_main.setAlpha(0.1f);

        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // set height depends on the device size
        popWindowForEventLog = new PopupWindow(inflatedViewFIREvent, width,height-50, true );

        popWindowForEventLog.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindowForEventLog.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindowForEventLog.setAnimationStyle(R.style.PopupAnimation);

        //to generate popUpWindow at bottom of ActionBar
        popWindowForEventLog.showAtLocation(getActivity().getActionBar().getCustomView(),Gravity.CENTER,0,20);

        popWindowForEventLog.setOutsideTouchable(true);
        popWindowForEventLog.setFocusable(true);
        popWindowForEventLog.getContentView().setFocusableInTouchMode(true);
        popWindowForEventLog.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    if (popWindowForEventLog != null && popWindowForEventLog.isShowing()) {
                        popWindowForEventLog.dismiss();

                    }
                    return true;
                }
                return false;
            }
        });

        popWindowForEventLog.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //Do Something here
                linear_main.setAlpha(1.0f);
            }
        });
    }
}
