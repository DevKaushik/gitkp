package com.kp.facedetection.interfaces;

import android.view.View;

/**
 * Created by DAT-Asset-128 on 28-11-2017.
 */

public interface OnItemClickListenerForUnnaturalDeath {
    void onClick(View view, int position);
}
