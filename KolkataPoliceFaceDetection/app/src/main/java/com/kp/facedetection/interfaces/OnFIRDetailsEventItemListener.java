package com.kp.facedetection.interfaces;

import android.view.View;

/**
 * Created by DAT-165 on 11-07-2017.
 */

public interface OnFIRDetailsEventItemListener {
    void onFirEventLogClick(View v, int pos);
}
