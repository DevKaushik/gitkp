package com.kp.facedetection.interfaces;

import org.json.JSONObject;

/**
 * Created by DAT-Asset-128 on 11-10-2017.
 */

public interface OnDescriptiveRoleUpdateListener {
    void updateDescriptiveRole(JSONObject jo);
}
