package com.kp.facedetection.interfaces;

/**
 * Created by DAT-Asset-131 on 20-09-2016.
 */
public interface OnShowAlertForProceedResult {

    void onShowAlertForProceedResultSuccess(String success);
    void onShowAlertForProceedResultError(String error);

}
