package com.kp.facedetection.GlobalSearch.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.VehicleSearchDetails;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by DAT-165 on 28-07-2016.
 */
public class GlobalVehicleSearchAdapter extends BaseAdapter {

    private Context context;
    private List<VehicleSearchDetails> vehicleSearchDetailsList;

    public GlobalVehicleSearchAdapter(Context context, List<VehicleSearchDetails> vehicleSearchDetailsList) {
        this.context = context;
        this.vehicleSearchDetailsList = vehicleSearchDetailsList;
    }


    @Override
    public int getCount() {

        int size = (vehicleSearchDetailsList.size()>0)? vehicleSearchDetailsList.size():0;
        return size;

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.modified_sdr_search_list_item, null);


            holder.tv_slNo=(TextView)convertView.findViewById(R.id.tv_slNo);
            holder.tv_name=(TextView)convertView.findViewById(R.id.tv_name);
            holder.tv_address=(TextView)convertView.findViewById(R.id.tv_address);
            holder.tv_mobileNo=(TextView)convertView.findViewById(R.id.tv_mobileNo);

            holder.tv_mobileNo.setVisibility(View.GONE);

            Constants.changefonts(holder.tv_slNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_name, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_address, context, "Calibri.ttf");

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);

        holder.tv_slNo.setText(sl_no);
        holder.tv_name.setText("NAME: "+vehicleSearchDetailsList.get(position).getO_name().trim());
        holder.tv_address.setText("ADDRESS: "+vehicleSearchDetailsList.get(position).getAddress().trim());

        return convertView;
    }

    class ViewHolder {

        TextView tv_slNo,tv_name,tv_address,tv_mobileNo;
    }
}
