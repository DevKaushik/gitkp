package com.kp.facedetection.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.BuildConfig;
import com.kp.facedetection.KPFaceDetectionApplication;
import com.kp.facedetection.MisActiveCustomAdapter;
import com.kp.facedetection.NotificationDetailActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnShowAlertForProceedResult;
import com.kp.facedetection.model.CriminalDetails;
import com.kp.facedetection.model.NotificationDetails;
import com.kp.facedetection.model.SpecialServicesModel;
import com.kp.facedetection.model.UserInfo;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class Utility {


    public void setDelegate(OnShowAlertForProceedResult delegate) {
        this.delegate = delegate;
    }

    static OnShowAlertForProceedResult delegate;

    public static void setUserInfo(Context mContext, UserInfo user) {
        try {
            SharedPreferences sharedpreferences = mContext
                    .getSharedPreferences("AppPref", Context.MODE_PRIVATE);
            Editor editor = sharedpreferences.edit();
            editor.putString("userinfo", ObjectSerializer.serialize(user));
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static UserInfo getUserInfo(Context context) {
        UserInfo userInfo = null;
        try {
            SharedPreferences sharedpreferences = context.getSharedPreferences(
                    "AppPref", Context.MODE_PRIVATE);
            userInfo = (UserInfo) ObjectSerializer
                    .deserialize(sharedpreferences.getString("userinfo", null));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userInfo;
    }
    public static void setSpecialServices(Context mContext,SpecialServicesModel specialServicesModel){
        try {
            SharedPreferences sharedpreferences = mContext
                    .getSharedPreferences("AppPref", Context.MODE_PRIVATE);
            Editor editor = sharedpreferences.edit();
            editor.putString("spservice", ObjectSerializer.serialize(specialServicesModel));
            editor.commit();
        }catch (Exception e){

        }

    }

    public static SpecialServicesModel getSpecialServices(Context context){
        SpecialServicesModel specialServicesModel=null;
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences("AppPref", Context.MODE_PRIVATE);
            specialServicesModel = (SpecialServicesModel) ObjectSerializer.deserialize(sharedPreferences.getString("spservice", null));
        }catch(Exception e) {
        }

         return specialServicesModel;
    }
    public static void setPushNotificationCount(Context context,String notificationCount){
        try{
            SharedPreferences sharedPreferences=context.getSharedPreferences("AppPref",Context.MODE_PRIVATE);
            Editor editor=sharedPreferences.edit();
            editor.putString("PushNotificationCount",notificationCount);
            editor.commit();

        }catch(Exception e){

        }
    }
    public static String getPushNotificationCount(Context context){
        String notificationCount="";
        try{
            SharedPreferences sharedPreferences=context.getSharedPreferences("AppPref",Context.MODE_PRIVATE);
            notificationCount=sharedPreferences.getString("PushNotificationCount","");

        }catch(Exception e){

        }
        return notificationCount;
    }
    public static void setImiNO(Context mContext, String imiNO) {
        try {
            SharedPreferences sharedpreferences = mContext
                    .getSharedPreferences("AppPref", Context.MODE_PRIVATE);
            Editor editor = sharedpreferences.edit();
            editor.putString("DeviceImiNO",imiNO );
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static String getImiNO(Context context) {
        String imiNo = "";
        try {
            SharedPreferences sharedpreferences = context.getSharedPreferences(
                    "AppPref", Context.MODE_PRIVATE);
            imiNo = sharedpreferences.getString("DeviceImiNO","");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imiNo;
    } public static void setFCMToken(Context mContext, String FCMID) {
        try {
            SharedPreferences sharedpreferences = mContext
                    .getSharedPreferences("AppPref", Context.MODE_PRIVATE);
            Editor editor = sharedpreferences.edit();
            editor.putString("FCMDeviceToken",FCMID );
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static String getFCMToken(Context context) {
        String deviceToken = "";
        try {
            SharedPreferences sharedpreferences = context.getSharedPreferences(
                    "AppPref", Context.MODE_PRIVATE);
            deviceToken = sharedpreferences.getString("FCMDeviceToken","");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deviceToken;
    }

    public static void setUserNotificationCount(Context mContext, String notificationCount) {
        try {
            SharedPreferences sharedpreferences = mContext
                    .getSharedPreferences("AppPref", Context.MODE_PRIVATE);
            Editor editor = sharedpreferences.edit();
            editor.putString("userNotificationCount", notificationCount);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static String getUserNotificationCount(Context context) {
        String usernotificationcount = "";
        try {
            SharedPreferences sharedpreferences = context.getSharedPreferences(
                    "AppPref", Context.MODE_PRIVATE);
            usernotificationcount = sharedpreferences.getString("userNotificationCount","0");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return usernotificationcount;
    }
    public static void setDocumentSearchSesionId(Context mContext, String sessionId) {
        try {
            SharedPreferences sharedpreferences = mContext
                    .getSharedPreferences("AppPref", Context.MODE_PRIVATE);
            Editor editor = sharedpreferences.edit();
            editor.putString("userDocumentSearchSesionId", sessionId);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static String getDocumentSearchSesionId(Context context) {
        String userDocumentSearchSesionId = "";
        try {
            SharedPreferences sharedpreferences = context.getSharedPreferences(
                    "AppPref", Context.MODE_PRIVATE);
            userDocumentSearchSesionId = sharedpreferences.getString("userDocumentSearchSesionId","0");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userDocumentSearchSesionId;
    }
    public static void setHotelSearchSesionId(Context mContext, String sessionId) {
        try {
            SharedPreferences sharedpreferences = mContext
                    .getSharedPreferences("AppPref", Context.MODE_PRIVATE);
            Editor editor = sharedpreferences.edit();
            editor.putString("userHotelSearchSesionId", sessionId);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static String getHotelSearchSesionId(Context context) {
        String userDocumentSearchSesionId = "";
        try {
            SharedPreferences sharedpreferences = context.getSharedPreferences(
                    "AppPref", Context.MODE_PRIVATE);
            userDocumentSearchSesionId = sharedpreferences.getString("userHotelSearchSesionId","0");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userDocumentSearchSesionId;
    }


    public static void storeTimestamp(Context mContext, String time_stamp) {
        try {
            SharedPreferences sharedpreferences = mContext
                    .getSharedPreferences("AppPref", Context.MODE_PRIVATE);
            Editor editor = sharedpreferences.edit();
            editor.putString("time_stamp_string", time_stamp);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getTimestamp(Context mContext) {
        String time_stamp = "";
        try {
            SharedPreferences sharedpreferences = mContext
                    .getSharedPreferences("AppPref", Context.MODE_PRIVATE);
            time_stamp = sharedpreferences.getString("time_stamp_string", "");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return time_stamp;
    }

    public static void storeTimestampValue(Context mContext, Long time_stamp_value) {
        try {
            SharedPreferences sharedpreferences = mContext
                    .getSharedPreferences("AppPref", Context.MODE_PRIVATE);
            Editor editor = sharedpreferences.edit();
            editor.putLong("time_stamp_value", time_stamp_value);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Long getTimestampValue(Context mContext) {
        Long time_stamp_value = 0L;
        try {
            SharedPreferences sharedpreferences = mContext
                    .getSharedPreferences("AppPref", Context.MODE_PRIVATE);
            time_stamp_value = sharedpreferences.getLong("time_stamp_value", 0L);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return time_stamp_value;
    }

    public static ArrayList<CriminalDetails> parseCrimiinalRecords(
            Context mContext, JSONArray jsonArray) {
        try {
            ArrayList<CriminalDetails> criminalList = new ArrayList<CriminalDetails>();


            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObj = jsonArray.optJSONObject(i);
                    CriminalDetails criminal = new CriminalDetails();




                    if (jsonObj.optString("FLAG") != "null")
                        criminal.setFlag("FLAG: " + jsonObj.optString("FLAG"));

                    JSONArray pic_array = jsonObj.getJSONArray("picture");

                    if (pic_array.length() > 0) {
                        List<String> pic_list = new ArrayList<>();
                        for (int j = 0; j < pic_array.length(); j++) {
                            pic_list.add(pic_array.optString(j));
                        }
                        criminal.setPicture_list(pic_list);
                    }

                    JSONArray briefMatch_array = jsonObj.getJSONArray("brief_match");
                    if (briefMatch_array.length() > 0) {
                        List<String> briefMatch_list = new ArrayList<>();
                        for (int j = 0; j < briefMatch_array.length(); j++) {
                            briefMatch_list.add(briefMatch_array.optString(j));
                        }
                        criminal.setBriefMatch_list(briefMatch_list);
                    }


                    if (jsonObj.optString("PS") != "null")
                        criminal.setPs("PS: " + jsonObj.optString("PS"));
                    if (jsonObj.optString("CASENO") != "null")
                        criminal.setCaseNo("CASENO: " + jsonObj.optString("CASENO"));
                    /*if(jsonObj.optString("NAME_ACCUSED")!="null")
                    criminal.setNameAccused("NAME_ACCUSED: " + jsonObj.optString("NAME_ACCUSED"));*/
                    if (jsonObj.optString("NAME") != "null")
                        criminal.setNameAccused("NAME_ACCUSED: " + jsonObj.optString("NAME"));
                    String alias = "Alias(es)^";
                    String father_name = "Father's Name^";
                    String age = "D.O.B./Age^";
                    String sex = "Sex^";
                    String nationality = "Nationality^";
                    String religion = "Religion^";
                    if (jsonObj.optString("PROV_CRM_NO") != null && !jsonObj.optString("PROV_CRM_NO").equalsIgnoreCase("") && !jsonObj.optString("PROV_CRM_NO").equalsIgnoreCase("null")){
                        //prv_crm_no = prv_crm_no + jsonObj.optString("PROV_CRM_NO");
                        criminal.setProvCrmNo(jsonObj.optString("PROV_CRM_NO"));
                        criminal.setProv_crm_no_for_image(jsonObj.optString("PROV_CRM_NO"));
                    }

                    if (jsonObj.optString("ANAME1") != null && !jsonObj.optString("ANAME1").equalsIgnoreCase("") && !jsonObj.optString("ANAME1").equalsIgnoreCase("null")) {
                        alias = alias + jsonObj.optString("ANAME1");
                    }
                    if (jsonObj.optString("ANAME2") != null && !jsonObj.optString("ANAME2").equalsIgnoreCase("") && !jsonObj.optString("ANAME2").equalsIgnoreCase("null")) {
                        alias = alias + "/ " + jsonObj.optString("ANAME2");
                    }
                    if (jsonObj.optString("ANAME3") != null && !jsonObj.optString("ANAME3").equalsIgnoreCase("") && !jsonObj.optString("ANAME3").equalsIgnoreCase("null")) {
                        alias = alias + "/ " + jsonObj.optString("ANAME3");
                    }
                    if (jsonObj.optString("ANAME4") != null && !jsonObj.optString("ANAME4").equalsIgnoreCase("") && !jsonObj.optString("ANAME4").equalsIgnoreCase("null")) {
                        alias = alias + "/ " + jsonObj.optString("ANAME4");
                    }


                    criminal.setAliasName(alias);


                    String criminal_alias = "";

                    if (jsonObj.optString("ANAME1") != null && !jsonObj.optString("ANAME1").equalsIgnoreCase("") && !jsonObj.optString("ANAME1").equalsIgnoreCase("null")) {
                        criminal_alias = " @" + jsonObj.optString("ANAME1");
                    }
                    if (jsonObj.optString("ANAME2") != null && !jsonObj.optString("ANAME2").equalsIgnoreCase("") && !jsonObj.optString("ANAME2").equalsIgnoreCase("null")) {
                        criminal_alias = criminal_alias + "/ " + jsonObj.optString("ANAME2");
                    }
                    if (jsonObj.optString("ANAME3") != null && !jsonObj.optString("ANAME3").equalsIgnoreCase("") && !jsonObj.optString("ANAME3").equalsIgnoreCase("null")) {
                        criminal_alias = criminal_alias + "/ " + jsonObj.optString("ANAME3");
                    }
                    if (jsonObj.optString("ANAME4") != null && !jsonObj.optString("ANAME4").equalsIgnoreCase("") && !jsonObj.optString("ANAME4").equalsIgnoreCase("null")) {
                        criminal_alias = criminal_alias + "/ " + jsonObj.optString("ANAME4");
                    }

                    criminal.setCriminal_alias(criminal_alias);


					/*if(jsonObj.optString("picture")!="null")
                    criminal.setPictureUrl(jsonObj.optString("picture"));*/
                    /*if(jsonObj.optString("ADDR_ACCUSED")!="null")
                    criminal.setAddressAccused("ADDR_ACCUSED: " + jsonObj.optString("ADDR_ACCUSED"));*/
                    if (jsonObj.optString("ADDRESS") != "null")
                        criminal.setAddressAccused("ADDR_ACCUSED: " + jsonObj.optString("ADDRESS"));

                    if (jsonObj.optString("SEX_ACCUSED") != "null")
                        criminal.setSexAccused("SEX_ACCUSED: " + jsonObj.optString("SEX_ACCUSED"));
                    if (jsonObj.optString("AGE_ACCUSED") != "null")
                        criminal.setAge("AGE_ACCUSED: " + jsonObj.optString("AGE_ACCUSED"));
                    if (jsonObj.optString("FATHER_ACCUSED") != "null")
                        criminal.setFatherAccused("FATHER_ACCUSED: " + jsonObj.optString("FATHER_ACCUSED"));
                    if (jsonObj.optString("FR_CHGNO") != "null")
                        criminal.setFrChgno("FR_CHGNO: " + jsonObj.optString("FR_CHGNO"));
                    if (jsonObj.optString("CHGYEAR") != "null")
                        criminal.setChgyear("CHGYEAR: " + jsonObj.optString("CHGYEAR"));
                    if (jsonObj.optString("RELIGION") != "null")
                        criminal.setReligion("RELIGION: " + jsonObj.optString("RELIGION"));
                    if (jsonObj.optString("SC_ST_OBC") != "null")
                        criminal.setScStObc("SC_ST_OB0C: " + jsonObj.optString("SC_ST_OBC"));
                    if (jsonObj.optString("OCCUPATION") != "null")
                        criminal.setOccupation("OCCUPATION: " + jsonObj.optString("OCCUPATION"));

                    if (jsonObj.optString("REG_CRM_NO") != "null")
                        criminal.setRegCrmNo("REG_CRM_NO: " + jsonObj.optString("REG_CRM_NO"));
                    if (jsonObj.optString("DT_ARREST") != "null")
                        criminal.setDtArrest("DT_ARREST: " + jsonObj.optString("DT_ARREST"));
                    if (jsonObj.optString("DT_RELEASE_BAIL") != "null")
                        criminal.setDtReleaseBail("DT_RELEASE_BAIL: " + jsonObj.optString("DT_RELEASE_BAIL"));
                    if (jsonObj.optString("DT_FORWARD_COURT") != "null")
                        criminal.setDtForwardCourt("DT_FORWARD_COURT: " + jsonObj.optString("DT_FORWARD_COURT"));
                    if (jsonObj.optString("ACTS_SECTIONS") != "null")
                        criminal.setActsSections("ACTS_SECTIONS: " + jsonObj.optString("ACTS_SECTIONS"));
                    if (jsonObj.optString("ACTS_SECTIONS") != "null")
                        criminal.setNamesSureties("NAMES_SURETIES: " + jsonObj.optString("NAMES_SURETIES"));
                    if (jsonObj.optString("ADDRESS_SURETIES") != "null")
                        criminal.setAddressSureties("ADDRESS_SURETIES: " + jsonObj.optString("ADDRESS_SURETIES"));
                    if (jsonObj.optString("PREV_CONVC_CASEREF") != "null")
                        criminal.setPrevConvcCaseref("PREV_CONVC_CASEREF: " + jsonObj.optString("PREV_CONVC_CASEREF"));
                    if (jsonObj.optString("STATUS") != "null")
                        criminal.setStatus("STATUS: " + jsonObj.optString("STATUS"));
                    if (jsonObj.optString("SUSPICION_APPVD") != "null")
                        criminal.setSuspicionAppvd("SUSPICION_APPVD: " + jsonObj.optString("SUSPICION_APPVD"));
                    if (jsonObj.optString("CASE_YR") != "null")
                        criminal.setCaseYr("CASE_YR: " + jsonObj.optString("CASE_YR"));
                    if (jsonObj.optString("SLNO") != "null")
                        criminal.setSlno("SLNO: " + jsonObj.optString("SLNO"));
                    if (jsonObj.optString("FIRCAT") != "null")
                        criminal.setFircat("FIRCAT: " + jsonObj.optString("FIRCAT"));
                    if (jsonObj.optString("FATHER_HUSBAND") != "null")
                        criminal.setFatherHusband("FATHER_HUSBAND: " + jsonObj.optString("FATHER_HUSBAND"));
                    if (jsonObj.optString("CRSGENERALID") != "null")
                        criminal.setCrsgeneralid("CRSGENERALID: " + jsonObj.optString("CRSGENERALID"));
                    if (jsonObj.optString("F_ADD") != "null")
                        criminal.setFadd("F_ADD: " + jsonObj.optString("F_ADD"));
                    if (jsonObj.optString("F_OCCU") != "null")
                        criminal.setFoccu("F_OCCU: " + jsonObj.optString("F_OCCU"));
                    if (jsonObj.optString("BIRTHYR") != "null")
                        criminal.setBirthyr("BIRTHYR: " + jsonObj.optString("BIRTHYR"));
                    if (jsonObj.optString("HEIGHT_FEET") != "null")
                        criminal.setHeightFeet("HEIGHT_FEET: " + jsonObj.optString("HEIGHT_FEET"));
                    if (jsonObj.optString("HEIGHT_INCH") != "null")
                        criminal.setHeightInch("HEIGHT_INCH: " + jsonObj.optString("HEIGHT_INCH"));
                    if (jsonObj.optString("PSROAD_PSPLACE") != "null")
                        criminal.setPsRoadPsplace("PSROAD_PSPLACE: " + jsonObj.optString("PSROAD_PSPLACE"));
                    if (jsonObj.optString("PSPS_KPJ") != "null")
                        criminal.setPspsKpj("PSPS_KPJ: " + jsonObj.optString("PSPS_KPJ"));
                    if (jsonObj.optString("PSPS_OUT") != "null")
                        criminal.setPspsOut("PSPS_OUT: " + jsonObj.optString("PSPS_OUT"));
                    if (jsonObj.optString("PSDIST") != "null")
                        criminal.setPsdist("PSDIST: " + jsonObj.optString("PSDIST"));
                    if (jsonObj.optString("PSDIST_OUT") != "null")
                        criminal.setPsdistOut("PSDIST_OUT: " + jsonObj.optString("PSDIST_OUT"));
                    if (jsonObj.optString("PSPIN") != "null")
                        criminal.setPspin("PSPIN: " + jsonObj.optString("PSPIN"));
                    if (jsonObj.optString("PSSTATE") != "null")
                        criminal.setPsstate("PSSTATE: " + jsonObj.optString("PSSTATE"));
                    if (jsonObj.optString("PRROAD_PRPLACE") != "null")
                        criminal.setPrroadPreplace("PRROAD_PRPLACE: " + jsonObj.optString("PRROAD_PRPLACE"));
                    if (jsonObj.optString("PRPS_KPJ") != "null")
                        criminal.setPrpsKpj("PRPS_KPJ: " + jsonObj.optString("PRPS_KPJ"));
                    if (jsonObj.optString("PRPS_KPJ") != "null")
                        criminal.setPrpsOut("PRPS_OUT: " + jsonObj.optString("PRPS_OUT"));
                    if (jsonObj.optString("PRDIST") != "null")
                        criminal.setPrdist("PRDIST: " + jsonObj.optString("PRDIST"));
                    if (jsonObj.optString("PRDIST_OUT") != "null")
                        criminal.setPrdistOut("PRDIST_OUT: " + jsonObj.optString("PRDIST_OUT"));
                    if (jsonObj.optString("PRPIN") != "null")
                        criminal.setPrpin("PRPIN: " + jsonObj.optString("PRPIN"));
                    if (jsonObj.optString("PRSTATE") != "null")
                        criminal.setPrstate("PRSTATE: " + jsonObj.optString("PRSTATE"));
                    if (jsonObj.optString("CLASS") != "null")
                        criminal.setClass("CLASS: " + jsonObj.optString("CLASS"));
                    if (jsonObj.optString("SUBCLASS") != "null")
                        criminal.setSubclass("SUBCLASS: " + jsonObj.optString("SUBCLASS"));
                    if (jsonObj.optString("PROPERTY") != "null")
                        criminal.setProperty("PROPERTY: " + jsonObj.optString("PROPERTY"));
                    if (jsonObj.optString("TRANSPORT") != "null")
                        criminal.setTransport("TRANSPORT: " + jsonObj.optString("TRANSPORT"));
                    if (jsonObj.optString("GROUND") != "null")
                        criminal.setGround("GROUND: " + jsonObj.optString("GROUND"));
                    if (jsonObj.optString("HSNO") != "null")
                        criminal.setHsno("HSNO: " + jsonObj.optString("HSNO"));
                    if (jsonObj.optString("PRODDATE") != "null")
                        criminal.setProddate("PRODDATE: " + jsonObj.optString("PRODDATE"));
                    if (jsonObj.optString("BEARD") != "null")
                        criminal.setBeard("BEARD: " + jsonObj.optString("BEARD"));
                    if (jsonObj.optString("BIRTHMARK") != "null")
                        criminal.setBirthmark("BIRTHMARK: " + jsonObj.optString("BIRTHMARK"));
                    if (jsonObj.optString("BUILT") != "null")
                        criminal.setBuilt("BUILT: " + jsonObj.optString("BUILT"));
                    if (jsonObj.optString("BURNMARK") != "null")
                        criminal.setBurnmark("BURNMARK: " + jsonObj.optString("BURNMARK"));
                    if (jsonObj.optString("COMPLEXION") != "null")
                        criminal.setComplexion("COMPLEXION: " + jsonObj.optString("COMPLEXION"));
                    if (jsonObj.optString("CUTMARK") != "null")
                        criminal.setCutmark("CUTMARK: " + jsonObj.optString("CUTMARK"));
                    if (jsonObj.optString("DEFORMITY") != "null")
                        criminal.setDeformity("DEFORMITY: " + jsonObj.optString("DEFORMITY"));
                    if (jsonObj.optString("EAR") != "null")
                        criminal.setEar("EAR: " + jsonObj.optString("EAR"));
                    if (jsonObj.optString("EYE") != "null")
                        criminal.setEye("EYE: " + jsonObj.optString("EYE"));
                    if (jsonObj.optString("EYE") != "null")
                        criminal.setEyebrow("EYEBROW: " + jsonObj.optString("EYEBROW"));
                    if (jsonObj.optString("FACE") != "null")
                        criminal.setFace("FACE: " + jsonObj.optString("FACE"));
                    if (jsonObj.optString("FOREHEAD") != "null")
                        criminal.setForehead("FOREHEAD: " + jsonObj.optString("FOREHEAD"));
                    if (jsonObj.optString("HAIR") != "null")
                        criminal.setHair("HAIR: " + jsonObj.optString("HAIR"));
                    if (jsonObj.optString("MOLE") != "null")
                        criminal.setMole("MOLE: " + jsonObj.optString("MOLE"));
                    if (jsonObj.optString("MOUSTACHE") != "null")
                        criminal.setMoustache("MOUSTACHE " + jsonObj.optString("MOUSTACHE"));
                    if (jsonObj.optString("NOSE") != "null")
                        criminal.setNose("NOSE: " + jsonObj.optString("NOSE"));
                    if (jsonObj.optString("SCARMARK") != "null")
                        criminal.setScarmark("SCARMARK: " + jsonObj.optString("SCARMARK"));
                    if (jsonObj.optString("TATTOOMARK") != "null")
                        criminal.setTattoomark("TATTOOMARK: " + jsonObj.optString("TATTOOMARK"));
                    if (jsonObj.optString("WARTMARK") != "null")
                        criminal.setWartMark("WARTMARK: " + jsonObj.optString("WARTMARK"));
                    if (jsonObj.optString("DIVISION") != "null")
                        criminal.setDivision("DIVISION: " + jsonObj.optString("DIVISION"));
                    if (jsonObj.optString("CRIMENO") != "null")
                        criminal.setCrimeno("CRIMENO: " + jsonObj.optString("CRIMENO"));
                    if (jsonObj.optString("OCCURNO") != "null")
                        criminal.setOccurno("OCCURNO: " + jsonObj.optString("OCCURNO"));
                    if (jsonObj.optString("TARGET") != "null")
                        criminal.setTarget("TARGET: " + jsonObj.optString("TARGET"));
                    if (jsonObj.optString("IDENTIFIER") != "null")
                        criminal.setIdentifier("IDENTIFIER: " + jsonObj.optString("IDENTIFIER"));
                    if (jsonObj.optString("ROUGHSEC") != "null")
                        criminal.setRoughsec("ROUGHSEC: " + jsonObj.optString("ROUGHSEC"));
                    if (jsonObj.optString("ROUGHPART") != "null")
                        criminal.setRoughpart("ROUGHPART: " + jsonObj.optString("ROUGHPART"));
                    if (jsonObj.optString("PHOTONO") != "null")
                        criminal.setPhotono("PHOTONO: " + jsonObj.optString("PHOTONO"));
                    if (jsonObj.optString("TTCRIMINALID") != "null")
                        criminal.setTtcriminallid("TTCRIMINALID: " + jsonObj.optString("TTCRIMINALID"));
                    if (jsonObj.optString("TRNID") != "null")
                        criminal.setTrnid("TRNID: " + jsonObj.optString("TRNID"));
                    if (jsonObj.optString("PSCODE") != "null")
                        criminal.setPs_code("PSCODE: " + jsonObj.optString("PSCODE"));
                    if (jsonObj.optString("WA_YEAR") != "null")
                        criminal.setWa_year("WA_YEAR: " + jsonObj.optString("WA_YEAR"));
                    if (jsonObj.optString("WASLNO") != "null")
                        criminal.setWaslno("WASLNO: " + jsonObj.optString("WASLNO"));
                    if (jsonObj.optString("CRIMENO") != "null")
                        criminal.setCrime_no("CRIMENO: " + jsonObj.optString("CRIMENO"));
                    if (jsonObj.optString("CRIMEYEAR") != "null")
                        criminal.setCrime_year("CRIMEYEAR: " + jsonObj.optString("CRIMEYEAR"));
                    if (jsonObj.optString("CASENO") != "null")
                        criminal.setCase_no("CASENO: " + jsonObj.optString("CASENO"));
                    if (jsonObj.optString("CASEYR") != "null")
                        criminal.setCase_year("CASEYR: " + jsonObj.optString("CASEYR"));
                    if (jsonObj.optString("SLNO") != "null")
                        criminal.setSl_no("SLNO: " + jsonObj.optString("SLNO"));
                    if (jsonObj.optString("FATHERNAME") != "null") {
                        father_name = father_name + jsonObj.optString("FATHERNAME");
                    }

                    criminal.setFatherName(father_name);
                  /*  if (jsonObj.optString("PROV_CRM_NO") != "null") {
                        prv_crm_no = prv_crm_no + jsonObj.optString("PROV_CRM_NO");
                    }
                     criminal.setProv_crm_no(prv_crm_no);
*/
                    if (jsonObj.optString("AGE") != "null") {
                        age = age + jsonObj.optString("AGE");
                    }

                    criminal.setDob_age(age);

                    if (jsonObj.optString("SEX") != "null") {
                        sex = sex + jsonObj.optString("SEX");
                    }

                    criminal.setSex(sex);

                    if (jsonObj.optString("NATIONALITY") != "null") {

                        nationality = nationality + jsonObj.optString("NATIONALITY");

                    }
                    criminal.setNationality(nationality);


                    if (jsonObj.optString("RELIGION") != "null") {

                        religion = religion + jsonObj.optString("RELIGION");

                    }

                    criminal.setReligion(religion);

                    if (jsonObj.optString("CRSGENERALID") != "null") {
                        criminal.setCrs_generalID(jsonObj.optString("CRSGENERALID"));
                    }

                    criminalList.add(criminal);




					/*criminal.setAge(jsonObj.optString("age"));
                    criminal.setAliasName(jsonObj
							.optString("criminal_aliases_name"));
					criminal.setBirthPlace(jsonObj.optString("place_of_birth"));
					criminal.setCaseReferences(jsonObj
							.optString("case_references"));
					criminal.setCast(jsonObj.optString("cast"));
					criminal.setCriminalName(jsonObj.optString("criminal_name"));
					criminal.setDob(jsonObj.optString("date_of_birth"));
					criminal.setFatherName(jsonObj.optString("father_name"));
					criminal.setId(jsonObj.optString("id"));
					criminal.setIdMark(jsonObj
							.optString("distinguishes_id_mark"));
					criminal.setImeiNo(jsonObj.optString("imei_no"));
					criminal.setMaritalStatus(jsonObj
							.optString("marital_status"));
					criminal.setMobileNo(jsonObj.optString("mobile_no"));
					criminal.setMotherDetails(jsonObj
							.optString("mothers_name_and_particulars"));
					criminal.setMotherToungue(jsonObj
							.optString("mother_tongue"));
					criminal.setOtherAddress(jsonObj
							.optString("other_known_address"));
					criminal.setPoliceStationId(jsonObj.optString("ps_id"));
					criminal.setPresentAddress(jsonObj
							.optString("present_residential_address"));

					criminal.setReligion(jsonObj.optString("religion"));
					criminal.setSpouseDetails(jsonObj
							.optString("name_of_spouse_and_particular"));

					criminal.setAddictionDetails(jsonObj
							.optString("particulars_of_addiction"));

					criminal.setAdvocateDetails(jsonObj
							.optString("particulars_of_advocate"));
					criminal.setAvgDailyIncome(jsonObj
							.optString("avg_daily_income"));
					criminal.setAvgMonthlyExpenses(jsonObj
							.optString("average_monthly_expenditure"));
					criminal.setBirthPlace(jsonObj.optString("place_of_birth"));
					criminal.setBrotherSisterDetails(jsonObj
							.optString("particulars_of_brothers_and_sisters"));
					criminal.setBrotherSisterInLawDetails(jsonObj
							.optString("particulars_of_brother_and_sister_in_law"));
					criminal.setCaseReferences(jsonObj
							.optString("case_references"));
					criminal.setEducation(jsonObj
							.optString("particulars_educational_qualification"));
					criminal.setElectricGasKerosineDetails(jsonObj
							.optString("particulars_of_electricity_gas_kerosin"));
					criminal.setElectronicsCommunicationDetails(jsonObj
							.optString("particulars_of_electronics_and_communication"));
					criminal.setFamilyDoctorHospitalDetails(jsonObj
							.optString("particulars_of_family_physician_doctor_hospital"));
					criminal.setFoodHabbitDetails(jsonObj
							.optString("particular_of_food_habit"));
					criminal.setMarriageDate(jsonObj
							.optString("date_of_marriage"));
					criminal.setMostVisitedShop(jsonObj
							.optString("particulars_of_mostly_visited_shop_market"));
					criminal.setMostVisitedWorshipDtails(jsonObj
							.optString("particulars_of_mostly_visited_worship"));
					criminal.setMotherDetails(jsonObj
							.optString("mothers_name_and_particulars"));
					criminal.setOtherAddress(jsonObj
							.optString("other_known_address"));
					criminal.setOtherLanguageDetails(jsonObj
							.optString("particulars_of_otherlanguage"));
					criminal.setOtherRelativeDetails(jsonObj
							.optString("particulars_of_other_relatives"));
					criminal.setSpouseDetails(jsonObj
							.optString("name_of_spouse_and_particular"));
					criminal.setSondaughterDetais(jsonObj
							.optString("particulars_of_son_and_daughter"));

					criminal.setPictureUrl(jsonObj.optString("picture"));
					criminal.setAdvocateDetails(jsonObj
							.optString("particulars_of_advocate"));
					criminal.setAvgDailyIncome(jsonObj
							.optString("avg_daily_income"));
					criminal.setAvgMonthlyExpenses(jsonObj
							.optString("average_monthly_expenditure"));
					criminal.setBnkPostTransactions(jsonObj
							.optString("particulars_of_bank_post_office_transaction"));
					criminal.setCoAccusedDetyails(jsonObj
							.optString("particulars_of_co_accused"));
					criminal.setDisposalValuables(jsonObj
							.optString("particulars_of_disposal_valuables"));
					criminal.setEducation(jsonObj
							.optString("particulars_educational_qualification"));
					criminal.setFamilyDoctorHospitalDetails(jsonObj
							.optString("particulars_of_family_physician_doctor_hospital"));
					criminal.setFatherName(jsonObj
							.optString("particulars_of_addiction"));
					criminal.setFavColor(jsonObj.optString("favourite_colour"));
					criminal.setFavDress(jsonObj
							.optString("particulars_of_addiction"));
					criminal.setFavDress(jsonObj.optString("favourite_dress"));
					criminal.setFavFood(jsonObj.optString("favourite_food"));
					criminal.setFavRecreation(jsonObj
							.optString("favourite_recreation"));
					criminal.setFoodHabbitDetails(jsonObj
							.optString("favourite_recreation"));
					criminal.setFavRecreation(jsonObj
							.optString("particular_of_food_habit"));
					criminal.setForeignContacts(jsonObj
							.optString("foreign_contacts"));
					criminal.setFriendRelativeOutWb(jsonObj
							.optString("particularsof_friends_relatives_staying_outside_wb"));
					criminal.setGuaranterDetails(jsonObj
							.optString("particulars_of_guarantor"));
					criminal.setIdProofDetails(jsonObj
							.optString("particulars_of_id_proof"));
					criminal.setLoanMortgageDetails(jsonObj
							.optString("particulars_of_loan_and_mortgage"));
					criminal.setLoveAffairsDetails(jsonObj
							.optString("details_of_love_affairs_illicit_relation"));
					criminal.setMaritalStatus(jsonObj
							.optString("marital_status"));
					criminal.setMarriageDate(jsonObj
							.optString("date_of_marriage"));
					criminal.setMostVisitedPlace(jsonObj
							.optString("particulars_of_mostly_visited_place_outside_wb"));
					criminal.setMostVisitedShop(jsonObj
							.optString("particulars_of_mostly_visited_shop_market"));
					criminal.setOfficerFacedEarlier(jsonObj
							.optString("particulars_of_police_officers_faced_earlier"));
					criminal.setOtherRelativeDetails(jsonObj
							.optString("particulars_of_other_relatives"));
					criminal.setPresentArrestDetails(jsonObj
							.optString("particulars_of_present_arrest"));
					criminal.setPresentPosition(jsonObj
							.optString("present_position"));
					criminal.setPrevConnections(jsonObj
							.optString("particulars_of_previous_connection"));
					criminal.setPrevInvolvement(jsonObj
							.optString("particulars_of_previous_involvements_in_criminal_cases"));
					criminal.setPreviousArrests(jsonObj
							.optString("particulars_of_previous_arrest"));
					criminal.setProfession(jsonObj.optString("profession"));
					criminal.setPropertyDetails(jsonObj
							.optString("particulars_of_movable_and_immovable_property"));
					criminal.setReligion(jsonObj.optString("religion"));
					criminal.setRevealingStatement(jsonObj
							.optString("brief_of_revealing_statement"));
					criminal.setSpouseDailyIncome(jsonObj
							.optString("avg_daily_income_of_spouse"));
					criminal.setSpouseEducation(jsonObj
							.optString("educational_qualification_of_spouse"));
					// criminal.setSpouseNameAndParticular(jsonObj
					// .optString("favourite_recreation"));
					criminal.setSpouseProfession(jsonObj
							.optString("profession_of_spouse"));
					criminal.setStayPcJcDetails(jsonObj
							.optString("particulars_of_staying_pc_jc"));
					criminal.setUsedVehicleDetails(jsonObj
							.optString("particulars_of_used_vehicles"));
					criminal.setVisitedBarDetails(jsonObj
							.optString("particulars_of_visited_bar"));
					criminal.setWifeChildrendiseasesDetails(jsonObj
							.optString("particulars_of_disease_of_wife_children"));*/


                }
            }
            // }
            // Toast.makeText(mContext, jObj.optString("message"),
            // Toast.LENGTH_LONG).show();
            // }
            return criminalList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //------------------------------------------------------------------------------------//


    public static ArrayList<CriminalDetails> parseCrimiinalAssociateRecords(
            Context mContext, JSONArray jsonArray) {
        try {
            ArrayList<CriminalDetails> criminalList = new ArrayList<CriminalDetails>();


            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObj = jsonArray.optJSONObject(i);
                    L.e("ACCUSSED & ARRESSTED obj:"+jsonObj);
                    CriminalDetails criminal = new CriminalDetails();

                    if (jsonObj.optString("FLAG") != "null")
                        criminal.setFlag("FLAG: " + jsonObj.optString("FLAG"));

                    JSONArray pic_array = jsonObj.optJSONArray("picture");
                    if(pic_array!=null) {
                        if (pic_array.length() > 0) {
                            List<String> pic_list = new ArrayList<>();
                            for (int j = 0; j < pic_array.length(); j++) {
                                pic_list.add(pic_array.optString(j));
                            }
                            criminal.setPicture_list(pic_list);
                        }
                    }
                    if (jsonObj.optString("PS") != "null")
                        criminal.setPs("PS: " + jsonObj.optString("PS"));
                    if (jsonObj.optString("CASENO") != "null")
                        criminal.setCaseNo("CASENO: " + jsonObj.optString("CASENO"));
                    /*if(jsonObj.optString("NAME_ACCUSED")!="null")
                    criminal.setNameAccused("NAME_ACCUSED: " + jsonObj.optString("NAME_ACCUSED"));*/
                    if(jsonObj.has("NAME")) {
                        if (jsonObj.optString("NAME") != "null")
                            criminal.setNameAccused("NAME_ACCUSED: " + jsonObj.optString("NAME"));
                    }
                    else{
                        if (jsonObj.optString("ARRESTEE") != "null")
                            criminal.setNameAccused("NAME_ACCUSED: " + jsonObj.optString("ARRESTEE"));

                    }
                    String alias = "Alias(es)^";
                    String father_name = "Father's Name^";
                    String age = "D.O.B./Age^";
                    String sex = "Sex^";
                    String nationality = "Nationality^";
                    String religion = "Religion^";

                    if (jsonObj.optString("ANAME1") != null && !jsonObj.optString("ANAME1").equalsIgnoreCase("") && !jsonObj.optString("ANAME1").equalsIgnoreCase("null")) {
                        alias = alias + jsonObj.optString("ANAME1");
                    }
                    if (jsonObj.optString("ANAME2") != null && !jsonObj.optString("ANAME2").equalsIgnoreCase("") && !jsonObj.optString("ANAME2").equalsIgnoreCase("null")) {
                        alias = alias + "/ " + jsonObj.optString("ANAME2");
                    }
                    if (jsonObj.optString("ANAME3") != null && !jsonObj.optString("ANAME3").equalsIgnoreCase("") && !jsonObj.optString("ANAME3").equalsIgnoreCase("null")) {
                        alias = alias + "/ " + jsonObj.optString("ANAME3");
                    }
                    if (jsonObj.optString("ANAME4") != null && !jsonObj.optString("ANAME4").equalsIgnoreCase("") && !jsonObj.optString("ANAME4").equalsIgnoreCase("null")) {
                        alias = alias + "/ " + jsonObj.optString("ANAME4");
                    }


                    criminal.setAliasName(alias);


                    String criminal_alias = "";

                    if (jsonObj.optString("ANAME1") != null && !jsonObj.optString("ANAME1").equalsIgnoreCase("") && !jsonObj.optString("ANAME1").equalsIgnoreCase("null")) {
                        criminal_alias = " @" + jsonObj.optString("ANAME1");
                    }
                    if (jsonObj.optString("ANAME2") != null && !jsonObj.optString("ANAME2").equalsIgnoreCase("") && !jsonObj.optString("ANAME2").equalsIgnoreCase("null")) {
                        criminal_alias = criminal_alias + "/ " + jsonObj.optString("ANAME2");
                    }
                    if (jsonObj.optString("ANAME3") != null && !jsonObj.optString("ANAME3").equalsIgnoreCase("") && !jsonObj.optString("ANAME3").equalsIgnoreCase("null")) {
                        criminal_alias = criminal_alias + "/ " + jsonObj.optString("ANAME3");
                    }
                    if (jsonObj.optString("ANAME4") != null && !jsonObj.optString("ANAME4").equalsIgnoreCase("") && !jsonObj.optString("ANAME4").equalsIgnoreCase("null")) {
                        criminal_alias = criminal_alias + "/ " + jsonObj.optString("ANAME4");
                    }

                    criminal.setCriminal_alias(criminal_alias);

                    if (jsonObj.optString("ADDRESS") != "null")
                        criminal.setAddressAccused("ADDR_ACCUSED: " + jsonObj.optString("ADDRESS"));

                    if (jsonObj.optString("SEX_ACCUSED") != "null")
                        criminal.setSexAccused("SEX_ACCUSED: " + jsonObj.optString("SEX_ACCUSED"));
                    if (jsonObj.optString("AGE_ACCUSED") != "null")
                        criminal.setAge("AGE_ACCUSED: " + jsonObj.optString("AGE_ACCUSED"));
                    if (jsonObj.optString("FATHER_ACCUSED") != "null")
                        criminal.setFatherAccused("FATHER_ACCUSED: " + jsonObj.optString("FATHER_ACCUSED"));
                    if (jsonObj.optString("FR_CHGNO") != "null")
                        criminal.setFrChgno("FR_CHGNO: " + jsonObj.optString("FR_CHGNO"));
                    if (jsonObj.optString("CHGYEAR") != "null")
                        criminal.setChgyear("CHGYEAR: " + jsonObj.optString("CHGYEAR"));
                    if (jsonObj.optString("RELIGION") != "null")
                        criminal.setReligion("RELIGION: " + jsonObj.optString("RELIGION"));
                    if (jsonObj.optString("SC_ST_OBC") != "null")
                        criminal.setScStObc("SC_ST_OB0C: " + jsonObj.optString("SC_ST_OBC"));
                    if (jsonObj.optString("OCCUPATION") != "null")
                        criminal.setOccupation("OCCUPATION: " + jsonObj.optString("OCCUPATION"));

                    if (jsonObj.optString("PROV_CRM_NO") != "null" && !jsonObj.optString("PROV_CRM_NO").equalsIgnoreCase("") && jsonObj.optString("PROV_CRM_NO") != null) {
                        criminal.setProvCrmNo("PROV_CRM_NO: " + jsonObj.optString("PROV_CRM_NO"));
                        criminal.setProv_crm_no_for_image(jsonObj.optString("PROV_CRM_NO"));
                    }

                    if (jsonObj.optString("REG_CRM_NO") != "null")
                        criminal.setRegCrmNo("REG_CRM_NO: " + jsonObj.optString("REG_CRM_NO"));
                    if (jsonObj.optString("DT_ARREST") != "null")
                        criminal.setDtArrest("DT_ARREST: " + jsonObj.optString("DT_ARREST"));
                    if (jsonObj.optString("DT_RELEASE_BAIL") != "null")
                        criminal.setDtReleaseBail("DT_RELEASE_BAIL: " + jsonObj.optString("DT_RELEASE_BAIL"));
                    if (jsonObj.optString("DT_FORWARD_COURT") != "null")
                        criminal.setDtForwardCourt("DT_FORWARD_COURT: " + jsonObj.optString("DT_FORWARD_COURT"));
                    if (jsonObj.optString("ACTS_SECTIONS") != "null")
                        criminal.setActsSections("ACTS_SECTIONS: " + jsonObj.optString("ACTS_SECTIONS"));
                    if (jsonObj.optString("ACTS_SECTIONS") != "null")
                        criminal.setNamesSureties("NAMES_SURETIES: " + jsonObj.optString("NAMES_SURETIES"));
                    if (jsonObj.optString("ADDRESS_SURETIES") != "null")
                        criminal.setAddressSureties("ADDRESS_SURETIES: " + jsonObj.optString("ADDRESS_SURETIES"));
                    if (jsonObj.optString("PREV_CONVC_CASEREF") != "null")
                        criminal.setPrevConvcCaseref("PREV_CONVC_CASEREF: " + jsonObj.optString("PREV_CONVC_CASEREF"));
                    if (jsonObj.optString("STATUS") != "null")
                        criminal.setStatus("STATUS: " + jsonObj.optString("STATUS"));
                    if (jsonObj.optString("SUSPICION_APPVD") != "null")
                        criminal.setSuspicionAppvd("SUSPICION_APPVD: " + jsonObj.optString("SUSPICION_APPVD"));
                    if (jsonObj.optString("CASE_YR") != "null")
                        criminal.setCaseYr("CASE_YR: " + jsonObj.optString("CASE_YR"));
                    if (jsonObj.optString("SLNO") != "null")
                        criminal.setSlno("SLNO: " + jsonObj.optString("SLNO"));
                    if (jsonObj.optString("FIRCAT") != "null")
                        criminal.setFircat("FIRCAT: " + jsonObj.optString("FIRCAT"));
                    if (jsonObj.optString("FATHER_HUSBAND") != "null")
                        criminal.setFatherHusband("FATHER_HUSBAND: " + jsonObj.optString("FATHER_HUSBAND"));
                    if (jsonObj.optString("CRSGENERALID") != "null")
                        criminal.setCrsgeneralid("CRSGENERALID: " + jsonObj.optString("CRSGENERALID"));
                    if (jsonObj.optString("F_ADD") != "null")
                        criminal.setFadd("F_ADD: " + jsonObj.optString("F_ADD"));
                    if (jsonObj.optString("F_OCCU") != "null")
                        criminal.setFoccu("F_OCCU: " + jsonObj.optString("F_OCCU"));
                    if (jsonObj.optString("BIRTHYR") != "null")
                        criminal.setBirthyr("BIRTHYR: " + jsonObj.optString("BIRTHYR"));
                    if (jsonObj.optString("HEIGHT_FEET") != "null")
                        criminal.setHeightFeet("HEIGHT_FEET: " + jsonObj.optString("HEIGHT_FEET"));
                    if (jsonObj.optString("HEIGHT_INCH") != "null")
                        criminal.setHeightInch("HEIGHT_INCH: " + jsonObj.optString("HEIGHT_INCH"));
                    if (jsonObj.optString("PSROAD_PSPLACE") != "null")
                        criminal.setPsRoadPsplace("PSROAD_PSPLACE: " + jsonObj.optString("PSROAD_PSPLACE"));
                    if (jsonObj.optString("PSPS_KPJ") != "null")
                        criminal.setPspsKpj("PSPS_KPJ: " + jsonObj.optString("PSPS_KPJ"));
                    if (jsonObj.optString("PSPS_OUT") != "null")
                        criminal.setPspsOut("PSPS_OUT: " + jsonObj.optString("PSPS_OUT"));
                    if (jsonObj.optString("PSDIST") != "null")
                        criminal.setPsdist("PSDIST: " + jsonObj.optString("PSDIST"));
                    if (jsonObj.optString("PSDIST_OUT") != "null")
                        criminal.setPsdistOut("PSDIST_OUT: " + jsonObj.optString("PSDIST_OUT"));
                    if (jsonObj.optString("PSPIN") != "null")
                        criminal.setPspin("PSPIN: " + jsonObj.optString("PSPIN"));
                    if (jsonObj.optString("PSSTATE") != "null")
                        criminal.setPsstate("PSSTATE: " + jsonObj.optString("PSSTATE"));
                    if (jsonObj.optString("PRROAD_PRPLACE") != "null")
                        criminal.setPrroadPreplace("PRROAD_PRPLACE: " + jsonObj.optString("PRROAD_PRPLACE"));
                    if (jsonObj.optString("PRPS_KPJ") != "null")
                        criminal.setPrpsKpj("PRPS_KPJ: " + jsonObj.optString("PRPS_KPJ"));
                    if (jsonObj.optString("PRPS_KPJ") != "null")
                        criminal.setPrpsOut("PRPS_OUT: " + jsonObj.optString("PRPS_OUT"));
                    if (jsonObj.optString("PRDIST") != "null")
                        criminal.setPrdist("PRDIST: " + jsonObj.optString("PRDIST"));
                    if (jsonObj.optString("PRDIST_OUT") != "null")
                        criminal.setPrdistOut("PRDIST_OUT: " + jsonObj.optString("PRDIST_OUT"));
                    if (jsonObj.optString("PRPIN") != "null")
                        criminal.setPrpin("PRPIN: " + jsonObj.optString("PRPIN"));
                    if (jsonObj.optString("PRSTATE") != "null")
                        criminal.setPrstate("PRSTATE: " + jsonObj.optString("PRSTATE"));
                    if (jsonObj.optString("CLASS") != "null")
                        criminal.setClass("CLASS: " + jsonObj.optString("CLASS"));
                    if (jsonObj.optString("SUBCLASS") != "null")
                        criminal.setSubclass("SUBCLASS: " + jsonObj.optString("SUBCLASS"));
                    if (jsonObj.optString("PROPERTY") != "null")
                        criminal.setProperty("PROPERTY: " + jsonObj.optString("PROPERTY"));
                    if (jsonObj.optString("TRANSPORT") != "null")
                        criminal.setTransport("TRANSPORT: " + jsonObj.optString("TRANSPORT"));
                    if (jsonObj.optString("GROUND") != "null")
                        criminal.setGround("GROUND: " + jsonObj.optString("GROUND"));
                    if (jsonObj.optString("HSNO") != "null")
                        criminal.setHsno("HSNO: " + jsonObj.optString("HSNO"));
                    if (jsonObj.optString("PRODDATE") != "null")
                        criminal.setProddate("PRODDATE: " + jsonObj.optString("PRODDATE"));
                    if (jsonObj.optString("BEARD") != "null")
                        criminal.setBeard("BEARD: " + jsonObj.optString("BEARD"));
                    if (jsonObj.optString("BIRTHMARK") != "null")
                        criminal.setBirthmark("BIRTHMARK: " + jsonObj.optString("BIRTHMARK"));
                    if (jsonObj.optString("BUILT") != "null")
                        criminal.setBuilt("BUILT: " + jsonObj.optString("BUILT"));
                    if (jsonObj.optString("BURNMARK") != "null")
                        criminal.setBurnmark("BURNMARK: " + jsonObj.optString("BURNMARK"));
                    if (jsonObj.optString("COMPLEXION") != "null")
                        criminal.setComplexion("COMPLEXION: " + jsonObj.optString("COMPLEXION"));
                    if (jsonObj.optString("CUTMARK") != "null")
                        criminal.setCutmark("CUTMARK: " + jsonObj.optString("CUTMARK"));
                    if (jsonObj.optString("DEFORMITY") != "null")
                        criminal.setDeformity("DEFORMITY: " + jsonObj.optString("DEFORMITY"));
                    if (jsonObj.optString("EAR") != "null")
                        criminal.setEar("EAR: " + jsonObj.optString("EAR"));
                    if (jsonObj.optString("EYE") != "null")
                        criminal.setEye("EYE: " + jsonObj.optString("EYE"));
                    if (jsonObj.optString("EYE") != "null")
                        criminal.setEyebrow("EYEBROW: " + jsonObj.optString("EYEBROW"));
                    if (jsonObj.optString("FACE") != "null")
                        criminal.setFace("FACE: " + jsonObj.optString("FACE"));
                    if (jsonObj.optString("FOREHEAD") != "null")
                        criminal.setForehead("FOREHEAD: " + jsonObj.optString("FOREHEAD"));
                    if (jsonObj.optString("HAIR") != "null")
                        criminal.setHair("HAIR: " + jsonObj.optString("HAIR"));
                    if (jsonObj.optString("MOLE") != "null")
                        criminal.setMole("MOLE: " + jsonObj.optString("MOLE"));
                    if (jsonObj.optString("MOUSTACHE") != "null")
                        criminal.setMoustache("MOUSTACHE " + jsonObj.optString("MOUSTACHE"));
                    if (jsonObj.optString("NOSE") != "null")
                        criminal.setNose("NOSE: " + jsonObj.optString("NOSE"));
                    if (jsonObj.optString("SCARMARK") != "null")
                        criminal.setScarmark("SCARMARK: " + jsonObj.optString("SCARMARK"));
                    if (jsonObj.optString("TATTOOMARK") != "null")
                        criminal.setTattoomark("TATTOOMARK: " + jsonObj.optString("TATTOOMARK"));
                    if (jsonObj.optString("WARTMARK") != "null")
                        criminal.setWartMark("WARTMARK: " + jsonObj.optString("WARTMARK"));
                    if (jsonObj.optString("DIVISION") != "null")
                        criminal.setDivision("DIVISION: " + jsonObj.optString("DIVISION"));
                    if (jsonObj.optString("CRIMENO") != "null")
                        criminal.setCrimeno("CRIMENO: " + jsonObj.optString("CRIMENO"));
                    if (jsonObj.optString("OCCURNO") != "null")
                        criminal.setOccurno("OCCURNO: " + jsonObj.optString("OCCURNO"));
                    if (jsonObj.optString("TARGET") != "null")
                        criminal.setTarget("TARGET: " + jsonObj.optString("TARGET"));
                    if (jsonObj.optString("IDENTIFIER") != "null")
                        criminal.setIdentifier("IDENTIFIER: " + jsonObj.optString("IDENTIFIER"));
                    if (jsonObj.optString("ROUGHSEC") != "null")
                        criminal.setRoughsec("ROUGHSEC: " + jsonObj.optString("ROUGHSEC"));
                    if (jsonObj.optString("ROUGHPART") != "null")
                        criminal.setRoughpart("ROUGHPART: " + jsonObj.optString("ROUGHPART"));
                    if (jsonObj.optString("PHOTONO") != "null")
                        criminal.setPhotono("PHOTONO: " + jsonObj.optString("PHOTONO"));
                    if (jsonObj.optString("TTCRIMINALID") != "null")
                        criminal.setTtcriminallid("TTCRIMINALID: " + jsonObj.optString("TTCRIMINALID"));
                    if (jsonObj.optString("TRNID") != "null")
                        criminal.setTrnid("TRNID: " + jsonObj.optString("TRNID"));
                    if (jsonObj.optString("PSCODE") != "null")
                        criminal.setPs_code("PSCODE: " + jsonObj.optString("PSCODE"));
                    if (jsonObj.optString("WA_YEAR") != "null")
                        criminal.setWa_year("WA_YEAR: " + jsonObj.optString("WA_YEAR"));
                    if (jsonObj.optString("WASLNO") != "null")
                        criminal.setWaslno("WASLNO: " + jsonObj.optString("WASLNO"));
                    if (jsonObj.optString("CRIMENO") != "null")
                        criminal.setCrime_no("CRIMENO: " + jsonObj.optString("CRIMENO"));
                    if (jsonObj.optString("CRIMEYEAR") != "null")
                        criminal.setCrime_year("CRIMEYEAR: " + jsonObj.optString("CRIMEYEAR"));
                    if (jsonObj.optString("CASENO") != "null")
                        criminal.setCase_no("CASENO: " + jsonObj.optString("CASENO"));
                    if (jsonObj.optString("CASEYR") != "null")
                        criminal.setCase_year("CASEYR: " + jsonObj.optString("CASEYR"));
                    if (jsonObj.optString("SLNO") != "null")
                        criminal.setSl_no("SLNO: " + jsonObj.optString("SLNO"));
                    if(jsonObj.has("FATHERNAME")) {
                        if (jsonObj.optString("FATHERNAME") != "null") {
                            father_name = father_name + jsonObj.optString("FATHERNAME");
                        }
                    }else{
                        if (jsonObj.optString("FATHER_HUSBAND") != "null") {
                            father_name = father_name + jsonObj.optString("FATHER_HUSBAND");
                        }

                    }

                    criminal.setFatherName(father_name);

                    if (jsonObj.optString("AGE") != "null") {
                        age = age + jsonObj.optString("AGE");
                    }

                    criminal.setDob_age(age);

                    if (jsonObj.optString("SEX") != "null") {
                        sex = sex + jsonObj.optString("SEX");
                    }

                    criminal.setSex(sex);

                    if (jsonObj.optString("NATIONALITY") != "null") {

                        nationality = nationality + jsonObj.optString("NATIONALITY");

                    }
                    criminal.setNationality(nationality);


                    if (jsonObj.optString("RELIGION") != "null") {

                        religion = religion + jsonObj.optString("RELIGION");

                    }

                    criminal.setReligion(religion);

                    if (jsonObj.optString("CRSGENERALID") != "null") {
                        criminal.setCrs_generalID(jsonObj.optString("CRSGENERALID"));
                    }

                    criminalList.add(criminal);


                }
            }

            return criminalList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* On 19-07-2017, To link with criminal records page for the arrestedPersonOnClick */



    public static void showOKDialogMessage(Context mContext, String title,
                                           String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        // alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("OK", new OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        alertDialog.show();
    }

    public static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static void showNotification(Context context,
                                        final Class<?> mActivityClass, Intent intent) {
        final int UPDATE_NOTIFICATION = 1;// fetchNotificationID(context) + 1;
        Log.e("data", "data " + intent);
        // if (intent != null && intent.getExtras() != null) {
        Bundle bundle = intent.getExtras();
        // bundle.putString("AA", "BB");
        Log.e("data", "Data " + bundle.getString("subject"));
        Log.e("data", "Data " + bundle.getString("from"));
        Log.e("data", "Data " + bundle.getString("sendername"));
        Log.e("data", "Data " + bundle.getString("notificationid"));
        bundle = intent.getExtras();
        NotificationCompat.Builder mNotifyBuilder;

        mNotifyBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(bundle.getString("sendername")).setSmallIcon(
                        R.drawable.logo);
        mNotifyBuilder.setContentText(bundle.getString("subject"));
        NotificationDetails notificationData = new NotificationDetails();
        notificationData.setSubject(bundle.getString("subject"));
        notificationData.setSenderName(bundle.getString("sendername"));
        notificationData.setNotificationId(bundle.getString("notificationid"));
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

//		int notifyID = Utility.getNotificationId(context) + 1;
//		Utility.setNotificationId(notifyID, context);
        mNotifyBuilder.setSound(RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        Bundle dataBundle = new Bundle();
        dataBundle.putSerializable(Constants.OBJECT, notificationData);
        Intent notificationIntent = new Intent(context,
                NotificationDetailActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.putExtra(Constants.OBJECT, dataBundle);
        notificationIntent.putExtra(Constants.NOTIFICATION_ID, bundle.getString("notificationid"));
        notificationIntent.setAction("" + System.currentTimeMillis());
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mNotifyBuilder.setContentIntent(contentIntent);
        notificationManager.notify(Integer.parseInt(notificationData.getNotificationId()), mNotifyBuilder.build());
    }

/*	public static int getNotificationId(Context context) {
		try {
			SharedPreferences appPref = context.getSharedPreferences("AppPref",
					Context.MODE_PRIVATE);
			int mitId = appPref.getInt("NOTIFICATION", 0);
			return mitId;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static void setNotificationId(int id, Context context) {
		try {
			SharedPreferences appPref = context.getSharedPreferences("AppPref",
					Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = appPref.edit();
			editor.putInt("NOTIFICATION", id);
			editor.commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

    public static void cancelNotification(Context mContext, String notifyId) {
        NotificationManager nMgr = (NotificationManager) mContext
                .getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancel(Integer.parseInt(notifyId));
    }

    public static void logout(Context mContext) {
        Utility.setUserInfo(mContext, null);

        KPFaceDetectionApplication application = (KPFaceDetectionApplication) ((Activity) mContext).getApplicationContext();
        int length = application.getOpenActivityList().size();
        for (int i = length - 1; i >= 0; i--) {
            Activity activity = application.getOpenActivityList().get(i);
            application.removeActivityToList(activity);
            activity.finish();
        }
        Intent intent = new Intent(mContext, com.kp.facedetection.LoginActivity.class);
//		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);

//		Utility.setNotificationId(0, mContext);
    }

    public static void logout2(Context mContext) {
        Utility.setUserInfo(mContext, null);

        Constants.prev_timeStampString = "";
        Utility.storeTimestampValue(mContext, 0L);
        Utility.storeTimestamp(mContext, Constants.prev_timeStampString);

//		((Activity)mContext).finish();
		/*Intent intent = new Intent(mContext, LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		mContext.startActivity(intent);*/

		/*System.runFinalizersOnExit(true);
		System.exit(0);
		android.os.Process.killProcess(android.os.Process.myPid());*/

    }

    public static void changefonts(TextView textViewpre_font,
                                   Context mContext, String font) {

        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/" + font);

        try {
            textViewpre_font.setTypeface(tf);

        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    public static void showToast(Context ctx, String msg, String type) {

        if (type.equalsIgnoreCase("long")) {
            Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
        }

    }

	/*This is the custom
	* dialog shows application information
	* at option menu
	* @params act --> object of Activity
	* */

    public static void customDialogForAppInfo(Activity act, String version) {
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        LayoutInflater inflater = act.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.app_info_dialog, null);

        TextView tv_app_version_header = (TextView) dialogView.findViewById(R.id.tv_app_version_header);
        TextView tv_appVersion = (TextView) dialogView.findViewById(R.id.tv_appVersion);

        Utility.changefonts(tv_appVersion, act, "Calibri.ttf");
        Utility.changefonts(tv_app_version_header, act, "Calibri Bold.ttf");

        tv_appVersion.setText("Version: " + version);

        builder.setView(dialogView);

        //final AlertDialog alertDialog = builder.show();
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        alertDialog.show();
    }

    public static String getAppVersion(Context ctx) {
        String versionCode = "1.0";
        try {
            versionCode = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return versionCode;
    }

    public static void showAlertForProceed(Activity act) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(act);
        // alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setTitle("Alert!");
        alertDialog.setMessage("It will take considerable time, proceed");
        alertDialog.setPositiveButton("YES", new OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                delegate.onShowAlertForProceedResultSuccess("Success");

            }
        });

        alertDialog.setNegativeButton("NO", new OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                delegate.onShowAlertForProceedResultError("Error");
            }
        });
        alertDialog.show();
    }


    public static String currentDateTime() {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);

    }

    public static String readStream(InputStream in) {
        StringBuilder sb = new StringBuilder();

        try {
            Reader reader = new InputStreamReader(in);
            int data = reader.read();
            while (data != -1) {
                char theChar = (char) data;
                data = reader.read();
                sb.append(theChar);

            }

            reader.close();
        } catch (Exception e) {

        }

        return sb.toString();
    }


    public static void customDialogForChangePwd(final Activity act) {
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        LayoutInflater inflater = act.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.change_password_dialog, null);
        builder.setView(dialogView);

        final EditText old_pass = (EditText) dialogView.findViewById(R.id.old_pass);
        final EditText new_pass = (EditText) dialogView.findViewById(R.id.new_pass);
        final EditText confirm_new_pass = (EditText) dialogView.findViewById(R.id.confirm_new_pass);
        Button btn_cancel = (Button) dialogView.findViewById(R.id.btn_cancel);
        Button btn_done = (Button) dialogView.findViewById(R.id.btn_done);

        final Context context = act;

        final AlertDialog alertDialog = builder.show();

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();

                }

            }
        });



        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValidate(old_pass, new_pass, confirm_new_pass)) {

                    if (alertDialog != null && alertDialog.isShowing())
                        alertDialog.dismiss();
                    //showAlertDialog(context, " Success!!! ","Successfully change password", true);

                    userChangePassword(context, old_pass, new_pass, confirm_new_pass);


                } else {
                    //showAlertDialog(context, " Error!!! ","Incorrect password", false);
                }

            }
        });

    }

    public static void userChangePassword(Context con, EditText old_pass, EditText new_pass, EditText confirm_new_pass){

        Activity act = (Activity) con;
        TaskManager taskManager = new TaskManager(act);
        taskManager.setMethod(Constants.METHOD_CHANGE_PASSWORD);
        taskManager.setChangePassword(true);

        String[] keys = {"user_id","current_password", "new_password"};
        String[] values = {Utility.getUserInfo(con).getUserId().trim(), old_pass.getText().toString().trim(), new_pass.getText().toString().trim()};
        taskManager.doStartTask(keys, values, true);
    }

    public static void parseUserChangePasswordResponse(Context context, String response) {
        Log.e("Response", "Response " + response);

        try{
            JSONObject jsonObj = new JSONObject(response);
            if(jsonObj != null && jsonObj.optString("status").toString().equalsIgnoreCase("1"))
                showAlertDialog(context, " Success!!! ","Your password has been successfully changed", true);
            else
                showAlertDialog(context, "Error!!! ",jsonObj.optString("message"), false);

        }catch (Exception e){
            Toast.makeText(context, Constants.ERROR_EXCEPTION_MSG, Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public static boolean isValidate(EditText old_pass, EditText new_pass, EditText confirm_new_pass) {

        String oldPwd = old_pass.getText().toString().trim();
        String newPwd = new_pass.getText().toString().trim();
        String confirmPwd = confirm_new_pass.getText().toString().trim();

        Boolean pwdStatus = false;

        if (!oldPwd.equals("") && !newPwd.equals("")) {

            if(!oldPwd.equalsIgnoreCase(newPwd)) {

                if (Utility.passwordValidate(newPwd, confirmPwd)) {

                    pwdStatus = true;

                } else {

                    if (confirmPwd.equals("")) {
                        confirm_new_pass.requestFocus();
                        confirm_new_pass.setError("Re-enter new password");
                    } else {
                        confirm_new_pass.requestFocus();
                        confirm_new_pass.setError("New password doesn't match with confirm password");
                    }
                    pwdStatus = false;

                }
            }

            else {
                new_pass.requestFocus();
                new_pass.setError("Old password and new password must not be same");
                pwdStatus = false;
            }


        }

        else if (oldPwd.equals("") ) {

            old_pass.requestFocus();
            old_pass.setError("Enter old password");
            pwdStatus = false;

        }

        else if (newPwd.equals("")) {

            if(oldPwd.equals("")){
                old_pass.requestFocus();
                old_pass.setError("Enter old password");
            }else {

                new_pass.requestFocus();
                new_pass.setError("Enter new password");
                pwdStatus = false;
            }
        }

        return pwdStatus;
    }

    public static boolean passwordValidate(String str, String str1) {

        if (str.equals(str1))
            return true;
        else
            return false;
    }


    public static void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);


        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    public static String getCurrentTimeStamp(){

        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();

        System.out.println("time Stamp: " + ts);

        getDateCurrentTimeZone(tsLong);

        return ts;

    }

    public static String getDateCurrentTimeZone(long timestamp) {
        try{
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date currenTimeZone = (Date) calendar.getTime();

            System.out.println("Current: "+currenTimeZone);

            return sdf.format(currenTimeZone);
        }catch (Exception e) {
        }
        return "";
    }

    public static void setCustomTextAppearance(Context context, TextView textView, int resId) {

        if (Build.VERSION.SDK_INT < 23) {

            textView.setTextAppearance(context,resId);

        } else {

            textView.setTextAppearance(resId);
        }

    }

    public static void setLogMessage(String tag, String msg){
        if (BuildConfig.DEBUG) {
            Log.e(tag,msg);
        }
    }

    public static void hideKeyBoard(View editText) {
        ((InputMethodManager) KPFaceDetectionApplication.getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }


}