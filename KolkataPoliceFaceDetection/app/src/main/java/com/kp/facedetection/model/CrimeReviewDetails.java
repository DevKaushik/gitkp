package com.kp.facedetection.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DAT-165 on 07-06-2017.
 */

public class CrimeReviewDetails implements Parcelable {

    private String crimeCategory = "";
    private String countOfCrime = "";
    private String prevcountOfCrime = "";
    private String countOfCrimeDue ="";
    private String code = "";
    private String fatalCount = "";
    private String nonFatalCount = "";

    public String getCrimeCategory() {
        return crimeCategory;
    }

    public void setCrimeCategory(String crimeCategory) {
        this.crimeCategory = crimeCategory;
    }

    public String getCountOfCrime() {
        return countOfCrime;
    }

    public void setCountOfCrime(String countOfCrime) {
        this.countOfCrime = countOfCrime;
    }
    public String getPrevCountOfCrime() {
        return prevcountOfCrime;
    }
    public void setPrevCountOfCrime(String prevcountOfCrime) {
        this.prevcountOfCrime = prevcountOfCrime;
    }
    public String getCountOfCrimeDue() {
        return countOfCrimeDue;
    }

    public void setCountOfCrimeDue(String countOfCrimeDue) {
        this.countOfCrimeDue = countOfCrimeDue;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFatalCount() {
        return fatalCount;
    }

    public void setFatalCount(String fatalCount) {
        this.fatalCount = fatalCount;
    }

    public String getNonFatalCount() {
        return nonFatalCount;
    }

    public void setNonFatalCount(String nonFatalCount) {
        this.nonFatalCount = nonFatalCount;
    }

    public CrimeReviewDetails(Parcel in) {
        crimeCategory = in.readString();
        countOfCrime = in.readString();
        code = in.readString();
        fatalCount = in.readString();
        nonFatalCount = in.readString();
    }

    public static final Creator<CrimeReviewDetails> CREATOR = new Creator<CrimeReviewDetails>() {
        @Override
        public CrimeReviewDetails createFromParcel(Parcel in) {
            return new CrimeReviewDetails(in);
        }

        @Override
        public CrimeReviewDetails[] newArray(int size) {
            return new CrimeReviewDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(crimeCategory);
        dest.writeString(countOfCrime);
        dest.writeString(code);
        dest.writeString(fatalCount);
        dest.writeString(nonFatalCount);
    }
}
