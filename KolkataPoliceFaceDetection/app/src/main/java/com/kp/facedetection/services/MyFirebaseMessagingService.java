package com.kp.facedetection.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kp.facedetection.DashboardActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.utility.ApplicationLifecycleManager;
import com.kp.facedetection.utility.BadgeUtils;
import com.kp.facedetection.utility.Utility;

/**
 * Created by user on 07-05-2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "DAPL";
    public String messageTitle = "";
    public String messageBody = "";
    static int unOpenCount=0;

    public static int notificationCount = 0;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("DAPL","FCM onMessageReceived called.........");
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        /*Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Message data payload: " + remoteMessage.getData());*/
        Log.d(TAG, "Message Notification Body: " + remoteMessage);

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
         //   messageTitle = remoteMessage.getData().get("title");
         //   messageBody = remoteMessage.getData().get("body");
        }

       // Check if message contains a notification payload.
      /*  if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
             messageTitle = remoteMessage.getNotification().getTitle();
             messageBody = remoteMessage.getNotification().getBody();
        }*/
        //Log.d(TAG, "sendNotification called...");
       // sendNotification(messageBody);

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

            if (ApplicationLifecycleManager.isAppVisible()) {
                sendNotification(messageBody);
            } else {
                Log.d(TAG, "_______________NOT VISIBLE");
                sendNotification(messageBody);
            }
        }



    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        notificationCount = (int)(System.currentTimeMillis() / 1000);
        Intent intent = null;
        intent = new Intent(this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.logo);

        NotificationCompat.Builder  notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo)
                .setLargeIcon(icon)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setGroupSummary(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setSound(RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        //Uri soundUri = Uri.parse("android.resource://"+getPackageName()+"/raw/loving_you");
        //notificationBuilder.setSound(Uri.parse("android.resource://"+ getApplicationContext().getPackageName() + "/" + R.raw.gets_in_the_way));
       // notificationBuilder.setSound(soundUri);
        //notificationBuilder.setSound(Uri.parse("https://www.dine2you.com/staging/loving_you.mp3"));

        long[] vibrate = {500,500,500,500,500,500,500,500,500,500,500,1000};
        notificationBuilder.setVibrate(vibrate);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notificationCount /* ID of notification */, notificationBuilder.build());
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //startActivity(intent);
       // int unOpenCount= Integer.parseInt(Utility.getPushNotificationCount(this));
        unOpenCount=unOpenCount+1;
        Log.e("DAPL","Notification Count--------"+unOpenCount);

        Utility.setPushNotificationCount(this, String.valueOf(unOpenCount));


// This is for bladge on home icon
        BadgeUtils.setBadge(this,(int)unOpenCount);

    }
}
