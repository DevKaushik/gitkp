package com.kp.facedetection.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by DAT-Asset-131 on 02-06-2016.
 */
public class AssociateDetailsAdapter extends BaseAdapter {

    private List<String> associate_detailsList;
    private Context context;


    public AssociateDetailsAdapter(Context c,  List<String> associate_detailsList) {

        context = c;
        this.associate_detailsList = associate_detailsList;

        System.out.println("List Size: "+associate_detailsList.size());
    }

    @Override
    public int getCount() {

        if(associate_detailsList.size()>0) {
            return associate_detailsList.size();
        }
        else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.layout_criminal_details_item_row, null);


            holder.tv_key = (TextView) convertView
                    .findViewById(R.id.tv_key);

            holder.tv_value = (TextView) convertView
                    .findViewById(R.id.tv_value);

            holder.relative_associate = (RelativeLayout) convertView
                    .findViewById(R.id.relative_associate);



            Constants.changefonts(holder.tv_key, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_value, context, "Calibri.ttf");


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String text = associate_detailsList.get(position);

        System.out.println("text: "+text);

        String original_key = text.substring(0, text.indexOf(": "));
        String original_value = text.substring(text.indexOf(": ")+1);

        holder.relative_associate.setVisibility(View.GONE);
        holder.tv_key.setText(original_key+":");
        holder.tv_value.setText(original_value);

        return convertView;


    }

    class ViewHolder {

        TextView tv_key;
        TextView tv_value;
        RelativeLayout relative_associate;
    }
}
