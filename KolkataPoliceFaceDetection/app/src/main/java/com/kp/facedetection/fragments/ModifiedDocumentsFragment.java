package com.kp.facedetection.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.google.android.gcm.GCMRegistrar;
import com.kp.facedetection.R;
import com.kp.facedetection.adapter.DocumentFragmentPagerAdapter;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by DAT-Asset-131 on 10-09-2016.
 */
public class ModifiedDocumentsFragment extends Fragment {

    private PagerSlidingTabStrip tabsStrip;
    private ViewPager viewPager;
    String user_id="";
    boolean isHotelAuthenticated=false;
    int selectedPosition=0;

    public static ModifiedDocumentsFragment newInstance( ) {

        ModifiedDocumentsFragment modifiedDocumentsFragment = new ModifiedDocumentsFragment();
        //warrantSearchFragment.setArguments(args);
        return modifiedDocumentsFragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_modified_document, container, false);

        return rootView;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        viewPager = (ViewPager)view.findViewById(R.id.viewpager);

        // Give the PagerSlidingTabStrip the ViewPager
        tabsStrip = (PagerSlidingTabStrip)view.findViewById(R.id.tabs);

        initialization();

    }

    private void initialization(){
        viewPager.setAdapter(new DocumentFragmentPagerAdapter(getChildFragmentManager(),isHotelAuthenticated,getContext()));
        tabsStrip.setDividerColorResource(R.color.colorWhite);
        tabsStrip.setIndicatorHeight(10);
        tabsStrip.setTextColorResource(R.color.colorWhite);
        tabsStrip.setIndicatorColorResource(R.color.colorWhite);

        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(viewPager);


        // Attach the page change listener to tab strip and **not** the view pager inside the activity
        tabsStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // This method will be invoked when a new page becomes selected.
            @Override
            public void onPageSelected(int position) {
                selectedPosition=position;
               if(position==8){
                   Log.e("Hotel search poition", String.valueOf(position));
                  // Toast.makeText(getActivity(), "Hotel search poition:"+position, Toast.LENGTH_SHORT).show();
                   getHotelSessionKey();

               }

            }

            // This method will be invoked when the current page is scrolled
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Code goes here
            }

            // Called when the scroll state changes:
            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });



    }
    public void getHotelSessionKey(){
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_DOCUMENT_HOTEL_SESSION_ID);
        taskManager.setSessionForDocumentHotelSearch(true);
//		String devicetoken = PushUtility.fetchC2DMToken(this);
        user_id= Utility.getUserInfo(getContext()).getUserId();
        String[] keys = {"user_id","device_type","imei"};
       /* String[] values = {et_userid.getText().toString().trim(),
                et_password.getText().toString(), "android", devicetoken,Constants.device_IMEI_No};*///Constants.device_IMEI_No
        String[] values = {user_id,"android", Utility.getImiNO(getActivity())};//Constants.device_IMEI_No, Utility.getImiNO(getActivity())
        taskManager.doStartTask(keys, values, true, true);
    }
    public void parseDocumentHotelSearchAunthicatedResult(String result){
        if(result != null && !result.equals("")){
        Log.e("HOTEL_RESPONSE", result);
            try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.optString("status").equalsIgnoreCase("1")) {
                    if (!jObj.optString("auth_key").equalsIgnoreCase("") || !jObj.optString("auth_key").equalsIgnoreCase(null)) {
                        //Toast.makeText(getActivity(), "AUTH KEY Exist", Toast.LENGTH_SHORT).show();
                        isHotelAuthenticated = true;
                        Utility.setHotelSearchSesionId(getActivity(), jObj.optString("auth_key"));
                        //   Toast.makeText(getActivity(), "AUTH KEY--------"+jObj.optString("auth_key"), Toast.LENGTH_SHORT).show();
                        viewPager.setCurrentItem(8);
                        //viewPager.setAdapter(new DocumentFragmentPagerAdapter(getChildFragmentManager(),isHotelAuthenticated,getContext()));

                    }
                    else {
                        viewPager.setCurrentItem(0);
                        Utility.showAlertDialog(getActivity(), "Alert", "Something wrong please try again", false);
                    }
                }
                else{
                    //Toast.makeText(getActivity(), "AUTH KEY Absent", Toast.LENGTH_SHORT).show();
                    viewPager.setCurrentItem(0);
                    Utility.showAlertDialog(getActivity(),"Alert","Something wrong please try again",false);

                }


            }catch (JSONException e){

                e.printStackTrace();
                Utility.showToast(getActivity(), "Some error has been encountered", "long");
            }
        }
    }




}
