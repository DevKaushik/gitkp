package com.kp.facedetection;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;

import com.google.firebase.iid.FirebaseInstanceId;
import com.kp.facedetection.interfaces.OnCameraListener;
import com.kp.facedetection.receiver.SessionCheckReceiver;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.utils.SessionManagement;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BaseActivity extends FragmentActivity {

    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;
    private BroadcastReceiver br;

    private int year, month, day;
    private String selectedDate;

    SharedPreferences sharedPrefs_dashboardData;
    public static SharedPreferences.Editor editor_dashboardData;


    private String div ="";
    private String crime ="";
    public static final String TAG_DIV_CODE_LIST_INNER = "tag_div_code_inner";
    public static final String TAG_CRIME_CATEGORY_LIST_INNER = "tag_crime_category_inner";

    public static SharedPreferences sharedPrefs_forinnerData;
    public static SharedPreferences.Editor editor_forinnerData;



    // Storage for camera image URI components
    private final static String CAPTURED_PHOTO_PATH_KEY = "mCurrentPhotoPath";
    private final static String CAPTURED_PHOTO_URI_KEY = "mCapturedImageURI";

    // Required for camera operations in order to save the image file on resume.
    private String mCurrentPhotoPath = null;
    private Uri mCapturedImageURI = null;


    /*Variables used for call permission*/
    private static final int CAMERA_REQUEST_CODE = 300;
    private Uri fileUri = null;
    private String mPath = null;
    SessionManagement sessionManagement;

    OnCameraListener cameraListener;
    private String role = "";
    private String user_id ="";
    String devicetoken="";


    protected void setCameraListener(OnCameraListener listener) {
        cameraListener = listener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setupAlarm();

		/* off screen capture */
        sessionManagement = new SessionManagement(this);
        Log.d("Jay", "ScreensortFlag:" + sessionManagement.getScreenShotFlag());
        if (sessionManagement.getScreenShotFlag().equalsIgnoreCase("1")) {

        } else {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                    WindowManager.LayoutParams.FLAG_SECURE);
        }
        alarmMgr = (AlarmManager) (this.getSystemService(Context.ALARM_SERVICE));
      //  setUpdatedChargesheetNotification();
     //   getcurrentDate();
      //  setAllData();
        user_id = Utility.getUserInfo(this).getUserId();
        Date date = new Date();
        Calendar calender = Calendar.getInstance();
        calender.setTime(date);

        calender.add(Calendar.DATE, -1);
        year = calender.get(Calendar.YEAR);
        month = calender.get(Calendar.MONTH)+1;
        day = calender.get(Calendar.DAY_OF_MONTH);

        selectedDate = day + "-" + month + "-"+year;
        L.e("SELECTED DATE-----------"+selectedDate);

    }

    private void setAllData() {

        // pass current date



    }
   public void makeNotifictionVisibleAsPerRole(Context con, CoordinatorLayout coordinatorLayout){
       if(con!=null) {
           role = Utility.getUserInfo(con).getUserDesignation();
           if (role.equalsIgnoreCase(Constants.USER_ROLE_ORS)) {
               coordinatorLayout.setVisibility(View.GONE);
           }
       }

   }


    public void getChargesheetdetailsAsperRole(Context con) {
        sharedPrefs_dashboardData = getSharedPreferences(DashboardActivity.MyPREFERENCES_DASHBOARD, Context.MODE_PRIVATE);
        editor_dashboardData = sharedPrefs_dashboardData.edit();
        sharedPrefs_forinnerData = getSharedPreferences(DashboardActivity.MyPREFERENCES_INNER_DATA, Context.MODE_PRIVATE);
        editor_forinnerData = sharedPrefs_forinnerData.edit();

        if(!sharedPrefs_forinnerData.getString(TAG_DIV_CODE_LIST_INNER,"").equalsIgnoreCase("")){
            div = sharedPrefs_forinnerData.getString(TAG_DIV_CODE_LIST_INNER,"");
        }
        else{
            div = "";
        }
        if(!sharedPrefs_forinnerData.getString(TAG_CRIME_CATEGORY_LIST_INNER,"").equalsIgnoreCase("")){
            crime = sharedPrefs_forinnerData.getString(TAG_CRIME_CATEGORY_LIST_INNER,"");
        }
        else{
            crime = "";
        }
        L.e("SELECTED DIV-----------"+div);

        L.e("SELECTED CRIME IN getChargesheetdetailsAsperRole-----------"+crime);
        role = Utility.getUserInfo(this).getUserDesignation();
        L.e("getChargesheetdetailsAsperRole called-----------");
        if (role.equalsIgnoreCase(Constants.USER_ROLE_DC)) {
            L.e("Chargesheet PS LIST called-----------");
            Intent in = new Intent(BaseActivity.this, ChargesheetDuePsListActivity.class);
            in.putExtra("FROM_PAGE","BaseActivity");
            in.putExtra("SELECTED_USER_ID", user_id);
            in.putExtra("SELECTED_CRIME_CS", crime);
            Activity ac=(Activity)con;
            L.e("ACTIVITY NAME IN PS----------------"+ac.getClass().getSimpleName());

            startActivity(in);
        }else if (role.equalsIgnoreCase(Constants.USER_ROLE_IO) || role.equalsIgnoreCase(Constants.USER_ROLE_OC)){
            L.e("Chargesheet CASE LIST called-----------");
            Intent in = new Intent(BaseActivity.this, ChargesheetDueCaseListActivity.class);
            in.putExtra("FROM_PAGE","BaseActivity");
            in.putExtra("SELECTED_USER_ID", user_id);
            in.putExtra("SELECTED_CRIME_CS", crime);
            Activity ac=(Activity)con;
            L.e("ACTIVITY NAME IN CASE----------------"+ac.getClass().getSimpleName());
            //ac.finish();
            //finish();
            startActivity(in);

        }


    }

    public void logoutTask() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_LOGOUT);
        taskManager.setLogout(true);
      //  String devicetoken = GCMRegistrar.getRegistrationId(this);
        if(FirebaseInstanceId.getInstance().getToken()!=null) {
            devicetoken = FirebaseInstanceId.getInstance().getToken();
        }
        else{
            devicetoken=Utility.getFCMToken(this);
        }
        String[] keys = {"user_id", "device_tokenid", "logid"};//[userid=> 1,devicetoken=>'dsadasas']
        String[] values = {Utility.getUserInfo(this).getUserId().trim(),devicetoken , Utility.getUserInfo(this).getUser_logid().trim()};
        taskManager.doStartTask(keys, values, true);

    }

    public void parseLogoutResponse(String response) {
        Log.e("Response", "Response " + response);
        try {
            JSONObject jObj = new JSONObject(response);
            if (jObj != null && jObj.opt("status").toString().equalsIgnoreCase("1")) {
                Utility.logout(this);
            } else {
                Toast.makeText(this, Constants.ERROR_EXCEPTION_MSG, Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        KPFaceDetectionApplication.activityResumed();
        System.out.println("Alarm manager cancel");
        Log.e("TAG", "Alarm Manager Cancel");

        Constants.resume_count++;
        if (Constants.resume_count >= 1) {
            alarmMgr.cancel(alarmIntent);
            Constants.resume_count = 0;
        }

        // For login from other devices
        if (Utility.getUserInfo(this) == null) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        KPFaceDetectionApplication.activityPaused();

        int seconds = Constants.logout_interval; //2hr
        // Create an intent that will be wrapped in PendingIntent
        Intent intent = new Intent(this, SessionCheckReceiver.class);

        // Create the pending intent and wrap our intent
        alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

        // Get the alarm manager service and schedule it to go off after 3s

        alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + (seconds * 1000), alarmIntent);

        System.out.println("Alarm Manager Started");
        Log.e("TAG", "Alarm Manager Started");

    }

    @Override
    protected void onDestroy() {
        //alarmMgr.cancel(alarmIntent);
        //unregisterReceiver(br);
        super.onDestroy();
    }


    /*
    * to disable mobile down key- OPTION MENU specifically
    * */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (mCurrentPhotoPath != null) {
            savedInstanceState.putString(CAPTURED_PHOTO_PATH_KEY, mCurrentPhotoPath);
        }
        if (mCapturedImageURI != null) {
            savedInstanceState.putString(CAPTURED_PHOTO_URI_KEY, mCapturedImageURI.toString());
        }

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.containsKey(CAPTURED_PHOTO_PATH_KEY)) {
            mCurrentPhotoPath = savedInstanceState.getString(CAPTURED_PHOTO_PATH_KEY);
        }
        if (savedInstanceState.containsKey(CAPTURED_PHOTO_URI_KEY)) {
            mCapturedImageURI = Uri.parse(savedInstanceState.getString(CAPTURED_PHOTO_URI_KEY));
        }

        //Log.e("IMAGE URI Restored",mCapturedImageURI.toString());

        super.onRestoreInstanceState(savedInstanceState);
    }

    /**
     * Getters and setters.
     */

    public String getCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }

    public void setCurrentPhotoPath(String mCurrentPhotoPath) {
        this.mCurrentPhotoPath = mCurrentPhotoPath;
    }

    public Uri getCapturedImageURI() {
        return mCapturedImageURI;
    }

    public void setCapturedImageURI(Uri mCapturedImageURI) {
        this.mCapturedImageURI = mCapturedImageURI;
    }



	/*
	*  Camera & Gallery related code
	* */

    public void onCallCameraButton() {
        Log.e("PARENT- ", " onCallCameraButton");
        checkCallpermission();
    }


    /*
    * Checks for App Permission
    * (Camera Permission, Write External Storage)
    * */
    private boolean checkCallpermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            askCallPermission();
        } else {
            uploadImageDialog();
        }

        return false;
    }


    /*
    * Ask for App Permission
    * (Camera Permission, Write External Storage)
    * */
    private void askCallPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    uploadImageDialog();
                } else {

                    if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) && !ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.CAMERA)) {

                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                        Toast.makeText(this, "Go to app settings to enable storage and camera permission.", Toast.LENGTH_LONG).show();
                    } else {
                        askCallPermission();
                    }
                }
                break;
        }
    }


    /*
    * Dialog options to select camera and gallery
    * */
    public void uploadImageDialog() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fileUri = getOutputMediaFileUri(Constants.MEDIA_TYPE_IMAGE, getFileName());
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                    startActivityForResult(intent, Constants.CAPTURE_INTENT_CALLED);
                } else if (options[item].equals("Choose from Gallery")) {
                    if (Build.VERSION.SDK_INT < 19) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constants.GALLERY_INTENT_CALLED);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("image/*");
                        startActivityForResult(intent, Constants.GALLERY_INTENT_CALLED);
                    }

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    /*
  * Receives results from Camera/Gallery
  * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            //Toast.makeText(BaseActivity.this, "You have canceled", Toast.LENGTH_LONG).show();
        } else {
            Uri selectedImageUri;
            switch (requestCode) {
                case Constants.CAPTURE_INTENT_CALLED:

                    selectedImageUri = fileUri;

                    Bitmap mBitmap;
                    try {
                        InputStream input = getContentResolver().openInputStream(selectedImageUri);
                        mBitmap = getBitmapExifInterface(selectedImageUri, decodeBitmap(input));

                        if (cameraListener != null) {
                            cameraListener.onBitmapReceivedFromCamera(mBitmap, mPath);
                        }

                        if (input != null) {
                            input.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Log.e("CAPTURE_INTENT_CALLED", " Uri " + selectedImageUri);
                    break;

                case Constants.GALLERY_INTENT_CALLED:
                    selectedImageUri = data.getData();
                    Log.e("GALLERY_INTENT_CALLED", " Uri " + selectedImageUri);
                    try {
                        InputStream input = getContentResolver().openInputStream(selectedImageUri);

                        mBitmap = getBitmapExifInterface(selectedImageUri, decodeBitmap(input));

                        if (cameraListener != null) {
                            cameraListener.onBitmapReceivedFromGallery(mBitmap, mPath);
                        }

                        if (input != null) {
                            input.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

            }

        }
    }


    /**
     * Returns the scaled bitmap (by calculating the sample size)
     */
    private Bitmap decodeBitmap(InputStream inputStream) {

        Bitmap bitmap = null;
        InputStream is1 = null, is2 = null;

        try {
            Log.e("PARENT- ", "BITMAP: InputStream- " + inputStream.toString());

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            // Fake code simulating the copy
            // You can generally do better with nio if you need...
            // And please, unlike me, do something about the Exceptions :D
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();

            // Open new InputStreams using the recorded bytes
            // Can be repeated as many times as you wish
            is1 = new ByteArrayInputStream(baos.toByteArray());
            is2 = new ByteArrayInputStream(baos.toByteArray());

                /*DecodeBitmap before sampling*/
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inDither = true;
            BitmapFactory.decodeStream(is1, null, options);
            int imageHeight = options.outHeight;
            int imageWidth = options.outWidth;


                /*Calculate screen height & width*/
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int reqHeight = displayMetrics.heightPixels / 2;
            int reqwidth = displayMetrics.widthPixels / 2;

            Log.e("PURCHASED_ACTIVITY ", "BITMAP: width- " + imageWidth + " height- " + imageHeight +
                    " reqheight- " + reqHeight + " reqWidth- " + reqwidth);

            options.inSampleSize = calculateInSampleSize(options, reqwidth, reqHeight);

            Log.e("PURCHASED_ACTIVITY ", "BITMAP: SampleSize- " + options.inSampleSize + " InputStream- " + inputStream.toString());
            // Decode bitmap with inSampleSize set (DecodeSampledBitmap)
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inDither = true;
            bitmap = BitmapFactory.decodeStream(is2, null, options);

            if (bitmap != null) {
                Log.e("PURCHASED_ACTIVITY ", "BITMAP: height- " + bitmap.getHeight() + " width- " + bitmap.getWidth());
            } else {
                Log.e("PURCHASED_ACTIVITY ", "BITMAP: is Null.");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (is1 != null) {
                    is1.close();
                }
                if (is2 != null) {
                    is2.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return bitmap;
    }


    /**
     * Check & Returns the correct oriented Bitmap Image
     */
    private Bitmap getBitmapExifInterface(Uri selectedImageUri, Bitmap bmp) {
        /////for rotation of image
        ExifInterface exif;
        int rotation;
        try {
            mPath = ImageFilePath.getPath(this, selectedImageUri);
//            mPath = selectedImageUri.getPath();// "file:///mnt/sdcard/FileName.mp3"
            Log.e("PARENT- ", " Path: " + mPath);
            exif = new ExifInterface(mPath);
            rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

            bmp = getRotatedBitmap(rotation, bmp);

            Log.e("PARENT- ", " Exif Rotation: " + rotation);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmp;
    }


    /**
     * For checking bitmap image orientation and returns the rotated bitmap.
     */
    public Bitmap getRotatedBitmap(int rotation, Bitmap bmp) {

        Matrix matrix = new Matrix();

        switch (rotation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bmp;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(270);
                break;
            case ExifInterface.ORIENTATION_UNDEFINED:
                return bmp;
            default:
                return bmp;
        }

        try {
            Bitmap bmRotated = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
            bmp.recycle();
            return bmRotated;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }

    }

    /*
  * Calculating sample size of the bitmap
  * */
    public int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }






	  /*---------------------------------------------------------------------------------------------*/
    /*-- Below are the methods for creating Image file, file name when Image capture from camera --*/
    /*---------------------------------------------------------------------------------------------*/

    /*
    * Creates file name for the image to be captured
    * */
    public String getFileName() {
		/*SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyy_hhmmss");
		return simpleDateFormat.format(new Date());*/
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        return imageFileName;
    }


    /**
     * Create a file Uri for saving an image or video
     */
    public Uri getOutputMediaFileUri(int type, String name) {
        return Uri.fromFile(getOutputMediaFile(type, name));
    }


    /**
     * Create a File for saving an image or video
     */
    public File getOutputMediaFile(int type, String fName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        Environment.getExternalStorageState();

        /*File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CRS");*/
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.app_name));
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.e("CRS", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        File mediaFile;
        if (type == Constants.MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + fName + ".jpg");
        } /*else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        }*/ else {
            return null;
        }

        return mediaFile;
    }

    /*------------------------------------- Ends here --------------------------------------------*/


    /**
     * - Class is used to get Absolute Path from file -<br>
     * Irrespective of any android build version.
     */
    private static class ImageFilePath {

        /**
         * Method for return file path of Gallery image
         *
         * @param context
         * @param uri
         * @return path of the selected image file from gallery
         */
        static String nopath = "Select Video Only";

        @SuppressLint("NewApi")
        static String getPath(final Context context, final Uri uri) {

            // check here to KITKAT or new version
            final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

            // DocumentProvider
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/"
                                + split[1];
                    }
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {

                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"),
                            Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{split[1]};

                    return getDataColumn(context, contentUri, selection,
                            selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {

                // Return the remote address
                if (isGooglePhotosUri(uri))
                    return uri.getLastPathSegment();

                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }

            return nopath;
        }

        /**
         * Get the value of the data column for this Uri. This is <span id="IL_AD2"
         * class="IL_AD">useful</span> for MediaStore Uris, and other file-based
         * ContentProviders.
         *
         * @param context       The context.
         * @param uri           The Uri to query.
         * @param selection     (Optional) Filter used in the query.
         * @param selectionArgs (Optional) Selection arguments used in the query.
         * @return The value of the _data column, which is typically a file path.
         */
        static String getDataColumn(Context context, Uri uri,
                                    String selection, String[] selectionArgs) {

            Cursor cursor = null;
            final String column = "_data";
            final String[] projection = {column};

            try {
                cursor = context.getContentResolver().query(uri, projection,
                        selection, selectionArgs, null);
                if (cursor != null && cursor.moveToFirst()) {
                    final int index = cursor.getColumnIndexOrThrow(column);
                    return cursor.getString(index);
                }
            } finally {
                if (cursor != null)
                    cursor.close();
            }
            return nopath;
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is ExternalStorageProvider.
         */
        static boolean isExternalStorageDocument(Uri uri) {
            return "com.android.externalstorage.documents".equals(uri
                    .getAuthority());
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is DownloadsProvider.
         */
        static boolean isDownloadsDocument(Uri uri) {
            return "com.android.providers.downloads.documents".equals(uri
                    .getAuthority());
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is MediaProvider.
         */
        static boolean isMediaDocument(Uri uri) {
            return "com.android.providers.media.documents".equals(uri
                    .getAuthority());
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is Google Photos.
         */
        static boolean isGooglePhotosUri(Uri uri) {
            return "com.google.android.apps.photos.content".equals(uri
                    .getAuthority());
        }
    }
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setUpdatedChargesheetNotificationValue(context,intent);
        }
    };
    void setUpdatedChargesheetNotificationValue(Context context,Intent intent){

    }

}
