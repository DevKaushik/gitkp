package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.BARDetails;
import com.kp.facedetection.model.HotelDetails;
import com.kp.facedetection.utility.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by user on 19-02-2018.
 */

public class HotelSearchAdapter extends BaseAdapter {
    private Context context;
    private List<HotelDetails> hotelDetailsList;

    public HotelSearchAdapter(Context context, List<HotelDetails> hotelDetailsList) {
        this.context = context;
        this.hotelDetailsList = hotelDetailsList;
    }


    @Override
    public int getCount() {

        int size = (hotelDetailsList.size()>0)? hotelDetailsList.size():0;
        return size;

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        HotelSearchAdapter.ViewHolder holder;
        if (convertView == null) {
            holder = new HotelSearchAdapter.ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.hotel_search_list_item_layout, null);

            holder.tv_name=(TextView)convertView.findViewById(R.id.tv_name);
            holder.tv_address=(TextView)convertView.findViewById(R.id.tv_address);
            holder.imageView=(ImageView)convertView.findViewById(R.id.iv_userImage);
            Constants.changefonts(holder.tv_name, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_address, context, "Calibri.ttf");


            convertView.setTag(holder);
        } else {
            holder = (HotelSearchAdapter.ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);


        holder.tv_name.setText("NAME: "+hotelDetailsList.get(position).getName().trim());
        holder.tv_address.setText("ADDRESS: "+hotelDetailsList.get(position).getAddress().trim());

        Picasso.with(context).load(hotelDetailsList.get(position).getImage().trim()).fit().centerCrop()
                .placeholder(R.drawable.human3)
                .error(R.drawable.human3)
                .into(holder.imageView);

        return convertView;
    }

    class ViewHolder {

        TextView tv_name,tv_address;
        ImageView imageView;

    }
}
