package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kp.facedetection.adapter.CaptureLogListAdapter;
import com.kp.facedetection.fragments.DetailsForCaptureFIRFragment;
import com.kp.facedetection.interfaces.OnCaptureLogDltListener;
import com.kp.facedetection.interfaces.OnCaptureTagClickForListener;
import com.kp.facedetection.interfaces.OnCaptureUnTagClickListener;
import com.kp.facedetection.model.CaptureLogListItemModel;
import com.kp.facedetection.singletone_classes.CommunicationViews;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class CaptureLogListActivity extends BaseActivity implements OnCaptureTagClickForListener, View.OnClickListener, OnCaptureLogDltListener, OnCaptureUnTagClickListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String[] keys;
    private String[] values;
    private String[] keys_id;
    private String[] values_id;

    private String pageno = "";
    private String totalResult;// = "";
    private String responseString = "";

    private RelativeLayout relative_frag;
    private TextView tv_resultCount;
    private Button btn_loadMore;
    private View view_line;
    private RecyclerView recycler_captureList;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<CaptureLogListItemModel> captureLogList = new ArrayList<>();
    private CaptureLogListAdapter captureListAdapter;

    public static final String KEY_DETAIL = "key_detail";
    private int pos;
    private boolean visibleLoadMore = false;
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.capture_list_layout);
        ObservableObject.getInstance().addObserver(this);
        setToolBar();

        keys = getIntent().getStringArrayExtra("KEYS_PARAM");
        values = getIntent().getStringArrayExtra("VALUES_PARAM");
        responseString = getIntent().getExtras().getString("JSON_RESPONSE");

        fetchAllData(responseString);
        initViews();


    }


    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }


    private void initViews() {

        view_line = findViewById(R.id.view_line);
        btn_loadMore = (Button) findViewById(R.id.btn_loadMore);
        relative_frag = (RelativeLayout) findViewById(R.id.relative_frag);
        tv_resultCount = (TextView) findViewById(R.id.tv_resultCountCapture);
        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        if (totalResult.equalsIgnoreCase("0")){
            tv_resultCount.setText("");
            Utility.showAlertDialog(this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
            return;
        }


        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }

        recycler_captureList = (RecyclerView) findViewById(R.id.recycler_captureLogList);
        mLayoutManager = new LinearLayoutManager(this);
        recycler_captureList.setLayoutManager(mLayoutManager);
        recycler_captureList.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));

        captureListAdapter = new CaptureLogListAdapter(this, captureLogList);
        recycler_captureList.setAdapter(captureListAdapter);

        captureListAdapter.setCaptureTagClickListener(this);
        captureListAdapter.setCaptureLogDltListener(this);
        captureListAdapter.setCaptureUnTagListener(this);


        if (Integer.parseInt(totalResult) > 20) {
            btn_loadMore.setVisibility(View.VISIBLE);
            visibleLoadMore = true;
            view_line.setVisibility(View.GONE);
        } else {
            visibleLoadMore = false;
            btn_loadMore.setVisibility(View.GONE);
            view_line.setVisibility(View.VISIBLE);
        }

        btn_loadMore.setOnClickListener(this);
    }


    private void fetchAllData(String result) {

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    pageno = jObj.opt("pageno").toString();
                    totalResult = jObj.opt("totalresult").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseCaptureLogListResponse(resultArray);
                } else {
                    totalResult = ""+0;
                    //Utility.showAlertDialog(this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        } else {
            Utility.showAlertDialog(this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
        }
    }


    private void parseCaptureLogListResponse(JSONArray resultArray) {

        if (pageno.equalsIgnoreCase("1"))
            captureLogList.clear();


        for (int i = 0; i < resultArray.length(); i++) {
            JSONObject obj = null;

            try {
                obj = resultArray.getJSONObject(i);
                CaptureLogListItemModel captureDetails = new CaptureLogListItemModel(Parcel.obtain());

                if (obj.optString("CASENO") != null && !obj.optString("CASENO").equalsIgnoreCase("null") && !obj.optString("CASENO").equalsIgnoreCase("")
                        && obj.optString("FIR_YR") != null && !obj.optString("FIR_YR").equalsIgnoreCase("null") && !obj.optString("FIR_YR").equalsIgnoreCase("")) {
                    captureDetails.tag_status = "Untag";
                    captureDetails.fir_no =  obj.optString("CASENO");
                    captureDetails.fir_yr = obj.optString("FIR_YR");
                } else {
                    captureDetails.tag_status = "Tag";
                }

                if (obj.optString("PS") != null) {
                    captureDetails.psCode = obj.optString("PS");
                    if (captureDetails.psCode.equalsIgnoreCase("ORS")) {
                        captureDetails.psName = "";

                    } else {

                        try {
                            int psIndex = Constants.policeStationIDArrayList.indexOf(captureDetails.psCode);
                            captureDetails.psName = Constants.policeStationNameArrayList.get(psIndex);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                captureDetails.caseNO = obj.optString("CASENO");
                captureDetails.caseDate = obj.optString("CASEDATE");
                captureDetails.recordDate = obj.optString("RECORD_DATE");
                captureDetails.po_lat = obj.optString("PO_LAT");
                captureDetails.po_long = obj.optString("PO_LONG");
                captureDetails.ioName = obj.optString("IO");
                captureDetails.address = obj.optString("ADDRESS");
                captureDetails.log_id = obj.optString("ID");
                captureDetails.note = obj.optString("NOTE");

                captureLogList.add(captureDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }


    @Override
    public void onCaptureTag(View view, int position) {

        CaptureLogListItemModel captureListDetail = captureLogList.get(position);
        Log.e("Capture detail pos::", " pos:: " + position);
        relative_frag.setVisibility(View.VISIBLE);
        relative_frag.bringToFront();
        btn_loadMore.setVisibility(View.GONE);


        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_DETAIL, captureListDetail);
        bundle.putInt("POSITION_VAL",position);

        DetailsForCaptureFIRFragment detailFragment = new DetailsForCaptureFIRFragment();
        detailFragment.setArguments(bundle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.relative_frag, detailFragment, DetailsForCaptureFIRFragment.TAG);
        fragmentTransaction.commit();
    }


    @Override
    public void onCaptureLogDlt(View view, int position) {

        showAlertForDeleteProcess(position);
    }


    private void showAlertForDeleteProcess(final int position) {

        Log.e("DELETE", "Log Delete" + position);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(CaptureLogListActivity.this);
        // alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setTitle("Alert!!!");
        alertDialog.setMessage("Do you want to delete the record?");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();

                deleteCaptureLocation(position);
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        alertDialog.show();
    }

    private void deleteCaptureLocation(int position){

        CaptureLogListItemModel captureItem = captureLogList.get(position);
        String log_id = captureItem.log_id;
        pos = position;

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_DELETE_TEMP_FIR);
        taskManager.setDeleteTempCaptureFIR(true);

        keys_id = new String[]{"id"};
        values_id = new String[]{log_id.trim()};
        taskManager.doStartTask(keys_id, values_id, true);
    }

    public void parseCaptureTempLogDeleteResponse(String response){
        //System.out.println("parseCaptureTempLogDeleteResponse "+ response);

        if(response != null && !response.equals("")) {

            JSONObject jObj = null;
            try {

                jObj = new JSONObject(response);
                if (jObj.optString("status").equalsIgnoreCase("1")) {

                    captureLogList.remove(pos);

                    int i = Integer.parseInt(totalResult) - 1;

                    totalResult = ""+i;

                    if (totalResult.equalsIgnoreCase("0")){
                        tv_resultCount.setText("");
                        view_line.setVisibility(View.GONE);
                        Utility.showAlertDialog(this, Constants.ERROR_CAPTURE_ALERT_TITLE, "All records are deleted", false);
                        return;
                    }

                    if(i > 1){
                        tv_resultCount.setText("Total Results: "+i);
                    }else if(i ==1){
                        tv_resultCount.setText("Total Result: "+ i);
                    }

                    if(i > 20){
                        btn_loadMore.setVisibility(View.VISIBLE);
                        view_line.setVisibility(View.GONE);
                    }
                    else{
                        btn_loadMore.setVisibility(View.GONE);
                        view_line.setVisibility(View.VISIBLE);
                    }

                    captureListAdapter.notifyDataSetChanged();
                    Utility.showToast(CaptureLogListActivity.this, Constants.DELETE_CAPTURE_LOG_ITEM,"short");
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(CaptureLogListActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }



    @Override
    public void onBackPressed() {

        Fragment frag = getSupportFragmentManager().findFragmentByTag(DetailsForCaptureFIRFragment.TAG);
        if (frag != null) {
            getSupportFragmentManager().beginTransaction().remove(frag).commit();
            tv_resultCount.setVisibility(View.VISIBLE);
            recycler_captureList.setVisibility(View.VISIBLE);

            if (visibleLoadMore== true) {
                Log.e("MOITRI:","Onback press - VISIBLE");
                btn_loadMore.setVisibility(View.VISIBLE);
                view_line.setVisibility(View.GONE);
            }
            else{
                Log.e("MOITRI:","Onback press - GONE");
                btn_loadMore.setVisibility(View.GONE);
                view_line.setVisibility(View.VISIBLE);
            }

        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_loadMore:
                loadMoreData();
                break;
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
        }
    }


    private void loadMoreData() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_LIST_CAPTURE);
        taskManager.setLogListCapturedFIRPagination(true);

        values[1] = (Integer.parseInt(values[1]) + 1) + "";
        taskManager.doStartTask(keys, values, true);
    }

    public void parseLogListCapturePaginationResponse(String response) {
        //System.out.println("parseLogListCapturePaginationResponse: " + response);

        fetchAllData(response);

        if (Integer.parseInt(totalResult) > captureLogList.size()) {
            visibleLoadMore = true;
            btn_loadMore.setVisibility(View.VISIBLE);
            view_line.setVisibility(View.GONE);
        } else if (Integer.parseInt(totalResult) == captureLogList.size()) {
            visibleLoadMore = false;
            btn_loadMore.setVisibility(View.GONE);
            view_line.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onCaptureUnTag(View view, int position) {

        showAlertForUntagProcess(position);
    }

    private void showAlertForUntagProcess(final int position) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(CaptureLogListActivity.this);
        // alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setTitle("Alert!!!");
        alertDialog.setMessage("Do you want to untag the record?");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();

                unTagDataAPICall(position);
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        alertDialog.show();
    }

    private void unTagDataAPICall(int position){

        CaptureLogListItemModel captureItem = captureLogList.get(position);
        String log_id = captureItem.log_id;
        pos = position;

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_UNTAG_RECORD);
        taskManager.setCapturedUnTagRecord(true);

        keys_id = new String[]{"id"};
        values_id = new String[]{log_id.trim()};
        taskManager.doStartTask(keys_id, values_id, true);
    }

    public void parseCaptureLogUnTagResponse(String response){
        //System.out.println("parseCaptureLogUnTagResponse "+response);

        if(response != null && !response.equals("")){

            JSONObject jObj = null;
            try{

                jObj = new JSONObject(response);
                if(jObj.optString("status").equalsIgnoreCase("1")){
                    CaptureLogListItemModel captureItem = captureLogList.get(pos);
                    captureItem.tag_status = "Tag";

                    captureLogList.set(pos,captureItem);
                    captureListAdapter.notifyDataSetChanged();
                }else{

                }
            }
            catch(JSONException e){
                e.printStackTrace();
                Utility.showAlertDialog(CaptureLogListActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }

    public void updateTagStatus(int position,String psName, String psCode){
        Log.e("POSITION TAG",""+position);
        CaptureLogListItemModel captureItem = captureLogList.get(position);
        captureItem.tag_status = "Untag";
        captureItem.psName = psName;
        captureItem.psCode = psCode;

        captureLogList.set(position,captureItem);
        captureListAdapter.notifyDataSetChanged();

        if(Integer.parseInt(totalResult) == 20 && visibleLoadMore== true ){
            visibleLoadMore = false;
            btn_loadMore.setVisibility(View.GONE);
            view_line.setVisibility(View.VISIBLE);
        }

        if (visibleLoadMore== true ) {
            Log.e("MOITRI:","UPdate TAg - VISIBLE");
            btn_loadMore.setVisibility(View.VISIBLE);
            view_line.setVisibility(View.GONE);
        }
        else{
            Log.e("MOITRI:","UPdate TAg - GONE");
            btn_loadMore.setVisibility(View.GONE);
            view_line.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }
    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
