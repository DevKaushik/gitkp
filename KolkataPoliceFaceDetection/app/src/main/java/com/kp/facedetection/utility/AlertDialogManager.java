package com.kp.facedetection.utility;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;

/**
 * TODO: Created by Tanay Mondal on 20-02-2017
 */
public class AlertDialogManager {

    private static OnDialogClickListener clickListener = null;
    private Dialog mDialog = null;

    /*public void showDialog(Context context, String title, String body, String positiveText, String negativeText, boolean isOnlyPositive) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(context).inflate(R.layout.layout_dialog_view, null);
        TextView tvBody = (TextView) view.findViewById(R.id.tv_body);
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        tvTitle.setText(title);
        tvBody.setText(body);

        TextView tvPositive = (TextView) view.findViewById(R.id.tv_positive);
        TextView tvNegative = (TextView) view.findViewById(R.id.tv_negative);
        tvPositive.setText(positiveText);
        tvNegative.setText(negativeText);

        View divider = view.findViewById(R.id.tv_divider);

        if (isOnlyPositive) {
            tvNegative.setVisibility(View.GONE);
            divider.setVisibility(View.GONE);
            tvPositive.setBackgroundResource(R.drawable.round_shape_dialog_bottom);
        }

        tvPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.onDialogPositiveClick();
                }
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });

        tvNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.onDialogNegativeClick();
                }

                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });


        builder.setView(view);
        builder.setCancelable(false);
        mDialog = builder.create();
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.show();
    }*/

    public void setDialogClickListener(OnDialogClickListener listener) {
        clickListener = listener;
    }

    public void showDialog(Context context, String title, String body, String positiveText, String negativeText, boolean isOnlyPositive) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        builder.setMessage(body);
        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (clickListener != null) {
                    clickListener.onDialogPositiveClick();
                }
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });

        if (!isOnlyPositive) {
            builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (clickListener != null) {
                        clickListener.onDialogNegativeClick();
                    }

                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                }
            });
        }


        builder.setCancelable(false);
        mDialog = builder.create();
        mDialog.show();
    }

    public void showDialogSingle(Context context, String title, String body, String positiveText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(body);
        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });

        builder.setCancelable(false);
        mDialog = builder.create();
        mDialog.show();
    }


    public interface OnDialogClickListener {
        void onDialogPositiveClick();

        void onDialogNegativeClick();
    }
}
