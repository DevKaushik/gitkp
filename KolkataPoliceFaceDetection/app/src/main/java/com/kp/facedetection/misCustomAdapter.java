package com.kp.facedetection;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class misCustomAdapter extends ArrayAdapter<misDataModel> implements View.OnClickListener{

    private ArrayList<misDataModel> misDataModel;
    Context mContext;


    private static class ViewHolder {
        TextView txtName;
        TextView txtRank;
        TextView txtPosting;
        TextView txtLoginTime;
        ImageView info;
    }


    public misCustomAdapter(ArrayList<misDataModel> data, Context context) {
        super(context, R.layout.row_item, data);
        this.misDataModel = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        misDataModel misDataModel=(misDataModel)object;

        switch (v.getId())
        {

            case R.id.item_info:

                Snackbar.make(v, "Release date " +misDataModel.getLogInTime(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();

                break;
        }

    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        misDataModel misDataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.name);
            viewHolder.txtRank = (TextView) convertView.findViewById(R.id.rank);
            viewHolder.txtPosting = (TextView) convertView.findViewById(R.id.post);
            viewHolder.txtLoginTime = (TextView) convertView.findViewById(R.id.loginTime);
            viewHolder.info = (ImageView) convertView.findViewById(R.id.item_info);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.txtName.setText("Name: "+ misDataModel.getName());
        viewHolder.txtRank.setText("Rank: "+misDataModel.getRank());
        viewHolder.txtPosting.setText("Posting: "+misDataModel.getPosting());
        viewHolder.txtLoginTime.setText("Log In Time: "+misDataModel.getLogInTime());
        viewHolder.info.setOnClickListener(this);
        viewHolder.info.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }

}
