package com.kp.facedetection.model;

/**
 * Created by DAT-Asset-128 on 21-11-2017.
 */

public class ModifiedWarrantDetailsModel {
    String warranteeDetails;
    String warrantObjType;
    String warrantHeaderType;
    String nerDate;
    String withWarrant;
    String nerRemarks;
    String processNo;
    String processType;
    String processRequestDate;
    String processCourtAppr;
    String processMemoNo;
    String processMemodate;
    String processExecuted;
    String processExecutedDate;
    String processRemarks;
    String propertiesSlNo;
    String propertiesArticle;
    String propertiesValuation;

    public String getWarranteeDetails() {
        return warranteeDetails;
    }

    public void setWarranteeDetails(String warranteeDetails) {
        this.warranteeDetails = warranteeDetails;
    }

    public String getWarrantObjType() {
        return warrantObjType;
    }

    public void setWarrantObjType(String warrantObjType) {
        this.warrantObjType = warrantObjType;
    }

    public String getWarrantHeaderType() {
        return warrantHeaderType;
    }

    public void setWarrantHeaderType(String warrantHeaderType) {
        this.warrantHeaderType = warrantHeaderType;
    }

    public String getNerDate() {
        return nerDate;
    }

    public void setNerDate(String nerDate) {
        this.nerDate = nerDate;
    }

    public String getWithWarrant() {
        return withWarrant;
    }

    public void setWithWarrant(String withWarrant) {
        this.withWarrant = withWarrant;
    }

    public String getNerRemarks() {
        return nerRemarks;
    }

    public void setNerRemarks(String nerRemarks) {
        this.nerRemarks = nerRemarks;
    }

    public String getProcessNo() {
        return processNo;
    }

    public void setProcessNo(String processNo) {
        this.processNo = processNo;
    }

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }

    public String getProcessRequestDate() {
        return processRequestDate;
    }

    public void setProcessRequestDate(String processRequestDate) {
        this.processRequestDate = processRequestDate;
    }

    public String getProcessCourtAppr() {
        return processCourtAppr;
    }

    public void setProcessCourtAppr(String processCourtAppr) {
        this.processCourtAppr = processCourtAppr;
    }

    public String getProcessMemoNo() {
        return processMemoNo;
    }

    public void setProcessMemoNo(String processMemoNo) {
        this.processMemoNo = processMemoNo;
    }

    public String getProcessMemodate() {
        return processMemodate;
    }

    public void setProcessMemodate(String processMemodate) {
        this.processMemodate = processMemodate;
    }

    public String getProcessExecuted() {
        return processExecuted;
    }

    public void setProcessExecuted(String processExecuted) {
        this.processExecuted = processExecuted;
    }

    public String getProcessExecutedDate() {
        return processExecutedDate;
    }

    public void setProcessExecutedDate(String processExecutedDate) {
        this.processExecutedDate = processExecutedDate;
    }

    public String getProcessRemarks() {
        return processRemarks;
    }

    public void setProcessRemarks(String processRemarks) {
        this.processRemarks = processRemarks;
    }

    public String getPropertiesSlNo() {
        return propertiesSlNo;
    }

    public void setPropertiesSlNo(String propertiesSlNo) {
        this.propertiesSlNo = propertiesSlNo;
    }

    public String getPropertiesArticle() {
        return propertiesArticle;
    }

    public void setPropertiesArticle(String propertiesArticle) {
        this.propertiesArticle = propertiesArticle;
    }

    public String getPropertiesValuation() {
        return propertiesValuation;
    }

    public void setPropertiesValuation(String propertiesValuation) {
        this.propertiesValuation = propertiesValuation;
    }
}
