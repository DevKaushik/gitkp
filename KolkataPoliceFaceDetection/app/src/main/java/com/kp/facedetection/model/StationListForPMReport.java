package com.kp.facedetection.model;

import android.os.Parcel;
import android.os.Parcelable;


public class StationListForPMReport implements Parcelable{

    private String station_code="";
    private String station_name="";
    private String station_inchargeName="";
    private String station_inchargeCode = "";


    public StationListForPMReport(Parcel in) {
        station_code = in.readString();
        station_name = in.readString();
        station_inchargeName = in.readString();
        station_inchargeCode = in.readString();
    }

    public static final Creator<StationListForPMReport> CREATOR = new Creator<StationListForPMReport>() {
        @Override
        public StationListForPMReport createFromParcel(Parcel in) {
            return new StationListForPMReport(in);
        }

        @Override
        public StationListForPMReport[] newArray(int size) {
            return new StationListForPMReport[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(station_code);
        dest.writeString(station_name);
        dest.writeString(station_inchargeName);
        dest.writeString(station_inchargeCode);
    }

    public String getStation_code() {
        return station_code;
    }

    public void setStation_code(String station_code) {
        this.station_code = station_code;
    }

    public String getStation_name() {
        return station_name;
    }

    public void setStation_name(String station_name) {
        this.station_name = station_name;
    }

    public String getStation_inchargeName() {
        return station_inchargeName;
    }

    public void setStation_inchargeName(String station_inchargeName) {
        this.station_inchargeName = station_inchargeName;
    }

    public String getStation_inchargeCode() {
        return station_inchargeCode;
    }

    public void setStation_inchargeCode(String station_inchargeCode) {
        this.station_inchargeCode = station_inchargeCode;
    }
}
