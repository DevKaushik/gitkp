package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 28-07-2016.
 */
public class WarrantSearchDetails implements Serializable {

    private String rowNumber="";
    private String waNo="";
    private String psRecvDate="";
    private String underSections="";
    private String waStatus="";
    private String waType="";
    private String ps="";
    private String waSlNo="";
    private String sl_no="";
    private String wa_yr="";
    private String waName = "";

    public String getWa_yr() {
        return wa_yr;
    }

    public void setWa_yr(String wa_yr) {
        this.wa_yr = wa_yr;
    }

    public String getSl_no() {
        return sl_no;
    }

    public void setSl_no(String sl_no) {
        this.sl_no = sl_no;
    }

    public String getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(String rowNumber) {
        this.rowNumber = rowNumber;
    }

    public String getWaNo() {
        return waNo;
    }

    public void setWaNo(String waNo) {
        this.waNo = waNo;
    }

    public String getPsRecvDate() {
        return psRecvDate;
    }

    public void setPsRecvDate(String psRecvDate) {
        this.psRecvDate = psRecvDate;
    }

    public String getUnderSections() {
        return underSections;
    }

    public void setUnderSections(String underSections) {
        this.underSections = underSections;
    }

    public String getWaStatus() {
        return waStatus;
    }

    public void setWaStatus(String waStatus) {
        this.waStatus = waStatus;
    }

    public String getWaType() {
        return waType;
    }

    public void setWaType(String waType) {
        this.waType = waType;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }

    public String getWaSlNo() {
        return waSlNo;
    }

    public void setWaSlNo(String waSlNo) {
        this.waSlNo = waSlNo;
    }

    public String getWaName() {
        return waName;
    }

    public void setWaName(String waName) {
        this.waName = waName;
    }
}
