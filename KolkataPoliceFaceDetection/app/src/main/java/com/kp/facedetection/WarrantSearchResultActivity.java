package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kp.facedetection.adapter.WarrantSearchAdapter;
import com.kp.facedetection.model.WarrantSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by DAT-165 on 28-07-2016.
 */
public class WarrantSearchResultActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, Observer {

    private String[] keys;
    private String[] values;
    private String pageno;
    private String totalResult="";
    private String searchCase = "";
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private TextView tv_resultCount;
    private ListView lv_warrantSearchList;
    private Button btnLoadMore;
    private TextView tv_caseName;
    private TextView tv_watermark;

    List<WarrantSearchDetails> warrantSearchDetailsList;
    private WarrantSearchAdapter warrantSearchAdapter;
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modified_case_search_result_layout);
        ObservableObject.getInstance().addObserver(this);

        initView();

        clickEvents();

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);


        keys = getIntent().getStringArrayExtra("keys");
        values = getIntent().getStringArrayExtra("value");
        pageno = getIntent().getStringExtra("pageno");
        totalResult = getIntent().getStringExtra("totalResult");
        warrantSearchDetailsList = (List<WarrantSearchDetails>) getIntent().getSerializableExtra("WARRANT_SEARCH_LIST");

        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }

        warrantSearchAdapter= new WarrantSearchAdapter(this,warrantSearchDetailsList);

        lv_warrantSearchList.setAdapter(warrantSearchAdapter);

        warrantSearchAdapter.notifyDataSetChanged();

        if (Integer.parseInt(totalResult) > 20) {

            // Adding Load More button to listview at bottom
            lv_warrantSearchList.addFooterView(btnLoadMore);
        }

        lv_warrantSearchList.setOnItemClickListener(this);

        /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-55);
    }

    private void initView() {

        tv_resultCount=(TextView)findViewById(R.id.tv_resultCount);
        lv_warrantSearchList=(ListView)findViewById(R.id.lv_caseSearchList);
        tv_caseName = (TextView)findViewById(R.id.tv_caseName);

        tv_caseName.setText("WARRANT");

        // LoadMore button
        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");

        tv_watermark = (TextView)findViewById(R.id.tv_watermark);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
           /* case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this,Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                // Utility.logout(this);
                break;
        }
        return true;

    }

    private void clickEvents(){

        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // Starting a new async task
                fetchWarrantSearchResultPagination();
            }
        });

    }

    private void fetchWarrantSearchResultPagination() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_WARRANT_SEARCH);
        taskManager.setWarrantSearchPagination(true);

        values[5] = (Integer.parseInt(pageno) + 1) + "";

        taskManager.doStartTask(keys, values, true);
    }

    public void parseWarrantSearchResultResponsePagination(String result,String[] keys, String[] values){

        //System.out.println("parseWarrantSearchResultResponse: " + result);

        if(result != null && !result.equals("")){

            try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.opt("status").toString().equalsIgnoreCase("1")){

                    this.keys=keys;
                    this.values=values;
                    pageno=jObj.opt("pageno").toString();
                    totalResult=jObj.opt("totalresult").toString();

                    JSONArray resultArray=jObj.getJSONArray("result");

                    parseWarrantSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(this," Search Error ",jObj.optString("message"),false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                //Utility.showToast(this, "Some error has been encountered", "long");
            }
        }
    }

    private void parseWarrantSearchResponse(JSONArray resultArray) {

        for (int i = 0; i < resultArray.length(); i++) {

            JSONObject jsonObj = null;

            try {

                jsonObj = resultArray.getJSONObject(i);

                WarrantSearchDetails warrantSearchDetails = new WarrantSearchDetails();

                if (!jsonObj.optString("ROWNUMBER").equalsIgnoreCase("null")) {
                    warrantSearchDetails.setRowNumber(jsonObj.optString("ROWNUMBER"));
                }
                if (!jsonObj.optString("NAME").equalsIgnoreCase("null") && !jsonObj.optString("NAME").equalsIgnoreCase("") && jsonObj.optString("NAME") != null ) {
                    warrantSearchDetails.setWaName(jsonObj.optString("NAME"));
                }
                if (!jsonObj.optString("WANO").equalsIgnoreCase("null")) {
                    warrantSearchDetails.setWaNo(jsonObj.optString("WANO"));
                }
                if (!jsonObj.optString("PS_RECV_DATE").equalsIgnoreCase("null")) {
                    warrantSearchDetails.setPsRecvDate(jsonObj.optString("PS_RECV_DATE"));
                }
                if (!jsonObj.optString("UNDER_SECTIONS").equalsIgnoreCase("null")) {
                    warrantSearchDetails.setUnderSections(jsonObj.optString("UNDER_SECTIONS"));
                }
                if (!jsonObj.optString("WA_STATUS").equalsIgnoreCase("null")) {
                    warrantSearchDetails.setWaStatus(jsonObj.optString("WA_STATUS"));
                }
                if (!jsonObj.optString("WATYPE").equalsIgnoreCase("null")) {
                    warrantSearchDetails.setWaType(jsonObj.optString("WATYPE"));
                }
                if (!jsonObj.optString("PS").equalsIgnoreCase("null")) {
                    warrantSearchDetails.setPs(jsonObj.optString("PS"));
                }
                if (!jsonObj.optString("WA_SLNO").equalsIgnoreCase("null")) {
                    warrantSearchDetails.setWaSlNo(jsonObj.optString("WA_SLNO"));
                }
                if (!jsonObj.optString("WA_YEAR").equalsIgnoreCase("null")) {
                    warrantSearchDetails.setWa_yr(jsonObj.optString("WA_YEAR"));
                }
                if (!jsonObj.optString("SLNO").equalsIgnoreCase("null")) {
                    warrantSearchDetails.setSl_no(jsonObj.optString("SLNO"));
                }


                warrantSearchDetailsList.add(warrantSearchDetails);


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        int currentPosition = lv_warrantSearchList.getFirstVisiblePosition();
        int lastVisiblePosition = lv_warrantSearchList.getLastVisiblePosition();

        // Setting new scroll position
        lv_warrantSearchList.setSelectionFromTop(currentPosition + 1, 0);

        if((Integer.parseInt(totalResult)-lastVisiblePosition)<20){
            lv_warrantSearchList.removeFooterView(btnLoadMore);
        }
    }

    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     * */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        TextView tv_caseName = (TextView) view.findViewById(R.id.tv_caseName);

        String ps_code = warrantSearchDetailsList.get(position).getPs();
        String sl_no = warrantSearchDetailsList.get(position).getSl_no();
        String wa_yr = warrantSearchDetailsList.get(position).getWa_yr();
        String wa_sl_no = warrantSearchDetailsList.get(position).getWaSlNo();

        Log.e("ps_code", ps_code + " " + wa_sl_no + " " + wa_yr + " " + sl_no);



        Intent in=new Intent(WarrantSearchResultActivity.this,WarrantSearchDetailsActivity.class);
        in.putExtra("WARRANT_DETAILS", tv_caseName.getText().toString().trim());
        in.putExtra("PS_CODE",ps_code);
        in.putExtra("SL_NO",sl_no);
        in.putExtra("WA_YR",wa_yr);
        in.putExtra("WA_SL_NO",wa_sl_no);
        in.putExtra("WA_POSITION",position);
        startActivity(in);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
