package com.kp.facedetection.fragments;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.adapter.AssociateDetailsAdapter;
import com.kp.facedetection.adapter.ExpandableListAdapterForProfile;
import com.kp.facedetection.imageloader.ImageLoader;
import com.kp.facedetection.model.AssociateDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by DAT-Asset-131 on 01-04-2016.
 */
public class ProfileFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String PS_CODE = "PS_CODE";
    public static final String CRIME_NO = "CRIME_NO";
    public static final String CRIME_YR = "CRIME_YR";
    public static final String CASE_NO = "CASE_NO";
    public static final String CASE_YR = "CASE_YR";
    public static final String SL_NO = "SL_NO";
    public static final String CRIMINAL_FLAG = "CRIMINAL_FLAG";
    public static final String TRNID = "TRNID";
    public static final String WA_YEAR = "WA_YEAR";
    public static final String WASLNO = "WASLNO";

    private int mPage;
    private String details;
    private ExpandableListView lvExp;
    private TextView tv_noData;

    private View inflatedView;
    private PopupWindow popWindow;

    private String ps_code="";
    private String crime_no="";
    private String crime_year="";
    private String case_no="";
    private String case_year="";
    private String sl_no="";
    private String criminal_flag = "";
    private String trnid="";
    private String wa_year="";
    private String wasl_no="";

    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    ExpandableListAdapterForProfile listAdapter;

    AssociateDetails associateDetails;
    private List<AssociateDetails> associateDetailsList = new ArrayList<AssociateDetails>();
    ImageLoader imageLoader;


    public static ProfileFragment newInstance(int page, String criminal_flag, String trnid, String ps_code,String crime_no,String crime_year,String case_no,String case_year,String sl_no,String wa_year,String wasl_no ) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        args.putString(CRIMINAL_FLAG, criminal_flag);
        args.putString(TRNID, trnid);
        args.putString(PS_CODE, ps_code);
        args.putString(CRIME_NO, crime_no);
        args.putString(CRIME_YR, crime_year);
        args.putString(CASE_NO, case_no);
        args.putString(CASE_YR, case_year);
        args.putString(SL_NO, sl_no);
        args.putString(WA_YEAR, wa_year);
        args.putString(WASLNO, wasl_no);
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        criminal_flag = getArguments().getString(CRIMINAL_FLAG);
        trnid = getArguments().getString(TRNID).replace("TRNID: ", "");
        ps_code = getArguments().getString(PS_CODE).replace("PSCODE: ", "");
        crime_no = getArguments().getString(CRIME_NO).replace("CRIMENO: ", "");
        crime_year = getArguments().getString(CRIME_YR).replace("CRIMEYEAR: ", "");
        case_no = getArguments().getString(CASE_NO).replace("CASENO: ", "");
        case_year = getArguments().getString(CASE_YR).replace("CASEYR: ", "");
        sl_no = getArguments().getString(SL_NO).replace("SLNO: ", "");
        wa_year = getArguments().getString(WA_YEAR).replace("WA_YEAR: ","");
        wasl_no =  getArguments().getString(WASLNO).replace("WASLNO: ","");

    }

    // Inflate the fragment layout we defined above for this fragment
    // Set the associated text for the title
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        tv_noData = (TextView) view.findViewById(R.id.tv_noData);
        lvExp = (ExpandableListView) view.findViewById(R.id.lvExp);

        Utility.changefonts(tv_noData, getActivity(), "Calibri.ttf");

        tv_noData.setVisibility(View.GONE);
        lvExp.setVisibility(View.GONE);

        getProfileDetails(criminal_flag,trnid,crime_no,crime_year,ps_code,case_no,case_year,sl_no,wa_year,wasl_no);
        //getProfileDetails("389", "2016","NGR","48","2016","2");
        clickEvents();

    }

    private void getProfileDetails(String criminal_flag,String trnid, String crime_no,String crime_year,String ps_code,String case_no,String case_year,String sl_no,String wa_year,String wasl_no){

        this.crime_no=crime_no;
        this.crime_year=crime_year;
        this.ps_code=ps_code;
        this.case_no=case_no;
        this.case_year=case_year;
        this.sl_no=sl_no;
        this.criminal_flag=criminal_flag;
        this.trnid=trnid;
        this.wa_year=wa_year;
        this.wasl_no=wasl_no;

        System.out.println("Crime No: "+crime_no);
        System.out.println("Crime Year: "+crime_year);
        System.out.println("PS Code: "+ps_code);
        System.out.println("Case No: "+case_no);
        System.out.println("Case Year: "+case_year);
        System.out.println("TRNID: "+trnid);
        System.out.println("Criminal Flag: "+criminal_flag);
        System.out.println("SL No: "+sl_no);
        System.out.println("Warrent YR: "+wa_year);
        System.out.println("WASL No: "+wasl_no);

        if (!Constants.internetOnline(getActivity())) {
            Utility.showAlertDialog(getActivity(), Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_PROFILE_DETAILS);
        taskManager.setProfileDetails(true);

      /*  String[] keys = { "flag","crimeno","crimeyear","pscode","caseno","caseyr","slno" };
       // String[] values = { "f", crime_no,crime_year,ps_code,case_no,case_year,sl_no};
        String[] values = { "f", "2140", "2013","E1","322","2013","1" };*/

        String[] keys = new String[0];
        String[] values = new String[0];

        if(criminal_flag.equalsIgnoreCase("a")){
            keys = new String[] {"flag","pscode","trnid"};
            values = new String[] {criminal_flag.trim(),ps_code.trim(),trnid.trim()};
        }

        else if(criminal_flag.equalsIgnoreCase("c")){
            keys = new String[] {"flag","crimeno","crimeyear"};
            values = new String[] {criminal_flag.trim(),crime_no.trim(),crime_year.trim()};
        }

        else if(criminal_flag.equalsIgnoreCase("w")){
            keys = new String[] {"flag","pscode","wa_year","waslno"};
            values = new String[] {criminal_flag.trim(),ps_code.trim(),wa_year.trim(),wasl_no.trim()};
        }

        else if(criminal_flag.equalsIgnoreCase("f")){
            keys = new String[] { "flag","crimeno","crimeyear","pscode","caseno","caseyr","slno" };
            values = new String[] { criminal_flag.trim(), crime_no,crime_year.trim(),ps_code.trim(),case_no.trim(),case_year.trim(),sl_no.trim()};
        }
        taskManager.doStartTask(keys, values, true);


    }

    public void parseProfileDetailsResult(String response ){

        //System.out.println("parseProfileDetailsResponse: " + response);

        if (response != null && !response.equals("")) {

            try {
                JSONObject jObj = new JSONObject(response);
                if(jObj.optString("status").equalsIgnoreCase("1")){

                    tv_noData.setVisibility(View.GONE);
                    lvExp.setVisibility(View.VISIBLE);

                    JSONArray result = jObj.getJSONArray("result");
                    JSONObject obj = result.getJSONObject(0);
                    parseJSONObject(obj);


                }
                else{
                    tv_noData.setVisibility(View.VISIBLE);
                    lvExp.setVisibility(View.GONE);
                    tv_noData.setText("Sorry, no profile details found");
                   // Utility.showToast(getActivity(),"Sorry, no profile details found","long");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        else {
            Utility.showToast(getActivity(), "Some error has been encountered", "long");
        }

    }

    private void parseJSONObject(JSONObject object){

        JSONObject general = object.optJSONObject("general");
        JSONObject descriptive_role = object.optJSONObject("descriptiverole");
        JSONObject crs = object.optJSONObject("crs");
        JSONArray associate = object.optJSONArray("associate");

        System.out.println("General Size: "+general.length());
        System.out.println("Descriptive Role Size: "+descriptive_role.length());
        System.out.println("CRS Size: "+crs.length());
        System.out.println("Associate Size: "+associate.length());

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();



        if(general.length()>0){
            listDataHeader.add("General");

            Iterator iterator_general = general.keys();
            List<String> list_general = new ArrayList<>();

            while(iterator_general.hasNext()){
                String key = (String)iterator_general.next();

                if(!general.optString(key).equalsIgnoreCase("null")) {

                    String value = general.optString(key);
                    list_general.add(key.replace("_"," ") + "^" + value);
                }

            }

            listDataChild.put(listDataHeader.get(listDataHeader.indexOf("General")), list_general);

        }

        if(descriptive_role.length()>0) {
            listDataHeader.add("Descriptive Role");

            Iterator iterator_descriptiveRole = descriptive_role.keys();
            List<String> list_descriptiveRole = new ArrayList<>();

            while(iterator_descriptiveRole.hasNext()){
                String key = (String)iterator_descriptiveRole.next();

                if(!descriptive_role.optString(key).equalsIgnoreCase("null")){
                    String value = descriptive_role.optString(key);
                    list_descriptiveRole.add(key.replace("_"," ")+"^"+value);
                }

            }

            listDataChild.put(listDataHeader.get(listDataHeader.indexOf("Descriptive Role")), list_descriptiveRole);
        }

        if(crs.length()>0) {
            listDataHeader.add("CRS");

            Iterator iterator_crs = crs.keys();
            List<String> list_crs = new ArrayList<>();

            while(iterator_crs.hasNext()){

                String key = (String)iterator_crs.next();
                if(!crs.optString(key).equalsIgnoreCase("null")) {
                    String value = crs.optString(key);
                    list_crs.add(key.replace("_"," ") + "^" + value);
                }

            }

            listDataChild.put(listDataHeader.get(listDataHeader.indexOf("CRS")), list_crs);

        }
        if(associate.length()>0) {

            listDataHeader.add("Associate");
            List<String> list_associate = new ArrayList<>();

            for(int k=0; k<associate.length(); k++)
            {
                JSONObject associateObj=associate.optJSONObject(k);
                associateDetails=new AssociateDetails();


                if(!associateObj.optString("NAME").contains("null"))
                    associateDetails.setAssociate_name("Name: " + associateObj.optString("NAME"));
                if(!associateObj.optString("ADDRESS").contains("null"))
                    associateDetails.setAssociate_address("Address: " + associateObj.optString("ADDRESS"));
                if(!associateObj.optString("PS").contains("null"))
                    associateDetails.setAssociate_ps("PS: " + associateObj.optString("PS"));
                if(!associateObj.optString("AGE").contains("null"))
                    associateDetails.setAssociate_age("Age: " + associateObj.optString("AGE"));
                if(!associateObj.optString("FATHER").contains("null"))
                    associateDetails.setAssociate_father("Father: " + associateObj.optString("FATHER"));
                if(!associateObj.optString("PHOTONO").contains("null"))
                    associateDetails.setAssociate_photoNo("Photo No: " + associateObj.optString("PHOTONO"));
                if(!associateObj.optString("CASENO").contains("null"))
                    associateDetails.setAssociate_caseNo("Case No: " + associateObj.optString("CASENO"));
                if(!associateObj.optString("CASEYEAR").contains("null"))
                    associateDetails.setAssociate_caseYear("Case Year: " + associateObj.optString("CASEYEAR"));
                if(!associateObj.optString("CATEGORY").contains("null"))
                    associateDetails.setAssociate_category("Category: " + associateObj.optString("CATEGORY"));
                if(!associateObj.optString("UNDER SECTION").contains("null"))
                    associateDetails.setAssociate_underSection("Under Section: " + associateObj.optString("UNDER_SECTION"));
                if(!associateObj.optString("US CLASS").contains("null"))
                    associateDetails.setAssociate_usClass("US Class: " + associateObj.optString("US_CLASS"));
                if(!associateObj.optString("CLASS").contains("null"))
                    associateDetails.setAssociate_class("Class: " + associateObj.optString("CLASS"));
                if(!associateObj.optString("PROPERTY").contains("null"))
                    associateDetails.setAssociate_property("Property: " + associateObj.optString("PROPERTY"));
                if(!associateObj.optString("TRANSPORT").contains("null"))
                    associateDetails.setAssociate_transport("Transport: " + associateObj.optString("TRANSPORT"));
                if(!associateObj.optString("GROUND").contains("null"))
                    associateDetails.setAssociate_ground("Ground: " + associateObj.optString("GROUND"));
                if(!associateObj.optString("ARMS").contains("null"))
                    associateDetails.setAssociate_arms("Arms: " + associateObj.optString("ARMS"));
                if(!associateObj.optString("HEIGHT FEET").contains("null"))
                    associateDetails.setAssociate_heightFeet("Height Feet: " + associateObj.optString("HEIGHT_FEET"));
                if(!associateObj.optString("HEIGHT INCH").contains("null"))
                    associateDetails.setAssociate_heightInch("Height Inch: " + associateObj.optString("HEIGHT_INCH"));
                if(!associateObj.optString("BEARD").contains("null"))
                    associateDetails.setAssociate_beard("Beard: " + associateObj.optString("BEARD"));

                if(!associateObj.optString("BIRTHMARK").contains("null"))
                    associateDetails.setAssociate_birthmark("BirthMark: " + associateObj.optString("BIRTHMARK"));

                if(!associateObj.optString("BUILT").contains("null"))
                    associateDetails.setAssociate_built("Built: " + associateObj.optString("BUILT"));

                if(!associateObj.optString("BURNMARK").contains("null"))
                    associateDetails.setAssociate_burnmark("Burnmark: " + associateObj.optString("BURNMARK"));

                if(!associateObj.optString("COMPLEXION").contains("null"))
                    associateDetails.setAssociate_complexion("Complexion: " + associateObj.optString("COMPLEXION"));

                if(!associateObj.optString("CUTMARK").contains("null"))
                    associateDetails.setAssociate_cutmark("Cutmark: " + associateObj.optString("CUTMARK"));

                if(!associateObj.optString("DEFORMITY").contains("null"))
                    associateDetails.setAssociate_deformity("Deformity: " + associateObj.optString("DEFORMITY"));

                if(!associateObj.optString("EAR").contains("null"))
                    associateDetails.setAssociate_ear("Ear: " + associateObj.optString("EAR"));

                if(!associateObj.optString("EYE").contains("null"))
                    associateDetails.setAssociate_eye("Eye: " + associateObj.optString("EYE"));

                if(!associateObj.optString("EYEBROW").contains("null"))
                    associateDetails.setAssociate_eyebrow("Eyebrow: " + associateObj.optString("EYEBROW"));

                if(!associateObj.optString("FACE").contains("null"))
                    associateDetails.setAssociate_face("Face: " + associateObj.optString("FACE"));

                if(!associateObj.optString("FOREHEAD").contains("null"))
                    associateDetails.setAssociate_forehead("Forehead: " + associateObj.optString("FOREHEAD"));

                if(!associateObj.optString("HAIR").contains("null"))
                    associateDetails.setAssociate_hair("Hair: " + associateObj.optString("HAIR"));

                if(!associateObj.optString("MOLE").contains("null"))
                    associateDetails.setAssociate_mole("Mole: " + associateObj.optString("MOLE"));

                if(!associateObj.optString("MOUSTACHE").contains("null"))
                    associateDetails.setAssociate_moustache("Moustache: " + associateObj.optString("MOUSTACHE"));

                if(!associateObj.optString("NOSE").contains("null"))
                    associateDetails.setAssociate_nose("Nose: " + associateObj.optString("NOSE"));

                if(!associateObj.optString("SCARMARK").contains("null"))
                    associateDetails.setAssociate_scarmark("Scarmark: " + associateObj.optString("SCARMARK"));

                if(!associateObj.optString("TATTOOMARK").contains("null"))
                    associateDetails.setAssociate_tattoomark("Tattoomark: " + associateObj.optString("TATTOOMARK"));

                if(!associateObj.optString("WARTMARK").contains("null"))
                    associateDetails.setAssociate_wartmark("Wartmark: " + associateObj.optString("WARTMARK"));

                if(!associateObj.optString("picture").contains("null"))
                    associateDetails.setAssociate_picture(associateObj.optString("picture"));

                associateDetailsList.add(associateDetails);

                list_associate.add(Integer.toString(k));
            }

            listDataChild.put(listDataHeader.get(listDataHeader.indexOf("Associate")), list_associate);

        }

       // listAdapter = new ExpandableListAdapterForProfile(getActivity(), listDataHeader, listDataChild,associateDetailsList);

        // setting list adapter
        lvExp.setAdapter(listAdapter);
        lvExp.expandGroup(0);

    }

    private void clickEvents(){

        lvExp.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if(listDataHeader.get(groupPosition).equalsIgnoreCase("Associate")){

                    // show full screen popup window
                    System.out.println("Associate Details Opening");
                    popUpWindowForAssosiateDetails(v,childPosition);

                }


                return false;
            }
        });

    }

    private void popUpWindowForAssosiateDetails(View v, int pos){

        imageLoader = new ImageLoader(getActivity());

        List<String> associate_detailsList = new ArrayList<>();

        LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        inflatedView = layoutInflater.inflate(R.layout.layout_associate_detail_popup, null,false);

        ListView lv_associateDetails = (ListView)inflatedView.findViewById(R.id.lv_associateDetails);
        ImageView iv_image = (ImageView)inflatedView.findViewById(R.id.iv_image);

        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, width,height-50, true );
        // set a background drawable with rounders corners
        // popWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_bg));

        popWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindow.setAnimationStyle(R.style.PopupAnimation);

        // show the popup at bottom of the screen and set some margin at bottom ie,
       // popWindow.showAtLocation(v, Gravity.BOTTOM, 0, 100);

        popWindow.showAsDropDown(getActivity().getActionBar().getCustomView(),0,30);

        popWindow.setOutsideTouchable(true);
        popWindow.setFocusable(true);
        popWindow.getContentView().setFocusableInTouchMode(true);
        popWindow.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    System.out.println("back pressed while opened popup");
                    popWindow.dismiss();
                    return true;
                }
                return false;
            }
        });

        if(!associateDetailsList.get(pos).getAssociate_name().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_name());
        }

        if(!associateDetailsList.get(pos).getAssociate_address().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_address());
        }

        if(!associateDetailsList.get(pos).getAssociate_ps().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_ps());
        }

        if(!associateDetailsList.get(pos).getAssociate_age().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_age());
        }

        if(!associateDetailsList.get(pos).getAssociate_father().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_father());
        }

        if(!associateDetailsList.get(pos).getAssociate_photoNo().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_photoNo());
        }

        if(!associateDetailsList.get(pos).getAssociate_caseNo().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_caseNo());
        }

        if(!associateDetailsList.get(pos).getAssociate_caseYear().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_caseYear());
        }

        if(!associateDetailsList.get(pos).getAssociate_category().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_category());
        }

        if(!associateDetailsList.get(pos).getAssociate_underSection().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_underSection());
        }

        if(!associateDetailsList.get(pos).getAssociate_usClass().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_usClass());
        }

        if(!associateDetailsList.get(pos).getAssociate_class().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_class());
        }

        if(!associateDetailsList.get(pos).getAssociate_property().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_property());
        }

        if(!associateDetailsList.get(pos).getAssociate_transport().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_transport());
        }

        if(!associateDetailsList.get(pos).getAssociate_ground().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_ground());
        }

        if(!associateDetailsList.get(pos).getAssociate_arms().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_arms());
        }

        if(!associateDetailsList.get(pos).getAssociate_heightFeet().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_heightFeet());
        }

        if(!associateDetailsList.get(pos).getAssociate_heightInch().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_heightInch());
        }

        if(!associateDetailsList.get(pos).getAssociate_beard().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_beard());
        }

        if(!associateDetailsList.get(pos).getAssociate_birthmark().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_birthmark());
        }

        if(!associateDetailsList.get(pos).getAssociate_built().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_built());
        }

        if(!associateDetailsList.get(pos).getAssociate_burnmark().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_burnmark());
        }

        if(!associateDetailsList.get(pos).getAssociate_complexion().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_complexion());
        }

        if(!associateDetailsList.get(pos).getAssociate_cutmark().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_cutmark());
        }

        if(!associateDetailsList.get(pos).getAssociate_deformity().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_deformity());
        }

        if(!associateDetailsList.get(pos).getAssociate_ear().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_ear());
        }

        if(!associateDetailsList.get(pos).getAssociate_eye().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_eye());
        }

        if(!associateDetailsList.get(pos).getAssociate_eyebrow().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_eyebrow());
        }

        if(!associateDetailsList.get(pos).getAssociate_face().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_face());
        }

        if(!associateDetailsList.get(pos).getAssociate_forehead().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_forehead());
        }

        if(!associateDetailsList.get(pos).getAssociate_hair().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_hair());
        }

        if(!associateDetailsList.get(pos).getAssociate_mole().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_mole());
        }

        if(!associateDetailsList.get(pos).getAssociate_moustache().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_moustache());
        }

        if(!associateDetailsList.get(pos).getAssociate_nose().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_nose());
        }

        if(!associateDetailsList.get(pos).getAssociate_scarmark().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_scarmark());
        }

        if(!associateDetailsList.get(pos).getAssociate_tattoomark().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_tattoomark());
        }

        if(!associateDetailsList.get(pos).getAssociate_wartmark().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_wartmark());
        }

        if(!associateDetailsList.get(pos).getAssociate_picture().equalsIgnoreCase("")) {
            imageLoader.DisplayImage(
                    associateDetailsList.get(pos).getAssociate_picture(),
                    iv_image);
        }

        AssociateDetailsAdapter associateDetailsAdapter = new AssociateDetailsAdapter(getActivity(),associate_detailsList);
        lv_associateDetails.setAdapter(associateDetailsAdapter);


    }
}
