package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 26-07-2016.
 */
public class CaseSearchWitnessDetails implements Serializable {

    private String nameWitness="";
    private String addressWitness="";

    public String getNameWitness() {
        return nameWitness;
    }

    public void setNameWitness(String nameWitness) {
        this.nameWitness = nameWitness;
    }

    public String getAddressWitness() {
        return addressWitness;
    }

    public void setAddressWitness(String addressWitness) {
        this.addressWitness = addressWitness;
    }
}
