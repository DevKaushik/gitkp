package com.kp.facedetection.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import com.kp.facedetection.fragments.SwipeFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-Asset-131 on 10-05-2016.
 */
public class ImageFragmentPagerAdapter extends FragmentPagerAdapter {

    private List<String> imgList = new ArrayList<String>();

    public ImageFragmentPagerAdapter(FragmentManager fm,List<String> imgList) {
        super(fm);
        this.imgList = imgList;
    }

    @Override
    public int getCount() {
        return imgList.size();
    }

    @Override
    public Fragment getItem(int position) {
        SwipeFragment fragment = new SwipeFragment();
        fragment.setImgList(imgList);
        return SwipeFragment.newInstance(position);
    }
}