package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;

import java.util.Observable;
import java.util.Observer;

public class MobileStolenSearchDetailsActivity extends BaseActivity implements View.OnClickListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private TextView tv_heading;

    private TextView tv_mobileNoValue;
    private TextView tv_imeiValue;
    private TextView tv_dtmissingValue;
    private TextView tv_complainantNameValue;
    private TextView tv_complainantAddressValue;
    private TextView tv_placeMissingValue;
    private TextView tv_ioNameValue;
    private TextView tv_psNameValue;
    private TextView tv_caseRefValue;
    private TextView tv_firgdValue;
    private TextView tv_gdDateValue;
    private TextView tv_statusValue;
    private TextView tv_makerValue;
    private TextView tv_coontactNoValue;
    private TextView tv_briefFactValue;
    private TextView tv_service_providerValue;
    private TextView tv_traced_mob_noValue;
    private TextView tv_name_of_userValue;
    private TextView tv_traced_onValue;
    private TextView tv_recoveryDateValue;
    private TextView tv_recoverByValue;
    private TextView tv_handover_dateValue;
    private TextView tv_imei2Value;
    private TextView tv_mob2Value;
    private TextView tv_service_provider2Value;
    private TextView tv_esnValue;
    private TextView tv_esn2Value;

    private TextView tv_watermark;

    private String maker_model_modified_text="";
    private String holder_MobileNo="";
    private String holder_ImeiNo="";
    private String holder_missingDt="";
    private String holder_complaintName="";
    private String holder_complainantAddress="";
    private String holder_placeMissing="";
    private String holder_ioName="";
    private String holder_psName="";
    private String holder_caseRef="";
    private String holder_firgd="";
    private String holder_gdDate="";
    private String holder_status="";
    private String holder_maker="";
    private String holder_model="";
    private String holder_coontactNo="";
    private String holder_briefFact="";
    private String holder_serviceProvider="";
    private String holder_traced_mob_no="";
    private String holder_name_of_user="";
    private String holder_traced_on="";
    private String holder_recovery_date="";
    private String holder_recover_by="";
    private String holder_date_of_handover="";
    private String holder_imeiNo2="";
    private String holder_MobileNo2="";
    private String holder_serviceProvider2="";
    private String holder_esn="";
    private String holder_esn2="";
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobilestolen_search_details);
        ObservableObject.getInstance().addObserver(this);

        initialization();
    }

    private void initialization() {

        tv_mobileNoValue = (TextView) findViewById(R.id.tv_mobileNoValue);
        tv_imeiValue = (TextView) findViewById(R.id.tv_imeiValue);
        tv_dtmissingValue = (TextView) findViewById(R.id.tv_dtmissingValue);
        tv_complainantNameValue = (TextView) findViewById(R.id.tv_complainantNameValue);
        tv_complainantAddressValue = (TextView) findViewById(R.id.tv_complainantAddressValue);
        tv_placeMissingValue = (TextView) findViewById(R.id.tv_placeMissingValue);
        tv_ioNameValue = (TextView) findViewById(R.id.tv_ioNameValue);
        tv_psNameValue = (TextView) findViewById(R.id.tv_psNameValue);
        tv_caseRefValue = (TextView) findViewById(R.id.tv_caseRefValue);
        tv_firgdValue = (TextView) findViewById(R.id.tv_firgdValue);
        tv_gdDateValue = (TextView) findViewById(R.id.tv_gdDateValue);
        tv_statusValue = (TextView) findViewById(R.id.tv_statusValue);
        tv_makerValue = (TextView) findViewById(R.id.tv_makerValue);
        tv_coontactNoValue = (TextView) findViewById(R.id.tv_coontactNoValue);
        tv_briefFactValue = (TextView) findViewById(R.id.tv_briefFactValue);
        tv_service_providerValue = (TextView) findViewById(R.id.tv_service_providerValue);
        tv_traced_mob_noValue = (TextView) findViewById(R.id.tv_traced_mob_noValue);
        tv_name_of_userValue = (TextView) findViewById(R.id.tv_name_of_userValue);
        tv_traced_onValue = (TextView) findViewById(R.id.tv_traced_onValue);
        tv_recoveryDateValue = (TextView) findViewById(R.id.tv_recoveryDateValue);
        tv_recoverByValue = (TextView) findViewById(R.id.tv_recoverByValue);
        tv_handover_dateValue = (TextView) findViewById(R.id.tv_handover_dateValue);
        tv_imei2Value = (TextView) findViewById(R.id.tv_imei2Value);
        tv_mob2Value = (TextView) findViewById(R.id.tv_mob2Value);
        tv_service_provider2Value = (TextView) findViewById(R.id.tv_service_provider2Value);
        tv_esnValue = (TextView) findViewById(R.id.tv_esnValue);
        tv_esn2Value = (TextView) findViewById(R.id.tv_esn2Value);



        tv_heading = (TextView) findViewById(R.id.tv_heading);

        tv_watermark = (TextView) findViewById(R.id.tv_watermark);

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);


        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        holder_MobileNo = getIntent().getStringExtra("Holder_MobileNo").trim();
        holder_ImeiNo = getIntent().getStringExtra("Holder_IMEINo").trim();
        holder_missingDt = getIntent().getStringExtra("Holder_MissingDt").trim();
        holder_complaintName = getIntent().getStringExtra("Holder_ComplainantName").trim();
        holder_complainantAddress = getIntent().getStringExtra("Holder_ComplainantAddress").trim();
        holder_placeMissing = getIntent().getStringExtra("Holder_MissingPlace").trim();
        holder_ioName = getIntent().getStringExtra("Holder_IOName").trim();
        holder_psName = getIntent().getStringExtra("Holder_PSName").trim();
        holder_caseRef = getIntent().getStringExtra("Holder_CaseRef").trim();
        holder_firgd = getIntent().getStringExtra("Holder_FIRGD").trim();
        holder_gdDate = getIntent().getStringExtra("Holder_GdDate").trim();
        holder_status = getIntent().getStringExtra("Holder_PrStatus").trim();
        holder_maker = getIntent().getStringExtra("Holder_Make").trim();
        holder_model = getIntent().getStringExtra("Holder_Model").trim();
        holder_coontactNo = getIntent().getStringExtra("Holder_ContactNo").trim();
        holder_briefFact =  getIntent().getStringExtra("Holder_BriefFact").trim();
        holder_serviceProvider =  getIntent().getStringExtra("Holder_ServiceProvider").trim();
        holder_traced_mob_no =  getIntent().getStringExtra("Holder_TracedMobNo").trim();
        holder_name_of_user =  getIntent().getStringExtra("Holder_NameOfUser").trim();
        holder_traced_on =  getIntent().getStringExtra("Holder_TracedOn").trim();
        holder_recovery_date = getIntent().getStringExtra("Holder_RecoveryDate").trim();
        holder_recover_by = getIntent().getStringExtra("Holder_RecoverBy").trim();
        holder_date_of_handover = getIntent().getStringExtra("Holder_handover_date").trim();
        holder_imeiNo2 = getIntent().getStringExtra("Holder_IMEINo2").trim();
        holder_MobileNo2 = getIntent().getStringExtra("Holder_MobileNo2").trim();
        holder_serviceProvider2 =  getIntent().getStringExtra("Holder_ServiceProvider2").trim();
        holder_esn =  getIntent().getStringExtra("Holder_ESN").trim();
        holder_esn2 =  getIntent().getStringExtra("Holder_ESN2").trim();

        if(!holder_maker.equalsIgnoreCase("")){
            maker_model_modified_text = maker_model_modified_text + holder_maker;
        }
        if(!holder_model.equalsIgnoreCase("")){
            maker_model_modified_text = maker_model_modified_text + " / " + holder_model;
        }


        tv_mobileNoValue.setText(holder_MobileNo);
        tv_imeiValue.setText(holder_ImeiNo);
        tv_dtmissingValue.setText(holder_missingDt);
        tv_complainantNameValue.setText(holder_complaintName);
        tv_complainantAddressValue.setText(holder_complainantAddress);
        tv_placeMissingValue.setText(holder_placeMissing);
        tv_ioNameValue.setText(holder_ioName);
        tv_psNameValue.setText(holder_psName);
        tv_caseRefValue.setText(holder_caseRef);
        tv_firgdValue.setText(holder_firgd);
        tv_gdDateValue.setText(holder_gdDate);
        tv_statusValue.setText(holder_status);
        tv_makerValue.setText(maker_model_modified_text);
        tv_coontactNoValue.setText(holder_coontactNo);
        tv_briefFactValue.setText(holder_briefFact);
        tv_service_providerValue.setText(holder_serviceProvider);
        tv_traced_mob_noValue.setText(holder_traced_mob_no);
        tv_name_of_userValue.setText(holder_name_of_user);
        tv_traced_onValue.setText(holder_traced_on);
        tv_recoveryDateValue.setText(holder_recovery_date);
        tv_recoverByValue.setText(holder_recover_by);
        tv_handover_dateValue.setText(holder_date_of_handover);
        tv_imei2Value.setText(holder_imeiNo2);
        tv_mob2Value.setText(holder_MobileNo2);
        tv_service_provider2Value.setText(holder_serviceProvider2);
        tv_esnValue.setText(holder_esn);
        tv_esn2Value.setText(holder_esn2);

        Constants.changefonts(tv_mobileNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_imeiValue, this, "Calibri.ttf");
        Constants.changefonts(tv_dtmissingValue, this, "Calibri.ttf");
        Constants.changefonts(tv_complainantNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_complainantAddressValue, this, "Calibri.ttf");
        Constants.changefonts(tv_placeMissingValue, this, "Calibri.ttf");
        Constants.changefonts(tv_ioNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_psNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_caseRefValue, this, "Calibri.ttf");
        Constants.changefonts(tv_firgdValue, this, "Calibri.ttf");
        Constants.changefonts(tv_gdDateValue, this, "Calibri.ttf");
        Constants.changefonts(tv_statusValue, this, "Calibri.ttf");
        Constants.changefonts(tv_makerValue, this, "Calibri.ttf");
        Constants.changefonts(tv_coontactNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_briefFactValue, this, "Calibri.ttf");
        Constants.changefonts(tv_service_providerValue, this, "Calibri.ttf");
        Constants.changefonts(tv_traced_mob_noValue, this, "Calibri.ttf");
        Constants.changefonts(tv_name_of_userValue, this, "Calibri.ttf");
        Constants.changefonts(tv_traced_onValue, this, "Calibri.ttf");
        Constants.changefonts(tv_recoveryDateValue, this, "Calibri.ttf");
        Constants.changefonts(tv_recoverByValue, this, "Calibri.ttf");
        Constants.changefonts(tv_handover_dateValue, this, "Calibri.ttf");
        Constants.changefonts(tv_imei2Value, this, "Calibri.ttf");
        Constants.changefonts(tv_mob2Value, this, "Calibri.ttf");
        Constants.changefonts(tv_service_provider2Value, this, "Calibri.ttf");
        Constants.changefonts(tv_esnValue, this, "Calibri.ttf");
        Constants.changefonts(tv_esn2Value, this, "Calibri.ttf");

        Constants.changefonts(tv_heading, this, "Calibri Bold.ttf");

         /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this,Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;

        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
