package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 14-07-2016.
 */
public class WarrantDetails implements Serializable {

    private String waIssueDate="";
    private String waNo="";
    private String underSection="";
    private String waStatus="";
    private String issuingCourt="";
    private String processNo="";
    private String type="";
    private String ioServe="";
    private String caseRef="";
    private String remark="";
    private String presentStatus="";
    private String warranties="";
    private String psName="";
    private String crimeNo="";
    private String returnableDateCourt="";
    private String wat_type="";

    public String getWat_type() {
        return wat_type;
    }

    public void setWat_type(String wat_type) {
        this.wat_type = wat_type;
    }



    public String getReturnableDateCourt() {
        return returnableDateCourt;
    }

    public void setReturnableDateCourt(String returnableDateCourt) {
        this.returnableDateCourt = returnableDateCourt;
    }




    public String getWaIssueDate() {
        return waIssueDate;
    }

    public void setWaIssueDate(String waIssueDate) {
        this.waIssueDate = waIssueDate;
    }

    public String getWaNo() {
        return waNo;
    }

    public void setWaNo(String waNo) {
        this.waNo = waNo;
    }

    public String getUnderSection() {
        return underSection;
    }

    public void setUnderSection(String underSection) {
        this.underSection = underSection;
    }

    public String getWaStatus() {
        return waStatus;
    }

    public void setWaStatus(String waStatus) {
        this.waStatus = waStatus;
    }

    public String getIssuingCourt() {
        return issuingCourt;
    }

    public void setIssuingCourt(String issuingCourt) {
        this.issuingCourt = issuingCourt;
    }

    public String getProcessNo() {
        return processNo;
    }

    public void setProcessNo(String processNo) {
        this.processNo = processNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIoServe() {
        return ioServe;
    }

    public void setIoServe(String ioServe) {
        this.ioServe = ioServe;
    }

    public String getCaseRef() {
        return caseRef;
    }

    public void setCaseRef(String caseRef) {
        this.caseRef = caseRef;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPresentStatus() {
        return presentStatus;
    }

    public void setPresentStatus(String presentStatus) {
        this.presentStatus = presentStatus;
    }

    public String getWarranties() {
        return warranties;
    }

    public void setWarranties(String warranties) {
        this.warranties = warranties;
    }

    public String getPsName() {
        return psName;
    }

    public void setPsName(String psName) {
        this.psName = psName;
    }

    public String getCrimeNo() {
        return crimeNo;
    }

    public void setCrimeNo(String crimeNo) {
        this.crimeNo = crimeNo;
    }
}
