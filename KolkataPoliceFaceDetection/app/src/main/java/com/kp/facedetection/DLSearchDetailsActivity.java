package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;

import java.util.Observable;
import java.util.Observer;

public class DLSearchDetailsActivity extends BaseActivity implements View.OnClickListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String holder_dlno="";
    private String holder_dlname="";
    private String holder_dlSwdof="";
    private String holder_dlDob="";
    private String holder_dlSex="";
    private String holder_pAddress="";
    private String holder_tAddress="";
    private String holder_authority="";
    private String holder_DLNTVLDTODT="";
    private String holder_DLTVLDTODT="";
    private String holder_DLTELE="";

    private TextView tv_DlNoValue;
    private TextView tv_DlNameValue;
    private TextView tv_DlSwdofValue;
    private TextView tv_DlDobValue;
    private TextView tv_dlSexValue;
    private TextView tv_DlPaddressValue;
    private TextView tv_DlTaddressValue;
    private TextView tv_DlIssueAuthorityValue;
    private TextView tv_DL_NTVLDTODTValue;
    private TextView tv_DL_TVLDTODTValue;
    private TextView tv_DL_DLTELEValue;

    private TextView tv_heading;

    private TextView tv_watermark;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dlsearch_details);
        ObservableObject.getInstance().addObserver(this);
        initialization();
    }

    private void initialization() {

        tv_DlNoValue = (TextView)findViewById(R.id.tv_DlNoValue);
        tv_DlNameValue = (TextView)findViewById(R.id.tv_DlNameValue);
        tv_DlSwdofValue = (TextView)findViewById(R.id.tv_DlSwdofValue);
        tv_DlDobValue = (TextView)findViewById(R.id.tv_DlDobValue);
        tv_dlSexValue = (TextView)findViewById(R.id.tv_dlSexValue);
        tv_DlPaddressValue = (TextView)findViewById(R.id.tv_DlPaddressValue);
        tv_DlTaddressValue = (TextView)findViewById(R.id.tv_DlTaddressValue);
        tv_DlIssueAuthorityValue = (TextView)findViewById(R.id.tv_DlIssueAuthorityValue);
        tv_DL_NTVLDTODTValue = (TextView)findViewById(R.id.tv_DL_NTVLDTODTValue);
        tv_DL_TVLDTODTValue = (TextView)findViewById(R.id.tv_DL_TVLDTODTValue);
        tv_DL_DLTELEValue = (TextView)findViewById(R.id.tv_DL_DLTELEValue);

        tv_heading = (TextView)findViewById(R.id.tv_heading);

        tv_watermark = (TextView)findViewById(R.id.tv_watermark);

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        holder_dlno = getIntent().getStringExtra("Holder_DLno").trim();
        holder_dlname = getIntent().getStringExtra("Holder_DlName").trim();
        holder_dlSwdof = getIntent().getStringExtra("Holder_DlSwdof").trim();
        holder_dlDob = getIntent().getStringExtra("Holder_DlDob").replace("00:00:00","").trim();
        holder_dlSex = getIntent().getStringExtra("Holder_DlSex").trim();
        holder_pAddress = getIntent().getStringExtra("Holder_DlPAddress").trim();
        holder_tAddress = getIntent().getStringExtra("Holder_DlTAddress").trim();
        holder_authority = getIntent().getStringExtra("Holder_DlAuthority").trim();
        holder_DLNTVLDTODT = getIntent().getStringExtra("Holder_DLNTVLDTODT").replace("00:00:00","").trim();
        holder_DLTVLDTODT = getIntent().getStringExtra("Holder_DLTVLDTODT").replace("00:00:00","").trim();
        holder_DLTELE = getIntent().getStringExtra("Holder_DLTELE").trim();

        tv_DlNoValue.setText(holder_dlno);
        tv_DlNameValue.setText(holder_dlname);
        tv_DlSwdofValue.setText(holder_dlSwdof);
        tv_DlDobValue.setText(holder_dlDob);

        if(holder_dlSex.equalsIgnoreCase("M")){
            tv_dlSexValue.setText("Male");
        }else if(holder_dlSex.equalsIgnoreCase("F")){
            tv_dlSexValue.setText("Female");
        }

        tv_DlPaddressValue.setText(holder_pAddress);
        tv_DlTaddressValue.setText(holder_tAddress);
        tv_DlIssueAuthorityValue.setText(holder_authority);
        tv_DL_NTVLDTODTValue.setText(holder_DLNTVLDTODT);
        tv_DL_TVLDTODTValue.setText(holder_DLTVLDTODT);
        tv_DL_DLTELEValue.setText(holder_DLTELE);

        Constants.changefonts(tv_DlNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_DlNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_DlSwdofValue, this, "Calibri.ttf");
        Constants.changefonts(tv_DlDobValue, this, "Calibri.ttf");
        Constants.changefonts(tv_dlSexValue, this, "Calibri.ttf");
        Constants.changefonts(tv_DlPaddressValue, this, "Calibri.ttf");
        Constants.changefonts(tv_DlTaddressValue, this, "Calibri.ttf");
        Constants.changefonts(tv_DlIssueAuthorityValue, this, "Calibri.ttf");
        Constants.changefonts(tv_DL_NTVLDTODTValue, this, "Calibri.ttf");
        Constants.changefonts(tv_DL_TVLDTODTValue, this, "Calibri.ttf");
        Constants.changefonts(tv_DL_DLTELEValue, this, "Calibri.ttf");

        Constants.changefonts(tv_heading, this, "Calibri Bold.ttf");

        /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);

    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
           /* case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this,Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }


    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
