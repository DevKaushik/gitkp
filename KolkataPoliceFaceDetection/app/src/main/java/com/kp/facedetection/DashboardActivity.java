package com.kp.facedetection;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.design.widget.CoordinatorLayout;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.fragments.AllDocumentDataSearchFragment;
import com.kp.facedetection.fragments.CaptureFIRFragment;
import com.kp.facedetection.interfaces.ChargesheetNotificationUpdateListener;
import com.kp.facedetection.receiver.ChargeSheetDueNotificationUpdateReceiver;
import com.kp.facedetection.services.ChargesheetDueNotificationService;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

import static com.kp.facedetection.utility.Utility.showAlertDialog;

public class DashboardActivity extends BaseActivity implements View.OnClickListener,ChargesheetNotificationUpdateListener {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private boolean doubleBackToExitPressedOnce;

    private TextView tv_currentDate;
    private TextView tv_firCount, tv_arrestCount, tv_cs_due, tv_disposalCount, tv_waExecsCount, tv_wawaRecvsCount, tv_wa_falling_due_count,tv_wa_falling_total_count,tv_wa_falling_due,tv_wa_due_count, tv_wa_pending_count, tv_rtaCount,tv_unnatural_deaths_count;
    private ImageView iv_search, iv_crimeReview, iv_imageMatching, iv_feedback, iv_changePwd, iv_logout, iv_captureFIR,iv_mis;
    private RelativeLayout rl_calender, rl_Fir, rl_Arrest,rl_cs_due, rl_waExecs, rl_rta, rl_Disposals, rl_waRecvs, rl_wa_falling_due,rl_wa_due, rl_wa_pending, rl_sdr ,rl_unnatural_deaths,rl_special_services,rl_bar,rl_hotel;
    private RelativeLayout relative_fragContain;
    private CoordinatorLayout charseet_due_notification;
    private PercentRelativeLayout fullDashboardView;

    public static final String PREFS_NAME2 = "MyPrefsFile2";
    private CheckBox dontShowAgain;
    private SharedPreferences settings;
    private int year, month, day;
    private String selectedDate;
    boolean isHotelAuthenticated=false;

    public static final int PREFERENCE_REQUEST_CODE = 450;

    public static final String MyPREFERENCES_DASHBOARD = "MyPrefs_for_dashboard";
    SharedPreferences sharedPrefs_dashboardData;
    public static SharedPreferences.Editor editor_dashboardData;

    private String div ="";
    private String ps = "";
    private String crime = "";

    public static final String MyPREFERENCES_INNER_DATA = "MyPrefs_for_InnerData";
    public static SharedPreferences sharedPrefs_forinnerData;
    public static SharedPreferences.Editor editor_forinnerData;

    public static final String TAG_PS_NAME_LIST_INNER = "tag_ps_name_inner";
    public static final String TAG_PS_ID_LIST_INNER = "tag_ps_id_inner";
    public static final String TAG_DIV_NAME_LIST_INNER = "tag_div_name_inner";
    public static final String TAG_DIV_CODE_LIST_INNER = "tag_div_code_inner";
    public static final String TAG_CRIME_CATEGORY_LIST_INNER = "tag_crime_category_inner";
    public static final String TAG_CRIME_CATEGORY_SHOW_LIST_INNER = "tag_crime_category_show_inner";

    public static final String TAG_CHECKED_KP = "checked_kp";
    public static final String TAG_CHECKED_DIV = "checked_div";
    public static final String TAG_CHECKED_PS = "checked_ps";
    boolean returnFlag=false;
    boolean isFirstTime=true;
    private String role="";
    private Context context;
    TextView chargesheetNotificationCount;
    String user_id="";
    String fromPage="";
    String unnaturalDeathTotalCount="";
    String notificationCount="";
    String tomorrowAsString;
    boolean r_flag;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_app_preference:
                if (!Constants.internetOnline(this)) {
                    showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
                }

                Intent in = new Intent(DashboardActivity.this, PreferenceActivity.class);
                in.putExtra("SELECTED_DATE_PREFERENCE", selectedDate);
                startActivityForResult(in, PREFERENCE_REQUEST_CODE);
                //Utility.showToast(DashboardActivity.this,"Work in progress","short");
                break;
            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(DashboardActivity.this);
                break;
            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;
            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;
            case R.id.menu_logout:
                logoutTask();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_layout);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM");
        tomorrowAsString = dateFormat.format(tomorrow);

        if(getIntent().hasExtra("FROM_PAGE"))
        {
            fromPage=getIntent().getStringExtra("FROM_PAGE");
        }
        context=getBaseContext();
        setToolBar();
        initViews();
        getHotelSessionKey();
        DashboardActivity.sharedPrefs_forinnerData = getSharedPreferences(DashboardActivity.MyPREFERENCES_INNER_DATA, Context.MODE_PRIVATE);
        editor_forinnerData = sharedPrefs_forinnerData.edit();

    }

    private void getRole() {
        if (Utility.getUserInfo(this).getUserRank().equals("NODAL OFFICER")) {
            r_flag = true;

        }
        if (Utility.getUserInfo(this).getUserRank().equals("DESIGNATED OFFICER")) {
            r_flag = false;
        }
    }


    private void setToolBar() {
        try {
            user_id = Utility.getUserInfo(this).getUserId();
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
            role = Utility.getUserInfo(this).getUserDesignation();
            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        ChargeSheetDueNotificationUpdateReceiver chargeSheetDueNotificationUpdateReceiver=new ChargeSheetDueNotificationUpdateReceiver();
        chargeSheetDueNotificationUpdateReceiver.setChargesheetNotificationUpdateListener(this);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
        KPFaceDetectionApplication.getApplication().addActivityToList(this);
       //startService(new Intent(getBaseContext(), ChargesheetDueNotificationService.class));
    }


    private void initViews() {

        tv_currentDate = (TextView)findViewById(R.id.tv_currentDate);
        tv_firCount = (TextView)findViewById(R.id.tv_firCount);
        tv_arrestCount = (TextView)findViewById(R.id.tv_arrestCount);
        tv_cs_due =(TextView)findViewById(R.id.tv_cs_due_value);
        tv_disposalCount = (TextView)findViewById(R.id.tv_disposalCount);
        tv_waExecsCount = (TextView)findViewById(R.id.tv_waExecsCount);
        tv_wawaRecvsCount = (TextView)findViewById(R.id.tv_wawaRecvsCount);
        tv_wa_falling_due_count = (TextView)findViewById(R.id.tv_wa_falling_due_count);
        tv_wa_falling_total_count = (TextView)findViewById(R.id.tv_wa_falling_total_count);
/*<<<<<<< HEAD
        tv_wa_falling_due_date=(TextView)findViewById(R.id.tv_wa_falling_due_date);
        tv_wa_due_count = (TextView)findViewById(R.id.tv_wa_due_count);
        Calendar cal=Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH,1);
        Date d=cal.getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM");
        String startDate=df.format(d);
        // Toast.makeText(context, "DATE"+startDate, Toast.LENGTH_SHORT).show();
        tv_wa_falling_due_date.setText(startDate);
=======*/
        tv_wa_falling_due = (TextView)findViewById(R.id.tv_wa_falling_due);
        tv_wa_due_count = (TextView)findViewById(R.id.tv_wa_due_count);
        tv_wa_falling_due.setText("W/A Due\non "+ tomorrowAsString);

        tv_wa_pending_count = (TextView)findViewById(R.id.tv_wa_pending_count);
        tv_rtaCount = (TextView) findViewById(R.id.tv_rtaCount);
        tv_unnatural_deaths_count=(TextView)findViewById(R.id.tv_unnatural_deaths_count);

        rl_calender = (RelativeLayout) findViewById(R.id.rl_calender);
        rl_calender.setOnClickListener(this);

        iv_search = (ImageView) findViewById(R.id.iv_search);
        iv_search.setOnClickListener(this);

        iv_crimeReview = (ImageView)findViewById(R.id.iv_crimeReview);
        iv_crimeReview.setOnClickListener(this);

      /*  iv_logout = (ImageView)findViewById(R.id.iv_logout);
        iv_logout.setOnClickListener(this);*/

        iv_mis = (ImageView)findViewById(R.id.iv_mis);
        iv_mis.setOnClickListener(this);

        iv_captureFIR = (ImageView)findViewById(R.id.iv_captureFIR);
        iv_captureFIR.setOnClickListener(this);

        rl_Fir = (RelativeLayout) findViewById(R.id.rl_Fir);
        rl_Fir.setOnClickListener(this);

        rl_Arrest = (RelativeLayout)findViewById(R.id.rl_Arrest);
        rl_Arrest.setOnClickListener(this);

        rl_cs_due = (RelativeLayout)findViewById(R.id.rl_cs_due);
        rl_cs_due.setOnClickListener(this);

        rl_Disposals= (RelativeLayout) findViewById(R.id.rl_Disposals);
        rl_Disposals.setOnClickListener(this);

        rl_waExecs = (RelativeLayout) findViewById(R.id.rl_waExecs);
        rl_waExecs.setOnClickListener(this);

        rl_waRecvs= (RelativeLayout) findViewById(R.id.rl_waRecvs);
        rl_waRecvs.setOnClickListener(this);

        rl_wa_falling_due = (RelativeLayout) findViewById(R.id.rl_wa_falling_due);
        rl_wa_falling_due.setOnClickListener(this);

        rl_wa_due = (RelativeLayout) findViewById(R.id.rl_wa_due);
        rl_wa_due.setOnClickListener(this);

        rl_rta = (RelativeLayout) findViewById(R.id.rl_rta);
        rl_rta.setOnClickListener(this);

        rl_sdr = (RelativeLayout) findViewById(R.id.rl_sdr);
        rl_sdr.setOnClickListener(this);

        rl_bar = (RelativeLayout) findViewById(R.id.rl_bar);
        rl_bar.setOnClickListener(this);

        rl_hotel = (RelativeLayout) findViewById(R.id.rl_hotel);
        rl_hotel.setOnClickListener(this);

        rl_wa_pending = (RelativeLayout) findViewById(R.id.rl_wa_pending);
        rl_wa_pending.setOnClickListener(this);

        rl_unnatural_deaths =(RelativeLayout) findViewById(R.id.rl_unnatural_deaths);
        rl_unnatural_deaths.setOnClickListener(this);

        rl_special_services =(RelativeLayout) findViewById(R.id.rl_special_services);
        rl_special_services.setOnClickListener(this);

        relative_fragContain = (RelativeLayout) findViewById(R.id.relative_fragContain);
        fullDashboardView = (PercentRelativeLayout) findViewById(R.id.fullDashboardView);
        charseet_due_notification=(CoordinatorLayout)findViewById(R.id.charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        Log.e("RES_CHECK",""+ getResources().getString(R.string.resolution_check));
        getcurrentDate();

    }
    //Chargesheet notification API call//
    public void callChargesheetNotification(){
        final String notValue;
        new Thread(new Runnable(){
            @Override
            public void run() {
                // Do network action in this function
                try{
                    //callNetworkOperation();

                    String update_chargesheet_notification = Constants.API_URL + Constants.METHOD_CHARGESHEET_NOTIFICATION_UPDATE ;
                    Log.e("URL", update_chargesheet_notification);
                    URL url = new URL(update_chargesheet_notification);

                    JSONObject postDataParams = new JSONObject();
                    postDataParams.put("user_id",user_id);
                    Log.e("params", postDataParams.toString());

                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(30000 /* milliseconds */);
                    conn.setConnectTimeout(30000 /* milliseconds */);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(getPostDataString(postDataParams));

                    writer.flush();
                    writer.close();
                    os.close();

                    int responseCode=conn.getResponseCode();

                    if (responseCode == HttpsURLConnection.HTTP_OK) {

                        String readStream = readStream(conn.getInputStream());
                        L.e("ChargeSheet NotificationCount response------- "+readStream);
                        JSONObject jsonObj = new JSONObject(readStream);
                       String notificationValue="";
                        if(jsonObj.optString("result")!=null && !jsonObj.optString("result").equalsIgnoreCase("")&& !jsonObj.optString("result").equalsIgnoreCase("null"))
                            notificationValue=jsonObj.optString("result");
                            //notificationValue="0";
                        else
                            notificationValue="0";
                        Utility.setUserNotificationCount(DashboardActivity.this,notificationValue);
                      /*  if(fromPage.equalsIgnoreCase("LOGIN")) {
                            final String notValue = notificationValue;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateNotificationCount(notValue);
                                }
                            });
                        }*/
                        final String notValue = notificationValue;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateNotificationCount(notValue);
                            }
                        });

                    }
                    else {
                        L.e("ChargeSheet NotificationCount response failed------- "+responseCode);
                    }

                }catch(Exception e){
                    L.e(e);
                }

            }
        }).start();


    }
    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
    public String readStream(InputStream in) {

        StringBuilder sb = new StringBuilder();

        try {
            Reader reader  = new InputStreamReader(in);
            int data = reader.read();
            while (data != -1) {
                char theChar = (char) data;
                data = reader.read();
                sb.append(theChar);

            }

            reader.close();
        }
        catch (Exception e) {

        }
        return sb.toString();
    }

    //Chargesheet notification API call//
    public void getcurrentDate()
    {
        Date date = new Date();
        Calendar calender = Calendar.getInstance();
        calender.setTime(date);

        calender.add(Calendar.DATE, -1);
        year = calender.get(Calendar.YEAR);
        month = calender.get(Calendar.MONTH)+1;
        day = calender.get(Calendar.DAY_OF_MONTH);

        if(day==1 || day == 21 || day == 31){
            tv_currentDate.setText(day+"st "+changeMonth(Integer.toString(month))/*+" "+year*/);

        }
        else if(day == 2 || day == 22){
            tv_currentDate.setText(day+"nd "+changeMonth(Integer.toString(month))/*+" "+year*/);
        }
        else if(day == 3 || day == 23){
            tv_currentDate.setText(day+"rd "+changeMonth(Integer.toString(month))/*+" "+year*/);
        }
        else {
            tv_currentDate.setText(day+"th "+changeMonth(Integer.toString(month))/*+" "+year*/);
        }

        selectedDate = day + "-" + month + "-"+year;
    }

    private void setData() {

        // pass current date


        sharedPrefs_dashboardData = getSharedPreferences(DashboardActivity.MyPREFERENCES_DASHBOARD, Context.MODE_PRIVATE);
        editor_dashboardData = sharedPrefs_dashboardData.edit();


        if(!sharedPrefs_forinnerData.getString(TAG_DIV_CODE_LIST_INNER,"").equalsIgnoreCase("")){
            div = sharedPrefs_forinnerData.getString(TAG_DIV_CODE_LIST_INNER,"");
        }
        else{
            div = "";
        }

        if(!sharedPrefs_forinnerData.getString(TAG_PS_ID_LIST_INNER,"").equalsIgnoreCase("")){
            ps = sharedPrefs_forinnerData.getString(TAG_PS_ID_LIST_INNER,"");
        }
        else{
            ps = "";
        }

        if(!sharedPrefs_forinnerData.getString(TAG_CRIME_CATEGORY_LIST_INNER,"").equalsIgnoreCase("")){
            crime = sharedPrefs_forinnerData.getString(TAG_CRIME_CATEGORY_LIST_INNER,"");
        }
        else{
            crime = "";
        }
        if(returnFlag==true || isFirstTime==true) {
            isFirstTime=false;
            returnFlag=false;
            DashboardApiCall(selectedDate, div, ps, crime);
        }

    }


    private void DashboardApiCall(String selectDate, String div, String ps, String crimeCat) {

        TaskManager taskManager = new TaskManager(DashboardActivity.this);
        taskManager.setMethod(Constants.METHOD_DASHBOARD);
        taskManager.setDashBoard(true);

        String[] keys = { "current_date", "div", "ps", "category","user_id"};
        String[] values = {selectDate.trim(), div.trim(), ps.trim(), crimeCat.trim(),Utility.getUserInfo(this).getUserId()};
        taskManager.doStartTask(keys, values, true);
    }


    public void parseDashboardResponse(String response){
        //System.out.println("parseDashboardResponse: "+ response);
        JSONObject jobj = null;
        try{
            jobj = new JSONObject(response);
            if(jobj != null && jobj.optString("status").equalsIgnoreCase("1")){
                JSONObject result = jobj.getJSONObject("result");
                    System.out.println("LENGTH:"+result.length());
                    tv_firCount.setText((Integer)result.opt("Firs")+"");
                    tv_arrestCount.setText((Integer)result.opt("Arrests")+"");
                    tv_cs_due.setText((Integer)result.opt("Cs_due")+"");
                    tv_disposalCount.setText((Integer)result.opt("Disposals")+"");
                    tv_waExecsCount.setText((Integer)result.opt("WA_Execs")+"");
                    tv_wawaRecvsCount.setText((Integer)result.opt("WA_Recvs")+"");
                    if((Integer)result.opt("WA_Fail")>0){
                        tv_wa_falling_due_count.setText((Integer) result.opt("WA_Fail") + "");
                        tv_wa_falling_due_count.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                    }
                    else
                    {
                        tv_wa_falling_due_count.setText((Integer) result.opt("WA_Fail") + "");
                        tv_wa_falling_due_count.setTextColor(getResources().getColor(android.R.color.black));

                    }
                    tv_wa_falling_total_count.setText((Integer)result.opt("WA_Total")+"");
                    tv_wa_due_count.setText((Integer)result.opt("WA_Not_Exec")+"");
                    tv_wa_pending_count.setText((Integer)result.opt("WA_Pending")+"");
                    tv_rtaCount.setText((Integer) result.opt("RTAs") + "");
                    tv_unnatural_deaths_count.setText((Integer) result.opt("Unnatural_death") + "");
                    unnaturalDeathTotalCount= result.opt("Unnatural_death")+"";

                   /* if(fromPage.equalsIgnoreCase("LOGIN")) {      //Checking is done whether dasboard is called from login page then only  chargesheet Notification  count API called
                         callChargesheetNotification();
                         fromPage="";
                     }*/
                     callChargesheetNotification();//Notifiction API called.
                     fromPage="";
            }
            else{
                showAlertDialog(this, Constants.SEARCH_ERROR_TITLE,Constants.ERROR_MSG_TO_RELOAD, false);
            }
        }
        catch(JSONException e){
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.iv_search:
                allSearchTabCall();
                break;

            case R.id.iv_crimeReview:
                //startActivity(new Intent(DashboardActivity.this,CrimeReviewActivity.class));
                Intent intent = new Intent(DashboardActivity.this, CrimeReviewActivity.class);
                intent.putExtra("SELECTED_DIV_CRIME_REV", div);
                intent.putExtra("SELECTED_PS_CRIME_REV", ps);
                intent.putExtra("SELECTED_CRIME_CRIME_REV", crime);
                startActivity(intent);
                break;

            /*case R.id.iv_logout :
                logoutTask();
                break;*/

            case R.id.iv_mis :
                Intent mintent = new Intent(DashboardActivity.this, MisActivity.class);
                startActivity(mintent);
                break;

            case R.id.rl_sdr :

                Intent sintent = new Intent(DashboardActivity.this, SDRSearchActivity.class);
                startActivity(sintent);
                break;

            case R.id.rl_bar :

                Intent bintent = new Intent(DashboardActivity.this, BarSearchActivity.class);
                startActivity(bintent);
                break;

            case R.id.rl_hotel:
                Intent htintent = new Intent(DashboardActivity.this, HotelSearchActivity.class);
                startActivity(htintent);
                break;


            case R.id.rl_calender:
                setDate(true);
                break;

            case R.id.rl_Fir:
                allFIRViewListResult();
                break;

            case R.id.rl_Arrest:
                allArrestViewListResult();
                break;
            case R.id.rl_cs_due:
                all_cs_dueViewListResult();
                break;

            case R.id.rl_Disposals:
                allDisposalViewResult();
                break;

            case R.id.rl_waExecs:
                allWarrantExcViewListResult();
                //Utility.showToast(this,"Work in progress","short");
                break;

            case R.id.rl_waRecvs:
                allWarrantRecvViewListResult();
                //Utility.showToast(this,"Work in progress","short");
                break;


            case R.id.rl_wa_falling_due:
                allWarrantFallingViewListResult("fail");
                //Utility.showToast(this,"Work in progress","short");
                break;
            case R.id.rl_wa_due:
                allWarrantFallingViewListResult("due");
                //Utility.showToast(this,"Work in progress","short");
                break;


            case R.id.rl_rta:
                allRTAViewListResult();
                break;

            case R.id.rl_wa_pending:
                allWarrantPendingViewListResult();
                //Utility.showToast(this,"Work in progress","short");
                break;

            case R.id.iv_captureFIR :
                captureFirFragmentOpen();
                break;
            case R.id.rl_unnatural_deaths :
                allUnnaturalDeathResult();
                break;
            case R.id.charseet_due_notification :
                L.e("NOTIFICATION VALUE---------------"+chargesheetNotificationCount.getText().toString());
                getChargesheetdetailsAsperRole(this);
                /*if(chargesheetNotificationCount.getText().toString().equals("0")) {
                    Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG_NO_UNREAD_NOTIFICATION, false);
                }
                else{
                    getChargesheetdetailsAsperRole();
                }*/
                break;
            case R.id.rl_special_services:

                if(Utility.getUserInfo(this).getSplSvcAllow().equals("1")) {
                    Intent inspl = new Intent(this, FingerprintAuthActivity.class);
                    startActivity(inspl);
                }
                else{

                    Utility.showAlertDialog(this,Constants.ERROR,"You are not authenticate to Proceed",false);

                }
                break;

        }

    }
   /* public void getChargesheetdetailsAsperRole(){
        if(role.equalsIgnoreCase("OC")|| role.equalsIgnoreCase("IC")) {
            Intent in = new Intent(DashboardActivity.this, ChargesheetDuePsListActivity.class);
            in.putExtra("SELECTED_DATE_ARREST", selectedDate);
            in.putExtra("SELECTED_DIV_ARREST", div);
            in.putExtra("SELECTED_PS_ARREST", ps);
            in.putExtra("SELECTED_CRIME_ARREST", crime);
            startActivity(in);
        }
        else{
            Intent in = new Intent(DashboardActivity.this, ChargesheetDueCaseListActivity.class);
           *//* in.putExtra("SELECTED_DATE_ARREST", selectedDate);
            in.putExtra("SELECTED_DIV_ARREST", div);
            in.putExtra("SELECTED_PS_ARREST", ps);
            in.putExtra("SELECTED_CRIME_ARREST", crime);*//*
            startActivity(in);

        }


    }
*/
    private void captureFirFragmentOpen(){

        CaptureFIRFragment captureFrag = new CaptureFIRFragment();
        FragmentManager fragManager = getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();

        relative_fragContain.setVisibility(View.VISIBLE);
        fullDashboardView.setVisibility(View.GONE);

        fragTransaction.replace(R.id.relative_fragContain, captureFrag, CaptureFIRFragment.TAG);
        fragTransaction.commit();
    }


    private void allFIRViewListResult(){
        if(!tv_firCount.getText().toString().equalsIgnoreCase("0")) {

            if (!Constants.internetOnline(this)) {
                showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
                return;
            }

            Intent in = new Intent(DashboardActivity.this, FIRTabFromDashboardActivity.class);
            in.putExtra("SELECTED_DATE", selectedDate);
            in.putExtra("SELECTED_DIV", div);
            in.putExtra("SELECTED_PS", ps);
            in.putExtra("SELECTED_CRIME", crime);
            startActivity(in);
        }
        else{
            showAlertDialog(DashboardActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_NO_RECORD, false);
        }
    }


    private void allArrestViewListResult(){
        if (!tv_arrestCount.getText().toString().equalsIgnoreCase("0")){

            if (!Constants.internetOnline(this)) {
                showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
                return;
            }
            Intent in = new Intent(DashboardActivity.this, ArrestTabFromDashboardActivity.class);
            in.putExtra("SELECTED_DATE_ARREST", selectedDate);
            in.putExtra("SELECTED_DIV_ARREST", div);
            in.putExtra("SELECTED_PS_ARREST", ps);
            in.putExtra("SELECTED_CRIME_ARREST", crime);

            startActivity(in);
        }
        else{
            showAlertDialog(DashboardActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_NO_RECORD, false);
        }
    }
   // all_cs_dueViewListResult
    private void all_cs_dueViewListResult(){
        if (!tv_cs_due.getText().toString().equalsIgnoreCase("0")){

            if (!Constants.internetOnline(this)) {
                showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
                return;
            }
            Intent in = new Intent(DashboardActivity.this, ChargesheetDuePsListActivity.class);
            in.putExtra("FROM_PAGE","DashboardActivity");
            in.putExtra("SELECTED_DATE", selectedDate);
            in.putExtra("SELECTED_DIV", div);
            in.putExtra("SELECTED_CRIME", crime);


            startActivity(in);
        }
        else{
            showAlertDialog(DashboardActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_NO_UNREAD_NOTIFICATION, false);
        }
    }


    private void allDisposalViewResult(){
        if (!tv_disposalCount.getText().toString().equalsIgnoreCase("0")){

            if (!Constants.internetOnline(this)) {
                showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
                return;
            }

            Intent in = new Intent(DashboardActivity.this, DisposalTabFromDashboardActivity.class);
            in.putExtra("SELECTED_DATE_DISPOSAL", selectedDate);
            in.putExtra("SELECTED_DIV_DISPOSAL", div);
            in.putExtra("SELECTED_PS_DISPOSAL", ps);
            in.putExtra("SELECTED_CRIME_DISPOSAL", crime);
            startActivity(in);
        }
        else{
            showAlertDialog(DashboardActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_NO_RECORD, false);
        }
    }

    private void allRTAViewListResult(){
        if(!tv_rtaCount.getText().toString().equalsIgnoreCase("0")) {

            if (!Constants.internetOnline(this)) {
                showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
                return;
            }

            Intent in = new Intent(DashboardActivity.this, RTATabFromDashboardActivity.class);
            in.putExtra("SELECTED_DATE_RTA", selectedDate);
            in.putExtra("SELECTED_DIV_RTA", div);
            in.putExtra("SELECTED_PS_RTA", ps);
            in.putExtra("SELECTED_CRIME_RTA", crime);
            startActivity(in);
        }
        else{
            showAlertDialog(DashboardActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_NO_RECORD, false);
        }
    }
    private void allUnnaturalDeathResult(){
        if(!tv_unnatural_deaths_count.getText().toString().equalsIgnoreCase("0")) {

            if (!Constants.internetOnline(this)) {
                showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
                return;
            }

            Intent in = new Intent(DashboardActivity.this, UnnaturalDeathListActivity.class);
            in.putExtra("UNNATURALDEATH_TOTAL_COUNT",unnaturalDeathTotalCount);
            in.putExtra("SELECTED_DATE_UND", selectedDate);
            in.putExtra("SELECTED_DIV_UND", div);
            in.putExtra("SELECTED_PS_UND", ps);
            in.putExtra("SELECTED_CRIME_RTA", crime);
            startActivity(in);
        }
        else{
            showAlertDialog(DashboardActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_NO_RECORD, false);
        }
    }



    private void allWarrantExcViewListResult(){
        if(!tv_waExecsCount.getText().toString().trim().equalsIgnoreCase("0")) {

            if (!Constants.internetOnline(this)) {
                showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
                return;
            }

            Intent in = new Intent(DashboardActivity.this, WarrantExcTabFromDashboardActivity.class);
            in.putExtra("SELECTED_DATE_WA_EX", selectedDate);
            in.putExtra("SELECTED_DIV_WA_EX", div);
            in.putExtra("SELECTED_PS_WA_EX", ps);
            startActivity(in);
        }
        else{
            showAlertDialog(DashboardActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_NO_RECORD, false);
        }
    }


    private void allWarrantRecvViewListResult(){
        if(!tv_wawaRecvsCount.getText().toString().equalsIgnoreCase("0")) {

            if (!Constants.internetOnline(this)) {
                showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
                return;
            }

            Intent in = new Intent(DashboardActivity.this, WarrantRecvTabFromDashboardActivity.class);
            in.putExtra("SELECTED_DATE_WA_RECV", selectedDate);
            in.putExtra("SELECTED_DIV_WA_RECV", div);
            in.putExtra("SELECTED_PS_WA_RECV", ps);
            startActivity(in);
        }
        else{
            showAlertDialog(DashboardActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_NO_RECORD, false);
        }
    }

    private void allWarrantFallingViewListResult(String type){
        if(type.equalsIgnoreCase("fail")&& !tv_wa_falling_due_count.getText().toString().equalsIgnoreCase("0"))
        {
            callingList(type);

        }
        else if(type.equalsIgnoreCase("due")&& !tv_wa_due_count.getText().toString().toString().equalsIgnoreCase("0")){
            callingList(type);
        }
        else{
            showAlertDialog(DashboardActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_NO_RECORD, false);
        }
    }
    private  void callingList(String type){
        if (!Constants.internetOnline(this)) {
            showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        Intent in = new Intent(DashboardActivity.this, WarrentFallingDuePsActivity.class);
        in.putExtra("SELECTED_DATE_WA_FALL", selectedDate);
        in.putExtra("SELECTED_DIV_WA_FALL", div);
        in.putExtra("SELECTED_PS_WA_FALL", ps);
        in.putExtra("WARRANT_TYPE",type);
        startActivity(in);
    }


    private void allWarrantPendingViewListResult(){
        if(!tv_wa_pending_count.getText().toString().equalsIgnoreCase("0")){

            if (!Constants.internetOnline(this)) {
                showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
                return;
            }

            Intent in = new Intent(DashboardActivity.this, WarrantPendingTabFromDashboardActivity.class);
            in.putExtra("SELECTED_DATE_WA_PENDING", selectedDate);
            in.putExtra("SELECTED_DIV_WA_PENDING", div);
            in.putExtra("SELECTED_PS_WA_PENDING", ps);
            startActivity(in);

        }
        else{
            showAlertDialog(DashboardActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_NO_RECORD, false);
        }
    }


    private void allSearchTabCall(){

        if (!Constants.internetOnline(this)) {
            showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }
        startActivity(new Intent(DashboardActivity.this,
                After_login.class));
    }



    public void setDate(boolean isMaxCurrentDate){
        Calendar calender = Calendar.getInstance();
        DatePickerDialog mDialog = new DatePickerDialog(DashboardActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        month++;
                        if(day==1 || day == 21 || day == 31){
                            tv_currentDate.setText(day+"st "+changeMonth(Integer.toString(month))/*+" "+year*/);
                        }
                        else if(day == 2 || day == 22){
                            tv_currentDate.setText(day+"nd "+changeMonth(Integer.toString(month))/*+" "+year*/);
                        }
                        else if(day == 3 || day == 23){
                            tv_currentDate.setText(day+"rd "+changeMonth(Integer.toString(month))/*+" "+year*/);
                        }
                        else {
                            tv_currentDate.setText(day+"th "+changeMonth(Integer.toString(month))/*+" "+year*/);
                        }

                        selectedDate = day+"-"+month+"-"+year;
                        //DashboardApiCall(selectedDate, "", "", "");

                        DashboardApiCall(selectedDate, div, ps, crime);

                    }
                }, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender
                .get(Calendar.DAY_OF_MONTH));
        if(isMaxCurrentDate){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                mDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                mDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            }
        }
        mDialog.show();
    }


    public static String changeMonth(String month) {

        switch(month) {
            case "1" :
                month = "Jan";
                break;
            case "2" :
                month = "Feb";
                break;
            case "3" :
                month = "Mar";
                break;
            case "4" :
                month = "Apr";
                break;
            case "5" :
                month = "May";
                break;
            case "6" :
                month = "Jun";
                break;
            case "7" :
                month = "Jul";
                break;
            case "8" :
                month = "Aug";
                break;
            case "9" :
                month = "Sep";
                break;
            case "10" :
                month = "Oct";
                break;
            case "11" :
                month = "Nov";
                break;
            case "12" :
                month = "Dec";
                break;
            default:
                break;

        }

        return month;

    }


    private void releaseNoteShow(){

        AlertDialog adb = new AlertDialog.Builder(this,R.style.CustomDialogTheme).create();
        LayoutInflater adbInflater = LayoutInflater.from(this);
        View alertLayout = adbInflater.inflate(R.layout.release_note_layout, null);

        settings = getSharedPreferences(PREFS_NAME2, 0);
        String skipMessage = settings.getString("skipMessage", "NOT checked");
        String appVersion = settings.getString("appVersion", "1.0.1");

        dontShowAgain = (CheckBox) alertLayout.findViewById(R.id.skip);
        adb.setView(alertLayout);
        adb.setTitle("What's new !!!");
        adb.setMessage(getResources().getString(R.string.whats_new));

        adb.setButton(DialogInterface.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String checkBoxResult = "NOT checked";
                String appVer = "1.0.1";

                if (dontShowAgain.isChecked()) {
                    checkBoxResult = "checked";
                    appVer = Utility.getAppVersion(DashboardActivity.this);
                    Log.e("TAG:","TAG 11 "+ appVer);
                }

                Log.e("TAG:","TAG 12 "+ appVer);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("skipMessage", checkBoxResult);
                editor.putString("appVersion", appVer);
                editor.commit();
                return;
            }
        });

        if (!skipMessage.equals("checked") || !appVersion.equals(Utility.getAppVersion(DashboardActivity.this))) {
            Log.e("TAG:","TAG 13 "+ appVersion);
            adb.show();
            adb.setCanceledOnTouchOutside(false);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode){
            case DashboardActivity.PREFERENCE_REQUEST_CODE :
                returnFlag=true;
                L.e("onActivityResult called--------------");
                if(resultCode == RESULT_OK){
                    L.e("onActivityResult2 called-------------");
                    div = data.getExtras().getString("DIV_PREFERENCE");
                    ps = data.getExtras().getString("PS_PREFERENCE");
                    crime = data.getExtras().getString("CRIME_CAT_PREFERENCE");

                    //DashboardApiCall(response_date, response_div, response_ps, response_crime);
                    //recreate();
                    DashboardApiCall(selectedDate,div,ps,crime);

                }
                else{
                    Log.e("MOITRI::","RESULT CANCEL ----");
                }
                break;
        }
    }




    @Override
    protected void onResume() {
        super.onResume();
        L.e("onresumed  called");

        if(Constants.isShowWhatsNew){
            releaseNoteShow();
            Constants.isShowWhatsNew = false;
        }
        else{
            Constants.isShowWhatsNew = false;
        }

        //

        setData();
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBackPressed() {

        Fragment frag = getSupportFragmentManager().findFragmentByTag(CaptureFIRFragment.TAG);

        if(frag != null){
            getSupportFragmentManager().beginTransaction().remove(frag).commit();
            relative_fragContain.setVisibility(View.GONE);
            fullDashboardView.setVisibility(View.VISIBLE);
            return;
        }
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = false;
        //Toast.makeText(this, "Double tap to exit", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Logout ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "Yes" , then user is allowed to exit from application
                logoutTask();
                finish();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No" , just cancel this dialog and  continue with app
                dialog.cancel();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
        L.e("stopservice called -------------");
        Intent intent = new Intent(DashboardActivity.this, ChargesheetDueNotificationService.class);
        stopService(intent);


    }
    @Override
    protected  void onStop(){
        super.onStop();
    }

    @Override
    public void onChargesheetNotificationUpdate(String value) {
        chargesheetNotificationCount.setText(value);

    }

   public void updateNotificationCount(String notificationCount){
        int count = Integer.parseInt(notificationCount);
        Log.e("notificationCount---",notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }

   /* public void setUpdatedChargesheetNotification(String data){

        CoordinatorLayout coordinatorLayout=(CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        TextView textView=(TextView)coordinatorLayout.findViewById(R.id.tv_chargeSheet_notification_count);
        textView.setText(data);
    }
*/
   public void parseAllSearchAunthicatedResultDash(String response){
       if (response != null && !response.equals("")) {
           try {
               JSONObject jObj = new JSONObject(response);
               // L.e("response---------"+response);
               if (jObj.optString("status").equalsIgnoreCase("1")) {
                   if(jObj.optString("session_id")!=null && !jObj.optString("session_id").equalsIgnoreCase("")&& !jObj.optString("session_id").equalsIgnoreCase("null")) {
                       String session_id = jObj.optString("session_id");
                       //L.e("session_id---------"+session_id);
                       Utility.setDocumentSearchSesionId(DashboardActivity.this, session_id);


                   }
               }
               else{
                   showAlertDialog(this," Search Error ",Constants.ERROR_MSG_TO_RELOAD,false);
               }

           } catch (JSONException e) {
               e.printStackTrace();
           }
       }
   }

    public void parseDocumentSearchAunthicatedResultDashboard(String response){
        Log.e("AUTH_RESPONSE",response);
        if (response != null && !response.equals("")) {
            try {
                JSONObject jObj = new JSONObject(response);
                // L.e("response---------"+response);
                if (jObj.optString("status").equalsIgnoreCase("1")) {
                    if(jObj.optString("session_id")!=null && !jObj.optString("session_id").equalsIgnoreCase("")&& !jObj.optString("session_id").equalsIgnoreCase("null")) {
                        String session_id = jObj.optString("session_id");
                        //L.e("session_id---------"+session_id);
                        Utility.setDocumentSearchSesionId(DashboardActivity.this, session_id);
//                        pushFragments(documentTabid, new ModifiedDocumentsFragment().newInstance(), true, false);
                    }
                }
                else{
                    showAlertDialog(this," Search Error ",Constants.ERROR_MSG_TO_RELOAD,false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void parseDocumentHotelSearchAunthicatedResult(String result){
        if(result != null && !result.equals("")){
            Log.e("HOTEL_RESPONSE", result);
            try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.optString("status").equalsIgnoreCase("1")) {
                    if (!jObj.optString("auth_key").equalsIgnoreCase("") || !jObj.optString("auth_key").equalsIgnoreCase(null)) {
                        //Toast.makeText(getActivity(), "AUTH KEY Exist", Toast.LENGTH_SHORT).show();
                        isHotelAuthenticated = true;
                        Utility.setHotelSearchSesionId(this, jObj.optString("auth_key"));
                        //   Toast.makeText(getActivity(), "AUTH KEY--------"+jObj.optString("auth_key"), Toast.LENGTH_SHORT).show();
                        //viewPager.setCurrentItem(8);
                        //viewPager.setAdapter(new DocumentFragmentPagerAdapter(getChildFragmentManager(),isHotelAuthenticated,getContext()));

                    }
                    else {
                        //viewPager.setCurrentItem(0);
                        showAlertDialog(this, "Alert", "Something wrong please try again", false);
                    }
                }
                else{
                    //Toast.makeText(getActivity(), "AUTH KEY Absent", Toast.LENGTH_SHORT).show();
//                    viewPager.setCurrentItem(0);
                    showAlertDialog(this,"Alert","Something wrong please try again",false);

                }


            }catch (JSONException e){

                e.printStackTrace();
                Utility.showToast(this, "Some error has been encountered", "long");
            }
        }
    }
    public void getHotelSessionKey(){
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_DOCUMENT_HOTEL_SESSION_ID);
        taskManager.setSessionForDocumentHotelSearch(true);
//		String devicetoken = PushUtility.fetchC2DMToken(this);
        user_id= Utility.getUserInfo(this).getUserId();
        String[] keys = {"user_id","device_type","imei"};
       /* String[] values = {et_userid.getText().toString().trim(),
                et_password.getText().toString(), "android", devicetoken,Constants.device_IMEI_No};*///Constants.device_IMEI_No
        String[] values = {user_id,"android", Utility.getImiNO(DashboardActivity.this)};//Constants.device_IMEI_No, Utility.getImiNO(getActivity())
        taskManager.doStartTask(keys, values, true, true);
    }
}
