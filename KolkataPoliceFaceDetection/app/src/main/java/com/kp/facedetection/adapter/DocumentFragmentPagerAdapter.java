package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.Toast;

import com.kp.facedetection.fragments.BarSearchFragment;
import com.kp.facedetection.fragments.DLSearchFragment;
import com.kp.facedetection.fragments.DocumentEpicFragment;
import com.kp.facedetection.fragments.HotelSearchFragment;
import com.kp.facedetection.fragments.ModifiedCableDataSearchFragment;
import com.kp.facedetection.fragments.ModifiedGasDataSearchFragment;
import com.kp.facedetection.fragments.ModifiedKMCDataSearchFragment;
import com.kp.facedetection.fragments.SDRDataSearchFragment;
import com.kp.facedetection.fragments.VehicleSearchFragment;
import com.kp.facedetection.utils.L;

/**
 * Created by DAT-Asset-131 on 10-09-2016.
 */
public class DocumentFragmentPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 6;
    private String tabTitles[] = new String[] { "DL SEARCH", "VEHICLE SEARCH", "KMC DATA SEARCH" ,"GAS DATA SEARCH", "CABLE DATA SEARCH", "EPIC DATA"};
    Context context;
    private boolean hotelFlag =false;

    public DocumentFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public DocumentFragmentPagerAdapter(FragmentManager fm, boolean hotelFlag,Context context) {
        super(fm);
        this.hotelFlag = hotelFlag;
        this.context=context;

    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // This will show FirstFragment
                return DLSearchFragment.newInstance();
            case 1: // This will show SecondFragment
                return VehicleSearchFragment.newInstance();
            case 2: // This will show thirdFragment
                //return KMCDataSearchFragment.newInstance();
                return ModifiedKMCDataSearchFragment.newInstance();
            case 3: // This will show fourthFragment
                //return GasDataSearchFragment.newInstance();
                return ModifiedGasDataSearchFragment.newInstance();
            case 4: // This will show fifthFragment
                //return CableDataSearchFragment.newInstance();
                return ModifiedCableDataSearchFragment.newInstance();
            case 5: // This will show sixthFragment
                return DocumentEpicFragment.newInstance();
//            case 6: // This will show seventhFragment
//                return SDRDataSearchFragment.newInstance();
//            case 6: // This will show eighthFragment
//                return BarSearchFragment.newInstance();
//            case 7: // This will show eighthFragment
//                return HotelSearchFragment.newInstance();
                /*   if(hotelFlag) {
                       return HotelSearchFragment.newInstance();
                   }
                   else {
                       Toast.makeText(context, "Something wrong", Toast.LENGTH_SHORT).show();
                       return BarSearchFragment.newInstance();
                   }
*/
               // return HotelSearchFragment.newInstance();

            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
