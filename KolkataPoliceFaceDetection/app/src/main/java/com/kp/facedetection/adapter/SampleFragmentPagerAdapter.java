package com.kp.facedetection.adapter;

/**
 * Created by DAT-Asset-131 on 01-04-2016.
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.astuetz.PagerSlidingTabStrip;
import com.kp.facedetection.R;
import com.kp.facedetection.fragments.ModifiedArrestFragment;
import com.kp.facedetection.fragments.ModifiedFIRFragment;
import com.kp.facedetection.fragments.ModifiedProfileFragment;
import com.kp.facedetection.fragments.ModifiedWarrantFragment;



public class SampleFragmentPagerAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.IconTabProvider {
    final int PAGE_COUNT = 4;
    private int tabIcons[] = {R.drawable.profile2, R.drawable.fir2, R.drawable.warrant,R.drawable.arrest};
    private String trnid;
    private String ps_code;
    private String wa_year;
    private String wasl_no;
    private String crime_no;
    private String crime_year;
    private String case_no;
    private String case_year;
    private String sl_no;
    private String criminal_flag;
    private String alias;
    private String father_name;
    private String age;
    private String sex;
    private String nationality;
    private String religion;
    private String crs_generalID;
    private String criminal_name;
    private String prov_crm_no = "";


    public SampleFragmentPagerAdapter(FragmentManager fm, String criminal_flag, String trnid , String ps_code, String wa_year, String wasl_no, String crime_no, String crime_year, String case_no, String case_year, String sl_no,String alias,String father_name,String age,String sex,String nationality,String religion,String crs_generalID,String criminal_name,String prov_crm_no) {
        super(fm);

        this.criminal_flag = criminal_flag;
        this.trnid = trnid;
        this.ps_code = ps_code;
        this.wa_year = wa_year;
        this.wasl_no = wasl_no;
        this.crime_no = crime_no;
        this.crime_year = crime_year;
        this.case_no = case_no;
        this.case_year = case_year;
        this.sl_no = sl_no;
        this.alias = alias;
        this.father_name = father_name;
        this.age = age;
        this.sex = sex;
        this.nationality = nationality;
        this.religion = religion;
        this.crs_generalID = crs_generalID;
        this.criminal_name = criminal_name;
        this.prov_crm_no = prov_crm_no;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show Profile Fragment
                //return ProfileFragment.newInstance(0,criminal_flag,trnid, ps_code,crime_no,crime_year,case_no,case_year,sl_no,wa_year,wasl_no);
                return ModifiedProfileFragment.newInstance(0,criminal_flag,alias,father_name,age,sex,nationality,religion,case_no,case_year,ps_code,sl_no,crs_generalID,crime_no,crime_year,prov_crm_no);
            case 1: // Fragment # 0 - This will show FirstFragment different title
                //return FIRFragment.newInstance(1, ps_code,crime_no,crime_year,case_no,case_year,sl_no);
                return ModifiedFIRFragment.newInstance(1, criminal_flag, crime_no, crime_year,case_no,case_year,ps_code,sl_no,criminal_name, prov_crm_no);
            case 2: // Fragment # 1 - This will show SecondFragment
               // return WarrentFragment.newInstance(2,ps_code,wa_year,wasl_no);
                return ModifiedWarrantFragment.newInstance(2, criminal_flag, wa_year, wasl_no, ps_code, crime_no, crime_year,prov_crm_no);
            case 3: // Fragment # 1 - This will show Arrest Fragment
                //return ArrestFragment.newInstance(3, trnid,ps_code);
                return ModifiedArrestFragment.newInstance(3, criminal_flag, trnid, ps_code, crime_no, crime_year, case_no, case_year, prov_crm_no);
            default:
                return null;
        }
    }

    @Override
    public int getPageIconResId(int position) {
        return tabIcons[position];
    }
}
