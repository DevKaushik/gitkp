package com.kp.facedetection.utility;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.os.Build;
import android.os.CancellationSignal;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.R;
import com.kp.facedetection.SpecialServiceActivity;
import com.kp.facedetection.SpecialServiceList;


/**
 * Created by user on 20-03-2018.
 */

@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManagerCompat.AuthenticationCallback  {

    private Context context;


    // Constructor
    public FingerprintHandler(Context mContext) {
        context = mContext;
    }


    public void startAuth(FingerprintManagerCompat manager, FingerprintManagerCompat.CryptoObject cryptoObject) {
        android.support.v4.os.CancellationSignal cancellationSignal = new android.support.v4.os.CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject,0, cancellationSignal, this, null);
    }


    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
         this.update("Fingerprint Authentication error\n" + errString, false);
        //showAlertDialog(context,Constants.ERROR,"Fingerprint Authentication error",false);

    }


    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
       // this.update("Fingerprint Authentication help\n" + helpString, false);


    }


    @Override
    public void onAuthenticationFailed() {
          this.update("Fingerprint Authentication failed.", false);
        //showAlertDialog(context,Constants.ERROR,"Fingerprint Authentication failed.",false);
    }


    @Override
    public void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
       // this.update("Fingerprint Authentication succeeded.", true);
        showAlertDialog(context,"Success","Fingerprint Authentication succeeded.",true);
    }


    public void update(String e, Boolean success){
        TextView textView = (TextView) ((Activity)context).findViewById(R.id.errorText);
        textView.setText(e);
        Constants.changefonts(textView, context,"Calibri Bold.ttf");

    }

    public  void showAlertDialog(final Context context, String title, String message, final Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);


        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }
                        Log.e("Jay","Rank:" + Utility.getUserInfo(context).getUserRank());
                        Log.e("Jay","Divcode:" + Utility.getUserInfo(context).getUserDivisionCode());
                     //   Toast.makeText(context, "Rank:"+ Utility.getUserInfo(context).getUserRank(), Toast.LENGTH_SHORT).show();
                     //   Toast.makeText(context, "Divcode:"+ Utility.getUserInfo(context).getUserDivisionCode(), Toast.LENGTH_SHORT).show();

                        if(status){
                            if(!Utility.getUserInfo(context).getUserDivisionCode().equalsIgnoreCase("ORS")) {
                                if(!Utility.getUserInfo(context).getUserRank().equalsIgnoreCase("SI 1") && !Utility.getUserInfo(context).getUserRank().equalsIgnoreCase("CV") && !Utility.getUserInfo(context).getUserRank().equalsIgnoreCase("")) {
                                    Intent it = new Intent(context, SpecialServiceList.class);
                                    context.startActivity(it);
                                }
                                else{
                                    update("You are not Authenticated  to proceed contact e-governance ", false);

                                }

                            }
                            else{
                                update("You are not Authenticated  to proceed contact e-governance", false);

                            }
                        }else{
                               update("You are not Authenticated ", false);

                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

}

