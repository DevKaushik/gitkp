package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by user on 19-02-2018.
 */

public class HotelDetails implements Serializable{
    String id;
    String name ;
    String dateOfBirth;
    String age;
    String sex;
    String fatherName;
    String nationality;
    String address;
    String address_foreign;
    String idNumber;
    String idtype;
    String commingFrom;
    String proceedingTo;
    String adult;
    String child;
    String contactNumber;
    String contactNumberForeign;
    String passportNo;
    String placeOfIssuePassport;
    String dateOfIssuePasport;
    String passportVlidTill;
    String vissaNo;
    String vissaIsueDate;
    String vissaValidTill;
    String typeOfVissa;
    String vissaIssuePlace;
    String arrivalDate;
    String depatureDate;
    String hotelCode;
    String hotelName;
    String hoteladdress;
    String hotelPs;
    String hotelphone;
    String hotelRoomNo;
    String hotelStatus;
    String visitPurpose;
    String policeArea;
    String division;
    String image;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getAddress_foreign() {
        return address_foreign;
    }

    public void setAddress_foreign(String address_foreign) {
        this.address_foreign = address_foreign;
    }

    public String getContactNumberForeign() {
        return contactNumberForeign;
    }

    public void setContactNumberForeign(String contactNumberForeign) {
        this.contactNumberForeign = contactNumberForeign;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getPlaceOfIssuePassport() {
        return placeOfIssuePassport;
    }

    public void setPlaceOfIssuePassport(String placeOfIssuePassport) {
        this.placeOfIssuePassport = placeOfIssuePassport;
    }

    public String getDateOfIssuePasport() {
        return dateOfIssuePasport;
    }

    public void setDateOfIssuePasport(String dateOfIssuePasport) {
        this.dateOfIssuePasport = dateOfIssuePasport;
    }

    public String getPassportVlidTill() {
        return passportVlidTill;
    }

    public void setPassportVlidTill(String passportVlidTill) {
        this.passportVlidTill = passportVlidTill;
    }

    public String getVissaNo() {
        return vissaNo;
    }

    public void setVissaNo(String vissaNo) {
        this.vissaNo = vissaNo;
    }

    public String getVissaIsueDate() {
        return vissaIsueDate;
    }

    public void setVissaIsueDate(String vissaIsueDate) {
        this.vissaIsueDate = vissaIsueDate;
    }

    public String getVissaValidTill() {
        return vissaValidTill;
    }

    public void setVissaValidTill(String vissaValidTill) {
        this.vissaValidTill = vissaValidTill;
    }

    public String getTypeOfVissa() {
        return typeOfVissa;
    }

    public void setTypeOfVissa(String typeOfVissa) {
        this.typeOfVissa = typeOfVissa;
    }

    public String getVissaIssuePlace() {
        return vissaIssuePlace;
    }

    public void setVissaIssuePlace(String vissaIssuePlace) {
        this.vissaIssuePlace = vissaIssuePlace;
    }

    public String getHotelCode() {
        return hotelCode;
    }

    public void setHotelCode(String hotelCode) {
        this.hotelCode = hotelCode;
    }

    public String getHotelPs() {
        return hotelPs;
    }

    public void setHotelPs(String hotelPs) {
        this.hotelPs = hotelPs;
    }

    public String getHotelphone() {
        return hotelphone;
    }

    public void setHotelphone(String hotelphone) {
        this.hotelphone = hotelphone;
    }

    public String getHotelStatus() {
        return hotelStatus;
    }

    public void setHotelStatus(String hotelStatus) {
        this.hotelStatus = hotelStatus;
    }

    public String getIdtype() {
        return idtype;
    }

    public void setIdtype(String idtype) {
        this.idtype = idtype;
    }

    public String getCommingFrom() {
        return commingFrom;
    }

    public void setCommingFrom(String commingFrom) {
        this.commingFrom = commingFrom;
    }

    public String getProceedingTo() {
        return proceedingTo;
    }

    public void setProceedingTo(String proceedingTo) {
        this.proceedingTo = proceedingTo;
    }

    public String getAdult() {
        return adult;
    }

    public void setAdult(String adult) {
        this.adult = adult;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getHoteladdress() {
        return hoteladdress;
    }

    public void setHoteladdress(String hoteladdress) {
        this.hoteladdress = hoteladdress;
    }

    public String getHotelRoomNo() {
        return hotelRoomNo;
    }

    public void setHotelRoomNo(String hotelRoomNo) {
        this.hotelRoomNo = hotelRoomNo;
    }

    public String getVisitPurpose() {
        return visitPurpose;
    }

    public void setVisitPurpose(String visitPurpose) {
        this.visitPurpose = visitPurpose;
    }

    public String getPoliceArea() {
        return policeArea;
    }

    public void setPoliceArea(String policeArea) {
        this.policeArea = policeArea;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getDepatureDate() {
        return depatureDate;
    }

    public void setDepatureDate(String depatureDate) {
        this.depatureDate = depatureDate;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }
}
