
package com.kp.facedetection.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "request_details_id",
        "request_id",
        "from_date",
        "to_date",
        "mobile_number",
        "ipaddress",
        "cell_id",
        "request_sl_no",
        "imei_number",
        "submit_datetime",
        "submit_by",
        "from_time",
        "to_time",
        "date",
        "time",
        "provider",
        "circle",
        "record_id",
        "logger_id",
        "logger_number",
        "officerPhone",
        "officerName",
        "logger_sub_group",
        "logger_sdr_provider",
        "logger_sdr_circle",
        "logger_sdr_holder",
        "logger_sdr_plan_type",
        "logger_sdr_address",
        "logger_sdr_entry_by",
        "logger_entry_time",
        "li_disconnected",
        "li_reconnected",
        "disconnected_time",
        "li_reconnected_time",
        "partial_reply_flag",
        "chk_no_app_list",
        "enable_disable_designated",
        "parallal_connection_designated",
        "reconnection_designated",
})
public class Detail implements Serializable{

    @JsonProperty("request_details_id")
    private String requestDetailsId;
    @JsonProperty("request_id")
    private String requestId;
    @JsonProperty("from_date")
    private String fromDate;
    @JsonProperty("to_date")
    private String toDate;
    @JsonProperty("mobile_number")
    private String mobileNumber;
    @JsonProperty("ipaddress")
    private String ipaddress;
    @JsonProperty("cell_id")
    private String cellId;
    @JsonProperty("request_sl_no")
    private String requestSlNo;
    @JsonProperty("imei_number")
    private String imeiNumber;
    @JsonProperty("submit_datetime")
    private String submitDatetime;
    @JsonProperty("submit_by")
    private String submitBy;
    @JsonProperty("from_time")
    private String fromTime;
    @JsonProperty("to_time")
    private String toTime;
    @JsonProperty("date")
    private String date;
    @JsonProperty("time")
    private String time;
    @JsonProperty("provider")
    private String provider;
    @JsonProperty("circle")
    private String circle;
    @JsonProperty("record_id")
    private String recordId;
    @JsonProperty("logger_id")
    private String loggerId;
    @JsonProperty("logger_number")
    private String loggerNumber;
    @JsonProperty("officerPhone")
    private String officerPhone;
    @JsonProperty("officerName")
    private String officerName;
    @JsonProperty("logger_sub_group")
    private String loggerSubGroup;
    @JsonProperty("logger_sdr_provider")
    private String loggerSdrProvider;
    @JsonProperty("logger_sdr_circle")
    private String loggerSdrCircle;
    @JsonProperty("logger_sdr_holder")
    private String loggerSdrHolder;
    @JsonProperty("logger_sdr_plan_type")
    private String loggerSdrPlanType;
    @JsonProperty("logger_sdr_address")
    private String loggerSdrAddress;
    @JsonProperty("logger_sdr_entry_by")
    private String loggerSdrEntryBy;
    @JsonProperty("logger_entry_time")
    private String loggerEntryTime;
    @JsonProperty("li_disconnected")
    private String liDisconnected;
    @JsonProperty("li_reconnected")
    private String liReconnected;
    @JsonProperty("disconnected_time")
    private String disconnectedTime;
    @JsonProperty("li_reconnected_time")
    private String liReconnectedTime;
    @JsonProperty("partial_reply_flag")
    private String partialReplyFlag;
    @JsonProperty("enable_disable_designated")
    private String enableDisableDesignated;
    @JsonProperty("parallal_connection_designated")
    private String parallalConnectionDesignated;
    @JsonProperty("reconnection_designated")
    private String reConnectionDesignated;
    @JsonProperty("chk_no_app_list")
    private List<ChkNoAppList> chkNoAppList = null;

    @JsonProperty("partial_reply_flag")
    public String getPartialReplyFlag() {
        return partialReplyFlag;
    }
    @JsonProperty("partial_reply_flag")
    public void setPartialReplyFlag(String partialReplyFlag) {
        this.partialReplyFlag = partialReplyFlag;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("request_details_id")
    public String getRequestDetailsId() {
        return requestDetailsId;
    }

    @JsonProperty("request_details_id")
    public void setRequestDetailsId(String requestDetailsId) {
        this.requestDetailsId = requestDetailsId;
    }

    @JsonProperty("request_id")
    public String getRequestId() {
        return requestId;
    }

    @JsonProperty("request_id")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @JsonProperty("from_date")
    public String getFromDate() {
        return fromDate;
    }

    @JsonProperty("from_date")
    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    @JsonProperty("to_date")
    public String getToDate() {
        return toDate;
    }

    @JsonProperty("to_date")
    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    @JsonProperty("mobile_number")
    public String getMobileNumber() {
        return mobileNumber;
    }

    @JsonProperty("mobile_number")
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @JsonProperty("ipaddress")
    public String getIpaddress() {
        return ipaddress;
    }

    @JsonProperty("ipaddress")
    public void setIpaddress(String ipaddress) {
        this.ipaddress = ipaddress;
    }

    @JsonProperty("cell_id")
    public String getCellId() {
        return cellId;
    }

    @JsonProperty("cell_id")
    public void setCellId(String cellId) {
        this.cellId = cellId;
    }

    @JsonProperty("request_sl_no")
    public String getRequestSlNo() {
        return requestSlNo;
    }

    @JsonProperty("request_sl_no")
    public void setRequestSlNo(String requestSlNo) {
        this.requestSlNo = requestSlNo;
    }

    @JsonProperty("imei_number")
    public String getImeiNumber() {
        return imeiNumber;
    }

    @JsonProperty("imei_number")
    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    @JsonProperty("submit_datetime")
    public String getSubmitDatetime() {
        return submitDatetime;
    }

    @JsonProperty("submit_datetime")
    public void setSubmitDatetime(String submitDatetime) {
        this.submitDatetime = submitDatetime;
    }

    @JsonProperty("submit_by")
    public String getSubmitBy() {
        return submitBy;
    }

    @JsonProperty("submit_by")
    public void setSubmitBy(String submitBy) {
        this.submitBy = submitBy;
    }

    @JsonProperty("from_time")
    public String getFromTime() {
        return fromTime;
    }

    @JsonProperty("from_time")
    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    @JsonProperty("to_time")
    public String getToTime() {
        return toTime;
    }

    @JsonProperty("to_time")
    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("time")
    public String getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(String time) {
        this.time = time;
    }

  /*  @JsonProperty("cell_id")
    public String getCell_id() {
        return cell_id;
    }
    @JsonProperty("cell_id")
    public void setCell_id(String cell_id) {
        this.cell_id = cell_id;
    }*/

    @JsonProperty("provider")
    public String getProvider() {
        return provider;
    }

    @JsonProperty("provider")
    public void setProvider(String provider) {
        this.provider = provider;
    }

    @JsonProperty("circle")
    public String getCircle() {
        return circle;
    }

    @JsonProperty("circle")
    public void setCircle(String circle) {
        this.circle = circle;
    }

    @JsonProperty("record_id")
    public String getRecordId() {
        return recordId;
    }

    @JsonProperty("record_id")
    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    @JsonProperty("logger_id")
    public String getLoggerId() {
        return loggerId;
    }

    @JsonProperty("logger_id")
    public void setLoggerId(String loggerId) {
        this.loggerId = loggerId;
    }

    @JsonProperty("logger_number")
    public String getLoggerNumber() {
        return loggerNumber;
    }

    @JsonProperty("logger_number")
    public void setLoggerNumber(String loggerNumber) {
        this.loggerNumber = loggerNumber;
    }

    @JsonProperty("officerPhone")
    public String getOfficerPhone() {
        return officerPhone;
    }

    @JsonProperty("officerPhone")
    public void setOfficerPhone(String officerPhone) {
        this.officerPhone = officerPhone;
    }

    @JsonProperty("officerName")
    public String getOfficerName() {
        return officerName;
    }

    @JsonProperty("officerName")
    public void setOfficerName(String officerName) {
        this.officerName = officerName;
    }

    @JsonProperty("logger_sub_group")
    public String getLoggerSubGroup() {
        return loggerSubGroup;
    }

    @JsonProperty("logger_sub_group")
    public void setLoggerSubGroup(String loggerSubGroup) {
        this.loggerSubGroup = loggerSubGroup;
    }

    @JsonProperty("logger_sdr_provider")
    public String getLoggerSdrProvider() {
        return loggerSdrProvider;
    }

    @JsonProperty("logger_sdr_provider")
    public void setLoggerSdrProvider(String loggerSdrProvider) {
        this.loggerSdrProvider = loggerSdrProvider;
    }

    @JsonProperty("logger_sdr_circle")
    public String getLoggerSdrCircle() {
        return loggerSdrCircle;
    }

    @JsonProperty("logger_sdr_circle")
    public void setLoggerSdrCircle(String loggerSdrCircle) {
        this.loggerSdrCircle = loggerSdrCircle;
    }

    @JsonProperty("logger_sdr_holder")
    public String getLoggerSdrHolder() {
        return loggerSdrHolder;
    }

    @JsonProperty("logger_sdr_holder")
    public void setLoggerSdrHolder(String loggerSdrHolder) {
        this.loggerSdrHolder = loggerSdrHolder;
    }

    @JsonProperty("logger_sdr_plan_type")
    public String getLoggerSdrPlanType() {
        return loggerSdrPlanType;
    }

    @JsonProperty("logger_sdr_plan_type")
    public void setLoggerSdrPlanType(String loggerSdrPlanType) {
        this.loggerSdrPlanType = loggerSdrPlanType;
    }

    @JsonProperty("logger_sdr_address")
    public String getLoggerSdrAddress() {
        return loggerSdrAddress;
    }

    @JsonProperty("logger_sdr_address")
    public void setLoggerSdrAddress(String loggerSdrAddress) {
        this.loggerSdrAddress = loggerSdrAddress;
    }

    @JsonProperty("logger_sdr_entry_by")
    public String getLoggerSdrEntryBy() {
        return loggerSdrEntryBy;
    }

    @JsonProperty("logger_sdr_entry_by")
    public void setLoggerSdrEntryBy(String loggerSdrEntryBy) {
        this.loggerSdrEntryBy = loggerSdrEntryBy;
    }

    @JsonProperty("logger_entry_time")
    public String getLoggerEntryTime() {
        return loggerEntryTime;
    }

    @JsonProperty("logger_entry_time")
    public void setLoggerEntryTime(String loggerEntryTime) {
        this.loggerEntryTime = loggerEntryTime;
    }

    @JsonProperty("li_disconnected")
    public String getLiDisconnected() {
        return liDisconnected;
    }

    @JsonProperty("li_disconnected")
    public void setLiDisconnected(String liDisconnected) {
        this.liDisconnected = liDisconnected;
    }

    @JsonProperty("li_reconnected")
    public String getLiReconnected() {
        return liReconnected;
    }

    @JsonProperty("li_reconnected")
    public void setLiReconnected(String liReconnected) {
        this.liReconnected = liReconnected;
    }

    @JsonProperty("disconnected_time")
    public String getDisconnectedTime() {
        return disconnectedTime;
    }

    @JsonProperty("disconnected_time")
    public void setDisconnectedTime(String disconnectedTime) {
        this.disconnectedTime = disconnectedTime;
    }

    @JsonProperty("li_reconnected_time")
    public String getLiReconnectedTime() {
        return liReconnectedTime;
    }

    @JsonProperty("li_reconnected_time")
    public void setLiReconnectedTime(String liReconnectedTime) {
        this.liReconnectedTime = liReconnectedTime;
    }
    @JsonProperty("enable_disable_designated")
    public String getEnableDisableDesignated() {
        return enableDisableDesignated;
    }
    @JsonProperty("enable_disable_designated")
    public void setEnableDisableDesignated(String enableDisableDesignated) {
        this.enableDisableDesignated = enableDisableDesignated;
    }
    @JsonProperty("parallal_connection_designated")
    public String getParallalConnectionDesignated() {
        return parallalConnectionDesignated;
    }
    @JsonProperty("parallal_connection_designated")
    public void setParallalConnectionDesignated(String parallalConnectionDesignated) {
        this.parallalConnectionDesignated = parallalConnectionDesignated;
    }
    @JsonProperty("reconnection_designated")
    public String getReConnectionDesignated() {
        return reConnectionDesignated;
    }
    @JsonProperty("reconnection_designated")
    public void setReConnectionDesignated(String reConnectionDesignated) {
        this.reConnectionDesignated = reConnectionDesignated;
    }


    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    @JsonProperty("chk_no_app_list")
    public List<ChkNoAppList> getChkNoAppList() {
        return chkNoAppList;
    }

    @JsonProperty("chk_no_app_list")
    public void setChkNoAppList(List<ChkNoAppList> chkNoAppList) {
        this.chkNoAppList = chkNoAppList;
    }
}
