package com.kp.facedetection.fragments;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.adapter.WarrantDetailsListAdapter;
import com.kp.facedetection.model.WarrantDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 14-07-2016.
 */
public class ModifiedWarrantFragment extends Fragment implements AdapterView.OnItemClickListener {

    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String FLAG = "FLAG";
    public static final String WA_YEAR = "WA_YEAR";
    public static final String WASLNO = "WASLNO";
    public static final String PS_CODE = "PS_CODE";
    public static final String CRIME_NO = "CRIME_NO";
    public static final String CRIME_YR = "CRIME_YR";
    public static final String PROV_CRM_NO = "PROV_CRM_NO";


    private int mPage;
    private String flag;
    private String wa_year = "";
    private String waslno = "";
    private String pscode = "";
    private String crimeno = "";
    private String crimeyear = "";
    private String prov_crm_no="";

    private ListView lv_arrest;
    private TextView tv_SlNo;
    private TextView tv_warrentName;
    private TextView tv_status;
    private TextView tv_noData;
    private LinearLayout linear_main;

    List<WarrantDetails> warrantDetailsList;
    WarrantDetailsListAdapter warrantDetailsListAdapter;

    View inflatedFIRview;   //view used for popUpWindow
    private PopupWindow popWindow;

    private String userName = "";


    public static ModifiedWarrantFragment newInstance(int page, String flag, String wa_year, String waslno, String ps_code, String crime_no, String crime_year,String prov_crm_no) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        args.putString(FLAG, flag);
        args.putString(WA_YEAR, wa_year);
        args.putString(WASLNO, waslno);
        args.putString(PS_CODE, ps_code);
        args.putString(CRIME_NO, crime_no);
        args.putString(CRIME_YR, crime_year);
        args.putString(PROV_CRM_NO, prov_crm_no);

        ModifiedWarrantFragment fragment = new ModifiedWarrantFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPage = getArguments().getInt(ARG_PAGE);
        flag = getArguments().getString(FLAG);
        wa_year = getArguments().getString(WA_YEAR).replace("WA_YEAR: ", "");
        waslno = getArguments().getString(WASLNO).replace("WASLNO: ", "");
        pscode = getArguments().getString(PS_CODE).replace("PSCODE: ", "");
        crimeno = getArguments().getString(CRIME_NO).replace("CRIMENO: ", "");
        crimeyear = getArguments().getString(CRIME_YR).replace("CRIMEYEAR: ", "");
        prov_crm_no = getArguments().getString(PROV_CRM_NO).trim();

        try {
            userName = Utility.getUserInfo(getActivity()).getName();
        } catch (Exception e) {
            e.printStackTrace();
        }


        System.out.println("--------------------------Warrant-----------------------------");
        System.out.println("Flag: " + flag);
        System.out.println("wa_year " + wa_year);
        System.out.println("waslno " + waslno);
        System.out.println("PS Code: " + pscode);
        System.out.println("Crime No: " + crimeno);
        System.out.println("Crime Yr: " + crimeyear);
        System.out.println("--------------------------Warrant-----------------------------");

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // View view = inflater.inflate(R.layout.fragment_details, container, false);
        View view = inflater.inflate(R.layout.fragment_fir, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        lv_arrest = (ListView) view.findViewById(R.id.lv_fir);
        tv_SlNo = (TextView) view.findViewById(R.id.tv_SlNo);
        tv_warrentName = (TextView) view.findViewById(R.id.tv_FirName);
        tv_status = (TextView) view.findViewById(R.id.tv_status);
        tv_noData = (TextView) view.findViewById(R.id.tv_noData);
        linear_main = (LinearLayout) view.findViewById(R.id.linear_main);

        linear_main.setVisibility(View.GONE);
        tv_noData.setVisibility(View.GONE);


        tv_SlNo.setText("SL.No");
        tv_warrentName.setText("Warrant");
        tv_status.setText("Status");

        tv_status.setTextSize(14);

        tv_warrentName.setGravity(Gravity.CENTER);
        Constants.changefonts(tv_SlNo, getActivity(), "Calibri.ttf");
        Constants.changefonts(tv_warrentName, getActivity(), "Calibri Bold.ttf");
        Constants.changefonts(tv_status, getActivity(), "Calibri.ttf");
        Constants.changefonts(tv_noData, getActivity(), "Calibri.ttf");

        getWarrantData();
    }

    private void getWarrantData() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_MODIFIED_WARRANT_DETAILS);
        taskManager.setCriminalModifiedWarrantDetails(true);

        if(flag.equalsIgnoreCase("CF")){
            String[] keys = {"flag", "crimeyear", "crimeno"};
            String[] values = {flag.trim(), crimeyear.trim(), crimeno.trim()};
            //String[] values = {"cf", "2011", "796"};

            taskManager.doStartTask(keys, values, true );
        }
        else if(flag.equalsIgnoreCase("OW")){
            String[] keys = {"flag", "wa_year", "waslno","pscode"};
            String[] values = {flag, wa_year, waslno,pscode};

            taskManager.doStartTask(keys, values, true );
        }
        else{
            String[] keys = {"flag", "prov_crm_no"};
            String[] values = {"OF", prov_crm_no};

            taskManager.doStartTask(keys, values, true );

        }



    }

    public void parseCriminalModifiedWarrantDetailsResponse(String response) {

        //System.out.println("parseCriminalModifiedArrestDetailsResponse: " + response);

        if (response != null && !response.equals("")) {

            try {
                JSONObject jObj = new JSONObject(response);

                if (jObj.optString("status").equalsIgnoreCase("1")) {

                    if (jObj.optString("message").contains("No warrant details found for this criminal")) {

                        linear_main.setVisibility(View.GONE);
                        tv_noData.setVisibility(View.VISIBLE);
                        tv_noData.setText("Sorry, no warrant details found");

                    }
                    else{
                        linear_main.setVisibility(View.VISIBLE);
                        tv_noData.setVisibility(View.GONE);

                        JSONArray result_jsonArray = jObj.getJSONArray("result");
                        if (result_jsonArray.length() > 0) {
                            parseJSONArrayForWarrant(result_jsonArray);
                        }
                        else {
                            System.out.println("No data found in result_Jsonarray");
                        }
                    }



                }
                else {
                    linear_main.setVisibility(View.GONE);
                    tv_noData.setVisibility(View.VISIBLE);
                    tv_noData.setText("Sorry, no warrant details found");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    private void parseJSONArrayForWarrant(JSONArray result_array) {

        WarrantDetails warrantDetails;
        warrantDetailsList = new ArrayList<WarrantDetails>();

        for (int i = 0; i < result_array.length(); i++) {

            warrantDetails = new WarrantDetails();

            try {
                JSONObject obj = result_array.getJSONObject(i);

                if(!obj.optString("WAISSUEDATE").equalsIgnoreCase("null"))
                    warrantDetails.setWaIssueDate(obj.optString("WAISSUEDATE"));
                if(!obj.optString("WANO").equalsIgnoreCase("null"))
                    warrantDetails.setWaNo(obj.optString("WANO"));
                if(!obj.optString("UNDER_SECTIONS").equalsIgnoreCase("null"))
                    warrantDetails.setUnderSection(obj.optString("UNDER_SECTIONS"));
                if(!obj.optString("WA_STATUS").equalsIgnoreCase("null"))
                    warrantDetails.setWaStatus(obj.optString("WA_STATUS"));
                if(!obj.optString("ISSUE_COURT").equalsIgnoreCase("null"))
                    warrantDetails.setIssuingCourt(obj.optString("ISSUE_COURT"));
                if(!obj.optString("PROCESS_NO").equalsIgnoreCase("null"))
                    warrantDetails.setProcessNo(obj.optString("PROCESS_NO"));
                if(!obj.optString("RETURNABLE_DATE_TO_COURT").equalsIgnoreCase("null"))
                    warrantDetails.setReturnableDateCourt(obj.optString("RETURNABLE_DATE_TO_COURT"));
                if(!obj.optString("WASUBTYPE").equalsIgnoreCase("null"))
                    warrantDetails.setWat_type(obj.optString("WASUBTYPE"));
                if(!obj.optString("OFFICER_TO_SERVE").equalsIgnoreCase("null"))
                    warrantDetails.setIoServe(obj.optString("OFFICER_TO_SERVE"));
                if(!obj.optString("REMARKS").equalsIgnoreCase("null"))
                    warrantDetails.setRemark(obj.optString("REMARKS"));

                warrantDetailsList.add(warrantDetails);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        warrantDetailsListAdapter = new WarrantDetailsListAdapter(getActivity(), warrantDetailsList);
        lv_arrest.setAdapter(warrantDetailsListAdapter);
        lv_arrest.setOnItemClickListener(this);

    }


    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String warrantName="Warrant No. "+warrantDetailsList.get(position).getWaNo()+" Dated "+warrantDetailsList.get(position).getWaIssueDate()+" of Sec. "+warrantDetailsList.get(position).getUnderSection();
        popUpWindowForWarrantListItem(view, position, warrantName);
    }

    private void popUpWindowForWarrantListItem(View view, int position, String itemName) {

        String issueCourt = warrantDetailsList.get(position).getIssuingCourt();
        String processNo = warrantDetailsList.get(position).getProcessNo();
        String type = warrantDetailsList.get(position).getWat_type();
        String returnableDate = warrantDetailsList.get(position).getReturnableDateCourt();
        String ioToServe = warrantDetailsList.get(position).getIoServe();
        String remarks = warrantDetailsList.get(position).getRemark();

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popUpWindow layout
        inflatedFIRview = layoutInflater.inflate(R.layout.warrant_popup, null, false);

        TextView tv_itemName = (TextView) inflatedFIRview.findViewById(R.id.tv_itemName);
        TextView tv_warrantNo = (TextView) inflatedFIRview.findViewById(R.id.tv_warrantNo);
        TextView tv_issuingCourtValue = (TextView) inflatedFIRview.findViewById(R.id.tv_issuingCourtValue);
        TextView tv_processNoValue = (TextView) inflatedFIRview.findViewById(R.id.tv_processNoValue);
        TextView tv_typeValue = (TextView) inflatedFIRview.findViewById(R.id.tv_typeValue);
        TextView tv_returnableDateValue = (TextView) inflatedFIRview.findViewById(R.id.tv_returnableDateValue);
        TextView tv_IOValue = (TextView) inflatedFIRview.findViewById(R.id.tv_IOValue);
        TextView tv_remarksValue = (TextView) inflatedFIRview.findViewById(R.id.tv_remarksValue);

        TextView tv_watermark = (TextView)inflatedFIRview.findViewById(R.id.tv_watermark);

         /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);

        Constants.changefonts(tv_itemName, getContext(), "Calibri Bold.ttf");


        tv_itemName.setText(itemName);
        tv_issuingCourtValue.setText(issueCourt);
        tv_processNoValue.setText(processNo);
        tv_typeValue.setText(type);
        tv_returnableDateValue.setText(returnableDate);
        tv_IOValue.setText(ioToServe);
        tv_remarksValue.setText(remarks);

        openPopupWindow();


    }

    private void openPopupWindow(){

        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // set height depends on the device size
        popWindow = new PopupWindow(inflatedFIRview, width,height-50, true );

        popWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindow.setHeight(WindowManager.LayoutParams.MATCH_PARENT);

        popWindow.setAnimationStyle(R.style.PopupAnimation);

        //to generate popUpWindow at bottom of ActionBar
        //popWindow.showAsDropDown(getActivity().getActionBar().getCustomView(), 0, 20);
        popWindow.showAtLocation(getActivity().getActionBar().getCustomView(),Gravity.CENTER,0,20);

        popWindow.setOutsideTouchable(true);
        popWindow.setFocusable(true);
        popWindow.getContentView().setFocusableInTouchMode(true);
        popWindow.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    System.out.println("back pressed while opened popup");
                    if(popWindow!=null && popWindow.isShowing()){
                        popWindow.dismiss();
                    }

                    return true;
                }
                return false;
            }
        });

    }


}
