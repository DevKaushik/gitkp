package com.kp.facedetection;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kp.facedetection.model.FIRWarrantDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;

import java.util.List;

/**
 * Created by DAT-Asset-131 on 21-07-2016.
 */
public class PopUpActivity extends Activity {

    private List<FIRWarrantDetails> firWarrantDetailsList;

    /** views of WARRANT popup window*/
    private TextView tv_issuingCourtValue;
    private TextView tv_processNoValue;
    private TextView tv_typeValue;
    private TextView tv_returnableDateValue;
    private TextView tv_IOValue;
    private TextView tv_presentStatusValue;
    private TextView tv_remarksValue;
    private TextView tv_itemName;
    private LinearLayout linear_warrantees;

    private String item_name="";

    /** views of WARRANT popup window*/
    private TextView tv_chargesheetItemName;
    private TextView tv_chargesheetNo;
    private TextView tv_chargesheetCourtValue;
    private TextView tv_chargesheetActsValue;
    private TextView tv_chargesheetTypeValue;
    private TextView tv_chargesheetSupplementaryValue;
    private TextView tv_chargesheetBriefFactValue;

    private String userName="";
    private String loginNumber="";
    private String appVersion = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setToolbar();

		/* off screen capture */
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        if(getIntent().getStringExtra("FROM")!=null){

//----------------------------------Activity Open From ModifiedFIRFragment Warrant Popup---------------------------------------//

            if(getIntent().getStringExtra("FROM").equalsIgnoreCase("ModifiedFIRFragment_WarrantDetails")){

                setContentView(R.layout.warrant_popup);

                item_name = getIntent().getStringExtra("ITEM_NAME");
                firWarrantDetailsList = (List<FIRWarrantDetails>) getIntent().getSerializableExtra("FIR_WARRANT_DETAILS_LIST");

                modifiedFIRFragmentWarrantInitialization();

            }

        }


    }

   /* private void setToolbar(){

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();


        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);

        tv_username.setText("Welcome "+userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }*/

    /*ModifiedFIRFragment Warrant
    * popup
    * Initialization*/

    private void modifiedFIRFragmentWarrantInitialization(){

        tv_itemName = (TextView)findViewById(R.id.tv_itemName);
        tv_issuingCourtValue = (TextView)findViewById(R.id.tv_issuingCourtValue);
        tv_processNoValue = (TextView)findViewById(R.id.tv_processNoValue);
        tv_typeValue = (TextView)findViewById(R.id.tv_typeValue);
        tv_returnableDateValue = (TextView)findViewById(R.id.tv_returnableDateValue);
        tv_IOValue = (TextView)findViewById(R.id.tv_IOValue);
        tv_presentStatusValue = (TextView)findViewById(R.id.tv_presentStatusValue);
        tv_remarksValue = (TextView)findViewById(R.id.tv_remarksValue);
        linear_warrantees = (LinearLayout)findViewById(R.id.linear_warrantees);

        Utility.changefonts(tv_itemName, PopUpActivity.this, "Calibri Bold.ttf");

        //set data into view

        tv_itemName.setText(item_name);
        tv_issuingCourtValue.setText(firWarrantDetailsList.get(0).getIssueCourt());
        tv_processNoValue.setText(firWarrantDetailsList.get(0).getProcessNo());
        tv_typeValue.setText(firWarrantDetailsList.get(0).getWaType());
        tv_returnableDateValue.setText(firWarrantDetailsList.get(0).getReturnableDateToCourt());
        tv_IOValue.setText(firWarrantDetailsList.get(0).getOfficerToServe());
        tv_presentStatusValue.setText(firWarrantDetailsList.get(0).getWaStatus());
        tv_remarksValue.setText(firWarrantDetailsList.get(0).getRemarks());

        if(firWarrantDetailsList.get(0).getFirWarranteesDetailsList().size()>0){

            int warranteesListSize = firWarrantDetailsList.get(0).getFirWarranteesDetailsList().size();

            for(int i=0;i<warranteesListSize;i++){

                LayoutInflater inflater = (LayoutInflater) this
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View v = inflater.inflate(R.layout.fir_warrant_list_item, null);

                RelativeLayout relative_warrantDetails = (RelativeLayout) v.findViewById(R.id.relative_warrantDetails);
                relative_warrantDetails.setId(i);
                TextView tv_SlNo = (TextView) v.findViewById(R.id.tv_SlNo);
                TextView tv_warranteesName = (TextView) v.findViewById(R.id.tv_warranteesName);
                TextView tv_warranteesAddress = (TextView) v.findViewById(R.id.tv_warranteesAddress);

                tv_SlNo.setText(firWarrantDetailsList.get(0).getFirWarranteesDetailsList().get(i).getSl_no());
                tv_warranteesName.setText(firWarrantDetailsList.get(0).getFirWarranteesDetailsList().get(i).getWarrantee_name());
                tv_warranteesAddress.setText(firWarrantDetailsList.get(0).getFirWarranteesDetailsList().get(i).getWarrantee_address());


                linear_warrantees.addView(v);
            }

        }
        else{

            TextView tv_warrantees = new TextView(this);
            tv_warrantees.setText("  No data available");
            tv_warrantees.setTextColor(ContextCompat.getColor(this, R.color.color_black));
            Utility.changefonts(tv_warrantees, this, "Calibri.ttf");

            linear_warrantees.addView(tv_warrantees);
            linear_warrantees.setGravity(Gravity.CENTER);
        }

    }

     /*ModifiedFIRFragment Warrant
    * popup
    * Initialization*/

    /*private void modifiedFIRFragmentChargesheetInitialization(){

        tv_chargesheetItemName=(TextView)inflatedFIRviewChargeSheet.findViewById(R.id.tv_itemName);
        tv_chargesheetNo = (TextView)inflatedFIRviewChargeSheet.findViewById(R.id.tv_chargesheetNo);
        tv_chargesheetCourtValue=(TextView)inflatedFIRviewChargeSheet.findViewById(R.id.tv_courtValue);
        tv_chargesheetActsValue=(TextView)inflatedFIRviewChargeSheet.findViewById(R.id.tv_actsValue);
        tv_chargesheetTypeValue=(TextView)inflatedFIRviewChargeSheet.findViewById(R.id.tv_typeValue);
        tv_chargesheetSupplementaryValue=(TextView)inflatedFIRviewChargeSheet.findViewById(R.id.tv_supplementaryValue);
        tv_chargesheetBriefFactValue=(TextView)inflatedFIRviewChargeSheet.findViewById(R.id.tv_briefFactValue);
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }


}

