package com.kp.facedetection.interfaces;

import android.view.View;

/**
 * Created by user on 26-03-2018.
 */

public interface OnItemClickListenerForIO {
    public void  onItemClickForIO(View v, int pos);
}
