package com.kp.facedetection.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by DAT-165 on 22-12-2016.
 */
public class PMReportList implements Serializable{

    private String status="";
    private String message= "";
    private ArrayList<StationListForPMReport> obj_StationList = new ArrayList<StationListForPMReport>();
    private ArrayList<DoneByListForPMReport> obj_doneByList = new ArrayList<DoneByListForPMReport>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<StationListForPMReport> getObj_StationList() {
        return obj_StationList;
    }

    public void setObj_StationList(StationListForPMReport obj) {
        obj_StationList.add(obj);
    }

    public ArrayList<DoneByListForPMReport> getObj_doneByList() {
        return obj_doneByList;
    }

    public void setObj_doneByList(DoneByListForPMReport obj) {
        obj_doneByList.add(obj);
    }
}
