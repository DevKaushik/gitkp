package com.kp.facedetection.fragments;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.kp.facedetection.R;
import com.kp.facedetection.SearchResultActivity;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.model.CriminalDetails;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class ImageSearchFragment extends Fragment {
	View rootLayout;
	int REQUEST_CAMERA = 0, SELECT_FILE = 1;
	Button btn_search, btn_selectphoto;
	ImageView iv_image;
	Bitmap imageBitmap;
	String imageLocation;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootLayout = View.inflate(getActivity(),
				R.layout.image_search_fragment_layout, null);
		initView();
		return rootLayout;
	}

	private void initView() {
		btn_search = (Button) rootLayout.findViewById(R.id.btn_search);
		btn_selectphoto = (Button) rootLayout
				.findViewById(R.id.btn_selectphoto);
		iv_image = (ImageView) rootLayout.findViewById(R.id.iv_image);

		Bitmap humanBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.human);
		iv_image.setImageBitmap(humanBitmap);
		btn_selectphoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				selectImage();
			}
		});

		btn_search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// BitmapDrawable drawable = (BitmapDrawable) iv_image
				// .getDrawable();
				// Bitmap bitmap = drawable.getBitmap();
				if (imageLocation != null && !imageLocation.equals("")
						&& imageBitmap != null)
					uploadImage();
				else {
					Toast.makeText(getActivity(),
							"Please choose an image to upload!",
							Toast.LENGTH_LONG).show();
				}
			}
		});

	}

	private void uploadImage() {

		if (!Constants.internetOnline(getActivity())) {
			Utility.showAlertDialog(getActivity(), Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
			return;

		}

		Log.e("IMAGE", "Location " + imageLocation);
		TaskManager taskManager = new TaskManager(this);
		taskManager.setUploadImage(true);
		taskManager.doStartTask(true, imageLocation, imageBitmap);
	}

	private void selectImage() {
		 final CharSequence[] items = { "Take Photo",
		 "Cancel" };
//		final CharSequence[] items = { "Take Photo", "Cancel",
//				"Choose from Library" };

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Add Photo!");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (items[item].equals("Take Photo")) {
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					startActivityForResult(intent, REQUEST_CAMERA);
				} else if (items[item].equals("Choose from Library")) {
					Intent intent = new Intent(
							Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					intent.setType("image/*");
					startActivityForResult(
							Intent.createChooser(intent, "Select File"),
							SELECT_FILE);
				} else if (items[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == SELECT_FILE)
				onSelectFromGalleryResult(data);
			else if (requestCode == REQUEST_CAMERA)
				onCaptureImageResult(data);
		}
	}

	private void onCaptureImageResult(Intent data) {
		imageBitmap = (Bitmap) data.getExtras().get("data");
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

		imageLocation = Environment.getExternalStorageDirectory()
				+ "/temp.jpeg";
		File destination = new File(imageLocation);

		FileOutputStream fo;
		try {
			destination.createNewFile();
			fo = new FileOutputStream(destination);
			fo.write(bytes.toByteArray());
			fo.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		iv_image.setImageDrawable(null);
		iv_image.setImageBitmap(imageBitmap);

	}

	@SuppressWarnings("deprecation")
	private void onSelectFromGalleryResult(Intent data) {
		Uri selectedImageUri = data.getData();
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();

		String[] projection = { MediaColumns.DATA };
		Cursor cursor = getActivity().managedQuery(selectedImageUri,
				projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
		cursor.moveToFirst();

		String selectedImagePath = cursor.getString(column_index);
		imageLocation = selectedImagePath;
		BitmapFactory.Options options = new BitmapFactory.Options();
		// options.inJustDecodeBounds = true;
		// BitmapFactory.decodeFile(selectedImagePath, options);
		final int REQUIRED_SIZE = 200;
		int scale = 1;
		while (options.outWidth / scale / 2 >= REQUIRED_SIZE
				&& options.outHeight / scale / 2 >= REQUIRED_SIZE)
			scale *= 2;
		options.inSampleSize = scale;
		options.inJustDecodeBounds = false;
		imageBitmap = BitmapFactory.decodeFile(selectedImagePath, options);

		imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
		imageLocation = Environment.getExternalStorageDirectory()
				.getAbsoluteFile() + "/temp.jpeg";
		File destination = new File(imageLocation);

		FileOutputStream fo;
		try {
			destination.createNewFile();
			fo = new FileOutputStream(destination);
			fo.write(bytes.toByteArray());
			fo.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		iv_image.setImageDrawable(null);
		iv_image.setImageBitmap(imageBitmap);
	}

	public void parseUploadResult(String response) {
		Log.e("Response", "Response " + response);
		try {
			int responseCode = Integer.parseInt(response);
			if (responseCode!=180 && responseCode > 0) {
				fetchMatchingImageData(response);
			} else {
				Utility.showOKDialogMessage(getActivity(), getResources()
						.getString(R.string.app_name), "No records found!");
			}
		} catch (NumberFormatException e) {
			Utility.showOKDialogMessage(getActivity(), getResources()
					.getString(R.string.app_name), "No records found!");
			e.printStackTrace();
		}

	}

	private void fetchMatchingImageData(String user_id) {

		TaskManager taskManager = new TaskManager(this);
		taskManager.setMethod(Constants.METHOD_PICTURE_SEARCH);
		taskManager.setFetchImageDetail(true);
		// String[] keys = {"picture_id" };
		String[] keys = { "user_id" };
		String[] values = { user_id.trim() };
		taskManager.doStartTask(keys, values, "", false, true);
	}

	public void parseMatchedImageDetails(String response) {
		// imageBitmap.recycle();
		try {
			if (response != null && !response.equals("")) {

				JSONObject jObj = new JSONObject(response);
				if (jObj.optBoolean("success")) {
					JSONArray jsonArray = jObj.optJSONArray("details");
					// JSONArray jsonArray = jObj.optJSONArray("result");
					if (jsonArray.length() > 0) {
						ArrayList<CriminalDetails> criminalList = Utility
								.parseCrimiinalRecords(getActivity(),
										jsonArray.optJSONArray(0));
						if (criminalList != null && criminalList.size() > 0) {
							Intent intent = new Intent(getActivity(),
									SearchResultActivity.class);
							intent.putExtra(Constants.OBJECT, criminalList);
							getActivity().startActivity(intent);
						} else {
							Utility.showOKDialogMessage(
									getActivity(),
									getResources().getString(R.string.app_name),
									"No records found!");
						}

					} else {
						Utility.showOKDialogMessage(getActivity(),
								getResources().getString(R.string.app_name),
								"No records found!");
					}
				}

			} else {
				Utility.showOKDialogMessage(getActivity(), getResources()
						.getString(R.string.app_name), "No records found!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		 * Log.e("Response1", "Response1 "+response); ArrayList<CriminalDetails>
		 * criminalList=Utility.parseCrimiinalRecords(getActivity(), response);
		 * if (criminalList!=null && criminalList.size() > 0) { Intent intent =
		 * new Intent(getActivity(), SearchResultActivity.class);
		 * intent.putExtra(Constants.OBJECT, criminalList);
		 * getActivity().startActivity(intent); }
		 */
	}
}
