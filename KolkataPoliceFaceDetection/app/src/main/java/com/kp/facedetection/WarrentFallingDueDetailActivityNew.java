package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.model.ModifiedWarrantDetailsModel;
import com.kp.facedetection.model.WarrantExecDetailModel;
import com.kp.facedetection.model.WarrantiesDetailOfDashboardWarrantModel;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.utils.OnSwipeTouchListener;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

public class WarrentFallingDueDetailActivityNew extends BaseActivity implements View.OnClickListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String selectedDate;
    private String selected_div;
    private String psCode;
    private String warrentType;
    private TextView tv_nbw;
    private TextView tv_bw;
    private RelativeLayout rl_content;
    private View nbwIndicator, bwIndicator;

    // Views for NBW
    private TextView tv_waNoValue;
    private TextView tv_returnDateValue;
    private TextView tv_statusValue;
    private TextView tv_caseRefValue, tv_caseRef;
    private TextView tv_issuedByVal;
    private TextView tv_processNoValue;
    private TextView tv_officerNameValue;
    private LinearLayout ll_otherDetails;
    private LinearLayout ll_warrantNer;
    private LinearLayout ll_warrantProcess;
    private LinearLayout ll_warrantProperties;


    private ScrollView ll_dataShow;
    private LinearLayout ll_noDataShow;
    private LinearLayout ll_NBW;

    // Views for BW
    private TextView tv_waNoValue_BW;
    private TextView tv_returnDateValue_BW;
    private TextView tv_statusValue_BW;
    private TextView tv_caseRefValue_BW, tv_caseRef_BW;
    private TextView tv_issuedByVal_BW;
    private TextView tv_processNoValue_BW;
    private TextView tv_officerNameValue_BW;
    private LinearLayout ll_otherDetails_BW;
    private LinearLayout ll_warrantNer_BW;
    private LinearLayout ll_warrantProcess_BW;
    private LinearLayout ll_warrantProperties_BW;
    private ScrollView ll_dataShow_BW;
    private LinearLayout ll_noDataShow_BW;
    private LinearLayout ll_BW;

    private View includeView;
    private boolean isNBWSelected = false;
    private boolean isBWSelected = false;
    private String[] keys;
    private String[] values;

    private ArrayList<WarrantExecDetailModel> warrantDetailsList = new ArrayList<>();
    private ArrayList<WarrantiesDetailOfDashboardWarrantModel> warrantiesList = new ArrayList<>();//arraylist for warantees
    private ArrayList<ModifiedWarrantDetailsModel> warrantNerList = new ArrayList<>();//arraylist for
    private WarrantExecDetailModel warrantExecDetailModel;

    private ArrayList<WarrantExecDetailModel> warrantDetailsList_BW = new ArrayList<>();
    private ArrayList<WarrantiesDetailOfDashboardWarrantModel> warrantiesList_BW = new ArrayList<>();
    private ArrayList<ModifiedWarrantDetailsModel> warrantNerList_BW = new ArrayList<>();//arraylist for
    private WarrantExecDetailModel warrantExecDetailModel_BW;

    private int pageNoNBW;
    private int pageNoBW;

    private int totalNBWCount;
    ;
    private int totalBWCount;

    private RelativeLayout bt_prevWA, bt_nextWA, rl_navigate, bottomView;
    private TextView tv_next, tv_prev;

    private boolean isDataInNbw = false;
    private TextView txt_count;
    private String tag = "";
    private int pgNBW = 0;
    private int pgBW = 0;
    int wa_position;
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.warrant_exc_layout);
        ObservableObject.getInstance().addObserver(this);

        setToolBar();
        initViews();
    }

    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }


    private void initViews() {
        selectedDate = getIntent().getExtras().getString("SELECT_DATE");
        selected_div = getIntent().getExtras().getString("SELECT_DIV");
        psCode = getIntent().getExtras().getString("PS_CODE");
        warrentType = getIntent().getExtras().getString("WARRANT_TYPE");
        wa_position = getIntent().getIntExtra("WA_POSITION", 0);


        rl_navigate = (RelativeLayout) findViewById(R.id.rl_navigate);
        bottomView = (RelativeLayout) findViewById(R.id.bottomView);
        includeView = findViewById(R.id.warrantData);
        tv_waNoValue = (TextView) includeView.findViewById(R.id.tv_waNoValue);
        tv_returnDateValue = (TextView) includeView.findViewById(R.id.tv_returnDateValue);
        tv_statusValue = (TextView) includeView.findViewById(R.id.tv_statusValue);
        tv_caseRef = (TextView) includeView.findViewById(R.id.tv_caseRef);
        tv_caseRefValue = (TextView) includeView.findViewById(R.id.tv_caseRefValue);
        tv_caseRefValue.setOnClickListener(this);
        tv_issuedByVal = (TextView) includeView.findViewById(R.id.tv_issuedByVal);
        tv_processNoValue = (TextView) includeView.findViewById(R.id.tv_processNoValue);
        tv_officerNameValue = (TextView) includeView.findViewById(R.id.tv_officerNameValue);
        ll_otherDetails = (LinearLayout) includeView.findViewById(R.id.ll_otherDetails);
        ll_warrantNer = (LinearLayout) includeView.findViewById(R.id.ll_warrant_ner_details);
        ll_warrantProcess = (LinearLayout) includeView.findViewById(R.id.ll_warrant_process_details);
        ll_warrantProperties = (LinearLayout) includeView.findViewById(R.id.ll_warrant_properties_details);

        ll_NBW = (LinearLayout) includeView.findViewById(R.id.ll_NBW);
        ll_NBW.setVisibility(View.GONE);
        ll_dataShow = (ScrollView) includeView.findViewById(R.id.ll_dataShow);
        ll_noDataShow = (LinearLayout) includeView.findViewById(R.id.ll_noDataShow);

        tv_waNoValue_BW = (TextView) includeView.findViewById(R.id.tv_waNoValue_BW);
        tv_returnDateValue_BW = (TextView) includeView.findViewById(R.id.tv_returnDateValue_BW);
        tv_statusValue_BW = (TextView) includeView.findViewById(R.id.tv_statusValue_BW);
        tv_caseRefValue_BW = (TextView) includeView.findViewById(R.id.tv_caseRefValue_BW);
        tv_caseRef_BW = (TextView) includeView.findViewById(R.id.tv_caseRefBW);
        tv_issuedByVal_BW = (TextView) includeView.findViewById(R.id.tv_issuedByVal_BW);
        tv_processNoValue_BW = (TextView) includeView.findViewById(R.id.tv_processNoValue_BW);
        tv_officerNameValue_BW = (TextView) includeView.findViewById(R.id.tv_officerNameValue_BW);
        ll_otherDetails_BW = (LinearLayout) includeView.findViewById(R.id.ll_otherDetails_BW);
        ll_warrantNer_BW = (LinearLayout) includeView.findViewById(R.id.ll_warrant_ner_details_BW);
        ll_warrantProcess_BW = (LinearLayout) includeView.findViewById(R.id.ll_warrant_process_details_BW);
        ll_warrantProperties_BW = (LinearLayout) includeView.findViewById(R.id.ll_warrant_properties_details_BW);

        ll_BW = (LinearLayout) includeView.findViewById(R.id.ll_BW);
        ll_BW.setVisibility(View.GONE);
        ll_dataShow_BW = (ScrollView) includeView.findViewById(R.id.ll_dataShow_BW);
        ll_noDataShow_BW = (LinearLayout) includeView.findViewById(R.id.ll_noDataShow_BW);


        nbwIndicator = findViewById(R.id.nbwIndicator);
        bwIndicator = findViewById(R.id.bwIndicator);
        nbwIndicator.setBackgroundColor(getResources().getColor(R.color.color_light_blue));
        bwIndicator.setBackgroundColor(getResources().getColor(R.color.color_indicator));

        txt_count = (TextView) findViewById(R.id.txt_count);
        tv_bw = (TextView) findViewById(R.id.tv_bw);
        tv_bw.setOnClickListener(this);

        tv_nbw = (TextView) findViewById(R.id.tv_nbw);
        tv_nbw.setOnClickListener(this);

        // first show NBW as default
        isNBWSelected = true;
        isBWSelected = false;

        ll_NBW.setVisibility(View.VISIBLE);
        ll_BW.setVisibility(View.GONE);

        tv_next = (TextView) findViewById(R.id.tv_next);
        tv_prev = (TextView) findViewById(R.id.tv_prev);

        bt_nextWA = (RelativeLayout) findViewById(R.id.bt_nextWA);
        bt_nextWA.setVisibility(View.GONE);
        bt_nextWA.setOnClickListener(this);

        bt_prevWA = (RelativeLayout) findViewById(R.id.bt_prevWA);
        bt_prevWA.setVisibility(View.GONE);
        bt_prevWA.setOnClickListener(this);

        setFontForViews();

        warrantNbwAPICall(1);
        warrantBWAPICall(1);

        swipePagerCall();
    }


    /*
   *  apply font to all views
   * */
    private void setFontForViews() {
        Constants.changefonts(tv_next, this, "Calibri.ttf");
        Constants.changefonts(tv_prev, this, "Calibri.ttf");

        // Views for NBW
        Constants.changefonts(tv_waNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_returnDateValue, this, "Calibri.ttf");
        Constants.changefonts(tv_statusValue, this, "Calibri.ttf");
        Constants.changefonts(tv_caseRefValue, this, "Calibri.ttf");
        Constants.changefonts(tv_issuedByVal, this, "Calibri.ttf");
        Constants.changefonts(tv_processNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_officerNameValue, this, "Calibri.ttf");


        // Views for BW
        Constants.changefonts(tv_waNoValue_BW, this, "Calibri.ttf");
        Constants.changefonts(tv_returnDateValue_BW, this, "Calibri.ttf");
        Constants.changefonts(tv_statusValue_BW, this, "Calibri.ttf");
        Constants.changefonts(tv_caseRefValue_BW, this, "Calibri.ttf");
        Constants.changefonts(tv_issuedByVal_BW, this, "Calibri.ttf");
        Constants.changefonts(tv_processNoValue_BW, this, "Calibri.ttf");
        Constants.changefonts(tv_officerNameValue_BW, this, "Calibri.ttf");

    }

    // API call for NBW
    private void warrantNbwAPICall(int pageNo) {

        TaskManager taskManager = new TaskManager(this);
        if (warrentType.equalsIgnoreCase(Constants.WARRENT_TYPE_FAIL)) {
            taskManager.setMethod(Constants.METHOD_ALL_WA_FALL_DETAILS);
        } else if (warrentType.equalsIgnoreCase(Constants.WARRENT_TYPE_DUE)) {
            taskManager.setMethod(Constants.METHOD_ALL_WA_DUE_DETAILS);

        }
        taskManager.setWaFallDueDetailSearchForNBW(true);

        keys = new String[]{"ps", "currDate", "watype", "pageno", "div"};
        values = new String[]{psCode.trim(), selectedDate.trim(), "NBW", "" + pageNo, selected_div.trim()};
        taskManager.doStartTask(keys, values, true);
    }


    public void parseWarrantRecvDetailsResultForNBW(String response) {
        //System.out.println("parseWarrantRecvDetailsResultForNBW " + response);

        try {
            JSONObject jObj = new JSONObject(response);
            if (jObj.optString("status").equalsIgnoreCase("1")) {

                isDataInNbw = true;
                if (isNBWSelected) {
                    showDataNBW();
                } else {
                    // NBW not selected,and all datapart view visible
                    parentHideWithAlldataSetNBW();
                }

                JSONArray resultArray = jObj.optJSONArray("result");
                pageNoNBW = Integer.parseInt(jObj.optString("pageno"));
                totalNBWCount = Integer.parseInt(jObj.optString("total"));
                parseAllWarrantExecData(resultArray);

                buttonVisibilityForNbw();

            } else {

                noDataTextSet_NBW();

                if (isNBWSelected) {

                    showNoDataFoundNBW();
                    isDataInNbw = false;
                } else {
                    // NBW not selected,and no data set text
                    parentHideWithNodataNBW();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void parseAllWarrantExecData(JSONArray resultArray) {
        if (resultArray != null && !resultArray.equals("")) {
            warrantDetailsList.clear();
            warrantiesList.clear();

            warrantExecDetailModel = new WarrantExecDetailModel();
            try {

                for (int i = 0; i < resultArray.length(); i++) {
                    JSONObject object = resultArray.optJSONObject(i);
                    warrantExecDetailModel.setWaNo(object.optString("WANO"));
                    warrantExecDetailModel.setReturnableDtaeToCourt(object.optString("RETURNABLE_DATE_TO_COURT"));
                    warrantExecDetailModel.setWaStatus(object.optString("WA_STATUS"));
                    warrantExecDetailModel.setPsCode(object.optString("PS"));
                    warrantExecDetailModel.setWaCaseNo(object.optString("CASENO"));
                    warrantExecDetailModel.setWaYear(object.optString("WA_YEAR"));
                    warrantExecDetailModel.setFirYear(object.optString("FIR_YR"));
                    warrantExecDetailModel.setUnderSec(object.optString("UNDER_SECTIONS"));
                    warrantExecDetailModel.setIssueCourt(object.optString("ISSUE_COURT"));
                    warrantExecDetailModel.setProcessNo(object.optString("PROCESS_NO"));
                    warrantExecDetailModel.setOfficerName(object.optString("IONAME"));
                    warrantExecDetailModel.setWaCat(object.optString("WACAT"));
                    warrantExecDetailModel.setActionDate(object.optString("ACTION_DATE"));
                    warrantExecDetailModel.setColorStatus(object.optString("fail_normal"));
                    warrantExecDetailModel.setWaDate(object.optString("casedate"));
                    if (object.has("warrenties")) {
                        JSONArray warrantiesArray = object.optJSONArray("warrenties");

                        for (int j = 0; j < warrantiesArray.length(); j++) {
                            JSONObject warrantiesObj = warrantiesArray.optJSONObject(j);
                            WarrantiesDetailOfDashboardWarrantModel warrantiesModel = new WarrantiesDetailOfDashboardWarrantModel();
                            warrantiesModel.setWaName(warrantiesObj.optString("NAME"));
                            warrantiesModel.setWaAddress(warrantiesObj.optString("ADDRESS"));
                            warrantiesModel.setWaFatherName(warrantiesObj.optString("FATHER_NAME"));
                            warrantExecDetailModel.setWarranties(warrantiesModel);
                        }
                    }
                    if (object.has("NON_EXEC_REPORT")) {

                        JSONArray warrantNerArray = object.optJSONArray("NON_EXEC_REPORT");
                        for (int j = 0; j < warrantNerArray.length(); j++) {
                          JSONObject warrantNerObj = warrantNerArray.optJSONObject(j);
                            ModifiedWarrantDetailsModel modifiedWarrantDetailsModel = new ModifiedWarrantDetailsModel();
                           if (warrantNerObj.optString("NERDATE") != null && !warrantNerObj.optString("NERDATE").equalsIgnoreCase("") && !warrantNerObj.optString("NERDATE").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel.setNerDate(warrantNerObj.optString("NERDATE"));
                            else
                                modifiedWarrantDetailsModel.setNerDate(warrantNerObj.optString("NA"));
                            if (warrantNerObj.optString("WITH_WA") != null && !warrantNerObj.optString("WITH_WA").equalsIgnoreCase("") && !warrantNerObj.optString("WITH_WA").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel.setWithWarrant(warrantNerObj.optString("WITH_WA"));
                            else
                                modifiedWarrantDetailsModel.setWithWarrant(warrantNerObj.optString("NA"));
                            if (warrantNerObj.optString("REMARKS") != null && !warrantNerObj.optString("REMARKS").equalsIgnoreCase("") && !warrantNerObj.optString("REMARKS").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel.setNerRemarks(warrantNerObj.optString("REMARKS"));
                            else
                                modifiedWarrantDetailsModel.setNerRemarks(warrantNerObj.optString("NA"));
                              /*  modifiedWarrantDetailsModel.setNerDate("18-JAN-12");
                                modifiedWarrantDetailsModel.setWithWarrant("YES");
                                modifiedWarrantDetailsModel.setNerRemarks("HELLO");*/
                            warrantExecDetailModel.setWarrantNerList(modifiedWarrantDetailsModel);

                        }
                    }
                    if (object.has("PROCESS_DETAILS")) {

                        JSONArray warrantProcessArray = object.optJSONArray("PROCESS_DETAILS");
                        for (int j = 0; j <warrantProcessArray.length(); j++) {
                            JSONObject warrantProcessObj = warrantProcessArray.optJSONObject(j);
                            ModifiedWarrantDetailsModel modifiedWarrantDetailsModel_process = new ModifiedWarrantDetailsModel();
                            if (warrantProcessObj.optString("PROCESS_NO") != null && !warrantProcessObj.optString("PROCESS_NO").equalsIgnoreCase("") && !warrantProcessObj.optString("PROCESS_NO").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessNo(warrantProcessObj.optString("PROCESS_NO"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessNo("NA");
                            if (warrantProcessObj.optString("PROCESS_TYPE") != null && !warrantProcessObj.optString("PROCESS_TYPE").equalsIgnoreCase("") && !warrantProcessObj.optString("PROCESS_TYPE").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessType(warrantProcessObj.optString("PROCESS_TYPE"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessType("NA");
                            if (warrantProcessObj.optString("REQUEST_DATE") != null && !warrantProcessObj.optString("REQUEST_DATE").equalsIgnoreCase("") && !warrantProcessObj.optString("REQUEST_DATE").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessRequestDate(warrantProcessObj.optString("REQUEST_DATE"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessRequestDate("NA");
                            if (warrantProcessObj.optString("COURT_APPVD") != null && !warrantProcessObj.optString("COURT_APPVD").equalsIgnoreCase("") && !warrantProcessObj.optString("COURT_APPVD").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessCourtAppr(warrantProcessObj.optString("COURT_APPVD"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessCourtAppr("NA");
                            if (warrantProcessObj.optString("MEMO_NO") != null && !warrantProcessObj.optString("MEMO_NO").equalsIgnoreCase("") && !warrantProcessObj.optString("MEMO_NO").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessMemoNo(warrantProcessObj.optString("MEMO_NO"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessMemoNo("NA");
                            if (warrantProcessObj.optString("MEMO_DT") != null && !warrantProcessObj.optString("MEMO_DT").equalsIgnoreCase("") && !warrantProcessObj.optString("MEMO_DT").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessMemodate(warrantProcessObj.optString("MEMO_DT"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessMemodate("NA");
                            if (warrantProcessObj.optString("EXECUTED") != null && !warrantProcessObj.optString("EXECUTED").equalsIgnoreCase("") && !warrantProcessObj.optString("EXECUTED").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessExecuted(warrantProcessObj.optString("EXECUTED"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessExecuted("NA");
                            if (warrantProcessObj.optString("EXECUTION_DT") != null && !warrantProcessObj.optString("EXECUTION_DT").equalsIgnoreCase("") && !warrantProcessObj.optString("EXECUTION_DT").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessExecutedDate(warrantProcessObj.optString("EXECUTION_DT"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessExecutedDate("NA");
                            if (warrantProcessObj.optString("REMARKS") != null && !warrantProcessObj.optString("REMARKS").equalsIgnoreCase("") && !warrantProcessObj.optString("REMARKS").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessRemarks(warrantProcessObj.optString("REMARKS"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessRemarks("NA");
                         /*   modifiedWarrantDetailsModel_process.setProcessNo("123");
                            modifiedWarrantDetailsModel_process.setProcessType("TEST");
                            modifiedWarrantDetailsModel_process.setProcessRemarks("testing with hardcore data");*/

                            warrantExecDetailModel.setWarrantProcessList(modifiedWarrantDetailsModel_process);

                        }
                    }
                    if (object.has("PROPERTY_DETAILS")) {

                        JSONArray warrantPropertiesArray = object.optJSONArray("PROPERTY_DETAILS");
                        for (int j = 0; j < warrantPropertiesArray.length(); j++) {
                           JSONObject warrantPropertiesObj = warrantPropertiesArray.optJSONObject(j);

                            ModifiedWarrantDetailsModel modifiedWarrantDetailsModel_properties = new ModifiedWarrantDetailsModel();
                           if (warrantPropertiesObj.optString("SLNO") != null && !warrantPropertiesObj.optString("SLNO").equalsIgnoreCase("") && !warrantPropertiesObj.optString("SLNO").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_properties.setPropertiesSlNo(warrantPropertiesObj.optString("SLNO"));
                            else
                                modifiedWarrantDetailsModel_properties.setPropertiesSlNo("NA");
                            if (warrantPropertiesObj.optString("ARTICLE") != null && !warrantPropertiesObj.optString("ARTICLE").equalsIgnoreCase("") && !warrantPropertiesObj.optString("ARTICLE").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_properties.setPropertiesArticle(warrantPropertiesObj.optString("ARTICLE"));
                            else
                                modifiedWarrantDetailsModel_properties.setPropertiesArticle("NA");
                            if (warrantPropertiesObj.optString("VALUATION") != null && !warrantPropertiesObj.optString("VALUATION").equalsIgnoreCase("") && !warrantPropertiesObj.optString("VALUATION").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_properties.setPropertiesValuation(warrantPropertiesObj.optString("VALUATION"));
                            else
                                modifiedWarrantDetailsModel_properties.setPropertiesValuation("NA");
                          /*  modifiedWarrantDetailsModel_properties.setPropertiesSlNo("18-JAN-12");
                            modifiedWarrantDetailsModel_properties.setPropertiesArticle("YES");
                            modifiedWarrantDetailsModel_properties.setPropertiesValuation("1234");*/
                                warrantExecDetailModel.setWarrantPropertiesList(modifiedWarrantDetailsModel_properties);

                        }
                    }
                }

                showAllData();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void showAllData() {
        try {

            String caseRef = "";


            // String retDate=spdf.format(warrantExecDetailModel.getReturnableDtaeToCourt());

            if (!warrantExecDetailModel.getWaNo().equalsIgnoreCase("null")) {
                String waNo = "";
                if (!warrantExecDetailModel.getWaCat().equalsIgnoreCase("null")) {
                    waNo = warrantExecDetailModel.getWaCat() + "-";
                }
                if (!warrantExecDetailModel.getWaNo().equalsIgnoreCase("null")) {
                    waNo = waNo + warrantExecDetailModel.getWaNo();
                }
                if (!warrantExecDetailModel.getActionDate().equalsIgnoreCase("null")) {
                    waNo = waNo + " issued on " + warrantExecDetailModel.getActionDate();
                }
                tv_waNoValue.setText(waNo);
//            tv_waNoValue.setText(warrantExecDetailModel.getWaNo());
            } else {
                tv_waNoValue.setText("NA");
            }

            if (!warrantExecDetailModel.getReturnableDtaeToCourt().equalsIgnoreCase("null")) {
                //checkOverdue(warrantExecDetailModel.getReturnableDtaeToCourt());
                DateUtils.overDueDay(this, warrantExecDetailModel.getReturnableDtaeToCourt(), tv_returnDateValue, warrantExecDetailModel.getColorStatus());


            } else {
                tv_returnDateValue.setText("NA");
            }

            if (!warrantExecDetailModel.getWaStatus().equalsIgnoreCase("null")) {
                // tv_statusValue.setText(warrantExecDetailModel.getWaStatus());
                if (!warrantExecDetailModel.getColorStatus().equalsIgnoreCase("null")) {
                    DateUtils.setStatusColor(this, tv_statusValue, warrantExecDetailModel.getColorStatus(), warrantExecDetailModel.getWaStatus());

                } else {
                    tv_statusValue.setText(warrantExecDetailModel.getWaStatus());
                }
            } else {
                tv_statusValue.setText("NA");
            }

            // In the warrant details - Case Ref., the value of CASEPS should be displayed, if case no is not null. 17-07-17
            if (warrantExecDetailModel.getWaCaseNo().equalsIgnoreCase("null")) {
                tv_caseRefValue.setVisibility(View.GONE);
                tv_caseRef.setVisibility(View.GONE);
            } else {
                if (!warrantExecDetailModel.getPsCode().equalsIgnoreCase("null")) {
                    caseRef = "Sec. " + warrantExecDetailModel.getPsCode();
                }
                if (!warrantExecDetailModel.getWaCaseNo().equalsIgnoreCase("null")) {
                    caseRef = caseRef + " C/No. " + warrantExecDetailModel.getWaCaseNo();
                }
                //  L.e("casedate"+warrantExecDetailModel_BW.getWaDate());
                if (!warrantExecDetailModel.getWaDate().equalsIgnoreCase("null") && !warrantExecDetailModel.getWaDate().equalsIgnoreCase("") /*&& !warrantExecDetailModel.getWaDate().equalsIgnoreCase(null)*/) {
                    caseRef = caseRef + " of " + warrantExecDetailModel.getWaDate();
                } else if (!warrantExecDetailModel.getFirYear().equalsIgnoreCase("null")) {
                    caseRef = caseRef + " of " + warrantExecDetailModel.getFirYear();
                }
                if (!warrantExecDetailModel.getUnderSec().equalsIgnoreCase("null")) {
                    caseRef = caseRef + "\nu/s " + warrantExecDetailModel.getUnderSec() + "IPC";
                }

//            COSSIPORE PS C/No.45 of 2017 u/s 120B/448/380/411 IPC.
                tv_caseRefValue.setText(caseRef);
            }
            tv_caseRefValue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!warrantExecDetailModel.getFirYear().equalsIgnoreCase("null")) {
                        callCaseFirDetails(warrantExecDetailModel.getPsCode(), warrantExecDetailModel.getWaCaseNo(), warrantExecDetailModel.getFirYear());
                    } else {
                        Utility.showAlertDialog(WarrentFallingDueDetailActivityNew.this, Constants.ERROR_TITLE, Constants.ERROR_MSG_NO_CASE_YEAR, false);
                    }
                }
            });

            if (!warrantExecDetailModel.getIssueCourt().equalsIgnoreCase("null")) {
                tv_issuedByVal.setText(warrantExecDetailModel.getIssueCourt());
            } else {
                tv_issuedByVal.setText("NA");
            }

            if (!warrantExecDetailModel.getProcessNo().equalsIgnoreCase("null")) {
                tv_processNoValue.setText(warrantExecDetailModel.getProcessNo());
            } else {
                tv_processNoValue.setText("NA");
            }

            if (!warrantExecDetailModel.getOfficerName().equalsIgnoreCase("null")) {
                tv_officerNameValue.setText(warrantExecDetailModel.getOfficerName());
            } else {
                tv_officerNameValue.setText("NA");
            }

            ll_otherDetails.removeAllViews();
            if (warrantExecDetailModel.getWarranties().size() > 0) {

                int length = warrantExecDetailModel.getWarranties().size();

                for (int i = 0; i < length; i++) {

                    Log.e("TAG", "TAG TAG " + i);
                    WarrantiesDetailOfDashboardWarrantModel model = warrantExecDetailModel.getWarranties().get(i);
                    TextView warrantiesNameAddressDetail = new TextView(WarrentFallingDueDetailActivityNew.this);
                    String detail = "";
                    if (!model.getWaName().equalsIgnoreCase("null"))
                        detail = model.getWaName().toString().trim();
                    if (!model.getWaFatherName().equalsIgnoreCase("null"))
                        detail = detail + " S/O Of " + model.getWaFatherName().toString().trim();
                    if (!model.getWaAddress().equalsIgnoreCase("null"))
                        detail = detail + ", " + model.getWaAddress().toString().trim();

                    warrantiesNameAddressDetail.setText(i + 1 + ". " + detail);
                    ll_otherDetails.addView(warrantiesNameAddressDetail);
                    warrantiesNameAddressDetail.setTextColor(ContextCompat.getColor(WarrentFallingDueDetailActivityNew.this, R.color.color_black));
                }
            } else {
                TextView noDetail = new TextView(WarrentFallingDueDetailActivityNew.this);
                noDetail.setText("No details for warrantee");
                noDetail.setTextColor(ContextCompat.getColor(WarrentFallingDueDetailActivityNew.this, R.color.color_black));
                ll_otherDetails.addView(noDetail);
                ll_otherDetails.setGravity(Gravity.CENTER);
            }
            ll_warrantNer.removeAllViews();
            if (warrantExecDetailModel.getWarrantNerList().size() > 0) {

                int length = warrantExecDetailModel.getWarrantNerList().size();
                for (int i = 0; i < length; i++) {
                    ModifiedWarrantDetailsModel modifiedWarrantDetailsModel = warrantExecDetailModel.getWarrantNerList().get(i);
                    LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View v = layoutInflater.inflate(R.layout.warrant_ner_item_layout, null);
                    LinearLayout ll = (LinearLayout) v.findViewById(R.id.warrant_ner_item);
                    ll.setId(i);
                    TextView nerDate = (TextView) v.findViewById(R.id.tv_ner_date);
                    TextView withWarrant = (TextView) v.findViewById(R.id.tv_with_warrant);
                    TextView remarks = (TextView) v.findViewById(R.id.tv_remarks);
                    if (modifiedWarrantDetailsModel.getNerDate() != null && !modifiedWarrantDetailsModel.getNerDate().equalsIgnoreCase("") && !modifiedWarrantDetailsModel.getNerDate().equalsIgnoreCase("null")) {
                        nerDate.setText(modifiedWarrantDetailsModel.getNerDate());
                    }
                    if (modifiedWarrantDetailsModel.getWithWarrant() != null && !modifiedWarrantDetailsModel.getWithWarrant().equalsIgnoreCase("") && !modifiedWarrantDetailsModel.getWithWarrant().equalsIgnoreCase("null")) {
                        if (modifiedWarrantDetailsModel.getWithWarrant().equalsIgnoreCase("Y")) {
                            withWarrant.setText("YES");
                        } else {
                            withWarrant.setText("NO");
                        }
                    }
                    if (modifiedWarrantDetailsModel.getNerRemarks() != null && !modifiedWarrantDetailsModel.getNerRemarks().equalsIgnoreCase("") && !modifiedWarrantDetailsModel.getNerRemarks().equalsIgnoreCase("null")) {
                        remarks.setText(modifiedWarrantDetailsModel.getNerRemarks());
                    }
                    ll_warrantNer.addView(v);


                }

            } else {
                LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = layoutInflater.inflate(R.layout.warrant_nbw_bw_norecord_layout, null);
                TextView tv = (TextView) v.findViewById(R.id.tv_warrant_nbw_bw_norecord);
                tv.setText("No  Non-Execution Reports found");
                ll_warrantNer.addView(v);

            }
            ll_warrantProcess.removeAllViews();
            if (warrantExecDetailModel.getWarrantProcessList().size() > 0) {

                int length = warrantExecDetailModel.getWarrantProcessList().size();
                for (int i = 0; i < length; i++) {
                    ModifiedWarrantDetailsModel modifiedWarrantDetailsModelProcess = warrantExecDetailModel.getWarrantProcessList().get(i);
                    LayoutInflater layoutInflaterprocess = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View v = layoutInflaterprocess.inflate(R.layout.warrant_process_item_layout, null);
                    LinearLayout ll = (LinearLayout) v.findViewById(R.id.ll_warrant_process_item);
                    ll.setId(i);
                    TextView tv_processNo = (TextView) v.findViewById(R.id.tv_processNo);
                    TextView tv_processType = (TextView) v.findViewById(R.id.tv_processType);
                    TextView processRequestDate = (TextView) v.findViewById(R.id.tv_requestDate);
                    TextView processCourtAppr = (TextView) v.findViewById(R.id.tv_courtApproved);
                    TextView processMemoNo = (TextView) v.findViewById(R.id.tv_memoNo);
                    TextView processMemodate = (TextView) v.findViewById(R.id.tv_memoDate);
                    TextView processExecuted = (TextView) v.findViewById(R.id.tv_Executed);
                    TextView processExecutedDate = (TextView) v.findViewById(R.id.tv_Executed_Date);
                    TextView processRemarks = (TextView) v.findViewById(R.id.tv_Remarks);
                    if (modifiedWarrantDetailsModelProcess.getProcessNo() != null && !modifiedWarrantDetailsModelProcess.getProcessNo().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessNo().equalsIgnoreCase("null")) {
                        tv_processNo.setText(modifiedWarrantDetailsModelProcess.getProcessNo());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessType() != null && !modifiedWarrantDetailsModelProcess.getProcessType().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessType().equalsIgnoreCase("null")) {
                        tv_processType.setText(modifiedWarrantDetailsModelProcess.getProcessType());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessRequestDate() != null && !modifiedWarrantDetailsModelProcess.getProcessRequestDate().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessRequestDate().equalsIgnoreCase("null")) {
                        processRequestDate.setText(modifiedWarrantDetailsModelProcess.getProcessRequestDate());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessCourtAppr() != null && !modifiedWarrantDetailsModelProcess.getProcessCourtAppr().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessCourtAppr().equalsIgnoreCase("null")) {
                        processCourtAppr.setText(modifiedWarrantDetailsModelProcess.getProcessCourtAppr());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessMemoNo() != null && !modifiedWarrantDetailsModelProcess.getProcessMemoNo().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessMemoNo().equalsIgnoreCase("null")) {
                        processMemoNo.setText(modifiedWarrantDetailsModelProcess.getProcessMemoNo());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessMemodate() != null && !modifiedWarrantDetailsModelProcess.getProcessMemodate().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessMemodate().equalsIgnoreCase("null")) {
                        processMemodate.setText(modifiedWarrantDetailsModelProcess.getProcessMemodate());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessExecuted() != null && !modifiedWarrantDetailsModelProcess.getProcessExecuted().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessExecuted().equalsIgnoreCase("null")) {
                        processExecuted.setText(modifiedWarrantDetailsModelProcess.getProcessExecuted());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessExecutedDate() != null && !modifiedWarrantDetailsModelProcess.getProcessExecutedDate().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessExecutedDate().equalsIgnoreCase("null")) {
                        processExecutedDate.setText(modifiedWarrantDetailsModelProcess.getProcessExecutedDate());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessRemarks() != null && !modifiedWarrantDetailsModelProcess.getProcessRemarks().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessRemarks().equalsIgnoreCase("null")) {
                        processRemarks.setText(modifiedWarrantDetailsModelProcess.getProcessRemarks());
                    }

                    ll_warrantProcess.addView(v);


                }

            } else {
                LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = layoutInflater.inflate(R.layout.warrant_nbw_bw_norecord_layout, null);
                TextView tv = (TextView) v.findViewById(R.id.tv_warrant_nbw_bw_norecord);
                tv.setText("No Process Details found");
                ll_warrantProcess.addView(v);

            }
            ll_warrantProperties.removeAllViews();
            if (warrantExecDetailModel.getWarrantPropertiesList().size() > 0) {

                int length = warrantExecDetailModel.getWarrantPropertiesList().size();
                for (int i = 0; i < length; i++) {
                    ModifiedWarrantDetailsModel modifiedWarrantDetailsModel = warrantExecDetailModel.getWarrantPropertiesList().get(i);
                    LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View v = layoutInflater.inflate(R.layout.warrant_properties_item_layout, null);
                    LinearLayout ll = (LinearLayout) v.findViewById(R.id.ll_warrant_properties_item);
                    ll.setId(i);
                    TextView slNo = (TextView) v.findViewById(R.id.tv_properties_sl_no);
                    TextView article = (TextView) v.findViewById(R.id.tv_properties_article);
                    TextView valuation = (TextView) v.findViewById(R.id.tv_properties_valuation);
                    if (modifiedWarrantDetailsModel.getPropertiesSlNo() != null && !modifiedWarrantDetailsModel.getPropertiesSlNo().equalsIgnoreCase("") && !modifiedWarrantDetailsModel.getPropertiesSlNo().equalsIgnoreCase("null")) {
                        slNo.setText(modifiedWarrantDetailsModel.getPropertiesSlNo());
                    }
                    if (modifiedWarrantDetailsModel.getPropertiesArticle() != null && !modifiedWarrantDetailsModel.getPropertiesArticle().equalsIgnoreCase("") && !modifiedWarrantDetailsModel.getPropertiesArticle().equalsIgnoreCase("null")) {
                        article.setText(modifiedWarrantDetailsModel.getPropertiesArticle());
                    }
                    if (modifiedWarrantDetailsModel.getPropertiesValuation() != null && !modifiedWarrantDetailsModel.getPropertiesValuation().equalsIgnoreCase("") && !modifiedWarrantDetailsModel.getPropertiesValuation().equalsIgnoreCase("null")) {
                        valuation.setText(modifiedWarrantDetailsModel.getPropertiesValuation());
                    }

                    ll_warrantProperties.addView(v);

                }

            } else {
                LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = layoutInflater.inflate(R.layout.warrant_nbw_bw_norecord_layout, null);
                TextView tv = (TextView) v.findViewById(R.id.tv_warrant_nbw_bw_norecord);
                tv.setText("No Article Details found");
                ll_warrantProperties.addView(v);

            }
        } catch (Exception e) {

        }


    }

    private void checkOverdue(String returnableDate) {
        SimpleDateFormat spdf = new SimpleDateFormat("dd-MM-yyyy");
        String currDate = spdf.format(new Date());
        String retDate = spdf.format(returnableDate);
        Toast.makeText(this, "Current Date" + currDate + " " + "Returnable Date" + retDate, Toast.LENGTH_SHORT).show();
        L.e("Current Date" + currDate + " " + "Returnable Date" + retDate);

    }


    private void showDataNBW() {
        ll_NBW.setVisibility(View.VISIBLE);
        ll_dataShow.setVisibility(View.VISIBLE);
        ll_noDataShow.setVisibility(View.GONE);
        ll_BW.setVisibility(View.GONE);
    }

    private void showDataBW() {

        ll_BW.setVisibility(View.VISIBLE);
        ll_dataShow_BW.setVisibility(View.VISIBLE);
        ll_noDataShow_BW.setVisibility(View.GONE);
        ll_NBW.setVisibility(View.GONE);
    }


    private void showNoDataFoundNBW() {

        ll_BW.setVisibility(View.GONE);
        ll_NBW.setVisibility(View.VISIBLE);
        ll_dataShow.setVisibility(View.GONE);
        ll_noDataShow.setVisibility(View.VISIBLE);

    }

    private void showNoDataFoundBW() {

        ll_NBW.setVisibility(View.GONE);
        ll_BW.setVisibility(View.VISIBLE);
        ll_dataShow_BW.setVisibility(View.GONE);
        ll_noDataShow_BW.setVisibility(View.VISIBLE);
    }

    private void parentHideWithNodataNBW() {

        ll_NBW.setVisibility(View.GONE);
        ll_dataShow.setVisibility(View.GONE);
        ll_noDataShow.setVisibility(View.VISIBLE);
    }


    private void parentHideWithNodataBW() {

        ll_BW.setVisibility(View.GONE);
        ll_dataShow_BW.setVisibility(View.GONE);
        ll_noDataShow_BW.setVisibility(View.VISIBLE);

    }

    private void parentHideWithAlldataSetNBW() {
        ll_NBW.setVisibility(View.GONE);
        ll_dataShow.setVisibility(View.VISIBLE);
        ll_noDataShow.setVisibility(View.GONE);
    }

    private void parentHideWithAlldataSetBW() {
        ll_BW.setVisibility(View.GONE);
        ll_dataShow_BW.setVisibility(View.VISIBLE);
        ll_noDataShow_BW.setVisibility(View.GONE);
    }


    private void noDataTextSet_NBW() {

        ll_noDataShow.removeAllViews();
        TextView tv_NoData = new TextView(WarrentFallingDueDetailActivityNew.this);
        tv_NoData.setText("No details available for NBW");
        tv_NoData.setTextColor(ContextCompat.getColor(WarrentFallingDueDetailActivityNew.this, R.color.color_black));
        tv_NoData.setTextColor(ContextCompat.getColor(WarrentFallingDueDetailActivityNew.this, R.color.color_black));
        tv_NoData.setTextSize(20);
        ll_noDataShow.addView(tv_NoData);
        ll_noDataShow.setGravity(Gravity.CENTER);
    }

    private void noDataTextSet_BW() {

        ll_noDataShow_BW.removeAllViews();
        TextView tv_NoData = new TextView(WarrentFallingDueDetailActivityNew.this);
        tv_NoData.setText("No details available for BW");
        tv_NoData.setTextColor(ContextCompat.getColor(WarrentFallingDueDetailActivityNew.this, R.color.color_black));
        tv_NoData.setTextColor(ContextCompat.getColor(WarrentFallingDueDetailActivityNew.this, R.color.color_black));
        tv_NoData.setTextSize(20);
        ll_noDataShow_BW.addView(tv_NoData);
        ll_noDataShow_BW.setGravity(Gravity.CENTER);
    }

    private void warrantBWAPICall(int pageNo) {

        TaskManager taskManager = new TaskManager(this);
        if (warrentType.equalsIgnoreCase(Constants.WARRENT_TYPE_FAIL)) {
            taskManager.setMethod(Constants.METHOD_ALL_WA_FALL_DETAILS);
        } else if (warrentType.equalsIgnoreCase(Constants.WARRENT_TYPE_DUE)) {
            taskManager.setMethod(Constants.METHOD_ALL_WA_DUE_DETAILS);

        }
        taskManager.setWaFallDueDetailSearchForBW(true);
        keys = new String[]{"ps", "currDate", "watype", "pageno", "div"};
        values = new String[]{psCode.trim(), selectedDate.trim(), "BW", "" + pageNo, selected_div.trim()};
        taskManager.doStartTask(keys, values, true);
    }


    public void parseWarrantRecvDetailsResultForBW(String response) {
        //System.out.println("parseWarrantRecvDetailsResultForBW " + response);

        try {
            JSONObject jObj = new JSONObject(response);

            if (jObj.optString("status").equalsIgnoreCase("1")) {

                // select BW tab i.e. no data in NBW
                if (!isDataInNbw && isNBWSelected) {
                    isBWSelected = true;
                    isNBWSelected = false;

                    ll_NBW.setVisibility(View.GONE);
                    ll_BW.setVisibility(View.VISIBLE);

                    tv_bw.setEnabled(false);
                    tv_nbw.setEnabled(true);

                    bwIndicator.setBackgroundColor(getResources().getColor(R.color.color_light_blue));
                    nbwIndicator.setBackgroundColor(getResources().getColor(R.color.color_indicator));

                    buttonVisibilityForBw();
                }

                if (isBWSelected) {
                    showDataBW();
                } else {
                    parentHideWithAlldataSetBW();
                }


                pageNoBW = Integer.parseInt(jObj.optString("pageno"));
                totalBWCount = Integer.parseInt(jObj.optString("total"));

                JSONArray resultArray = jObj.optJSONArray("result");
                parseAllWarrantExecDataForBW(resultArray);

                if (isBWSelected) {
                    buttonVisibilityForBw();
                } else {

                }

            } else {

                noDataTextSet_BW();

                if (isBWSelected) {

                    isDataInNbw = false;
                    showNoDataFoundBW();
                } else {
                    parentHideWithNodataBW();
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseAllWarrantExecDataForBW(JSONArray resultArray) {
        if (resultArray != null && !resultArray.equals("")) {

            warrantDetailsList_BW.clear();
            warrantiesList_BW.clear();

            warrantExecDetailModel_BW = new WarrantExecDetailModel();
            try {

                for (int i = 0; i < resultArray.length(); i++) {

                    JSONObject object = resultArray.optJSONObject(i);

                    warrantExecDetailModel_BW.setWaNo(object.optString("WANO"));
                    warrantExecDetailModel_BW.setReturnableDtaeToCourt(object.optString("RETURNABLE_DATE_TO_COURT"));
                    warrantExecDetailModel_BW.setWaStatus(object.optString("WA_STATUS"));
                    warrantExecDetailModel_BW.setPsCode(object.optString("PS"));
                    warrantExecDetailModel_BW.setWaCaseNo(object.optString("CASENO"));
                    warrantExecDetailModel_BW.setWaYear(object.optString("WA_YEAR"));
                    warrantExecDetailModel_BW.setFirYear(object.optString("FIR_YR"));
                    warrantExecDetailModel_BW.setUnderSec(object.optString("UNDER_SECTIONS"));
                    warrantExecDetailModel_BW.setIssueCourt(object.optString("ISSUE_COURT"));
                    warrantExecDetailModel_BW.setProcessNo(object.optString("PROCESS_NO"));
                    warrantExecDetailModel_BW.setOfficerName(object.optString("IONAME"));
                    warrantExecDetailModel_BW.setWaCat(object.optString("WACAT"));
                    warrantExecDetailModel_BW.setActionDate(object.optString("ACTION_DATE"));
                    warrantExecDetailModel_BW.setColorStatus(object.optString("fail_normal"));
                    warrantExecDetailModel_BW.setWaDate(object.optString("casedate"));

                    JSONArray warrantiesArray = object.optJSONArray("warrenties");

                    for (int j = 0; j < warrantiesArray.length(); j++) {

                        JSONObject warrantiesObj = warrantiesArray.optJSONObject(j);

                        WarrantiesDetailOfDashboardWarrantModel warrantiesModel = new WarrantiesDetailOfDashboardWarrantModel();
                        warrantiesModel.setWaName(warrantiesObj.optString("NAME"));
                        warrantiesModel.setWaAddress(warrantiesObj.optString("ADDRESS"));
                        warrantiesModel.setWaFatherName(warrantiesObj.optString("FATHER_NAME"));
                        warrantExecDetailModel_BW.setWarranties(warrantiesModel);
                    }
                    if (object.has("NON_EXEC_REPORT")) {

                        JSONArray warrantNerArray = object.optJSONArray("NON_EXEC_REPORT");
                        for (int j = 0; j < warrantNerArray.length(); j++) {
                            JSONObject warrantNerObj = warrantNerArray.optJSONObject(j);
                            ModifiedWarrantDetailsModel modifiedWarrantDetailsModel = new ModifiedWarrantDetailsModel();
                            if (warrantNerObj.optString("NERDATE") != null && !warrantNerObj.optString("NERDATE").equalsIgnoreCase("") && !warrantNerObj.optString("NERDATE").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel.setNerDate(warrantNerObj.optString("NERDATE"));
                            else
                                modifiedWarrantDetailsModel.setNerDate(warrantNerObj.optString("NA"));
                            if (warrantNerObj.optString("WITH_WA") != null && !warrantNerObj.optString("WITH_WA").equalsIgnoreCase("") && !warrantNerObj.optString("WITH_WA").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel.setWithWarrant(warrantNerObj.optString("WITH_WA"));
                            else
                                modifiedWarrantDetailsModel.setWithWarrant(warrantNerObj.optString("NA"));
                            if (warrantNerObj.optString("REMARKS") != null && !warrantNerObj.optString("REMARKS").equalsIgnoreCase("") && !warrantNerObj.optString("REMARKS").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel.setNerRemarks(warrantNerObj.optString("REMARKS"));
                            else
                                modifiedWarrantDetailsModel.setNerRemarks(warrantNerObj.optString("NA"));

                 /*       modifiedWarrantDetailsModel.setNerDate("18-JAN-12");
                        modifiedWarrantDetailsModel.setWithWarrant("YES");
                        modifiedWarrantDetailsModel.setNerRemarks("HELLO");*/
                            warrantExecDetailModel_BW.setWarrantNerList(modifiedWarrantDetailsModel);

                        }
                    }

                    if (object.has("PROCESS_DETAILS")) {

                        JSONArray warrantProcessArray = object.optJSONArray("PROCESS_DETAILS");
                        for (int j = 0; j < warrantProcessArray.length(); j++) {
                            JSONObject warrantProcessObj = warrantProcessArray.optJSONObject(j);
                            ModifiedWarrantDetailsModel modifiedWarrantDetailsModel_process = new ModifiedWarrantDetailsModel();
                            if (warrantProcessObj.optString("PROCESS_NO") != null && !warrantProcessObj.optString("PROCESS_NO").equalsIgnoreCase("") && !warrantProcessObj.optString("PROCESS_NO").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessNo(warrantProcessObj.optString("PROCESS_NO"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessNo("NA");
                            if (warrantProcessObj.optString("PROCESS_TYPE") != null && !warrantProcessObj.optString("PROCESS_TYPE").equalsIgnoreCase("") && !warrantProcessObj.optString("PROCESS_TYPE").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessType(warrantProcessObj.optString("PROCESS_TYPE"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessType("NA");
                            if (warrantProcessObj.optString("REQUEST_DATE") != null && !warrantProcessObj.optString("REQUEST_DATE").equalsIgnoreCase("") && !warrantProcessObj.optString("REQUEST_DATE").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessRequestDate(warrantProcessObj.optString("REQUEST_DATE"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessRequestDate("NA");
                            if (warrantProcessObj.optString("COURT_APPVD") != null && !warrantProcessObj.optString("COURT_APPVD").equalsIgnoreCase("") && !warrantProcessObj.optString("COURT_APPVD").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessCourtAppr(warrantProcessObj.optString("COURT_APPVD"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessCourtAppr("NA");
                            if (warrantProcessObj.optString("MEMO_NO") != null && !warrantProcessObj.optString("MEMO_NO").equalsIgnoreCase("") && !warrantProcessObj.optString("MEMO_NO").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessMemoNo(warrantProcessObj.optString("MEMO_NO"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessMemoNo("NA");
                            if (warrantProcessObj.optString("MEMO_DT") != null && !warrantProcessObj.optString("MEMO_DT").equalsIgnoreCase("") && !warrantProcessObj.optString("MEMO_DT").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessMemodate(warrantProcessObj.optString("MEMO_DT"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessMemodate("NA");
                            if (warrantProcessObj.optString("EXECUTED") != null && !warrantProcessObj.optString("EXECUTED").equalsIgnoreCase("") && !warrantProcessObj.optString("EXECUTED").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessExecuted(warrantProcessObj.optString("EXECUTED"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessExecuted("NA");
                            if (warrantProcessObj.optString("EXECUTION_DT") != null && !warrantProcessObj.optString("EXECUTION_DT").equalsIgnoreCase("") && !warrantProcessObj.optString("EXECUTION_DT").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessExecutedDate(warrantProcessObj.optString("EXECUTION_DT"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessExecutedDate("NA");
                            if (warrantProcessObj.optString("REMARKS") != null && !warrantProcessObj.optString("REMARKS").equalsIgnoreCase("") && !warrantProcessObj.optString("REMARKS").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_process.setProcessRemarks(warrantProcessObj.optString("REMARKS"));
                            else
                                modifiedWarrantDetailsModel_process.setProcessRemarks("NA");
                            warrantExecDetailModel_BW.setWarrantProcessList(modifiedWarrantDetailsModel_process);

                        }
                    }
                    if (object.has("PROPERTY_DETAILS")) {

                        JSONArray warrantPropertiesArray = object.optJSONArray("PROPERTY_DETAILS");
                        for (int j = 0; j < warrantPropertiesArray.length(); j++) {
                            JSONObject warrantPropertiesObj = warrantPropertiesArray.optJSONObject(j);
                            ModifiedWarrantDetailsModel modifiedWarrantDetailsModel_properties = new ModifiedWarrantDetailsModel();
                            if (warrantPropertiesObj.optString("SLNO") != null && !warrantPropertiesObj.optString("SLNO").equalsIgnoreCase("") && !warrantPropertiesObj.optString("SLNO").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_properties.setPropertiesSlNo(warrantPropertiesObj.optString("SLNO"));
                            else
                                modifiedWarrantDetailsModel_properties.setPropertiesSlNo("NA");
                            if (warrantPropertiesObj.optString("ARTICLE") != null && !warrantPropertiesObj.optString("ARTICLE").equalsIgnoreCase("") && !warrantPropertiesObj.optString("ARTICLE").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_properties.setPropertiesArticle(warrantPropertiesObj.optString("ARTICLE"));
                            else
                                modifiedWarrantDetailsModel_properties.setPropertiesArticle("NA");
                            if (warrantPropertiesObj.optString("VALUATION") != null && !warrantPropertiesObj.optString("VALUATION").equalsIgnoreCase("") && !warrantPropertiesObj.optString("VALUATION").equalsIgnoreCase("null"))
                                modifiedWarrantDetailsModel_properties.setPropertiesValuation(warrantPropertiesObj.optString("VALUATION"));
                            else
                                modifiedWarrantDetailsModel_properties.setPropertiesValuation("NA");
                           /* modifiedWarrantDetailsModel_properties.setPropertiesSlNo(warrantPropertiesObj.optString("SLNO"));
                            modifiedWarrantDetailsModel_properties.setPropertiesArticle(warrantPropertiesObj.optString("ARTICLE"));
                            modifiedWarrantDetailsModel_properties.setPropertiesValuation(warrantPropertiesObj.optString("VALUATION"));*/

                            warrantExecDetailModel_BW.setWarrantPropertiesList(modifiedWarrantDetailsModel_properties);

                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            showAllData_BW();
        }
    }


    private void showAllData_BW() {
        try {


            String caseRef = "";

            if (!warrantExecDetailModel_BW.getWaNo().equalsIgnoreCase("null")) {
                tv_waNoValue_BW.setText(warrantExecDetailModel_BW.getWaNo());
            } else {
                tv_waNoValue_BW.setText("NA");
            }

            if (!warrantExecDetailModel_BW.getReturnableDtaeToCourt().equalsIgnoreCase("null")) {
                // tv_returnDateValue_BW.setText(warrantExecDetailModel_BW.getReturnableDtaeToCourt());
                DateUtils.overDueDay(this, warrantExecDetailModel_BW.getReturnableDtaeToCourt(), tv_returnDateValue_BW, warrantExecDetailModel_BW.getColorStatus());

               /* if (day > 0) {
                    if (day == 1) {
                        tv_returnDateValue_BW.setText(warrantExecDetailModel_BW.getReturnableDtaeToCourt()+"( Overdue by " + String.valueOf(day) + "day )");
                    } else {
                        tv_returnDateValue_BW.setText(warrantExecDetailModel_BW.getReturnableDtaeToCourt()+"( Overdue by " + String.valueOf(day) + "days )");
                    }
                } else {
                    tv_returnDateValue_BW.setText(warrantExecDetailModel_BW.getReturnableDtaeToCourt());
                }*/


            } else {
                tv_returnDateValue_BW.setText("NA");
            }
            if (!warrantExecDetailModel_BW.getWaStatus().equalsIgnoreCase("null")) {
                //tv_statusValue_BW.setText(warrantExecDetailModel_BW.getWaStatus());
                if (!warrantExecDetailModel_BW.getColorStatus().equalsIgnoreCase("null")) {
                    DateUtils.setStatusColor(this, tv_statusValue_BW, warrantExecDetailModel_BW.getColorStatus(), warrantExecDetailModel_BW.getWaStatus());

                } else {
                    tv_statusValue_BW.setText(warrantExecDetailModel_BW.getWaStatus());
                }
            } else {
                tv_statusValue_BW.setText("NA");
            }

            // In the warrant details - Case Ref., the value of CASEPS should be displayed, if case no is not null. 17-07-17
            if (warrantExecDetailModel_BW.getWaCaseNo().equalsIgnoreCase("null")) {
                tv_caseRefValue_BW.setVisibility(View.GONE);
                tv_caseRef_BW.setVisibility(View.GONE);
            } else {
                if (!warrantExecDetailModel_BW.getPsCode().equalsIgnoreCase("null")) {
                    caseRef = "Sec. " + warrantExecDetailModel_BW.getPsCode();
                }
                if (!warrantExecDetailModel_BW.getWaCaseNo().equalsIgnoreCase("null")) {
                    caseRef = caseRef + " C/No. " + warrantExecDetailModel_BW.getWaCaseNo();
                }
                if (!warrantExecDetailModel_BW.getWaDate().equalsIgnoreCase("null") && !warrantExecDetailModel_BW.getWaDate().equalsIgnoreCase("") /*&& !warrantExecDetailModel.getWaDate().equalsIgnoreCase(null)*/) {
                    caseRef = caseRef + " of " + warrantExecDetailModel_BW.getWaDate();
                } else if (!warrantExecDetailModel_BW.getFirYear().equalsIgnoreCase("null")) {
                    caseRef = caseRef + " of " + warrantExecDetailModel_BW.getFirYear();
                }
                if (!warrantExecDetailModel_BW.getUnderSec().equalsIgnoreCase("null")) {
                    caseRef = caseRef + "\nu/s " + warrantExecDetailModel_BW.getUnderSec();
                }
                tv_caseRefValue_BW.setText(caseRef);
            }
            tv_caseRefValue_BW.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callCaseFirDetails(warrantExecDetailModel_BW.getPsCode(), warrantExecDetailModel_BW.getWaCaseNo(), warrantExecDetailModel_BW.getFirYear());
                }
            });
            if (!warrantExecDetailModel_BW.getIssueCourt().equalsIgnoreCase("null")) {
                tv_issuedByVal_BW.setText(warrantExecDetailModel_BW.getIssueCourt());
            } else {
                tv_issuedByVal_BW.setText("NA");
            }

            if (!warrantExecDetailModel_BW.getProcessNo().equalsIgnoreCase("null")) {
                tv_processNoValue_BW.setText(warrantExecDetailModel_BW.getProcessNo());
            } else {
                tv_processNoValue_BW.setText("NA");
            }

            if (!warrantExecDetailModel_BW.getOfficerName().equalsIgnoreCase("null")) {
                tv_officerNameValue_BW.setText(warrantExecDetailModel_BW.getOfficerName());
            } else {
                tv_officerNameValue_BW.setText("NA");
            }

            ll_otherDetails_BW.removeAllViews();
            if (warrantExecDetailModel_BW.getWarranties().size() > 0) {

                int length = warrantExecDetailModel_BW.getWarranties().size();

                for (int i = 0; i < length; i++) {
                    Log.e("TAG", "TAG TAG " + i);
                    WarrantiesDetailOfDashboardWarrantModel model = warrantExecDetailModel_BW.getWarranties().get(i);
                    TextView warrantiesNameAddressDetail = new TextView(WarrentFallingDueDetailActivityNew.this);

                    String detail = "";
                    if (!model.getWaName().equalsIgnoreCase("null"))
                        detail = model.getWaName().toString().trim();
                    if (!model.getWaFatherName().equalsIgnoreCase("null"))
                        detail = detail + " S/O Of " + model.getWaFatherName().toString().trim();
                    if (!model.getWaAddress().equalsIgnoreCase("null"))
                        detail = detail + ", " + model.getWaAddress().toString().trim();

                    warrantiesNameAddressDetail.setText(i + 1 + ". " + detail + "\n");
                    warrantiesNameAddressDetail.setTextColor(ContextCompat.getColor(WarrentFallingDueDetailActivityNew.this, R.color.color_black));
                    ll_otherDetails_BW.addView(warrantiesNameAddressDetail);
                }
            } else {
                TextView noDetail = new TextView(WarrentFallingDueDetailActivityNew.this);
                noDetail.setText("No details for warrantee");
                noDetail.setTextColor(ContextCompat.getColor(WarrentFallingDueDetailActivityNew.this, R.color.color_black));
                ll_otherDetails_BW.addView(noDetail);
                ll_otherDetails_BW.setGravity(Gravity.CENTER);
            }
            ll_warrantNer_BW.removeAllViews();

            if (warrantExecDetailModel_BW.getWarrantNerList().size() > 0) {

                int length = warrantExecDetailModel_BW.getWarrantNerList().size();
                for (int i = 0; i < length; i++) {
                    ModifiedWarrantDetailsModel modifiedWarrantDetailsModel = warrantExecDetailModel_BW.getWarrantNerList().get(i);
                    LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View v = layoutInflater.inflate(R.layout.warrant_ner_item_layout, null);
                    LinearLayout ll = (LinearLayout) v.findViewById(R.id.warrant_ner_item);
                    ll.setId(i);
                    TextView nerDate = (TextView) v.findViewById(R.id.tv_ner_date);
                    TextView withWarrant = (TextView) v.findViewById(R.id.tv_with_warrant);
                    TextView remarks = (TextView) v.findViewById(R.id.tv_remarks);
                    if (modifiedWarrantDetailsModel.getNerDate() != null && !modifiedWarrantDetailsModel.getNerDate().equalsIgnoreCase("") && !modifiedWarrantDetailsModel.getNerDate().equalsIgnoreCase("null")) {
                        nerDate.setText(modifiedWarrantDetailsModel.getNerDate());
                    }
                    if (modifiedWarrantDetailsModel.getWithWarrant() != null && !modifiedWarrantDetailsModel.getWithWarrant().equalsIgnoreCase("") && !modifiedWarrantDetailsModel.getWithWarrant().equalsIgnoreCase("null")) {
                        if (modifiedWarrantDetailsModel.getWithWarrant().equalsIgnoreCase("Y")) {
                            withWarrant.setText("YES");
                        } else {
                            withWarrant.setText("NO");
                        }
                    }
                    if (modifiedWarrantDetailsModel.getNerRemarks() != null && !modifiedWarrantDetailsModel.getNerRemarks().equalsIgnoreCase("") && !modifiedWarrantDetailsModel.getNerRemarks().equalsIgnoreCase("null")) {
                        remarks.setText(modifiedWarrantDetailsModel.getNerRemarks());
                    }
                    ll_warrantNer_BW.addView(v);


                }

            } else {
                LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = layoutInflater.inflate(R.layout.warrant_nbw_bw_norecord_layout, null);
                TextView tv = (TextView) v.findViewById(R.id.tv_warrant_nbw_bw_norecord);
                tv.setText("No Non-Execution Reports found");
                ll_warrantNer_BW.addView(v);

            }

            ll_warrantProcess_BW.removeAllViews();
            if (warrantExecDetailModel_BW.getWarrantProcessList().size() > 0) {

                int length = warrantExecDetailModel_BW.getWarrantProcessList().size();
                for (int i = 0; i < length; i++) {
                    ModifiedWarrantDetailsModel modifiedWarrantDetailsModelProcess = warrantExecDetailModel_BW.getWarrantProcessList().get(i);
                    LayoutInflater layoutInflaterprocess = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View v = layoutInflaterprocess.inflate(R.layout.warrant_process_item_layout, null);
                    LinearLayout ll = (LinearLayout) v.findViewById(R.id.ll_warrant_process_item);
                    ll.setId(i);
                    TextView tv_processNo = (TextView) v.findViewById(R.id.tv_processNo);
                    TextView tv_processType = (TextView) v.findViewById(R.id.tv_processType);
                    TextView processRequestDate = (TextView) v.findViewById(R.id.tv_requestDate);
                    TextView processCourtAppr = (TextView) v.findViewById(R.id.tv_courtApproved);
                    TextView processMemoNo = (TextView) v.findViewById(R.id.tv_memoNo);
                    TextView processMemodate = (TextView) v.findViewById(R.id.tv_memoDate);
                    TextView processExecuted = (TextView) v.findViewById(R.id.tv_Executed);
                    TextView processExecutedDate = (TextView) v.findViewById(R.id.tv_Executed_Date);
                    TextView processRemarks = (TextView) v.findViewById(R.id.tv_Remarks);
                    if (modifiedWarrantDetailsModelProcess.getProcessNo() != null && !modifiedWarrantDetailsModelProcess.getProcessNo().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessNo().equalsIgnoreCase("null")) {
                        tv_processNo.setText(modifiedWarrantDetailsModelProcess.getProcessNo());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessType() != null && !modifiedWarrantDetailsModelProcess.getProcessType().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessType().equalsIgnoreCase("null")) {
                        tv_processType.setText(modifiedWarrantDetailsModelProcess.getProcessType());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessRequestDate() != null && !modifiedWarrantDetailsModelProcess.getProcessRequestDate().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessRequestDate().equalsIgnoreCase("null")) {
                        processRequestDate.setText(modifiedWarrantDetailsModelProcess.getProcessRequestDate());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessCourtAppr() != null && !modifiedWarrantDetailsModelProcess.getProcessCourtAppr().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessCourtAppr().equalsIgnoreCase("null")) {
                        processCourtAppr.setText(modifiedWarrantDetailsModelProcess.getProcessCourtAppr());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessMemoNo() != null && !modifiedWarrantDetailsModelProcess.getProcessMemoNo().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessMemoNo().equalsIgnoreCase("null")) {
                        processMemoNo.setText(modifiedWarrantDetailsModelProcess.getProcessMemoNo());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessMemodate() != null && !modifiedWarrantDetailsModelProcess.getProcessMemodate().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessMemodate().equalsIgnoreCase("null")) {
                        processMemodate.setText(modifiedWarrantDetailsModelProcess.getProcessMemodate());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessExecuted() != null && !modifiedWarrantDetailsModelProcess.getProcessExecuted().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessExecuted().equalsIgnoreCase("null")) {
                        processExecuted.setText(modifiedWarrantDetailsModelProcess.getProcessExecuted());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessExecutedDate() != null && !modifiedWarrantDetailsModelProcess.getProcessExecutedDate().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessExecutedDate().equalsIgnoreCase("null")) {
                        processExecutedDate.setText(modifiedWarrantDetailsModelProcess.getProcessExecutedDate());
                    }
                    if (modifiedWarrantDetailsModelProcess.getProcessRemarks() != null && !modifiedWarrantDetailsModelProcess.getProcessRemarks().equalsIgnoreCase("") && !modifiedWarrantDetailsModelProcess.getProcessRemarks().equalsIgnoreCase("null")) {
                        processRemarks.setText(modifiedWarrantDetailsModelProcess.getProcessRemarks());
                    }

                    ll_warrantProcess_BW.addView(v);


                }

            } else {
                LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = layoutInflater.inflate(R.layout.warrant_nbw_bw_norecord_layout, null);
                TextView tv = (TextView) v.findViewById(R.id.tv_warrant_nbw_bw_norecord);
                tv.setText("No Process Details found");
                ll_warrantProcess_BW.addView(v);

            }
            ll_warrantProperties_BW.removeAllViews();
            if (warrantExecDetailModel_BW.getWarrantPropertiesList().size() > 0) {

                int length = warrantExecDetailModel_BW.getWarrantPropertiesList().size();
                for (int i = 0; i < length; i++) {
                    ModifiedWarrantDetailsModel modifiedWarrantDetailsModel = warrantExecDetailModel_BW.getWarrantPropertiesList().get(i);
                    LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View v = layoutInflater.inflate(R.layout.warrant_properties_item_layout, null);
                    LinearLayout ll = (LinearLayout) v.findViewById(R.id.ll_warrant_properties_item);
                    ll.setId(i);
                    TextView slNo = (TextView) v.findViewById(R.id.tv_properties_sl_no);
                    TextView article = (TextView) v.findViewById(R.id.tv_properties_article);
                    TextView valuation = (TextView) v.findViewById(R.id.tv_properties_valuation);
                    if (modifiedWarrantDetailsModel.getPropertiesSlNo() != null && !modifiedWarrantDetailsModel.getPropertiesSlNo().equalsIgnoreCase("") && !modifiedWarrantDetailsModel.getPropertiesSlNo().equalsIgnoreCase("null")) {
                        slNo.setText(modifiedWarrantDetailsModel.getPropertiesSlNo());
                    }
                    if (modifiedWarrantDetailsModel.getPropertiesArticle() != null && !modifiedWarrantDetailsModel.getPropertiesArticle().equalsIgnoreCase("") && !modifiedWarrantDetailsModel.getPropertiesArticle().equalsIgnoreCase("null")) {
                        article.setText(modifiedWarrantDetailsModel.getPropertiesArticle());
                    }
                    if (modifiedWarrantDetailsModel.getPropertiesValuation() != null && !modifiedWarrantDetailsModel.getPropertiesValuation().equalsIgnoreCase("") && !modifiedWarrantDetailsModel.getPropertiesValuation().equalsIgnoreCase("null")) {
                        valuation.setText(modifiedWarrantDetailsModel.getPropertiesValuation());
                    }

                    ll_warrantProperties_BW.addView(v);

                }

            } else {
                LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = layoutInflater.inflate(R.layout.warrant_nbw_bw_norecord_layout, null);
                TextView tv = (TextView) v.findViewById(R.id.tv_warrant_nbw_bw_norecord);
                tv.setText("No Article Details found");
                ll_warrantProperties_BW.addView(v);

            }
        } catch (Exception e) {

        }


    }

    public void callCaseFirDetails(String psCode, String caseNo, String caseYr) {
        Intent it = new Intent(this, CaseSearchFIRDetailsActivity.class);
        it.putExtra("FROM_PAGE", "WarrentActivityNew");
        if (isNBWSelected) {
            it.putExtra("ITEM_NAME", tv_caseRefValue.getText().toString().trim());
            it.putExtra("HEADER_VALUE", tv_caseRefValue.getText().toString().trim());
        } else if (isBWSelected) {
            it.putExtra("ITEM_NAME", tv_caseRefValue_BW.getText().toString().trim());
            it.putExtra("HEADER_VALUE", tv_caseRefValue_BW.getText().toString().trim());
        }
        it.putExtra("PS_CODE", psCode);
        it.putExtra("CASE_NO", caseNo);
        it.putExtra("CASE_YR", caseYr);
        it.putExtra("POSITION", wa_position);
        startActivity(it);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tv_nbw:

                isNBWSelected = true;
                isBWSelected = false;

                ll_BW.setVisibility(View.GONE);
                ll_NBW.setVisibility(View.VISIBLE);

                tv_nbw.setEnabled(false);
                tv_bw.setEnabled(true);

                nbwIndicator.setBackgroundColor(getResources().getColor(R.color.color_light_blue));
                bwIndicator.setBackgroundColor(getResources().getColor(R.color.color_indicator));

                buttonVisibilityForNbw();
                break;

            case R.id.tv_bw:

                isBWSelected = true;
                isNBWSelected = false;

                ll_NBW.setVisibility(View.GONE);
                ll_BW.setVisibility(View.VISIBLE);

                tv_bw.setEnabled(false);
                tv_nbw.setEnabled(true);

                bwIndicator.setBackgroundColor(getResources().getColor(R.color.color_light_blue));
                nbwIndicator.setBackgroundColor(getResources().getColor(R.color.color_indicator));

                buttonVisibilityForBw();
                break;

            case R.id.bt_nextWA:

                if (tag.equalsIgnoreCase("1")) {
                    nextButtonCallNBW();
                } else if (tag.equalsIgnoreCase("2")) {
                    nextButtonCallBW();
                } else {

                }
                break;

            case R.id.bt_prevWA:

                if (tag.equalsIgnoreCase("1")) {
                    previousButtonCallNBW();
                } else if (tag.equalsIgnoreCase("2")) {
                    previousButtonCallBW();
                } else {

                }
                break;
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
                break;

        }
    }


    void buttonVisibilityForNbw() {

        if (pageNoNBW == totalNBWCount && totalNBWCount > 1) {
            bt_nextWA.setVisibility(View.GONE);
            bt_prevWA.setVisibility(View.VISIBLE);
        } else if (pageNoNBW == 1 && totalNBWCount > 1) {

            bt_prevWA.setVisibility(View.GONE);
            bt_nextWA.setVisibility(View.VISIBLE);
        } else if (pageNoNBW == 1 && totalNBWCount == 1) {
            bt_nextWA.setVisibility(View.GONE);
            bt_prevWA.setVisibility(View.GONE);
        } else {
            bt_nextWA.setVisibility(View.VISIBLE);
            bt_prevWA.setVisibility(View.VISIBLE);
        }


        if (totalNBWCount > 1) {
            tag = "1";
            rl_navigate.setVisibility(View.VISIBLE);

            bottomView.setBackgroundResource(R.color.colorWhite);
            txt_count.setVisibility(View.VISIBLE);
            txt_count.setText(pageNoNBW + "/" + totalNBWCount);
        } else {
            rl_navigate.setVisibility(View.GONE);
            if (totalNBWCount == 1) {

                bottomView.setBackgroundResource(R.drawable.buttom_bar);
                txt_count.setVisibility(View.VISIBLE);
                txt_count.setText(pageNoNBW + "/" + totalNBWCount);
            } else {
                bottomView.setBackgroundResource(R.color.colorWhite);
                txt_count.setVisibility(View.GONE);
            }
        }
    }

    void buttonVisibilityForBw() {

        if (pageNoBW == totalBWCount && totalBWCount > 1) {
            bt_nextWA.setVisibility(View.GONE);
            bt_prevWA.setVisibility(View.VISIBLE);
        } else if (pageNoBW == 1 && totalBWCount > 1) {

            bt_prevWA.setVisibility(View.GONE);
            bt_nextWA.setVisibility(View.VISIBLE);
        } else if (pageNoBW == 1 && totalBWCount == 1) {
            bt_nextWA.setVisibility(View.GONE);
            bt_prevWA.setVisibility(View.GONE);
        } else {
            bt_nextWA.setVisibility(View.VISIBLE);
            bt_prevWA.setVisibility(View.VISIBLE);
        }

        if (totalBWCount > 1) {
            tag = "2";
            rl_navigate.setVisibility(View.VISIBLE);

            bottomView.setBackgroundResource(R.color.colorWhite);
            txt_count.setVisibility(View.VISIBLE);
            txt_count.setText(pageNoBW + "/" + totalBWCount);
        } else {
            rl_navigate.setVisibility(View.GONE);
            if (totalBWCount == 1) {

                bottomView.setBackgroundResource(R.drawable.buttom_bar);
                txt_count.setVisibility(View.VISIBLE);
                txt_count.setText(pageNoBW + "/" + totalBWCount);
            } else {
                bottomView.setBackgroundResource(R.color.colorWhite);
                txt_count.setVisibility(View.GONE);
            }
        }
    }


    private void nextButtonCallNBW() {
        if (pageNoNBW < totalNBWCount) {

            warrantNbwAPICall(pageNoNBW + 1);
        }
    }

    private void nextButtonCallBW() {

        if (pageNoBW < totalBWCount) {
            warrantBWAPICall(pageNoBW + 1);
        }
    }


    private void previousButtonCallNBW() {
        if (pageNoNBW != 0) {
            pgNBW = pageNoNBW - 1;
        }

        if (pgNBW > 0) {
            warrantNbwAPICall(pgNBW);
        }
    }

    private void previousButtonCallBW() {
        if (pageNoBW != 0) {
            pgBW = pageNoBW - 1;
        }

        if (pgBW > 0) {
            warrantBWAPICall(pgBW);
        }
    }

    private void swipePagerCall() {

        includeView.setOnTouchListener(new OnSwipeTouchListener(WarrentFallingDueDetailActivityNew.this) {

            @Override
            public void onSwipeLeft() {

                if (isNBWSelected && !isBWSelected && (pageNoNBW < totalNBWCount)) {
                    nextButtonCallNBW();
                } else if (isBWSelected && !isNBWSelected && (pageNoBW < totalBWCount)) {
                    nextButtonCallBW();
                } else {

                }
            }


            @Override
            public void onSwipeRight() {
                if (isNBWSelected && !isBWSelected) {
                    previousButtonCallNBW();
                } else if (isBWSelected && !isNBWSelected) {
                    previousButtonCallBW();
                } else {

                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }
    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
