package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.adapter.DashboardArrestPagerAdapter;
import com.kp.facedetection.fragments.DashboardArrestOthersFragment;
import com.kp.facedetection.fragments.DashboardArrestSpecificFragment;
import com.kp.facedetection.model.SpecificArrestDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ArrestTabFromDashboardActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String selectedDate = "";
    private String selected_div = "";
    private String selected_ps = "";
    private String selected_crime = "";

    private String totalResult, totalResultOther;
    private String[] keys;
    private String[] values;
    private String pageNo, pageNoOther;

    private View specificTab, otherTab, specificIndicator, otherIndicator;
    private TextView tvSpecific, tvOther;
    private TextView tv_SpecificCount,tv_OtherCount ;
    private ViewPager viewPager;

    private ArrayList<SpecificArrestDetails> specificArrestDetailsList = new ArrayList<>();
    private ArrayList<SpecificArrestDetails> otherArrestDetailsList = new ArrayList<>();
    DashboardArrestPagerAdapter pagerAdapter;

    TextView chargesheetNotificationCount;
    String notificationCount="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.arrest_tab_from_dashboard_layout);
        ObservableObject.getInstance().addObserver(this);

        setToolBar();
        initViews();
        specificArrestDetailsApiCall();
        otherArrestDetailsApiCall();
    }

    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }


    private void initViews(){

        selectedDate = getIntent().getExtras().getString("SELECTED_DATE_ARREST");
        selected_div = getIntent().getExtras().getString("SELECTED_DIV_ARREST");
        selected_ps = getIntent().getExtras().getString("SELECTED_PS_ARREST");
        selected_crime = getIntent().getExtras().getString("SELECTED_CRIME_ARREST");

        pagerAdapter = new DashboardArrestPagerAdapter(getSupportFragmentManager());

        viewPager = (ViewPager)findViewById(R.id.pager);
        viewPager.setAdapter(pagerAdapter);

        specificIndicator = findViewById(R.id.specificIndicator);
        otherIndicator = findViewById(R.id.otherIndicator);

        specificTab = findViewById(R.id.specificTab);
        tvSpecific = (TextView)specificTab.findViewById(R.id.tv_tabName);
        tv_SpecificCount = (TextView)specificTab.findViewById(R.id.tv_arrestCount);
        tv_SpecificCount.setVisibility(View.INVISIBLE);
        tvSpecific.setText("SPECIFIC ARREST");
        specificTab.setOnClickListener(this);

        otherTab = findViewById(R.id.otherTab);
        tvOther = (TextView)otherTab.findViewById(R.id.tv_tabName);
        tv_OtherCount = (TextView)otherTab.findViewById(R.id.tv_arrestCount);
        tv_OtherCount.setVisibility(View.INVISIBLE);
        tvOther.setText("OTHERS");
        otherTab.setOnClickListener(this);

        specificIndicator.setBackgroundColor(getResources().getColor(R.color.color_light_blue));
        viewPager.setCurrentItem(0);
        viewPager.setOnPageChangeListener(this);
    }


    @Override
    public void onClick(View v) {

        switch(v.getId()){

            case R.id.specificTab:
                specificTabSelect();
                break;

            case R.id.otherTab:
                otherTabSelect();
                break;
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (position == 0){
            specificIndicator.setBackgroundColor(getResources().getColor(R.color.color_light_blue));
            otherIndicator.setBackgroundColor(getResources().getColor(R.color.color_indicator));
        }
        else{
            otherIndicator.setBackgroundColor(getResources().getColor(R.color.color_light_blue));
            specificIndicator.setBackgroundColor(getResources().getColor(R.color.color_indicator));
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    private void specificTabSelect(){
        specificIndicator.setBackgroundColor(getResources().getColor(R.color.color_light_blue));
        otherIndicator.setBackgroundColor(getResources().getColor(R.color.color_indicator));
        viewPager.setCurrentItem(0);
    }

    private void otherTabSelect(){
        otherIndicator.setBackgroundColor(getResources().getColor(R.color.color_light_blue));
        specificIndicator.setBackgroundColor(getResources().getColor(R.color.color_indicator));
        viewPager.setCurrentItem(1);
    }



    private void specificArrestDetailsApiCall(){
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_SPECIFIC_ARREST_VIEW);
        taskManager.setAllSpecificArrestView(true);
        keys = new String[]{"currDate","page_no","div","ps","category","user_id"};
        values = new String[]{selectedDate.trim(),"1",selected_div.trim(), selected_ps.trim(), selected_crime.trim(),Utility.getUserInfo(this).getUserId()};
        taskManager.doStartTask(keys, values, true);
    }


    public void parseAllSpecificArrestViewResult(String result, String[] keys, String[] values ){
        //System.out.println("parseAllSpecificArrestViewResult"+ result);

        try {
            JSONObject jObj = new JSONObject(result);
            if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                if(jObj.opt("totalresult") != null && !jObj.opt("totalresult").equals("")){
                    totalResult = jObj.opt("totalresult").toString();
                    tv_SpecificCount.setVisibility(View.VISIBLE);
                    tv_SpecificCount.setText(totalResult);
                }

                pageNo = jObj.opt("pageno").toString();
                JSONArray resultArray = jObj.optJSONArray("result");
                parseAllSpecificArrestResponse(resultArray);
            } else {
                DashboardArrestSpecificFragment frag = (DashboardArrestSpecificFragment) pagerAdapter.getFragmentList().get(0);
                frag.setTextViewVisibleForSpecific();
                //Utility.showAlertDialog(this, " Search Error ", "No records found", false);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
        }
    }


    private void parseAllSpecificArrestResponse(JSONArray resultArray){

        specificArrestDetailsList.clear();
        for (int i = 0; i < resultArray.length(); i++) {

            SpecificArrestDetails specificArrestDetails = new SpecificArrestDetails(Parcel.obtain());
            try {
                JSONObject jObj = resultArray.getJSONObject(i);
                if(jObj.optString("ADDRESS") != null && !jObj.optString("ADDRESS").equalsIgnoreCase("null") && !jObj.optString("ADDRESS").equalsIgnoreCase("")){
                    specificArrestDetails.setAddress(jObj.optString("ADDRESS"));
                }

                if(jObj.optString("AGE") != null && !jObj.optString("AGE").equalsIgnoreCase("null") && !jObj.optString("AGE").equalsIgnoreCase("") && !jObj.optString("AGE").equalsIgnoreCase("NA")){
                    specificArrestDetails.setAge(jObj.optString("AGE"));
                }else{
                    specificArrestDetails.setAge("");
                }

                if(jObj.optString("ALIASNAME") != null && !jObj.optString("ALIASNAME").equalsIgnoreCase("null") && !jObj.optString("ALIASNAME").equalsIgnoreCase("")
                        && !jObj.optString("ALIASNAME").equalsIgnoreCase("NA") && !jObj.optString("ALIASNAME").equalsIgnoreCase("NIL")){
                    specificArrestDetails.setAliasName(jObj.optString("ALIASNAME"));
                }
                if(jObj.optString("ARRAGAINST")!= null && !jObj.optString("ARRAGAINST").equalsIgnoreCase("null") && !jObj.optString("ARRAGAINST").equalsIgnoreCase("")){
                    specificArrestDetails.setArrestAgainst(jObj.optString("ARRAGAINST"));
                }
                if(jObj.optString("ARRESTEE")!= null && !jObj.optString("ARRESTEE").equalsIgnoreCase("null") && !jObj.optString("ARRESTEE").equalsIgnoreCase("")){
                    specificArrestDetails.setArrestee(jObj.optString("ARRESTEE"));
                }
                if(jObj.optString("ARREST_DATE")!= null && !jObj.optString("ARREST_DATE").equalsIgnoreCase("null") && !jObj.optString("ARREST_DATE").equalsIgnoreCase("")){
                    specificArrestDetails.setArrestDate(jObj.optString("ARREST_DATE"));
                }
                if(jObj.optString("CASEDATE")!= null && !jObj.optString("CASEDATE").equalsIgnoreCase("null") && !jObj.optString("CASEDATE").equalsIgnoreCase("")){
                    specificArrestDetails.setCaseDate(jObj.optString("CASEDATE"));
                }
                if(jObj.optString("CASEREF")!= null && !jObj.optString("CASEREF").equalsIgnoreCase("null") && !jObj.optString("CASEREF").equalsIgnoreCase("")){
                    specificArrestDetails.setCaseRef(jObj.optString("CASEREF"));
                }
                if(jObj.optString("CRIME_HEAD")!= null && !jObj.optString("CRIME_HEAD").equalsIgnoreCase("null") && !jObj.optString("CRIME_HEAD").equalsIgnoreCase("")){
                    specificArrestDetails.setCrimeHead(jObj.optString("CRIME_HEAD"));
                }
                if(jObj.optString("FATHER_HUSBAND")!= null && !jObj.optString("FATHER_HUSBAND").equalsIgnoreCase("null") && !jObj.optString("FATHER_HUSBAND").equalsIgnoreCase("")){
                    specificArrestDetails.setFatherName(jObj.optString("FATHER_HUSBAND"));
                }
                if(jObj.optString("PSNAME")!= null && !jObj.optString("PSNAME").equalsIgnoreCase("null") && !jObj.optString("PSNAME").equalsIgnoreCase("")){
                    specificArrestDetails.setPsName(jObj.optString("PSNAME"));
                }
                if(jObj.optString("SEX")!= null && !jObj.optString("SEX").equalsIgnoreCase("null") && !jObj.optString("SEX").equalsIgnoreCase("") && !jObj.optString("SEX").equalsIgnoreCase("NA")){
                    specificArrestDetails.setGender(jObj.optString("SEX"));
                }
                if(jObj.optString("UNDER_SECTION")!= null && !jObj.optString("UNDER_SECTION").equalsIgnoreCase("null") && !jObj.optString("UNDER_SECTION").equalsIgnoreCase("")){
                    specificArrestDetails.setUnderSection(jObj.optString("UNDER_SECTION"));
                }
                if(jObj.optString("PSCODE") != null && !jObj.optString("PSCODE").equalsIgnoreCase("null") && !jObj.optString("PSCODE").equalsIgnoreCase("")){
                    specificArrestDetails.setPsCode(jObj.optString("PSCODE"));
                }
                if(jObj.optString("PSCODE") != null && !jObj.optString("PSCODE").equalsIgnoreCase("null") && !jObj.optString("PSCODE").equalsIgnoreCase("")){
                    specificArrestDetails.setPsCode(jObj.optString("PSCODE"));
                }
                if(jObj.optString("CASE_YR") != null && !jObj.optString("CASE_YR").equalsIgnoreCase("null") && !jObj.optString("CASE_YR").equalsIgnoreCase("")){
                    specificArrestDetails.setCaseYr(jObj.optString("CASE_YR"));
                }

                if(jObj.optString("UNIT") != null && !jObj.optString("UNIT").equalsIgnoreCase("null") && !jObj.optString("UNIT").equalsIgnoreCase("")){
                    specificArrestDetails.setUNIT(jObj.optString("UNIT"));
                }
                if(jObj.optString("PS") != null && !jObj.optString("PS").equalsIgnoreCase("null") && !jObj.optString("PS").equalsIgnoreCase("")){
                    specificArrestDetails.setPS(jObj.optString("PS"));
                }

                JSONArray pic_url_array = jObj.optJSONArray("PICTURE_URL");
                List<String> pic_list = new ArrayList<>();
                if(pic_url_array != null){
                    for (int k = 0; k < pic_url_array.length(); k++ ){
                        pic_list.add(pic_url_array.optString(k));
                    }
                    specificArrestDetails.setPicture_list(pic_list);
                }


                specificArrestDetailsList.add(specificArrestDetails);

            }
            catch(JSONException e){
                e.printStackTrace();
            }
        }

        DashboardArrestSpecificFragment frag = (DashboardArrestSpecificFragment) pagerAdapter.getFragmentList().get(0);
        frag.addAllSpecificData(specificArrestDetailsList, totalResult, pageNo, keys, values);
    }


    private void otherArrestDetailsApiCall(){
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_OTHER_ARREST_VIEW);
        taskManager.setAllOtherArrestView(true);

        keys = new String[]{"currDate","page_no","div","ps","category","user_id"};
        values = new String[]{selectedDate.trim(),"1",selected_div.trim(), selected_ps.trim(), selected_crime.trim(),Utility.getUserInfo(this).getUserId()};
        taskManager.doStartTask(keys, values, true);
    }

    public void parseAllOtherArrestViewResult(String result){
        //System.out.println("parseAllOtherArrestViewResult"+ result);

        try {
            JSONObject jObj = new JSONObject(result);
            if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                if(jObj.opt("totalresult") != null && !jObj.opt("totalresult").equals("")){
                    totalResultOther = jObj.opt("totalresult").toString();
                    tv_OtherCount.setVisibility(View.VISIBLE);
                    tv_OtherCount.setText(totalResultOther);
                }

                pageNoOther = jObj.opt("pageno").toString();
                JSONArray resultArray = jObj.getJSONArray("result");
                parseAllOtherArrestResponse(resultArray);
            } else {
                DashboardArrestOthersFragment frag = (DashboardArrestOthersFragment) pagerAdapter.getFragmentList().get(1);
                frag.setTextViewVisibleForOther();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
        }
    }


    private void parseAllOtherArrestResponse(JSONArray resultArray){

        otherArrestDetailsList.clear();
        for (int i = 0; i < resultArray.length(); i++) {

            SpecificArrestDetails specificArrestDetails = new SpecificArrestDetails(Parcel.obtain());
            try {
                JSONObject jObj = resultArray.getJSONObject(i);
                if(jObj.optString("ADDRESS") != null && !jObj.optString("ADDRESS").equalsIgnoreCase("null") && !jObj.optString("ADDRESS").equalsIgnoreCase("")){
                    specificArrestDetails.setAddress(jObj.optString("ADDRESS"));
                }

                if(jObj.optString("AGE") != null && !jObj.optString("AGE").equalsIgnoreCase("null") && !jObj.optString("AGE").equalsIgnoreCase("") && !jObj.optString("AGE").equalsIgnoreCase("NA")){
                    specificArrestDetails.setAge(jObj.optString("AGE"));
                }else{
                    specificArrestDetails.setAge("");
                }


                if(jObj.optString("ALIASNAME") != null && !jObj.optString("ALIASNAME").equalsIgnoreCase("null") && !jObj.optString("ALIASNAME").equalsIgnoreCase("")
                        && !jObj.optString("ALIASNAME").equalsIgnoreCase("NA") && !jObj.optString("ALIASNAME").equalsIgnoreCase("NIL")){
                    specificArrestDetails.setAliasName(jObj.optString("ALIASNAME"));
                }
                if(jObj.optString("ARRAGAINST")!= null && !jObj.optString("ARRAGAINST").equalsIgnoreCase("null") && !jObj.optString("ARRAGAINST").equalsIgnoreCase("")){
                    specificArrestDetails.setArrestAgainst(jObj.optString("ARRAGAINST"));
                }
                if(jObj.optString("ARRESTEE")!= null && !jObj.optString("ARRESTEE").equalsIgnoreCase("null") && !jObj.optString("ARRESTEE").equalsIgnoreCase("")){
                    specificArrestDetails.setArrestee(jObj.optString("ARRESTEE"));
                }
                if(jObj.optString("ARREST_DATE")!= null && !jObj.optString("ARREST_DATE").equalsIgnoreCase("null") && !jObj.optString("ARREST_DATE").equalsIgnoreCase("")){
                    specificArrestDetails.setArrestDate(jObj.optString("ARREST_DATE"));
                }
                if(jObj.optString("CASEDATE")!= null && !jObj.optString("CASEDATE").equalsIgnoreCase("null") && !jObj.optString("CASEDATE").equalsIgnoreCase("")){
                    specificArrestDetails.setCaseDate(jObj.optString("CASEDATE"));
                }
                if(jObj.optString("CASEREF")!= null && !jObj.optString("CASEREF").equalsIgnoreCase("null") && !jObj.optString("CASEREF").equalsIgnoreCase("")){
                    specificArrestDetails.setCaseRef(jObj.optString("CASEREF"));
                }
                if(jObj.optString("CRIME_HEAD")!= null && !jObj.optString("CRIME_HEAD").equalsIgnoreCase("null") && !jObj.optString("CRIME_HEAD").equalsIgnoreCase("")){
                    specificArrestDetails.setCrimeHead(jObj.optString("CRIME_HEAD"));
                }
                if(jObj.optString("FATHER_HUSBAND")!= null && !jObj.optString("FATHER_HUSBAND").equalsIgnoreCase("null") && !jObj.optString("FATHER_HUSBAND").equalsIgnoreCase("")){
                    specificArrestDetails.setFatherName(jObj.optString("FATHER_HUSBAND"));
                }
                if(jObj.optString("PSNAME")!= null && !jObj.optString("PSNAME").equalsIgnoreCase("null") && !jObj.optString("PSNAME").equalsIgnoreCase("")){
                    specificArrestDetails.setPsName(jObj.optString("PSNAME"));
                }
                if(jObj.optString("SEX")!= null && !jObj.optString("SEX").equalsIgnoreCase("null") && !jObj.optString("SEX").equalsIgnoreCase("") && !jObj.optString("SEX").equalsIgnoreCase("NA")){
                    specificArrestDetails.setGender(jObj.optString("SEX"));
                }
                if(jObj.optString("UNDER_SECTION")!= null && !jObj.optString("UNDER_SECTION").equalsIgnoreCase("null") && !jObj.optString("UNDER_SECTION").equalsIgnoreCase("")){
                    specificArrestDetails.setUnderSection(jObj.optString("UNDER_SECTION"));
                }
                if(jObj.optString("PSCODE") != null && !jObj.optString("PSCODE").equalsIgnoreCase("null") && !jObj.optString("PSCODE").equalsIgnoreCase("")){
                    specificArrestDetails.setPsCode(jObj.optString("PSCODE"));
                }
                if(jObj.optString("CASE_YR") != null && !jObj.optString("CASE_YR").equalsIgnoreCase("null") && !jObj.optString("CASE_YR").equalsIgnoreCase("")){
                    specificArrestDetails.setCaseYr(jObj.optString("CASE_YR"));
                }
                if(jObj.optString("UNIT") != null && !jObj.optString("UNIT").equalsIgnoreCase("null") && !jObj.optString("UNIT").equalsIgnoreCase("")){
                    specificArrestDetails.setUNIT(jObj.optString("UNIT"));
                }
                if(jObj.optString("PS") != null && !jObj.optString("PS").equalsIgnoreCase("null") && !jObj.optString("PS").equalsIgnoreCase("")){
                    specificArrestDetails.setPS(jObj.optString("PS"));
                }

                JSONArray pic_url_array = jObj.optJSONArray("PICTURE_URL");
                List<String> pic_list = new ArrayList<>();
                if(pic_url_array != null) {
                    for (int k = 0; k < pic_url_array.length(); k++) {
                        pic_list.add(pic_url_array.optString(k));
                    }
                    specificArrestDetails.setPicture_list(pic_list);
                }

                otherArrestDetailsList.add(specificArrestDetails);
            }
            catch(JSONException e){
                e.printStackTrace();
            }
        }
        DashboardArrestOthersFragment frag = (DashboardArrestOthersFragment) pagerAdapter.getFragmentList().get(1);
        frag.addAllOtherData(otherArrestDetailsList, totalResultOther, pageNo, keys, values);
    }



    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
           /* case R.id.menu_send_push:
                //tabHost.setCurrentTab(2);
                //startActivity(new Intent(this, SendPushActivity.class));
                //	pushFragments("Push", new SendPushActivity(), true, false);
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }


    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }
    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }

}
