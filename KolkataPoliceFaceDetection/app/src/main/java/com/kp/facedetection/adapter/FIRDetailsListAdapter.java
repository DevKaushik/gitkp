package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnFIRDetailsEventItemListener;
import com.kp.facedetection.interfaces.OnFIRDetailsNameListener;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.model.FIRDetails;

import java.util.List;

/**
 * Created by DAT-Asset-131 on 18-05-2016.
 */
public class FIRDetailsListAdapter extends BaseAdapter {

    private Context context;
    private List<FIRDetails> firDetailsList;
    private String flag;
    OnFIRDetailsNameListener onFIRDetailsNameListener;
    OnFIRDetailsEventItemListener onFIRDetailsEventItemListener;

    public FIRDetailsListAdapter(Context context, List<FIRDetails> firDetailsList, String flag) {

        this.context = context;
        this.firDetailsList = firDetailsList;
        this.flag = flag;
    }

    @Override
    public int getCount() {

        if (firDetailsList.size() > 0) {
            return firDetailsList.size();
        } else {
            return 0;
        }

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.layout_fir_details_item_row_modified2, null);

            holder.tv_SlNo = (TextView) convertView
                    .findViewById(R.id.tv_SlNo);

            holder.tv_FirName = (TextView) convertView
                    .findViewById(R.id.tv_FirName);

            holder.tv_status = (TextView) convertView
                    .findViewById(R.id.tv_status);

            holder.iv_eventLog = (ImageView) convertView
                    .findViewById(R.id.iv_eventLog);

            Constants.changefonts(holder.tv_SlNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_FirName, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_status, context, "Calibri.ttf");
            //Constants.changefonts(holder.tv_eventLog, context, "Calibri.ttf");

            holder.tv_FirName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String firName = "";
        if (flag.equalsIgnoreCase("OF")) {

            String under_section = "";
            int under_section_data_length = firDetailsList.get(position).getFirUnderSectionDetailsList().size();

            for (int i = 0; i < under_section_data_length; i++) {

                if (i == 0) {
                    under_section = firDetailsList.get(position).getFirUnderSectionDetailsList().get(i).getUnder_section().trim();
                } else {
                    under_section = under_section + ", " + firDetailsList.get(position).getFirUnderSectionDetailsList().get(i).getUnder_section().trim();
                }
            }
            firName = firDetailsList.get(position).getPs_name() + " C/No." + firDetailsList.get(position).getCase_ref() + " Dated " + firDetailsList.get(position).getCase_date() + " u/s " + under_section;
        } else if (flag.equalsIgnoreCase("OC")) {

            String under_section = firDetailsList.get(position).getUnder_section();
            firName = firDetailsList.get(position).getPs_code() + " PS" + " C/No." + firDetailsList.get(position).getCase_ref() + " Dated " + firDetailsList.get(position).getCase_date() + " u/s " + under_section;
        } else if (flag.equalsIgnoreCase("CF")) {

            String under_section = "";
            int under_section_data_length = firDetailsList.get(position).getFirUnderSectionDetailsList().size();

            for (int i = 0; i < under_section_data_length; i++) {

                if (i == 0) {
                    under_section = firDetailsList.get(position).getFirUnderSectionDetailsList().get(i).getUnder_section().trim();
                } else {
                    under_section = under_section + ", " + firDetailsList.get(position).getFirUnderSectionDetailsList().get(i).getUnder_section().trim();
                }
            }
            firName = firDetailsList.get(position).getPs_name() + " C/No." + firDetailsList.get(position).getCase_ref() + " Dated " + firDetailsList.get(position).getCase_date() + " u/s " + under_section;
        }

        SpannableString mySpannableString = new SpannableString(firName);
        mySpannableString.setSpan(new UnderlineSpan(), 0, mySpannableString.length(), 0);
        holder.tv_FirName.setText(mySpannableString);


        holder.tv_SlNo.setText(Integer.toString(position + 1));

        // STATUS name change
        if (firDetailsList.get(position).getFir_status().equalsIgnoreCase(Constants.STATUS_INVESTIGATION_IN_PROGRESS))
            holder.tv_status.setText(Constants.STATUS_PENDING);
        else if (firDetailsList.get(position).getFir_status().equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_NON_COGNIZABLE))
            holder.tv_status.setText(Constants.STATUS_FR_NON_COG);
        else if (firDetailsList.get(position).getFir_status().equalsIgnoreCase(Constants.STATUS_CHARGESHEETED))
            holder.tv_status.setText(Constants.STATUS_CS);
        else if (firDetailsList.get(position).getFir_status().equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_CIVIL))
            holder.tv_status.setText(Constants.STATUS_FR_CIVIL);
        else if (firDetailsList.get(position).getFir_status().equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_TRUE))
            holder.tv_status.setText(Constants.STATUS_FRT);
        else if (firDetailsList.get(position).getFir_status().equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_FALSE))
            holder.tv_status.setText(Constants.STATUS_FR_FALSE);
        else if (firDetailsList.get(position).getFir_status().equalsIgnoreCase(Constants.STATUS_TRANSFERRED_TO_OTHER_UNIT)) {
            if(firDetailsList.get(position).getCaseTransferList().size() > 0){

                String trfd_psDetails = "";
                if(firDetailsList.get(position).getCaseTransferList().get(0).getPsName() != null
                        && !firDetailsList.get(position).getCaseTransferList().get(0).getPsName().equalsIgnoreCase("null")
                        && !firDetailsList.get(position).getCaseTransferList().get(0).getPsName().equalsIgnoreCase("")){

                    trfd_psDetails = firDetailsList.get(position).getCaseTransferList().get(0).getPsName();

                    if(firDetailsList.get(position).getCaseTransferList().get(0).getToUnit() != null
                            && !firDetailsList.get(position).getCaseTransferList().get(0).getToUnit().equalsIgnoreCase("null")
                            && !firDetailsList.get(position).getCaseTransferList().get(0).getToUnit().equalsIgnoreCase("")){

                        trfd_psDetails = trfd_psDetails + ", " + firDetailsList.get(position).getCaseTransferList().get(0).getToUnit();
                    }
                    holder.tv_status.setText(trfd_psDetails);
                }
                else if(firDetailsList.get(position).getCaseTransferList().get(0).getToUnit() != null
                        && !firDetailsList.get(position).getCaseTransferList().get(0).getToUnit().equalsIgnoreCase("null")
                        && !firDetailsList.get(position).getCaseTransferList().get(0).getToUnit().equalsIgnoreCase("")){

                    trfd_psDetails = firDetailsList.get(position).getCaseTransferList().get(0).getToUnit();
                    holder.tv_status.setText(trfd_psDetails);
                }
                else{
                    holder.tv_status.setText(Constants.STATUS_TRFD);
                }
            }
            else {
                holder.tv_status.setText(Constants.STATUS_TRFD);
            }
        }
        else
            holder.tv_status.setText(firDetailsList.get(position).getFir_status());


        // Event Log part
        if(firDetailsList.get(position).getPrsStatusList().size()> 0 ){

            holder.iv_eventLog.setImageResource(R.drawable.event_log);
        }
        else{

            holder.iv_eventLog.setImageResource(R.drawable.no_event_log);
        }


        holder.tv_FirName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (onFIRDetailsNameListener != null){
                    onFIRDetailsNameListener.onFirNameClick(v,position);
                }
            }
        });

        holder.iv_eventLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onFIRDetailsEventItemListener != null){
                    onFIRDetailsEventItemListener.onFirEventLogClick(v,position);
                }
            }
        });

        return convertView;
    }

    public void setFIRNameTapListener(OnFIRDetailsNameListener onFIRDetailsNameListener){
        this.onFIRDetailsNameListener = onFIRDetailsNameListener;
    }

    public void setFIREventLogTapListener(OnFIRDetailsEventItemListener onFIRDetailsEventItemListener){
        this.onFIRDetailsEventItemListener = onFIRDetailsEventItemListener;
    }

    class ViewHolder{

        TextView tv_SlNo;
        TextView tv_FirName;
        TextView tv_status;
        //TextView tv_eventLog;
        ImageView iv_eventLog;

    }

}


