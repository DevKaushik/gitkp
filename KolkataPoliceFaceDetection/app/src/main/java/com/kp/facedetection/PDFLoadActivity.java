package com.kp.facedetection;
import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.asynctasks.AsynctaskForDownloadPDF;
import com.kp.facedetection.interfaces.OnPDFDownload;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

public class PDFLoadActivity extends BaseActivity implements View.OnClickListener, OnPDFDownload, Observer {

    private TextView tv_watermark;
    ImageView iv_download;
    private WebView webview;
    private ProgressDialog pDialog;
    private String pdfUrl = "";
    private String existing_pdf_path_fir = "";
    private String existing_pdf_path_pm = "";

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private boolean isFIR_PDF;
    private String pdf_file_name = "";
    private String ps_code = "";
    private String case_no = "";
    private String case_year = "";
    private String pm_no  = "";
    private String pm_year = "";
    private String user_Id = "";

    String path = "";
    boolean isDownloadPDF=false;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_show);
        ObservableObject.getInstance().addObserver(this);
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }


        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        initViews();
        listener();
    }


    private void initViews() {

        tv_watermark = (TextView) findViewById(R.id.tv_watermark);
        iv_download = (ImageView) findViewById(R.id.iv_download);

        webview = (WebView) findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);


        pDialog = new ProgressDialog(PDFLoadActivity.this);
        //pDialog.setTitle("PDF");
        pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);

        //String pdf = "http://182.71.240.212/kpfirs/uploads/2017/S1/5-2017-S1.pdf";

        pdfUrl = getIntent().getStringExtra("PDF_DOWNLOAD_URL");
         if(getIntent().hasExtra("USER_ID"))
         {
             user_Id=getIntent().getStringExtra("USER_ID");
         }
        if(getIntent().hasExtra("PS_CODE"))
        {
             ps_code=getIntent().getStringExtra("PS_CODE");
        }
        if(getIntent().hasExtra("CASE_NO"))
        {
            case_no=getIntent().getStringExtra("CASE_NO");
        }
        if(getIntent().hasExtra("CASE_YEAR"))
        {
            case_year=getIntent().getStringExtra("CASE_YEAR");
        }
        if(getIntent().hasExtra("PM_NO"))
        {
            pm_no=getIntent().getStringExtra("PM_NO");
        }
        if(getIntent().hasExtra("PM_YEAR"))
        {
            pm_year=getIntent().getStringExtra("PM_YEAR");
        }

        if(getIntent().hasExtra("REPORT_TYPE"))
        {
            isFIR_PDF=getIntent().getBooleanExtra("REPORT_TYPE",false);
        }
        if (getIntent().hasExtra("DOWNLOAD_PDF"))
        {
            isDownloadPDF=getIntent().getBooleanExtra("DOWNLOAD_PDF",false);
        }
        if(isDownloadPDF) {
            iv_download.setVisibility(View.VISIBLE);
            iv_download.setOnClickListener(this);
        }
        else
        {
            iv_download.setVisibility(View.GONE);
        }

        Log.e("URL :", " " + pdfUrl);
        webview.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=" + pdfUrl);


         /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);


    }


    private void listener() {
        webview.setWebViewClient(new WebViewClient() {


            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pDialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                pDialog.dismiss();
                /*webview.loadUrl("javascript:(function() { " +
                        "document.getElementsByClassName('ndfHFb-c4YZDc-GSQQnc-LgbsSe ndfHFb-c4YZDc-to915-LgbsSe VIpgJd-TzA9Ye-eEGnhe ndfHFb-c4YZDc-LgbsSe')[0].style.display='none'; })()");*/

                if (Build.VERSION.SDK_INT < 19) {
                    webview.loadUrl("javascript:(function() { " +
                            "document.getElementsByClassName('ndfHFb-c4YZDc-GSQQnc-LgbsSe ndfHFb-c4YZDc-to915-LgbsSe VIpgJd-TzA9Ye-eEGnhe ndfHFb-c4YZDc-LgbsSe')[0].style.display='none'; })()");
                } else {
                    webview.loadUrl("javascript:(function() { " +
                            "document.querySelector('[role=\"toolbar\"]').remove();})()");
                }
            }
        });
    }


   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
           /* case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_download:
                if (ps_code.isEmpty()) {
                    Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, "PS code is not available", false);
                }
                if(isFIR_PDF) {
                    if (case_no.isEmpty()) {
                        Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, "Case No is not available", false);
                    } else if (case_year.isEmpty()) {
                        Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, "Case Year is not available", false);
                    } else {
                        verifyStoragePermissions();
                    }
                }
                else
                {
                    if (pm_no.isEmpty()) {
                        Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, "PM No is not available", false);
                    } else if (pm_year.isEmpty()) {
                        Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, "PM Year is not available", false);
                    } else {
                        verifyStoragePermissions();
                    }

                }
                break;
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;

        }
    }
    public void verifyStoragePermissions()
    {
        if (ActivityCompat.checkSelfPermission(PDFLoadActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
          //  L.e("Permission not granted------------");
                ActivityCompat.requestPermissions(PDFLoadActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);

        } else {
            //You already have the permission, just go ahead.
          //  L.e("Permission granted------------");
            proceedAfterPermission();
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
                proceedAfterPermission();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(PDFLoadActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(PDFLoadActivity.this);
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();


                            ActivityCompat.requestPermissions(PDFLoadActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(getBaseContext(),"Unable to get Permission",Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public void proceedAfterPermission()
    {
         http://182.71.240.211/crimebabuapp/uploads/pmreport/CMCH-2017-1093.pdf
        if (pdfUrl.length() > 0) {
            pdf_file_name = pdfUrl.substring(pdfUrl.lastIndexOf("/"));
            // String path = "";
            File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "KP");
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.e("KP", "failed to create directory");
                }
            }
            path = mediaStorageDir.getPath() + File.separator + pdf_file_name;
            L.e(path);
            File file = new File(path);
            if (file.exists()) {
                //Toast.makeText(this, "File exist", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.myDialog);
                builder.setTitle("Alert!!!");
                builder.setMessage("Report already downloaded.Do you want to update the report?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //showPDF(path);
                        downloadPDFData(pdfUrl);

                    }
                });


                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                //Toast.makeText(this, "File not exist", Toast.LENGTH_SHORT).show();

                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.myDialog);
                builder.setTitle("Alert!!!");
                builder.setMessage("Do you want to download this report?");

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        downloadPDFData(pdfUrl);
                        //   updatelog();

                    }
                });


                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }


    }
    public void updatelog()
    {
        if (!Constants.internetOnline(PDFLoadActivity.this)) {
            Utility.showAlertDialog(PDFLoadActivity.this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_UPDATE_FIR_DOWNLOADLOG);
        taskManager.setFirDownloadLog(true);

        String[] keys = { "user_id","ps","fir_no","fir_yr"};
        String[] values = {user_Id.trim(), ps_code.trim(), case_no.trim(), case_year.trim()};
        taskManager.doStartTask(keys, values, true);
    }
    public void updatelogpm()
    {
        if (!Constants.internetOnline(PDFLoadActivity.this)) {
            Utility.showAlertDialog(PDFLoadActivity.this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_UPDATE_PM_DOWNLOADLOG);
        taskManager.setFirDownloadLog(true);

        String[] keys = { "user_id","ps","pm_no","pm_year"};
        String[] values = {user_Id.trim(), ps_code.trim(), pm_no.trim(), pm_year.trim()};
        taskManager.doStartTask(keys, values, true);
    }
    public void parseFirDownloadLog(String result) {
        ///System.out.println("CaseSearchFIRDetailsResult: " + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                     // String message=jObj.opt("message").toString();
                     // Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                    showPDF(path);
                } else {
                    File file = new File(path);
                    file.delete();
                    AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.myDialog);
                    builder.setTitle("Alert!!!");
                    builder.setMessage(Constants.ERROR_PDF_DOWNLOAD_MSG);

                    builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            downloadPDFData(pdfUrl);
                            //   updatelog();

                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }

    }


    private void downloadPDFData(String pdf_url) {

        if (Constants.internetOnline(PDFLoadActivity.this)) {

            AsynctaskForDownloadPDF asynctaskForDownloadPDF = new AsynctaskForDownloadPDF(PDFLoadActivity.this, pdf_url, isFIR_PDF);
            asynctaskForDownloadPDF.execute();
            asynctaskForDownloadPDF.setDelegate(PDFLoadActivity.this);

        } else {

            Utility.showAlertDialog(PDFLoadActivity.this, "Connectivity Problem", "Sorry, internet connection not found", false);

        }

    }




    @Override
    public void onPDFDownloadSuccess(String filePath, boolean forWhich) {
        if(forWhich) {
            path = filePath;
            Log.e("OnPDFDownloadSuccess", filePath);
            //showPDF(filePath);
            updatelog();
        }
        else
        {
            path = filePath;
            Log.e("OnPDFDownloadSuccess", filePath);
           // showPDF(filePath);
           updatelogpm();
        }

    }

    @Override
    public void onPDFDownloadError(String error) {

        PDFLoadActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                Utility.showToast(PDFLoadActivity.this, Constants.ERROR_EXCEPTION_MSG, "long");
            }
        });


    }

    private void showPDF(String filePath) {

        File file = new File(filePath);
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file), "application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something

            Utility.showAlertDialog(PDFLoadActivity.this, "PDF Reader Problem", "Your device doesn't have PDF reader. Download a PDF reader app from play store", false);

        }
    }


    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
