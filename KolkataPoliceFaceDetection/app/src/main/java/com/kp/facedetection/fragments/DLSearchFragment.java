package com.kp.facedetection.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.kp.facedetection.DLSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.model.DLSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kaushik on 10-09-2016.
 */
public class DLSearchFragment extends Fragment {

    private RelativeLayout relative_dl_main;
    private TextView tv_notAuthorized;

    private Button btn_dlSearch;
    private Button btn_reset;

    private EditText et_dl_number;
    private EditText et_holder_name;
    private EditText et_dl_address;
    private EditText et_dl_pincode;
    private EditText et_dl_mobile;
    private EditText et_fathers_name;
    private EditText et_issue_authority;

    private Spinner sp_dl_holder_age;

    private RadioGroup radio_group_gender;

    private String pageno="";
    private String totalResult="";
    private String[] keys;
    private String[] values;

    private String genderValue = "";

    private List<DLSearchDetails> dlSearchDeatilsList;

    ArrayAdapter<CharSequence> ageAdapter;
    String userId="";
    String devicetoken="";
    String auth_key="";

    public static DLSearchFragment newInstance() {

        DLSearchFragment DLSearchFragment = new DLSearchFragment();
        return DLSearchFragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dl_search_layout, container, false);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        userId= Utility.getUserInfo(getContext()).getUserId();
        relative_dl_main = (RelativeLayout) view.findViewById(R.id.relative_dl_main);
        tv_notAuthorized = (TextView) view.findViewById(R.id.tv_notAuthorized);

        btn_dlSearch = (Button) view.findViewById(R.id.btn_dlSearch);
        btn_reset = (Button) view.findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_dlSearch);
        Constants.buttonEffect(btn_reset);

        et_dl_number = (EditText) view.findViewById(R.id.et_dl_number);
        et_holder_name = (EditText) view.findViewById(R.id.et_holder_name);
        et_dl_address = (EditText) view.findViewById(R.id.et_dl_address);
        et_dl_pincode = (EditText) view.findViewById(R.id.et_dl_pincode);
        et_dl_mobile = (EditText) view.findViewById(R.id.et_dl_mobile);
        et_fathers_name = (EditText) view.findViewById(R.id.et_fathers_name);
        et_issue_authority = (EditText) view.findViewById(R.id.et_issue_authority);

        sp_dl_holder_age = (Spinner) view.findViewById(R.id.sp_dl_holder_age);

        radio_group_gender = (RadioGroup) view.findViewById(R.id.radio_group_gender);

        setAgeSpinnerData();


        try{
            if (Utility.getUserInfo(getActivity()).getDl_allow().equalsIgnoreCase("1")) {
                relative_dl_main.setVisibility(View.VISIBLE);
                tv_notAuthorized.setVisibility(View.GONE);
            } else {
                relative_dl_main.setVisibility(View.GONE);
                tv_notAuthorized.setVisibility(View.VISIBLE);
                Constants.changefonts(tv_notAuthorized, getActivity(), "Calibri Bold.ttf");
            }
        }catch (NullPointerException e){
            relative_dl_main.setVisibility(View.GONE);
            tv_notAuthorized.setVisibility(View.VISIBLE);
            Constants.changefonts(tv_notAuthorized, getActivity(), "Calibri Bold.ttf");
        }

        clickEvents();

    }

    /* All click events */

    private void clickEvents() {

        radioGroupClickEvents();

        /* search button click event */

        btn_dlSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //fetchSearchResult();
                String dl_number = et_dl_number.getText().toString().trim();
                String holder_name = et_holder_name.getText().toString().trim();
                String dl_address = et_dl_address.getText().toString().trim();
                String dl_pincode = et_dl_pincode.getText().toString().trim();
                String dl_father_name = et_fathers_name.getText().toString().trim();
                String dl_issue_authority = et_issue_authority.getText().toString().trim();
                String dl_age = sp_dl_holder_age.getSelectedItem().toString().replace("Select Age Range", "").trim();
                //  String dl_mobile = et_dl_mobile.getText().toString().trim();

                if (!dl_number.equalsIgnoreCase("") || !holder_name.equalsIgnoreCase("") || !dl_address.equalsIgnoreCase("")
                        || !dl_pincode.equalsIgnoreCase("") || !dl_father_name.equalsIgnoreCase("") || !dl_issue_authority.equalsIgnoreCase("")
                        || !dl_age.equalsIgnoreCase("") || !genderValue.equalsIgnoreCase("")) {

                    showAlertForProceed();
                } else {

                    showAlertDialog(getActivity(), " Search Error!!! ", "Please provide at least one value for search", false);

                }


            }
        });

         /* reset button click event */

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_dl_number.setText("");
                et_holder_name.setText("");
                et_dl_address.setText("");
                et_dl_pincode.setText("");
                et_dl_mobile.setText("");
                et_fathers_name.setText("");
                et_issue_authority.setText("");


                radio_group_gender.clearCheck();
                genderValue = "";

                setAgeSpinnerData();

            }
        });

    }

    private void showAlertForProceed() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setTitle("Alert!");
        alertDialog.setMessage("It will take considerable time, proceed");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                fetchSearchResult();
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        alertDialog.show();
    }

    private void fetchSearchResult() {

        String dl_number = et_dl_number.getText().toString().trim();
        String holder_name = et_holder_name.getText().toString().trim();
        String dl_address = et_dl_address.getText().toString().trim();
        String dl_pincode = et_dl_pincode.getText().toString().trim();
        String dl_father_name = et_fathers_name.getText().toString().trim();
        String dl_issue_authority = et_issue_authority.getText().toString().trim();
        String dl_age = sp_dl_holder_age.getSelectedItem().toString().replace("Select Age Range", "").trim();
       // String dl_mobile = et_dl_mobile.getText().toString().trim();

        if(!dl_number.equalsIgnoreCase("") || !holder_name.equalsIgnoreCase("") || !dl_address.equalsIgnoreCase("")
                || !dl_pincode.equalsIgnoreCase("") || !dl_father_name.equalsIgnoreCase("") || !dl_issue_authority.equalsIgnoreCase("")
                || !dl_age.equalsIgnoreCase("") || !genderValue.equalsIgnoreCase("")){
            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.MODIFIED_METHOD_DOCUMENT_DL_SEARCH);
            taskManager.setDocumentDLSearch(true);
            try{
                devicetoken = GCMRegistrar.getRegistrationId(getActivity());
                auth_key=Utility.getDocumentSearchSesionId(getActivity());
            }
            catch (Exception e){

            }
            String[] keys = {"dl_no", "dl_name", "dl_address", "dl_pin", "page_no", "dl_fathername", "dl_issueauthority", "dl_age", "dl_sex", "user_id", "device_type", "device_token","imei","auth_key"};
            String[] values = {dl_number.trim(), holder_name.trim(), dl_address, dl_pincode.trim(), "1", dl_father_name.trim(), dl_issue_authority.trim(), dl_age.trim(), genderValue.trim(), userId,"android", devicetoken,Utility.getImiNO(getActivity()),auth_key};
            taskManager.doStartTask(keys, values, true, true);

        }
        else{

            showAlertDialog(getActivity(), " Search Error!!! ", "Please provide atleast one value for search", false);

        }

    }


    /**
     * Function to display simple Alert Dialog
     *
     * @param context - application context
     * @param title   - alert dialog title
     * @param message - alert message
     * @param status  - success/failure (used to set icon)
     */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    public void parseDocumentDLSearch(String result,String[] keys, String[] values) {

        //System.out.println("Document DL Search Result: " + result);


        if(result != null && !result.equals("")){

            try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.optString("status").equalsIgnoreCase("1")){

                    this.keys=keys;
                    this.values=values;
                    pageno=jObj.opt("page").toString();
                    totalResult=jObj.opt("count").toString();

                    JSONArray resultArray=jObj.getJSONArray("result");

                    parseDLSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(getActivity()," Search Error ",jObj.optString("message"),false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                showAlertDialog(getActivity(), " Search Error!!! ", "Some error has been encountered", false);
            }
        }

    }

    private void parseDLSearchResponse(JSONArray resultArray) {

        dlSearchDeatilsList = new ArrayList<DLSearchDetails>();

        for(int i=0;i<resultArray.length();i++){

            JSONObject jsonObj=null;

            try {
                jsonObj=resultArray.getJSONObject(i);

                DLSearchDetails dlSearchDetails = new DLSearchDetails();

                String dlpa_address="";
                String dlta_address="";

                if(!jsonObj.optString("DLNO").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_No(jsonObj.optString("DLNO"));
                }

                if(!jsonObj.optString("DLNAME").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_Name(jsonObj.optString("DLNAME"));
                }

                if(!jsonObj.optString("DLSWDOF").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_SwdOf(jsonObj.optString("DLSWDOF"));
                }

                if(!jsonObj.optString("DLDOB").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_Dob(jsonObj.optString("DLDOB"));
                }

                if(!jsonObj.optString("DLSEX").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_Sex(jsonObj.optString("DLSEX"));
                }

                if(!jsonObj.optString("DLPADD1").equalsIgnoreCase("null") && !jsonObj.optString("DLPADD1").equalsIgnoreCase("")){
                    dlpa_address = dlpa_address + jsonObj.optString("DLPADD1");
                }
                if(!jsonObj.optString("DLPADD2").equalsIgnoreCase("null") && !jsonObj.optString("DLPADD2").equalsIgnoreCase("")){
                    dlpa_address = dlpa_address +", "+ jsonObj.optString("DLPADD2");
                }
                if(!jsonObj.optString("DLPADD3").equalsIgnoreCase("null") && !jsonObj.optString("DLPADD3").equalsIgnoreCase("")){
                    dlpa_address = dlpa_address +", "+ jsonObj.optString("DLPADD3");
                }
                if(!jsonObj.optString("DLPPINCD").equalsIgnoreCase("null") && !jsonObj.optString("DLPPINCD").equalsIgnoreCase("")){
                    dlpa_address = dlpa_address +", "+ jsonObj.optString("DLPPINCD");
                }

                dlSearchDetails.setDl_PAddress(dlpa_address);

                if(!jsonObj.optString("DLTADD1").equalsIgnoreCase("null") && !jsonObj.optString("DLTADD1").equalsIgnoreCase("")){
                    dlta_address = dlta_address + jsonObj.optString("DLTADD1");
                }
                if(!jsonObj.optString("DLTADD2").equalsIgnoreCase("null") && !jsonObj.optString("DLTADD2").equalsIgnoreCase("")){
                    dlta_address = dlta_address +", "+ jsonObj.optString("DLTADD2");
                }
                if(!jsonObj.optString("DLTADD3").equalsIgnoreCase("null") && !jsonObj.optString("DLTADD3").equalsIgnoreCase("")){
                    dlta_address = dlta_address +", "+ jsonObj.optString("DLTADD3");
                }
                if(!jsonObj.optString("DLTPINCD").equalsIgnoreCase("null") && !jsonObj.optString("DLTPINCD").equalsIgnoreCase("")){
                    dlta_address = dlta_address +", "+ jsonObj.optString("DLTPINCD");
                }

                dlSearchDetails.setDl_TAddress(dlta_address);

                if(!jsonObj.optString("DLISSUEAUTHORITY").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_IssueAuthority(jsonObj.optString("DLISSUEAUTHORITY"));
                }

                if(!jsonObj.optString("DLNTVLDTODT").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_Ntvldtodt(jsonObj.optString("DLNTVLDTODT"));
                }

                if(!jsonObj.optString("DLTVLDTODT").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_Tvldtodt(jsonObj.optString("DLTVLDTODT"));
                }

                if(!jsonObj.optString("DLTELE").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_tele(jsonObj.optString("DLTELE"));
                }

                dlSearchDeatilsList.add(dlSearchDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        Intent intent=new Intent(getActivity(), DLSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno", pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putExtra("DL_SEARCH_LIST", (Serializable) dlSearchDeatilsList);
        startActivity(intent);

    }

    private void setAgeSpinnerData(){

        ageAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Age_array, R.layout.simple_spinner_item);
        ageAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_dl_holder_age.setAdapter(ageAdapter);
        ageAdapter.notifyDataSetChanged();

        sp_dl_holder_age.setSelection(0);

    }


    private void radioGroupClickEvents(){

        radio_group_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                // find which radio button is selected
                if(checkedId == R.id.radio_male) {
                    genderValue = "MALE";

                } else if(checkedId == R.id.radio_female) {
                    genderValue = "FEMALE";
                }
            }

        });

    }
}