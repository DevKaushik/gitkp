package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kp.facedetection.adapter.HighCourtCaseSearchAdapter;
import com.kp.facedetection.model.HCourtCaseDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class HighCourtCaseSearchResultActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, Observer {

    private String[] keys;
    private String[] values;
    private String pageno;
    private String totalResult = "";
    private String searchCase = "";
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private List<HCourtCaseDetails> hCourtCaseDetailsList;
    HighCourtCaseSearchAdapter highCourtCaseSearchAdapter;

    private TextView tv_resultCount;
    private ListView lv_highCourtCaseSearchList;
    private Button btnLoadMore;

    private TextView tv_watermark;
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.highcourt_case_search_result_layout);
        ObservableObject.getInstance().addObserver(this);
        initialization();
        clickEvents();

    }

    private void initialization() {

        tv_resultCount = (TextView) findViewById(R.id.tv_resultCount);
        lv_highCourtCaseSearchList = (ListView) findViewById(R.id.lv_highCourtCaseSearchList);

        // LoadMore button
        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        keys = getIntent().getStringArrayExtra("keys");
        values = getIntent().getStringArrayExtra("value");
        pageno = getIntent().getStringExtra("pageno");
        totalResult = getIntent().getStringExtra("totalResult");

        Bundle bundle = getIntent().getExtras();
        hCourtCaseDetailsList = bundle.getParcelableArrayList("COURT_CASE_SEARCH_LIST");

        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }


        highCourtCaseSearchAdapter = new HighCourtCaseSearchAdapter(this,hCourtCaseDetailsList);
        lv_highCourtCaseSearchList.setAdapter(highCourtCaseSearchAdapter);
        highCourtCaseSearchAdapter.notifyDataSetChanged();

        if (Integer.parseInt(totalResult) > 20) {

            // Adding Load More button to listview at bottom
            lv_highCourtCaseSearchList.addFooterView(btnLoadMore);
        }

        lv_highCourtCaseSearchList.setOnItemClickListener(this);


        /*  Set user name as Watermark  */
        tv_watermark = (TextView) findViewById(R.id.tv_watermark);
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-55);

    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
           /* case R.id.menu_send_push:

                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                // Utility.logout(this);
                break;
        }
        return true;
    }

    private void clickEvents() {

        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // Starting a new async task
                fetchCourtCaseSearchResultPagination();
            }
        });

    }



    private void fetchCourtCaseSearchResultPagination() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_COURT_CASE_LIST_SEARCH);
        taskManager.setCourtCaseSearchPagination(true);

        values[12] = (Integer.parseInt(pageno) + 1) + "";

        taskManager.doStartTask(keys, values, true);
    }

    public void parseCourtCaseSearchPagination(String result, String[] keys, String[] values) {

        //System.out.println("parseCourtCaseSearchPagination: " + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("page").toString();
                    totalResult = jObj.opt("count").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseCourtCaseSearchResponse(resultArray);

                } else {
                    showAlertDialog(this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }


            } catch (JSONException e) {

                e.printStackTrace();
                showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }

    private void parseCourtCaseSearchResponse(JSONArray resultArray) {

        for (int i = 0; i < resultArray.length(); i++) {

            JSONObject row = null;

            try {
                row = resultArray.getJSONObject(i);

                HCourtCaseDetails hCourtCaseDetails = new HCourtCaseDetails(Parcel.obtain());

                if(!row.optString("SIDE").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setSideValue(row.optString("SIDE"));

                if(!row.optString("CASEDATE").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setCaseDate(row.optString("CASEDATE"));

                if(!row.optString("COURTNO").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setCourtNo(row.optString("COURTNO"));

                if(!row.optString("JUSTICE1").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setJustice1(row.optString("JUSTICE1"));

                if(!row.optString("JUSTICE2").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setJustice2(row.optString("JUSTICE2"));

                if(!row.optString("JUSTICE3").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setJustice3(row.optString("JUSTICE3"));

                if(!row.optString("CATEGORY").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setCategory(row.optString("CATEGORY"));

                if(!row.optString("CASETYPE").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setCaseType(row.optString("CASETYPE"));

                if(!row.optString("CASENO").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setCaseNo(row.optString("CASENO"));

                if(!row.optString("CASEYEAR").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setCaseYr(row.optString("CASEYEAR"));

                if(!row.optString("PARTY").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setParty(row.optString("PARTY"));

                if(!row.optString("ADDLCASE").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setAddLcase(row.optString("ADDLCASE"));

                if(!row.optString("ADV_PETITIONER").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setAdvPetitioner(row.optString("ADV_PETITIONER"));

                if(!row.optString("ADV_RESPDT").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setAdvResPdt(row.optString("ADV_RESPDT"));

                if(!row.optString("BRIEF_NOTE").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setBriefNote(row.optString("BRIEF_NOTE"));

                if(!row.optString("OUTCOME").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setOutCome(row.optString("OUTCOME"));

                if(!row.optString("NEXTDATE").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setNextDate(row.optString("NEXTDATE"));

                hCourtCaseDetailsList.add(hCourtCaseDetails);


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        int currentPosition = lv_highCourtCaseSearchList.getFirstVisiblePosition();
        int lastVisiblePosition = lv_highCourtCaseSearchList.getLastVisiblePosition();

        // Setting new scroll position
        lv_highCourtCaseSearchList.setSelectionFromTop(currentPosition + 1, 0);

        if((Integer.parseInt(totalResult)-lastVisiblePosition)<20){
            lv_highCourtCaseSearchList.removeFooterView(btnLoadMore);
        }
    }


    private void showAlertDialog(Context context, String title, final String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        passData(position);

    }

    private void passData(int position){

        Intent in = new Intent(HighCourtCaseSearchResultActivity.this, HighCourtCaseSearchDetailsActivity.class);
        in.putExtra("POSITION",position);
        in.putParcelableArrayListExtra("COURT_CASE_SEARCH_RESULT", (ArrayList<? extends Parcelable>) hCourtCaseDetailsList);
        startActivity(in);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }


    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
