package com.kp.facedetection.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 25-07-2016.
 */
public class CaseSearchFIRWarrant implements Serializable {

    private String slNo="";
    private String slDate="";
    private String waType="";
    private String waNo="";
    private String waIssueDate="";
    private String ps="";
    private String caseNo="";
    private String underSections="";
    private String issueCourt="";
    private String psRcevDate="";
    private String officerToServe="";
    private String returnableDateToCourt="";
    private String dtReturnToCourt="";
    private String waCat="";
    private String waSubType="";
    private String waStatus="";
    private String actionDate="";
    private String firYr="";
    private String processNo="";
    private String casePs="";
    private String remarks="";
    private String waYear="";
    private String waSlno="";
    private String psCode="";
    private String psName="";
    private String divCode="";
    private String ocName="";

    private List<FIRWarranteesDetails> firWarranteesDetailsList = new ArrayList<FIRWarranteesDetails>();

    public List<FIRWarranteesDetails> getFirWarranteesDetailsList() {
        return firWarranteesDetailsList;
    }

    public void setFirWarranteesDetailsList(FIRWarranteesDetails obj) {
        firWarranteesDetailsList.add(obj);
    }


    public String getSlNo() {
        return slNo;
    }

    public void setSlNo(String slNo) {
        this.slNo = slNo;
    }

    public String getSlDate() {
        return slDate;
    }

    public void setSlDate(String slDate) {
        this.slDate = slDate;
    }

    public String getWaType() {
        return waType;
    }

    public void setWaType(String waType) {
        this.waType = waType;
    }

    public String getWaNo() {
        return waNo;
    }

    public void setWaNo(String waNo) {
        this.waNo = waNo;
    }

    public String getWaIssueDate() {
        return waIssueDate;
    }

    public void setWaIssueDate(String waIssueDate) {
        this.waIssueDate = waIssueDate;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getUnderSections() {
        return underSections;
    }

    public void setUnderSections(String underSections) {
        this.underSections = underSections;
    }

    public String getIssueCourt() {
        return issueCourt;
    }

    public void setIssueCourt(String issueCourt) {
        this.issueCourt = issueCourt;
    }

    public String getPsRcevDate() {
        return psRcevDate;
    }

    public void setPsRcevDate(String psRcevDate) {
        this.psRcevDate = psRcevDate;
    }

    public String getOfficerToServe() {
        return officerToServe;
    }

    public void setOfficerToServe(String officerToServe) {
        this.officerToServe = officerToServe;
    }

    public String getReturnableDateToCourt() {
        return returnableDateToCourt;
    }

    public void setReturnableDateToCourt(String returnableDateToCourt) {
        this.returnableDateToCourt = returnableDateToCourt;
    }

    public String getDtReturnToCourt() {
        return dtReturnToCourt;
    }

    public void setDtReturnToCourt(String dtReturnToCourt) {
        this.dtReturnToCourt = dtReturnToCourt;
    }

    public String getWaCat() {
        return waCat;
    }

    public void setWaCat(String waCat) {
        this.waCat = waCat;
    }

    public String getWaSubType() {
        return waSubType;
    }

    public void setWaSubType(String waSubType) {
        this.waSubType = waSubType;
    }

    public String getWaStatus() {
        return waStatus;
    }

    public void setWaStatus(String waStatus) {
        this.waStatus = waStatus;
    }

    public String getActionDate() {
        return actionDate;
    }

    public void setActionDate(String actionDate) {
        this.actionDate = actionDate;
    }

    public String getFirYr() {
        return firYr;
    }

    public void setFirYr(String firYr) {
        this.firYr = firYr;
    }

    public String getProcessNo() {
        return processNo;
    }

    public void setProcessNo(String processNo) {
        this.processNo = processNo;
    }

    public String getCasePs() {
        return casePs;
    }

    public void setCasePs(String casePs) {
        this.casePs = casePs;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getWaYear() {
        return waYear;
    }

    public void setWaYear(String waYear) {
        this.waYear = waYear;
    }

    public String getWaSlno() {
        return waSlno;
    }

    public void setWaSlno(String waSlno) {
        this.waSlno = waSlno;
    }

    public String getPsCode() {
        return psCode;
    }

    public void setPsCode(String psCode) {
        this.psCode = psCode;
    }

    public String getPsName() {
        return psName;
    }

    public void setPsName(String psName) {
        this.psName = psName;
    }

    public String getDivCode() {
        return divCode;
    }

    public void setDivCode(String divCode) {
        this.divCode = divCode;
    }

    public String getOcName() {
        return ocName;
    }

    public void setOcName(String ocName) {
        this.ocName = ocName;
    }
}
