package com.kp.facedetection.fragments;

import org.json.JSONObject;

import com.kp.facedetection.BaseActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SendPushActivity extends Fragment implements
		OnClickListener {
    View rootLayout;
	EditText et_from, et_subject, et_details;
	Button btn_send;

	private String userName="";
	private String loginNumber="";
	TextView chargesheetNotificationCount;

    @Nullable
    @SuppressLint("NewApi")
	@Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		//super.onCreate(savedInstanceState);
        rootLayout = View.inflate(getActivity(),
                R.layout.send_push_layout, null);
		//setContentView(R.layout.send_push_layout);
		/*ActionBar actionBar = getActivity().getActionBar();
		actionBar.setLogo(R.drawable.kplogo4);
		actionBar.setDisplayShowHomeEnabled(true);

		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#00004d")));
		getActivity().getActionBar().setIcon(R.drawable.kplogo4);
		getActivity().getActionBar().setTitle("");*/

		userName = Utility.getUserInfo(getActivity()).getName();
		loginNumber = Utility.getUserInfo(getActivity()).getLoginNumber();

		System.out.println("UserName: "+userName);

		ActionBar mActionBar = getActivity().getActionBar();
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#00004d")));
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater mInflater = LayoutInflater.from(getActivity());

		View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
		TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
		TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
		CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
		charseet_due_notification.setOnClickListener(this);
		chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);

		tv_username.setText("Welcome "+userName);
		tv_loginCount.setText("Login Count: "+loginNumber);

		Constants.changefonts(tv_username, getActivity(), "Calibri Bold.ttf");
		Constants.changefonts(tv_loginCount, getActivity(), "Calibri.ttf");

		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);
		
		//KPFaceDetectionApplication.getApplication().addActivityToList(getActivity());
		
		initView(rootLayout);
        return rootLayout;
    }

	private void initView(View rootLayout) {
		et_details = (EditText) rootLayout.findViewById(R.id.et_details);
		et_from = (EditText) rootLayout.findViewById(R.id.et_from);
		et_subject = (EditText) rootLayout.findViewById(R.id.et_subject);
		btn_send = (Button) rootLayout.findViewById(R.id.btn_send);
		Log.e("object", et_details + "**" + et_from + "**" + et_subject + "**"
				+ btn_send);
		btn_send.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_send:
			sendPushTask();
			break;
		case R.id.charseet_due_notification:
				//BaseActivity ba=new BaseActivity();
				//ba.getChargesheetdetailsAsperRole(this);
				break;
		default:
			break;
		}
	}

	private void sendPushTask() {

		if (!Constants.internetOnline(getActivity())) {
			Utility.showAlertDialog(getActivity(), Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
			return;
		}

		TaskManager taskManager = new TaskManager(this);
		taskManager.setMethod(Constants.METHOD_SEND_PUSH);
		taskManager.setSendPush(true);
		// [subject=>
		// 1,description=>'dsadasas',sendername=>'bisu',userid=>1,'devicetype'=>'android']
		String[] keys = { "user_id", "device_type", "sendername", "subject",
				"description" };
		String[] values = { Utility.getUserInfo(getActivity()).getUserId().trim(), "android",
				et_from.getText().toString().trim(),
				et_subject.getText().toString().trim(),
				et_details.getText().toString().trim() };
		taskManager.doStartTask(keys, values, true);
	}

	public void parseNotificationResponse(String response) {
		Log.e("Response", "Response " + response);
		// {"success":true,"message":"Success","result":{"notificationid":34}}
		try {
			JSONObject jObj = new JSONObject(response);
			if (jObj != null && jObj.opt("status").toString().equalsIgnoreCase("1")) {
				et_details.setText("");
				et_from.setText("");
				et_subject.setText("");
				Toast.makeText(getActivity(), R.string.txt_successful_push,
						Toast.LENGTH_LONG).show();
//				notificationDetaileTask(jObj.optJSONObject("result").optString(
//						"notificationid"));
			} else {
				Toast.makeText(getActivity(), "Message failed to deliever!",
						Toast.LENGTH_LONG).show();
			}

		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(getActivity(), "Message failed to deliever!",
					Toast.LENGTH_LONG).show();
		}

	}

	
/*	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getActivity().getMenuInflater();
		inflater.inflate(R.menu.menu_main, menu);
		return super.onCreateOptionsMenu(menu);
	}*/

	/*@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		super.onOptionsItemSelected(item);

		switch (item.getItemId()) {
		case R.id.menu_send_push:
			startActivity(new Intent(this, SendPushActivity.class));
			break;

		case R.id.menu_logout:
			logoutTask();
			break;
		}
		return true;

	}*/
	
	/*@Override
	protected void onDestroy() {
		super.onDestroy();
		KPFaceDetectionApplication.getApplication().removeActivityToList(this);
	}*/
}
