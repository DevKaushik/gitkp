package com.kp.facedetection.fragments;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.adapter.ArrestDetailsListAdapter;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.model.ModifiedArrestDetails;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 14-07-2016.
 */
public class ModifiedArrestFragment extends Fragment implements AdapterView.OnItemClickListener {

    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String FLAG = "FLAG";
    public static final String TRNID = "TRNID";
    public static final String PS_CODE = "PS_CODE";
    public static final String CRIME_NO = "CRIME_NO";
    public static final String CRIME_YR = "CRIME_YR";
    public static final String CASE_NO = "CASE_NO";
    public static final String CASE_YEAR = "CASE_YEAR";
    public static final String PROV_CRM_NO = "PROV_CRM_NO";

    private int mPage;
    private String flag;
    private String crime_no = "";
    private String crime_year = "";
    private String trnid = "";
    private String ps_code = "";
    private String case_no = "";
    private String case_year = "";
    private String prov_crm_no = "";


    private String aliasName = "Alias Name^";
    private String fatherName = "Father's Name^";
    private String age = "Age^";
    private String sex = "Sex^";
    private String arrestedOn = "Arrested On^";
    private String arrestedBy = "Arested By^";
    private String placeOfArrest = "Place Of Arrest^";
    private String underSection = "Under Section^";
    private String categoryOfCrime = "Category Of Crime^";
    private String caseReference = "Case Reference";

    private ListView lv_arrest;
    private TextView tv_SlNo;
    private TextView tv_ArrestName;
    private TextView tv_status;
    private TextView tv_noData;
    private LinearLayout linear_main;

    private View vw_line3,vw_line2;

    View inflatedFIRview;   //view used for popUpWindow
    private PopupWindow popWindow;

    private List<ModifiedArrestDetails> arrestDetailsList;
    ArrestDetailsListAdapter arrestDetailsListAdapter;

    private String userName = "";

    public static ModifiedArrestFragment newInstance(int page, String flag, String trnid, String ps_code, String crime_no, String crime_year, String case_no, String case_yr, String prov_crm_no) {

        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        args.putString(FLAG, flag);
        args.putString(TRNID, trnid);
        args.putString(PS_CODE, ps_code);
        args.putString(CRIME_NO, crime_no);
        args.putString(CRIME_YR, crime_year);
        args.putString(CASE_NO, case_no);
        args.putString(CASE_YEAR, case_yr);
        args.putString(PROV_CRM_NO, prov_crm_no);

        ModifiedArrestFragment fragment = new ModifiedArrestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPage = getArguments().getInt(ARG_PAGE);
        flag = getArguments().getString(FLAG);
        trnid = getArguments().getString(TRNID).replace("TRNID: ", "");
        ps_code = getArguments().getString(PS_CODE).replace("PSCODE: ", "");
        crime_no = getArguments().getString(CRIME_NO).replace("CRIMENO: ", "");
        crime_year = getArguments().getString(CRIME_YR).replace("CRIMEYEAR: ", "");
        case_no = getArguments().getString(CASE_NO).replace("CASENO: ", "");
        case_year = getArguments().getString(CASE_YEAR).replace("CASEYR: ", "");
        prov_crm_no = getArguments().getString(PROV_CRM_NO);

        try {
            userName = Utility.getUserInfo(getActivity()).getName();
        } catch (Exception e) {
            e.printStackTrace();
        }


        System.out.println("--------------------------Arrest-----------------------------");
        System.out.println("Flag: " + flag);
        System.out.println("trnid " + trnid);
        System.out.println("PS Code: " + ps_code);
        System.out.println("Crime No: " + crime_no);
        System.out.println("Crime Yr: " + crime_year);
        System.out.println("--------------------------Arrest-----------------------------");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // View view = inflater.inflate(R.layout.fragment_details, container, false);
        View view = inflater.inflate(R.layout.fragment_fir, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        lv_arrest = (ListView) view.findViewById(R.id.lv_fir);
        tv_SlNo = (TextView) view.findViewById(R.id.tv_SlNo);
        tv_ArrestName = (TextView) view.findViewById(R.id.tv_FirName);
        tv_status = (TextView) view.findViewById(R.id.tv_status);
        tv_noData = (TextView) view.findViewById(R.id.tv_noData);
        linear_main = (LinearLayout) view.findViewById(R.id.linear_main);
        vw_line2=(View) view.findViewById(R.id.vw_line2);
        vw_line3=(View) view.findViewById(R.id.vw_line3);


        linear_main.setVisibility(View.GONE);
        tv_noData.setVisibility(View.GONE);

        vw_line2.setVisibility(View.GONE);
        //vw_line3.setVisibility(View.GONE);
        tv_status.setVisibility(View.GONE);

        tv_SlNo.setText("SL.No");
        tv_ArrestName.setText("Arrest");
        tv_status.setText("Status");

        tv_ArrestName.setGravity(Gravity.CENTER);
        Constants.changefonts(tv_SlNo, getActivity(), "Calibri.ttf");
        Constants.changefonts(tv_ArrestName, getActivity(), "Calibri Bold.ttf");
        Constants.changefonts(tv_status, getActivity(), "Calibri.ttf");
        Constants.changefonts(tv_noData, getActivity(), "Calibri.ttf");

        getArrestData();
    }

    private void getArrestData() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_MODIFIED_ARREST_DETAILS);
        taskManager.setCriminalModifiedArrestDetails(true);

        if (flag.equalsIgnoreCase("OA")) {

            String[] keys = {"flag", "trnid", "pscode"};
            String[] values = {flag.trim(), trnid.trim(), ps_code.trim()};
            //String[] values = {"OA", "20707", "BHL"};
            taskManager.doStartTask(keys, values, true );

        } else {

            if(case_year.equalsIgnoreCase("")){
                if(!crime_year.equalsIgnoreCase("")) {
                    case_year =crime_year;
                }
            }


            String[] keys = {"flag", "crimeyear", "crimeno", "pscode", "caseyear", "caseno", "prov_crm_no"};
            String[] values = {"OF", crime_year, crime_no, ps_code, case_year, case_no, prov_crm_no};

            taskManager.doStartTask(keys, values, true );

        }


    }


    public void parseCriminalModifiedArrestDetailsResponse(String response) {

       // System.out.println("parseCriminalModifiedArrestDetailsResponse: " + response);

        if (response != null && !response.equals("")) {

            try {
                JSONObject jObj = new JSONObject(response);
                if (jObj.optString("status").equalsIgnoreCase("1")) {

                    if (jObj.optString("message").contains("No arrest details found for this criminal")) {

                        linear_main.setVisibility(View.GONE);
                        tv_noData.setVisibility(View.VISIBLE);
                        tv_noData.setText("Sorry, no arrest details found");

                    } else {

                        linear_main.setVisibility(View.VISIBLE);
                        tv_noData.setVisibility(View.GONE);

                        JSONArray result_jsonArray = jObj.getJSONArray("result");
                        if (result_jsonArray.length() > 0) {

                            if(flag.equalsIgnoreCase("OC")){
                                parseJSONArrayForArrestOC(result_jsonArray);
                            }
                            else{
                                parseJSONArrayForArrestExceptOC(result_jsonArray);
                            }


                        } else {
                            System.out.println("No data found in result_Jsonarray");
                        }

                    }


                } else {
                    linear_main.setVisibility(View.GONE);
                    tv_noData.setVisibility(View.VISIBLE);
                    tv_noData.setText("Sorry, no arrest details found");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                linear_main.setVisibility(View.GONE);
                tv_noData.setVisibility(View.VISIBLE);
                tv_noData.setText("Sorry, no arrest details found");

            }
        } else {
            Utility.showToast(getActivity(), "Some error has been encountered", "long");

        }

    }

    private void parseJSONArrayForArrestExceptOC(JSONArray result_array) {

        ModifiedArrestDetails modifiedArrestDetails;
        arrestDetailsList = new ArrayList<ModifiedArrestDetails>();

        for (int i = 0; i < result_array.length(); i++) {

            String aliasName = "";
            String fatherOrHusbandName = "";
            String age = "";
            String sex = "";
            String arrestedOn = "";
            String arrestedBy = "";
            String placeOfArrest = "";
            String underSection = "";
            String categoryOfCrime = "";
            String caseReference = "";
            String psName = "";
            String caseYear = "";
            String caseDate = "";

            modifiedArrestDetails = new ModifiedArrestDetails();

            try {
                JSONObject obj = result_array.getJSONObject(i);

                if (obj.optString("ALIASNAME") != null && !obj.optString("ALIASNAME").equalsIgnoreCase("") && !obj.optString("ALIASNAME").equalsIgnoreCase("null")) {
                    aliasName = obj.optString("ALIASNAME");
                }
                if (obj.optString("FATHER_HUSBAND") != null && !obj.optString("FATHER_HUSBAND").equalsIgnoreCase("") && !obj.optString("FATHER_HUSBAND").equalsIgnoreCase("null")) {
                    fatherOrHusbandName = obj.optString("FATHER_HUSBAND");
                }
                if (obj.optString("AGE") != null && !obj.optString("AGE").equalsIgnoreCase("") && !obj.optString("AGE").equalsIgnoreCase("null")) {
                    age = obj.optString("AGE");
                }
                if (obj.optString("SEX") != null && !obj.optString("SEX").equalsIgnoreCase("") && !obj.optString("SEX").equalsIgnoreCase("null")) {
                    sex = obj.optString("SEX");
                }
                if (obj.optString("ARREST_DATE") != null && !obj.optString("ARREST_DATE").equalsIgnoreCase("") && !obj.optString("ARREST_DATE").equalsIgnoreCase("null")) {
                    arrestedOn = obj.optString("ARREST_DATE");
                }
                if (obj.optString("OCNAME") != null && !obj.optString("OCNAME").equalsIgnoreCase("") && !obj.optString("OCNAME").equalsIgnoreCase("null")) {
                    arrestedBy = obj.optString("OCNAME");
                }
                if (obj.optString("PLACE_ARREST") != null && !obj.optString("PLACE_ARREST").equalsIgnoreCase("") && !obj.optString("PLACE_ARREST").equalsIgnoreCase("null")) {
                    placeOfArrest = obj.optString("PLACE_ARREST");
                }
                if (obj.optString("UNDER_SECTION") != null && !obj.optString("UNDER_SECTION").equalsIgnoreCase("") && !obj.optString("UNDER_SECTION").equalsIgnoreCase("null")) {
                    underSection = obj.optString("UNDER_SECTION");
                }
                if (obj.optString("CASEREF") != null && !obj.optString("CASEREF").equalsIgnoreCase("") && !obj.optString("CASEREF").equalsIgnoreCase("null")) {
                    caseReference = obj.optString("CASEREF");
                }
                if (obj.optString("PSNAME") != null && !obj.optString("PSNAME").equalsIgnoreCase("") && !obj.optString("PSNAME").equalsIgnoreCase("null")) {
                    psName = obj.optString("PSNAME");
                }
                if (obj.optString("ARRAGAINST") != null && !obj.optString("ARRAGAINST").equalsIgnoreCase("") && !obj.optString("ARRAGAINST").equalsIgnoreCase("null")) {
                    categoryOfCrime = obj.optString("ARRAGAINST");
                }
                if (obj.optString("CASE_YR") != null && !obj.optString("CASE_YR").equalsIgnoreCase("") && !obj.optString("CASE_YR").equalsIgnoreCase("null")) {
                    caseYear = obj.optString("CASE_YR");
                }
                if (obj.optString("CASEDATE") != null && !obj.optString("CASEDATE").equalsIgnoreCase("") && !obj.optString("CASEDATE").equalsIgnoreCase("null")) {
                    caseDate = obj.optString("CASEDATE");
                }


                modifiedArrestDetails.setAliasName(aliasName);
                modifiedArrestDetails.setFatherOrHusbandName(fatherOrHusbandName);
                modifiedArrestDetails.setAge(age);
                modifiedArrestDetails.setArrestedBy(arrestedBy);
                modifiedArrestDetails.setArrestedOn(arrestedOn);
                modifiedArrestDetails.setCaseReference(caseReference);
                modifiedArrestDetails.setPlaceOfArrest(placeOfArrest);
                modifiedArrestDetails.setPsName(psName);
                modifiedArrestDetails.setUnderSection(underSection);
                modifiedArrestDetails.setSex(sex);
                modifiedArrestDetails.setCategoryOfCrime(categoryOfCrime);
                modifiedArrestDetails.setCaseYear(caseYear);
                modifiedArrestDetails.setCaseDate(caseDate);

                arrestDetailsList.add(modifiedArrestDetails);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        arrestDetailsListAdapter = new ArrestDetailsListAdapter(getActivity(), arrestDetailsList);
        lv_arrest.setAdapter(arrestDetailsListAdapter);
        lv_arrest.setOnItemClickListener(this);
    }



    private void parseJSONArrayForArrestOC(JSONArray result_array) {

        ModifiedArrestDetails modifiedArrestDetails;
        arrestDetailsList = new ArrayList<ModifiedArrestDetails>();

        for (int i = 0; i < result_array.length(); i++) {

            String aliasName = "";
            String fatherOrHusbandName = "";
            String age = "";
            String sex = "";
            String arrestedOn = "";
            String arrestedBy = "";
            String placeOfArrest = "";
            String underSection = "";
            String categoryOfCrime = "";
            String caseReference = "";
            String psName = "";
            String caseYear = "";
            String caseDate = "";

            modifiedArrestDetails = new ModifiedArrestDetails();

            try {
                JSONObject obj = result_array.getJSONObject(i);

                if (obj.optString("ANAME1") != null && !obj.optString("ANAME1").equalsIgnoreCase("") && !obj.optString("ANAME1").equalsIgnoreCase("null")) {
                    aliasName = obj.optString("ANAME1");
                }
                if (obj.optString("ANAME2") != null && !obj.optString("ANAME2").equalsIgnoreCase("") && !obj.optString("ANAME2").equalsIgnoreCase("null")) {
                    aliasName = aliasName + "/"+obj.optString("ANAME2");
                }
                if (obj.optString("ANAME3") != null && !obj.optString("ANAME3").equalsIgnoreCase("") && !obj.optString("ANAME3").equalsIgnoreCase("null")) {
                    aliasName = aliasName + "/"+obj.optString("ANAME3");
                }
                if (obj.optString("ANAME4") != null && !obj.optString("ANAME4").equalsIgnoreCase("") && !obj.optString("ANAME4").equalsIgnoreCase("null")) {
                    aliasName = aliasName + "/"+obj.optString("ANAME2");
                }



                if (obj.optString("F_NAME") != null && !obj.optString("F_NAME").equalsIgnoreCase("") && !obj.optString("F_NAME").equalsIgnoreCase("null")) {
                    fatherOrHusbandName = obj.optString("F_NAME");
                }
                if (obj.optString("AGE") != null && !obj.optString("AGE").equalsIgnoreCase("") && !obj.optString("AGE").equalsIgnoreCase("null")) {
                    age = obj.optString("AGE");
                }
                if (obj.optString("SEX") != null && !obj.optString("SEX").equalsIgnoreCase("") && !obj.optString("SEX").equalsIgnoreCase("null")) {
                    sex = obj.optString("SEX");
                }
                if (obj.optString("ARRESTDATE") != null && !obj.optString("ARRESTDATE").equalsIgnoreCase("") && !obj.optString("ARRESTDATE").equalsIgnoreCase("null")) {
                    arrestedOn = obj.optString("ARRESTDATE");
                }
                if (obj.optString("IO") != null && !obj.optString("IO").equalsIgnoreCase("") && !obj.optString("IO").equalsIgnoreCase("null")) {
                    arrestedBy = obj.optString("IO");
                }
                if (obj.optString("PSROAD_PSPLACE") != null && !obj.optString("PSROAD_PSPLACE").equalsIgnoreCase("") && !obj.optString("PSROAD_PSPLACE").equalsIgnoreCase("null")) {
                    placeOfArrest = obj.optString("PSROAD_PSPLACE");
                }
                if (obj.optString("SEC11") != null && !obj.optString("SEC11").equalsIgnoreCase("") && !obj.optString("SEC11").equalsIgnoreCase("null")) {
                    underSection = obj.optString("SEC11");
                }
                if (obj.optString("CASEREF") != null && !obj.optString("CASEREF").equalsIgnoreCase("") && !obj.optString("CASEREF").equalsIgnoreCase("null")) {
                    caseReference = obj.optString("CASEREF");
                }
                if (obj.optString("PSPS_OUT") != null && !obj.optString("PSPS_OUT").equalsIgnoreCase("") && !obj.optString("PSPS_OUT").equalsIgnoreCase("null")) {
                    psName = obj.optString("PSPS_OUT")+" PS ";
                }
                if (obj.optString("ARRAGAINST") != null && !obj.optString("ARRAGAINST").equalsIgnoreCase("") && !obj.optString("ARRAGAINST").equalsIgnoreCase("null")) {
                    categoryOfCrime = obj.optString("ARRAGAINST");
                }
                if (obj.optString("CASE_YR") != null && !obj.optString("CASE_YR").equalsIgnoreCase("") && !obj.optString("CASE_YR").equalsIgnoreCase("null")) {
                    caseYear = obj.optString("CASE_YR");
                }
                if (obj.optString("CASEDATE") != null && !obj.optString("CASEDATE").equalsIgnoreCase("") && !obj.optString("CASEDATE").equalsIgnoreCase("null")) {
                    caseDate = obj.optString("CASEDATE");
                }


                modifiedArrestDetails.setAliasName(aliasName);
                modifiedArrestDetails.setFatherOrHusbandName(fatherOrHusbandName);
                modifiedArrestDetails.setAge(age);
                modifiedArrestDetails.setArrestedBy(arrestedBy);
                modifiedArrestDetails.setArrestedOn(arrestedOn);
                modifiedArrestDetails.setCaseReference(caseReference);
                modifiedArrestDetails.setPlaceOfArrest(placeOfArrest);
                modifiedArrestDetails.setPsName(psName);
                modifiedArrestDetails.setUnderSection(underSection);
                modifiedArrestDetails.setSex(sex);
                modifiedArrestDetails.setCategoryOfCrime(categoryOfCrime);
                modifiedArrestDetails.setCaseDate(caseDate);
                modifiedArrestDetails.setCaseYear(caseYear);
                arrestDetailsList.add(modifiedArrestDetails);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        arrestDetailsListAdapter = new ArrestDetailsListAdapter(getActivity(), arrestDetailsList);
        lv_arrest.setAdapter(arrestDetailsListAdapter);
        lv_arrest.setOnItemClickListener(this);
    }


    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

      //  String arrestName = "Arrested on " + arrestDetailsList.get(position).getArrestedOn() + " by " + arrestDetailsList.get(position).getPsName() + " (case no " + arrestDetailsList.get(position).getCaseReference()+")";

        TextView tv_FirName = (TextView) view.findViewById(R.id.tv_FirName);

        String arrestName = tv_FirName.getText().toString().trim();
        popUpWindowForArrestListItem(view, position, arrestName);
    }

    private void popUpWindowForArrestListItem(View view, int position, String itemName) {

        String aliasName = arrestDetailsList.get(position).getAliasName();
        String fatherOrHusbandName = arrestDetailsList.get(position).getFatherOrHusbandName();
        String age = arrestDetailsList.get(position).getAge();
        String sex = arrestDetailsList.get(position).getSex();
        String arrestedOn = arrestDetailsList.get(position).getArrestedOn();
        String arrestedBy = arrestDetailsList.get(position).getArrestedBy();
        String placeOfArrest = arrestDetailsList.get(position).getPlaceOfArrest();
        String underSection = arrestDetailsList.get(position).getUnderSection();
//        String caseReference = arrestDetailsList.get(position).getCaseReference();
//        Case/GDE No.xxx dt.dd/mm/yyyy  : Format changed on 19-07-17
        String caseReference = "";
        caseReference = "Case/GDE No."+arrestDetailsList.get(position).getCaseReference()+"dt."+arrestDetailsList.get(position).getArrestedOn();
//        psNamme + C/No. + caseReference + of + caseYear(Not here) + u/s + underSection + IPC
        /*if (!arrestDetailsList.get(position).getCaseReference().equalsIgnoreCase("")) {
            caseReference = arrestDetailsList.get(position).getPsName() +
                    " C/No. " + arrestDetailsList.get(position).getCaseReference() +
                    " of " + arrestDetailsList.get(position).getCaseYear() +
                    " u/s " + arrestDetailsList.get(position).getUnderSection() + " IPC";
        }*/
        String category_of_crime = arrestDetailsList.get(position).getCategoryOfCrime();


        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popUpWindow layout
        inflatedFIRview = layoutInflater.inflate(R.layout.layout_arrest_details_popup, null, false);

        TextView tv_itemName = (TextView) inflatedFIRview.findViewById(R.id.tv_itemName);
        TextView tv_aliasNameValue = (TextView) inflatedFIRview.findViewById(R.id.tv_aliasNameValue);
        TextView tv_fNameValue = (TextView) inflatedFIRview.findViewById(R.id.tv_fNameValue);
        TextView tv_ageValue = (TextView) inflatedFIRview.findViewById(R.id.tv_ageValue);
        TextView tv_sexValue = (TextView) inflatedFIRview.findViewById(R.id.tv_sexValue);
        TextView tv_arrestDateValue = (TextView) inflatedFIRview.findViewById(R.id.tv_arrestDateValue);
        TextView tv_arrestedByValue = (TextView) inflatedFIRview.findViewById(R.id.tv_arrestedByValue);
        TextView tv_underSectionValue = (TextView) inflatedFIRview.findViewById(R.id.tv_underSectionValue);
        TextView tv_placeValue = (TextView) inflatedFIRview.findViewById(R.id.tv_placeValue);
        TextView tv_caseRefValue = (TextView) inflatedFIRview.findViewById(R.id.tv_caseRefValue);
        TextView tv_crimeCategoryValue = (TextView) inflatedFIRview.findViewById(R.id.tv_crimeCategoryValue);

        TextView tv_watermark = (TextView)inflatedFIRview.findViewById(R.id.tv_watermark);

         /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);

        Constants.changefonts(tv_itemName, getContext(), "Calibri Bold.ttf");


        tv_itemName.setText(itemName);
        tv_aliasNameValue.setText(aliasName);
        tv_fNameValue.setText(fatherOrHusbandName);
        tv_ageValue.setText(age);
        tv_sexValue.setText(sex);
        tv_arrestDateValue.setText(arrestedOn);
        tv_arrestedByValue.setText(arrestedBy);
        tv_underSectionValue.setText(underSection);
        tv_caseRefValue.setText(caseReference);
        tv_placeValue.setText(placeOfArrest);
        tv_crimeCategoryValue.setText(category_of_crime);

        openPopupWindow();


    }

    private void openPopupWindow(){

        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // set height depends on the device size
        popWindow = new PopupWindow(inflatedFIRview, width,height-50, true );

        popWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindow.setAnimationStyle(R.style.PopupAnimation);

        //to generate popUpWindow at bottom of ActionBar
        //popWindow.showAsDropDown(getActivity().getActionBar().getCustomView(), 0, 20);
        popWindow.showAtLocation(getActivity().getActionBar().getCustomView(),Gravity.CENTER,0,20);

        popWindow.setOutsideTouchable(true);
        popWindow.setFocusable(true);
        popWindow.getContentView().setFocusableInTouchMode(true);
        popWindow.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    System.out.println("back pressed while opened popup");
                    if(popWindow!=null && popWindow.isShowing()){
                        popWindow.dismiss();
                    }

                    return true;
                }
                return false;
            }
        });

    }




}

