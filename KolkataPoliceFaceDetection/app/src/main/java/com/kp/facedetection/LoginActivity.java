package com.kp.facedetection;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kp.facedetection.asynctasks.AsynctaskForSendSMS;
import com.kp.facedetection.interfaces.OnSendSMSResult;
import com.kp.facedetection.model.UserInfo;
import com.kp.facedetection.receiver.SessionCheckReceiver;
import com.kp.facedetection.services.AppInfoService;
import com.kp.facedetection.services.ChargesheetDueNotificationService;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.FingerprintHandler;
import com.kp.facedetection.FingerprintHandler_login;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.utils.SessionManagement;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Iterator;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.net.ssl.HttpsURLConnection;

import static javax.crypto.Cipher.getInstance;

/**
 * Created by DAT-Asset-40 on 05-12-2015.
 */
public class LoginActivity extends Activity implements OnClickListener, OnSendSMSResult {
    EditText et_userid, et_password;
    Button btn_login, btn_send, btn_cancel;
    TextView tv_forget,desc;
    RelativeLayout rl_forget,rl_password;
    EditText et_email;
    private ImageView iv_datSiteLaunch;
    SessionManagement sessionManagement;
    private String app_version = "";
    TextView tv_appVersion;

    private Tracker mTracker;

    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;

    public static final String PREFS_NAME = "MyPrefsFile1";
    private CheckBox dontShowAgain;
    private SharedPreferences settings;
    Intent intent;
    String user_id = "";
    FingerprintManagerCompat fingerprintManager = null;
    KeyguardManager keyguardManager = null;
    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "CRS_LOGIN";
    private Cipher cipher;
    private String isFingerPrint = "0";
    private ImageView icon;
    String devicetoken="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        if (Build.VERSION.SDK_INT > 22) {

            try {

                keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
                fingerprintManager = FingerprintManagerCompat.from(getApplicationContext());

                if (!fingerprintManager.isHardwareDetected()) {
                    setContentView(R.layout.login_layout);
                    initView();
                    isFingerPrint = "0";
                }
                else{
                    setContentView(R.layout.login_layout_fingerprint);
                 //   isFingerPrint = "1";
                    initView();
                    icon = (ImageView)findViewById(R.id.icon);
                    icon.setOnClickListener(this);

                }
            } catch (Exception e) {
                //showAlertDialog(this, "Error!!", "App does not support this device Fingerprint Sensor checking.", false);
            }
        }
        else{
            setContentView(R.layout.login_layout);
            initView();
            isFingerPrint = "0";
        }
    }

    @SuppressLint("NewApi")
    private void fingerprintAuthCall(){

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this,"Fingerprint authentication permission not enabled", Toast.LENGTH_LONG).show();
        } else {
            // Check whether at least one fingerprint is registered
            if (!fingerprintManager.hasEnrolledFingerprints()) {
                Toast.makeText(this,"Register at least one fingerprint in Settings",Toast.LENGTH_LONG).show();
            } else {
                // Checks whether lock screen security is enabled or not
                if (!keyguardManager.isKeyguardSecure()) {
                    Toast.makeText(this,"Lock screen security not enabled in Settings",Toast.LENGTH_LONG).show();
                } else {
                    generateKey();

                    if (cipherInit()) {
                        FingerprintManagerCompat.CryptoObject cryptoObject = new FingerprintManagerCompat.CryptoObject(cipher);
                        FingerprintHandler_login helper = new FingerprintHandler_login(this);
                        helper.startAuth(fingerprintManager, cryptoObject);
                    }

                }
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }


        javax.crypto.KeyGenerator keyGenerator;
        try {
            keyGenerator = javax.crypto.KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }


        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }


        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (Exception e) {    //KeyPermanentlyInvalidatedException e
            return false;
        } /*catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }*/
    }

    private void initView() {

        alarmMgr = (AlarmManager) (this.getSystemService(Context.ALARM_SERVICE));

        sessionManagement = new SessionManagement(this);

        System.out.println("Store Email Status: " + sessionManagement.isStoredEmail());

        System.out.println("IMEI: " + Constants.device_IMEI_No);

        et_userid = (EditText) findViewById(R.id.et_userid);
        et_password = (EditText) findViewById(R.id.et_password);
        desc = (TextView) findViewById(R.id.desc);
        btn_login = (Button) findViewById(R.id.btn_login);

        tv_appVersion = (TextView) findViewById(R.id.tv_appVersion);
        tv_forget = (TextView) findViewById(R.id.tv_forget);
        rl_forget = (RelativeLayout) findViewById(R.id.rl_forget);
        rl_password = (RelativeLayout) findViewById(R.id.rl_password);
        et_email = (EditText) findViewById(R.id.et_email);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_send = (Button) findViewById(R.id.btn_send);

        iv_datSiteLaunch = (ImageView) findViewById(R.id.iv_datSiteLaunch);

        Constants.loginButtonEffect(btn_login);

        rl_forget.setVisibility(View.GONE);
        btn_login.setOnClickListener(this);

        tv_forget.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        btn_send.setOnClickListener(this);

        iv_datSiteLaunch.setOnClickListener(this);

        if (sessionManagement.isStoredEmail()) {

            // get user data from session
            HashMap<String, String> user = sessionManagement.getUserEmail();
            // email
            String email = user.get(sessionManagement.KEY_EMAIL);
            et_userid.setText(email);
            et_userid.setSelection(et_userid.getText().length());
        }

        //Analytics Integration
        // Obtain the shared Tracker instance.
        KPFaceDetectionApplication application = (KPFaceDetectionApplication) getApplication();
        mTracker = application.getDefaultTracker();

        Utility.changefonts(tv_appVersion, this, "Calibri.ttf");
        app_version = Utility.getAppVersion(LoginActivity.this);
        tv_appVersion.setText("ver. " + app_version);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:

                try {
                    String userId = et_userid.getText().toString().trim();
                    String password = et_password.getText().toString().trim();
                    if (userId.equals("")) {
                        //showMesage("Please enter a valid Username", et_userid);
                        showAlertDialog(this, " Error ", "Please enter a valid username ", false);
                    } else if (password.equals("")) {
                        // showMesage("Please enter a valid Password", et_password);
                        showAlertDialog(this, " Error ", "Please enter a valid password ", false);
                    } else {
                        View view = this.getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        disableTemporaryButtonClick();
                        C2dmRegistration();
//				logInTask();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


                // For Login with other device && comment BaseActivity 133
               /* startActivity(new Intent(LoginActivity.this,
                        DashboardActivity.class));
                finish();*/

                break;
            case R.id.tv_forget:
                /*tv_forget.setVisibility(View.GONE);
                rl_forget.setVisibility(View.VISIBLE);*/

                // temporarily commented out 'forgot pwd' block  by 27/04/2017
                //showAlertForProceed();
                break;

            case R.id.iv_datSiteLaunch:

                websiteLauch();
                break;

            case R.id.icon:
                rl_password.setVisibility(View.INVISIBLE);
                icon.setVisibility(View.INVISIBLE);
                btn_login.setVisibility(View.INVISIBLE);
                desc.setVisibility(View.VISIBLE);
                Constants.changefonts(desc, this,"Calibri Bold.ttf");
                //desc.setPadding(0,0,0,40);
                fingerprintAuthCall();

            /*case R.id.btn_cancel:
                et_email.setText("");
                rl_forget.setVisibility(View.GONE);
                tv_forget.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_send:
                String email = et_email.getText().toString().trim();
                if (!email.equals("") && Utility.isValidEmailAddress(email)) {
                    //forgetPasspordTask();
                    userForgotPasswordTask();
                } else {
                   // showMesage("Please enter a valid email ", et_email);
                    showAlertDialog(this," Error ","Please enter a valid email ",false);
                }
                break;*/
            default:
                break;
        }
    }


    private void websiteLauch() {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_BROWSABLE);
        intent.setData(Uri.parse("http://www.digitalaptech.com/"));
        startActivity(intent);
    }

    private void showMesage(String msg, View view) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        view.requestFocus();
    }

    public void C2dmRegistration() {

        if (!Constants.internetOnline(this)) {
            Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        logInTask2();
      /*  String registerationID = GCMRegistrar.getRegistrationId(this);
        //Log.e("Device Token ", "Token " + registerationID);
        Utility.setLogMessage("Device Token ", "Token " + registerationID);

        if (registerationID.equals("")) {
            registerOnGCM();
        } else if (!GCMRegistrar.isRegisteredOnServer(this)) {
            GCMRegistrar.unregister(this);
            registerOnGCM();
        } else {
            //logInTask();
            logInTask2();
        }*/
        // pushFragments(new MainMenuFragment(), true, true, false);
    }

    private void registerOnGCM() {
        // GCMRegistrar.setRegisterOnServerLifespan(this, 1 * 60 * 1000);//
        // Expiery
        // time
        // is 1
        // minutes
        GCMRegistrar.register(this, GCMIntentService.SENDER_ID);
        GCMRegistrar.setRegisteredOnServer(this, true);
        new GCMRegistrationTask().execute();
    }

    class GCMRegistrationTask extends AsyncTask<Void, Void, Void> {

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                dialog = new ProgressDialog(LoginActivity.this);
                dialog.setMessage("Please wait...");
                dialog.setCancelable(false);
                dialog.show();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            while (("").equalsIgnoreCase(GCMRegistrar
                    .getRegistrationId(LoginActivity.this))) {
            }
           /* Log.e("Token2",
                    "Token "
                            + GCMRegistrar.getRegistrationId(LoginActivity.this));*/
            Utility.setLogMessage("Token2", "Token " + GCMRegistrar.getRegistrationId(LoginActivity.this));
            Constants.device_token = GCMRegistrar.getRegistrationId(LoginActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
                logInTask2();
            }

        }
    }

    private void logInTask() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_AUTHENTICATE);
        taskManager.setLogin(true);
//		String devicetoken = PushUtility.fetchC2DMToken(this);
      //  String devicetoken = GCMRegistrar.getRegistrationId(this);
        devicetoken= FirebaseInstanceId.getInstance().getToken();
        String[] keys = {"user_email", "user_password", "device_type", "device_tokenid"};
        String[] values = {et_userid.getText().toString().trim(),
                et_password.getText().toString().trim(), "android", devicetoken};
        taskManager.doStartTask(keys, values, true);

    }

    private void logInTask2() {
         Log.e("DAPL","logInTask2 called.........");
        String[] values;
        String[] keys;
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_AUTHENTICATE2);
        taskManager.setLogin(true);
//		String devicetoken = PushUtility.fetchC2DMToken(this);
      // String devicetoken = GCMRegistrar.getRegistrationId(this);
        if(FirebaseInstanceId.getInstance().getToken()!=null) {
            devicetoken = FirebaseInstanceId.getInstance().getToken();
        }
        else{
            devicetoken=Utility.getFCMToken(this);
        }
        if(isFingerPrint.equalsIgnoreCase("1")) {
            keys = new String[]{"user_email", "device_type", "device_tokenid", "imeino", "is_fingerprint"};//GCM TOKEN ->APA91bHCWeqanK8w2wFL0iW5qpkKSi4jP00VlNayo5dzUuD8WFF8JYOHhcC4Qz-llqWeERpe2rjqaE_6d0XddExLFYpgmxZyjcSzQYm5_OWJILWFsdoCWoB4iirLCqQGq2TxghJIx2D4
            values = new String[]{et_userid.getText().toString().trim(), "android", devicetoken, "911518650997488", "1"};
           /* values = new String[]{et_userid.getText().toString().trim(),"android",devicetoken,Constants.device_IMEI_No, "1"};*/
        }

        else{
            keys = new String[]{"user_email", "user_password", "device_type", "device_tokenid", "imeino", "is_fingerprint"};
            values = new String[]{et_userid.getText().toString().trim(), et_password.getText().toString().trim(), "android",devicetoken , "911518650997488", "0"};
           /* values = new String[]{et_userid.getText().toString().trim(), et_password.getText().toString(), "android", devicetoken, Constants.device_IMEI_No, "0"};*/


        }

        taskManager.doStartTask(keys, values, true);

    }

    private void forgetPasspordTask() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_FORGET_PASSWORD);
        taskManager.setForgetPassword(true);
        String[] keys = {"email"};
        String[] values = {et_email.getText().toString().trim(),
                et_password.getText().toString().trim()};
        taskManager.doStartTask(keys, values, true);

    }

    public void parseLoginResponse(String jsonStr) {

        if (jsonStr != null && !jsonStr.equals("")) {
            try {
                JSONObject jObj = new JSONObject(jsonStr);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {
                    JSONObject jsonObj = jObj.optJSONObject("result");
                    //if (jArray != null && jArray.length() > 0) {
                    UserInfo user = new UserInfo();
                    //JSONObject jsonObj = jArray.getJSONObject(0);
                    if (jsonObj.optString("USER_NAME") != null && !jsonObj.optString("USER_NAME").equalsIgnoreCase("") && !jsonObj.optString("USER_NAME").equalsIgnoreCase("null"))
                        user.setName(jsonObj.optString("USER_NAME"));
                    else
                        user.setName("");
                    if (jsonObj.optString("USER_ID") != null && !jsonObj.optString("USER_ID").equalsIgnoreCase("") && !jsonObj.optString("USER_ID").equalsIgnoreCase("null")) {
                        user.setUserId(jsonObj.optString("USER_ID"));//"2142/350"//
                    } else {
                        user.setUserId("");
                        Utility.showToast(this, "User Id not found", "long");
                    }

                    if (jsonObj.optString("USER_PHONE_NO") != null && !jsonObj.optString("USER_PHONE_NO").equalsIgnoreCase("") && !jsonObj.optString("USER_PHONE_NO").equalsIgnoreCase("null"))
                        user.setUserPhoneNo(jsonObj.optString("USER_PHONE_NO"));
                    else
                        user.setUserPhoneNo("");

                    if (jsonObj.optString("USER_EMAIL") != null && !jsonObj.optString("USER_EMAIL").equalsIgnoreCase("") && !jsonObj.optString("USER_EMAIL").equalsIgnoreCase("null"))
                        user.setUserEmail(jsonObj.optString("USER_EMAIL"));
                    else
                        user.setUserEmail("");

                    if (jsonObj.optString("USER_STATUS") != null && !jsonObj.optString("USER_STATUS").equalsIgnoreCase("") && !jsonObj.optString("USER_STATUS").equalsIgnoreCase("null"))
                        user.setUserStatus(jsonObj.optString("USER_STATUS"));
                    else
                        user.setUserStatus("");
                    if (jsonObj.optString("USER_RANK") != null && !jsonObj.optString("USER_RANK").equalsIgnoreCase("") && !jsonObj.optString("USER_RANK").equalsIgnoreCase("null"))
                        user.setUserRank(jsonObj.optString("USER_RANK"));//jsonObj.optString("USER_RANK")
                    else
                        user.setUserRank("");
                    if (jsonObj.optString("USER_DIVISIONCODE") != null && !jsonObj.optString("USER_DIVISIONCODE").equalsIgnoreCase("") && !jsonObj.optString("USER_DIVISIONCODE").equalsIgnoreCase("null"))
                        user.setUserDivisionCode(jsonObj.optString("USER_DIVISIONCODE"));
                    else
                        user.setUserDivisionCode("");
                    if (jsonObj.optString("USER_PSCODE") != null && !jsonObj.optString("USER_PSCODE").equalsIgnoreCase("") && !jsonObj.optString("USER_PSCODE").equalsIgnoreCase("null"))
                        user.setUserPscode(jsonObj.optString("USER_PSCODE"));
                    else
                        user.setUserPscode("");
                    if (jsonObj.optString("USER_CREATEDATE") != null && !jsonObj.optString("USER_CREATEDATE").equalsIgnoreCase("") && !jsonObj.optString("USER_CREATEDATE").equalsIgnoreCase("null"))
                        user.setUserCreateDate(jsonObj.optString("USER_CREATEDATE"));
                    else
                        user.setUserCreateDate("");

                    if (jsonObj.optString("USER_LASTLOGIN") != null && !jsonObj.optString("USER_LASTLOGIN").equalsIgnoreCase("") && !jsonObj.optString("USER_LASTLOGIN").equalsIgnoreCase("null"))
                        user.setUserLastLogin(jsonObj.optString("USER_LASTLOGIN"));
                    else
                        user.setUserLastLogin("");

                    if (jsonObj.optString("USER_MACID") != null && !jsonObj.optString("USER_MACID").equalsIgnoreCase("") && !jsonObj.optString("USER_MACID").equalsIgnoreCase("null"))
                        user.setUserMacId(jsonObj.optString("USER_MACID"));
                    else
                        user.setUserMacId("");
                    // user.setUserBatchNo(jsonObj.optString("batch_no"));
                    if (jsonObj.optString("USER_PASSWORD") != null && !jsonObj.optString("USER_PASSWORD").equalsIgnoreCase("") && !jsonObj.optString("USER_PASSWORD").equalsIgnoreCase("null"))
                        user.setPassword(jsonObj.optString("USER_PASSWORD"));
                    else
                        user.setPassword("");

                    if (jsonObj.optString("USER_PASSWDSTATUS") != null && !jsonObj.optString("USER_PASSWDSTATUS").equalsIgnoreCase("") && !jsonObj.optString("USER_PASSWDSTATUS").equalsIgnoreCase("null"))
                        user.setPasswordStatus(jsonObj.optString("USER_PASSWDSTATUS"));
                    else
                        user.setPasswordStatus("");

                    if (jsonObj.optString("USER_LOGIN_NO") != null && !jsonObj.optString("USER_LOGIN_NO").equalsIgnoreCase("") && !jsonObj.optString("USER_LOGIN_NO").equalsIgnoreCase("null"))
                        user.setLoginNumber(jsonObj.optString("USER_LOGIN_NO"));
                    else
                        user.setLoginNumber("");

                    if (jsonObj.optString("USER_LOGID") != null && !jsonObj.optString("USER_LOGID").equalsIgnoreCase("") && !jsonObj.optString("USER_LOGID").equalsIgnoreCase("null"))
                        user.setUser_logid(jsonObj.optString("USER_LOGID"));
                    else
                        user.setUser_logid("");

                    if (jsonObj.optString("APP_VERSION") != null && !jsonObj.optString("APP_VERSION").equalsIgnoreCase("") && !jsonObj.optString("APP_VERSION").equalsIgnoreCase("null"))
                        user.setApp_version(jsonObj.optString("APP_VERSION"));
                    else
                        user.setApp_version("");

                    if (jsonObj.optString("SDR_ALLOW") != null && !jsonObj.optString("SDR_ALLOW").equalsIgnoreCase("") && !jsonObj.optString("SDR_ALLOW").equalsIgnoreCase("null"))
                        user.setSdr_allow(jsonObj.optString("SDR_ALLOW"));
                    else
                        user.setSdr_allow("");

                    if (jsonObj.optString("DL_ALLOW") != null && !jsonObj.optString("DL_ALLOW").equalsIgnoreCase("") && !jsonObj.optString("DL_ALLOW").equalsIgnoreCase("null"))
                        user.setDl_allow(jsonObj.optString("DL_ALLOW"));
                    else
                        user.setDl_allow("");

                    if (jsonObj.optString("VEHICLE_ALLOW") != null && !jsonObj.optString("VEHICLE_ALLOW").equalsIgnoreCase("") && !jsonObj.optString("VEHICLE_ALLOW").equalsIgnoreCase("null"))
                        user.setVehicle_allow(jsonObj.optString("VEHICLE_ALLOW"));
                    else
                        user.setVehicle_allow("");

                    if (jsonObj.optString("KMC_ALLOW") != null && !jsonObj.optString("KMC_ALLOW").equalsIgnoreCase("") && !jsonObj.optString("KMC_ALLOW").equalsIgnoreCase("null"))
                        user.setKmc_allow(jsonObj.optString("KMC_ALLOW"));
                    else
                        user.setKmc_allow("");

                    if (jsonObj.optString("GAS_ALLOW") != null && !jsonObj.optString("GAS_ALLOW").equalsIgnoreCase("") && !jsonObj.optString("GAS_ALLOW").equalsIgnoreCase("null"))
                        user.setGas_allow(jsonObj.optString("GAS_ALLOW"));
                    else
                        user.setGas_allow("");

                    if (jsonObj.optString("CABLE_ALLOW") != null && !jsonObj.optString("CABLE_ALLOW").equalsIgnoreCase("") && !jsonObj.optString("CABLE_ALLOW").equalsIgnoreCase("null"))
                        user.setCable_allow(jsonObj.optString("CABLE_ALLOW"));
                    else
                        user.setCable_allow("");
                     /* -----------------------------------------for special service------------------------------------------------------------------*/
                    if (jsonObj.optString("SPLSVC_ALLOW") != null && !jsonObj.optString("SPLSVC_ALLOW").equalsIgnoreCase("") && !jsonObj.optString("SPLSVC_ALLOW").equalsIgnoreCase("null"))
                        user.setSplSvcAllow(jsonObj.optString("SPLSVC_ALLOW"));//"SPLSVC_ALLOW": "1"
                    else
                        user.setSplSvcAllow("");
                    /* -----------------------------------------for special service which request allowed to user------------------------------------------------------------------*/
                    if (jsonObj.optString("CDR_REQUEST_ACCESS") != null && !jsonObj.optString("CDR_REQUEST_ACCESS").equalsIgnoreCase("") && !jsonObj.optString("CDR_REQUEST_ACCESS").equalsIgnoreCase("null"))
                        user.setCdrRequestAccess(jsonObj.optString("CDR_REQUEST_ACCESS"));
                    else
                        user.setCdrRequestAccess("");
                    if (jsonObj.optString("SDR_REQUEST_ACCESS") != null && !jsonObj.optString("SDR_REQUEST_ACCESS").equalsIgnoreCase("") && !jsonObj.optString("SDR_REQUEST_ACCESS").equalsIgnoreCase("null"))
                        user.setSdrRequestAccess(jsonObj.optString("SDR_REQUEST_ACCESS"));
                    else
                        user.setSdrRequestAccess("");
                    if (jsonObj.optString("IMEI_REQUEST_ACCESS") != null && !jsonObj.optString("IMEI_REQUEST_ACCESS").equalsIgnoreCase("") && !jsonObj.optString("IMEI_REQUEST_ACCESS").equalsIgnoreCase("null"))
                        user.setImeiRequestAccess(jsonObj.optString("IMEI_REQUEST_ACCESS"));
                    else
                        user.setImeiRequestAccess("");
                    if (jsonObj.optString("TL_REQUEST_ACCESS") != null && !jsonObj.optString("TL_REQUEST_ACCESS").equalsIgnoreCase("") && !jsonObj.optString("TL_REQUEST_ACCESS").equalsIgnoreCase("null"))
                        user.setTowerRequestAccess(jsonObj.optString("TL_REQUEST_ACCESS"));
                    else
                        user.setTowerRequestAccess("");
                    if (jsonObj.optString("IPDR_REQUEST_ACCESS") != null && !jsonObj.optString("IPDR_REQUEST_ACCESS").equalsIgnoreCase("") && !jsonObj.optString("IPDR_REQUEST_ACCESS").equalsIgnoreCase("null"))
                        user.setIpdrRequestAccess(jsonObj.optString("IPDR_REQUEST_ACCESS"));
                    else
                        user.setIpdrRequestAccess("");
                    if (jsonObj.optString("ILD_REQUEST_ACCESS") != null && !jsonObj.optString("ILD_REQUEST_ACCESS").equalsIgnoreCase("") && !jsonObj.optString("ILD_REQUEST_ACCESS").equalsIgnoreCase("null"))
                        user.setIldRequestAccess(jsonObj.optString("ILD_REQUEST_ACCESS"));
                    else
                        user.setIldRequestAccess("");
                   /* if (jsonObj.optString("CDMP_REQUEST_ACCESS") != null && !jsonObj.optString("CDMP_REQUEST_ACCESS").equalsIgnoreCase("") && !jsonObj.optString("CDMP_REQUEST_ACCESS").equalsIgnoreCase("null"))
                        user.setCdumpRequestAccess(jsonObj.optString("CDMP_REQUEST_ACCESS"));
                    else
                        user.setCdumpRequestAccess("");*/
                    if (jsonObj.optString("CAF_REQUEST_ACCESS") != null && !jsonObj.optString("CAF_REQUEST_ACCESS").equalsIgnoreCase("") && !jsonObj.optString("CAF_REQUEST_ACCESS").equalsIgnoreCase("null"))
                        user.setCafRequestAccess(jsonObj.optString("CAF_REQUEST_ACCESS"));
                    else
                        user.setCafRequestAccess("");
                    if (jsonObj.optString("LOGGER_REQUEST_ACCESS") != null && !jsonObj.optString("LOGGER_REQUEST_ACCESS").equalsIgnoreCase("") && !jsonObj.optString("LOGGER_REQUEST_ACCESS").equalsIgnoreCase("null"))
                        user.setLoggerRequestAccess(jsonObj.optString("LOGGER_REQUEST_ACCESS"));//jsonObj.optString("LOGGER_REQUEST_ACCESS")
                    else
                        user.setLoggerRequestAccess("");
                    if (jsonObj.optString("MNP_REQUEST_ACCESS") != null && !jsonObj.optString("MNP_REQUEST_ACCESS").equalsIgnoreCase("") && !jsonObj.optString("MNP_REQUEST_ACCESS").equalsIgnoreCase("null"))
                        user.setMnpRequestAccess(jsonObj.optString("MNP_REQUEST_ACCESS"));
                    else
                        user.setLoggerRequestAccess("");
                    if (jsonObj.optString("RD_REQUEST_ACCESS") != null && !jsonObj.optString("RD_REQUEST_ACCESS").equalsIgnoreCase("") && !jsonObj.optString("RD_REQUEST_ACCESS").equalsIgnoreCase("null"))
                        user.setRdRequestAccess(jsonObj.optString("RD_REQUEST_ACCESS"));
                    else
                        user.setLoggerRequestAccess("");
                    if (jsonObj.optString("CIDCHART_REQUEST_ACCESS") != null && !jsonObj.optString("CIDCHART_REQUEST_ACCESS").equalsIgnoreCase("") && !jsonObj.optString("CIDCHART_REQUEST_ACCESS").equalsIgnoreCase("null"))
                        user.setCellIdChart(jsonObj.optString("CIDCHART_REQUEST_ACCESS"));
                    else
                        user.setCellIdChart("");

                    /*-------------------------------------------for special service which request allowed to user----------------------------------------------------------*/

                    if (jsonObj.optString("IS_OC") != null && !jsonObj.optString("IS_OC").equalsIgnoreCase("") && !jsonObj.optString("IS_OC").equalsIgnoreCase("null"))
                        user.setIS_OC(jsonObj.optString("IS_OC"));
                    else
                        user.setIS_OC("");

                    if (jsonObj.optString("IS_DC") != null && !jsonObj.optString("IS_DC").equalsIgnoreCase("") && !jsonObj.optString("IS_DC").equalsIgnoreCase("null"))
                        user.setIS_DC(jsonObj.optString("IS_DC"));
                    else
                        user.setIS_DC("");



                    if (jsonObj.optString("ROLE") != null && !jsonObj.optString("ROLE").equalsIgnoreCase("") && !jsonObj.optString("ROLE").equalsIgnoreCase("null"))
                        user.setUserDesignation(jsonObj.optString("ROLE"));//"1"

                    //user.setScreenshot_allow(jsonObj.optString("SCREENSHOT_ALLOW"));
                    // user.setToken(jsonObj.optString("token"));
                    Utility.setUserInfo(this, user);


                    try {

                        //Log.e("APP VERSION",Utility.getUserInfo(this).getApp_version());
                        Utility.setLogMessage("APP VERSION", Utility.getUserInfo(this).getApp_version());

                        if (Utility.getUserInfo(this).getApp_version().equalsIgnoreCase("null") || (!Utility.getUserInfo(this).getApp_version().equalsIgnoreCase(Utility.getAppVersion(this)))) {

                            Intent logoutIntent = new Intent(this, AppInfoService.class);
                            startService(logoutIntent);
                        } else {
                            // Log.e("App Version", "Stored");
                            Utility.setLogMessage("App Version", "Stored");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    sessionManagement.storeEmailSession(jsonObj.optString("USER_EMAIL"));
                    if (!jsonObj.optString("SCREENSHOT_ALLOW").equalsIgnoreCase("") || !jsonObj.optString("SCREENSHOT_ALLOW").equalsIgnoreCase("null")) {
                        sessionManagement.setScreenShotFlag(jsonObj.optString("SCREENSHOT_ALLOW"));
                    }
                    View view = this.getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    startActivity(new Intent(LoginActivity.this,
                            DashboardActivity.class).putExtra("FROM_PAGE", "LOGIN"));
                    Constants.isShowWhatsNew = true;
                    finish();


                    //}
                }
                if (!jObj.optString("message").toString().equalsIgnoreCase("Success"))
               /* Toast.makeText(this, jObj.optString("message"),
                        Toast.LENGTH_LONG).show();*/
                    showAlertDialog(this, " Error ", jObj.optString("message"), false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    public void parseForgetpasswordResponse(String response) {
        //Log.e("Response", "Response " + response);
        Utility.setLogMessage("Response", "Response " + response);
        try {
            JSONObject jObj = new JSONObject(response);
            if (jObj != null && jObj.opt("status").toString().equalsIgnoreCase("1")) {
                Utility.showOKDialogMessage(this, getResources().getString(R.string.app_name), jObj.optString("message"));
                et_email.setText("");
                rl_forget.setVisibility(View.GONE);
                tv_forget.setVisibility(View.VISIBLE);
            } else {
                //Toast.makeText(this, "Some error has been encountered", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            //Toast.makeText(this, "Some error has been encountered", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void disableTemporaryButtonClick() {

        btn_login.setEnabled(false);

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                LoginActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        btn_login.setEnabled(true);

                    }
                });
            }
        }).start();

    }

    /**
     * Function to display simple Alert Dialog
     *
     * @param context - application context
     * @param title   - alert dialog title
     * @param message - alert message
     * @param status  - success/failure (used to set icon)
     */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {

        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }


                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    public void showAlertForProceed() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);
        alertDialog.setTitle("Alert!");
        alertDialog.setMessage("Do you want to reset your password ? ");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                userForgotPasswordTask();

            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        alertDialog.show();
    }

    /*This method is used
    * to forgot user password
    * */

    private void userForgotPasswordTask() {

        if (!Constants.internetOnline(this)) {
            Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_FORGOT_PSWD);
        taskManager.setUserForgotPassword(true);
        String[] keys = {"imei_no"};
        String[] values = {Constants.device_IMEI_No};
        taskManager.doStartTask(keys, values, true);

    }

    public void parseUserForgotPasswordResponse(String jsonStr) {

        if (jsonStr != null && !jsonStr.equals("")) {

            try {
                JSONObject jObj = new JSONObject(jsonStr);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {
                    JSONObject jsonObj = jObj.getJSONObject("result");

                    String ph_no = jsonObj.optString("USER_PHONE_NO");
                    String current_pswd = jsonObj.optString("CURRENT_PASSWORD");

                    //Log.e("PH_No",ph_no);
                    Utility.setLogMessage("PH_No", ph_no);
                    //Log.e("C_PSWD",current_pswd);
                    Utility.setLogMessage("C_PSWD", current_pswd);


                    sendCurrentPasswordToUser(ph_no, current_pswd);

                } else {
                    showAlertDialog(this, " Error ", jObj.optString("message"), false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    private void sendCurrentPasswordToUser(String ph_no, String current_pswd) {

        if (Constants.internetOnline(LoginActivity.this)) {

            AsynctaskForSendSMS asynctaskForSendSMS = new AsynctaskForSendSMS(LoginActivity.this, ph_no, current_pswd);
            asynctaskForSendSMS.execute();
            asynctaskForSendSMS.setDelegate(LoginActivity.this);

        } else {

            showAlertDialog(LoginActivity.this, "Connectivity Problem", "Sorry, internet connection not found", false);

        }


    }

    @Override
    public void onSendSMSResultSuccess(String success) {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                showAlertDialog(LoginActivity.this, " Success ", "The Password has been successfully sent to your mobile ", true);
            }
        });

    }

    @Override
    public void onSendSMSResultError(final String error) {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                showAlertDialog(LoginActivity.this, " Error ", error, false);
            }
        });
    }

    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("LoginActivity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        KPFaceDetectionApplication.activityResumed();
        System.out.println("Alarm manager cancel");
        //Log.e("TAG", "Alarm Manager Cancel");
        Utility.setLogMessage("TAG", "Alarm Manager Cancel");

        Constants.resume_count++;
        if (Constants.resume_count >= 1) {
            alarmMgr.cancel(alarmIntent);
            Constants.resume_count = 0;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        KPFaceDetectionApplication.activityPaused();

        int seconds = Constants.logout_interval; //2hr
        // Create an intent that will be wrapped in PendingIntent
        Intent intent = new Intent(this, SessionCheckReceiver.class);

        // Create the pending intent and wrap our intent
        alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

        // Get the alarm manager service and schedule it to go off after 3s

        alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + (seconds * 1000), alarmIntent);

        System.out.println("Alarm Manager Started");
        //Log.e("TAG", "Alarm Manager Started");
        Utility.setLogMessage("TAG", "Alarm Manager Started");

    }

    public void showAlertDialogFingerPrintSuccess(final Context context, String title, String message, final Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);


        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                        if(status){
                            isFingerPrint = "1";
                            //initView();

                            try {
                                String userId = et_userid.getText().toString().trim();
                                if (userId.equals("")) {
                                    //showMesage("Please enter a valid Username", et_userid);
                                    showAlertDialog(context, " Error ", "Please enter a valid username ", false);
                                }
                                else {
                                    View view = ((LoginActivity)context).getCurrentFocus();
                                    if (view != null) {
                                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                    }
                                    disableTemporaryButtonClick();
                                    C2dmRegistration();
//				logInTask();
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }

                        }else{
                            //update("You are not Authenticated ", false);

                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }
}