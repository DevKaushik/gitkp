package com.kp.facedetection.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.imageloader.ImageLoader;
import com.kp.facedetection.model.CriminalDetails;
import com.kp.facedetection.utility.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 01-08-2016.
 */
public class ModifiedCriminalAdapter extends BaseAdapter {

    private Context mContext;;
    List<CriminalDetails> criminalList;
    ImageLoader imageLoader;
    private List<String> imageExtensionList = new ArrayList<String>();

    public ModifiedCriminalAdapter(Context context,List<CriminalDetails> criminalList) {

        mContext = context;
        this.criminalList = criminalList;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {

        if(criminalList.size()>0) {
            return criminalList.size();
        }
        else {
            return 0;
        }

    }


    @Override
    public Object getItem(int position) {
        return criminalList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.modified_search_listitem_layout, null);

            holder.tv_name=(TextView)convertView.findViewById(R.id.tv_name);
            holder.tv_alias_name = (TextView) convertView.findViewById(R.id.tv_alias_name);
            holder.tv_dob = (TextView) convertView.findViewById(R.id.tv_dob);
            holder.iv_searchResultImage = (ImageView) convertView.findViewById(R.id.iv_searchResultImage);
            holder.tv_cr_flagValue = (TextView) convertView.findViewById(R.id.tv_cr_flagValue);

            holder.tv_dob.setEllipsize(TextUtils.TruncateAt.END);

            Constants.changefonts(holder.tv_name, mContext, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_alias_name, mContext, "Calibri.ttf");
            Constants.changefonts(holder.tv_dob, mContext, "Calibri.ttf");
            Constants.changefonts(holder.tv_cr_flagValue, mContext, "Calibri Bold.ttf");

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        holder.tv_alias_name.setVisibility(View.GONE);

        if (criminalList.get(position).getNameAccused().contains("NAME_ACCUSED: ")) {

            if (!criminalList.get(position).getNameAccused().replace("NAME_ACCUSED: ", "").equalsIgnoreCase("")) {

                if (!criminalList.get(position).getCriminal_alias().equalsIgnoreCase("")) {
                    holder.tv_name.setText(criminalList.get(position).getNameAccused().replace("NAME_ACCUSED: ", "") + criminalList.get(position).getCriminal_alias());
                } else {
                    holder.tv_name.setText(criminalList.get(position).getNameAccused().replace("NAME_ACCUSED: ", ""));
                }
            } else {
                holder.tv_name.setVisibility(View.GONE);
            }
        }

        if (criminalList.get(position).getAddressAccused().contains("ADDR_ACCUSED: ")) {
            holder.tv_dob.setVisibility(View.VISIBLE);
            if (!criminalList.get(position).getAddressAccused().replace("ADDR_ACCUSED: ", "").equalsIgnoreCase("")) {
                holder.tv_dob.setVisibility(View.VISIBLE);
                holder.tv_dob.setText(criminalList.get(position).getAddressAccused().replace("ADDR_ACCUSED: ", ""));
            } else {
                holder.tv_dob.setVisibility(View.GONE);
            }
        } else {
            holder.tv_dob.setVisibility(View.GONE);
        }


        if (criminalList.get(position).getPicture_list().size() > 0) {

            imageExtensionList.clear();

            imageExtensionList.add("gif");
            imageExtensionList.add("bmp");
            imageExtensionList.add("jpg");
            imageExtensionList.add("jpeg");
            imageExtensionList.add("png");


            final String original_url = criminalList.get(position).getPicture_list().get(0);
            final String extension = original_url.substring(original_url.lastIndexOf(".") + 1);
            System.out.println("Extension: " + extension);
            imageExtensionList.remove(imageExtensionList.indexOf(extension));

            if (imageLoader.getBitmap(original_url) != null) {

                imageLoader.DisplayImage(
                        original_url,
                        holder.iv_searchResultImage);
            }
            else{

                for (int i = 0; i < imageExtensionList.size(); i++) {

                    if (imageLoader.getBitmap(original_url.replace(extension, imageExtensionList.get(i))) != null) {

                        final int finalI = i;
                        ((Activity) mContext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imageLoader.DisplayImage(
                                        original_url.replace(extension, imageExtensionList.get(finalI)),
                                        holder.iv_searchResultImage);
                            }
                        });

                        break;
                    }

                }


            }

           /* new Thread(new Runnable() {
                @Override
                public void run() {

                    if (imageLoader.getBitmap(original_url) != null) {
                        ((Activity) mContext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                imageLoader.DisplayImage(
                                        original_url,
                                        holder.iv_searchResultImage);

                            }
                        });
                    } else {

                        for (int i = 0; i < imageExtensionList.size(); i++) {

                            if (imageLoader.getBitmap(original_url.replace(extension, imageExtensionList.get(i))) != null) {

                                final int finalI = i;
                                ((Activity) mContext).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        imageLoader.DisplayImage(
                                                original_url.replace(extension, imageExtensionList.get(finalI)),
                                                holder.iv_searchResultImage);
                                    }
                                });

                                break;
                            }

                        }
                    }

                }
            }).start();*/

        }
        else{
            holder.iv_searchResultImage.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.human3));
        }

        return convertView;
    }


    class ViewHolder {
        TextView tv_name, tv_alias_name, tv_dob, tv_cr_flagValue;
        ImageView iv_searchResultImage;
    }

}
