package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 01-11-2016.
 */
public class MobileStolenSearchDetails implements Serializable {

    private String rowNo="";
    private String trnId="";
    private String dtMissing = "";
    private String psCode="";
    private String mobileNo="";
    private String imeiNo="";
    private String complainantName="";
    private String caseRef="";
    private String psName="";
    private String complainantAddress="";
    private String plMissing="";
    private String ioName="";
    private String gdeDate="";
    private String brFact="";
    private String prStatus="";
    private String reference="";
    private String firGD="";
    private String contactNo="";
    private String usec="";
    private String sProvider="";
    private String make="";
    private String model="";
    private String dtHandedOver="";
    private String sourceName="";
    private String tracedOn="";
    private String tracedMobNo="";
    private String nameUser="";
    private String addr="";
    private String recoverDate="";
    private String recoverBy="";
    private String esn="";
    private String devType="";
    private String mobileNo2="";
    private String imei2="";
    private String esn2="";
    private String sprovider2="";

    public String getTracedOn() {
        return tracedOn;
    }

    public void setTracedOn(String tracedOn) {
        this.tracedOn = tracedOn;
    }

    public String getTracedMobNo() {
        return tracedMobNo;
    }

    public void setTracedMobNo(String tracedMobNo) {
        this.tracedMobNo = tracedMobNo;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getRecoverDate() {
        return recoverDate;
    }

    public void setRecoverDate(String recoverDate) {
        this.recoverDate = recoverDate;
    }

    public String getRecoverBy() {
        return recoverBy;
    }

    public void setRecoverBy(String recoverBy) {
        this.recoverBy = recoverBy;
    }

    public String getEsn() {
        return esn;
    }

    public void setEsn(String esn) {
        this.esn = esn;
    }

    public String getDevType() {
        return devType;
    }

    public void setDevType(String devType) {
        this.devType = devType;
    }

    public String getMobileNo2() {
        return mobileNo2;
    }

    public void setMobileNo2(String mobileNo2) {
        this.mobileNo2 = mobileNo2;
    }

    public String getImei2() {
        return imei2;
    }

    public void setImei2(String imei2) {
        this.imei2 = imei2;
    }

    public String getEsn2() {
        return esn2;
    }

    public void setEsn2(String esn2) {
        this.esn2 = esn2;
    }

    public String getSprovider2() {
        return sprovider2;
    }

    public void setSprovider2(String sprovider2) {
        this.sprovider2 = sprovider2;
    }

    public String getPsName() {
        return psName;
    }

    public void setPsName(String psName) {
        this.psName = psName;
    }

    public String getRowNo() {
        return rowNo;
    }

    public void setRowNo(String rowNo) {
        this.rowNo = rowNo;
    }

    public String getTrnId() {
        return trnId;
    }

    public void setTrnId(String trnId) {
        this.trnId = trnId;
    }

    public String getDtMissing() {
        return dtMissing;
    }

    public void setDtMissing(String dtMissing) {
        this.dtMissing = dtMissing;
    }

    public String getPsCode() {
        return psCode;
    }

    public void setPsCode(String psCode) {
        this.psCode = psCode;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getImeiNo() {
        return imeiNo;
    }

    public void setImeiNo(String imeiNo) {
        this.imeiNo = imeiNo;
    }

    public String getComplainantName() {
        return complainantName;
    }

    public void setComplainantName(String complainantName) {
        this.complainantName = complainantName;
    }

    public String getCaseRef() {
        return caseRef;
    }

    public void setCaseRef(String caseRef) {
        this.caseRef = caseRef;
    }

    public String getComplainantAddress() {
        return complainantAddress;
    }

    public void setComplainantAddress(String complainantAddress) {
        this.complainantAddress = complainantAddress;
    }

    public String getPlMissing() {
        return plMissing;
    }

    public void setPlMissing(String plMissing) {
        this.plMissing = plMissing;
    }

    public String getIoName() {
        return ioName;
    }

    public void setIoName(String ioName) {
        this.ioName = ioName;
    }

    public String getGdeDate() {
        return gdeDate;
    }

    public void setGdeDate(String gdeDate) {
        this.gdeDate = gdeDate;
    }

    public String getBrFact() {
        return brFact;
    }

    public void setBrFact(String brFact) {
        this.brFact = brFact;
    }

    public String getPrStatus() {
        return prStatus;
    }

    public void setPrStatus(String prStatus) {
        this.prStatus = prStatus;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getFirGD() {
        return firGD;
    }

    public void setFirGD(String firGD) {
        this.firGD = firGD;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getUsec() {
        return usec;
    }

    public void setUsec(String usec) {
        this.usec = usec;
    }

    public String getsProvider() {
        return sProvider;
    }

    public void setsProvider(String sProvider) {
        this.sProvider = sProvider;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDtHandedOver() {
        return dtHandedOver;
    }

    public void setDtHandedOver(String dtHandedOver) {
        this.dtHandedOver = dtHandedOver;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }
}
