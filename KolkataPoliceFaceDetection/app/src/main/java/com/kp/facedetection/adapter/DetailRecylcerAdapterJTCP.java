package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnItemClickListenerForDC;
import com.kp.facedetection.interfaces.OnItemClickListenerForJTCP;
import com.kp.facedetection.interfaces.OnItemClickListenerforLIPartialSaveJTCP;
import com.kp.facedetection.model.DC;
import com.kp.facedetection.model.JTCP;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;

import java.util.List;

/**
 * Created by user on 15-05-2018.
 */

public class DetailRecylcerAdapterJTCP extends RecyclerView.Adapter<DetailRecylcerAdapterJTCP.ViewHolder>  {
    OnItemClickListenerForJTCP onItemClickListenerForJTCP;
    List<JTCP> dataJTCP;
    Context mContext;
    OnItemClickListenerforLIPartialSaveJTCP onItemClickListenerforLIPartialSaveJTCP;

    public DetailRecylcerAdapterJTCP(Context context, List<JTCP> dataJTCP) {
        this.mContext = context;
        this.dataJTCP = dataJTCP;
    }
    public void setOnItemClickListenerForJTCP(OnItemClickListenerForJTCP onItemClickListenerForJTCP){
        this.onItemClickListenerForJTCP=onItemClickListenerForJTCP;
    }
    public void setOnItemClickListenerforLIPartialSave( OnItemClickListenerforLIPartialSaveJTCP onItemClickListenerforLIPartialSaveJTCP){
        this.onItemClickListenerforLIPartialSaveJTCP = onItemClickListenerforLIPartialSaveJTCP;
    }

    @Override
    public DetailRecylcerAdapterJTCP.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.special_services_inflater, viewGroup, false);
        DetailRecylcerAdapterJTCP.ViewHolder viewHolder = new DetailRecylcerAdapterJTCP.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DetailRecylcerAdapterJTCP.ViewHolder holder, int position) {

        holder.name_TV.setText(dataJTCP.get(position).getOfficerName());
        holder.id_TV.setText(dataJTCP.get(position).getId());
        holder.caseRef_TV.setText(dataJTCP.get(position).getCaseRef());
        holder.reqType_TV.setText(dataJTCP.get(position).getRequestType());
        if(dataJTCP.get(position).getReconnection().equalsIgnoreCase("1")){
            holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[0]);
        }
        else if(dataJTCP.get(position).getDisconnection().equalsIgnoreCase("1")){
            holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[1]);
        }
        else {
            if(dataJTCP.get(position).getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(URGENT)");
            }
            else {
                holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(GENERAL)");
            }
            //holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2]);
        }

        holder.div_TV.setText(dataJTCP.get(position).getDiv()+"/"+dataJTCP.get(position).getPs());
        holder.status.setText(dataJTCP.get(position).getStatusMsg());

        holder.status.setBackgroundColor(Color.parseColor(dataJTCP.get(position).getStatusColour()));
        holder.status.setText(dataJTCP.get(position).getStatusMsg());
        String date_time=dataJTCP.get(position).getRequestTime();
        String date="";
        if(date_time.contains(" ")) {
            String[] dateTimeArray= date_time.split(" ");
            date=dateTimeArray[0];
            String formatedDate= DateUtils.changeDateFormat(date);
            holder.date_time.setText(formatedDate);
        }

        Constants.changefonts(holder.name_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.id_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.caseRef_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.reqType_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.div_request_subtype_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.div_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.status_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.date_time_label, mContext,"Calibri Bold.ttf");

        Constants.changefonts(holder.name_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.id_TV, mContext,"Calibri.ttf");
        Constants.changefonts(holder.caseRef_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.reqType_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.div_request_subtype_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.div_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.status, mContext, "Calibri.ttf");
        Constants.changefonts(holder.date_time, mContext, "Calibri.ttf");
     /*   if (position % 2 == 0) {
            holder.ll_special_service_request_item.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            holder.ll_special_service_request_item.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }*/


    }


    @Override
    public int getItemCount() {
        return (dataJTCP != null ? dataJTCP.size() : 0);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView id_TV,caseRef_TV,name_TV,reqType_TV,div_TV,status,div_request_subtype_TV,date_time;
        TextView id_TV_label,caseRef_TV_label,name_TV_label,reqType_TV_label,div_TV_label,status_label,div_request_subtype_label,date_time_label;
        LinearLayout ll_special_service_request_item;


        public ViewHolder(View itemView) {
            super(itemView);
            ll_special_service_request_item=(LinearLayout)itemView.findViewById(R.id.ll_special_service_request_item);
            name_TV = (TextView) itemView.findViewById(R.id.name_TV);
            id_TV = (TextView) itemView.findViewById(R.id.id_TV);
            caseRef_TV = (TextView) itemView.findViewById(R.id.caseRef_TV);
            reqType_TV = (TextView) itemView.findViewById(R.id.reqType_TV);
            div_request_subtype_TV=(TextView) itemView.findViewById(R.id.div_request_subtype_TV);
            div_request_subtype_label=(TextView)itemView.findViewById(R.id.div_request_subtype_label);
            div_TV =(TextView) itemView.findViewById(R.id.div_TV);
            status = (TextView) itemView.findViewById(R.id.status);
            date_time = (TextView) itemView.findViewById(R.id.date_time);

            caseRef_TV_label=(TextView) itemView.findViewById(R.id.caseRef_TV_label);
            name_TV_label=(TextView) itemView.findViewById(R.id.name_TV_label);
            id_TV_label = (TextView) itemView.findViewById(R.id.id_TV_label);
            reqType_TV_label=(TextView) itemView.findViewById(R.id.reqType_TV_label);
            div_TV_label=(TextView) itemView.findViewById(R.id.div_TV_label);
            status_label=(TextView) itemView.findViewById(R.id.status_label);
            date_time_label=(TextView) itemView.findViewById(R.id.date_time_label);
            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {
           // onItemClickListenerForJTCP.onItemClickForJTCP(v,getLayoutPosition());
            if(dataJTCP.get(getLayoutPosition()).getDraftSave().equalsIgnoreCase("1")&& dataJTCP.get(getLayoutPosition()).getCompleteSave().equalsIgnoreCase("0")){
                onItemClickListenerforLIPartialSaveJTCP.onItemClickforLIPartialSaveJTCP(getLayoutPosition());
            }
            else if(dataJTCP.get(getLayoutPosition()).getDraftSave().equalsIgnoreCase("1")&& dataJTCP.get(getLayoutPosition()).getCompleteSave().equalsIgnoreCase("1")){
                onItemClickListenerForJTCP.onItemClickForJTCP(v,getLayoutPosition());
            }
            else {
                onItemClickListenerForJTCP.onItemClickForJTCP(v,getLayoutPosition());
            }
        }
    }

}
