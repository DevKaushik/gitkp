package com.kp.facedetection.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.kp.facedetection.KMCSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.model.KMCSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 06-12-2016.
 */
public class ModifiedKMCDataSearchFragment extends Fragment {

    private RelativeLayout relative_kmc_main;
    private TextView tv_notAuthorized;

    private EditText et_wardNo,et_owner_name, et_person_liable, et_address;
    private Button btn_kmcSearch, btn_reset;

    private String pageno="";
    private String totalResult="";
    private String[] keys;
    private String[] values;
    String userId="";
    String devicetoken="";
    String auth_key="";


    private List<KMCSearchDetails> kmcSearchDeatilsList;

    public static ModifiedKMCDataSearchFragment newInstance() {

        ModifiedKMCDataSearchFragment modifiedKMCDataSearchFragment= new ModifiedKMCDataSearchFragment();
        return modifiedKMCDataSearchFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.modified_fragment_kmcdata_search,container,false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        userId= Utility.getUserInfo(getContext()).getUserId();
        relative_kmc_main = (RelativeLayout) view.findViewById(R.id.relative_kmc_main);
        tv_notAuthorized = (TextView) view.findViewById(R.id.tv_notAuthorized);

        et_wardNo = (EditText) view.findViewById(R.id.et_wardNo);
        et_owner_name = (EditText) view.findViewById(R.id.et_owner_name);
        et_person_liable = (EditText) view.findViewById(R.id.et_person_liable);
        et_address = (EditText) view.findViewById(R.id.et_address);

        btn_kmcSearch = (Button) view.findViewById(R.id.btn_kmcSearch);
        btn_reset = (Button) view.findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_kmcSearch);
        Constants.buttonEffect(btn_reset);

        try {
            if (Utility.getUserInfo(getActivity()).getKmc_allow().equalsIgnoreCase("1")) {
                relative_kmc_main.setVisibility(View.VISIBLE);
                tv_notAuthorized.setVisibility(View.GONE);
            } else {
                relative_kmc_main.setVisibility(View.GONE);
                tv_notAuthorized.setVisibility(View.VISIBLE);
                Constants.changefonts(tv_notAuthorized, getActivity(), "Calibri Bold.ttf");
            }
        }catch(NullPointerException e){
            e.printStackTrace();
            relative_kmc_main.setVisibility(View.GONE);
            tv_notAuthorized.setVisibility(View.VISIBLE);
            Constants.changefonts(tv_notAuthorized, getActivity(), "Calibri Bold.ttf");
        }

        clickEvents();
    }

    private void clickEvents() {

        btn_kmcSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String wardNo = et_wardNo.getText().toString().trim();
                String ownerName = et_owner_name.getText().toString().trim();
                String personLiable = et_person_liable.getText().toString().trim();
                String maiilingAddress = et_address.getText().toString().trim();

                if(!wardNo.equalsIgnoreCase("") || !ownerName.equalsIgnoreCase("")
                        || !personLiable.equalsIgnoreCase("") || !maiilingAddress.equalsIgnoreCase("")){

                    showAlertForProceed();
                }
                else{
                    showAlertDialog(getActivity(), " Search Error!!! ", "Please provide atleast one value for search", false);
                }

            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_wardNo.setText("");
                et_owner_name.setText("");
                et_address.setText("");
                et_person_liable.setText("");
            }
        });
    }



    /*
   * Show alert before loading the data
   * */
    private void showAlertForProceed() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setTitle("Alert!");
        alertDialog.setMessage("It will take considerable time, proceed");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                fetchSearchResult();
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        alertDialog.show();
    }


    /*
  *    web service call
  * */
    private void fetchSearchResult() {

        String wardNo = et_wardNo.getText().toString().trim();
        String ownerName = et_owner_name.getText().toString().trim();
        String personLiable = et_person_liable.getText().toString().trim();
        String maiilingAddress = et_address.getText().toString().trim();

        if(!wardNo.equalsIgnoreCase("") || !ownerName.equalsIgnoreCase("")
                || !personLiable.equalsIgnoreCase("") || !maiilingAddress.equalsIgnoreCase("")){

            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.MODIFIED_METHOD_DOCUMENT_KMC_SEARCH);
            taskManager.setDocumentKMCSearch(true);
            try{
                devicetoken = GCMRegistrar.getRegistrationId(getActivity());
                auth_key=Utility.getDocumentSearchSesionId(getActivity());
            }
            catch (Exception e){

            }

            String []keys= {"wardno", "owner_name", "person_liable", "address", "pageno","user_id","device_type", "device_token","imei","auth_key"};
            String []values= {wardNo.trim(), ownerName.trim(), personLiable.trim(), maiilingAddress.trim(), "1",userId,"android", devicetoken,Utility.getImiNO(getActivity()),auth_key};

            taskManager.doStartTask(keys,values,true,true);

        }
        else{
            showAlertDialog(getActivity(), " Search Error!!! ", "Please provide atleast one value for search", false);
        }
    }


    /*
    *   Parsing all data
    * */
    public void parseDocumentKMCSearch(String result,String[] keys, String[] values) {

        //System.out.println("Document KMC Search Result: " + result);

        if(result != null && !result.equals("")){

            try{

                JSONObject jObj = new JSONObject(result);
                if(jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("page").toString();
                    totalResult = jObj.opt("count").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseKMCSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(getActivity()," Search Error ",jObj.optString("message"),false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                showAlertDialog(getActivity(), " Search Error!!! ", "Some error has been encountered", false);
            }

        }
    }


    private void parseKMCSearchResponse(JSONArray resultArray) {

        kmcSearchDeatilsList = new ArrayList<KMCSearchDetails>();

        for(int i=0;i<resultArray.length();i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj = resultArray.getJSONObject(i);

                KMCSearchDetails kmcSearchDetails = new KMCSearchDetails(Parcel.obtain());

                if(!jsonObj.optString("ROWNUMBER").equalsIgnoreCase("null")){
                    kmcSearchDetails.setKmc_slNo(jsonObj.optString("ROWNUMBER"));
                }

                if(!jsonObj.optString("WARD").equalsIgnoreCase("null")){
                    kmcSearchDetails.setKmc_wardNo(jsonObj.optString("WARD"));
                }

                if(!jsonObj.optString("PREMISES").equalsIgnoreCase("null")){
                    kmcSearchDetails.setKmc_premises(jsonObj.optString("PREMISES"));
                }

                if(!jsonObj.optString("OWNER").equalsIgnoreCase("null")){
                    kmcSearchDetails.setKmc_owner(jsonObj.optString("OWNER"));
                }

                if(!jsonObj.optString("PERSON_LIABLE").equalsIgnoreCase("null")){
                    kmcSearchDetails.setKmc_personLiable(jsonObj.optString("PERSON_LIABLE"));
                }

                if(!jsonObj.optString("MAILING_ADDRESS").equalsIgnoreCase("null")){
                    kmcSearchDetails.setKmc_mailingAddress(jsonObj.optString("MAILING_ADDRESS"));
                }

                kmcSearchDeatilsList.add(kmcSearchDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Intent intent = new Intent(getActivity(), KMCSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putParcelableArrayListExtra("KMC_SEARCH_LIST", (ArrayList<? extends Parcelable>) kmcSearchDeatilsList);
        startActivity(intent);
    }


    /*
   *  show alert if any error occured
   * */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }
}
