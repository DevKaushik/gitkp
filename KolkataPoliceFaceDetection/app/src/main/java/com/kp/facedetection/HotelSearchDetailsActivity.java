package com.kp.facedetection;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.model.HotelDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

public class HotelSearchDetailsActivity extends BaseActivity implements View.OnClickListener {
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";
    private String userImage = "";
    private String id="";
    private String name="";
    private String age="";
    private String sex="";
    private String address="";
    private String contactNo="";
    private String idType="";
    private String idNumber="";
    private String comingFrom="";
    private String proceedingTo="";
    private String adult="";
    private String child="";
    private String hotelName="";
    private String hotelAddress="";
    private String hotelRoomNo="";
    private String dateTimeArrival="";
    private String dateTimeDeparture="";
    private String visitPurpose;
    private String policeArea;
    private String division;

    /* view initialization */
    ImageView imageView;
    ImageView iv_userImage_foreign;

    private TextView tv_id_numberValue;
    private TextView tv_nameValue;
    private TextView tv_ageValue;
    private TextView tv_sexValue;
    private TextView tv_addressValue;
    private TextView tv_contactValue;
    private TextView tv_idNo_value;
    private TextView tv_idTypeValue;
    private TextView tv_coming_value;
    private TextView tv_proceedingValue;
    private TextView tv_adultValue;
    private TextView tv_childValue;
    private TextView tv_hotelNameValue;
    private TextView tv_hotelCodeValue;
    private TextView tv_hotelAddressValue;
    private TextView tv_hotel_ps_value;
    private TextView tv_hotelroomNoValue;
    private TextView tv_status_value;
    private TextView tv_dateofArrivalValue;
    private TextView tv_dateofDepartureValue;
    private TextView tv_purposeofVisitValue;
    private TextView tv_policaAreaValue;
    private TextView tv_divisionValue;

    private TextView tv_id_number_foreignValue;
    private TextView tv_name_foreignValue;
    private TextView tv_date_of_birthValue;
    private TextView tv_nationality_value;
    private TextView tv_addressValue_foreign;
    private TextView tv_addressValue_india;
    private TextView tv_contact_foreignValue;
    private TextView tv_contact_indianValue;
    private TextView tv_passportNoValue_foreign;
    private TextView tv_passport_placeValue;
    private TextView tv_passport_issuedateValue;
    private TextView tv_passport_validValue;
    private TextView tv_vissaNoValue;
    private TextView tv_visa_Issue_DateValue;
    private TextView tv_visa_Valid_TillValue;
    private TextView tv_type_of_VisaValue;
    private TextView tv_visa_issue_placeValue;
    private TextView tv_comingValue_foreign;
    private TextView tv_proceedingValue_foreign;
    private TextView tv_dateofArrivalValue_foreign;
    private TextView tv_dateofDepartureValue_foreign;
    private TextView tv_hotelNameValue_foreign;
    private TextView tv_hotel_code_value_foreign;
    private TextView tv_hotelAddressValue_foreign;
    private TextView tv_hotel_ps_value_foreign;
    private TextView tv_hotel_phone_value_foreign;
    private TextView tv_hotelroomNoValue_foreign;
    private TextView tv_status_value_foreign;
    private TextView tv_purposeofVisitValue_foreign;
    private TextView tv_policaAreaValue_foreign;
    private TextView tv_divisionValue_foreign;
    LinearLayout ll_national,ll_foreign;




    TextView chargesheetNotificationCount;
    String notificationCount="";
    String VisitorType="";
    HotelDetails hotelDetails;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_search_details);
        getHotelDetailsIntent();
        initialization();
    }
    private void getHotelDetailsIntent(){
        VisitorType=getIntent().getStringExtra("visitoreType");
        hotelDetails= (HotelDetails) getIntent().getSerializableExtra("HOTEL_DETAILS");
    }
    private void initialization(){
        ll_national=(LinearLayout)findViewById(R.id.ll_national);
        ll_foreign=(LinearLayout)findViewById(R.id.ll_foreign);

        imageView=(ImageView)findViewById(R.id.iv_userImage);
        imageView.setOnClickListener(this);
        tv_id_numberValue=(TextView)findViewById(R.id.tv_id_numberValue);
        tv_nameValue=(TextView)findViewById(R.id.tv_nameValue);
        tv_ageValue=(TextView)findViewById(R.id.tv_ageValue);
        tv_sexValue=(TextView)findViewById(R.id.tv_sexValue);
        tv_addressValue=(TextView)findViewById(R.id.tv_addressValue);
        tv_contactValue=(TextView)findViewById(R.id.tv_contactValue);
        tv_idNo_value=(TextView)findViewById(R.id.tv_idNo_value);
        tv_idTypeValue=(TextView)findViewById(R.id.tv_idTypeValue);
        tv_coming_value=(TextView)findViewById(R.id.tv_coming_value);
        tv_proceedingValue=(TextView)findViewById(R.id.tv_proceedingValue);
        tv_adultValue=(TextView)findViewById(R.id.tv_adultValue);
        tv_childValue=(TextView)findViewById(R.id.tv_childValue);
        tv_hotelNameValue=(TextView)findViewById(R.id.tv_hotelNameValue);
        tv_hotelCodeValue=(TextView)findViewById(R.id.tv_hotel_code_value);
        tv_hotel_ps_value=(TextView)findViewById(R.id.tv_hotel_ps_value);
        tv_hotelAddressValue=(TextView)findViewById(R.id.tv_hotelAddressValue);
        tv_hotelroomNoValue=(TextView)findViewById(R.id.tv_hotelroomNoValue);
        tv_status_value=(TextView)findViewById(R.id.tv_status_value);
        tv_dateofArrivalValue=(TextView)findViewById(R.id.tv_dateofArrivalValue);
        tv_dateofDepartureValue=(TextView)findViewById(R.id.tv_dateofDepartureValue);
        tv_purposeofVisitValue=(TextView)findViewById(R.id.tv_purposeofVisitValue);
        tv_policaAreaValue=(TextView)findViewById(R.id.tv_policaAreaValue);
        tv_divisionValue=(TextView)findViewById(R.id.tv_divisionValue);


        iv_userImage_foreign=(ImageView)findViewById(R.id.iv_userImage_foreign);
        iv_userImage_foreign.setOnClickListener(this);
        tv_id_number_foreignValue=(TextView)findViewById(R.id.tv_id_number_foreignValue);
        tv_name_foreignValue=(TextView)findViewById(R.id.tv_name_foreignValue);
        tv_date_of_birthValue=(TextView)findViewById(R.id.tv_date_of_birthValue);
        tv_nationality_value=(TextView)findViewById(R.id.tv_nationality_value);
        tv_addressValue_foreign=(TextView)findViewById(R.id.tv_addressValue_foreign);
        tv_addressValue_india=(TextView)findViewById(R.id.tv_addressValue_india);
        tv_contact_foreignValue=(TextView)findViewById(R.id.tv_contact_foreignValue);
        tv_contact_indianValue=(TextView)findViewById(R.id.tv_contact_indianValue);
        tv_passportNoValue_foreign=(TextView)findViewById(R.id.tv_passportNoValue_foreign);
        tv_passport_placeValue=(TextView)findViewById(R.id.tv_passport_placeValue);
        tv_passport_issuedateValue=(TextView)findViewById(R.id.tv_passport_issuedateValue);
        tv_passport_validValue=(TextView)findViewById(R.id.tv_passport_validValue);
        tv_vissaNoValue=(TextView)findViewById(R.id.tv_vissaNoValue);
        tv_visa_Issue_DateValue=(TextView)findViewById(R.id.tv_visa_Issue_DateValue);
        tv_visa_Valid_TillValue=(TextView)findViewById(R.id.tv_visa_Valid_TillValue);
        tv_type_of_VisaValue=(TextView)findViewById(R.id.tv_type_of_VisaValue);
        tv_visa_issue_placeValue=(TextView)findViewById(R.id.tv_visa_issue_placeValue);
        tv_comingValue_foreign=(TextView)findViewById(R.id.tv_comingValue_foreign);
        tv_proceedingValue_foreign=(TextView)findViewById(R.id.tv_proceedingValue_foreign);
        tv_dateofArrivalValue_foreign=(TextView)findViewById(R.id.tv_dateofArrivalValue_foreign);
        tv_dateofDepartureValue_foreign=(TextView)findViewById(R.id.tv_dateofDepartureValue_foreign);
        tv_hotelNameValue_foreign=(TextView)findViewById(R.id.tv_hotelNameValue_foreign);
        tv_hotel_code_value_foreign=(TextView)findViewById(R.id.tv_hotel_code_value_foreign);
        tv_hotelAddressValue_foreign=(TextView)findViewById(R.id.tv_hotelAddressValue_foreign);
        tv_hotel_ps_value_foreign=(TextView)findViewById(R.id.tv_hotel_ps_value_foreign);
        tv_hotel_phone_value_foreign=(TextView)findViewById(R.id.tv_hotel_phone_value_foreign);
        tv_hotelroomNoValue_foreign=(TextView)findViewById(R.id.tv_hotelroomNoValue_foreign);
        tv_status_value_foreign=(TextView)findViewById(R.id.tv_status_value_foreign);
        tv_purposeofVisitValue_foreign=(TextView)findViewById(R.id.tv_purposeofVisitValue_foreign);
        tv_policaAreaValue_foreign=(TextView)findViewById(R.id.tv_policaAreaValue_foreign);
        tv_divisionValue_foreign=(TextView)findViewById(R.id.tv_divisionValue_foreign);

        Constants.changefonts(tv_id_numberValue, this, "Calibri.ttf");
        Constants.changefonts(tv_nameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_ageValue, this, "Calibri.ttf");
        Constants.changefonts(tv_sexValue, this, "Calibri.ttf");
        Constants.changefonts(tv_addressValue, this, "Calibri.ttf");
        Constants.changefonts(tv_contactValue, this, "Calibri.ttf");
        Constants.changefonts(tv_idNo_value, this, "Calibri.ttf");
        Constants.changefonts(tv_idTypeValue, this, "Calibri.ttf");
        Constants.changefonts(tv_coming_value, this, "Calibri.ttf");
        Constants.changefonts(tv_proceedingValue, this, "Calibri.ttf");
        Constants.changefonts(tv_adultValue, this, "Calibri.ttf");
        Constants.changefonts(tv_childValue, this, "Calibri.ttf");
        Constants.changefonts(tv_hotelNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_hotelCodeValue, this, "Calibri.ttf");
        Constants.changefonts(tv_hotel_ps_value, this, "Calibri.ttf");
        Constants.changefonts(tv_hotelAddressValue, this, "Calibri.ttf");
        Constants.changefonts(tv_hotelroomNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_status_value, this, "Calibri.ttf");
        Constants.changefonts(tv_dateofArrivalValue, this, "Calibri.ttf");
        Constants.changefonts(tv_dateofDepartureValue, this, "Calibri.ttf");
        Constants.changefonts(tv_purposeofVisitValue, this, "Calibri.ttf");
        Constants.changefonts(tv_policaAreaValue, this, "Calibri.ttf");
        Constants.changefonts(tv_divisionValue, this, "Calibri.ttf");

        Constants.changefonts(tv_id_number_foreignValue, this, "Calibri.ttf");
        Constants.changefonts(tv_name_foreignValue, this, "Calibri.ttf");
        Constants.changefonts(tv_date_of_birthValue, this, "Calibri.ttf");
        Constants.changefonts(tv_nationality_value, this, "Calibri.ttf");
        Constants.changefonts(tv_addressValue_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_addressValue_india, this, "Calibri.ttf");
        Constants.changefonts(tv_contact_foreignValue, this, "Calibri.ttf");
        Constants.changefonts(tv_contact_indianValue, this, "Calibri.ttf");
        Constants.changefonts(tv_passportNoValue_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_passport_placeValue, this, "Calibri.ttf");
        Constants.changefonts(tv_passport_issuedateValue, this, "Calibri.ttf");
        Constants.changefonts(tv_passport_validValue, this, "Calibri.ttf");
        Constants.changefonts(tv_vissaNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_visa_Issue_DateValue, this, "Calibri.ttf");
        Constants.changefonts(tv_visa_Valid_TillValue, this, "Calibri.ttf");
        Constants.changefonts(tv_type_of_VisaValue, this, "Calibri.ttf");
        Constants.changefonts(tv_visa_issue_placeValue, this, "Calibri.ttf");
        Constants.changefonts(tv_comingValue_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_proceedingValue_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_dateofArrivalValue_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_dateofDepartureValue_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_hotelNameValue_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_hotel_code_value_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_hotelAddressValue_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_hotel_ps_value_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_hotel_phone_value_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_hotelroomNoValue_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_status_value_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_purposeofVisitValue_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_policaAreaValue_foreign, this, "Calibri.ttf");
        Constants.changefonts(tv_divisionValue_foreign, this, "Calibri.ttf");



        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
        if(VisitorType.equalsIgnoreCase("1")) {
            ll_national.setVisibility(View.VISIBLE);
            ll_foreign.setVisibility(View.GONE);
            Picasso.with(this).load(hotelDetails.getImage().trim()).fit().centerCrop()
                    .placeholder(R.drawable.human3)
                    .error(R.drawable.human3)
                    .into(imageView);
            tv_id_numberValue.setText(hotelDetails.getId());
            tv_nameValue.setText(hotelDetails.getName());
            tv_ageValue.setText(hotelDetails.getAge());
            tv_sexValue.setText(hotelDetails.getSex());
            tv_addressValue.setText(hotelDetails.getAddress());
            tv_contactValue.setText(hotelDetails.getContactNumber());
            tv_idNo_value.setText(hotelDetails.getIdNumber());
            tv_idTypeValue.setText(hotelDetails.getIdtype());
            tv_coming_value.setText(hotelDetails.getCommingFrom());
            tv_proceedingValue.setText(hotelDetails.getProceedingTo());
            tv_adultValue.setText(hotelDetails.getAdult());
            tv_childValue.setText(hotelDetails.getChild());
            tv_hotelCodeValue.setText(hotelDetails.getHotelCode());
            tv_hotel_ps_value.setText(hotelDetails.getHotelPs());
            tv_status_value.setText(hotelDetails.getHotelStatus());
            tv_hotelNameValue.setText(hotelDetails.getHotelName());
            tv_hotelAddressValue.setText(hotelDetails.getHoteladdress());
            tv_hotelroomNoValue.setText(hotelDetails.getHotelRoomNo());
            tv_dateofArrivalValue.setText(hotelDetails.getArrivalDate());
            tv_dateofDepartureValue.setText(hotelDetails.getDepatureDate());
            tv_purposeofVisitValue.setText(hotelDetails.getVisitPurpose());
            tv_policaAreaValue.setText(hotelDetails.getPoliceArea());
            tv_divisionValue.setText(hotelDetails.getDivision());
        }
        else if(VisitorType.equalsIgnoreCase("2")){
            ll_national.setVisibility(View.GONE);
            ll_foreign.setVisibility(View.VISIBLE);
            Picasso.with(this).load(hotelDetails.getImage().trim()).fit().centerCrop()
                    .placeholder(R.drawable.human3)
                    .error(R.drawable.human3)
                    .into(iv_userImage_foreign);

            tv_id_number_foreignValue.setText(hotelDetails.getId());
            tv_name_foreignValue.setText(hotelDetails.getName());
            tv_date_of_birthValue.setText(hotelDetails.getDateOfBirth());
            tv_nationality_value.setText(hotelDetails.getNationality());
            tv_addressValue_foreign.setText(hotelDetails.getAddress_foreign());
            tv_addressValue_india.setText(hotelDetails.getAddress());
            tv_contact_foreignValue.setText(hotelDetails.getContactNumberForeign());
            tv_contact_indianValue.setText(hotelDetails.getContactNumber());
            tv_passportNoValue_foreign.setText(hotelDetails.getPassportNo());
            tv_passport_placeValue.setText(hotelDetails.getPlaceOfIssuePassport());
            tv_passport_issuedateValue.setText(hotelDetails.getDateOfIssuePasport());
            tv_passport_validValue.setText(hotelDetails.getPassportVlidTill());
            tv_vissaNoValue.setText(hotelDetails.getVissaNo());
            tv_visa_Issue_DateValue.setText(hotelDetails.getVissaIsueDate());
            tv_visa_Valid_TillValue.setText(hotelDetails.getVissaValidTill());
            tv_type_of_VisaValue.setText(hotelDetails.getTypeOfVissa());
            tv_visa_issue_placeValue.setText(hotelDetails.getVissaIssuePlace());
            tv_comingValue_foreign.setText(hotelDetails.getCommingFrom());
            tv_proceedingValue_foreign.setText(hotelDetails.getProceedingTo());
            tv_dateofArrivalValue_foreign.setText(hotelDetails.getArrivalDate());
            tv_dateofDepartureValue_foreign.setText(hotelDetails.getDepatureDate());
            tv_hotelNameValue_foreign.setText(hotelDetails.getHotelName());
            tv_hotel_code_value_foreign.setText(hotelDetails.getHotelCode());
            tv_hotelAddressValue_foreign.setText(hotelDetails.getHoteladdress());
            tv_hotel_ps_value_foreign.setText(hotelDetails.getHotelPs());
            tv_hotel_phone_value_foreign.setText(hotelDetails.getHotelphone());
            tv_hotelroomNoValue_foreign.setText(hotelDetails.getHotelRoomNo());
            tv_status_value_foreign.setText(hotelDetails.getHotelStatus());
            tv_purposeofVisitValue_foreign.setText(hotelDetails.getVisitPurpose());
            tv_policaAreaValue_foreign.setText(hotelDetails.getPoliceArea());
            tv_divisionValue_foreign.setText(hotelDetails.getDivision());

        }



    }
    private void showImagePopup(String imageUrl) {

        Dialog imageDialog = new Dialog(this);
        imageDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View inflatedView;

        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        inflatedView = layoutInflater.inflate(R.layout.image_layout, null, false);
        imageDialog.setContentView(inflatedView);

        final ImageView iv_popup = (ImageView) inflatedView.findViewById(R.id.iv_popup);

        ImageLoader.getInstance().displayImage(imageUrl,iv_popup);
        imageDialog.show();
    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        int id=v.getId();
        switch(id){
            case R.id.iv_userImage:
                //Toast.makeText(this, "Image clicked", Toast.LENGTH_SHORT).show();
                showImagePopup(hotelDetails.getImage());
                break;
            case R.id.iv_userImage_foreign:
               // Toast.makeText(this, "Image clicked", Toast.LENGTH_SHORT).show();
                showImagePopup(hotelDetails.getImage());
                break;
        }

    }
}
