package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnItemClickListenerForAllRTAView;
import com.kp.facedetection.model.CrimeReviewDetails;

import java.util.ArrayList;

import me.grantland.widget.AutofitTextView;

/**
 * Created by DAT-165 on 09-06-2017.
 */

public class RTAPSListAdapter extends RecyclerView.Adapter<RTAPSListAdapter.MyViewHolder>{

    Context con;
    ArrayList<CrimeReviewDetails> rtaPSList = new ArrayList<>();
    private OnItemClickListenerForAllRTAView allRTAViewClickListener;

    public RTAPSListAdapter(Context con, ArrayList<CrimeReviewDetails> rtaPSList) {
        this.con = con;
        this.rtaPSList = rtaPSList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rtatab_list_item_layout,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final CrimeReviewDetails crimeDetailsForRTA = rtaPSList.get(position);
        if (crimeDetailsForRTA.getCrimeCategory() != null && !crimeDetailsForRTA.getCrimeCategory().equalsIgnoreCase("")
                && !crimeDetailsForRTA.getCrimeCategory().equalsIgnoreCase("null")) {
            holder.tv_categoryName.setText(crimeDetailsForRTA.getCrimeCategory());
        }
        if (crimeDetailsForRTA.getNonFatalCount() != null && !crimeDetailsForRTA.getNonFatalCount().equalsIgnoreCase("")
                && !crimeDetailsForRTA.getNonFatalCount().equalsIgnoreCase("null") && !crimeDetailsForRTA.getNonFatalCount().equalsIgnoreCase("0")) {
            holder.tv_nonFatalCount.setText(crimeDetailsForRTA.getNonFatalCount());
            //holder.tv_nonFatalCount.setText("5555");
        }
        else{
            holder.tv_nonFatalCount.setVisibility(View.GONE);
        }

        if (crimeDetailsForRTA.getFatalCount() != null && !crimeDetailsForRTA.getFatalCount().equalsIgnoreCase("")
                && !crimeDetailsForRTA.getFatalCount().equalsIgnoreCase("null") && !crimeDetailsForRTA.getFatalCount().equalsIgnoreCase("0")) {
            holder.tv_fatalCount.setText(crimeDetailsForRTA.getFatalCount());
        }
        else{
            holder.tv_fatalCount.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return rtaPSList == null ? 0 : rtaPSList.size();
    }

    public void setClickListener(OnItemClickListenerForAllRTAView allRTAViewClickListener) {
        this.allRTAViewClickListener = allRTAViewClickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView tv_categoryName;
        public AutofitTextView tv_nonFatalCount, tv_fatalCount;
        public MyViewHolder(View itemView) {
            super(itemView);
            tv_categoryName = (TextView)itemView.findViewById(R.id.tv_categoryName);
            tv_fatalCount = (AutofitTextView)itemView.findViewById(R.id.tv_fatalCount);
            tv_nonFatalCount = (AutofitTextView)itemView.findViewById(R.id.tv_nonFatalCount);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (allRTAViewClickListener != null){
                allRTAViewClickListener.onClick(view,getAdapterPosition());
            }
        }
    }
}
