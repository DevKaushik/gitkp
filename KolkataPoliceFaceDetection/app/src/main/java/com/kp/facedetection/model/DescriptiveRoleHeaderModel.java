package com.kp.facedetection.model;

/**
 * Created by DAT-Asset-128 on 11-10-2017.
 */

public class DescriptiveRoleHeaderModel {
    public String heading;

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String status;

}
