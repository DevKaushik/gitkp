package com.kp.facedetection.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;


import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Kaushik on 16-09-2016.
 */
public class AppInfoService extends IntentService {

    private static final String TAG = "AppInfoService";
    private String responsebody="";

    public AppInfoService() {
        super(TAG);
    }


    @Override
    protected void onHandleIntent(Intent intent) {

        System.out.println("App Info Service Start start");
        NetworkOperation();

    }

    private void NetworkOperation(){

        System.out.println("Update app version operation start");

        try {

            String update_appVersion = Constants.API_URL + Constants.METHOD_UPDATE_APPVERSION ;

            URL url = new URL(update_appVersion);

            JSONObject postDataParams = new JSONObject();
            postDataParams.put("user_id", Utility.getUserInfo(this).getUserId());
            postDataParams.put("imei_no", Constants.device_IMEI_No);
            postDataParams.put("version", Utility.getAppVersion(this));
            Log.e("params", postDataParams.toString());

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(30000 /* milliseconds */);
            conn.setConnectTimeout(30000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();

            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {

                String readStream = readStream(conn.getInputStream());
                System.out.println("Response "+readStream);

                parseAppInfoResponse(readStream);

            }
            else {
            }
        }
        catch(Exception e){

            e.printStackTrace();

        }

    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }


    public String readStream(InputStream in) {

        StringBuilder sb = new StringBuilder();

        try {
            Reader reader  = new InputStreamReader(in);
            int data = reader.read();
            while (data != -1) {
                char theChar = (char) data;
                data = reader.read();
                sb.append(theChar);

            }

            reader.close();
        }
        catch (Exception e) {

        }
        return sb.toString();
    }

    private void parseAppInfoResponse(String response) {
        Log.e("Update App Response", "Response " + response);

    }
}
