package com.kp.facedetection.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 25-07-2016.
 */
public class CaseSearchFIRDetails implements Serializable {

    private String psName="";
    private String ocComment="";
    private String acComment="";
    private String dcComment="";
    private String caseNo="";
    private String io="";
    private String OCNAME="";
    private String ACNAME="";
    private String DCNAME="";
    private String ioPhone="";
    private String ocPhone="";
    private String acPhone="";
    private String dcPhone="";
    private String complainant="";
    private String complainant_address="";
    private String gdeNo="";
    private String firYr="";
    private String gdeDate="";
    private String briefFact="";
    private String caseDate="";
    private String firStatus="";
    private String gr_c_no="";
    private String statusDate="";
    private String courtName="";
    private String underSection="";
    private String caseYr="";
    private String category="";
    private String flag="";
    private String pdf_url="";
    private String pmreport_pdf_url = "";
    private String poLat = "";
    private String poLong = "";
    private String psCode = "";
    private String pm_no = "";
    private String pm_year = "";
    private List<CaseSearchFIRAllAccused> caseSearchFIRAllAccusedList=new ArrayList<CaseSearchFIRAllAccused>();
    private List<CaseSearchFIRAllArrested> caseSearchFIRAllArrestedList=new ArrayList<CaseSearchFIRAllArrested>();
    private List<CaseSearchFIRChargeSheet> caseSearchFIRChargeSheetList=new ArrayList<CaseSearchFIRChargeSheet>();
    private List<CaseSearchFIRWarrant> caseSearchFIRWarrantList=new ArrayList<CaseSearchFIRWarrant>();
    private List<CaseTransferDetails> caseSearchCaseTransferList=new ArrayList<CaseTransferDetails>();

    private ArrayList<CriminalDetails> allAccusedDataList = new ArrayList<CriminalDetails>();
    private ArrayList<CriminalDetails> allArrestedDataList = new ArrayList<CriminalDetails>();

    public ArrayList<CriminalDetails> getAllAccusedDataList() {
        return allAccusedDataList;
    }

    public void setAllAccusedDataList(ArrayList<CriminalDetails> allAccusedDataList) {
        this.allAccusedDataList = allAccusedDataList;
    }

    public ArrayList<CriminalDetails> getAllArrestedDataList() {
        return allArrestedDataList;
    }

    public void setAllArrestedDataList(ArrayList<CriminalDetails> allArrestedDataList) {
        this.allArrestedDataList = allArrestedDataList;
    }
    public String getIoPhone() {
        return ioPhone;
    }
    public String getOcPhone() {
        return ocPhone;
    }
    public String getAcPhone() {
        return acPhone;
    }
    public String getDcPhone() {
        return dcPhone;
    }

    public void setIoPhone(String ioPhone) {
        this.ioPhone = ioPhone;
    }
    public void setOcPhone(String ocPhone) {
        this.ocPhone = ocPhone;
    }
    public void setAcPhone(String acPhone) {
        this.acPhone = acPhone;
    }
    public void setDcPhone(String dcPhone) {
        this.dcPhone = dcPhone;
    }

    public String getPm_no() {
        return pm_no;
    }

    public void setPm_no(String pm_no) {
        this.pm_no = pm_no;
    }

    public String getPm_year() {
        return pm_year;
    }

    public void setPm_year(String pm_year) {
        this.pm_year = pm_year;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getPsName() {
        return psName;
    }

    public void setPsName(String psName) {
        this.psName = psName;
    }

    public String getOcComment() {
        return ocComment;
    }

    public void setOcComment(String ocComment) {
        this.ocComment = ocComment;
    }

    public String getAcComment() {
        return acComment;
    }

    public void setAcComment(String acComment) {
        this.acComment = acComment;
    }

    public String getDcComment() {
        return dcComment;
    }

    public void setDcComment(String dcComment) {
        this.dcComment = dcComment;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getIo() {
        return io;
    }

    public String getOc() {
        return OCNAME;
    }
    public String getAc(){
        return ACNAME;
    }

    public String getDc() {
        return DCNAME;
    }

    public void setIo(String io) {
        this.io = io;
    }

    public void setOc (String OCNAME){
        this.OCNAME = OCNAME;
    }
    public void setAc (String ACNAME) {
        this.ACNAME = ACNAME;
    }
    public void setDc (String DCNAME) {
        this.DCNAME = DCNAME;
    }

    public String getComplainant() {
        return complainant;
    }

    public void setComplainant(String complainant) {
        this.complainant = complainant;
    }

    public String getGdeNo() {
        return gdeNo;
    }

    public void setGdeNo(String gdeNo) {
        this.gdeNo = gdeNo;
    }

    public String getFirYr() {
        return firYr;
    }

    public void setFirYr(String firYr) {
        this.firYr = firYr;
    }

    public String getGdeDate() {
        return gdeDate;
    }

    public void setGdeDate(String gdeDate) {
        this.gdeDate = gdeDate;
    }

    public String getBriefFact() {
        return briefFact;
    }

    public void setBriefFact(String briefFact) {
        this.briefFact = briefFact;
    }

    public String getCaseDate() {
        return caseDate;
    }

    public void setCaseDate(String caseDate) {
        this.caseDate = caseDate;
    }

    public String getFirStatus() {
        return firStatus;
    }

    public void setFirStatus(String firStatus) {
        this.firStatus = firStatus;
    }

    public String getGr_c_no() {
        return gr_c_no;
    }

    public void setGr_c_no(String gr_c_no) {
        this.gr_c_no = gr_c_no;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getCourtName() {
        return courtName;
    }

    public void setCourtName(String courtName) {
        this.courtName = courtName;
    }

    public String getUnderSection() {
        return underSection;
    }

    public void setUnderSection(String underSection) {
        this.underSection = underSection;
    }

    public String getCaseYr() {
        return caseYr;
    }

    public void setCaseYr(String caseYr) {
        this.caseYr = caseYr;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPdf_url() {
        return pdf_url;
    }

    public void setPdf_url(String pdf_url) {
        this.pdf_url = pdf_url;
    }

    public String getPmreport_pdf_url() {
        return pmreport_pdf_url;
    }

    public void setPmreport_pdf_url(String pmreport_pdf_url) {
        this.pmreport_pdf_url = pmreport_pdf_url;
    }

    public List<CaseSearchFIRAllAccused> getCaseSearchFIRAllAccusedList() {
        return caseSearchFIRAllAccusedList;
    }

    public void setCaseSearchFIRAllAccusedList(CaseSearchFIRAllAccused obj) {
        caseSearchFIRAllAccusedList.add(obj);
    }

    public List<CaseSearchFIRAllArrested> getCaseSearchFIRAllArrestedList() {
        return caseSearchFIRAllArrestedList;
    }

    public void setCaseSearchFIRAllArrestedList(CaseSearchFIRAllArrested obj) {
        caseSearchFIRAllArrestedList.add(obj);
    }

    public List<CaseSearchFIRChargeSheet> getCaseSearchFIRChargeSheetList() {
        return caseSearchFIRChargeSheetList;
    }

    public void setCaseSearchFIRChargeSheetList(CaseSearchFIRChargeSheet obj) {
        caseSearchFIRChargeSheetList.add(obj);
    }

    public List<CaseSearchFIRWarrant> getCaseSearchFIRWarrantList() {
        return caseSearchFIRWarrantList;
    }

    public void setCaseSearchFIRWarrantList(CaseSearchFIRWarrant obj) {
        caseSearchFIRWarrantList.add(obj);
    }

    public List<CaseTransferDetails> getCaseSearchCaseTransferList() {
        return caseSearchCaseTransferList;
    }

    public void setCaseSearchCaseTransferList(CaseTransferDetails obj) {
        caseSearchCaseTransferList.add(obj);
    }

    public String getPoLat() {
        return poLat;
    }

    public void setPoLat(String poLat) {
        this.poLat = poLat;
    }

    public String getPoLong() {
        return poLong;
    }

    public void setPoLong(String poLong) {
        this.poLong = poLong;
    }

    public String getPsCode() {
        return psCode;
    }

    public void setPsCode(String psCode) {
        this.psCode = psCode;
    }

    public String getComplainant_address() {
        return complainant_address;
    }

    public void setComplainant_address(String complainant_address) {
        this.complainant_address = complainant_address;
    }
}
