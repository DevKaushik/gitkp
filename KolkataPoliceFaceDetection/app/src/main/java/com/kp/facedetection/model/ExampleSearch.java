package com.kp.facedetection.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 12-06-2018.
 */

public class ExampleSearch {
    @JsonProperty("status")
    private String status;
    @JsonProperty("message")
    private String message;
    @JsonProperty("IO")
    private List<IO> iO = null;
    @JsonProperty("OC")
    private List<OC> oC = null;
    @JsonProperty("AC")
    private List<AC> aC = null;
    @JsonProperty("DC")
    private List<DC> dC = null;
    @JsonProperty("JTCP")
    private List<JTCP> jtCp = null;
    @JsonProperty("ADDLCP")
    private List<ADDLCP> aDDLCP = null;
    @JsonProperty("CP")
    private List<CP> cP = null;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("IO")
    public List<IO> getIO() {
        return iO;
    }

    @JsonProperty("IO")
    public void setIO(List<IO> iO) {
        this.iO = iO;
    }

    @JsonProperty("OC")
    public List<OC> getOC() {
        return oC;
    }

    @JsonProperty("OC")
    public void setOC(List<OC> oC) {
        this.oC = oC;
    }

    @JsonProperty("AC")
    public List<AC> getAC() {
        return aC;
    }

    @JsonProperty("AC")
    public void setAC(List<AC> aC) {
        this.aC = aC;
    }

    @JsonProperty("DC")
    public List<DC> getDC() {
        return dC;
    }

    @JsonProperty("DC")
    public void setDC(List<DC> dC) {
        this.dC = dC;
    }
    @JsonProperty("JTCP")
    public List<JTCP> getJtCp() {
        return jtCp;
    }
    @JsonProperty("JTCP")
    public void setJtCp(List<JTCP> jtCp) {
        this.jtCp = jtCp;
    }

    @JsonProperty("ADDLCP")
    public List<ADDLCP> getADDLCP() {
        return aDDLCP;
    }

    @JsonProperty("ADDLCP")
    public void setADDLCP(List<ADDLCP> aDDLCP) {
        this.aDDLCP = aDDLCP;
    }
    @JsonProperty("CP")
    public List<CP> getCP() {
        return cP;
    }

    @JsonProperty("CP")
    public void setCP(List<CP> cP) {
        this.cP = cP;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
