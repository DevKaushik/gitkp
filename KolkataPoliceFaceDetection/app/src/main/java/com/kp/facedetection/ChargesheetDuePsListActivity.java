package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.adapter.ChargesheetDueListAdapter;
import com.kp.facedetection.interfaces.OnItemClickListenerForChargesheetDue;
import com.kp.facedetection.model.ChargesheetDueDetails;
import com.kp.facedetection.model.CrimeReviewDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class ChargesheetDuePsListActivity extends BaseActivity  implements OnItemClickListenerForChargesheetDue, View.OnClickListener, Observer {
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String selectedDate="";
    private String selected_div="";
    private String selected_crime="";
    RecyclerView chargeSheetRecyclerView;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<ChargesheetDueDetails> chargesheetDueDetailsArrayList =new ArrayList<>();
    ChargesheetDueListAdapter chargesheetDueListAdapter;
    String role="";
    String from="PS";

    TextView chargesheetNotificationCount,no_record;
    String fromPage="";
    String user_id="";
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chargesheet_due_ps_list);
        ObservableObject.getInstance().addObserver(this);
        fromPage = getIntent().getStringExtra("FROM_PAGE");
        setToolBar();
        initView();
    }
    public void setToolBar(){
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
            role = Utility.getUserInfo(this).getUserDesignation();
            role="OC";

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }
    public void initView() {
        if(fromPage.equalsIgnoreCase("DashboardActivity")) {
            selectedDate = getIntent().getExtras().getString("SELECTED_DATE");
            selected_div = getIntent().getExtras().getString("SELECTED_DIV");
            selected_crime = getIntent().getExtras().getString("SELECTED_CRIME");
        }
        else if(fromPage.equalsIgnoreCase("BaseActivity")){
            user_id= getIntent().getExtras().getString("SELECTED_USER_ID");
            selected_crime = getIntent().getExtras().getString("SELECTED_CRIME_CS");
        }
        no_record=(TextView)findViewById(R.id.tv_no_record);
        chargeSheetRecyclerView=(RecyclerView)findViewById(R.id.rv_chargesheet_due);
        layoutManager=new LinearLayoutManager(this);
        chargeSheetRecyclerView.setLayoutManager(layoutManager);
        chargeSheetRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));

        chargesheetDueListAdapter=new ChargesheetDueListAdapter(this,chargesheetDueDetailsArrayList,from);
        chargeSheetRecyclerView.setAdapter(chargesheetDueListAdapter);
        chargesheetDueListAdapter.setOnClickListener(this);
        getAllchargeSheetPs();
    }
    public void  getAllchargeSheetPs() {
        TaskManager taskManager = new TaskManager(this);
        if(fromPage.equalsIgnoreCase("DashboardActivity")) {
            taskManager.setMethod(Constants.METHOD_CHARGESHEET_PS_LIST);
            taskManager.setChargesheetDuePs(true);
            String[] keys = {"currDate", "div","user_id"};
            String[] values = {selectedDate.trim(), selected_div.trim(),Utility.getUserInfo(this).getUserId()};
            taskManager.doStartTask(keys, values, true);
        }
        else if(fromPage.equalsIgnoreCase("BaseActivity")){
            taskManager.setMethod(Constants.METHOD_CHARGESHEET_PS_LIST_NOTIFICATION);
            taskManager.setChargesheetDuePs(true);
            String[] keys = {"user_id"};
            String[] values = {user_id.trim()};
            taskManager.doStartTask(keys, values, true);

        }

    }
    public void parseChargeSheetDuePsResult(String response) {
        //System.out.println("parseWarrantExecPSResult" + response);

        if (response != null && !response.equals("")) {

            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.opt("status").toString().equalsIgnoreCase("1")) {
                    JSONArray result = jobj.optJSONArray("result");
                    parsePSResponse(result);
                } else {
                   // Utility.showAlertDialog(ChargesheetDuePsListActivity.this, Constants.ERROR_DETAILS_TITLE, jobj.optString("message"), false);
                    no_record.setVisibility(View.VISIBLE);
                    no_record.setText("No PS found");
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(ChargesheetDuePsListActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }
    private void parsePSResponse(JSONArray resultArray) {


        for (int i = 0; i < resultArray.length(); i++) {
            ChargesheetDueDetails psDetails = new ChargesheetDueDetails();
            JSONObject object = resultArray.optJSONObject(i);
            psDetails.setPsCode(object.optString("PSCODE"));
            psDetails.setPsName(object.optString("PSNAME"));
            psDetails.setNoOfCase(object.optString("COUNT"));

            chargesheetDueDetailsArrayList.add(psDetails);
        }
        chargesheetDueListAdapter.notifyDataSetChanged();
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.charseet_due_notification:
                if(fromPage.equalsIgnoreCase("DashboardActivity")) {

                    getChargesheetdetailsAsperRole(this);
                   // finish();
                }
        }
    }

    @Override
    public void onClickChargesheetDue(View view, int position) {


            if (!Constants.internetOnline(this)) {
                Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
                return;
            }
        if(fromPage.equalsIgnoreCase("DashboardActivity")) {
            Intent in = new Intent(ChargesheetDuePsListActivity.this, ChargesheetDueCaseListActivity.class);
            in.putExtra("FROM_PAGE",fromPage);
            in.putExtra("SELECTED_DATE_CS", selectedDate);
            in.putExtra("SELECTED_PS_NAME_CS", chargesheetDueDetailsArrayList.get(position).getPsName());
            in.putExtra("SELECTED_PS_CS", chargesheetDueDetailsArrayList.get(position).getPsCode());
            in.putExtra("SELECTED_CRIME_CS", selected_crime);
          //  in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
           // finish();
            startActivity(in);
        }
        else if(fromPage.equalsIgnoreCase("BaseActivity")){
            Intent in = new Intent(ChargesheetDuePsListActivity.this, ChargesheetDueCaseListActivity.class);
            in.putExtra("FROM_PAGE",fromPage);
            in.putExtra("SELECTED_USER_ID",user_id);
            in.putExtra("SELECTED_CRIME_CS", selected_crime);
           // finish();
            startActivity(in);

        }


    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);

                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }
    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
