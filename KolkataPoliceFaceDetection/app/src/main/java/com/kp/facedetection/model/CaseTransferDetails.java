package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 28-01-2017.
 */
public class CaseTransferDetails implements Serializable{

    private String fromPS= "";
    private String toPS = "";
    private String fromIoName = "";
    private String toIoName = "";
    private String toUnit = "";
    private String caseNo = "";
    private String caseYr = "";
    private String tranferDt = "";
    private String remarks = "";
    private String psName = "";


    public String getFromPS() {
        return fromPS;
    }

    public void setFromPS(String fromPS) {
        this.fromPS = fromPS;
    }

    public String getToPS() {
        return toPS;
    }

    public void setToPS(String toPS) {
        this.toPS = toPS;
    }

    public String getFromIoName() {
        return fromIoName;
    }

    public void setFromIoName(String fromIoName) {
        this.fromIoName = fromIoName;
    }

    public String getToIoName() {
        return toIoName;
    }

    public void setToIoName(String toIoName) {
        this.toIoName = toIoName;
    }

    public String getToUnit() {
        return toUnit;
    }

    public void setToUnit(String toUnit) {
        this.toUnit = toUnit;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getCaseYr() {
        return caseYr;
    }

    public void setCaseYr(String caseYr) {
        this.caseYr = caseYr;
    }

    public String getTranferDt() {
        return tranferDt;
    }

    public void setTranferDt(String tranferDt) {
        this.tranferDt = tranferDt;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getPsName() {
        return psName;
    }

    public void setPsName(String psName) {
        this.psName = psName;
    }
}
