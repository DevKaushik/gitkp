package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.adapter.RecyclerAdapterForCategory;
import com.kp.facedetection.interfaces.OnItemClickListenerForCrimeReview;
import com.kp.facedetection.model.CrimeReviewDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class FIRTabFromDashboardActivity extends BaseActivity implements OnItemClickListenerForCrimeReview, View.OnClickListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String selectedDate = "";
    private String selectedDiv = "";
    private String selectedPs = "";
    private String selectedCrime = "";

    private RecyclerView recycler_categoryList;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<CrimeReviewDetails> categoryList = new ArrayList<>();
    private RecyclerAdapterForCategory categoryListAdapter;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firtab_from_dashboard_layout);
        ObservableObject.getInstance().addObserver(this);

        setToolBar();
        initViews();
    }


    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }


    private void initViews() {

        selectedDate = getIntent().getExtras().getString("SELECTED_DATE");
        selectedDiv = getIntent().getExtras().getString("SELECTED_DIV");
        selectedPs = getIntent().getExtras().getString("SELECTED_PS");
        selectedCrime = getIntent().getExtras().getString("SELECTED_CRIME");

        recycler_categoryList = (RecyclerView) findViewById(R.id.recycler_psList);
        mLayoutManager = new LinearLayoutManager(this);
        recycler_categoryList.setLayoutManager(mLayoutManager);
        recycler_categoryList.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));

        allFIRCategoryListCall();
        categoryListAdapter = new RecyclerAdapterForCategory(this, categoryList);
        recycler_categoryList.setAdapter(categoryListAdapter);
        categoryListAdapter.setClickListener(this);
    }


    private void    allFIRCategoryListCall() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_FIR_VIEW_BY_CATEGORY);
        taskManager.setAllFirViewByCategory(true);

        String[] keys = {"currDate","div","ps","category","user_id"};
        String[] values = {selectedDate.trim(), selectedDiv.trim(), selectedPs.trim(), selectedCrime.trim(),Utility.getUserInfo(this).getUserId()};
        taskManager.doStartTask(keys, values, true);
    }

    public void parseAllFirViewSearchResult(String response) {
        //System.out.println("parseAllFirViewSearchResult:" + response);
        try {
            JSONObject jObj = new JSONObject(response);
            if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                JSONArray resultArray = jObj.getJSONArray("result");
                parseAllFirPSResponse(resultArray);
            } else {
                Utility.showAlertDialog(FIRTabFromDashboardActivity.this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_NO_LOCATION_FOUND_MSG, false);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Utility.showAlertDialog(FIRTabFromDashboardActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
        }
    }

    private void parseAllFirPSResponse(JSONArray resultArray) {
        categoryList.clear();
        for (int i = 0; i < resultArray.length(); i++) {

            CrimeReviewDetails crimeReviewDetails = new CrimeReviewDetails(Parcel.obtain());
            try {
                JSONObject jObj = resultArray.getJSONObject(i);
                if(jObj.optString("CATEGORY") != null && !jObj.optString("CATEGORY").equalsIgnoreCase("") && !jObj.optString("CATEGORY").equalsIgnoreCase("null")
                        && !jObj.optString("CATEGORY").equalsIgnoreCase("0")){
                    crimeReviewDetails.setCrimeCategory(jObj.optString("CATEGORY"));
                    if(jObj.optString("COUNT") != null && !jObj.optString("COUNT").equalsIgnoreCase("") && !jObj.optString("COUNT").equalsIgnoreCase("null")){
                        crimeReviewDetails.setCountOfCrime(jObj.optString("COUNT"));
                    }
                    categoryList.add(crimeReviewDetails);
                }

            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
        categoryListAdapter.notifyDataSetChanged();
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
           /* case R.id.menu_send_push:
                //tabHost.setCurrentTab(2);
                //startActivity(new Intent(this, SendPushActivity.class));
                //	pushFragments("Push", new SendPushActivity(), true, false);
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View view, int position) {
        final CrimeReviewDetails crimeReviewDetails = categoryList.get(position);
        String category = crimeReviewDetails.getCrimeCategory().toString().trim();

        Intent intent = new Intent(FIRTabFromDashboardActivity.this, FIRPSListActivity.class);
        intent.putExtra("FROM_DATE",selectedDate);
        intent.putExtra("CATEGORY",category);
        intent.putExtra("SELECTED_DIV",selectedDiv);
        intent.putExtra("SELECTED_PS",selectedPs);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
