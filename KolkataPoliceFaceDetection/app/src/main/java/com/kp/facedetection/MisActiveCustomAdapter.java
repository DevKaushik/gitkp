package com.kp.facedetection;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.kp.facedetection.utility.Utility;

import java.util.ArrayList;

public class MisActiveCustomAdapter extends ArrayAdapter<MisActiveUsersDataModel> implements View.OnClickListener,Filterable {

    private ArrayList<MisActiveUsersDataModel> MisActiveUsersDataModel;
    Context mContext;

    private static class ViewHolder {
        TextView UserName;
        TextView UserRank;
        TextView UserPs;
        TextView Appversion;
        TextView LastLoginDate;
        TextView LastLoginTime;
        ImageView info;
    }

    public MisActiveCustomAdapter(ArrayList<MisActiveUsersDataModel> data, Context context) {
        super(context, R.layout.activeusers_row_item, data);
        this.MisActiveUsersDataModel = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {
        int position=(Integer) v.getTag();
        Object object= getItem(position);
        MisActiveUsersDataModel MisActiveUsersDataModel=(MisActiveUsersDataModel)object;

        switch (v.getId())
        {

            case R.id.item_info:

                Snackbar.make(v, "Release date " +MisActiveUsersDataModel.getLogInTime(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();

                break;
        }
    }
    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        MisActiveUsersDataModel MisActiveUsersDataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        MisActiveCustomAdapter.ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;


        if (convertView == null) {

            viewHolder = new MisActiveCustomAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.activeusers_row_item, parent, false);
            viewHolder.UserName = (TextView) convertView.findViewById(R.id.name);
            viewHolder.UserRank = (TextView) convertView.findViewById(R.id.rank);
            viewHolder.UserPs = (TextView) convertView.findViewById(R.id.psname);
            viewHolder.Appversion = (TextView) convertView.findViewById(R.id.app_version);
            viewHolder.LastLoginDate = (TextView) convertView.findViewById(R.id.last_login_date);
            viewHolder.LastLoginTime = (TextView) convertView.findViewById(R.id.loginTime);
            viewHolder.info = (ImageView) convertView.findViewById(R.id.item_info);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MisActiveCustomAdapter.ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.UserName.setText("Name: "+ MisActiveUsersDataModel.getName());
        viewHolder.UserRank.setText("Rank: "+MisActiveUsersDataModel.getRank());
        viewHolder.UserPs.setText("Posting: "+MisActiveUsersDataModel.getPosting());

        String CURVER = MisActiveUsersDataModel.getCurVer();
        if (!MisActiveUsersDataModel.getAppversion().equalsIgnoreCase(CURVER))
        {
            viewHolder.Appversion.setText("App Version: "+MisActiveUsersDataModel.getAppversion());
            viewHolder.Appversion.setTextColor(Color.parseColor("#ef0e0e"));
        }else {
            viewHolder.Appversion.setText("App Version: "+MisActiveUsersDataModel.getAppversion());
            viewHolder.Appversion.setTextColor(Color.parseColor("#000000"));
        }

        viewHolder.LastLoginDate.setText("Last Login Date: "+MisActiveUsersDataModel.getLastlogindate());
        viewHolder.LastLoginTime.setText("Last Login Time: "+MisActiveUsersDataModel.getLogInTime());
        viewHolder.info.setOnClickListener(this);
        viewHolder.info.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }

}
