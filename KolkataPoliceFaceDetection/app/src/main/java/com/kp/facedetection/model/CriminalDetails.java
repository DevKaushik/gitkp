package com.kp.facedetection.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CriminalDetails implements Serializable {

    /**
     *
     */
    // friendDetails,otherContsct,associateDetails,
    private static final long serialVersionUID = 1L;
    String id="", pictureUrl="",
            aliasName="", presentAddress="", age="";
    private String ps="";
    private String caseNo="";
    private String nameAccused="";
    private String addressAccused="";
    private String sexAccused="";
    private String fatherAccused="";
    private String frChgno="";
    private String chgyear="";
    private String scStObc="";
    private String occupation="";
    private String provCrmNo="";
    private String regCrmNo="";
    private String dtArrest="";
    private String dtReleaseBail="";
    private String dtForwardCourt="";
    private String actsSections="";
    private String namesSureties="";
    private String addressSureties="";
    private String prevConvcCaseref="";
    private String status="";
    private String suspicionAppvd="";
    private String caseYr="";
    private String slno="";
    private String fircat="";
    private String fatherHusband="";
    private String crsgeneralid="";
    private String fadd="";
    private String foccu="";
    private String birthyr="";
    private String heightFeet="";
    private String heightInch="";
    private String psRoadPsplace="";
    private String pspsKpj="";
    private String pspsOut="";
    private String psdist="";
    private String psdistOut="";
    private String pspin="";
    private String psstate="";
    private String prroadPreplace="";
    private String prpsKpj="";
    private String prpsOut="";
    private String prdist="";
    private String prdistOut="";
    private String prpin="";
    private String prstate="";
    private String aClass="";
    private String subclass="";
    private String property="";
    private String transport="";
    private String ground="";
    private String hsno="";
    private String proddate="";
    private String beard="";
    private String birthmark="";
    private String built="";
    private String burnmark="";
    private String complexion="";
    private String cutmark="";
    private String deformity="";
    private String ear="";
    private String eye="";
    private String eyebrow="";
    private String face="";
    private String forehead="";
    private String hair="";
    private String mole="";
    private String moustache="";
    private String nose="";
    private String scarmark="";
    private String tattoomark="";
    private String wartMark="";
    private String division="";
    private String crimeno="";
    private String occurno="";
    private String target="";
    private String identifier="";
    private String roughsec="";
    private String roughpart="";
    private String photono="";
    private String ttcriminallid="";
    private String flag="";
    private String trnid="";
    private String ps_code="";
    private String wa_year="";
    private String waslno="";
    private String crime_no="";
    private String crime_year="";
    private String case_no="";
    private String case_year="";
    private String sl_no="";
    private String criminal_alias="";

    /*Modified Data latest update */
    private String fatherName="";
    private String dob_age="";
    private String sex="";
    private String nationality="";
    private String religion="";

    private String prov_crm_no_for_image="";
    private String prov_crm_no="";

    public String getProv_crm_no() {
        return prov_crm_no;
    }

    public void setProv_crm_no(String prov_crm_no) {
        this.prov_crm_no = prov_crm_no;
    }

    public String getProv_crm_no_for_image() {
        return prov_crm_no_for_image;
    }

    public void setProv_crm_no_for_image(String prov_crm_no_for_image) {
        this.prov_crm_no_for_image = prov_crm_no_for_image;
    }

    public String getCriminal_alias() {
        return criminal_alias;
    }

    public void setCriminal_alias(String criminal_alias) {
        this.criminal_alias = criminal_alias;
    }

    public String getCrs_generalID() {
        return crs_generalID;
    }

    public void setCrs_generalID(String crs_generalID) {
        this.crs_generalID = crs_generalID;
    }

    private String crs_generalID="";

    private List<String> picture_list = new ArrayList<>();

    private List<String> briefMatch_list = new ArrayList<>();


    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getDob_age() {
        return dob_age;
    }

    public void setDob_age(String dob_age) {
        this.dob_age = dob_age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }


    public String getSl_no() {
        return sl_no;
    }

    public void setSl_no(String sl_no) {
        this.sl_no = sl_no;
    }

    public String getCase_year() {
        return case_year;
    }

    public void setCase_year(String case_year) {
        this.case_year = case_year;
    }

    public String getCase_no() {
        return case_no;
    }

    public void setCase_no(String case_no) {
        this.case_no = case_no;
    }

    public String getCrime_year() {
        return crime_year;
    }

    public void setCrime_year(String crime_year) {
        this.crime_year = crime_year;
    }

    public String getCrime_no() {
        return crime_no;
    }

    public void setCrime_no(String crime_no) {
        this.crime_no = crime_no;
    }


    public List<String> getPicture_list() {
        return picture_list;
    }

    public void setPicture_list(List<String> picture_list) {
        this.picture_list = picture_list;
    }


    public List<String> getBriefMatch_list() {
        return briefMatch_list;
    }

    public void setBriefMatch_list(List<String> briefMatch_list) {
        this.briefMatch_list = briefMatch_list;
    }


    public String getWaslno() {
        return waslno;
    }

    public void setWaslno(String waslno) {
        this.waslno = waslno;
    }


    public String getWa_year() {
        return wa_year;
    }

    public void setWa_year(String wa_year) {
        this.wa_year = wa_year;
    }


    public String getPs_code() {
        return ps_code;
    }

    public void setPs_code(String ps_code) {
        this.ps_code = ps_code;
    }



    public String getTrnid() {
        return trnid;
    }

    public void setTrnid(String trnid) {
        this.trnid = trnid;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }




    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }


    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }




    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }


    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }


    public void setPs(String ps) {
        this.ps = ps;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public void setNameAccused(String nameAccused) {
        this.nameAccused = nameAccused;
    }

    public void setAddressAccused(String addressAccused) {
        this.addressAccused = addressAccused;
    }

    public void setSexAccused(String sexAccused) {
        this.sexAccused = sexAccused;
    }

    public void setFatherAccused(String fatherAccused) {
        this.fatherAccused = fatherAccused;
    }

    public void setFrChgno(String frChgno) {
        this.frChgno = frChgno;
    }

    public void setChgyear(String chgyear) {
        this.chgyear = chgyear;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public void setScStObc(String scStObc) {
        this.scStObc = scStObc;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public void setProvCrmNo(String provCrmNo) {
        this.provCrmNo = provCrmNo;
    }

    public void setRegCrmNo(String regCrmNo) {
        this.regCrmNo = regCrmNo;
    }

    public void setDtArrest(String dtArrest) {
        this.dtArrest = dtArrest;
    }

    public void setDtReleaseBail(String dtReleaseBail) {
        this.dtReleaseBail = dtReleaseBail;
    }

    public void setDtForwardCourt(String dtForwardCourt) {
        this.dtForwardCourt = dtForwardCourt;
    }

    public void setActsSections(String actsSections) {
        this.actsSections = actsSections;
    }

    public void setNamesSureties(String namesSureties) {
        this.namesSureties = namesSureties;
    }

    public void setAddressSureties(String addressSureties) {
        this.addressSureties = addressSureties;
    }

    public void setPrevConvcCaseref(String prevConvcCaseref) {
        this.prevConvcCaseref = prevConvcCaseref;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSuspicionAppvd(String suspicionAppvd) {
        this.suspicionAppvd = suspicionAppvd;
    }

    public void setCaseYr(String caseYr) {
        this.caseYr = caseYr;
    }

    public void setSlno(String slno) {
        this.slno = slno;
    }

    public void setFircat(String fircat) {
        this.fircat = fircat;
    }

    public void setFatherHusband(String fatherHusband) {
        this.fatherHusband = fatherHusband;
    }

    public void setCrsgeneralid(String crsgeneralid) {
        this.crsgeneralid = crsgeneralid;
    }

    public void setFadd(String fadd) {
        this.fadd = fadd;
    }

    public void setFoccu(String foccu) {
        this.foccu = foccu;
    }

    public void setBirthyr(String birthyr) {
        this.birthyr = birthyr;
    }

    public void setHeightFeet(String heightFeet) {
        this.heightFeet = heightFeet;
    }

    public void setHeightInch(String heightInch) {
        this.heightInch = heightInch;
    }

    public void setPsRoadPsplace(String psRoadPsplace) {
        this.psRoadPsplace = psRoadPsplace;
    }

    public void setPspsKpj(String pspsKpj) {
        this.pspsKpj = pspsKpj;
    }

    public void setPspsOut(String pspsOut) {
        this.pspsOut = pspsOut;
    }

    public void setPsdist(String psdist) {
        this.psdist = psdist;
    }

    public void setPsdistOut(String psdistOut) {
        this.psdistOut = psdistOut;
    }

    public void setPspin(String pspin) {
        this.pspin = pspin;
    }

    public void setPsstate(String psstate) {
        this.psstate = psstate;
    }

    public void setPrroadPreplace(String prroadPreplace) {
        this.prroadPreplace = prroadPreplace;
    }

    public void setPrpsKpj(String prpsKpj) {
        this.prpsKpj = prpsKpj;
    }

    public void setPrpsOut(String prpsOut) {
        this.prpsOut = prpsOut;
    }

    public void setPrdist(String prdist) {
        this.prdist = prdist;
    }

    public void setPrdistOut(String prdistOut) {
        this.prdistOut = prdistOut;
    }

    public void setPrpin(String prpin) {
        this.prpin = prpin;
    }

    public void setPrstate(String prstate) {
        this.prstate = prstate;
    }

    public void setClass(String aClass) {
        this.aClass = aClass;
    }

    public void setSubclass(String subclass) {
        this.subclass = subclass;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public void setGround(String ground) {
        this.ground = ground;
    }

    public void setHsno(String hsno) {
        this.hsno = hsno;
    }

    public void setProddate(String proddate) {
        this.proddate = proddate;
    }

    public void setBeard(String beard) {
        this.beard = beard;
    }

    public void setBirthmark(String birthmark) {
        this.birthmark = birthmark;
    }

    public void setBuilt(String built) {
        this.built = built;
    }

    public void setBurnmark(String burnmark) {
        this.burnmark = burnmark;
    }

    public void setComplexion(String complexion) {
        this.complexion = complexion;
    }

    public void setCutmark(String cutmark) {
        this.cutmark = cutmark;
    }

    public void setDeformity(String deformity) {
        this.deformity = deformity;
    }

    public void setEar(String ear) {
        this.ear = ear;
    }

    public void setEye(String eye) {
        this.eye = eye;
    }

    public void setEyebrow(String eyebrow) {
        this.eyebrow = eyebrow;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public void setForehead(String forehead) {
        this.forehead = forehead;
    }

    public void setHair(String hair) {
        this.hair = hair;
    }

    public void setMole(String mole) {
        this.mole = mole;
    }

    public void setMoustache(String moustache) {
        this.moustache = moustache;
    }

    public void setNose(String nose) {
        this.nose = nose;
    }

    public void setScarmark(String scarmark) {
        this.scarmark = scarmark;
    }

    public void setTattoomark(String tattoomark) {
        this.tattoomark = tattoomark;
    }

    public void setWartMark(String wartMark) {
        this.wartMark = wartMark;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public void setCrimeno(String crimeno) {
        this.crimeno = crimeno;
    }

    public void setOccurno(String occurno) {
        this.occurno = occurno;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void setRoughsec(String roughsec) {
        this.roughsec = roughsec;
    }

    public void setRoughpart(String roughpart) {
        this.roughpart = roughpart;
    }

    public void setPhotono(String photono) {
        this.photono = photono;
    }

    public void setTtcriminallid(String ttcriminallid) {
        this.ttcriminallid = ttcriminallid;
    }

    public String getPs() {
        return ps;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public String getNameAccused() {
        return nameAccused;
    }

    public String getAddressAccused() {
        return addressAccused;
    }

    public String getSexAccused() {
        return sexAccused;
    }

    public String getFatherAccused() {
        return fatherAccused;
    }

    public String getFrChgno() {
        return frChgno;
    }

    public String getChgyear() {
        return chgyear;
    }

    public String getNationality() {
        return nationality;
    }

    public String getScStObc() {
        return scStObc;
    }

    public String getOccupation() {
        return occupation;
    }

    public String getProvCrmNo() {
        return provCrmNo;
    }

    public String getRegCrmNo() {
        return regCrmNo;
    }

    public String getDtArrest() {
        return dtArrest;
    }

    public String getDtReleaseBail() {
        return dtReleaseBail;
    }

    public String getDtForwardCourt() {
        return dtForwardCourt;
    }

    public String getActsSections() {
        return actsSections;
    }

    public String getNamesSureties() {
        return namesSureties;
    }

    public String getAddressSureties() {
        return addressSureties;
    }

    public String getPrevConvcCaseref() {
        return prevConvcCaseref;
    }

    public String getStatus() {
        return status;
    }

    public String getSuspicionAppvd() {
        return suspicionAppvd;
    }

    public String getCaseYr() {
        return caseYr;
    }

    public String getSlno() {
        return slno;
    }

    public String getFircat() {
        return fircat;
    }

    public String getFatherHusband() {
        return fatherHusband;
    }

    public String getCrsgeneralid() {
        return crsgeneralid;
    }

    public String getFadd() {
        return fadd;
    }

    public String getFoccu() {
        return foccu;
    }

    public String getBirthyr() {
        return birthyr;
    }

    public String getHeightFeet() {
        return heightFeet;
    }

    public String getHeightInch() {
        return heightInch;
    }

    public String getPsRoadPsplace() {
        return psRoadPsplace;
    }

    public String getPspsKpj() {
        return pspsKpj;
    }

    public String getPspsOut() {
        return pspsOut;
    }

    public String getPsdist() {
        return psdist;
    }

    public String getPsdistOut() {
        return psdistOut;
    }

    public String getPspin() {
        return pspin;
    }

    public String getPsstate() {
        return psstate;
    }

    public String getPrroadPreplace() {
        return prroadPreplace;
    }

    public String getPrpsKpj() {
        return prpsKpj;
    }

    public String getPrpsOut() {
        return prpsOut;
    }

    public String getPrdist() {
        return prdist;
    }

    public String getPrdistOut() {
        return prdistOut;
    }

    public String getPrpin() {
        return prpin;
    }

    public String getPrstate() {
        return prstate;
    }

    public String getClassa() {
        return aClass;
    }

    public String getSubclass() {
        return subclass;
    }

    public String getProperty() {
        return property;
    }

    public String getTransport() {
        return transport;
    }

    public String getGround() {
        return ground;
    }

    public String getHsno() {
        return hsno;
    }

    public String getProddate() {
        return proddate;
    }

    public String getBeard() {
        return beard;
    }

    public String getBirthmark() {
        return birthmark;
    }

    public String getBuilt() {
        return built;
    }

    public String getBurnmark() {
        return burnmark;
    }

    public String getTtcriminallid() {
        return ttcriminallid;
    }

    public String getPhotono() {
        return photono;
    }

    public String getRoughpart() {
        return roughpart;
    }

    public String getRoughsec() {
        return roughsec;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getTarget() {
        return target;
    }

    public String getOccurno() {
        return occurno;
    }

    public String getCrimeno() {
        return crimeno;
    }

    public String getDivision() {
        return division;
    }

    public String getWartMark() {
        return wartMark;
    }

    public String getTattoomark() {
        return tattoomark;
    }

    public String getScarmark() {
        return scarmark;
    }

    public String getNose() {
        return nose;
    }

    public String getMoustache() {
        return moustache;
    }

    public String getMole() {
        return mole;
    }

    public String getHair() {
        return hair;
    }

    public String getForehead() {
        return forehead;
    }

    public String getFace() {
        return face;
    }

    public String getEyebrow() {
        return eyebrow;
    }

    public String getEye() {
        return eye;
    }

    public String getEar() {
        return ear;
    }

    public String getDeformity() {
        return deformity;
    }

    public String getCutmark() {
        return cutmark;
    }

    public String getComplexion() {
        return complexion;
    }
}
