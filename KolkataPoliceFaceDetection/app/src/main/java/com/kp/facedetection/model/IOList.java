package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by Kaushik on 12-09-2016.
 */
public class IOList implements Serializable {

    private String io_code="";
    private String io_name="";
    private String ps="";
    private String active_flag="";
    private String gf_no="";

    public String getIo_code() {
        return io_code;
    }

    public void setIo_code(String io_code) {
        this.io_code = io_code;
    }

    public String getIo_name() {
        return io_name;
    }

    public void setIo_name(String io_name) {
        this.io_name = io_name;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }

    public String getActive_flag() {
        return active_flag;
    }

    public void setActive_flag(String active_flag) {
        this.active_flag = active_flag;
    }

    public String getGf_no() {
        return gf_no;
    }

    public void setGf_no(String gf_no) {
        this.gf_no = gf_no;
    }
}
