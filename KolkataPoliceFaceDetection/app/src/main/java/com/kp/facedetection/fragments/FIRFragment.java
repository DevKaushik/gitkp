package com.kp.facedetection.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.adapter.ExpandableListAdapterForFIR;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by DAT-Asset-145 on 13-05-2016.
 */
public class FIRFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String PS_CODE = "PS_CODE";
    public static final String CRIME_NO = "CRIME_NO";
    public static final String CRIME_YR = "CRIME_YR";
    public static final String CASE_NO = "CASE_NO";
    public static final String CASE_YR = "CASE_YR";
    public static final String SL_NO = "SL_NO";
    private String ps_code="";
    private String crime_no="";
    private String crime_year="";
    private String case_no="";
    private String case_year="";
    private String sl_no="";

    private String details="";

    private String TAG="FIRFragment";

   // private TextView tv_details;
    private TextView tv_noData;
    //private RelativeLayout relative_details;

    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    ExpandableListAdapterForFIR listAdapter;
//    private FIRDetails firDetails;
//    private List<FIRDetails> firDetailsList = new ArrayList<FIRDetails>();
    private ExpandableListView lvExp;

    private int mPage;

    public static FIRFragment newInstance(int page,String ps_code,String crime_no,String crime_year,String case_no,String case_year,String sl_no) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        args.putString(PS_CODE, ps_code);
        args.putString(CRIME_NO, crime_no);
        args.putString(CRIME_YR, crime_year);
        args.putString(CASE_NO, case_no);
        args.putString(CASE_YR, case_year);
        args.putString(SL_NO, sl_no);
        FIRFragment fragment = new FIRFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        ps_code = getArguments().getString(PS_CODE).replace("PSCODE: ", "");
        crime_no = getArguments().getString(CRIME_NO).replace("CRIMENO: ", "");
        crime_year = getArguments().getString(CRIME_YR).replace("CRIMEYEAR: ", "");
        case_no = getArguments().getString(CASE_NO).replace("CASENO: ", "");
        case_year = getArguments().getString(CASE_YR).replace("CASEYR: ", "");
        sl_no = getArguments().getString(SL_NO).replace("SLNO: ", "");

       /* System.out.println("ps_code: " + ps_code);
        System.out.println("crime_no: " + crime_no);
        System.out.println("crime_year: " + crime_year);
        System.out.println("case_no: " + case_no);
        System.out.println("case_year: " + case_year);
        System.out.println("sl_no: " + sl_no);*/

    }

    // Inflate the fragment layout we defined above for this fragment
    // Set the associated text for the title
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        // tvTitle.setText(details);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

       //tv_details = (TextView) view.findViewById(R.id.tv_detail);
       // tv_noData = (TextView) view.findViewById(R.id.tv_noData);
       // relative_details = (RelativeLayout) view.findViewById(R.id.relative_details);

        tv_noData = (TextView) view.findViewById(R.id.tv_noData);
        lvExp = (ExpandableListView) view.findViewById(R.id.lvExp);

        Utility.changefonts(tv_noData, getActivity(), "Calibri.ttf");

        tv_noData.setVisibility(View.GONE);
        lvExp.setVisibility(View.GONE);


        //getFIRDetails(ps_code,crime_no,crime_year,case_no,case_year,sl_no);
      //  getModifiedFIRDetails(crime_no,crime_year);

    }

    private void getFIRDetails(String ps_code, String crime_no, String crime_year, String case_no,String case_year, String sl_no){

        this.ps_code = ps_code;
        this.crime_no = crime_no;
        this.crime_year = crime_year;
        this.case_no = case_no;
        this.case_year = case_year;
        this.sl_no = sl_no;


        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_FIR_DETAILS);
        taskManager.setCriminalDetailsFIR(true);

        String[] keys = { "flag","crimeno","crimeyear","pscode","caseno", "caseyr", "slno" };

        String[] values = { "F", crime_no.trim(), crime_year.trim(), ps_code.trim(), case_no.trim(), case_year.trim(), sl_no.trim() };

        taskManager.doStartTask(keys, values, true);
    }

    private void getModifiedFIRDetails(String crime_no, String crime_year){


        this.crime_no = crime_no;
        this.crime_year = crime_year;

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_FIR_DETAILS);
        taskManager.setModifiedCriminalDetailsFIR(true);

        String[] keys = { "crimeno","crimeyear"};

        String[] values = {crime_no.trim(), crime_year.trim() };
        //String[] values = {"2140", "2013" };

        taskManager.doStartTask(keys, values, true);
    }

    public void parseModifiedFIRResultResponse(String response) {

        System.out.println("within FIR");

        //System.out.println("Response in FIR:" + response);

        if (!response.equalsIgnoreCase("null") && !response.equals("")) {

            try {

                JSONObject jsonObject=new JSONObject(response);
                if(jsonObject.optString("status").equalsIgnoreCase("1")){

                    tv_noData.setVisibility(View.GONE);
                    lvExp.setVisibility(View.VISIBLE);

                    JSONArray result=jsonObject.getJSONArray("result");
                    parseFIRJSONObject(result);


                }
                else{
                    tv_noData.setVisibility(View.VISIBLE);
                    lvExp.setVisibility(View.GONE);
                    tv_noData.setText("Sorry, no FIR details found");
                    //Utility.showToast(getActivity(),"Sorry, no FIR details found","long");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        else {
            Utility.showToast(getActivity(), Constants.ERROR_MSG_TO_RELOAD, "long");
        }

    }

    public void parseFIRJSONObject(JSONArray result) {

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        JSONObject firobj;

        for (int i = 0; i < result.length(); i++) {
            firobj = result.optJSONObject(i);

          if(firobj.length()>0) {


                listDataHeader.add("FIR: "+firobj.optString("CASENO")+"/"+firobj.optString("CASE_YR"));

                Iterator iterator_fir = firobj.keys();
                List<String> list_fir = new ArrayList<>();

                while(iterator_fir.hasNext()){
                    String key = (String)iterator_fir.next();

                    if(!firobj.optString(key).equalsIgnoreCase("null")){
                        String value = firobj.optString(key);
                        list_fir.add(key.replace("_"," ") + "^" + value);

                    }

                }

                listDataChild.put(listDataHeader.get(listDataHeader.indexOf("FIR: "+firobj.optString("CASENO")+"/"+firobj.optString("CASE_YR"))),list_fir);
            }

            listAdapter = new ExpandableListAdapterForFIR(getActivity(), listDataHeader, listDataChild);

            // setting list adapter
            lvExp.setAdapter(listAdapter);

        }


    }


    public void parseFIRResultResponse(String response) {

        System.out.println("parseFIRResultResponse: " + response);

       /* if (response != null && !response.equals("")) {

            try {
                JSONObject jObj = new JSONObject(response);
                if(jObj.optString("status").equalsIgnoreCase("1")){

                    relative_details.setVisibility(View.VISIBLE);
                    tv_noData.setVisibility(View.GONE);

                    JSONObject criminal_details = jObj.getJSONObject("result");
                    createTextResults(criminal_details);

                }
                else{
                    // Toast.makeText(getActivity(), "Sorry, no warrant details found", Toast.LENGTH_LONG).show();
                    relative_details.setVisibility(View.GONE);
                    tv_noData.setVisibility(View.VISIBLE);
                    tv_noData.setText("Sorry, no fir details found");


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        else {
            Toast.makeText(getActivity(), "Some error has been encountered", Toast.LENGTH_LONG).show();
        }
*/
    }

    private void createTextResults(JSONObject criminal_details) {

        if (criminal_details.optString("PS") != null && !criminal_details.optString("PS").equalsIgnoreCase("null"))
            details = "PS : " + criminal_details.optString("PS");

        if (criminal_details.optString("CASENO") != null && !criminal_details.optString("CASENO").equalsIgnoreCase("null"))
            details = details + "\n\n"+ "CASENO : " + criminal_details.optString("CASENO");

        if (criminal_details.optString("NAME_ACCUSED") != null && !criminal_details.optString("NAME_ACCUSED").equalsIgnoreCase("null"))
            details = details + "\n\n"+"NAME_ACCUSED : " + criminal_details.optString("NAME_ACCUSED");

        if (criminal_details.optString("ALIAS1") != null && !criminal_details.optString("ALIAS1").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "ALIAS1 : " + criminal_details.optString("ALIAS1");

        if (criminal_details.optString("ALIAS2") != null && !criminal_details.optString("ALIAS2").equalsIgnoreCase("null"))
            details = details + "\n\n"+"ALIAS2 : " + criminal_details.optString("ALIAS2");

        if (criminal_details.optString("ALIAS3") != null && !criminal_details.optString("ALIAS3").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "ALIAS3 : " + criminal_details.optString("ALIAS3");

        if (criminal_details.optString("ADDR_ACCUSED") != null && !criminal_details.optString("ADDR_ACCUSED").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "ADDR_ACCUSED : " + criminal_details.optString("ADDR_ACCUSED");

        if (criminal_details.optString("SEX_ACCUSED") != null && !criminal_details.optString("SEX_ACCUSED").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "SEX_ACCUSED : " + criminal_details.optString("SEX_ACCUSED");

        if (criminal_details.optString("AGE_ACCUSED") != null && !criminal_details.optString("AGE_ACCUSED").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "AGE_ACCUSED : " + criminal_details.optString("AGE_ACCUSED");

        if (criminal_details.optString("FATHER_ACCUSED") != null && !criminal_details.optString("FATHER_ACCUSED").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "FATHER_ACCUSED : " + criminal_details.optString("FATHER_ACCUSED");

        if (criminal_details.optString("FR_CHGNO") != null && !criminal_details.optString("FR_CHGNO").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "FR_CHGNO : " + criminal_details.optString("FR_CHGNO");

        if (criminal_details.optString("CHGYEAR") != null && !criminal_details.optString("CHGYEAR").equalsIgnoreCase("null"))
            details = details + "\n\n"+"CHGYEAR : " + criminal_details.optString("CHGYEAR");

        if (criminal_details.optString("NATIONALITY") != null && !criminal_details.optString("NATIONALITY").equalsIgnoreCase("null"))
            details = details + "\n\n"+"NATIONALITY : " + criminal_details.optString("NATIONALITY");

        if (criminal_details.optString("RELIGION") != null && !criminal_details.optString("RELIGION").equalsIgnoreCase("null"))
            details = details + "\n\n"+"RELIGION : " + criminal_details.optString("RELIGION");

        if (criminal_details.optString("SC_ST_OBC") != null && !criminal_details.optString("SC_ST_OBC").equalsIgnoreCase("null"))
            details = details + "\n\n"+"SC_ST_OBC : " + criminal_details.optString("SC_ST_OBC");

        if (criminal_details.optString("OCCUPATION") != null && !criminal_details.optString("OCCUPATION").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "OCCUPATION : " + criminal_details.optString("OCCUPATION");

        if (criminal_details.optString("PROV_CRM_NO") != null && !criminal_details.optString("PROV_CRM_NO").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PROV_CRM_NO : " + criminal_details.optString("PROV_CRM_NO");

        if (criminal_details.optString("REG_CRM_NO") != null && !criminal_details.optString("REG_CRM_NO").equalsIgnoreCase("null"))
            details = details + "\n\n"+"REG_CRM_NO : " + criminal_details.optString("REG_CRM_NO");

        if (criminal_details.optString("DT_ARREST") != null && !criminal_details.optString("DT_ARREST").equalsIgnoreCase("null"))
            details = details + "\n\n"+"DT_ARREST : " + criminal_details.optString("DT_ARREST");

        if (criminal_details.optString("DT_RELEASE_BAIL") != null && !criminal_details.optString("DT_RELEASE_BAIL").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "DT_RELEASE_BAIL : " + criminal_details.optString("DT_RELEASE_BAIL");

        if (criminal_details.optString("DT_FORWARD_COURT") != null && !criminal_details.optString("DT_FORWARD_COURT").equalsIgnoreCase("null"))
            details = details + "\n\n"+"DT_FORWARD_COURT : " + criminal_details.optString("DT_FORWARD_COURT");

        if (criminal_details.optString("ACTS_SECTIONS") != null && !criminal_details.optString("ACTS_SECTIONS").equalsIgnoreCase("null"))
            details = details + "\n\n"+"ACTS_SECTIONS : " + criminal_details.optString("ACTS_SECTIONS");

        if (criminal_details.optString("NAMES_SURETIES") != null && !criminal_details.optString("NAMES_SURETIES").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "NAMES_SURETIES : " + criminal_details.optString("NAMES_SURETIES");

        if (criminal_details.optString("ADDRESS_SURETIES") != null && !criminal_details.optString("ADDRESS_SURETIES").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "ADDRESS_SURETIES : " + criminal_details.optString("ADDRESS_SURETIES");

        if (criminal_details.optString("PREV_CONVC_CASEREF") != null && !criminal_details.optString("PREV_CONVC_CASEREF").equalsIgnoreCase("null"))
            details =details + "\n\n"+ "PREV_CONVC_CASEREF : " + criminal_details.optString("PREV_CONVC_CASEREF");

        if (criminal_details.optString("STATUS") != null && !criminal_details.optString("STATUS").equalsIgnoreCase("null"))
            details = details + "\n\n"+"STATUS : " + criminal_details.optString("STATUS");

        if (criminal_details.optString("SUSPICION_APPVD") != null && !criminal_details.optString("SUSPICION_APPVD").equalsIgnoreCase("null"))
            details = details + "\n\n"+"SUSPICION_APPVD : " + criminal_details.optString("SUSPICION_APPVD");

        if (criminal_details.optString("CASE_YR") != null && !criminal_details.optString("CASE_YR").equalsIgnoreCase("null"))
            details = details + "\n\n"+"CASE_YR : " + criminal_details.optString("CASE_YR");

        if (criminal_details.optString("SLNO") != null && !criminal_details.optString("SLNO").equalsIgnoreCase("null"))
            details = details + "\n\n"+"SLNO : " + criminal_details.optString("SLNO");

        if (criminal_details.optString("FIRCAT") != null && !criminal_details.optString("FIRCAT").equalsIgnoreCase("null"))
            details = details + "\n\n"+"FIRCAT : " + criminal_details.optString("FIRCAT");

        if (criminal_details.optString("FATHER_HUSBAND") != null && !criminal_details.optString("FATHER_HUSBAND").equalsIgnoreCase("null"))
            details = details + "\n\n"+"FATHER_HUSBAND : " + criminal_details.optString("FATHER_HUSBAND");

        if (criminal_details.optString("CRSGENERALID") != null && !criminal_details.optString("CRSGENERALID").equalsIgnoreCase("null"))
            details = details + "\n\n"+"CRSGENERALID : " + criminal_details.optString("CRSGENERALID");

        if (criminal_details.optString("F_ADD") != null && !criminal_details.optString("F_ADD").equalsIgnoreCase("null"))
            details = details + "\n\n"+"F_ADD : " + criminal_details.optString("F_ADD");

        if (criminal_details.optString("F_OCCU") != null && !criminal_details.optString("F_OCCU").equalsIgnoreCase("null"))
            details = details + "\n\n"+"F_OCCU : " + criminal_details.optString("F_OCCU");

        if (criminal_details.optString("BIRTHYR") != null && !criminal_details.optString("BIRTHYR").equalsIgnoreCase("null"))
            details = details + "\n\n"+"BIRTHYR : " + criminal_details.optString("BIRTHYR");

        if (criminal_details.optString("HEIGHT_FEET") != null && !criminal_details.optString("HEIGHT_FEET").equalsIgnoreCase("null"))
            details = details + "\n\n"+"HEIGHT_FEET : " + criminal_details.optString("HEIGHT_FEET");

        if (criminal_details.optString("HEIGHT_INCH") != null && !criminal_details.optString("HEIGHT_INCH").equalsIgnoreCase("null"))
            details = details + "\n\n"+"HEIGHT_INCH : " + criminal_details.optString("HEIGHT_INCH");

        if (criminal_details.optString("PSROAD_PSPLACE") != null && !criminal_details.optString("PSROAD_PSPLACE").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PSROAD_PSPLACE : " + criminal_details.optString("PSROAD_PSPLACE");

        if (criminal_details.optString("PSPS_KPJ") != null && !criminal_details.optString("PSPS_KPJ").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PSPS_KPJ : " + criminal_details.optString("PSPS_KPJ");

        if (criminal_details.optString("PSPS_OUT") != null && !criminal_details.optString("PSPS_OUT").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PSPS_OUT : " + criminal_details.optString("PSPS_OUT");

        if (criminal_details.optString("PSDIST") != null && !criminal_details.optString("PSDIST").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PSDIST : " + criminal_details.optString("PSDIST");

        if (criminal_details.optString("PSDIST_OUT") != null && !criminal_details.optString("PSDIST_OUT").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PSDIST_OUT : " + criminal_details.optString("PSDIST_OUT");

        if (criminal_details.optString("PSPIN") != null && !criminal_details.optString("PSPIN").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PSPIN : " + criminal_details.optString("PSPIN");

        if (criminal_details.optString("PSSTATE") != null && !criminal_details.optString("PSSTATE").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PSSTATE : " + criminal_details.optString("PSSTATE");

        if (criminal_details.optString("PRROAD_PRPLACE") != null && !criminal_details.optString("PRROAD_PRPLACE").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PRROAD_PRPLACE : " + criminal_details.optString("PRROAD_PRPLACE");

        if (criminal_details.optString("PRPS_KPJ") != null && !criminal_details.optString("PRPS_KPJ").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PRPS_KPJ : " + criminal_details.optString("PRPS_KPJ");

        if (criminal_details.optString("PRPS_OUT") != null && !criminal_details.optString("PRPS_OUT").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PRPS_OUT : " + criminal_details.optString("PRPS_OUT");

        if (criminal_details.optString("PRDIST") != null && !criminal_details.optString("PRDIST").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PRDIST : " + criminal_details.optString("PRDIST");

        if (criminal_details.optString("PRDIST_OUT") != null && !criminal_details.optString("PRDIST_OUT").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PRDIST_OUT : " + criminal_details.optString("PRDIST_OUT");

        if (criminal_details.optString("PRPIN") != null && !criminal_details.optString("PRPIN").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PRPIN : " + criminal_details.optString("PRPIN");

        if (criminal_details.optString("PRSTATE") != null && !criminal_details.optString("PRSTATE").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PRSTATE : " + criminal_details.optString("PRSTATE");

        if (criminal_details.optString("CLASS") != null && !criminal_details.optString("CLASS").equalsIgnoreCase("null"))
            details = details + "\n\n"+"CLASS : " + criminal_details.optString("CLASS");

        if (criminal_details.optString("SUBCLASS") != null && !criminal_details.optString("SUBCLASS").equalsIgnoreCase("null"))
            details = details + "\n\n"+"SUBCLASS : " + criminal_details.optString("SUBCLASS");

        if (criminal_details.optString("PROPERTY") != null && !criminal_details.optString("PROPERTY").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PROPERTY : " + criminal_details.optString("PROPERTY");

        if (criminal_details.optString("TRANSPORT") != null && !criminal_details.optString("TRANSPORT").equalsIgnoreCase("null"))
            details = details + "\n\n"+"TRANSPORT : " + criminal_details.optString("TRANSPORT");

        if (criminal_details.optString("ARMS") != null && !criminal_details.optString("ARMS").equalsIgnoreCase("null"))
            details = details + "\n\n"+"ARMS : " + criminal_details.optString("ARMS");

        if (criminal_details.optString("GROUND") != null && !criminal_details.optString("GROUND").equalsIgnoreCase("null"))
            details = details + "\n\n"+"GROUND : " + criminal_details.optString("GROUND");

        if (criminal_details.optString("HSNO") != null && !criminal_details.optString("HSNO").equalsIgnoreCase("null"))
            details = details + "\n\n"+"HSNO : " + criminal_details.optString("HSNO");

        if (criminal_details.optString("PRODDATE") != null && !criminal_details.optString("PRODDATE").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PRODDATE : " + criminal_details.optString("PRODDATE");

        if (criminal_details.optString("BEARD") != null && !criminal_details.optString("BEARD").equalsIgnoreCase("null"))
            details = details + "\n\n"+"BEARD : " + criminal_details.optString("BEARD");

        if (criminal_details.optString("BIRTHMARK") != null && !criminal_details.optString("BIRTHMARK").equalsIgnoreCase("null"))
            details = details + "\n\n"+"BIRTHMARK : " + criminal_details.optString("BIRTHMARK");

        if (criminal_details.optString("BUILT") != null && !criminal_details.optString("BUILT").equalsIgnoreCase("null"))
            details = details + "\n\n"+"BUILT : " + criminal_details.optString("BUILT");

        if (criminal_details.optString("BURNMARK") != null && !criminal_details.optString("BURNMARK").equalsIgnoreCase("null"))
            details = details + "\n\n"+"BURNMARK : " + criminal_details.optString("BURNMARK");

        if (criminal_details.optString("COMPLEXION") != null && !criminal_details.optString("COMPLEXION").equalsIgnoreCase("null"))
            details = details + "\n\n"+"COMPLEXION : " + criminal_details.optString("COMPLEXION");

        if (criminal_details.optString("CUTMARK") != null && !criminal_details.optString("CUTMARK").equalsIgnoreCase("null"))
            details = details + "\n\n"+"CUTMARK : " + criminal_details.optString("CUTMARK");

        if (criminal_details.optString("DEFORMITY") != null && !criminal_details.optString("DEFORMITY").equalsIgnoreCase("null"))
            details = details + "\n\n"+"DEFORMITY : " + criminal_details.optString("DEFORMITY");

        if (criminal_details.optString("EAR") != null && !criminal_details.optString("EAR").equalsIgnoreCase("null"))
            details = details + "\n\n"+"EAR : " + criminal_details.optString("EAR");

        if (criminal_details.optString("EYE") != null && !criminal_details.optString("EYE").equalsIgnoreCase("null"))
            details = details + "\n\n"+"EYE : " + criminal_details.optString("EYE");

        if (criminal_details.optString("EYEBROW") != null && !criminal_details.optString("EYEBROW").equalsIgnoreCase("null"))
            details = details + "\n\n"+"EYEBROW : " + criminal_details.optString("EYEBROW");

        if (criminal_details.optString("FACE") != null && !criminal_details.optString("FACE").equalsIgnoreCase("null"))
            details = details + "\n\n"+"FACE : " + criminal_details.optString("FACE");

        if (criminal_details.optString("FOREHEAD") != null && !criminal_details.optString("FOREHEAD").equalsIgnoreCase("null"))
            details = details + "\n\n"+"FOREHEAD : " + criminal_details.optString("FOREHEAD");

        if (criminal_details.optString("HAIR") != null && !criminal_details.optString("HAIR").equalsIgnoreCase("null"))
            details = details + "\n\n"+"HAIR : " + criminal_details.optString("HAIR");

        if (criminal_details.optString("MOLE") != null && !criminal_details.optString("MOLE").equalsIgnoreCase("null"))
            details = details + "\n\n"+"MOLE : " + criminal_details.optString("MOLE");

        if (criminal_details.optString("MOUSTACHE") != null && !criminal_details.optString("MOUSTACHE").equalsIgnoreCase("null"))
            details = details + "\n\n"+"MOUSTACHE : " + criminal_details.optString("MOUSTACHE");

        if (criminal_details.optString("NOSE") != null && !criminal_details.optString("NOSE").equalsIgnoreCase("null"))
            details = details + "\n\n"+"NOSE : " + criminal_details.optString("NOSE");

        if (criminal_details.optString("SCARMARK") != null && !criminal_details.optString("SCARMARK").equalsIgnoreCase("null"))
            details = details + "\n\n"+"SCARMARK : " + criminal_details.optString("SCARMARK");

        if (criminal_details.optString("TATTOOMARK") != null && !criminal_details.optString("TATTOOMARK").equalsIgnoreCase("null"))
            details = details + "\n\n"+"TATTOOMARK : " + criminal_details.optString("TATTOOMARK");

        if (criminal_details.optString("WARTMARK") != null && !criminal_details.optString("WARTMARK").equalsIgnoreCase("null"))
            details = details + "\n\n"+"WARTMARK : " + criminal_details.optString("WARTMARK");

        if (criminal_details.optString("DIVISION") != null && !criminal_details.optString("DIVISION").equalsIgnoreCase("null"))
            details = details + "\n\n"+"DIVISION : " + criminal_details.optString("DIVISION");

        if (criminal_details.optString("CRIMENO") != null && !criminal_details.optString("CRIMENO").equalsIgnoreCase("null"))
            details = details + "\n\n"+"CRIMENO : " + criminal_details.optString("CRIMENO");

        if (criminal_details.optString("OCCURNO") != null && !criminal_details.optString("OCCURNO").equalsIgnoreCase("null"))
            details = details + "\n\n"+"OCCURNO : " + criminal_details.optString("OCCURNO");

        if (criminal_details.optString("TARGET") != null && !criminal_details.optString("TARGET").equalsIgnoreCase("null"))
            details = details + "\n\n"+"TARGET : " + criminal_details.optString("TARGET");

        if (criminal_details.optString("IDENTIFIER") != null && !criminal_details.optString("IDENTIFIER").equalsIgnoreCase("null"))
            details = details + "\n\n"+"IDENTIFIER : " + criminal_details.optString("IDENTIFIER");

        if (criminal_details.optString("ROUGHSEC") != null && !criminal_details.optString("ROUGHSEC").equalsIgnoreCase("null"))
            details = details + "\n\n"+"ROUGHSEC : " + criminal_details.optString("ROUGHSEC");

        if (criminal_details.optString("ROUGHPART") != null && !criminal_details.optString("ROUGHPART").equalsIgnoreCase("null"))
            details = details + "\n\n"+"ROUGHPART : " + criminal_details.optString("ROUGHPART");

        if (criminal_details.optString("PHOTONO") != null && !criminal_details.optString("PHOTONO").equalsIgnoreCase("null"))
            details = details + "\n\n"+"PHOTONO : " + criminal_details.optString("PHOTONO");

        if (criminal_details.optString("TTCRIMINALID") != null && !criminal_details.optString("TTCRIMINALID").equalsIgnoreCase("null"))
            details = details + "\n\n"+"TTCRIMINALID : " + criminal_details.optString("TTCRIMINALID");

        if (criminal_details.optString("ALIAS4") != null && !criminal_details.optString("ALIAS4").equalsIgnoreCase("null"))
            details = details + "\n\n"+"ALIAS4 : " + criminal_details.optString("ALIAS4");

        if (criminal_details.optString("CRIMEYEAR") != null && !criminal_details.optString("CRIMEYEAR").equalsIgnoreCase("null"))
            details = details + "\n\n"+"CRIMEYEAR : " + criminal_details.optString("CRIMEYEAR");

       // tv_details.setText(details);

    }

}
