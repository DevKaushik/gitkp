package com.kp.facedetection.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by JAYDEEP on 3/26/2018.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "status_msg",
        "status_colour",
        "request_type",
        "officerName",
        "caseRef",
        "requestTime",
        "requestedBy",
        "sendStatus",
        "mcellRead",
        "mcellReadBy",
        "mcellReadTime",
        "providerReply",
        "providerReplyBy",
        "providerReplyTime",
        "mCellToPsAction",
        "mCellToPsActionBy",
        "mCellToPsActionTime",
        "emailId",
        "phoneNumber",
        "ps",
        "div",
        "rank",
        "approvedByOC",
        "approvedByOCTime",
        "approvedOCName",
        "approvedByDC",
        "approvedByDCTime",
        "approvedDCName",
        "approvedByAC",
        "approvedACTime",
        "approvedACName",
        "approvedJtCp",
        "approvedJtCpTime",
        "approvedJtCpName",
        "approvedNodal",
        "approvedNodalTime",
        "approvedNodalName",
        "approvedDesignatedOficer",
        "approvedDesignatedOfficerName",
        "approvedDesignatedOfficerTime",
        "skippedOC",
        "skippedAC",
        "skippedDC",
        "revoked",
        "oc_approval_flag",
        "ac_approval_flag",
        "dc_approval_flag",
        "jtcp_approval_flag",
        "nodal_approval_flag",
        "designated_approval_flag",
        "reconnection",
        "disconnection",
        "group",
        "kp_mcell_memo_no",
        "kp_mcell_memo_no_date",
        "cp_memo_no",
        "cp_memo_no_date",
        "hs_memo_no",
        "hs_memo_no_date",
        "mcpo_no",
        "mcpo_no_date",
        "mcdo_no",
        "mcdo_no_date",
        "date_of_activation",
        "date_of_disconnection",
        "user",
        "listener",
        "just_title",
        "just_subject",
        "requested_duration_for",
        "urgent_general_flag",
        "activated",
        "activated_by",
        "activated_time",
        "draft_save",
        "complete_save",
        "request_user_id",
        "requestPSName",
        "requeststateLI",
        "expired",
        "mcell_action_li",
        "mcell_action_li_by",
        "mcell_action_li_datetime",
        "send_to_hs",
        "send_to_hs_by",
        "send_to_hs_datetime",
        "received_hs_order",
        "received_hs_order_by",
        "received_hs_order_datetime",
        "mcell_verify_view",
        "mcell_view_by",
        "mcell_time",
        "requestBriefDataSuperiors",
        "details",
        "notification_details"
})
public class DC implements Serializable {
    @JsonProperty("id")
    private String id;
    @JsonProperty("status_msg")
    private String statusMsg;
    @JsonProperty("status_colour")
    private String statusColour;
    @JsonProperty("request_type")
    private String requestType;
    @JsonProperty("officerName")
    private String officerName;
    @JsonProperty("caseRef")
    private String caseRef;
    @JsonProperty("requestTime")
    private String requestTime;
    @JsonProperty("requestedBy")
    private String requestedBy;
    @JsonProperty("sendStatus")
    private String sendStatus;
    @JsonProperty("mcellRead")
    private String mcellRead;
    @JsonProperty("mcellReadBy")
    private String mcellReadBy;
    @JsonProperty("mcellReadTime")
    private String mcellReadTime;
    @JsonProperty("providerReply")
    private String providerReply;
    @JsonProperty("providerReplyBy")
    private String providerReplyBy;
    @JsonProperty("providerReplyTime")
    private String providerReplyTime;
    @JsonProperty("mCellToPsAction")
    private String mCellToPsAction;
    @JsonProperty("mCellToPsActionBy")
    private String mCellToPsActionBy;
    @JsonProperty("mCellToPsActionTime")
    private String mCellToPsActionTime;
    @JsonProperty("emailId")
    private String emailId;
    @JsonProperty("phoneNumber")
    private String phoneNumber;
    @JsonProperty("ps")
    private String ps;
    @JsonProperty("div")
    private String div;
    @JsonProperty("rank")
    private String rank;
    @JsonProperty("approvedByOC")
    private String approvedByOC;
    @JsonProperty("approvedByOCTime")
    private String approvedByOCTime;
    @JsonProperty("approvedOCName")
    private String approvedOCName;
    @JsonProperty("approvedByDC")
    private String approvedByDC;
    @JsonProperty("approvedByDCTime")
    private String approvedByDCTime;
    @JsonProperty("approvedDCName")
    private String approvedDCName;
    @JsonProperty("approvedByAC")
    private String approvedByAC;
    @JsonProperty("approvedACTime")
    private String approvedACTime;
    @JsonProperty("approvedACName")
    private String approvedACName;
    @JsonProperty("approvedJtCp")
    private String approvedJtCp;
    @JsonProperty("approvedJtCpTime")
    private String approvedJtCpTime;
    @JsonProperty("approvedJtCpName")
    private String approvedJtCpName;
    @JsonProperty("approvedNodal")
    private String approvedNodal;
    @JsonProperty("approvedNodalTime")
    private String approvedNodalTime;
    @JsonProperty("approvedNodalName")
    private String approvedNodalName;
    @JsonProperty("approvedDesignatedOficer")
    private String approvedDesignatedOficer;
    @JsonProperty("approvedDesignatedOfficerName")
    private String approvedDesignatedOfficerName;
    @JsonProperty("approvedDesignatedOfficerTime")
    private String approvedDesignatedOfficerTime;
    @JsonProperty("skippedOC")
    private String skippedOC;
    @JsonProperty("skippedAC")
    private String skippedAC;
    @JsonProperty("skippedDC")
    private String skippedDC;
    @JsonProperty("revoked")
    private String revoked;
    @JsonProperty("oc_approval_flag")
    private String ocApprovalFlag;
    @JsonProperty("ac_approval_flag")
    private String acApprovalFlag;
    @JsonProperty("dc_approval_flag")
    private String dcApprovalFlag;
    @JsonProperty("jtcp_approval_flag")
    private String jtcpApprovalFlag;
    @JsonProperty("nodal_approval_flag")
    private String nodalApprovalFlag;
    @JsonProperty("designated_approval_flag")
    private String designatedApprovalFlag;
    @JsonProperty("reconnection")
    private String reconnection;
    @JsonProperty("disconnection")
    private String disconnection;
    @JsonProperty("group")
    private String group;
    @JsonProperty("kp_mcell_memo_no")
    private Object kpMcellMemoNo;
    @JsonProperty("kp_mcell_memo_no_date")
    private Object kpMcellMemoNoDate;
    @JsonProperty("cp_memo_no")
    private Object cpMemoNo;
    @JsonProperty("cp_memo_no_date")
    private Object cpMemoNoDate;
    @JsonProperty("hs_memo_no")
    private Object hsMemoNo;
    @JsonProperty("hs_memo_no_date")
    private Object hsMemoNoDate;
    @JsonProperty("mcpo_no")
    private Object mcpoNo;
    @JsonProperty("mcpo_no_date")
    private Object mcpoNoDate;
    @JsonProperty("mcdo_no")
    private Object mcdoNo;
    @JsonProperty("mcdo_no_date")
    private Object mcdoNoDate;
    @JsonProperty("date_of_activation")
    private String dateOfActivation;
    @JsonProperty("date_of_disconnection")
    private Object dateOfDisconnection;
    @JsonProperty("user")
    private String user;
    @JsonProperty("listener")
    private String listener;
    @JsonProperty("just_title")
    private String justTitle;
    @JsonProperty("just_subject")
    private String justSubject;
    @JsonProperty("requested_duration_for")
    private String requestedDurationFor;
    @JsonProperty("urgent_general_flag")
    private String urgentGeneralFlag;
    @JsonProperty("activated")
    private String activated;
    @JsonProperty("activated_by")
    private String activatedBy;
    @JsonProperty("activated_time")
    private String activatedTime;
    @JsonProperty("draft_save")
    private String draftSave;
    @JsonProperty("complete_save")
    private String completeSave;
    @JsonProperty("request_user_id")
    private String requestUserId;
    @JsonProperty("requestPSName")
    private String requestPSName;
    @JsonProperty("requeststateLI")
    private String requeststateLI;
    @JsonProperty("expired")
    private String expired;
    @JsonProperty("mcell_action_li")
    private String mcellActionLi;
    @JsonProperty("mcell_action_li_by")
    private String mcellActionLiBy;
    @JsonProperty("mcell_action_li_datetime")
    private String mcellActionLiDatetime;
    @JsonProperty("send_to_hs")
    private String sendToHs;
    @JsonProperty("send_to_hs_by")
    private String sendToHsBy;
    @JsonProperty("send_to_hs_datetime")
    private String sendToHsDatetime;
    @JsonProperty("received_hs_order")
    private Object receivedHsOrder;
    @JsonProperty("received_hs_order_by")
    private String receivedHsOrderBy;
    @JsonProperty("received_hs_order_datetime")
    private String receivedHsOrderDatetime;
    @JsonProperty("mcell_verify_view")
    private String mcellVerifyView;
    @JsonProperty("mcell_view_by")
    private Object mcellViewBy;
    @JsonProperty("mcell_time")
    private String mcellTime;
    @JsonProperty("requestBriefDataSuperiors")
    private String requestBriefDataSuperiors;
    @JsonProperty("details")
    private List<Detail> details = null;
    @JsonProperty("notification_details")
    private List<NotificationDetail> notificationDetails = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("status_msg")
    public String getStatusMsg() {
        return statusMsg;
    }

    @JsonProperty("status_msg")
    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    @JsonProperty("status_colour")
    public String getStatusColour() {
        return statusColour;
    }

    @JsonProperty("status_colour")
    public void setStatusColour(String statusColour) {
        this.statusColour = statusColour;
    }

    @JsonProperty("request_type")
    public String getRequestType() {
        return requestType;
    }

    @JsonProperty("request_type")
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    @JsonProperty("officerName")
    public String getOfficerName() {
        return officerName;
    }

    @JsonProperty("officerName")
    public void setOfficerName(String officerName) {
        this.officerName = officerName;
    }

    @JsonProperty("caseRef")
    public String getCaseRef() {
        return caseRef;
    }

    @JsonProperty("caseRef")
    public void setCaseRef(String caseRef) {
        this.caseRef = caseRef;
    }

    @JsonProperty("requestTime")
    public String getRequestTime() {
        return requestTime;
    }

    @JsonProperty("requestTime")
    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    @JsonProperty("requestedBy")
    public String getRequestedBy() {
        return requestedBy;
    }

    @JsonProperty("requestedBy")
    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    @JsonProperty("sendStatus")
    public String getSendStatus() {
        return sendStatus;
    }

    @JsonProperty("sendStatus")
    public void setSendStatus(String sendStatus) {
        this.sendStatus = sendStatus;
    }

    @JsonProperty("mcellRead")
    public String getMcellRead() {
        return mcellRead;
    }

    @JsonProperty("mcellRead")
    public void setMcellRead(String mcellRead) {
        this.mcellRead = mcellRead;
    }

    @JsonProperty("mcellReadBy")
    public String getMcellReadBy() {
        return mcellReadBy;
    }

    @JsonProperty("mcellReadBy")
    public void setMcellReadBy(String mcellReadBy) {
        this.mcellReadBy = mcellReadBy;
    }

    @JsonProperty("mcellReadTime")
    public String getMcellReadTime() {
        return mcellReadTime;
    }

    @JsonProperty("mcellReadTime")
    public void setMcellReadTime(String mcellReadTime) {
        this.mcellReadTime = mcellReadTime;
    }

    @JsonProperty("providerReply")
    public String getProviderReply() {
        return providerReply;
    }

    @JsonProperty("providerReply")
    public void setProviderReply(String providerReply) {
        this.providerReply = providerReply;
    }

    @JsonProperty("providerReplyBy")
    public String getProviderReplyBy() {
        return providerReplyBy;
    }

    @JsonProperty("providerReplyBy")
    public void setProviderReplyBy(String providerReplyBy) {
        this.providerReplyBy = providerReplyBy;
    }

    @JsonProperty("providerReplyTime")
    public String getProviderReplyTime() {
        return providerReplyTime;
    }

    @JsonProperty("providerReplyTime")
    public void setProviderReplyTime(String providerReplyTime) {
        this.providerReplyTime = providerReplyTime;
    }

    @JsonProperty("mCellToPsAction")
    public String getMCellToPsAction() {
        return mCellToPsAction;
    }

    @JsonProperty("mCellToPsAction")
    public void setMCellToPsAction(String mCellToPsAction) {
        this.mCellToPsAction = mCellToPsAction;
    }

    @JsonProperty("mCellToPsActionBy")
    public String getMCellToPsActionBy() {
        return mCellToPsActionBy;
    }

    @JsonProperty("mCellToPsActionBy")
    public void setMCellToPsActionBy(String mCellToPsActionBy) {
        this.mCellToPsActionBy = mCellToPsActionBy;
    }

    @JsonProperty("mCellToPsActionTime")
    public String getMCellToPsActionTime() {
        return mCellToPsActionTime;
    }

    @JsonProperty("mCellToPsActionTime")
    public void setMCellToPsActionTime(String mCellToPsActionTime) {
        this.mCellToPsActionTime = mCellToPsActionTime;
    }

    @JsonProperty("emailId")
    public String getEmailId() {
        return emailId;
    }

    @JsonProperty("emailId")
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @JsonProperty("phoneNumber")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @JsonProperty("phoneNumber")
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @JsonProperty("ps")
    public String getPs() {
        return ps;
    }

    @JsonProperty("ps")
    public void setPs(String ps) {
        this.ps = ps;
    }

    @JsonProperty("div")
    public String getDiv() {
        return div;
    }

    @JsonProperty("div")
    public void setDiv(String div) {
        this.div = div;
    }

    @JsonProperty("rank")
    public String getRank() {
        return rank;
    }

    @JsonProperty("rank")
    public void setRank(String rank) {
        this.rank = rank;
    }

    @JsonProperty("approvedByOC")
    public String getApprovedByOC() {
        return approvedByOC;
    }

    @JsonProperty("approvedByOC")
    public void setApprovedByOC(String approvedByOC) {
        this.approvedByOC = approvedByOC;
    }

    @JsonProperty("approvedByOCTime")
    public String getApprovedByOCTime() {
        return approvedByOCTime;
    }

    @JsonProperty("approvedByOCTime")
    public void setApprovedByOCTime(String approvedByOCTime) {
        this.approvedByOCTime = approvedByOCTime;
    }

    @JsonProperty("approvedOCName")
    public String getApprovedOCName() {
        return approvedOCName;
    }

    @JsonProperty("approvedOCName")
    public void setApprovedOCName(String approvedOCName) {
        this.approvedOCName = approvedOCName;
    }

    @JsonProperty("approvedByDC")
    public String getApprovedByDC() {
        return approvedByDC;
    }

    @JsonProperty("approvedByDC")
    public void setApprovedByDC(String approvedByDC) {
        this.approvedByDC = approvedByDC;
    }

    @JsonProperty("approvedByDCTime")
    public String getApprovedByDCTime() {
        return approvedByDCTime;
    }

    @JsonProperty("approvedByDCTime")
    public void setApprovedByDCTime(String approvedByDCTime) {
        this.approvedByDCTime = approvedByDCTime;
    }

    @JsonProperty("approvedDCName")
    public String getApprovedDCName() {
        return approvedDCName;
    }

    @JsonProperty("approvedDCName")
    public void setApprovedDCName(String approvedDCName) {
        this.approvedDCName = approvedDCName;
    }

    @JsonProperty("approvedByAC")
    public String getApprovedByAC() {
        return approvedByAC;
    }

    @JsonProperty("approvedByAC")
    public void setApprovedByAC(String approvedByAC) {
        this.approvedByAC = approvedByAC;
    }

    @JsonProperty("approvedACTime")
    public String getApprovedACTime() {
        return approvedACTime;
    }

    @JsonProperty("approvedACTime")
    public void setApprovedACTime(String approvedACTime) {
        this.approvedACTime = approvedACTime;
    }

    @JsonProperty("approvedACName")
    public String getApprovedACName() {
        return approvedACName;
    }

    @JsonProperty("approvedACName")
    public void setApprovedACName(String approvedACName) {
        this.approvedACName = approvedACName;
    }

    @JsonProperty("approvedJtCp")
    public String getApprovedJtCp() {
        return approvedJtCp;
    }

    @JsonProperty("approvedJtCp")
    public void setApprovedJtCp(String approvedJtCp) {
        this.approvedJtCp = approvedJtCp;
    }

    @JsonProperty("approvedJtCpTime")
    public String getApprovedJtCpTime() {
        return approvedJtCpTime;
    }

    @JsonProperty("approvedJtCpTime")
    public void setApprovedJtCpTime(String approvedJtCpTime) {
        this.approvedJtCpTime = approvedJtCpTime;
    }

    @JsonProperty("approvedJtCpName")
    public String getApprovedJtCpName() {
        return approvedJtCpName;
    }

    @JsonProperty("approvedJtCpName")
    public void setApprovedJtCpName(String approvedJtCpName) {
        this.approvedJtCpName = approvedJtCpName;
    }

    @JsonProperty("approvedNodal")
    public String getApprovedNodal() {
        return approvedNodal;
    }

    @JsonProperty("approvedNodal")
    public void setApprovedNodal(String approvedNodal) {
        this.approvedNodal = approvedNodal;
    }

    @JsonProperty("approvedNodalTime")
    public String getApprovedNodalTime() {
        return approvedNodalTime;
    }

    @JsonProperty("approvedNodalTime")
    public void setApprovedNodalTime(String approvedNodalTime) {
        this.approvedNodalTime = approvedNodalTime;
    }

    @JsonProperty("approvedNodalName")
    public String getApprovedNodalName() {
        return approvedNodalName;
    }

    @JsonProperty("approvedNodalName")
    public void setApprovedNodalName(String approvedNodalName) {
        this.approvedNodalName = approvedNodalName;
    }

    @JsonProperty("approvedDesignatedOficer")
    public String getApprovedDesignatedOficer() {
        return approvedDesignatedOficer;
    }

    @JsonProperty("approvedDesignatedOficer")
    public void setApprovedDesignatedOficer(String approvedDesignatedOficer) {
        this.approvedDesignatedOficer = approvedDesignatedOficer;
    }

    @JsonProperty("approvedDesignatedOfficerName")
    public String getApprovedDesignatedOfficerName() {
        return approvedDesignatedOfficerName;
    }

    @JsonProperty("approvedDesignatedOfficerName")
    public void setApprovedDesignatedOfficerName(String approvedDesignatedOfficerName) {
        this.approvedDesignatedOfficerName = approvedDesignatedOfficerName;
    }

    @JsonProperty("approvedDesignatedOfficerTime")
    public String getApprovedDesignatedOfficerTime() {
        return approvedDesignatedOfficerTime;
    }

    @JsonProperty("approvedDesignatedOfficerTime")
    public void setApprovedDesignatedOfficerTime(String approvedDesignatedOfficerTime) {
        this.approvedDesignatedOfficerTime = approvedDesignatedOfficerTime;
    }

    @JsonProperty("skippedOC")
    public String getSkippedOC() {
        return skippedOC;
    }

    @JsonProperty("skippedOC")
    public void setSkippedOC(String skippedOC) {
        this.skippedOC = skippedOC;
    }

    @JsonProperty("skippedAC")
    public String getSkippedAC() {
        return skippedAC;
    }

    @JsonProperty("skippedAC")
    public void setSkippedAC(String skippedAC) {
        this.skippedAC = skippedAC;
    }

    @JsonProperty("skippedDC")
    public String getSkippedDC() {
        return skippedDC;
    }

    @JsonProperty("skippedDC")
    public void setSkippedDC(String skippedDC) {
        this.skippedDC = skippedDC;
    }

    @JsonProperty("revoked")
    public String getRevoked() {
        return revoked;
    }

    @JsonProperty("revoked")
    public void setRevoked(String revoked) {
        this.revoked = revoked;
    }

    @JsonProperty("oc_approval_flag")
    public String getOcApprovalFlag() {
        return ocApprovalFlag;
    }

    @JsonProperty("oc_approval_flag")
    public void setOcApprovalFlag(String ocApprovalFlag) {
        this.ocApprovalFlag = ocApprovalFlag;
    }

    @JsonProperty("ac_approval_flag")
    public String getAcApprovalFlag() {
        return acApprovalFlag;
    }

    @JsonProperty("ac_approval_flag")
    public void setAcApprovalFlag(String acApprovalFlag) {
        this.acApprovalFlag = acApprovalFlag;
    }

    @JsonProperty("dc_approval_flag")
    public String getDcApprovalFlag() {
        return dcApprovalFlag;
    }

    @JsonProperty("dc_approval_flag")
    public void setDcApprovalFlag(String dcApprovalFlag) {
        this.dcApprovalFlag = dcApprovalFlag;
    }

    @JsonProperty("jtcp_approval_flag")
    public String getJtcpApprovalFlag() {
        return jtcpApprovalFlag;
    }

    @JsonProperty("jtcp_approval_flag")
    public void setJtcpApprovalFlag(String jtcpApprovalFlag) {
        this.jtcpApprovalFlag = jtcpApprovalFlag;
    }

    @JsonProperty("nodal_approval_flag")
    public String getNodalApprovalFlag() {
        return nodalApprovalFlag;
    }

    @JsonProperty("nodal_approval_flag")
    public void setNodalApprovalFlag(String nodalApprovalFlag) {
        this.nodalApprovalFlag = nodalApprovalFlag;
    }

    @JsonProperty("designated_approval_flag")
    public String getDesignatedApprovalFlag() {
        return designatedApprovalFlag;
    }

    @JsonProperty("designated_approval_flag")
    public void setDesignatedApprovalFlag(String designatedApprovalFlag) {
        this.designatedApprovalFlag = designatedApprovalFlag;
    }

    @JsonProperty("reconnection")
    public String getReconnection() {
        return reconnection;
    }

    @JsonProperty("reconnection")
    public void setReconnection(String reconnection) {
        this.reconnection = reconnection;
    }

    @JsonProperty("disconnection")
    public String getDisconnection() {
        return disconnection;
    }

    @JsonProperty("disconnection")
    public void setDisconnection(String disconnection) {
        this.disconnection = disconnection;
    }

    @JsonProperty("group")
    public String getGroup() {
        return group;
    }

    @JsonProperty("group")
    public void setGroup(String group) {
        this.group = group;
    }

    @JsonProperty("kp_mcell_memo_no")
    public Object getKpMcellMemoNo() {
        return kpMcellMemoNo;
    }

    @JsonProperty("kp_mcell_memo_no")
    public void setKpMcellMemoNo(Object kpMcellMemoNo) {
        this.kpMcellMemoNo = kpMcellMemoNo;
    }

    @JsonProperty("kp_mcell_memo_no_date")
    public Object getKpMcellMemoNoDate() {
        return kpMcellMemoNoDate;
    }

    @JsonProperty("kp_mcell_memo_no_date")
    public void setKpMcellMemoNoDate(Object kpMcellMemoNoDate) {
        this.kpMcellMemoNoDate = kpMcellMemoNoDate;
    }

    @JsonProperty("cp_memo_no")
    public Object getCpMemoNo() {
        return cpMemoNo;
    }

    @JsonProperty("cp_memo_no")
    public void setCpMemoNo(Object cpMemoNo) {
        this.cpMemoNo = cpMemoNo;
    }

    @JsonProperty("cp_memo_no_date")
    public Object getCpMemoNoDate() {
        return cpMemoNoDate;
    }

    @JsonProperty("cp_memo_no_date")
    public void setCpMemoNoDate(Object cpMemoNoDate) {
        this.cpMemoNoDate = cpMemoNoDate;
    }

    @JsonProperty("hs_memo_no")
    public Object getHsMemoNo() {
        return hsMemoNo;
    }

    @JsonProperty("hs_memo_no")
    public void setHsMemoNo(Object hsMemoNo) {
        this.hsMemoNo = hsMemoNo;
    }

    @JsonProperty("hs_memo_no_date")
    public Object getHsMemoNoDate() {
        return hsMemoNoDate;
    }

    @JsonProperty("hs_memo_no_date")
    public void setHsMemoNoDate(Object hsMemoNoDate) {
        this.hsMemoNoDate = hsMemoNoDate;
    }

    @JsonProperty("mcpo_no")
    public Object getMcpoNo() {
        return mcpoNo;
    }

    @JsonProperty("mcpo_no")
    public void setMcpoNo(Object mcpoNo) {
        this.mcpoNo = mcpoNo;
    }

    @JsonProperty("mcpo_no_date")
    public Object getMcpoNoDate() {
        return mcpoNoDate;
    }

    @JsonProperty("mcpo_no_date")
    public void setMcpoNoDate(Object mcpoNoDate) {
        this.mcpoNoDate = mcpoNoDate;
    }

    @JsonProperty("mcdo_no")
    public Object getMcdoNo() {
        return mcdoNo;
    }

    @JsonProperty("mcdo_no")
    public void setMcdoNo(Object mcdoNo) {
        this.mcdoNo = mcdoNo;
    }

    @JsonProperty("mcdo_no_date")
    public Object getMcdoNoDate() {
        return mcdoNoDate;
    }

    @JsonProperty("mcdo_no_date")
    public void setMcdoNoDate(Object mcdoNoDate) {
        this.mcdoNoDate = mcdoNoDate;
    }

    @JsonProperty("date_of_activation")
    public String getDateOfActivation() {
        return dateOfActivation;
    }

    @JsonProperty("date_of_activation")
    public void setDateOfActivation(String dateOfActivation) {
        this.dateOfActivation = dateOfActivation;
    }

    @JsonProperty("date_of_disconnection")
    public Object getDateOfDisconnection() {
        return dateOfDisconnection;
    }

    @JsonProperty("date_of_disconnection")
    public void setDateOfDisconnection(Object dateOfDisconnection) {
        this.dateOfDisconnection = dateOfDisconnection;
    }

    @JsonProperty("user")
    public String getUser() {
        return user;
    }

    @JsonProperty("user")
    public void setUser(String user) {
        this.user = user;
    }

    @JsonProperty("listener")
    public String getListener() {
        return listener;
    }

    @JsonProperty("listener")
    public void setListener(String listener) {
        this.listener = listener;
    }

    @JsonProperty("just_title")
    public String getJustTitle() {
        return justTitle;
    }

    @JsonProperty("just_title")
    public void setJustTitle(String justTitle) {
        this.justTitle = justTitle;
    }

    @JsonProperty("just_subject")
    public String getJustSubject() {
        return justSubject;
    }

    @JsonProperty("just_subject")
    public void setJustSubject(String justSubject) {
        this.justSubject = justSubject;
    }

    @JsonProperty("requested_duration_for")
    public String getRequestedDurationFor() {
        return requestedDurationFor;
    }

    @JsonProperty("requested_duration_for")
    public void setRequestedDurationFor(String requestedDurationFor) {
        this.requestedDurationFor = requestedDurationFor;
    }

    @JsonProperty("urgent_general_flag")
    public String getUrgentGeneralFlag() {
        return urgentGeneralFlag;
    }

    @JsonProperty("urgent_general_flag")
    public void setUrgentGeneralFlag(String urgentGeneralFlag) {
        this.urgentGeneralFlag = urgentGeneralFlag;
    }

    @JsonProperty("activated")
    public String getActivated() {
        return activated;
    }

    @JsonProperty("activated")
    public void setActivated(String activated) {
        this.activated = activated;
    }

    @JsonProperty("activated_by")
    public String getActivatedBy() {
        return activatedBy;
    }

    @JsonProperty("activated_by")
    public void setActivatedBy(String activatedBy) {
        this.activatedBy = activatedBy;
    }

    @JsonProperty("activated_time")
    public String getActivatedTime() {
        return activatedTime;
    }

    @JsonProperty("activated_time")
    public void setActivatedTime(String activatedTime) {
        this.activatedTime = activatedTime;
    }

    @JsonProperty("draft_save")
    public String getDraftSave() {
        return draftSave;
    }

    @JsonProperty("draft_save")
    public void setDraftSave(String draftSave) {
        this.draftSave = draftSave;
    }

    @JsonProperty("complete_save")
    public String getCompleteSave() {
        return completeSave;
    }

    @JsonProperty("complete_save")
    public void setCompleteSave(String completeSave) {
        this.completeSave = completeSave;
    }

    @JsonProperty("request_user_id")
    public String getRequestUserId() {
        return requestUserId;
    }

    @JsonProperty("request_user_id")
    public void setRequestUserId(String requestUserId) {
        this.requestUserId = requestUserId;
    }

    @JsonProperty("requestPSName")
    public String getRequestPSName() {
        return requestPSName;
    }

    @JsonProperty("requestPSName")
    public void setRequestPSName(String requestPSName) {
        this.requestPSName = requestPSName;
    }

    @JsonProperty("requeststateLI")
    public String getRequeststateLI() {
        return requeststateLI;
    }

    @JsonProperty("requeststateLI")
    public void setRequeststateLI(String requeststateLI) {
        this.requeststateLI = requeststateLI;
    }

    @JsonProperty("expired")
    public String getExpired() {
        return expired;
    }

    @JsonProperty("expired")
    public void setExpired(String expired) {
        this.expired = expired;
    }

    @JsonProperty("mcell_action_li")
    public String getMcellActionLi() {
        return mcellActionLi;
    }

    @JsonProperty("mcell_action_li")
    public void setMcellActionLi(String mcellActionLi) {
        this.mcellActionLi = mcellActionLi;
    }

    @JsonProperty("mcell_action_li_by")
    public String getMcellActionLiBy() {
        return mcellActionLiBy;
    }

    @JsonProperty("mcell_action_li_by")
    public void setMcellActionLiBy(String mcellActionLiBy) {
        this.mcellActionLiBy = mcellActionLiBy;
    }

    @JsonProperty("mcell_action_li_datetime")
    public String getMcellActionLiDatetime() {
        return mcellActionLiDatetime;
    }

    @JsonProperty("mcell_action_li_datetime")
    public void setMcellActionLiDatetime(String mcellActionLiDatetime) {
        this.mcellActionLiDatetime = mcellActionLiDatetime;
    }

    @JsonProperty("send_to_hs")
    public String getSendToHs() {
        return sendToHs;
    }

    @JsonProperty("send_to_hs")
    public void setSendToHs(String sendToHs) {
        this.sendToHs = sendToHs;
    }

    @JsonProperty("send_to_hs_by")
    public String getSendToHsBy() {
        return sendToHsBy;
    }

    @JsonProperty("send_to_hs_by")
    public void setSendToHsBy(String sendToHsBy) {
        this.sendToHsBy = sendToHsBy;
    }

    @JsonProperty("send_to_hs_datetime")
    public String getSendToHsDatetime() {
        return sendToHsDatetime;
    }

    @JsonProperty("send_to_hs_datetime")
    public void setSendToHsDatetime(String sendToHsDatetime) {
        this.sendToHsDatetime = sendToHsDatetime;
    }

    @JsonProperty("received_hs_order")
    public Object getReceivedHsOrder() {
        return receivedHsOrder;
    }

    @JsonProperty("received_hs_order")
    public void setReceivedHsOrder(Object receivedHsOrder) {
        this.receivedHsOrder = receivedHsOrder;
    }

    @JsonProperty("received_hs_order_by")
    public String getReceivedHsOrderBy() {
        return receivedHsOrderBy;
    }

    @JsonProperty("received_hs_order_by")
    public void setReceivedHsOrderBy(String receivedHsOrderBy) {
        this.receivedHsOrderBy = receivedHsOrderBy;
    }

    @JsonProperty("received_hs_order_datetime")
    public String getReceivedHsOrderDatetime() {
        return receivedHsOrderDatetime;
    }

    @JsonProperty("received_hs_order_datetime")
    public void setReceivedHsOrderDatetime(String receivedHsOrderDatetime) {
        this.receivedHsOrderDatetime = receivedHsOrderDatetime;
    }

    @JsonProperty("mcell_verify_view")
    public String getMcellVerifyView() {
        return mcellVerifyView;
    }

    @JsonProperty("mcell_verify_view")
    public void setMcellVerifyView(String mcellVerifyView) {
        this.mcellVerifyView = mcellVerifyView;
    }

    @JsonProperty("mcell_view_by")
    public Object getMcellViewBy() {
        return mcellViewBy;
    }

    @JsonProperty("mcell_view_by")
    public void setMcellViewBy(Object mcellViewBy) {
        this.mcellViewBy = mcellViewBy;
    }

    @JsonProperty("mcell_time")
    public String getMcellTime() {
        return mcellTime;
    }

    @JsonProperty("mcell_time")
    public void setMcellTime(String mcellTime) {
        this.mcellTime = mcellTime;
    }

    @JsonProperty("requestBriefDataSuperiors")
    public String getRequestBriefDataSuperiors() {
        return requestBriefDataSuperiors;
    }
    @JsonProperty("requestBriefDataSuperiors")
    public void setRequestBriefDataSuperiors(String requestBriefDataSuperiors) {
        this.requestBriefDataSuperiors = requestBriefDataSuperiors;
    }

    @JsonProperty("details")
    public List<Detail> getDetails() {
        return details;
    }

    @JsonProperty("details")
    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    @JsonProperty("notification_details")
    public List<NotificationDetail> getNotificationDetails() {
        return notificationDetails;
    }

    @JsonProperty("notification_details")
    public void setNotificationDetails(List<NotificationDetail> notificationDetails) {
        this.notificationDetails = notificationDetails;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
