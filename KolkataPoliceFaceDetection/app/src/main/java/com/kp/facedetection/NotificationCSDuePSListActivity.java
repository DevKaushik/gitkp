package com.kp.facedetection;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.kp.facedetection.interfaces.OnItemClickListenerForChargesheetDue;

import java.util.Observable;
import java.util.Observer;

public class NotificationCSDuePSListActivity extends BaseActivity implements OnItemClickListenerForChargesheetDue, View.OnClickListener, Observer {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_csdue_pslist);

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onClickChargesheetDue(View view, int position) {

    }

    @Override
    public void update(Observable observable, Object data) {

    }
}
