package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-Asset-131 on 22-03-2016.
 */
public class DivisionList implements Serializable{

    private String div_code = "";
    private String div_name = "";

    public String getDiv_name() {
        return div_name;
    }

    public void setDiv_name(String div_name) {
        this.div_name = div_name;
    }

    public String getDiv_code() {
        return div_code;
    }

    public void setDiv_code(String div_code) {
        this.div_code = div_code;
    }




}
