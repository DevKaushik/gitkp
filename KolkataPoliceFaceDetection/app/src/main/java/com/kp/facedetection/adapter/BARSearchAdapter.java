package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.BARDetails;
import com.kp.facedetection.model.SDRDetails;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by user on 15-02-2018.
 */

public class BARSearchAdapter extends BaseAdapter {
    private Context context;
    private List<BARDetails> barDetailsList;

    public BARSearchAdapter(Context context, List<BARDetails> barDetailsList) {
        this.context = context;
        this.barDetailsList = barDetailsList;
    }


    @Override
    public int getCount() {

        int size = (barDetailsList.size()>0)? barDetailsList.size():0;
        return size;

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        BARSearchAdapter.ViewHolder holder;
        if (convertView == null) {
            holder = new BARSearchAdapter.ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.bar_search_list_item_layout, null);


            holder.tv_slNo=(TextView)convertView.findViewById(R.id.tv_slNo);
            holder.tv_name=(TextView)convertView.findViewById(R.id.tv_name);
            holder.tv_address=(TextView)convertView.findViewById(R.id.tv_address);
            Constants.changefonts(holder.tv_slNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_name, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_address, context, "Calibri.ttf");


            convertView.setTag(holder);
        } else {
            holder = (BARSearchAdapter.ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);

        holder.tv_slNo.setText(sl_no);
        holder.tv_name.setText("NAME: "+barDetailsList.get(position).getName().trim());
        holder.tv_address.setText("ADDRESS: "+barDetailsList.get(position).getAddress().trim());

        return convertView;
    }

    class ViewHolder {

        TextView tv_slNo,tv_name,tv_address;

    }
}
