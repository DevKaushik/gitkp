package com.kp.facedetection.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 09-05-2018.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "request_id",
        "notify_content",
        "notify_flag",
        "notify_date_time",
        "notify_by",
        "notification_read_flag",
        "is_replied",
        "replied_against_id"
})
public class NotificationDetail implements Serializable {
    @JsonProperty("id")
    private String id;
    @JsonProperty("request_id")
    private String requestId;
    @JsonProperty("notify_content")
    private String notifyContent;
    @JsonProperty("notify_flag")
    private String notifyFlag;
    @JsonProperty("notify_date_time")
    private String notifyDateTime;
    @JsonProperty("notify_by")
    private String notifyBy;
    @JsonProperty("notification_read_flag")
    private String notificationReadFlag;
    @JsonProperty("is_replied")
    private String isReplied;
    @JsonProperty("replied_against_id")
    private String repliedAgainstId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("request_id")
    public String getRequestId() {
        return requestId;
    }

    @JsonProperty("request_id")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @JsonProperty("notify_content")
    public String getNotifyContent() {
        return notifyContent;
    }

    @JsonProperty("notify_content")
    public void setNotifyContent(String notifyContent) {
        this.notifyContent = notifyContent;
    }

    @JsonProperty("notify_flag")
    public String getNotifyFlag() {
        return notifyFlag;
    }

    @JsonProperty("notify_flag")
    public void setNotifyFlag(String notifyFlag) {
        this.notifyFlag = notifyFlag;
    }

    @JsonProperty("notify_date_time")
    public String getNotifyDateTime() {
        return notifyDateTime;
    }

    @JsonProperty("notify_date_time")
    public void setNotifyDateTime(String notifyDateTime) {
        this.notifyDateTime = notifyDateTime;
    }

    @JsonProperty("notify_by")
    public String getNotifyBy() {
        return notifyBy;
    }

    @JsonProperty("notify_by")
    public void setNotifyBy(String notifyBy) {
        this.notifyBy = notifyBy;
    }

    @JsonProperty("notification_read_flag")
    public String getNotificationReadFlag() {
        return notificationReadFlag;
    }

    @JsonProperty("notification_read_flag")
    public void setNotificationReadFlag(String notificationReadFlag) {
        this.notificationReadFlag = notificationReadFlag;
    }

    @JsonProperty("is_replied")
    public String getIsReplied() {
        return isReplied;
    }

    @JsonProperty("is_replied")
    public void setIsReplied(String isReplied) {
        this.isReplied = isReplied;
    }

    @JsonProperty("replied_against_id")
    public String getRepliedAgainstId() {
        return repliedAgainstId;
    }

    @JsonProperty("replied_against_id")
    public void setRepliedAgainstId(String repliedAgainstId) {
        this.repliedAgainstId = repliedAgainstId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
