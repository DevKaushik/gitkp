package com.kp.facedetection;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.adapter.ChargesheetDueListAdapter;
import com.kp.facedetection.interfaces.OnItemClickListenerForChargesheetDue;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;

import java.util.Observable;
import java.util.Observer;

public class NotificationCSDueCaseListActivity extends BaseActivity implements OnItemClickListenerForChargesheetDue, View.OnClickListener, Observer {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_csdue_case_list);
        ObservableObject.getInstance().addObserver(this);
        setToolBar();
        initView();
    }
    public void setToolBar(){

    }
    public void initView() {


    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onClickChargesheetDue(View view, int position) {

    }

    @Override
    public void update(Observable observable, Object data) {

    }
}
