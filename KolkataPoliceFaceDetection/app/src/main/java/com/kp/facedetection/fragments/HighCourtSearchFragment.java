package com.kp.facedetection.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.kp.facedetection.HighCourtCaseSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.model.CourtCaseCategoryListDetails;
import com.kp.facedetection.model.CourtRoomListDetails;
import com.kp.facedetection.model.CourtTypeListDetails;
import com.kp.facedetection.model.HCourtCaseDetails;
import com.kp.facedetection.model.HighCourtList;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by DAT-165 on 23-03-2017.
 */

public class HighCourtSearchFragment extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {


    private Spinner sp_sideValue, sp_caseYear_value, sp_courtNo_value, sp_category_value, sp_caseType_value;
    private TextView tv_date_start_value, tv_date_end_value;
    private EditText et_justice_value, et_caseNo_value, et_party_value, et_petitioner_value, et_briefNote_value;
    private ScrollView scrollView1;
    private Button btn_HC_search, btn_reset;

    private List<String> caseYearList = new ArrayList<String>();
    ArrayAdapter<String> caseYearAdapter;

    ArrayAdapter<CharSequence> sideAdapter;

    //--- variables for  Court room no
    protected String[] array_courtRoom;
    protected ArrayList<CharSequence> selectedCourtRoom = new ArrayList<CharSequence>();
    private String courtRoomString = "";

    public JSONArray jsonArray_courtRoomList;
    public JSONArray jsonArray_courtTypeList;
    public JSONArray jsonArray_caseCategoryList;
    public HighCourtList highCourtList;

    List<String> courtRoomArrayList = new ArrayList<String>();
    List<String> courtCaseTypeArrayList = new ArrayList<String>();
    List<String> courtCaseCategoryArrayList = new ArrayList<String>();

    private ArrayAdapter<String> courtRoomAdapter;
    private ArrayAdapter<String> courtCaseCategoryAdapter;
    private ArrayAdapter<String> courtCaseTypeAdapter;

    private String userId="";
    private String pageno="";
    private String totalResult="";
    private String[] keys;
    private String[] values;

    private List<HCourtCaseDetails> courtCaseSearchDetailsList;


    public static HighCourtSearchFragment newInstance( ) {

        HighCourtSearchFragment highCourtSearchFragment = new HighCourtSearchFragment();
        return highCourtSearchFragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.highcourt_case_search_layout, container, false);

        // Inflate the layout for this fragment
        return rootView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        userId= Utility.getUserInfo(getContext()).getUserId();
        sp_sideValue = (Spinner) view.findViewById(R.id.sp_sideValue);
        sp_caseYear_value = (Spinner) view.findViewById(R.id.sp_caseYear_value);
        sp_courtNo_value = (Spinner) view.findViewById(R.id.sp_courtNo_value);
        sp_category_value = (Spinner) view.findViewById(R.id.sp_category_value);
        sp_caseType_value = (Spinner) view.findViewById(R.id.sp_caseType_value);

        tv_date_start_value = (TextView) view.findViewById(R.id.tv_date_start_value);
        tv_date_end_value = (TextView) view.findViewById(R.id.tv_date_end_value);

        et_justice_value = (EditText) view.findViewById(R.id.et_justice_value);
        et_caseNo_value = (EditText) view.findViewById(R.id.et_caseNo_value);
        et_party_value = (EditText) view.findViewById(R.id.et_party_value);
        et_petitioner_value = (EditText) view.findViewById(R.id.et_petitioner_value);
        et_briefNote_value = (EditText) view.findViewById(R.id.et_briefNote_value);

        scrollView1 = (ScrollView) view.findViewById(R.id.scrollView1);

        btn_HC_search = (Button)view.findViewById(R.id.btn_HC_search);
        btn_reset = (Button)view.findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_HC_search);
        Constants.buttonEffect(btn_reset);


        // Case year Spinner set
        Calendar calendar = Calendar.getInstance();
        int current_year = calendar.get(Calendar.YEAR);
        caseYearList.clear();
        caseYearList.add("Enter Case Year");

        for(int i=current_year;i>=1980;i--){
            caseYearList.add(Integer.toString(i));
        }

        caseYearAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_custom_textcolor, caseYearList);
        caseYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_caseYear_value.setAdapter(caseYearAdapter);
        caseYearAdapter.notifyDataSetChanged();
        sp_caseYear_value.setSelection(0);




        // spinner set to Side
        sideAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Side_Array, R.layout.simple_spinner_item);

        sideAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_sideValue.setAdapter(sideAdapter);
        sp_sideValue.setSelection(0);


        // set court room value
        courtRoomArrayList.add("Select Court Room No.");
        courtRoomAdapter = new ArrayAdapter<String>(getActivity(),R.layout.simple_spinner_item, courtRoomArrayList);
        courtRoomAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_courtNo_value.setAdapter(courtRoomAdapter);


        // set court Category value
        courtCaseCategoryArrayList.add("Select Case Category");
        courtCaseCategoryAdapter = new ArrayAdapter<String>(getActivity(),R.layout.simple_spinner_item,courtCaseCategoryArrayList);
        courtCaseCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_category_value.setAdapter(courtCaseCategoryAdapter);

        // set Court Case Type value
        courtCaseTypeArrayList.add("Select Case Type");
        courtCaseTypeAdapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, courtCaseTypeArrayList);
        courtCaseTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_caseType_value.setAdapter(courtCaseTypeAdapter);

        // API call
        getCourtCaseData();

        clickEvents();

    }

    private void clickEvents(){
        sp_caseYear_value.setOnItemSelectedListener(this);

        tv_date_start_value.setOnClickListener(this);
        tv_date_end_value.setOnClickListener(this);
        btn_reset.setOnClickListener(this);
        btn_HC_search.setOnClickListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        sp_caseYear_value.setSelection(0);
    }


    // some list will populate after click on tab
    private void getCourtCaseData(){

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_COURT_CASE_CONTENT);
        taskManager.setCourtCaseListData(true);

        String[] keys = {};
        String[] values = {};
        taskManager.doStartTask(keys, values, true, true);
    }

    public void parseGetCourtCaseListDataResponse(String response) {
        //System.out.println("Court Case List Data: " + response);

        if(response != null && !response.equals("")){

            JSONObject jObj = null;
            try{
                jObj = new JSONObject(response);
                highCourtList = new HighCourtList();

                if(jObj != null && jObj.optString("status").equals("1") ){

                    highCourtList.setStatus(jObj.optString("status"));
                    highCourtList.setMessage(jObj.optString("message"));

                    jsonArray_courtRoomList = jObj.optJSONObject("result").getJSONArray("court_rooms");
                    jsonArray_courtTypeList = jObj.optJSONObject("result").getJSONArray("court_type");
                    jsonArray_caseCategoryList = jObj.optJSONObject("result").getJSONArray("court_case_category");

                    for(int i = 0 ; i < jsonArray_courtRoomList.length(); i++){

                        CourtRoomListDetails courtRoomListDetails = new CourtRoomListDetails(Parcel.obtain());
                        JSONObject row = jsonArray_courtRoomList.getJSONObject(i);
                        courtRoomListDetails.setCourtNo(row.optString("COURT_NO"));
                        courtRoomListDetails.setNumValue(row.getString("NUM_VALUE"));

                        courtRoomArrayList.add(row.optString("COURT_NO"));

                        highCourtList.setObj_courtRoomList(courtRoomListDetails);
                    }
                    courtRoomAdapter.notifyDataSetChanged();

                    for (int j= 0; j< jsonArray_courtTypeList.length(); j++){

                        CourtTypeListDetails courtTypeListDetails = new CourtTypeListDetails(Parcel.obtain());
                        JSONObject row = jsonArray_courtTypeList.getJSONObject(j);
                        courtTypeListDetails.setCaseType(row.optString("CASE_TYPE"));

                        courtCaseTypeArrayList.add(row.optString("CASE_TYPE"));
                        highCourtList.setObj_courtTypeList(courtTypeListDetails);
                    }
                    courtCaseTypeAdapter.notifyDataSetChanged();


                    for(int k = 0; k < jsonArray_caseCategoryList.length(); k++){

                        CourtCaseCategoryListDetails courtCaseCategoryListDetails = new CourtCaseCategoryListDetails(Parcel.obtain());
                        JSONObject row = jsonArray_caseCategoryList.getJSONObject(k);
                        courtCaseCategoryListDetails.setCaseCategory(row.optString("CASECATG"));

                        courtCaseCategoryArrayList.add(row.optString("CASECATG"));
                        highCourtList.setObj_caseCategoryList(courtCaseCategoryListDetails);
                    }
                    courtCaseCategoryAdapter.notifyDataSetChanged();

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



    private void resetDataFieldValues(){

        sp_sideValue.setSelection(0);
        sp_courtNo_value.setSelection(0);
        sp_category_value.setSelection(0);
        sp_caseType_value.setSelection(0);
        sp_caseYear_value.setSelection(0);

        tv_date_start_value.setText("");
        tv_date_end_value.setText("");

        et_justice_value.setText("");
        et_caseNo_value.setText("");
        et_party_value.setText("");
        et_petitioner_value.setText("");
        et_briefNote_value.setText("");
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_reset:
                resetDataFieldValues();
                break;

            case R.id.btn_HC_search:

                fetchData();
                break;

            case R.id.tv_date_start_value:
                DateUtils.setDate(getActivity(), tv_date_start_value, true);
                break;

            case R.id.tv_date_end_value:
                DateUtils.setDate(getActivity(), tv_date_end_value, true);
                break;
        }
    }


    private void fetchData(){
        String startDate = tv_date_start_value.getText().toString().trim();
        String endDate = tv_date_end_value.getText().toString().trim();

        if (!endDate.equalsIgnoreCase("") && startDate.equalsIgnoreCase("")) {
            showAlertDialog(getActivity(), " Search Error!!! ", "Please provide both dates", false);
        } else if (endDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("")) {
            showAlertDialog(getActivity(), " Search Error!!! ", "Please provide both dates", false);
        } else if (!endDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("")) {
            boolean date_result_flag = DateUtils.dateComparison(startDate, endDate);
            if (!date_result_flag) {
                showAlertDialog(getActivity(), " Search Error!!! ", "From date can't be greater than To date", false);
            } else {
                fetchHighCourtCaseSearchResult();
            }
        } else {
            fetchHighCourtCaseSearchResult();
        }
    }


    private void fetchHighCourtCaseSearchResult(){

        String date_from = tv_date_start_value.getText().toString().trim();
        String date_to = tv_date_end_value.getText().toString().trim();
        String justice_value = et_justice_value.getText().toString().toUpperCase().trim().replace("Enter Justice", "");
        String caseNo_value = et_caseNo_value.getText().toString().toUpperCase().trim().replace("Enter a Case Number", "");
        String party_value = et_party_value.getText().toString().toUpperCase().trim().replace("Enter Party Name", "");
        String advocate_value = et_petitioner_value.getText().toString().toUpperCase().trim().replace("Enter Petitioner's Advocate", "");
        String brief_value = et_briefNote_value.getText().toString().toUpperCase().trim().replace("Enter Brief Note", "");

        String side_value = sp_sideValue.getSelectedItem().toString().trim().replace("Select Side", "");
        String courtRoom_value = sp_courtNo_value.getSelectedItem().toString().trim().replace("Select Court Room No.", "");
        String category_value = sp_category_value.getSelectedItem().toString().trim().replace("Select Case Category", "");
        String caseType_value = sp_caseType_value.getSelectedItem().toString().trim().replace("Select Case Type", "");
        String caseYr_value = sp_caseYear_value.getSelectedItem().toString().trim().replace("Enter Case Year", "");

        if(!side_value.equalsIgnoreCase("") || !date_from.equalsIgnoreCase("") || !date_to.equalsIgnoreCase("") || !justice_value.equalsIgnoreCase("")
                || !caseNo_value.equalsIgnoreCase("") || !party_value.equalsIgnoreCase("") || !advocate_value.equalsIgnoreCase("")
                || !brief_value.equalsIgnoreCase("") || !courtRoom_value.equalsIgnoreCase("") || !category_value.equalsIgnoreCase("")
                || !caseType_value.equalsIgnoreCase("") || !caseYr_value.equalsIgnoreCase("")){

            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_COURT_CASE_LIST_SEARCH);
            taskManager.setCourtCaseListSearch(true);

            String[] keys = {"side", "from_date", "to_date", "court_no", "justice", "category", "case_type", "case_no", "case_yr", "party", "petitioner_advocate", "note", "page_no","user_id"};
            String[] values = {side_value.trim(), date_from.trim(), date_to.trim(), courtRoom_value.trim(), justice_value.trim(), category_value.trim(), caseType_value.trim(), caseNo_value.trim(), caseYr_value.trim(), party_value.trim(),advocate_value.trim(), brief_value.trim(), "1", userId};
            taskManager.doStartTask(keys, values, true);
        }
        else {

            showAlertDialog(getActivity(), " Search Error!!! ", "Please provide atleast one value for search", false);
        }

    }


    public void parseCourtCaseSearchResultResponse(String result, String[] keys, String[] values) {

        //System.out.println("parseCourtCaseSearchResultResponse: " + result);

        if(result != null && !result.equals("")){

            try {
                JSONObject jObj = new JSONObject(result);
                if(jObj.optString("status").equalsIgnoreCase("1")){

                    this.keys = keys;
                    this.values = values;
                    this.totalResult = jObj.optString("count").toString();
                    this.pageno = jObj.optString("page").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseCourtCaseSearchResponse(resultArray);
                }

                else{
                    showAlertDialog(getActivity()," Search Error ",jObj.optString("message"),false );
                }
            }
            catch(JSONException e){
                e.printStackTrace();
                showAlertDialog(getActivity(), " Search Error!!! ", "Some error has been encountered", false);
            }
        }

    }


    private void parseCourtCaseSearchResponse(JSONArray resultArray){

        courtCaseSearchDetailsList = new ArrayList<HCourtCaseDetails>();

        for(int i = 0; i < resultArray.length(); i++){

            JSONObject row = null;
            String justiceStr = "";
            try{
                row = resultArray.getJSONObject(i);

                HCourtCaseDetails hCourtCaseDetails = new HCourtCaseDetails(Parcel.obtain());

                if(!row.optString("SIDE").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setSideValue(row.optString("SIDE"));

                if(!row.optString("CASEDATE").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setCaseDate(row.optString("CASEDATE"));

                if(!row.optString("COURTNO").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setCourtNo(row.optString("COURTNO"));

                if(!row.optString("JUSTICE1").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setJustice1(row.optString("JUSTICE1"));

                if(!row.optString("JUSTICE2").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setJustice2(row.optString("JUSTICE2"));

                if(!row.optString("JUSTICE3").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setJustice3(row.optString("JUSTICE3"));

                if(!row.optString("CATEGORY").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setCategory(row.optString("CATEGORY"));

                if(!row.optString("CASETYPE").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setCaseType(row.optString("CASETYPE"));

                if(!row.optString("CASENO").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setCaseNo(row.optString("CASENO"));

                if(!row.optString("CASEYEAR").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setCaseYr(row.optString("CASEYEAR"));

                if(!row.optString("PARTY").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setParty(row.optString("PARTY"));

                if(!row.optString("ADDLCASE").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setAddLcase(row.optString("ADDLCASE"));

                if(!row.optString("ADV_PETITIONER").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setAdvPetitioner(row.optString("ADV_PETITIONER"));

                if(!row.optString("ADV_RESPDT").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setAdvResPdt(row.optString("ADV_RESPDT"));

                if(!row.optString("BRIEF_NOTE").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setBriefNote(row.optString("BRIEF_NOTE"));

                if(!row.optString("OUTCOME").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setOutCome(row.optString("OUTCOME"));

                if(!row.optString("NEXTDATE").equalsIgnoreCase("null"))
                    hCourtCaseDetails.setNextDate(row.optString("NEXTDATE"));

                courtCaseSearchDetailsList.add(hCourtCaseDetails);

            }

            catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Intent intent=new Intent(getActivity(), HighCourtCaseSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putExtra("COURT_CASE_SEARCH_LIST", (ArrayList<? extends Parcelable>) courtCaseSearchDetailsList );
        startActivity(intent);
    }



    private void showAlertDialog(Context context, String title, final String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }
}
