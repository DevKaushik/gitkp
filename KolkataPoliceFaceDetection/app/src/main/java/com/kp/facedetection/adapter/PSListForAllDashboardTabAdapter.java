package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnItemClickListenerForAllFIRView;
import com.kp.facedetection.model.CrimeReviewDetails;

import java.util.ArrayList;

/**
 * Created by DAT-165 on 07-06-2017.
 */

public class PSListForAllDashboardTabAdapter extends RecyclerView.Adapter<PSListForAllDashboardTabAdapter.MyViewHolder> {

    Context con;
    ArrayList<CrimeReviewDetails> psList = new ArrayList<>();
    private OnItemClickListenerForAllFIRView allFirViewClickListener;

    public PSListForAllDashboardTabAdapter(Context con, ArrayList<CrimeReviewDetails> psList) {
        this.con = con;
        this.psList = psList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.crime_review_list_item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final CrimeReviewDetails crimeReviewDetails = psList.get(position);
        if (crimeReviewDetails.getCrimeCategory() != null && !crimeReviewDetails.getCrimeCategory().equalsIgnoreCase("")
                && !crimeReviewDetails.getCrimeCategory().equalsIgnoreCase("null")) {
            holder.tv_categoryName.setText(crimeReviewDetails.getCrimeCategory());
        }
        if (crimeReviewDetails.getCountOfCrime() != null && !crimeReviewDetails.getCountOfCrime().equalsIgnoreCase("")
                && !crimeReviewDetails.getCountOfCrime().equalsIgnoreCase("null")) {

            holder.tv_count.setText(crimeReviewDetails.getCountOfCrime());
        }
        if(crimeReviewDetails.getCountOfCrimeDue()!=null && !crimeReviewDetails.getCountOfCrimeDue().equalsIgnoreCase("")&& !crimeReviewDetails.getCountOfCrimeDue().equalsIgnoreCase("null")){
            holder.tv_count_due.setText(crimeReviewDetails.getCountOfCrimeDue());
        }

    }

    @Override
    public int getItemCount() {
        return psList == null ? 0 : psList.size();
    }

    public void setClickListener(OnItemClickListenerForAllFIRView allFirViewClickListener) {
        this.allFirViewClickListener = allFirViewClickListener;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_categoryName, tv_count ,tv_count_due;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_categoryName = (TextView) itemView.findViewById(R.id.tv_categoryName);
            tv_count = (TextView) itemView.findViewById(R.id.tv_count);
           // tv_count_due = (TextView) itemView.findViewById(R.id.tv_count_due);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (allFirViewClickListener != null) {
                allFirViewClickListener.onClick(view, getAdapterPosition());
            }
        }
    }
}
