package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.CoordinatorLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kp.facedetection.adapter.CableSearchAdapter;
import com.kp.facedetection.model.CableSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by DAT-165 on 07-12-2016.
 */
public class CableSearchResultActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, Observer {

    private String[] keys;
    private String[] values;
    private String pageno;
    private String totalResult = "";
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private TextView tv_resultCount;
    private ListView lv_CableSearchList;
    private Button btnLoadMore;
    private TextView tv_watermark;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    CableSearchAdapter cableSearchAdapter;
    private List<CableSearchDetails> cableSearchDeatilsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sdrsearch_result);
        ObservableObject.getInstance().addObserver(this);
        initialization();
        clickEvents();
    }

    private void initialization() {

        tv_resultCount = (TextView) findViewById(R.id.tv_resultCount);
        lv_CableSearchList = (ListView) findViewById(R.id.lv_sdrSearchList);

        // LoadMore button
        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        keys = getIntent().getStringArrayExtra("keys");
        values = getIntent().getStringArrayExtra("value");
        pageno = getIntent().getStringExtra("pageno");
        totalResult = getIntent().getStringExtra("totalResult");

        Bundle bundle = getIntent().getExtras();
        cableSearchDeatilsList = bundle.getParcelableArrayList("CABLE_SEARCH_LIST");

        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }

        cableSearchAdapter = new CableSearchAdapter(this,cableSearchDeatilsList);
        lv_CableSearchList.setAdapter(cableSearchAdapter);
        cableSearchAdapter.notifyDataSetChanged();

        if (Integer.parseInt(totalResult) > 20) {

            // Adding Load More button to listview at bottom
            lv_CableSearchList.addFooterView(btnLoadMore);
        }

        lv_CableSearchList.setOnItemClickListener(this);

         /*  Set user name as Watermark  */
        tv_watermark = (TextView) findViewById(R.id.tv_watermark);
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-55);

    }


    private void clickEvents() {

        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // Starting a new async task
                fetchCableSearchResultPagination();
            }
        });
    }


    private void fetchCableSearchResultPagination() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.MODIFIED_METHOD_DOCUMENT_CABLE_SEARCH);
        taskManager.setDocumentCableSearchPagination(true);

        values[2] = (Integer.parseInt(pageno) + 1) + "";

        taskManager.doStartTask(keys, values, true, true);

    }


    public void parseDocumentCableSearchPagination(String result, String[] keys, String[] values) {

      //  System.out.println("Document Cable Search Result Pagination: " + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("STATUS").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("PAGE").toString();
                   // totalResult = jObj.opt("COUNT").toString();

                    JSONArray resultArray=jObj.getJSONArray("RESULT");
                    parseCableSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(this," Search Error ",jObj.optString("MESSAGE"),false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    private void parseCableSearchResponse(JSONArray resultArray) {

        for (int i = 0; i < resultArray.length(); i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj = resultArray.getJSONObject(i);

                CableSearchDetails cableSearchDetails = new CableSearchDetails(Parcel.obtain());

                if(!jsonObj.optString("NAME").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderName(jsonObj.optString("NAME"));
                }

                if(!jsonObj.optString("ADDRESS").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderAddress(jsonObj.optString("ADDRESS"));
                }

                if(!jsonObj.optString("CITY").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderCity(jsonObj.optString("CITY"));
                }

                if(!jsonObj.optString("STATE").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderState(jsonObj.optString("STATE"));
                }

                if(!jsonObj.optString("ZONE").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderZone(jsonObj.optString("ZONE"));
                }

                if(!jsonObj.optString("PINCODE").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderPincode(jsonObj.optString("PINCODE"));
                }

                if(!jsonObj.optString("STB").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderSTBNo(jsonObj.optString("STB"));
                }

                if(!jsonObj.optString("VC").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_VCNo(jsonObj.optString("VC"));
                }

                if(!jsonObj.optString("MOBILENUMBER").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderMobileNo(jsonObj.optString("MOBILENUMBER"));
                }

                if(!jsonObj.optString("OFFICENUMBER").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderOfficeNo(jsonObj.optString("OFFICENUMBER"));
                }

                cableSearchDeatilsList.add(cableSearchDetails);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

        int currentPosition = lv_CableSearchList.getFirstVisiblePosition();
        int lastVisiblePosition = lv_CableSearchList.getLastVisiblePosition();

        // Setting new scroll position
        lv_CableSearchList.setSelectionFromTop(currentPosition + 1, 0);

        if((Integer.parseInt(totalResult)-lastVisiblePosition)<20){
            lv_CableSearchList.removeFooterView(btnLoadMore);
        }
    }


    /*
    *      Function to display simple Alert Dialog
    * */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent in = new Intent(CableSearchResultActivity.this,CableSearchDetailsActivity.class);
        in.putExtra("POSITION",position);
        in.putParcelableArrayListExtra("CABLE_SEARCH_RESULT", (ArrayList<? extends Parcelable>) cableSearchDeatilsList);
        //in.putParcelableArrayListExtra()
        startActivity(in);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }
    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
