package com.kp.facedetection.interfaces;

import android.view.View;

/**
 * Created by DAT-165 on 08-06-2017.
 */

public interface OnCaptureTagClickForListener {
    void onCaptureTag(View view, int position);
}
