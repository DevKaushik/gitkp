package com.kp.facedetection.interfaces;

/**
 * Created by DAT-165 on 01-06-2017.
 */

public interface OnCaptureAddressUpdateListener {
    void addressLatLongUpdate(String address, String latitude, String longitude, boolean visible);
}
