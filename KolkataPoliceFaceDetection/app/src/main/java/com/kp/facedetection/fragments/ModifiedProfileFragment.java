package com.kp.facedetection.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.CriminalDetailActivity;
import com.kp.facedetection.ModifiedCaseSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.adapter.AssociateDetailsAdapter;
import com.kp.facedetection.adapter.ExpandableListAdapterForProfile;
import com.kp.facedetection.interfaces.OnDescriptiveRoleUpdateListener;
import com.kp.facedetection.model.AssociateDetails;
import com.kp.facedetection.model.CriminalDetails;
import com.kp.facedetection.model.DescriptiveRoleModelModified;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by DAT-Asset-131 on 02-07-2016.
 */
public class ModifiedProfileFragment extends Fragment implements OnDescriptiveRoleUpdateListener {

    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String ALIAS = "ALIAS";
    public static final String FATHER_NAME = "FATHER_NAME";
    public static final String AGE = "AGE";
    public static final String SEX = "SEX";
    public static final String NATIONALITY = "NATIONALITY";
    public static final String RELIGION = "RELIGION";
    public static final String FLAG = "FLAG";
    public static final String CASE_NO = "CASE_NO";
    public static final String CASE_YR = "CASE_YR";
    public static final String SL_NO = "SL_NO";
    public static final String PS_CODE = "PS_CODE";
    public static final String CRS_GENERAL_ID = "CRS_GENERAL_ID";
    public static final String CRIME_NO = "CRIME_NO";
    public static final String CRIME_YR = "CRIME_YR";
    public static final String PROV_CRM_NO = "PROV_CRM_NO";

    private int mPage;
    private String flag="";
    private String alias="";
    private String father_name="";
    private String age="";
    private String sex="";
    private String nationality="";
    private String religion="";
    private String ps_code="";
    private String case_no="";
    private String case_year="";
    private String sl_no="";
    private String crs_generalID="";
    private String crime_no="";
    private String crime_year="";
    private String prov_crm_no="";
    private String prov_crm_no_general="";


    private String beard = "Beard^";
    private String birth_mark="Birth Mark^";
    private String built="Built^";
    private String burn_mark="Burn Mark^";
    private String complexion="Complexion^";
    private String cut_mark="Cut Mark^";
    private String deformity="Deformity^";
    private String ear="Ear^";
    private String eye="Eye^";
    private String eye_brow="Eye Brow^";
    private String face="Face^";
    private String forehead="Forehead^";
    private String hair="Hair^";
    private String mole="Mole^";
    private String moustache="Moustache^";
    private String nose="Nose^";
    private String scar_mark="Scar Mark^";
    private String tattoo_mark="Tattoo Mark^";
    private String wart_mark="Wart Mark^";

    private String crimeYear = "Crime Year^";
    private String crimeNo = "Crime No^";
    private String photos = "Photo(s)^";
    private StringBuilder photo_id;

    private ExpandableListView lvExp;
    private TextView tv_noData;

    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    ExpandableListAdapterForProfile listAdapter;

    List<DescriptiveRoleModelModified> descriptiveRoleList = new ArrayList<DescriptiveRoleModelModified>();
    List<String> CRSList = new ArrayList<String>();
    List<String> CRSImageList = new ArrayList<String>();

    AssociateDetails associateDetails;
    private List<AssociateDetails> associateDetailsList = new ArrayList<AssociateDetails>();

    private View inflatedView;
    private PopupWindow popWindow;

    private String associate_msg="";
    private String descriptive_msg="";

    private ArrayList<CriminalDetails> criminalAssosiateList;

    public static ModifiedProfileFragment newInstance(int page,String flag,String alias,String father_name,String age,String sex,String nationality,String religion,String case_no,String case_year,String ps_code,String sl_no,String crs_generalID,String crime_no,String crime_year, String prov_crm_no) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        args.putString(FLAG, flag);
        args.putString(ALIAS, alias);
        args.putString(FATHER_NAME, father_name);
        args.putString(AGE, age);
        args.putString(SEX, sex);
        args.putString(NATIONALITY, nationality);
        args.putString(RELIGION, religion);
        args.putString(PS_CODE, ps_code);
        args.putString(CASE_NO, case_no);
        args.putString(CASE_YR, case_year);
        args.putString(SL_NO, sl_no);
        args.putString(CRS_GENERAL_ID, crs_generalID);
        args.putString(CRIME_NO, crime_no);
        args.putString(CRIME_YR, crime_year);
        args.putString(PROV_CRM_NO, prov_crm_no);
        ModifiedProfileFragment fragment = new ModifiedProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        flag = getArguments().getString(FLAG);
        alias = getArguments().getString(ALIAS);
        father_name = getArguments().getString(FATHER_NAME);
        age = getArguments().getString(AGE);
        sex = getArguments().getString(SEX);
        nationality = getArguments().getString(NATIONALITY);
        religion = getArguments().getString(RELIGION);
        ps_code = getArguments().getString(PS_CODE).replace("PSCODE: ", "");
        case_no = getArguments().getString(CASE_NO).replace("CASENO: ", "");
        case_year = getArguments().getString(CASE_YR).replace("CASEYR: ", "");
        sl_no = getArguments().getString(SL_NO).replace("SLNO: ", "");
        crs_generalID = getArguments().getString(CRS_GENERAL_ID);
        crime_no = getArguments().getString(CRIME_NO).replace("CRIMENO: ", "");
        crime_year = getArguments().getString(CRIME_YR).replace("CRIMEYEAR: ", "");
     /*   if(getArguments().getString(PROV_CRM_NO).contains("PROV_CRM_NO:")) {
            String prv_crm_no = "Criminal No^";
            prov_crm_no_general = prv_crm_no + getArguments().getString(PROV_CRM_NO).replace("PROV_CRM_NO:","");
            prov_crm_no = getArguments().getString(PROV_CRM_NO).replace("PROV_CRM_NO:","");
        }*/

        String prv_crm_no = "Criminal No^";
        prov_crm_no_general = prv_crm_no + getArguments().getString(PROV_CRM_NO);
        prov_crm_no = getArguments().getString(PROV_CRM_NO);

        System.out.println("--------------------------Profile-----------------------------");
        System.out.println("Flag: " + flag);
        System.out.println("Alias: " + alias);
        System.out.println("Father's Name: " + father_name);
        System.out.println("Age: " + age);
        System.out.println("Gender: " + sex);
        System.out.println("Nationality: " + nationality);
        System.out.println("Religion: " + religion);
        System.out.println("PS Code: " + ps_code);
        System.out.println("Case No: " + case_no);
        System.out.println("Case Yr: " + case_year);
        System.out.println("SL No: " + sl_no);
        System.out.println("CRS General ID: " + crs_generalID);
        System.out.println("Crime No: " + crime_no);
        System.out.println("Crime Yr: " + crime_year);
        System.out.println("PROV CRIMINAL NO GENERAL: " + prov_crm_no_general);
        System.out.println("PROV CRIMINAL NO : " + prov_crm_no);

        System.out.println("--------------------------Profile-----------------------------");


    }

    // Inflate the fragment layout we defined above for this fragment
    // Set the associated text for the title
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        tv_noData = (TextView) view.findViewById(R.id.tv_noData);
        lvExp = (ExpandableListView) view.findViewById(R.id.lvExp);


        Utility.changefonts(tv_noData, getActivity(), "Calibri.ttf");

        tv_noData.setVisibility(View.GONE);
        lvExp.setVisibility(View.GONE);


        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        CRSList.clear();
        CRSImageList.clear();
        associateDetailsList.clear();
        descriptiveRoleList.clear();

        setProfileData();

        getDescriptiveRole();

        clickEvents();

    }

    private void setProfileData(){

        lvExp.setVisibility(View.VISIBLE);

        listDataHeader.add("General");
        listDataHeader.add("Descriptive Role");
        listDataHeader.add("CRS");
        listDataHeader.add("Associate");

        List<String> list_general = new ArrayList<>();
        list_general.add(prov_crm_no_general);
        list_general.add(alias);
        list_general.add(father_name);
        list_general.add(age);
        list_general.add(sex);
        list_general.add(nationality);
        list_general.add(religion);
        listDataChild.put(listDataHeader.get(listDataHeader.indexOf("General")), list_general);
    }


    private void getDescriptiveRole(){

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_PROFILE_DESCRIPTIVE_ROLE);
        taskManager.setCriminalProfileDescriptiveRole(true);

        String[] keys = new String[0];
        String[] values = new String[0];

       if(flag.equalsIgnoreCase("OC")){

            //CRSGeneralId  - Md. Zoomrati
            keys = new String[] {"flag","crsgeneralid"};
            values = new String[] {flag.trim(), crs_generalID.trim()};

        }else if(flag.equalsIgnoreCase("CF") || flag.equalsIgnoreCase("OF") ){

           if(case_year.equalsIgnoreCase("")){
               if(!crime_year.equalsIgnoreCase("")){
                   case_year = crime_year;
               }
           }
            //caseno, caseyr,pscode,slno
            keys = new String[] { "flag","pscode","caseno","caseyr","slno","prov_crm_no" };
            values = new String[] { flag,ps_code,case_no,case_year,sl_no, prov_crm_no};

        }
        //new added
        else {
           if(case_year.equalsIgnoreCase("")){
               if(!crime_year.equalsIgnoreCase("")){
                   case_year = crime_year;
               }
           }
           //caseno, caseyr,pscode,slno
           keys = new String[] { "flag","pscode","caseno","caseyr","slno","prov_crm_no" };
           values = new String[] { "OF",ps_code,case_no,case_year,sl_no, prov_crm_no};


       }

        taskManager.doStartTask(keys, values, true );

    }

    public void parseCriminalDescriptiveRoleResponse(String response ){

        System.out.println("parseCriminalDescriptiveRoleResponse: " + response);
        L.e(response);

        if (response != null && !response.equals("")) {

            /* get CRS Data */
            getCRS();

            try {
                JSONObject jObj = new JSONObject(response);
                descriptive_msg = jObj.optString("message");
                if(jObj.optString("status").equalsIgnoreCase("1")){
                    JSONObject obj_result = jObj.optJSONObject("result");
                    if(obj_result.length()>0) {
                        parseJSONObject(obj_result);
                    }

                }
                else{
                    List<String> list_desc = new ArrayList<>();
                    listDataChild.put(listDataHeader.get(listDataHeader.indexOf("Descriptive Role")), list_desc);
                }
               /* else{

                   setDescriptiveRoleForWarrentArrest();
                  //  showAlertDialog(getActivity(), Constants.ERROR, *//*Constants.ERROR_EXCEPTION_MSG*//*"No record of descriptive role found", false);

                }*/

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        else {
            Utility.showToast(getActivity(), Constants.ERROR_EXCEPTION_MSG, "long");
        }

    }
    public void parseCriminalDescriptiveRoleResponseUpdate(String response ){
        if(response!=null && !response.equals(""))
        {
            try{
                JSONObject jObj= new JSONObject(response);
                if(jObj.optString("status").equalsIgnoreCase("1"))
                {
                    String mess= jObj.optString("message");
                    L.e("Update Result:"+mess);
                    showAlertDialog(getActivity(), "Success", Constants.SUCCESS, true);
                }
                else
                {

                    showAlertDialog(getActivity(), Constants.ERROR, Constants.UPDATION_ERROR, false);
                }
            }catch (JSONException e)
            {
                e.printStackTrace();
                showAlertDialog(getActivity(), Constants.ERROR, Constants.ERROR_EXCEPTION_MSG, false);


            }
        }

    }

    public void parseJSONObject(JSONObject jobj){
        L.e("descriptive role:----->" + jobj);
        List<String> list_desc = new ArrayList<>();
        JSONArray beardArray=jobj.optJSONArray("BEARD");
        addToDescriptiveRoleList(beardArray,"BEARD");
        list_desc.add("BEARD");
        JSONArray birthMarkArray=jobj.optJSONArray("BIRTHMARK");
        addToDescriptiveRoleList(birthMarkArray,"BIRTHMARK");
        list_desc.add("BIRTHMARK");
        JSONArray buildArray=jobj.optJSONArray("BUILT");
        addToDescriptiveRoleList(buildArray,"BUILT");
        list_desc.add("BUILT");
        JSONArray burnmarkArray=jobj.optJSONArray("BURNMARK");
        addToDescriptiveRoleList(burnmarkArray,"BURNMARK");
        list_desc.add("BURNMARK");
        JSONArray complexionArray=jobj.optJSONArray("COMPLEXION");
        addToDescriptiveRoleList(complexionArray,"COMPLEXION");
        list_desc.add("COMPLEXION");
        JSONArray cut_markArray=jobj.optJSONArray("CUTMARK");
        addToDescriptiveRoleList(cut_markArray,"CUTMARK");
        list_desc.add("CUTMARK");
        JSONArray deformityArray=jobj.optJSONArray("DEFORMITY");
        addToDescriptiveRoleList(deformityArray,"DEFORMITY");
        list_desc.add("DEFORMITY");
        JSONArray earArray=jobj.optJSONArray("EAR");
        addToDescriptiveRoleList(earArray,"EAR");
        list_desc.add("EAR");
        JSONArray eyeArray=jobj.optJSONArray("EYE");
        addToDescriptiveRoleList(eyeArray,"EYE");
        list_desc.add("EYE");
        JSONArray eye_browArray=jobj.optJSONArray("EYEBROW");
        addToDescriptiveRoleList(eye_browArray,"EYEBROW");
        list_desc.add("EYEBROW");
        JSONArray faceArray=jobj.optJSONArray("FACE");
        addToDescriptiveRoleList(faceArray,"FACE");
        list_desc.add("FACE");
        JSONArray foreheadArray=jobj.optJSONArray("FOREHEAD");
        addToDescriptiveRoleList(foreheadArray,"FOREHEAD");
        list_desc.add("FOREHEAD");
        JSONArray hairArray=jobj.optJSONArray("HAIR");
        addToDescriptiveRoleList(hairArray,"HAIR");
        list_desc.add("HAIR");
        JSONArray moleArray=jobj.optJSONArray("MOLE");
        addToDescriptiveRoleList(moleArray,"MOLE");
        list_desc.add("MOLE");
        JSONArray moustacheArray=jobj.optJSONArray("MOUSTACHE");
        addToDescriptiveRoleList(moustacheArray,"MOUSTACHE");
        list_desc.add("MOUSTACHE");
        JSONArray noseArray=jobj.optJSONArray("NOSE");
        addToDescriptiveRoleList(noseArray,"NOSE");
        list_desc.add("NOSE");
        JSONArray scar_markArray=jobj.optJSONArray("SCARMARK");
        addToDescriptiveRoleList(scar_markArray,"SCARMARK");
        list_desc.add("SCARMARK");
        JSONArray tattoo_markArray=jobj.optJSONArray("TATTOOMARK");
        addToDescriptiveRoleList(tattoo_markArray,"TATTOOMARK");
        list_desc.add("TATTOOMARK");
        JSONArray wart_markArray=jobj.optJSONArray("WARTMARK");
        addToDescriptiveRoleList(wart_markArray,"WARTMARK");
        list_desc.add("WARTMARK");
        JSONArray hight_feet=jobj.optJSONArray("HEIGHT_FEET");
        addToDescriptiveRoleList(hight_feet,"HEIGHT_FEET");
        list_desc.add("HEIGHT_FEET");
        JSONArray hight_inch=jobj.optJSONArray("HEIGHT_INCH");
        addToDescriptiveRoleList(hight_inch,"HEIGHT_INCH");
        list_desc.add("HEIGHT_INCH");
        /*  descriptiveRoleList.add(beard+jobj.optString("BEARD").replace("null", ""));
        descriptiveRoleList.add(birth_mark + jobj.optString("BIRTHMARK").replace("null", ""));
        descriptiveRoleList.add(built + jobj.optString("BUILT").replace("null", ""));
        descriptiveRoleList.add(burn_mark + jobj.optString("BURNMARK").replace("null", ""));
        descriptiveRoleList.add(complexion + jobj.optString("COMPLEXION").replace("null", ""));
        descriptiveRoleList.add(cut_mark + jobj.optString("CUTMARK").replace("null", ""));
        descriptiveRoleList.add(deformity + jobj.optString("DEFORMITY").replace("null", ""));
        descriptiveRoleList.add(ear + jobj.optString("EAR").replace("null", ""));
        descriptiveRoleList.add(eye + jobj.optString("EYE").replace("null",""));
        descriptiveRoleList.add(eye_brow + jobj.optString("EYEBROW").replace("null",""));
        descriptiveRoleList.add(face + jobj.optString("FACE").replace("null",""));
        descriptiveRoleList.add(forehead + jobj.optString("FOREHEAD").replace("null",""));
        descriptiveRoleList.add(hair + jobj.optString("HAIR").replace("null",""));
        descriptiveRoleList.add(mole + jobj.optString("MOLE").replace("null",""));
        descriptiveRoleList.add(moustache + jobj.optString("MOUSTACHE").replace("null",""));
        descriptiveRoleList.add(nose + jobj.optString("NOSE").replace("null",""));
        descriptiveRoleList.add(scar_mark + jobj.optString("SCARMARK").replace("null", ""));
        descriptiveRoleList.add(tattoo_mark + jobj.optString("TATTOOMARK").replace("null",""));
        descriptiveRoleList.add(wart_mark + jobj.optString("WARTMARK").replace("null", ""));*/
        listDataChild.put(listDataHeader.get(listDataHeader.indexOf("Descriptive Role")), list_desc);
    }
    public void addToDescriptiveRoleList(JSONArray jsonArray,String category){
        DescriptiveRoleModelModified descriptiveRoleModelModified=new DescriptiveRoleModelModified();
        if(category.equalsIgnoreCase("HEIGHT_FEET")||category.equalsIgnoreCase("HEIGHT_INCH"))
        {
            descriptiveRoleModelModified.setCategory(category);
            try {
                JSONObject Objs2 = jsonArray.getJSONObject(0);
                if(!Objs2.optString("value").equalsIgnoreCase("null")) {
                    descriptiveRoleModelModified.setIndValue(Objs2.optString("value"));
                }
                else
                {
                    descriptiveRoleModelModified.setIndValue("");
                }
                L.e(Objs2.optString("value"));


            } catch (JSONException e) {

            }



        }
        else {
            ArrayList<String> Key = new ArrayList<String>();
            ArrayList<String> Selected = new ArrayList<String>();
            descriptiveRoleModelModified.setCategory(category);
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject Objs = jsonArray.getJSONObject(i);
                    Key.add(Objs.optString("key"));
                    Selected.add(Objs.optString("isselected"));

                } catch (JSONException e) {

                }
            }

            descriptiveRoleModelModified.setValue(Key);
            descriptiveRoleModelModified.setIsSelected(Selected);
        }
        descriptiveRoleList.add(descriptiveRoleModelModified);

    }

    private void setDescriptiveRoleForWarrentArrest(){

      /*  descriptiveRoleList.add(beard);
        descriptiveRoleList.add(birth_mark);
        descriptiveRoleList.add(built);
        descriptiveRoleList.add(burn_mark);
        descriptiveRoleList.add(complexion );
        descriptiveRoleList.add(cut_mark);
        descriptiveRoleList.add(deformity);
        descriptiveRoleList.add(ear);
        descriptiveRoleList.add(eye);
        descriptiveRoleList.add(eye_brow);
        descriptiveRoleList.add(face);
        descriptiveRoleList.add(forehead);
        descriptiveRoleList.add(hair);
        descriptiveRoleList.add(mole);
        descriptiveRoleList.add(moustache);
        descriptiveRoleList.add(nose);
        descriptiveRoleList.add(scar_mark);
        descriptiveRoleList.add(tattoo_mark);
        descriptiveRoleList.add(wart_mark);

        listDataChild.put(listDataHeader.get(listDataHeader.indexOf("Descriptive Role")), descriptiveRoleList);
*/
    }

    private void getCRS(){

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_PROFILE_CRS);
        taskManager.setCriminalProfileCRS(true);


        String[] keys = {"flag","crimeno","crimeyear"};
        String[] values ={"OF", crime_no.trim(),crime_year.trim()};

        taskManager.doStartTask(keys, values, true );

    }

    public void parseCriminalCRS(String response){

        //System.out.println("parseCriminalCRSResponse: " + response);

        if (response != null && !response.equals("")) {


            /* get Associates Data */
            getAssociate();

            try {
                JSONObject jObj = new JSONObject(response);
                if(jObj.optString("status").equalsIgnoreCase("1")){

                    JSONArray array_result = jObj.optJSONArray("result");
                    if(array_result.length()>0){
                        parseJSONArrayForCRS(array_result);
                    }
                    else{
                        setCRSData();
                    }

                }
                else{

                    setCRSData();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        else {
            Utility.showToast(getActivity(), Constants.ERROR_EXCEPTION_MSG, "long");
        }

    }

    public void parseJSONArrayForCRS(JSONArray crs){

        photo_id = new StringBuilder(photos);

        for(int i=0;i<crs.length();i++){
            JSONObject obj_crs = crs.optJSONObject(i);
            Iterator iterator_crs = obj_crs.keys();

            while(iterator_crs.hasNext()){

                String key = (String)iterator_crs.next();
                photo_id.append(key+"  ");
                if(!obj_crs.optString(key).equalsIgnoreCase("null")) {
                    String value = obj_crs.optString(key);
                    CRSImageList.add(value);
                }

            }
        }

        CRSList.add(crimeYear + crime_year);
        CRSList.add(crimeNo+crime_no);
        CRSList.add(photo_id.toString());

        listDataChild.put(listDataHeader.get(listDataHeader.indexOf("CRS")), CRSList);

      /*  listAdapter = new ExpandableListAdapterForProfile(getActivity(), listDataHeader, listDataChild,descriptiveRoleList,CRSList,CRSImageList);

        // setting list adapter
        lvExp.setAdapter(listAdapter);
        lvExp.expandGroup(0);*/

    }

    private void setCRSData(){

        CRSList.add(crimeYear + crime_year);
        CRSList.add(crimeNo+crime_no);
        CRSList.add(photos);

        listDataChild.put(listDataHeader.get(listDataHeader.indexOf("CRS")), CRSList);

       /* listAdapter = new ExpandableListAdapterForProfile(getActivity(), listDataHeader, listDataChild,descriptiveRoleList,CRSList,CRSImageList);

        // setting list adapter
        lvExp.setAdapter(listAdapter);
        lvExp.expandGroup(0);*/

    }


    private void getAssociate() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_PROFILE_ASSOCIATES);
        taskManager.setCriminalProfileAssociates(true);


        String[] keys = {"flag", "crimeno", "crimeyear"};
        String[] values = {"OF", crime_no.trim(), crime_year.trim()};
      //  String[] values = {"CF", "1193", "2015"};

        taskManager.doStartTask(keys, values, true);
    }


    public void parseCriminalAssociates(String response) {

        System.out.println("parseCriminalAssociateResponse: " + response);

        if (response != null && !response.equals("")) {

            try {
                JSONObject jObj = new JSONObject(response);
                associate_msg = jObj.optString("message");
                if (jObj.optString("status").equalsIgnoreCase("1")) {


                    JSONArray array_result = jObj.getJSONArray("result");
                    if (array_result.length() > 0) {
                        try {
                            parseJSONArrayForAssociates(array_result);
                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                    } else {
                        setAssociateData();

                    }

                } else {
                    setAssociateData();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            Utility.showToast(getActivity(), Constants.ERROR_EXCEPTION_MSG, "long");
        }

    }

    public void parseJSONArrayForAssociates(JSONArray associate_array) {


        List<String> list_associate = new ArrayList<>();

        for(int i=0;i<associate_array.length();i++){
         /*   JSONObject jobj= null;
            try {
                jobj = (JSONObject) associate_array.get(i);
                jobj.put("PROV_CRM_NO","153998");
                associate_array.put(i,jobj);
                Log.e("TAG----","JSON ARRAY ASSOCIATE"+associate_array);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
            JSONObject associateObj = associate_array.optJSONObject(i);
            associateDetails=new AssociateDetails();
            if(!associateObj.optString("name").contains("null"))
                associateDetails.setAssociate_name("Name: " + associateObj.optString("name"));
            if(!associateObj.optString("address").contains("null"))
                associateDetails.setAssociate_address("Address: " + associateObj.optString("address"));
            if(!associateObj.optString("pic").contains("null"))
                associateDetails.setAssociate_picture(associateObj.optString("pic"));
           /* if(!associateObj.optString("PROV_CRM_NO").contains("null"))
                associateDetails.setAssociate_prov_criminal_number(associateObj.optString("PROV_CRM_NO"));
*/
            associateDetailsList.add(associateDetails);

            list_associate.add(Integer.toString(i));

        }

        listDataChild.put(listDataHeader.get(listDataHeader.indexOf("Associate")), list_associate);

        listAdapter = new ExpandableListAdapterForProfile(getActivity(), listDataHeader, listDataChild,descriptiveRoleList,CRSList,CRSImageList,associateDetailsList);
        listAdapter.setOnDescriptiveRoleUpdateListener(ModifiedProfileFragment.this);
        // setting list adapter
        lvExp.setAdapter(listAdapter);
        lvExp.expandGroup(0);

        criminalAssosiateList = Utility
                .parseCrimiinalAssociateRecords(getActivity(), associate_array);

    }

    public void setAssociateData(){

        listAdapter = new ExpandableListAdapterForProfile(getActivity(), listDataHeader, listDataChild,descriptiveRoleList,CRSList,CRSImageList,associateDetailsList);
        listAdapter.setOnDescriptiveRoleUpdateListener(ModifiedProfileFragment.this);
        // setting list adapter
        lvExp.setAdapter(listAdapter);
        lvExp.expandGroup(0);
    }

    private void clickEvents(){

        lvExp.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (listDataHeader.get(groupPosition).equalsIgnoreCase("Associate")) {

                    // show full screen popup window
                    System.out.println("Associate Details Opening");
                    //popUpWindowForAssosiateDetails(v, childPosition);
                    if(criminalAssosiateList.get(childPosition).getProvCrmNo().equals("")|| criminalAssosiateList.get(childPosition).getProvCrmNo().equals("null") ||criminalAssosiateList.get(childPosition).getProvCrmNo().equals(null)) {
                        Utility.showAlertDialog(getActivity(),Constants.ERROR,Constants.ERROR_PRV_CRIMINAL_NO,false);

                    }else{
                        Intent intent = new Intent(getActivity(), CriminalDetailActivity.class);
                        intent.putExtra(Constants.OBJECT, criminalAssosiateList.get(childPosition));
                        intent.putExtra("searchCase", "criminalSearch");
                        startActivity(intent);

                    }
                }



                    return false;
            }
        });

        lvExp.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (listDataHeader.get(groupPosition).equalsIgnoreCase("Associate")) {

                    if (associateDetailsList.size() == 0) {
                        showAlertDialog(getActivity()," No Data Found ",associate_msg,false);
                    }
                }
                if (listDataHeader.get(groupPosition).equalsIgnoreCase("Descriptive Role")) {

                    if (descriptiveRoleList.size() == 0) {
                        showAlertDialog(getActivity()," No Data Found ",descriptive_msg,false);
                    }
                }


                return false;
            }
        });

    }

    private void popUpWindowForAssosiateDetails(View v, int pos){

        //imageLoader = new ImageLoader(getActivity());

        List<String> associate_detailsList = new ArrayList<>();

        LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        inflatedView = layoutInflater.inflate(R.layout.layout_associate_detail_popup, null,false);

        ListView lv_associateDetails = (ListView)inflatedView.findViewById(R.id.lv_associateDetails);
        ImageView iv_image = (ImageView)inflatedView.findViewById(R.id.iv_image);

        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, width,height-50, true );
        // set a background drawable with rounders corners
        // popWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_bg));

        popWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindow.setAnimationStyle(R.style.PopupAnimation);

        // show the popup at bottom of the screen and set some margin at bottom ie,
        // popWindow.showAtLocation(v, Gravity.BOTTOM, 0, 100);

        popWindow.showAsDropDown(getActivity().getActionBar().getCustomView(),0,30);

        popWindow.setOutsideTouchable(true);
        popWindow.setFocusable(true);
        popWindow.getContentView().setFocusableInTouchMode(true);
        popWindow.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    System.out.println("back pressed while opened popup");
                    popWindow.dismiss();
                    return true;
                }
                return false;
            }
        });

        if(!associateDetailsList.get(pos).getAssociate_name().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_name());
        }

        if(!associateDetailsList.get(pos).getAssociate_address().equalsIgnoreCase("")) {
            associate_detailsList.add(associateDetailsList.get(pos).getAssociate_address());
        }



        AssociateDetailsAdapter associateDetailsAdapter = new AssociateDetailsAdapter(getActivity(),associate_detailsList);
        lv_associateDetails.setAdapter(associateDetailsAdapter);


    }


    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     * */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    @Override
    public void updateDescriptiveRole(JSONObject jo) {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_PROFILE_DESCRIPTIVE_ROLE_UPDATE);
        taskManager.setCriminalProfileDescriptiveRoleUpdate(true);
        String[] keys = new String[0];
        String[] values = new String[0];
        keys = new String[] { "prov_crm_no","roles" };
        values = new String[] { prov_crm_no.trim(),jo.toString().trim()};
        taskManager.doStartTask(keys, values, true);


    }
}
