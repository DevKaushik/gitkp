package com.kp.facedetection.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by DAT-165 on 07-07-2016.
 */
public class FIROtherAccusedDetail implements Serializable {

    private String name="";
    private String fatherName="";
    private String address="";
    private String alias="";
   /* private String psNameAsCode = "";
    private String caseRef = "";
    private String crimeYr = "";*/

    public ArrayList<CriminalDetails> getOtherAccusedListItemData() {
        return otherAccusedList;
    }

    public void setOtherAccusedListItemData(ArrayList<CriminalDetails> otherAccusedList) {
        this.otherAccusedList.addAll(otherAccusedList);
    }

    private ArrayList<CriminalDetails> otherAccusedList = new ArrayList<CriminalDetails>();

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }


}
