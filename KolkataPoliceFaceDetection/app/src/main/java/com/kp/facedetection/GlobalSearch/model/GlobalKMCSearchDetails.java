package com.kp.facedetection.GlobalSearch.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DAT-165 on 06-12-2016.
 */
public class GlobalKMCSearchDetails implements Parcelable {

    private String kmc_slNo = "";
    private String kmc_wardNo = "";
    private String kmc_premises = "";
    private String kmc_owner = "";
    private String kmc_personLiable = "";
    private String kmc_mailingAddress = "";


    public GlobalKMCSearchDetails(Parcel in) {
        setKmc_slNo(in.readString());
        setKmc_wardNo(in.readString());
        setKmc_premises(in.readString());
        setKmc_owner(in.readString());
        setKmc_personLiable(in.readString());
        setKmc_mailingAddress(in.readString());
    }

    public static final Creator<GlobalKMCSearchDetails> CREATOR = new Creator<GlobalKMCSearchDetails>() {
        @Override
        public GlobalKMCSearchDetails createFromParcel(Parcel in) {
            return new GlobalKMCSearchDetails(in);
        }

        @Override
        public GlobalKMCSearchDetails[] newArray(int size) {
            return new GlobalKMCSearchDetails[size];
        }
    };



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getKmc_slNo());
        dest.writeString(getKmc_wardNo());
        dest.writeString(getKmc_premises());
        dest.writeString(getKmc_owner());
        dest.writeString(getKmc_personLiable());
        dest.writeString(getKmc_mailingAddress());
    }

    public String getKmc_slNo() {
        return kmc_slNo;
    }

    public void setKmc_slNo(String kmc_slNo) {
        this.kmc_slNo = kmc_slNo;
    }

    public String getKmc_wardNo() {
        return kmc_wardNo;
    }

    public void setKmc_wardNo(String kmc_wardNo) {
        this.kmc_wardNo = kmc_wardNo;
    }

    public String getKmc_premises() {
        return kmc_premises;
    }

    public void setKmc_premises(String kmc_premises) {
        this.kmc_premises = kmc_premises;
    }

    public String getKmc_owner() {
        return kmc_owner;
    }

    public void setKmc_owner(String kmc_owner) {
        this.kmc_owner = kmc_owner;
    }

    public String getKmc_personLiable() {
        return kmc_personLiable;
    }

    public void setKmc_personLiable(String kmc_personLiable) {
        this.kmc_personLiable = kmc_personLiable;
    }

    public String getKmc_mailingAddress() {
        return kmc_mailingAddress;
    }

    public void setKmc_mailingAddress(String kmc_mailingAddress) {
        this.kmc_mailingAddress = kmc_mailingAddress;
    }
}
