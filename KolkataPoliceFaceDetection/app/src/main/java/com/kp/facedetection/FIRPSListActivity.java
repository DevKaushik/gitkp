package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.adapter.PSListForAllDashboardTabAdapter;
import com.kp.facedetection.interfaces.OnItemClickListenerForAllFIRView;
import com.kp.facedetection.model.CaseSearchDetails;
import com.kp.facedetection.model.CrimeReviewDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class FIRPSListActivity extends BaseActivity implements OnItemClickListenerForAllFIRView, View.OnClickListener, Observer {

    private String userId = "";
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String[] key_map;
    private String[] value_map;
    private String[] keys;
    private String[] values;
    private String pageno="";
    private String totalResult="";
    private List<CaseSearchDetails> caseSearchDetailsList = new ArrayList<CaseSearchDetails>();

    private String fromDate = "";
    private String toDate = "";
    private String category = "";
    private String selected_div ="";
    private String selected_ps = "";

    private RecyclerView recycler_psList;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<CrimeReviewDetails> psList = new ArrayList<>();
    private PSListForAllDashboardTabAdapter psListAdapter;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firtab_from_dashboard_layout);
        ObservableObject.getInstance().addObserver(this);
        setToolBar();
        initViews();
    }


    private void setToolBar() {
        try {
            userId = Utility.getUserInfo(this).getUserId();
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }


    private void initViews() {

        fromDate = getIntent().getExtras().getString("FROM_DATE");
        category = getIntent().getExtras().getString("CATEGORY");
        selected_div = getIntent().getExtras().getString("SELECTED_DIV");
        selected_ps = getIntent().getExtras().getString("SELECTED_PS");


        recycler_psList = (RecyclerView) findViewById(R.id.recycler_psList);
        mLayoutManager = new LinearLayoutManager(this);
        recycler_psList.setLayoutManager(mLayoutManager);
        //recycler_psList.setItemAnimator(new DefaultItemAnimator());
        recycler_psList.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));

        allFIRPSListCall();
        psListAdapter = new PSListForAllDashboardTabAdapter(this, psList);
        recycler_psList.setAdapter(psListAdapter);
        psListAdapter.setClickListener(this);
    }


    private void allFIRPSListCall() {
       TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_FIR_VIEW);
        taskManager.setFIRPSItemSearch(true);

        String[] keys = {"currDate","category","div","ps","user_id"};
        String[] values = {fromDate.trim(),category.trim(), selected_div.trim(), selected_ps.trim(),Utility.getUserInfo(this).getUserId()};
        taskManager.doStartTask(keys, values, true);
    }

    public void parseFIRCategoryToPSResult(String result, String[] keys, String[] values) {
        //System.out.println("parseFIRCategoryToPSResult :"+ result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseCrimeCategoryPSResponse(resultArray);
                }
                else{
                    Utility.showAlertDialog(FIRPSListActivity.this,Constants.ERROR_DETAILS_TITLE,Constants.ERROR_MSG_DETAIL,false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(FIRPSListActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }

    private void parseCrimeCategoryPSResponse(JSONArray resultArray) {
        psList.clear();
        for (int i = 0; i < resultArray.length(); i++) {

            CrimeReviewDetails crimeReviewDetails = new CrimeReviewDetails(Parcel.obtain());
            try {
                JSONObject jObj = resultArray.getJSONObject(i);
                if (jObj.optString("PSNAME") != null && !jObj.optString("PSNAME").equalsIgnoreCase("") && !jObj.optString("PSNAME").equalsIgnoreCase("null")) {
                    crimeReviewDetails.setCrimeCategory(jObj.optString("PSNAME"));
                }
                if (jObj.optString("COUNT") != null && !jObj.optString("COUNT").equalsIgnoreCase("") && !jObj.optString("COUNT").equalsIgnoreCase("null")) {
                    crimeReviewDetails.setCountOfCrime(jObj.optString("COUNT"));
                }
                if (jObj.optString("PSCODE") != null && !jObj.optString("PSCODE").equalsIgnoreCase("") && !jObj.optString("PSCODE").equalsIgnoreCase("null")) {
                    crimeReviewDetails.setCode(jObj.optString("PSCODE"));
                }

                psList.add(crimeReviewDetails);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
        psListAdapter.notifyDataSetChanged();
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View view, int position) {
        final CrimeReviewDetails crimeReviewDetails = psList.get(position);
        String psCode = crimeReviewDetails.getCode().toString().trim();

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");

        Date dateFrom ;
        String strFromDate="", strToDate="";
        try {
            dateFrom = formatter.parse(fromDate);

            strFromDate = formatter1.format(dateFrom);
        }
        catch (ParseException e){
            e.printStackTrace();
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_MODIFIED_CASE_SEARCH);
        taskManager.setFIRAllViewToCaseSearch(true);

        String[] keys = { "caseno", "caseyear",
                "datefrom", "dateto", "policestations", "crimecategory","crimetime","mod_oper","pageno", "complaint", "io", "divisions", "brief_keyword","status", "inv_unit", "inv_section"};
        String[] values = {"","",strFromDate.trim(), strFromDate.trim(), "'"+psCode.trim()+"'", "'"+category.trim()+"'","","","1","","",selected_div.trim(), "", "", "", ""};

        key_map = new String[]{"div", "ps", "crime_cat", "from_date", "to_date", "caseno", "caseyear", "crimetime", "mod_oper", "complaint", "status", "io", "brief_keyword", "pageno", "inv_unit", "inv_section"};
        value_map = new String[]{selected_div.trim(), "'"+psCode.trim()+"'", "'"+category.trim()+"'", strFromDate.trim(), strFromDate.trim(), "", "", "", "", "", "", "", "","1", "", ""};

        taskManager.doStartTask(keys, values, true);
    }

    public void parseFIRToCaseSearchResult(String result, String[] keys, String[] values) {
        //System.out.println("parseFIRToCaseSearchResult :" + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("pageno").toString();
                    totalResult = jObj.opt("totalresult").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseCaseSearchResponse(resultArray);
                }
                else{
                    Utility.showAlertDialog(this,Constants.ERROR_DETAILS_TITLE,Constants.ERROR_MSG_DETAIL,false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }


    private void parseCaseSearchResponse(JSONArray result_array){

        caseSearchDetailsList.clear();

        for(int i=0;i<result_array.length();i++){

            JSONObject obj = null;

            try {

                obj = result_array.getJSONObject(i);

                CaseSearchDetails caseSearchDetails = new CaseSearchDetails();

                if(!obj.optString("ROWNUMBER").equalsIgnoreCase("null"))
                    caseSearchDetails.setRowNumber(obj.optString("ROWNUMBER"));
                if(!obj.optString("PSCODE").equalsIgnoreCase("null"))
                    caseSearchDetails.setPsCode(obj.optString("PSCODE"));
                if(!obj.optString("PS").equalsIgnoreCase("null"))
                    caseSearchDetails.setPs(obj.optString("PS"));
                if(!obj.optString("CASENO").equalsIgnoreCase("null"))
                    caseSearchDetails.setCaseNo(obj.optString("CASENO"));
                if(!obj.optString("CASEDATE").equalsIgnoreCase("null"))
                    caseSearchDetails.setCaseDate(obj.optString("CASEDATE"));
                if(!obj.optString("UNDER_SECTION").equalsIgnoreCase("null"))
                    caseSearchDetails.setUnderSection(obj.optString("UNDER_SECTION"));
                if(!obj.optString("CATEGORY").equalsIgnoreCase("null"))
                    caseSearchDetails.setCategory(obj.optString("CATEGORY"));
                if(!obj.optString("CASE_YR").equalsIgnoreCase("null"))
                    caseSearchDetails.setCaseYr(obj.optString("CASE_YR"));
                if(!obj.optString("FIR_STATUS").equalsIgnoreCase("null"))
                    caseSearchDetails.setFirStatus(obj.optString("FIR_STATUS"));
                if(!obj.optString("OCCUR_TIMING").equalsIgnoreCase("null"))
                    caseSearchDetails.setOccurTime(obj.optString("OCCUR_TIMING"));
                if(!obj.optString("MOD_OPER").equalsIgnoreCase("null"))
                    caseSearchDetails.setModOper(obj.optString("MOD_OPER"));
                if(!obj.optString("PO_LAT").equalsIgnoreCase("null") && !obj.optString("PO_LAT").equalsIgnoreCase("") && obj.optString("PO_LAT") != null)
                    caseSearchDetails.setPoLat(obj.optString("PO_LAT"));
                if(!obj.optString("PO_LONG").equalsIgnoreCase("null") && !obj.optString("PO_LONG").equalsIgnoreCase("") && obj.optString("PO_LONG") != null)
                    caseSearchDetails.setPoLong(obj.optString("PO_LONG"));

                JSONArray briefMatch_array = obj.getJSONArray("brief_match");
                if(briefMatch_array.length() > 0){
                    List<String> briefMatch_list = new ArrayList<>();
                    for(int j=0;j<briefMatch_array.length();j++){
                        briefMatch_list.add(briefMatch_array.optString(j));
                    }
                    caseSearchDetails.setBriefMatch_list(briefMatch_list);
                }

                caseSearchDetailsList.add(caseSearchDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Intent intent=new Intent(FIRPSListActivity.this, ModifiedCaseSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putExtra("key_map",key_map);
        intent.putExtra("value_map",value_map);
        intent.putExtra("MODIFIED_CASE_SEARCH_LIST", (Serializable) caseSearchDetailsList);
        startActivity(intent);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
