package com.kp.facedetection.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.kp.facedetection.R;
import com.kp.facedetection.utility.Constants;

/**
 * Created by DAT-Asset-131 on 26-04-2016.
 */
public class DocumentsFragment extends Fragment {

    private Button btn_document_search;
    private WebView documentSearchWv;

    public DocumentsFragment(){

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_document, container, false);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        documentSearchWv=(WebView)view.findViewById(R.id.document_serach_wv);
        documentSearchWv.getSettings().setJavaScriptEnabled(true); // enable javascript

        documentSearchWv.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                //Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
            }
        });

        documentSearchWv.loadUrl(Constants.DOCUMENT_SEARCH_URL);

       // btn_document_search = (Button)view.findViewById(R.id.btn_document_search);

       // clickEvents();

    }


    private void clickEvents(){
/*
        btn_document_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentLoad();
            }

        });*/

    }

    private void fragmentLoad() {
        new AlertDialog.Builder(getActivity()).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setTitle("CRS Message").setMessage("This item is not available for you at this moment.").show();
    }

}
