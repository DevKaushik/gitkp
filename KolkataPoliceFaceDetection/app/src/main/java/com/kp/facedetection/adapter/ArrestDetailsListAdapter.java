package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.model.ModifiedArrestDetails;
import com.kp.facedetection.utils.L;

import java.util.List;

/**
 * Created by DAT-165 on 14-07-2016.
 */
public class ArrestDetailsListAdapter extends BaseAdapter {

    private Context context;
    private List<ModifiedArrestDetails> arrestDetailList;

    public ArrestDetailsListAdapter(Context context,List<ModifiedArrestDetails> arrestDetailList) {

        this.context = context;
        this.arrestDetailList = arrestDetailList;
    }

    @Override
    public int getCount() {

        if(arrestDetailList.size()>0) {
            return arrestDetailList.size();
        }
        else {
            return 0;
        }

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.layout_fir_details_item_row, null);


            holder.tv_SlNo = (TextView) convertView
                    .findViewById(R.id.tv_SlNo);

            holder.tv_ArrestName = (TextView) convertView
                    .findViewById(R.id.tv_FirName);

            holder.tv_status = (TextView) convertView
                    .findViewById(R.id.tv_status);

            holder.vw_line2=(View) convertView.findViewById(R.id.vw_line2);
            holder.vw_line2.setVisibility(View.GONE);
            holder.tv_status.setVisibility(View.GONE);

            holder.tv_ArrestName.setGravity(Gravity.CENTER);
            Constants.changefonts(holder.tv_SlNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_ArrestName, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_status, context, "Calibri.ttf");

            holder.tv_ArrestName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        Previous : Arrested on 22-AUG-16 by SURVEY PARK PS (case no 244).
//        Current : Arrested on 22-AUG-16 by SURVEY PARK PS (Case/GDE No. 244 dated 20-AUG-16).      // Changed on 25-07-17
        String arrestName = "Arrested";

     //   String arrestName = "Arrested on "+arrestDetailList.get(position).getArrestedOn()+" at "+arrestDetailList.get(position).getPsName()+" with Case Ref "+arrestDetailList.get(position).getCaseReference();

        if(!arrestDetailList.get(position).getArrestedOn().equalsIgnoreCase("")){
            arrestName = arrestName+" on " + arrestDetailList.get(position).getArrestedOn();
        }

        if(!arrestDetailList.get(position).getPsName().equalsIgnoreCase("")){
            arrestName = arrestName+ " by "+arrestDetailList.get(position).getPsName();
        }

        if(!arrestDetailList.get(position).getCaseReference().equalsIgnoreCase("")){
            L.e("CASE_DATE--"+arrestDetailList.get(position).getCaseDate());
            arrestName = arrestName+" (Case/GDE No. "+arrestDetailList.get(position).getCaseReference();

         if (!arrestDetailList.get(position).getCaseDate().equalsIgnoreCase("")){
                arrestName = arrestName + " dated "+arrestDetailList.get(position).getCaseDate();
            }
            arrestName = arrestName+")";
        }

        holder.tv_SlNo.setText(Integer.toString(position + 1));



        SpannableString mySpannableString = new SpannableString(arrestName);
        mySpannableString.setSpan(new UnderlineSpan(), 0, mySpannableString.length(), 0);
        holder.tv_ArrestName.setText(mySpannableString);


        return convertView;
    }

    class ViewHolder {

        TextView tv_SlNo;
        TextView tv_ArrestName;
        TextView tv_status;
        private View vw_line2;

    }

}
