package com.kp.facedetection.GlobalSearch.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.kp.facedetection.AllDocumentDataSearchResultActivity;
import com.kp.facedetection.BaseActivity;
import com.kp.facedetection.FeedbackActivity;
import com.kp.facedetection.KPFaceDetectionApplication;
import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnShowAlertForProceedResult;
import com.kp.facedetection.model.AllDocumentSearchType;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class GlobalSDRSearchDetailsActivity extends BaseActivity implements View.OnClickListener, Observer,OnShowAlertForProceedResult {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String holder_name="";
    private String holder_presentAddress="";
    private String holder_permanentAddress="";
    private String holder_city="";
    private String holder_state="";
    private String holder_pincode="";
    private String holder_actProvider="";
    private String holder_actDate="";
    private String holder_mobileNo="";
    private String holder_phoneNo="";

    /* view initialization */
    private TextView tv_nameValue;
    private TextView tv_id_number;
    private TextView tv_id_type;

    private TextView tv_presentAddressValue;
    private TextView tv_permanentAddressValue;
    private TextView tv_cityValue;
    private TextView tv_stateValue;
    private TextView tv_pincodeValue;
    private TextView tv_activationProviderValue;
    private TextView tv_actDateValue;
    private TextView tv_mobileValue, tv_phoneValue;
    private TextView tv_heading;
    TextView chargesheetNotificationCount;
    String notificationCount="";
    String name = "", address = "", contact = "";
    CheckBox chk_dl,chk_vehicle,chk_sdr,chk_cable,chk_kmc,chk_gas,chk_select_all;
    private String pageno = "";
    private String totalResult = "";
    private String[] keys;
    private String[] values;
    String userId = "";
    private Button btn_Search, btn_reset;
    String datafields ="";
    ArrayList<AllDocumentSearchType> allDocumentSearchTypesArrayList = new ArrayList<AllDocumentSearchType>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_sdrsearch_details);
        ObservableObject.getInstance().addObserver(this);
        initialization();
    }

    private void initialization(){

        tv_nameValue = (TextView)findViewById(R.id.tv_nameValue);
        tv_id_number = (TextView)findViewById(R.id.tv_id_number);
        tv_id_type = (TextView)findViewById(R.id.tv_id_type);
        tv_presentAddressValue = (TextView)findViewById(R.id.tv_presentAddressValue);
        tv_permanentAddressValue = (TextView)findViewById(R.id.tv_permanentAddressValue);
        tv_cityValue = (TextView)findViewById(R.id.tv_cityValue);
        tv_stateValue = (TextView)findViewById(R.id.tv_stateValue);
        tv_pincodeValue = (TextView)findViewById(R.id.tv_pincodeValue);
        tv_activationProviderValue = (TextView)findViewById(R.id.tv_activationProviderValue);
        tv_actDateValue = (TextView)findViewById(R.id.tv_actDateValue);
        tv_mobileValue = (TextView)findViewById(R.id.tv_mobileValue);
        tv_phoneValue = (TextView)findViewById(R.id.tv_phoneValue);
        tv_heading = (TextView)findViewById(R.id.tv_heading);

        Constants.changefonts(tv_nameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_id_number, this, "Calibri.ttf");
        Constants.changefonts(tv_id_type, this, "Calibri.ttf");
        Constants.changefonts(tv_presentAddressValue, this, "Calibri.ttf");
        Constants.changefonts(tv_permanentAddressValue, this, "Calibri.ttf");
        Constants.changefonts(tv_cityValue, this, "Calibri.ttf");
        Constants.changefonts(tv_stateValue, this, "Calibri.ttf");
        Constants.changefonts(tv_pincodeValue, this, "Calibri.ttf");
        Constants.changefonts(tv_activationProviderValue, this, "Calibri.ttf");
        Constants.changefonts(tv_actDateValue, this, "Calibri.ttf");
        Constants.changefonts(tv_mobileValue, this, "Calibri.ttf");
        Constants.changefonts(tv_phoneValue, this, "Calibri.ttf");
        Constants.changefonts(tv_heading, this, "Calibri.ttf");

        Constants.changefonts(tv_heading, this, "Calibri Bold.ttf");

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        holder_name = getIntent().getStringExtra("HOLDER_NAME");
        holder_presentAddress = getIntent().getStringExtra("HOLDER_PRESENT_ADDRESS");
        holder_permanentAddress = getIntent().getStringExtra("HOLDER_PERMANENT_ADDRESS");
        holder_city = getIntent().getStringExtra("HOLDER_CITY");
        holder_state = getIntent().getStringExtra("HOLDER_STATE");
        holder_pincode = getIntent().getStringExtra("HOLDER_PINCODE");
        holder_actProvider = getIntent().getStringExtra("HOLDER_ACT_PROVIDER");
        holder_actDate = getIntent().getStringExtra("HOLDER_ACT_DATE");
        holder_mobileNo = getIntent().getStringExtra("HOLDER_MOBILE");
        holder_phoneNo = getIntent().getStringExtra("HOLDER_PHONE");

        tv_nameValue.setText(holder_name);
        tv_id_number.setText(getIntent().getStringExtra("ID_NUMBER"));
        tv_id_type.setText(getIntent().getStringExtra("ID_TYPE"));
        tv_presentAddressValue.setText(holder_presentAddress);
        tv_permanentAddressValue.setText(holder_permanentAddress);
        tv_cityValue.setText(holder_city);
        tv_stateValue.setText(holder_state);
        tv_pincodeValue.setText(holder_pincode);
        tv_activationProviderValue.setText(holder_actProvider);
        tv_actDateValue.setText(holder_actDate);
        tv_mobileValue.setText(holder_mobileNo);
        tv_phoneValue.setText(holder_phoneNo);

          /* section for check box and search button  initilization */
        chk_dl=(CheckBox)findViewById(R.id.chk_dl);
        chk_dl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_dl.setChecked(true);
                else
                    chk_dl.setChecked(false);
            }
        });

        chk_vehicle=(CheckBox)findViewById(R.id.chk_vehicle);
        chk_vehicle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_vehicle.setChecked(true);
                else
                    chk_vehicle.setChecked(false);
            }
        });


        chk_cable=(CheckBox)findViewById(R.id.chk_cable);
        chk_cable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_cable.setChecked(true);
                else
                    chk_cable.setChecked(false);
            }
        });


        chk_kmc=(CheckBox)findViewById(R.id.chk_kmc);
        chk_kmc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_kmc.setChecked(true);
                else
                    chk_kmc.setChecked(false);
            }
        });


        chk_gas=(CheckBox)findViewById(R.id.chk_gas);

        chk_gas.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_gas.setChecked(true);
                else
                    chk_gas.setChecked(false);
            }
        });

        chk_select_all=(CheckBox)findViewById(R.id.chk_select_all);
        chk_select_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    chk_dl.setChecked(true);
                    chk_vehicle.setChecked(true);
                    chk_cable.setChecked(true);
                    chk_kmc.setChecked(true);
                    chk_gas.setChecked(true);

                }
                else{
                    chk_dl.setChecked(false);
                    chk_vehicle.setChecked(false);
                    chk_cable.setChecked(false);
                    chk_kmc.setChecked(false);
                    chk_gas.setChecked(false);
                }
            }
        });


        btn_Search = (Button) findViewById(R.id.btn_Search);
        btn_reset = (Button) findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_Search);
        Constants.buttonEffect(btn_reset);
        clickEvents();


    }
    public void clickEvents() {
        btn_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name =holder_name  ;

                address = holder_permanentAddress.toLowerCase();

                contact = holder_mobileNo;

                if(chk_dl.isChecked()){
                    datafields="1";
                }else{
                    datafields=datafields.replace("1","");
                }

                if(chk_vehicle.isChecked()){
                    datafields=datafields+","+"2";
                }
                else{
                    datafields=datafields.replace(",2","");
                }

                if(chk_cable.isChecked()){
                    datafields=datafields+","+"4";
                }
                else{
                    datafields=datafields.replace(",4","");

                }

                if(chk_kmc.isChecked()){
                    datafields=datafields+","+"5";
                }
                else{
                    datafields=datafields.replace(",5","");

                }

                if(chk_gas.isChecked()){
                    datafields=datafields+","+"6";
                }
                else{
                    datafields=datafields.replace(",6","");

                }
                if(datafields.equalsIgnoreCase("")){
                    Utility.showAlertDialog(GlobalSDRSearchDetailsActivity.this, " Search Error!!! ", "Please select at least one  search type", false);
                }
                else{
                    if (!name.isEmpty() || !address.isEmpty() || !contact.isEmpty()) {
                        Utility utility = new Utility();
                        utility.setDelegate(GlobalSDRSearchDetailsActivity.this);
                        Utility.showAlertForProceed(GlobalSDRSearchDetailsActivity.this);
                    } else {

                        Utility.showAlertDialog(GlobalSDRSearchDetailsActivity.this, " Search Error!!! ", "Not getting any value for search", false);

                    }


                }


            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                chk_dl.setChecked(false);
                chk_vehicle.setChecked(false);
                chk_cable.setChecked(false);
                chk_kmc.setChecked(false);
                chk_gas.setChecked(false);
                chk_select_all.setChecked(false);



            }
        });
    }

    @Override
    public void onShowAlertForProceedResultSuccess(String success) {
        fetchSearchResult();
    }
    private void fetchSearchResult() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_DOCUMENT_RELATIONAL_SEARCH);
        taskManager.setSDRAllDataRelationalSearch(true);
        //user_id,device_token,device_type,imei,auth_key,name,address,pageno
        String[] keys = {"name", "address", "phoneNumber", "dataFields", "page_no"};
        String[] values = {name, address, contact,datafields, "1"};
        taskManager.doStartTask(keys, values, true, true);
    }
    public void parseSDRAllDataRelationalSearchResultResponse(String result, String[] keys, String[] values) {
        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("page").toString();
                    totalResult = jObj.opt("count").toString();

                    JSONObject resultObject = jObj.getJSONObject("result");

                    createAllDocumentSearchTypeList(resultObject);

                } else {
                    Utility.showAlertDialog(this, " Search Error ", jObj.optString("message"), false);
                }


            } catch (JSONException e) {

                e.printStackTrace();
                //Utility.showToast(getActivity(), "Some error has been encountered", "long");
                Utility.showAlertDialog(this, " Search Error ", "Some error has been encountered", false);
            }
        }

    }
    public void createAllDocumentSearchTypeList(JSONObject resultArray) {
        allDocumentSearchTypesArrayList.clear();
        if (resultArray.has("1")) {
            try {
                JSONObject jsonObjectDL = resultArray.getJSONObject("1");
                if (jsonObjectDL.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[0]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("2")) {
            try {
                JSONObject jsonObjectVehicle = resultArray.getJSONObject("2");
                if (jsonObjectVehicle.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[1]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }

        if (resultArray.has("3")) {
            try {
                JSONObject jsonObjectSDR = resultArray.getJSONObject("3");
                if (jsonObjectSDR.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[2]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("4")) {
            try {
                JSONObject jsonObjectCable = resultArray.getJSONObject("4");
                if (jsonObjectCable.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[3]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("5")) {
            try {
                JSONObject jsonObjectKMC = resultArray.getJSONObject("5");
                if (jsonObjectKMC.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[4]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("6")) {
            try {
                JSONObject jsonObjectGas = resultArray.getJSONObject("6");
                if (jsonObjectGas.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[5]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }

        }
        Intent intent = new Intent(this, AllDocumentDataSearchResultActivity.class);
        intent.putExtra("keys", keys);
        intent.putExtra("value", values);
        intent.putExtra("ALL_DOCUMENT_SEARCH_TYPE", (Serializable) allDocumentSearchTypesArrayList);
        intent.putExtra("ALL_DOCUMENT_SEARCH_LIST", String.valueOf(resultArray));
        startActivity(intent);

    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this,Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }



    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }



    @Override
    public void onShowAlertForProceedResultError(String error) {

    }
}
