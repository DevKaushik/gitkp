package com.kp.facedetection.interfaces;

/**
 * Created by Kaushik on 20-12-2016.
 */
public interface OnPDFDownload {

    void onPDFDownloadSuccess(String filePath, boolean forWhich);
    void onPDFDownloadError(String error);


}
