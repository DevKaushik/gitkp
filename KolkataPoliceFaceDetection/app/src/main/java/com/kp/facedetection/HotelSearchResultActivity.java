package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kp.facedetection.adapter.BARSearchAdapter;
import com.kp.facedetection.adapter.HotelSearchAdapter;
import com.kp.facedetection.model.BARDetails;
import com.kp.facedetection.model.HotelDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HotelSearchResultActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener{
    private String[] keys;
    private String[] values;
    private String pageno;
    private String totalResult="";
    private String searchCase = "";
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private TextView tv_resultCount;
    private ListView lv_hotelSearchList;
    private Button btnLoadMore;

    private List<HotelDetails> hotelDetailsList;
    ArrayList<HotelDetails> hotelDetailsArrayList ;
    HotelSearchAdapter hotelSearchAdapter;
    String visitoreType="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_search_result);
        initialization();
        clickEvents();
    }
    private void initialization(){


        tv_resultCount=(TextView)findViewById(R.id.tv_resultCount);
        lv_hotelSearchList = (ListView)findViewById(R.id.lv_HotelSearchList);

        // LoadMore button
        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);

        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        keys = getIntent().getStringArrayExtra("keys");
        values = getIntent().getStringArrayExtra("value");
        pageno = getIntent().getStringExtra("pageno");
        totalResult = getIntent().getStringExtra("totalResult");
        visitoreType = getIntent().getStringExtra("visitoreType");


        hotelDetailsList = (List<HotelDetails>) getIntent().getSerializableExtra("HOTEL_SEARCH_LIST");

        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }

        hotelSearchAdapter = new HotelSearchAdapter(this,hotelDetailsList);
        lv_hotelSearchList.setAdapter(hotelSearchAdapter);
        hotelSearchAdapter.notifyDataSetChanged();

        if (Integer.parseInt(totalResult) > 20) {

            // Adding Load More button to listview at bottom
            lv_hotelSearchList.addFooterView(btnLoadMore);

        }

        lv_hotelSearchList.setOnItemClickListener(this);

    }

    private void clickEvents(){

        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Starting a new async task
                fetchHotelSearchResultPagination();
            }
        });

    }
    private void fetchHotelSearchResultPagination() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_HOTEL_SEARCH);
        taskManager.setHotelSearchPagination(true);

        values[10] = (Integer.parseInt(pageno) + 1) + "";

        taskManager.doStartTask(keys, values, true, true);


    }
    public void parseHotelSearchResultPaginationResponse(String result, String[] keys, String[] values) {

        // System.out.println("parseSDRSearchResultResponse: " + result);
        Log.d("JAY" ,"parseHotelSearchResult----" + result);

        if(result != null && !result.equals("")){

           try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.opt("status").toString().equalsIgnoreCase("1")){

                    this.keys=keys;
                    this.values=values;
                    pageno=jObj.opt("pageno").toString();
                   // totalResult=jObj.opt("count").toString();
                    JSONArray resultArray=jObj.getJSONArray("result");
                    parseHotelSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(this," Search Error ",jObj.optString("message"),false);
                }


           }catch (JSONException e){

                e.printStackTrace();
                showAlertDialog(this, " Search Error!!! ", "Some error has been encountered", false);
            }
        }

    }
    private void parseHotelSearchResponse(JSONArray resultArray) {

        for(int i=0;i<resultArray.length();i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj=resultArray.getJSONObject(i);

                HotelDetails hotelDetails = new HotelDetails();
                if(visitoreType.equalsIgnoreCase("1")) {

                    if (!jsonObj.optString("Full_Name").equalsIgnoreCase("null") && !jsonObj.optString("Full_Name").equalsIgnoreCase("")) {
                        hotelDetails.setName(jsonObj.optString("Full_Name"));
                    }
                    String address="";
                    if (!jsonObj.optString("Village_Street_Name_with_No").equalsIgnoreCase("null") && !jsonObj.optString("Village_Street_Name_with_No").equalsIgnoreCase("")) {
                        address=address+jsonObj.optString("Village_Street_Name_with_No");
                    }
                    if (!jsonObj.optString("Post_Office").equalsIgnoreCase("null") && !jsonObj.optString("Post_Office").equalsIgnoreCase("")) {
                        address=address+","+jsonObj.optString("Post_Office");
                    }
                    if (!jsonObj.optString("District").equalsIgnoreCase("null") && !jsonObj.optString("District").equalsIgnoreCase("")) {
                        address=address+","+jsonObj.optString("District");
                    }
                    if (!jsonObj.optString("State").equalsIgnoreCase("null") && !jsonObj.optString("State").equalsIgnoreCase("")) {
                        address=address+","+jsonObj.optString("State");
                    }
                    if (!jsonObj.optString("PIN").equalsIgnoreCase("null") && !jsonObj.optString("PIN").equalsIgnoreCase("")) {
                        address=address+","+jsonObj.optString("PIN");
                    }
                    hotelDetails.setAddress(address);
                    String imageURL="";
                    if (!jsonObj.optString("Photograph").equalsIgnoreCase("null") && !jsonObj.optString("Photograph").equalsIgnoreCase("")) {
                        imageURL=imageURL+Constants.IMAGE_URL_NATIONAL;
                        imageURL=imageURL+jsonObj.optString("Photograph");
                    }
                    hotelDetails.setImage(imageURL);
                    if (!jsonObj.optString("ID").equalsIgnoreCase("null") && !jsonObj.optString("ID").equalsIgnoreCase("")) {
                        hotelDetails.setId(jsonObj.optString("ID"));
                    }
                    if (!jsonObj.optString("Age").equalsIgnoreCase("null") && !jsonObj.optString("Age").equalsIgnoreCase("")) {
                        hotelDetails.setAge(jsonObj.optString("Age"));
                    } if (!jsonObj.optString("Sex").equalsIgnoreCase("null") && !jsonObj.optString("Sex").equalsIgnoreCase("")) {
                        hotelDetails.setSex(jsonObj.optString("Sex"));
                    }if (!jsonObj.optString("Contact_No").equalsIgnoreCase("null") && !jsonObj.optString("Contact_No").equalsIgnoreCase("")) {
                        hotelDetails.setContactNumber(jsonObj.optString("Contact_No"));
                    }if (!jsonObj.optString("ID_Proof_No").equalsIgnoreCase("null") && !jsonObj.optString("ID_Proof_No").equalsIgnoreCase("")) {
                        hotelDetails.setIdNumber(jsonObj.optString("ID_Proof_No"));
                    }if (!jsonObj.optString("Type_of_ID_Proof").equalsIgnoreCase("null") && !jsonObj.optString("Type_of_ID_Proof").equalsIgnoreCase("")) {
                        hotelDetails.setIdtype(jsonObj.optString("Type_of_ID_Proof"));
                    }if (!jsonObj.optString("Coming_From").equalsIgnoreCase("null") && !jsonObj.optString("Coming_From").equalsIgnoreCase("")) {
                        hotelDetails.setCommingFrom(jsonObj.optString("Coming_From"));
                    }if (!jsonObj.optString("Proceeding_To").equalsIgnoreCase("null") && !jsonObj.optString("Proceeding_To").equalsIgnoreCase("")) {
                        hotelDetails.setProceedingTo(jsonObj.optString("Proceeding_To"));
                    }if (!jsonObj.optString("Accompaning_No_Major").equalsIgnoreCase("null") && !jsonObj.optString("Accompaning_No_Major").equalsIgnoreCase("")) {
                        hotelDetails.setAdult(jsonObj.optString("Accompaning_No_Major"));
                    }if (!jsonObj.optString("Accompaning_No_Minor").equalsIgnoreCase("null") && !jsonObj.optString("Accompaning_No_Minor").equalsIgnoreCase("")) {
                        hotelDetails.setChild(jsonObj.optString("Accompaning_No_Minor"));
                    }if (!jsonObj.optString("Hotel_Name").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_Name").equalsIgnoreCase("")) {
                        hotelDetails.setHotelName(jsonObj.optString("Hotel_Name"));
                    }if (!jsonObj.optString("Hotel_Address").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_Address").equalsIgnoreCase("")) {
                        hotelDetails.setHoteladdress(jsonObj.optString("Hotel_Address"));
                    }if (!jsonObj.optString("Room_No").equalsIgnoreCase("null") && !jsonObj.optString("Room_No").equalsIgnoreCase("")) {
                        hotelDetails.setHotelRoomNo(jsonObj.optString("Room_No"));
                    }
                    String arrival_date_time="";
                    if (!jsonObj.optString("Date_of_Arrival").equalsIgnoreCase("null") && !jsonObj.optString("Date_of_Arrival").equalsIgnoreCase("")) {

                        String [] arrOfStr = jsonObj.optString("Date_of_Arrival").split(" ");
                        arrival_date_time=DateUtils.changeDateFormat(arrOfStr[0]);
                    }
                    if (!jsonObj.optString("Time_of_Arrival").equalsIgnoreCase("null") && !jsonObj.optString("Time_of_Arrival").equalsIgnoreCase("")) {


                        arrival_date_time=arrival_date_time +" at "+ jsonObj.optString("Time_of_Arrival");
                    }
                    hotelDetails.setArrivalDate(arrival_date_time);
                    String dep_date_time="";
                    if (!jsonObj.optString("Date_of_Departure").equalsIgnoreCase("null") && !jsonObj.optString("Date_of_Departure").equalsIgnoreCase("")) {

                        String [] arrOfStr = jsonObj.optString("Date_of_Departure").split(" ");
                        dep_date_time=DateUtils.changeDateFormat(arrOfStr[0]);
                    }
                    if (!jsonObj.optString("Time_of_Departure").equalsIgnoreCase("null") && !jsonObj.optString("Time_of_Departure").equalsIgnoreCase("")) {

                        dep_date_time=dep_date_time +" at "+ jsonObj.optString("Time_of_Departure");
                    }
                    hotelDetails.setDepatureDate(dep_date_time);
                    int  purposeOfVisit;
                    if (!jsonObj.optString("Purpose_of_visit").equalsIgnoreCase("null") && !jsonObj.optString("Purpose_of_visit").equalsIgnoreCase("")) {

                        purposeOfVisit=jsonObj.optInt("Purpose_of_visit");
                        hotelDetails.setVisitPurpose(Constants.PURPOSE_OF_VISIT[purposeOfVisit-1]);
                    }
                    if (!jsonObj.optString("Police_Area").equalsIgnoreCase("null") && !jsonObj.optString("Police_Area").equalsIgnoreCase("")) {

                        hotelDetails.setPoliceArea(jsonObj.optString("Police_Area"));
                    }
                    if (!jsonObj.optString("divn").equalsIgnoreCase("null") && !jsonObj.optString("divn").equalsIgnoreCase("")) {

                        hotelDetails.setDivision(jsonObj.optString("divn"));
                    }



                }
                else if(visitoreType.equalsIgnoreCase("2")){
                    if (!jsonObj.optString("Full_Name").equalsIgnoreCase("null") && !jsonObj.optString("Full_Name").equalsIgnoreCase("")) {
                        hotelDetails.setName(jsonObj.optString("Full_Name"));
                    }
                    if (!jsonObj.optString("Address_in_Country_where_Residing_Permanently").equalsIgnoreCase("null") && !jsonObj.optString("Address_in_Country_where_Residing_Permanently").equalsIgnoreCase("")) {
                        hotelDetails.setAddress_foreign(jsonObj.optString("Address_in_Country_where_Residing_Permanently"));
                    }
                    if (!jsonObj.optString("Address_Reference_in_India").equalsIgnoreCase("null") && !jsonObj.optString("Address_Reference_in_India").equalsIgnoreCase("")) {
                        hotelDetails.setAddress(jsonObj.optString("Address_Reference_in_India"));
                    }
                    String imageURL="";
                    if (!jsonObj.optString("Photograph").equalsIgnoreCase("null") && !jsonObj.optString("Photograph").equalsIgnoreCase("")) {
                        imageURL=imageURL+Constants.IMAGE_URL_FOREIGN;
                        imageURL=imageURL+jsonObj.optString("Photograph");
                    }
                    hotelDetails.setImage(imageURL);

                    if (!jsonObj.optString("ID").equalsIgnoreCase("null") && !jsonObj.optString("ID").equalsIgnoreCase("")) {
                        hotelDetails.setId(jsonObj.optString("ID"));
                    }
                    if (!jsonObj.optString("Date_of_Birth").equalsIgnoreCase("null") && !jsonObj.optString("Date_of_Birth").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Date_of_Birth").split(" ");
                        String date_of_birth= DateUtils.changeDateFormat(arrOfStr[0]);
                        hotelDetails.setDateOfBirth(date_of_birth);
                       // hotelDetails.setDateOfBirth(jsonObj.optString("Date_of_Birth"));
                    }
                    if (!jsonObj.optString("Nationality").equalsIgnoreCase("null") && !jsonObj.optString("Nationality").equalsIgnoreCase("")) {
                        hotelDetails.setNationality(jsonObj.optString("Nationality"));
                    }
                    if (!jsonObj.optString("Passport_No").equalsIgnoreCase("null") && !jsonObj.optString("Passport_No").equalsIgnoreCase("")) {
                        hotelDetails.setPassportNo(jsonObj.optString("Passport_No"));
                    }
                    if (!jsonObj.optString("Place_of_Issue_of_Passport").equalsIgnoreCase("null") && !jsonObj.optString("Place_of_Issue_of_Passport").equalsIgnoreCase("")) {
                        hotelDetails.setPlaceOfIssuePassport(jsonObj.optString("Place_of_Issue_of_Passport"));
                    }

                    if (!jsonObj.optString("Date_of_Issue_of_Passport").equalsIgnoreCase("null") && !jsonObj.optString("Date_of_Issue_of_Passport").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Date_of_Issue_of_Passport").split(" ");
                        String date_of_issue_of_passport=DateUtils.changeDateFormat(arrOfStr[0]);
                        hotelDetails.setDateOfIssuePasport(date_of_issue_of_passport);
                    }
                    if (!jsonObj.optString("Passport_Valid_Till").equalsIgnoreCase("null") && !jsonObj.optString("Passport_Valid_Till").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Passport_Valid_Till").split(" ");
                        String passport_valid_till=DateUtils.changeDateFormat(arrOfStr[0]);
                        hotelDetails.setPassportVlidTill(passport_valid_till);
                    }


                    if (!jsonObj.optString("Visa_No").equalsIgnoreCase("null") && !jsonObj.optString("Visa_No").equalsIgnoreCase("")) {
                        hotelDetails.setVissaNo(jsonObj.optString("Visa_No"));
                    }
                    if (!jsonObj.optString("Visa_Issue_Date").equalsIgnoreCase("null") && !jsonObj.optString("Visa_Issue_Date").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Visa_Issue_Date").split(" ");
                        String visa_issue_date=DateUtils.changeDateFormat(arrOfStr[0]);
                        hotelDetails.setVissaIsueDate(visa_issue_date);
                    }
                    if (!jsonObj.optString("Visa_Valid_Till").equalsIgnoreCase("null") && !jsonObj.optString("Visa_Valid_Till").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Visa_Valid_Till").split(" ");
                        String visa_valid_till=DateUtils.changeDateFormat(arrOfStr[0]);
                        hotelDetails.setVissaValidTill(visa_valid_till);
                    }

                    if (!jsonObj.optString("Type_of_Visa").equalsIgnoreCase("null") && !jsonObj.optString("Type_of_Visa").equalsIgnoreCase("")) {
                        hotelDetails.setTypeOfVissa(jsonObj.optString("Type_of_Visa"));
                    }
                    if (!jsonObj.optString("Visa_Issue_Place").equalsIgnoreCase("null") && !jsonObj.optString("Visa_Issue_Place").equalsIgnoreCase("")) {
                        hotelDetails.setVissaIssuePlace(jsonObj.optString("Visa_Issue_Place"));
                    }

                    if (!jsonObj.optString("Phone_No_in_India").equalsIgnoreCase("null") && !jsonObj.optString("Phone_No_in_India").equalsIgnoreCase("")) {
                        hotelDetails.setContactNumber(jsonObj.optString("Phone_No_in_India"));
                    }
                    if (!jsonObj.optString("Phone_No_in_Country_Where_Permanently_Residing").equalsIgnoreCase("null") && !jsonObj.optString("Phone_No_in_Country_Where_Permanently_Residing").equalsIgnoreCase("")) {
                        hotelDetails.setContactNumberForeign(jsonObj.optString("Phone_No_in_Country_Where_Permanently_Residing"));
                    }
                    if (!jsonObj.optString("Arrived_From").equalsIgnoreCase("null") && !jsonObj.optString("Arrived_From").equalsIgnoreCase("")) {
                        hotelDetails.setCommingFrom(jsonObj.optString("Arrived_From"));
                    }
                    if (!jsonObj.optString("Proceeding_To").equalsIgnoreCase("null") && !jsonObj.optString("Proceeding_To").equalsIgnoreCase("")) {
                        hotelDetails.setProceedingTo(jsonObj.optString("Proceeding_To"));
                    }
                    if (!jsonObj.optString("Hotel_Name").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_Name").equalsIgnoreCase("")) {
                        hotelDetails.setHotelName(jsonObj.optString("Hotel_Name"));
                    }
                    if (!jsonObj.optString("Hotel_Code").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_Code").equalsIgnoreCase("")) {
                        hotelDetails.setHotelCode(jsonObj.optString("Hotel_Code"));
                    }
                    if (!jsonObj.optString("Hotel_Address").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_Address").equalsIgnoreCase("")) {
                        hotelDetails.setHoteladdress(jsonObj.optString("Hotel_Address"));
                    }
                    if (!jsonObj.optString("Hotel_PS").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_PS").equalsIgnoreCase("")) {
                        hotelDetails.setHotelPs(jsonObj.optString("Hotel_PS"));
                    }
                    if (!jsonObj.optString("Hotel_Phone_No").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_Phone_No").equalsIgnoreCase("")) {
                        hotelDetails.setHotelphone(jsonObj.optString("Hotel_Phone_No"));
                    }
                    if (!jsonObj.optString("Status").equalsIgnoreCase("null") && !jsonObj.optString("Status").equalsIgnoreCase("")) {
                        hotelDetails.setHotelStatus(jsonObj.optString("Status"));
                    }

                    if (!jsonObj.optString("Room_No").equalsIgnoreCase("null") && !jsonObj.optString("Room_No").equalsIgnoreCase("")) {
                        hotelDetails.setHotelRoomNo(jsonObj.optString("Room_No"));
                    }

                    String arrival_date_time="";
                    if (!jsonObj.optString("Date_of_Arrival_in_Hotel").equalsIgnoreCase("null") && !jsonObj.optString("Date_of_Arrival_in_Hotel").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Date_of_Arrival_in_Hotel").split(" ");
                        arrival_date_time=DateUtils.changeDateFormat(arrOfStr[0]);

                    }
                    if (!jsonObj.optString("Time_of_Arrival_in_Hotel").equalsIgnoreCase("null") && !jsonObj.optString("Time_of_Arrival_in_Hotel").equalsIgnoreCase("")) {


                        arrival_date_time=arrival_date_time+ " at "+jsonObj.optString("Time_of_Arrival_in_Hotel");
                    }
                    hotelDetails.setArrivalDate(arrival_date_time);
                    String dep_date_time="";
                    if (!jsonObj.optString("Date_of_Departure").equalsIgnoreCase("null") && !jsonObj.optString("Date_of_Departure").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Date_of_Departure").split(" ");
                        dep_date_time=DateUtils.changeDateFormat(arrOfStr[0]);

                    }
                    if (!jsonObj.optString("Time_of_Departure").equalsIgnoreCase("null") && !jsonObj.optString("Time_of_Departure").equalsIgnoreCase("")) {

                        dep_date_time=dep_date_time+ " at "+jsonObj.optString("Time_of_Departure");
                    }
                    hotelDetails.setDepatureDate(dep_date_time);
                    if (!jsonObj.optString("Purpose_of_Visit").equalsIgnoreCase("null") && !jsonObj.optString("Purpose_of_Visit").equalsIgnoreCase("")) {

                        hotelDetails.setVisitPurpose(jsonObj.optString("Purpose_of_Visit"));
                    }
                    if (!jsonObj.optString("Police_Area").equalsIgnoreCase("null") && !jsonObj.optString("Police_Area").equalsIgnoreCase("")) {

                        hotelDetails.setPoliceArea(jsonObj.optString("Police_Area"));
                    }
                    if (!jsonObj.optString("divn").equalsIgnoreCase("null") && !jsonObj.optString("divn").equalsIgnoreCase("")) {

                        hotelDetails.setDivision(jsonObj.optString("divn"));
                    }



                }

                hotelDetailsList.add(hotelDetails);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        int currentPosition = lv_hotelSearchList.getFirstVisiblePosition();
        int lastVisiblePosition = lv_hotelSearchList.getLastVisiblePosition();

        // Setting new scroll position
        lv_hotelSearchList.setSelectionFromTop(currentPosition + 1, 0);

        if((Integer.parseInt(totalResult)-lastVisiblePosition)<20){
            lv_hotelSearchList.removeFooterView(btnLoadMore);
        }

    }
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        HotelDetails hotelDetails;
        hotelDetails=hotelDetailsList.get(position);
        Intent intent=new Intent(HotelSearchResultActivity.this,HotelSearchDetailsActivity.class);
        intent.putExtra("visitoreType",visitoreType);
        intent.putExtra("HOTEL_DETAILS", hotelDetails);
        startActivity(intent);


    }
}
