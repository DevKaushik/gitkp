package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gcm.GCMRegistrar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kp.facedetection.model.AllContentList;
import com.kp.facedetection.model.LIPartialSavedData;
import com.kp.facedetection.model.PoliceStationList;
import com.kp.facedetection.model.SpecialServicesModel;
import com.kp.facedetection.model.UserInfo;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class SpecialServiceActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private String userName = "";
    private String userId = "";
    private String loginNumber = "";
    private String appVersion = "";
    TextView chargesheetNotificationCount;
    String notificationCount = "";
    EditText et_request_type, et_holder_request_name, et_holder_refer;
    EditText et_reference_ps, et_reference_year, et_reference_caseNo, et_reference_enquiry ,et_enquiry_details;
    EditText et_callDump_Date,et_callDump_From_time,et_callDump_To_time;
    private String[] requestArray;//= {"CDR", "SDR", "IMEI TRACK", "TOWER LOCATION", "IPDR", "ILD", "CALL DUMP", "CAF", "IP RESOLUTION"};//1,2,3,4,5,6,7,8,9
    private List<String> requestList = new ArrayList<String>();
    private String[] referenceArry = {"Case", "Enquiry"};
    private String[] liArray = {"Phone", "Imei"};
    private String selectedrequestTypeValue = "";
    private ArrayList<String> requestArrayList = new ArrayList<String>();
    private ArrayList<String> referenceArrayList;
    private ArrayList<String> liArrayList;


    TextView tv_holderPhone, tv_circle, tv_fromDate, tv_toDate;
    EditText et_phone, et_circle, et_fromDate, et_toDate, et_phone_imei;
    Button bttn_save_request, bttn_add_request_form, bttn_save_request_LI, bttn_partial_save_request_LI;
    String devicetoken;
    String phoneNoValue, circleValue, fromDatevalue, toDateValue;
    String selectedrequestType = "";
    String selectedreferenceType = "";

    String selectedLIType = "Phone";
    String selectedType = "Phone";
    RelativeLayout rl_special_service_form, rl_mslip_layout;
    LinearLayout ll_date_holder, ll_special_services_form_all, ll_special_services_form_cell;
    LinearLayout ll_reference_case, ll_reference_enquiry;
    LinearLayout ll_LI_addtional_layout_above_addmore, ll_LI_addtional_layout_below_addmore,ll_callDump_addtional_layout_above_addmore;
    AllContentList allContentList;
    List<String> policeStationArrayList = new ArrayList<String>();
    protected String[] array_policeStation;
    public JSONArray policeStationListArray;
    ArrayList<PoliceStationList> obj_policeStationList;
    protected ArrayList<CharSequence> selectedPoliceStations = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedPoliceStationsId = new ArrayList<CharSequence>();
    public String policeStationString = "";
    private List<String> yearList = new ArrayList<String>();
    private String[] yearArray;
    private String[] logger_arr_str;
    private  String[] provider_arr_str={"AIRTEL","VODAPHONE","JIO","BSNL KOL","BSNL WB","IDEA"};
    private String[] group_arr_str = new String[0];
    AutoCompleteTextView autoCompleteTextViewGroup;
    public View addviewLi;
    private RadioGroup radioGroupConnReConn;
    private RadioButton radioButtonConn, radioButtonReConn;
    private String[] justificationTitle;
    private String[] justificationTemplate;
    private Spinner spjustTitle;
    private EditText et_justification;
    EditText et_state;
    EditText et_listener, et_user;
    String draft_complete_flag = "Complete";
    String fcmDeviceId = "";
    String auth_key = "";
    String[] stateNameList;
    String[] stateIdList;
    String selectedStateId = "";
    JSONArray group_arr;
    boolean partialsavedLIform = false;
    String requestType = "";
    String partialSavedLIRequestMasterID = "";
    LIPartialSavedData partialSavedData = null;
    String ps_name="",state="";
    String phone_imei_flag="";
    RelativeLayout Rl_reqest_process_type;
    String urgent_general_flag="0";
    RadioGroup radioGroup;
    RadioButton RbGeneral,RbUrgent;
    ImageView toggleStateType;
    String panIndiaFlag="1";
    boolean search_pan_india_flag=false;
   // TextInputLayout textInputLayoutEnquirydetails;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_service);
        setToolBar();
        initViews();
        getIntentdata();
        getFcmToken();


    }

    private void getIntentdata() {

        requestType = getIntent().getStringExtra("REQUEST_TYPE");
        partialSavedLIRequestMasterID = getIntent().getStringExtra("REQUEST_MAASTER_ID");
        if (requestType.equalsIgnoreCase("LI_PARTIAL_SAVE")) {
            partialsavedLIform = true;
            populateLIPartialSavedData();
        }

    }

    private void populateLIPartialSavedData() {

        et_request_type.setText("LI");
        selectedrequestType =Constants.SPECIAL_SERVICE_ARRAY[9];
        setselectedrequestTypeValue();
        checkingRequestPermission(); //
    }

    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
            userId = Utility.getUserInfo(this).getUserId();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout) mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this, charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount = (TextView) mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount = Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }

    public void initViews() {
        Rl_reqest_process_type=(RelativeLayout)findViewById(R.id.Rl_reqest_process_type);
        radioGroup=(RadioGroup)findViewById(R.id.Rg_reqest_process_type);
        RbGeneral=(RadioButton)findViewById(R.id.Rb_general);
        RbGeneral.setOnClickListener(this);
        RbGeneral.setChecked(true);
        RbUrgent=(RadioButton)findViewById(R.id.Rb_urgent);
        RbUrgent.setOnClickListener(this);
        et_request_type = (EditText) findViewById(R.id.et_request_type);
        et_holder_request_name = (EditText) findViewById(R.id.et_holder_request_name);
        et_holder_refer = (EditText) findViewById(R.id.et_holder_refer);
        et_reference_ps = (EditText) findViewById(R.id.et_reference_ps);
        et_reference_year = (EditText) findViewById(R.id.et_reference_year);
        et_reference_caseNo = (EditText) findViewById(R.id.et_reference_caseNo);
        et_reference_enquiry = (EditText) findViewById(R.id.et_reference_enquiry);
        et_enquiry_details = (EditText) findViewById(R.id.et_enquiry_details);
        et_phone_imei = (EditText) findViewById(R.id.et_phone_imei);
        ll_reference_case = (LinearLayout) findViewById(R.id.ll_reference_case);
        ll_reference_enquiry = (LinearLayout) findViewById(R.id.ll_reference_enquiry);
       // textInputLayoutEnquirydetails=(TextInputLayout)findViewById(R.id.input_enquiry_details);

        ll_LI_addtional_layout_above_addmore = (LinearLayout) findViewById(R.id.ll_LI_addtional_layout_above_addmore);
        ll_LI_addtional_layout_below_addmore = (LinearLayout) findViewById(R.id.ll_LI_addtional_layout_below_addmore);
        ll_callDump_addtional_layout_above_addmore=(LinearLayout) findViewById(R.id.ll_callDump_addtional_layout_above_addmore);
        et_callDump_Date=(EditText) findViewById(R.id.et_callDump_date);
        et_callDump_From_time=(EditText) findViewById(R.id.et_callDump_from_time);
        et_callDump_To_time=(EditText) findViewById(R.id.et_callDump_to_time);


        autoCompleteTextViewGroup = (AutoCompleteTextView) findViewById(R.id.acom_group);
        toggleStateType=(ImageView) findViewById(R.id.Tb_search_type);
        toggleStateType.setOnClickListener(this);
        et_state = (EditText) findViewById(R.id.et_state);
        // radioGroupConnReConn=(RadioGroup)findViewById(R.id.radioGroupConnReConn);
        // radioGroupConnReConn.setOnClickListener(this);
        // radioButtonConn=(RadioButton)findViewById(R.id.radioButtonConn);
        //  radioButtonConn.setOnClickListener(this);
        // radioButtonReConn=(RadioButton)findViewById(R.id.radioButtonReConn);
        //radioButtonReConn.setOnClickListener(this);
        // rl_mslip_layout=(RelativeLayout)findViewById(R.id.rl_mslip_layout);
        et_listener = (EditText) findViewById(R.id.et_listener);
        et_user = (EditText) findViewById(R.id.et_user);
        spjustTitle = (Spinner) findViewById(R.id.sp_tv_justification_type);
        et_justification = (EditText) findViewById(R.id.et_justification);

        tv_holderPhone = (TextView) findViewById(R.id.tv_holderPhone);
        tv_circle = (TextView) findViewById(R.id.tv_circle);
        tv_fromDate = (TextView) findViewById(R.id.tv_fromDate);
        tv_toDate = (TextView) findViewById(R.id.tv_toDate);

        et_holder_request_name.setText(userName);

        rl_special_service_form = (RelativeLayout) findViewById(R.id.rl_special_services_form);
        // ll_special_services_form_all==(LinearLayout)findViewById(R.id.ll_special_services_form);
        ll_special_services_form_all = (LinearLayout) findViewById(R.id.ll_all_special_services_form);
        ll_date_holder = (LinearLayout) findViewById(R.id.ll_date);


        creatRequestTypeList();
        requestArray = requestArrayList.toArray(new String[requestArrayList.size()]);
        referenceArrayList = new ArrayList<>(Arrays.asList(referenceArry));
        liArrayList = new ArrayList<>(Arrays.asList(liArray));

        et_phone = (EditText) findViewById(R.id.et_phone);
        et_circle = (EditText) findViewById(R.id.et_circle);
        et_fromDate = (EditText) findViewById(R.id.et_fromDate);
        et_toDate = (EditText) findViewById(R.id.et_toDate);

        Calendar calendar = Calendar.getInstance();
        int current_year = calendar.get(Calendar.YEAR);

        /* Case year Spinner set */
        yearList.clear();

        for (int i = current_year; i >= 1980; i--) {
            yearList.add(Integer.toString(i));
        }
        yearArray = yearList.toArray(new String[yearList.size()]);

        bttn_save_request = (Button) findViewById(R.id.bttn_save_request);
        bttn_save_request.setOnClickListener(this);
        bttn_save_request_LI = (Button) findViewById(R.id.bttn_save_request_LI);
        bttn_save_request_LI.setOnClickListener(this);
        bttn_partial_save_request_LI = (Button) findViewById(R.id.bttn_partial_save_request_LI);
        bttn_partial_save_request_LI.setOnClickListener(this);
        bttn_add_request_form = (Button) findViewById(R.id.bttn_add_request_form);
        bttn_add_request_form.setOnClickListener(this);


        clickEvents();
    }

    public void getFcmToken() {
        // Toast.makeText(this, "getFcmToken called", Toast.LENGTH_SHORT).show();

        fcmDeviceId = FirebaseInstanceId.getInstance().getToken();
        Log.d("DAPL", "FCMID:-----" + fcmDeviceId);
        if ((fcmDeviceId == null || fcmDeviceId.equals(""))) {
            fcmDeviceId = Utility.getFCMToken(this);
        } else {
            Utility.setFCMToken(this, fcmDeviceId);
        }
        Log.d("DAPL", "___________________________________" + fcmDeviceId);
    }

    public void fetchloggerinfo() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_SPECIAL_SERVICE_LI_DATA);
        taskManager.setSpecialServiceLoggerInfo(true);
        String[] keys = {"user_ps", "user_div_code"};
        String[] values = {Utility.getUserInfo(this).getUserPscode(), Utility.getUserInfo(this).getUserDivisionCode()};
        taskManager.doStartTask(keys, values, true, true);
    }

    public void parseSpecialServiceLoggerInfoResult(String response) {
        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optString("status").equals("1")) {
                    JSONArray logger_arr = jsonObject.optJSONArray("logger_arr");
                    group_arr = jsonObject.optJSONArray("group_arr");
                    JSONArray justification_arr = jsonObject.optJSONArray("justification_arr");
                    if (logger_arr.length() > 0) {
                        logger_arr_str = new String[logger_arr.length()];
                        for (int i = 0; i < logger_arr.length(); i++) {
                            JSONObject loggerNoObject = (JSONObject) logger_arr.get(i);
                            logger_arr_str[i] = loggerNoObject.optString("logger_no");
                        }
                    }
                    if (group_arr.length() > 0) {
                        group_arr_str = new String[group_arr.length()];
                        for (int i = 0; i < group_arr.length(); i++) {
                            JSONObject groupObject = (JSONObject) group_arr.get(i);
                            group_arr_str[i] = groupObject.optString("group_name");
                        }
                    }
                    if (justification_arr.length() > 0) {
                        int justTitleCount = justification_arr.length() + 1;
                        int justTemplateCount = justification_arr.length() + 1;

                        justificationTitle = new String[justTitleCount];
                        justificationTemplate = new String[justTemplateCount];
                        justificationTitle[0] = "Select Justification Type";
                        justificationTemplate[0] = "";
                        int j = 1;
                        for (int i = 0; i < justification_arr.length(); i++) {

                            JSONObject justObject = (JSONObject) justification_arr.get(i);
                            justificationTitle[j] = justObject.optString("title");
                            justificationTemplate[j] = justObject.optString("subject");
                            j++;
                        }
                    }

                    setGroupAndLoggetValue();
                    changeRequestFormLayout();

                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    private void setGroupAndLoggetValue() {
        if (group_arr.length() > 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                    (this, android.R.layout.select_dialog_item, group_arr_str);
            autoCompleteTextViewGroup.setThreshold(1);
            autoCompleteTextViewGroup.setAdapter(adapter);
        }
        spjustTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Toast.makeText(SpecialServiceActivity.this, "Position" + position, Toast.LENGTH_SHORT).show();
                et_justification.setText(justificationTemplate[position]);
                ((TextView) spjustTitle.getSelectedView()).setTextColor(getResources().getColor(R.color.color_deep_blue));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, justificationTitle);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spjustTitle.setAdapter(arrayAdapter);


    }

    private void creatRequestTypeList() {
        //CALL DUMP ,IP RESL
        if (Utility.getUserInfo(this).getCdrRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("CDR");
        }
        if (Utility.getUserInfo(this).getSdrRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("SDR");
        }
        if (Utility.getUserInfo(this).getImeiRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("IMEI TRACK");
        }
        if (Utility.getUserInfo(this).getTowerRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("TOWER LOCATION");
        }
        if (Utility.getUserInfo(this).getIpdrRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("IPDR");
        }
        if (Utility.getUserInfo(this).getIldRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("ILD");
        }
      /*  if (Utility.getUserInfo(this).getCdumpRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("CALL DUMP");
        }
*/
        if (Utility.getUserInfo(this).getCafRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("CAF");
        }
        if (Utility.getUserInfo(this).getLoggerRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("LI");
        }
        if (Utility.getUserInfo(this).getMnpRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("MNP");
        }
        if (Utility.getUserInfo(this).getRdRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("Recharge Details");
        }
        if (Utility.getUserInfo(this).getCellIdChart().equalsIgnoreCase("1")) {
            requestArrayList.add("CELL-ID CHART");
        }

    }

    private void clickEvents() {

        et_callDump_Date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    DateUtils.setDate(SpecialServiceActivity.this, et_callDump_Date, true);
                }
                return false;
            }
        });
        et_callDump_From_time.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                   DateUtils.setTime(SpecialServiceActivity.this,et_callDump_From_time);
                }
                return false;
            }
        });
       et_callDump_To_time.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    DateUtils.setTime(SpecialServiceActivity.this,et_callDump_To_time);
                }
                return false;
            }
        });

        et_request_type.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showRequestTypedialog();

                }
                return false;
            }
        });
        et_holder_refer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showReferenceTypedialog();

                }
                return false;
            }
        });
        et_reference_ps.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    fetchAllContent();

                }
                return false;
            }
        });

        et_reference_year.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showYearDialog();

                }
                return false;
            }
        });
        et_phone_imei.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showPhoneImeiDialog();

                }
                return false;
            }
        });
        et_state.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    fetchAllState();


                }
                return false;
            }
        });


    }

    private void showYearDialog() {

        if (yearList.size() > 0) {
            final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this, R.style.myDialog);
            builder.setTitle("Select Year")
                    .setItems(yearArray, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // The 'which' argument contains the index position
                            // of the selected item
                            et_reference_year.setText(yearArray[which]);
                        }
                    });

            android.support.v7.app.AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void showPhoneImeiDialog() {
        if (liArrayList.size() > 0) {
            final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this, R.style.myDialog);
            builder.setTitle("Select LI Type")
                    .setItems(liArray, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // The 'which' argument contains the index position
                            // of the selected item

                            et_phone_imei.setText(liArrayList.get(which));
                            selectedLIType = liArray[which];
                            if (ll_special_services_form_all.getChildCount() > 0) {
                                ll_special_services_form_all.removeAllViews();

                            }


                        }
                    });

            android.support.v7.app.AlertDialog dialog = builder.create();
            dialog.show();
        }

    }

    public void fetchAllState() {
        try {
            auth_key = Utility.getDocumentSearchSesionId(this);
        } catch (Exception e) {

        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.MODIFIED_METHOD_SDR_SEARCH_ALL_STATE);
        taskManager.setLISearchAllState(true);
        String[] keys = {"imei"};
        String[] values = {Utility.getImiNO(this)};
        taskManager.doStartTask(keys, values, true, true);

    }

    public void parseLISearchAllStateResult(String response) {
        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optString("status").equals("1")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("result");
                    if (jsonArray.length() > 0) {
                        stateNameList = new String[jsonArray.length()];
                        stateIdList = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObjectState = jsonArray.optJSONObject(i);
                            stateIdList[i] = jsonObjectState.optString("id");
                            stateNameList[i] = jsonObjectState.optString("state");
                        }
                    }
                    showStateDialog();
                }

            } catch (Exception e) {

            }
        }

    }

    public void showStateDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select State");
        builder.setSingleChoiceItems(stateNameList, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                state=stateNameList[which];
                et_state.setText(stateNameList[which]);
                selectedStateId = stateIdList[which];
                //  Toast.makeText(SpecialServiceActivity.this, "ID:"+selectedStateId, Toast.LENGTH_SHORT).show();

            }
        });

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                dialog.dismiss();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                //tv_ps_value.setText("Select Police Stations");
                et_state.setText("");

            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();

    }
    // for fetching PS list--------------------------------------------------------------------//

    private void fetchAllContent() {

        if (!Constants.internetOnline(this)) {
            Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_GETCONTENT);
        taskManager.setAllContentSpService(true);
        String[] keys = {};
        String[] values = {};
        taskManager.doStartTask(keys, values, true);
    }

    public void parseGetAllContentResponse(String response) {

        //System.out.println("Get All Content Result: " + response);

        //fetchIdentityCategory();

        if (response != null && !response.equals("")) {

            try {
                Constants.divisionArrayList.clear();
                Constants.crimeCategoryArrayList.clear();
                Constants.policeStationIDArrayList.clear();
                Constants.policeStationNameArrayList.clear();
                //Constants.IOCodeArrayList.clear();
                //Constants.IONameArrayList.clear();
                Constants.firStatusArrayList.clear();

                //	Constants.firStatusArrayList.add("Select a status");

                JSONObject jObj = new JSONObject(response);
                allContentList = new AllContentList();
                if (jObj != null && jObj.optString("status").equalsIgnoreCase("1")) {

                    allContentList.setStatus(jObj.optString("status"));
                    allContentList.setMessage(jObj.optString("message"));

                    policeStationListArray = jObj.getJSONObject("result").getJSONArray("policestationlist");


                    for (int i = 0; i < policeStationListArray.length(); i++) {

                        PoliceStationList policeStationList = new PoliceStationList();

                        JSONObject row = policeStationListArray.getJSONObject(i);
                        policeStationList.setPs_code(row.optString("PSCODE"));
                        policeStationList.setPs_name(row.optString("PSNAME"));
                        policeStationList.setDiv_code(row.optString("DIVCODE"));
                        policeStationList.setOc_name(row.optString("OCNAME"));

                        policeStationArrayList.add(row.optString("PSNAME"));

                        Constants.policeStationNameArrayList.add(row.optString("PSNAME"));
                        Constants.policeStationIDArrayList.add(row.optString("PSCODE"));

                        allContentList.setObj_policeStationList(policeStationList);

                    }


                    obj_policeStationList = allContentList.getObj_policeStationList();
                    array_policeStation = new String[policeStationArrayList.size()];
                    array_policeStation = policeStationArrayList.toArray(array_policeStation);
                    showSelectPoliceStationsDialog();


                } else {
                    showAlertDialog(this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Tb_search_type:

                if(!search_pan_india_flag){
                    toggleStateType.setImageResource(R.drawable.toggle_right);
                    panIndiaFlag="0";
                    search_pan_india_flag=true;
                }
                else{
                    toggleStateType.setImageResource(R.drawable.toggle_left);
                    panIndiaFlag="1";
                    search_pan_india_flag=false;
                }
                break;
            case R.id.bttn_save_request:

               /* phoneNoValue=et_phone.getText().toString();
                circleValue=et_circle.getText().toString();
                fromDatevalue=et_fromDate.getText().toString();
                toDateValue=et_toDate.getText().toString();
               */
                if (et_request_type.getText().toString().trim().isEmpty()) {
                    Utility.showAlertDialog(this, " Error ", "Please Select Request type ", false);
                }
                else if(et_enquiry_details.getText().toString().trim().isEmpty()){
                    Utility.showAlertDialog(this, " Error ", "Please enter reasoning of request ", false);
                }
                else if (et_holder_refer.getText().toString().trim().isEmpty()) {
                    Utility.showAlertDialog(this, "Error", "Please Select Refernce type", false);
                } else if (et_holder_refer.getText().toString().equalsIgnoreCase("Case")) {
                    if (et_reference_ps.getText().toString().trim().isEmpty()) {
                        Utility.showAlertDialog(this, " Error ", "Please select ps ", false);
                    } else if (et_reference_year.getText().toString().trim().isEmpty()) {
                        Utility.showAlertDialog(this, " Error ", "Please select year ", false);
                    } else if (et_reference_caseNo.getText().toString().trim().isEmpty()) {
                        Utility.showAlertDialog(this, " Error ", "Please enter caseNo ", false);
                    } else {
                        checkRequestType();
                    }

                } else if (et_holder_refer.getText().toString().equalsIgnoreCase("Enquiry")) {
                    if (et_reference_enquiry.getText().toString().trim().isEmpty()) {
                        Utility.showAlertDialog(this, " Error ", "Please enter enquiry ", false);
                    } else {
                        checkRequestType();
                    }
                }
                break;
            case R.id.bttn_save_request_LI:
               // Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show();
                if (et_request_type.getText().toString().trim().isEmpty()) {
                    Utility.showAlertDialog(this, " Error ", "Please Select Request type ", false);
                }
                else if(et_enquiry_details.getText().toString().trim().isEmpty()){
                    Utility.showAlertDialog(this, " Error ", "Please enter reasoning of request", false);
                }else if (et_holder_refer.getText().toString().trim().isEmpty()) {
                    Utility.showAlertDialog(this, "Error", "Please Select Refernce type", false);
                } else if (et_holder_refer.getText().toString().equalsIgnoreCase("Case")) {
                    if (et_reference_ps.getText().toString().trim().isEmpty()) {
                        Utility.showAlertDialog(this, " Error ", "Please select ps ", false);
                    } else if (et_reference_year.getText().toString().trim().isEmpty()) {
                        Utility.showAlertDialog(this, " Error ", "Please select year ", false);
                    } else if (et_reference_caseNo.getText().toString().trim().isEmpty()) {
                        Utility.showAlertDialog(this, " Error ", "Please enter caseNo ", false);
                    } else {
                        checkRequestType();
                    }

                } else if (et_holder_refer.getText().toString().equalsIgnoreCase("Enquiry")) {
                    if (et_reference_enquiry.getText().toString().trim().isEmpty()) {
                        Utility.showAlertDialog(this, " Error ", "Please enter enquiry ", false);
                    }else {
                        checkRequestType();
                    }
                }
                break;
            case R.id.bttn_partial_save_request_LI:
                if (et_holder_refer.getText().toString().equalsIgnoreCase("Case")) {
                    if (et_reference_ps.getText().toString().trim().isEmpty()) {
                        Utility.showAlertDialog(this, " Error ", "Please select ps ", false);
                        return;
                    } else if (et_reference_year.getText().toString().trim().isEmpty()) {
                        Utility.showAlertDialog(this, " Error ", "Please select year ", false);
                        return;
                    } else if (et_reference_caseNo.getText().toString().trim().isEmpty()) {
                        Utility.showAlertDialog(this, " Error ", "Please enter caseNo ", false);
                        return;
                    } else {
                        setpartialsaveDetails();
                    }

                }
                else{
                    setpartialsaveDetails();
                }


                break;
            case R.id.bttn_add_request_form:
                addrequestForm();
                break;

            case R.id.Rb_general:
                if(RbGeneral.isChecked()){
                    urgent_general_flag="0";
                }
                break;
            case R.id.Rb_urgent:
                if(RbUrgent.isChecked()){
                    urgent_general_flag="1";
                }


        }

    }

    public void setpartialsaveDetails() {
        draft_complete_flag = "Draft";
        JSONArray dataArray = createFormdataLI();
        saveRequestData(dataArray.toString());


    }

    public void addrequestForm() {
        if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[0])) {

            final LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addview = layoutInflater.inflate(R.layout.cdr_item_layout, null);
            final EditText et_phone = (EditText) addview.findViewById(R.id.et_phone);
            // final EditText finalCdrPhone = cdrPhone;
            et_phone.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() == 10) {
                        CdrcallforProvider(addview, et_phone.getText().toString());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            EditText et_circle = (EditText) addview.findViewById(R.id.et_circle);
            final EditText et_fromDate = (EditText) addview.findViewById(R.id.et_fromDate);
            final EditText et_toDate = (EditText) addview.findViewById(R.id.et_toDate);
            TextView tv_delete = (TextView) addview.findViewById(R.id.tv_delete_row);
            tv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LinearLayout) addview.getParent()).removeView(addview);
                }
            });
            et_fromDate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        DateUtils.setDate(SpecialServiceActivity.this, et_fromDate, true);
                    }
                    return false;
                }
            });
            et_toDate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        DateUtils.setDate(SpecialServiceActivity.this, et_toDate, true);
                    }
                    return false;
                }
            });

            ll_special_services_form_all.addView(addview);
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[1]) || selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[3]) || selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[7]) || selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[10])) {
            final LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addview = layoutInflater.inflate(R.layout.sdr_tower_caf_layout, null);
            final EditText et_phone = (EditText) addview.findViewById(R.id.et_phone);
            et_phone.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() == 10) {
                        CdrcallforProvider(addview, et_phone.getText().toString());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            TextView tv_delete = (TextView) addview.findViewById(R.id.tv_delete_row);
            tv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LinearLayout) addview.getParent()).removeView(addview);
                }
            });
            ll_special_services_form_all.addView(addview);
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[2])) {
            final LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addview = layoutInflater.inflate(R.layout.imei_item_layout, null);
            final EditText et_imei = (EditText) addview.findViewById(R.id.et_imei);

           /* EditText et_circle = (EditText) addview.findViewById(R.id.et_circle);*/
            final EditText et_fromDate = (EditText) addview.findViewById(R.id.et_fromDate);
            final EditText et_toDate = (EditText) addview.findViewById(R.id.et_toDate);
            TextView tv_delete = (TextView) addview.findViewById(R.id.tv_delete_row);
            tv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LinearLayout) addview.getParent()).removeView(addview);
                }
            });
            et_fromDate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        DateUtils.setDate(SpecialServiceActivity.this, et_fromDate, true);
                    }
                    return false;
                }
            });
            et_toDate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        DateUtils.setDate(SpecialServiceActivity.this, et_toDate, true);
                    }
                    return false;
                }
            });

            ll_special_services_form_all.addView(addview);

        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[4]) || selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[5])) {
            final LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addview = layoutInflater.inflate(R.layout.ipdr_ild_item_layout, null);
            final EditText et_phone = (EditText) addview.findViewById(R.id.et_phone);
            EditText et_circle = (EditText) addview.findViewById(R.id.et_circle);
            if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[5])) {
                et_phone.setFilters(new InputFilter[]{new InputFilter.LengthFilter(13)});
            } else {
                et_phone.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
            }
            et_phone.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                /*    if(selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[5])) {
                        if (s.length() > 13) {
                            CdrcallforProvider(addview, et_phone.getText().toString());
                        }
                    }*/
                    if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[4])) {
                        if (s.length() == 10) {
                            CdrcallforProvider(addview, et_phone.getText().toString());
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            final EditText et_fromDate = (EditText) addview.findViewById(R.id.et_fromDate);
            final EditText et_toDate = (EditText) addview.findViewById(R.id.et_toDate);
            TextView tv_delete = (TextView) addview.findViewById(R.id.tv_delete_row);
            tv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LinearLayout) addview.getParent()).removeView(addview);
                }
            });
            et_fromDate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        DateUtils.setDate(SpecialServiceActivity.this, et_fromDate, true);
                    }
                    return false;
                }
            });
            et_toDate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        DateUtils.setDate(SpecialServiceActivity.this, et_toDate, true);
                    }
                    return false;
                }
            });

            ll_special_services_form_all.addView(addview);

        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[6]) || selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[12])) {
            final LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addview = layoutInflater.inflate(R.layout.call_dump_cell_id_layout, null);
            final TextView tv_cellid = (TextView) addview.findViewById(R.id.tv_cellid);
            final EditText et_cellid = (EditText) addview.findViewById(R.id.et_cellid);
            final Spinner spProvider= (Spinner) addview.findViewById(R.id.sp_provider);
            TextView tv_delete = (TextView) addview.findViewById(R.id.tv_delete_row);
            tv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LinearLayout) addview.getParent()).removeView(addview);
                }
            });
            if(selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[6]) ){
                tv_cellid.setVisibility(View.VISIBLE);
                et_cellid.setVisibility(View.VISIBLE);
            }
            else{
                tv_cellid.setVisibility(View.GONE);
                et_cellid.setVisibility(View.GONE);
            }

            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, provider_arr_str);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            spProvider.setAdapter(arrayAdapter);
            spProvider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ((TextView) spProvider.getSelectedView()).setTextColor(getResources().getColor(R.color.color_deep_blue));

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            ll_special_services_form_all.addView(addview);


        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[9])) {
            createLIItem();

        }
        else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[11])) {

            final LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addview = layoutInflater.inflate(R.layout.reacharge_details_layout, null);
            final EditText et_phone = (EditText) addview.findViewById(R.id.et_phone);
            et_phone.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() == 10) {
                        CdrcallforProvider(addview, et_phone.getText().toString());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            //EditText et_circle = (EditText) addview.findViewById(R.id.et_circle);
            final EditText et_fromDate = (EditText) addview.findViewById(R.id.et_fromDate);
            final EditText et_toDate = (EditText) addview.findViewById(R.id.et_toDate);
            TextView tv_delete = (TextView) addview.findViewById(R.id.tv_delete_row);
            tv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LinearLayout) addview.getParent()).removeView(addview);
                }
            });
            et_fromDate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        DateUtils.setDate(SpecialServiceActivity.this, et_fromDate, true);
                    }
                    return false;
                }
            });
            et_toDate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        DateUtils.setDate(SpecialServiceActivity.this, et_toDate, true);
                    }
                    return false;
                }
            });

            ll_special_services_form_all.addView(addview);
        }



    }

    public void createLIItem() {
        if (et_state.getText().toString().equals("")) {
            Utility.showAlertDialog(this, " Error ", "Please select state ", false);
        } else {
            final LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addviewLi = layoutInflater.inflate(R.layout.legal_interception_item_layout, null);
            final TextView tv_phone_label = (TextView) addviewLi.findViewById(R.id.tv_phone);
            final TextView tv_phone_text = (TextView) addviewLi.findViewById(R.id.tv_phone_text);
            final TextView tv_officer_phone =(TextView) addviewLi.findViewById(R.id.tv_officer_phone);

            final EditText et_phone = (EditText) addviewLi.findViewById(R.id.et_phone);
            final EditText et_imei = (EditText) addviewLi.findViewById(R.id.et_imei);
            final EditText et_officer_phone = (EditText) addviewLi.findViewById(R.id.et_officer_phone);


            EditText et_circle = (EditText) addviewLi.findViewById(R.id.et_circle);
            et_phone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {

                        //   Toast.makeText(SpecialServiceActivity.this, "LOST FOCUS", Toast.LENGTH_SHORT).show();
                        String Value = "";
                        if (selectedLIType.equalsIgnoreCase("Phone")) {
                            Value = et_phone.getText().toString();
                            if (Value.length() == 10) {
                                setSDRofRow(addviewLi, Value);
                                checkPhoneforBlacklist(addviewLi, Value);
                            } else {
                                Utility.showAlertDialog(SpecialServiceActivity.this, "Error", "Phone no should be 10 digit", false);
                            }

                        }

                    }
                }
            });
            et_imei.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        String Value = "";
                        // if(selectedLIType.equalsIgnoreCase("Imei")){
                        Value = et_imei.getText().toString();
                        if (Value.length() > 13 && Value.length() < 16) {
                           // setSDRofRow(addviewLi, Value);
                           // checkPhoneforBlacklist(addviewLi, Value);
                        } else {
                            Utility.showAlertDialog(SpecialServiceActivity.this, "Error", "Imei no should be 14 or 15 digit", false);
                        }


                    }
                }
            });


            if (selectedLIType.equalsIgnoreCase("Phone")) {
                et_phone.setVisibility(View.VISIBLE);
                tv_phone_label.setText("Phone");
                tv_phone_text.setVisibility(View.VISIBLE);
                et_imei.setVisibility(View.GONE);
                et_phone.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
            } else if (selectedLIType.equalsIgnoreCase("Imei")) {
                tv_phone_label.setText("Imei");
                tv_phone_text.setVisibility(View.GONE);
                et_phone.setVisibility(View.GONE);
                et_imei.setVisibility(View.VISIBLE);
                et_imei.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
            }

            final Spinner spLoggerNo = (Spinner) addviewLi.findViewById(R.id.sp_loggerNo);

            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, logger_arr_str);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            spLoggerNo.setAdapter(arrayAdapter);
            spLoggerNo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ((TextView) spLoggerNo.getSelectedView()).setTextColor(getResources().getColor(R.color.color_deep_blue));
                    if(spLoggerNo.getItemAtPosition(position).toString().equalsIgnoreCase("MOBILE DIVERSION")){
                        tv_officer_phone.setVisibility(View.VISIBLE);
                        et_officer_phone.setVisibility(View.VISIBLE);
                    }
                    else{
                        tv_officer_phone.setVisibility(View.GONE);
                        et_officer_phone.setVisibility(View.GONE);
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            TextView tv_delete = (TextView) addviewLi.findViewById(R.id.tv_delete_row);
            tv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LinearLayout) addviewLi.getParent()).removeView(addviewLi);
                }
            });

            ll_special_services_form_all.addView(addviewLi);

        }
    }

    public void setSDRofRow(View addview, String value) {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_LI_SDR_DETAILS_AGAINST_PHONE);
        taskManager.setSpecialServiceLISDRInfo(true, addview);
        String[] keys = {"TYPE_VALUE", "LI_TYPE", "STATE_ID"};
        String[] values = {value, selectedLIType, selectedStateId};
        taskManager.doStartTask(keys, values, true, true);
    }

    public void parseSpecialServiceLISDRinfoResult(String result, View addLI) {
        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.optString("status").equalsIgnoreCase("1")) {
                    JSONObject jObjresult = jObj.optJSONObject("result");
                    View view = addLI;
                    EditText sdrname = (EditText) view.findViewById(R.id.et_sdr_name);
                    EditText sdrAddress = (EditText) view.findViewById(R.id.et_sdr_address);
                    EditText provider = (EditText) view.findViewById(R.id.et_provider);
                    EditText circle = (EditText) view.findViewById(R.id.et_circle);
                    EditText planType = (EditText) view.findViewById(R.id.et_plan_type);
                    // String sdrnameStr="", sdrAddressStr="",providerStr="",circleStr="",planTypeStr="";
                    Log.d("DAPL", "sdrname:" + jObjresult.optString("nameString") + "sdrAddress:" + jObjresult.optString("address") + "provider:" + jObjresult.optString("provider"));

                    if (jObjresult.optString("nameString") != null) {
                        sdrname.setText(jObjresult.optString("nameString"));
                    } else {
                        sdrname.setText("");
                    }
                    if (jObjresult.optString("address") != null) {
                        sdrAddress.setText(jObjresult.optString("address"));
                    } else {
                        sdrAddress.setText("");
                    }
                    if (jObjresult.optString("provider") != null) {
                        provider.setText(jObjresult.optString("provider"));
                    } else {
                        provider.setText("");
                    }
                    if (jObjresult.optString("circle") != null) {
                        circle.setText(jObjresult.optString("circle"));
                    } else {
                        circle.setText("");
                    }
                    if (jObjresult.optString("planType") != null) {
                        planType.setText(jObjresult.optString("planType"));
                    } else {
                        planType.setText("");
                    }


                } else {

                }

            } catch (Exception e) {

            }
        }

    }

    public void checkPhoneforBlacklist(View addview, String phoneNoValue) {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_LI_PHONE_BLACKLIST);
        taskManager.checkBlackListed(true, addview);
        String[] keys = {"phone_number"};
        String[] values = {phoneNoValue};
        taskManager.doStartTask(keys, values, true, true);
    }

    public void parsecheckBlackListedReult(String result, View addLIBlackList) {
        if (result != null && !result.equals("")) {
            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.optString("status").equalsIgnoreCase("1")) {
                    View view = addLIBlackList;
                    TextView phoneBlackList = (TextView) view.findViewById(R.id.Tv_phoneNumberBlackListed);
                    phoneBlackList.setText("1");
                } else {
                    View view = addLIBlackList;
                    TextView phoneBlackList = (TextView) view.findViewById(R.id.Tv_phoneNumberBlackListed);
                    phoneBlackList.setText("0");
                }

            } catch (Exception e) {

            }
        }

    }

    public void checkRequestType() {
        if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[0])) {
            if (ll_special_services_form_all.getChildCount() > 0) {
                EditText cdrPhone;
                EditText cdrCircle;
                EditText cdrFromDate;
                EditText cdrToDate;
                boolean allfildvalidate = false;

                for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
                    View cdrView = ll_special_services_form_all.getChildAt(i);
                    cdrPhone = (EditText) cdrView.findViewById(R.id.et_phone);
                    cdrCircle = (EditText) cdrView.findViewById(R.id.et_circle);
                    cdrFromDate = (EditText) cdrView.findViewById(R.id.et_fromDate);
                    cdrToDate = (EditText) cdrView.findViewById(R.id.et_toDate);
                    if (cdrPhone.getText().toString().isEmpty()) {
                        //showMesage("Please enter a valid Username", et_userid);
                        Utility.showAlertDialog(this, " Error ", "Please enter Phone Number ", false);
                        return;
                    } else if (cdrPhone.getText().toString().length() != 10) {
                        Utility.showAlertDialog(this, " Error ", "Please enter valid Phone Number", false);
                        return;
                    } else if (cdrFromDate.getText().toString().isEmpty()) {
                        // showMesage("Please enter a valid Password", et_password);
                        Utility.showAlertDialog(this, " Error ", "Please enter from date", false);
                        return;
                    } else if (cdrToDate.getText().toString().isEmpty()) {
                        // showMesage("Please enter a valid Password", et_password);
                        Utility.showAlertDialog(this, " Error ", "Please enter to date", false);
                        return;
                    } else if (!cdrToDate.getText().toString().equalsIgnoreCase("") && !cdrFromDate.getText().toString().equalsIgnoreCase("")) {
                        boolean date_result_flag = DateUtils.dateComparison(cdrFromDate.getText().toString(), cdrToDate.getText().toString());

                        if (!date_result_flag) {
                            showAlertDialog(this, "Alert !!!", "From date can't be greater than To date", false);
                            return;
                        } else {

                            allfildvalidate = true;

                        }
                    }


                }
                JSONArray dataArray = createFormdataCDR();
                if (allfildvalidate) {
                    saveRequestData(dataArray.toString());
                }

            } else {
                Utility.showAlertDialog(this, " Error ", "Please add  more ", false);

            }

        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[1]) || selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[3])
                || selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[7]) || selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[10])) {
            if (ll_special_services_form_all.getChildCount() > 0) {
                EditText sdrPhone;
                boolean allfildvalidate = false;
                for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
                    View sdrView = ll_special_services_form_all.getChildAt(i);
                    sdrPhone = (EditText) sdrView.findViewById(R.id.et_phone);
                    if (sdrPhone.getText().toString().isEmpty()) {
                        //showMesage("Please enter a valid Username", et_userid);
                        Utility.showAlertDialog(this, " Error ", "Please enter Phone Number ", false);
                        return;
                    } else if (sdrPhone.getText().toString().length() != 10) {
                        Utility.showAlertDialog(this, " Error ", "Please enter valid Phone Number", false);
                        return;
                    } else {
                        allfildvalidate = true;

                    }

                }
                JSONArray dataArray = createFormdataSdrTowerCaf();
                if (allfildvalidate) {
                    saveRequestData(dataArray.toString());
                }
            } else {
                Utility.showAlertDialog(this, " Error ", "Please add  more ", false);

            }


        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[2])) {
            if (ll_special_services_form_all.getChildCount() > 0) {
                EditText imeiPhone;
                EditText imeiFromDate;
                EditText imeiToDate;
                boolean allfildvalidate = false;
                for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
                    View imeiView = ll_special_services_form_all.getChildAt(i);
                    imeiPhone = (EditText) imeiView.findViewById(R.id.et_imei);
                    imeiFromDate = (EditText) imeiView.findViewById(R.id.et_fromDate);
                    imeiToDate = (EditText) imeiView.findViewById(R.id.et_toDate);
                    if (imeiPhone.getText().toString().isEmpty()) {
                        //showMesage("Please enter a valid Username", et_userid);
                        Utility.showAlertDialog(this, " Error ", "Please enter IMEI Number ", false);
                        return;
                    } else if (imeiPhone.getText().toString().length() < 14 || imeiPhone.getText().toString().length() > 15) {
                        Utility.showAlertDialog(this, " Error ", "Please enter valid IMEI Number", false);
                        return;
                    } else if (imeiFromDate.getText().toString().isEmpty()) {
                        // showMesage("Please enter a valid Password", et_password);
                        Utility.showAlertDialog(this, " Error ", "Please enter from date", false);
                        return;
                    } else if (imeiToDate.getText().toString().isEmpty()) {
                        // showMesage("Please enter a valid Password", et_password);
                        Utility.showAlertDialog(this, " Error ", "Please enter to date", false);
                        return;
                    } else if (!imeiToDate.getText().toString().equalsIgnoreCase("") && !imeiFromDate.getText().toString().equalsIgnoreCase("")) {
                        boolean date_result_flag = DateUtils.dateComparison(imeiFromDate.getText().toString(), imeiToDate.getText().toString());
                        if (!date_result_flag) {
                            showAlertDialog(this, "Alert !!!", "From date can't be greater than To date", false);
                            return;
                        } else {

                            allfildvalidate = true;
                        }
                    }


                }
                JSONArray dataArray = createFormdataIMEI();
                if (allfildvalidate) {
                    saveRequestData(dataArray.toString());
                }
            } else {
                Utility.showAlertDialog(this, " Error ", "Please add  more ", false);

            }
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[4]) || selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[5])) {

            if (ll_special_services_form_all.getChildCount() > 0) {
                EditText ipdrPhone;
                EditText ipdrFromDate;
                EditText ipdrToDate;
                boolean allfildvalidate = false;


                for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
                    allfildvalidate = false;
                    View cdrView = ll_special_services_form_all.getChildAt(i);
                    ipdrPhone = (EditText) cdrView.findViewById(R.id.et_phone);
                    ipdrFromDate = (EditText) cdrView.findViewById(R.id.et_fromDate);
                    ipdrToDate = (EditText) cdrView.findViewById(R.id.et_toDate);

                    if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[5])) {

                        if (ipdrPhone.getText().toString().isEmpty()) {
                            //showMesage("Please enter a valid Username", et_userid);
                            Utility.showAlertDialog(this, " Error ", "Please enter Phone Number ", false);
                            return;

                        } else if (ipdrPhone.getText().toString().length() <= 2) {
                            Utility.showAlertDialog(this, " Error ", "Please enter valid Phone Number", false);
                            return;
                        } else if (ipdrFromDate.getText().toString().isEmpty()) {
                            // showMesage("Please enter a valid Password", et_password);
                            Utility.showAlertDialog(this, " Error ", "Please enter from date", false);
                            return;
                        } else if (ipdrToDate.getText().toString().isEmpty()) {
                            // showMesage("Please enter a valid Password", et_password);
                            Utility.showAlertDialog(this, " Error ", "Please enter to date", false);
                            return;
                        } else if (!ipdrToDate.getText().toString().equalsIgnoreCase("") && !ipdrFromDate.getText().toString().equalsIgnoreCase("")) {
                            Log.e("DAPL", "Date from:" + ipdrFromDate.getText().toString() + "for row:" + i);
                            Log.e("DAPL", "Date to:" + ipdrToDate.getText().toString() + "for row:" + i);
                            boolean date_result_flag = DateUtils.dateComparison(ipdrFromDate.getText().toString(), ipdrToDate.getText().toString());
                            Log.e("DAPL", "Date compare flag:" + date_result_flag + "for row:" + i);
                            Log.e("DAPL", "All field validate  except date for row:" + i);
                            if (!date_result_flag) {
                                showAlertDialog(this, "Alert !!!", "From date can't be greater than To date", false);
                                Log.e("DAPL", "Date validation  failed for row:" + i);
                                return;
                            } else {
                                Log.e("DAPL", "All field validate for row:" + i);
                                allfildvalidate = true;
                            }
                        }
                    } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[4])) {
                        if (ipdrPhone.getText().toString().isEmpty()) {
                            //showMesage("Please enter a valid Username", et_userid);
                            Utility.showAlertDialog(this, " Error ", "Please enter Phone Number ", false);
                            return;

                        } else if (ipdrPhone.getText().toString().length() != 10) {
                            Utility.showAlertDialog(this, " Error ", "Please enter valid Phone Number", false);
                            return;
                        } else if (ipdrFromDate.getText().toString().isEmpty()) {
                            // showMesage("Please enter a valid Password", et_password);
                            Utility.showAlertDialog(this, " Error ", "Please enter from date", false);
                            return;
                        } else if (ipdrToDate.getText().toString().isEmpty()) {
                            // showMesage("Please enter a valid Password", et_password);
                            Utility.showAlertDialog(this, " Error ", "Please enter to date", false);
                            return;
                        } else if (!ipdrToDate.getText().toString().equalsIgnoreCase("") && !ipdrFromDate.getText().toString().equalsIgnoreCase("")) {
                            Log.e("DAPL", "Date from:" + ipdrFromDate.getText().toString() + "for row:" + i);
                            Log.e("DAPL", "Date to:" + ipdrToDate.getText().toString() + "for row:" + i);
                            boolean date_result_flag = DateUtils.dateComparison(ipdrFromDate.getText().toString(), ipdrToDate.getText().toString());
                            Log.e("DAPL", "Date compare flag:" + date_result_flag + "for row:" + i);
                            Log.e("DAPL", "All field validate  except date for row:" + i);
                            if (!date_result_flag) {
                                showAlertDialog(this, "Alert !!!", "From date can't be greater than To date", false);
                                Log.e("DAPL", "Date validation  failed for row:" + i);
                                return;
                            } else {
                                Log.e("DAPL", "All field validate for row:" + i);
                                allfildvalidate = true;
                            }
                        }

                    }


                }
                JSONArray dataArray = createFormdataIPDRILD();
                if (allfildvalidate) {
                    Log.e("DAPL", "allfildvalidate:-----" + allfildvalidate);
                    saveRequestData(dataArray.toString());
                }

            } else {
                Utility.showAlertDialog(this, " Error ", "Please add  more ", false);

            }

        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[6]) || selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[12])) {
           /*  if(selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[6])){
                 if(et_callDump_Date.getText().toString().isEmpty()){
                     Utility.showAlertDialog(this, " Error ", "Please select date ", false);
                     return;
                 }
                 else if( et_callDump_From_time.getText().toString().isEmpty()){
                     Utility.showAlertDialog(this," Error ", "Please select from time ", false);
                     return;
                 }
                 else if(et_callDump_To_time.getText().toString().isEmpty()){
                     Utility.showAlertDialog(this," Error ", "Please select to time ", false);
                     return;
                 }
                else{
                     if(!DateUtils.timeComarison(et_callDump_From_time.getText().toString(),et_callDump_To_time.getText().toString())){
                         Utility.showAlertDialog(this," Error ", "From time can not be greater than to time ", false);
                     }
                     else {
                         Utility.showAlertDialog(this," Success ", "Time validated ", true);
                     }
                 }
             }*/
            if (ll_special_services_form_all.getChildCount() > 0) {
                EditText cellId;
                Spinner spProvider;
                EditText circle;
                boolean allfildvalidate = false;
                for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
                    allfildvalidate = false;
                    View callDumpCellId = ll_special_services_form_all.getChildAt(i);
                    cellId = (EditText) callDumpCellId.findViewById(R.id.et_cellid);
                    circle = (EditText) callDumpCellId.findViewById(R.id.et_circle);
                    spProvider = (Spinner) callDumpCellId.findViewById(R.id.sp_provider);

                    if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[6])) {

                        if (spProvider.getSelectedItem().toString().isEmpty()) {
                            Utility.showAlertDialog(this, " Error ", "Please select Provider ", false);
                            return;

                        } else if (cellId.getText().toString().isEmpty()) {
                            Utility.showAlertDialog(this, " Error ", "Please enter Cell ID Number", false);
                            return;
                        } else if (circle.getText().toString().isEmpty()) {
                            // showMesage("Please enter a valid Password", et_password);
                            Utility.showAlertDialog(this, " Error ", "Please enter Circle", false);
                            return;
                        } else {
                            allfildvalidate = true;
                        }
                    } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[12])) {
                        if (spProvider.getSelectedItem().toString().isEmpty()) {
                            Utility.showAlertDialog(this, " Error ", "Please select Provider ", false);
                            return;

                        }else if (circle.getText().toString().isEmpty()) {
                            // showMesage("Please enter a valid Password", et_password);
                            Utility.showAlertDialog(this, " Error ", "Please enter Circle", false);
                            return;
                        } else {
                            allfildvalidate = true;
                        }

                    }


                }
                JSONArray dataArray = createFormdatacallDumpcellId();
                if (allfildvalidate) {
                    Log.e("DAPL", "allfildvalidate:-----" + allfildvalidate);
                    Log.e("DAPL","DATAARRAY:---"+dataArray.toString());
                    saveRequestData(dataArray.toString());
                }

            } else {
                Utility.showAlertDialog(this, " Error ", "Please add  more ", false);

            }

        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[9])) {

            JSONArray dataArray = new JSONArray();
            boolean allfildvalidate = false;
            if (ll_special_services_form_all.getChildCount() > 0) {
                EditText phone1;
                EditText phone;
                EditText imei;
                EditText imei1;
                Spinner loggerNo;
                EditText officerPhone;
                EditText officerName;
                EditText subGroup;
                EditText sdrName;
                EditText sdrAddress;
                EditText provider;
                EditText circle;
                EditText planType;
                Boolean phoneNumberDuplicateFlag=false;
                Boolean imeiNumberDuplicateFlag=false;
                if(ll_special_services_form_all.getChildCount()>0){
                    if (selectedLIType.equalsIgnoreCase("Phone")) {
                        View view = ll_special_services_form_all.getChildAt(0);
                        phone1 = (EditText) view.findViewById(R.id.et_phone);
                        String phoneNumber1=phone1.getText().toString();
                        for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
                            View view2 = ll_special_services_form_all.getChildAt(i);
                            phone = (EditText) view2.findViewById(R.id.et_phone);
                            String phoneNumbernext=phone.getText().toString();
                            if(i!=0 && phoneNumber1.equalsIgnoreCase(phoneNumbernext)){
                                phoneNumberDuplicateFlag=true;
                                break;
                            }
                        }

                    } else if (selectedLIType.equalsIgnoreCase("Imei")) {
                        View view = ll_special_services_form_all.getChildAt(0);
                        imei1 = (EditText) view.findViewById(R.id.et_imei);
                        String imeiNumber1=imei1.getText().toString();
                        for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
                            View view2 = ll_special_services_form_all.getChildAt(i);
                            imei = (EditText) view2.findViewById(R.id.et_imei);
                            String imeiNumbernext=imei.getText().toString();
                            if(i!=0 && imeiNumber1.equalsIgnoreCase(imeiNumbernext)){
                                imeiNumberDuplicateFlag=true;
                                break;
                            }
                        }
                    }


                }
                if(phoneNumberDuplicateFlag){
                    Utility.showAlertDialog(this,Constants.ERROR,"Duplicate Phone Number not allowed ",false);
                }
                else if(imeiNumberDuplicateFlag){
                    Utility.showAlertDialog(this,Constants.ERROR,"Duplicate Imei Number not allowed ",false);
                }
                else {

                    for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
                        View view = ll_special_services_form_all.getChildAt(i);
                        phone = (EditText) view.findViewById(R.id.et_phone);
                        imei = (EditText) view.findViewById(R.id.et_imei);
                        loggerNo = (Spinner) view.findViewById(R.id.sp_loggerNo);
                        officerPhone = (EditText) view.findViewById(R.id.et_officer_phone);
                        officerName = (EditText) view.findViewById(R.id.et_officer_name);
                        subGroup = (EditText) view.findViewById(R.id.et_subGroup);
                        sdrName = (EditText) view.findViewById(R.id.et_sdr_name);
                        sdrAddress = (EditText) view.findViewById(R.id.et_sdr_address);
                        provider = (EditText) view.findViewById(R.id.et_provider);
                        circle = (EditText) view.findViewById(R.id.et_circle);
                        planType = (EditText) view.findViewById(R.id.et_plan_type);

                        if (selectedLIType.equalsIgnoreCase("Phone") && phone.getText().toString().isEmpty()) {

                            Utility.showAlertDialog(this, " Error ", "Please enter Phone Number ", false);
                            return;

                        } else if (selectedLIType.equalsIgnoreCase("Imei") && imei.getText().toString().isEmpty()) {
                            Utility.showAlertDialog(this, " Error ", "Please enter Imei Number ", false);
                            return;

                        } else if (loggerNo.getSelectedItem().toString().isEmpty()) {
                            Utility.showAlertDialog(this, " Error ", "Please enter Logger Number ", false);
                            return;
                        }/* else if (officerPhone.getText().toString().isEmpty()) {

                        Utility.showAlertDialog(this, " Error ", "Please enter officer phone number ", false);
                        return;

                    }*/ else if (officerName.getText().toString().isEmpty()) {

                            Utility.showAlertDialog(this, " Error ", "Please enter officer name ", false);
                            return;

                        } else if (officerName.getText().toString().isEmpty()) {

                            Utility.showAlertDialog(this, " Error ", "Please enter officer name ", false);
                            return;

                        } else if (subGroup.getText().toString().isEmpty()) {

                            Utility.showAlertDialog(this, " Error ", "Please enter SubGroup name ", false);
                            return;

                        } else if (sdrName.getText().toString().isEmpty()) {

                            Utility.showAlertDialog(this, " Error ", "Please enter SDR name ", false);
                            return;

                        } else if (sdrAddress.getText().toString().isEmpty()) {

                            Utility.showAlertDialog(this, " Error ", "Please enter SDR address ", false);
                            return;

                        } else if (provider.getText().toString().isEmpty()) {

                            Utility.showAlertDialog(this, " Error ", "Please enter provider name ", false);
                            return;

                        } else if (circle.getText().toString().isEmpty()) {

                            Utility.showAlertDialog(this, " Error ", "Please enter circle ", false);
                            return;

                        } else if (planType.getText().toString().isEmpty()) {

                            Utility.showAlertDialog(this, " Error ", "Please enter plan type ", false);
                            return;

                        } else {

                            allfildvalidate = true;
                        }


                    }

                    dataArray = createFormdataLI();
                }

            } else {
                Utility.showAlertDialog(this, " Error ", "Please add  more ", false);
            }
            if (allfildvalidate) {
                if (autoCompleteTextViewGroup.getText().toString().isEmpty()) {

                    Utility.showAlertDialog(this, " Error ", "Please enter Group ", false);
                    return;

                } else if (et_listener.getText().toString().isEmpty()) {

                    Utility.showAlertDialog(this, " Error ", "Please enter Listener ", false);
                    return;

                } else if (et_user.getText().toString().isEmpty()) {

                    Utility.showAlertDialog(this, " Error ", "Please enter User ", false);
                    return;

                } else if (spjustTitle.getSelectedItem().toString().equalsIgnoreCase("Select Justiication Type")) {
                    Utility.showAlertDialog(this, " Error ", "Please select Justification type ", false);
                    return;

                } else if (et_justification.getText().toString().isEmpty()) {

                    Utility.showAlertDialog(this, " Error ", "Please enter Justification ", false);
                    return;

                } else {
                    saveRequestData(dataArray.toString());
                }


            }

        }
        else if(selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[11])){
            if (ll_special_services_form_all.getChildCount() > 0) {
                EditText rdPhone;
                EditText cdrCircle;
                EditText rdFromDate;
                EditText rdToDate;
                boolean allfildvalidate = false;

                for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
                    View cdrView = ll_special_services_form_all.getChildAt(i);
                    rdPhone = (EditText) cdrView.findViewById(R.id.et_phone);
                    //cdrCircle = (EditText) cdrView.findViewById(R.id.et_circle);
                    rdFromDate = (EditText) cdrView.findViewById(R.id.et_fromDate);
                    rdToDate = (EditText) cdrView.findViewById(R.id.et_toDate);
                    if (rdPhone.getText().toString().isEmpty()) {
                        //showMesage("Please enter a valid Username", et_userid);
                        Utility.showAlertDialog(this, " Error ", "Please enter Phone Number ", false);
                        return;
                    } else if (rdPhone.getText().toString().length() != 10) {
                        Utility.showAlertDialog(this, " Error ", "Please enter valid Phone Number", false);
                        return;
                    } else if (rdFromDate.getText().toString().isEmpty()) {
                        // showMesage("Please enter a valid Password", et_password);
                        Utility.showAlertDialog(this, " Error ", "Please enter from date", false);
                        return;
                    } else if (rdToDate.getText().toString().isEmpty()) {
                        // showMesage("Please enter a valid Password", et_password);
                        Utility.showAlertDialog(this, " Error ", "Please enter to date", false);
                        return;
                    } else if (!rdToDate.getText().toString().equalsIgnoreCase("") && !rdFromDate.getText().toString().equalsIgnoreCase("")) {
                        boolean date_result_flag = DateUtils.dateComparison(rdFromDate.getText().toString(), rdToDate.getText().toString());

                        if (!date_result_flag) {
                            showAlertDialog(this, "Alert !!!", "From date can't be greater than To date", false);
                            return;
                        } else {

                            allfildvalidate = true;

                        }
                    }


                }
                JSONArray dataArray = createFormdataRD();
                if (allfildvalidate) {
                    saveRequestData(dataArray.toString());
                }

            } else {
                Utility.showAlertDialog(this, " Error ", "Please add  more ", false);

            }

        }

    }

    public void CdrcallforProvider(View cdrView, String value) {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_LI_SDR_DETAILS_AGAINST_PHONE);
        taskManager.setSpecialServiceAllSDRinfo(true, cdrView);
        String[] keys = {"TYPE_VALUE", "LI_TYPE"};
        String[] values = {value, selectedType};
        taskManager.doStartTask(keys, values, true, true);

    }

    public void parseSpecialServiceAllSDRinfoResult(String result, View requestView) {
        if (result != null && !result.equals("")) {
            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.optString("status").equalsIgnoreCase("1")) {
                    JSONObject jObjresult = jObj.optJSONObject("result");
                    View view = requestView;

                    EditText provider = (EditText) view.findViewById(R.id.et_phone_provider);

                    // String sdrnameStr="", sdrAddressStr="",providerStr="",circleStr="",planTypeStr="";
                    Log.d("KP", "sdrname:" + jObjresult.optString("nameString") + "sdrAddress:" + jObjresult.optString("address") + "provider:" + jObjresult.optString("provider"));


                    if (jObjresult.optString("provider") != null) {
                        provider.setText(jObjresult.optString("provider"));
                    } else {
                        provider.setText("");
                    }


                }

            } catch (Exception e) {

            }
        }

    }

    public JSONArray createFormdataCDR() {
        JSONArray jsonArraycdr = new JSONArray();

        EditText cdrPhone;
        EditText cdrCircle;
        EditText cdrFromDate;
        EditText cdrToDate;
        EditText provider;

        for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
            final View cdrView = ll_special_services_form_all.getChildAt(i);
            cdrPhone = (EditText) cdrView.findViewById(R.id.et_phone);
            provider = (EditText) cdrView.findViewById(R.id.et_phone_provider);
            cdrCircle = (EditText) cdrView.findViewById(R.id.et_circle);
            cdrFromDate = (EditText) cdrView.findViewById(R.id.et_fromDate);
            cdrToDate = (EditText) cdrView.findViewById(R.id.et_toDate);
            try {
                JSONObject cdrObj = new JSONObject();
                cdrObj.put("phone", cdrPhone.getText().toString());
                cdrObj.put("provider", provider.getText().toString());
                cdrObj.put("circle", cdrCircle.getText().toString());
                cdrObj.put("fromdate", cdrFromDate.getText().toString());
                cdrObj.put("todate", cdrToDate.getText().toString());
                jsonArraycdr.put(cdrObj);
            } catch (JSONException e) {

            }


        }


        return jsonArraycdr;

    }public JSONArray createFormdataRD() {
        JSONArray jsonArraycdr = new JSONArray();

        EditText rdPhone;
        EditText rdFromDate;
        EditText rdToDate;
        for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
            final View cdrView = ll_special_services_form_all.getChildAt(i);
            rdPhone = (EditText) cdrView.findViewById(R.id.et_phone);
            rdFromDate = (EditText) cdrView.findViewById(R.id.et_fromDate);
            rdToDate = (EditText) cdrView.findViewById(R.id.et_toDate);
            try {
                JSONObject cdrObj = new JSONObject();
                cdrObj.put("phone", rdPhone.getText().toString());
                cdrObj.put("fromdate", rdFromDate.getText().toString());
                cdrObj.put("todate", rdToDate.getText().toString());
                jsonArraycdr.put(cdrObj);
            } catch (JSONException e) {

            }


        }


        return jsonArraycdr;

    }


    public JSONArray createFormdataIMEI() {
        JSONArray jsonArrayimei = new JSONArray();

        EditText imeiPhone;
        EditText imeiCircle;
        EditText imeiFromDate;
        EditText imeiToDate;

        for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
            View cdrView = ll_special_services_form_all.getChildAt(i);
            imeiPhone = (EditText) cdrView.findViewById(R.id.et_imei);
            EditText provider = (EditText) cdrView.findViewById(R.id.et_phone_provider);
            imeiCircle = (EditText) cdrView.findViewById(R.id.et_circle);
            imeiFromDate = (EditText) cdrView.findViewById(R.id.et_fromDate);
            imeiToDate = (EditText) cdrView.findViewById(R.id.et_toDate);
            try {
                JSONObject cdrObj = new JSONObject();
                cdrObj.put("imei", imeiPhone.getText().toString());
                cdrObj.put("provider", provider.getText().toString());
                cdrObj.put("circle", imeiCircle.getText().toString());
                cdrObj.put("fromdate", imeiFromDate.getText().toString());
                cdrObj.put("todate", imeiToDate.getText().toString());
                jsonArrayimei.put(cdrObj);
            } catch (JSONException e) {

            }


        }


        return jsonArrayimei;

    }


    public JSONArray createFormdataSdrTowerCaf() {
        JSONArray jsonArraysdr = new JSONArray();
        EditText sdrPhone;
        for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
            View cdrView = ll_special_services_form_all.getChildAt(i);
            sdrPhone = (EditText) cdrView.findViewById(R.id.et_phone);
            EditText provider = (EditText) cdrView.findViewById(R.id.et_phone_provider);
            try {
                JSONObject cdrObj = new JSONObject();
                cdrObj.put("phone", sdrPhone.getText().toString());
                cdrObj.put("provider", provider.getText().toString());
                jsonArraysdr.put(cdrObj);
            } catch (JSONException e) {

            }


        }
        return jsonArraysdr;

    }

    public JSONArray createFormdataIPDRILD() {
        JSONArray jsonArrayipdr = new JSONArray();

        EditText ipdrPhone;
        EditText ipdrFromDate;
        EditText ipdrToDate;


        for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
            View cdrView = ll_special_services_form_all.getChildAt(i);
            ipdrPhone = (EditText) cdrView.findViewById(R.id.et_phone);
            EditText provider = (EditText) cdrView.findViewById(R.id.et_phone_provider);
            ipdrFromDate = (EditText) cdrView.findViewById(R.id.et_fromDate);
            ipdrToDate = (EditText) cdrView.findViewById(R.id.et_toDate);
            try {
                JSONObject cdrObj = new JSONObject();
                cdrObj.put("phone", ipdrPhone.getText().toString());
                cdrObj.put("provider", provider.getText().toString());
                cdrObj.put("fromdate", ipdrFromDate.getText().toString());
                cdrObj.put("todate", ipdrToDate.getText().toString());
                jsonArrayipdr.put(cdrObj);
            } catch (JSONException e) {

            }


        }


        return jsonArrayipdr;

    }

    public JSONArray createFormdatacallDumpcellId() {
        JSONArray jsonArrayCallDumpCellId = new JSONArray();
        EditText cellId;
        Spinner spProvider;
        EditText circle;

        for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
            View view = ll_special_services_form_all.getChildAt(i);
            cellId = (EditText) view.findViewById(R.id.et_cellid);
            spProvider = (Spinner) view.findViewById(R.id.sp_provider);
            circle = (EditText) view.findViewById(R.id.et_circle);
            try {
                JSONObject CallDumpCellIdObj = new JSONObject();
                CallDumpCellIdObj.put("provider", spProvider.getSelectedItem().toString());
                CallDumpCellIdObj.put("cell_id", cellId.getText().toString());
                CallDumpCellIdObj.put("circle",circle.getText().toString());
                jsonArrayCallDumpCellId.put(CallDumpCellIdObj);
            } catch (JSONException e) {

            }


        }

        return jsonArrayCallDumpCellId;

    }


    public JSONArray createFormdataLI() {
        JSONArray jsonArrayLI = new JSONArray();
        EditText phone1;
        EditText phone;
        TextView blackList;
        EditText imei;
        Spinner loggerNo;
        EditText officerPhone;
        EditText officerName;
        EditText subGroup;
        EditText sdrName;
        EditText sdrAddress;
        EditText provider;
        EditText circle;
        EditText planType;



            for (int i = 0; i < ll_special_services_form_all.getChildCount(); i++) {
                View view = ll_special_services_form_all.getChildAt(i);
                phone = (EditText) view.findViewById(R.id.et_phone);
                blackList = (TextView) view.findViewById(R.id.Tv_phoneNumberBlackListed);
                imei = (EditText) view.findViewById(R.id.et_imei);
                loggerNo = (Spinner) view.findViewById(R.id.sp_loggerNo);
                officerPhone = (EditText) view.findViewById(R.id.et_officer_phone);
                officerName = (EditText) view.findViewById(R.id.et_officer_name);
                subGroup = (EditText) view.findViewById(R.id.et_subGroup);
                sdrName = (EditText) view.findViewById(R.id.et_sdr_name);
                sdrAddress = (EditText) view.findViewById(R.id.et_sdr_address);
                provider = (EditText) view.findViewById(R.id.et_provider);
                circle = (EditText) view.findViewById(R.id.et_circle);
                planType = (EditText) view.findViewById(R.id.et_plan_type);
                try {
                    JSONObject LiObj = new JSONObject();
                    if (selectedLIType.equalsIgnoreCase("Phone")) {
                        LiObj.put("phone", phone.getText().toString());
                        LiObj.put("is_blacklist", blackList.getText().toString());
                    } else if (selectedLIType.equalsIgnoreCase("Imei")) {
                        LiObj.put("imei", imei.getText().toString());
                    }
                    LiObj.put("loggerNo", loggerNo.getSelectedItem().toString());
                    LiObj.put("officerNo", officerPhone.getText().toString());
                    LiObj.put("officerName", officerName.getText().toString());
                    LiObj.put("subGroup", subGroup.getText().toString());
                    LiObj.put("sdrName", sdrName.getText().toString());
                    LiObj.put("sdrAddress", sdrAddress.getText().toString());
                    LiObj.put("provider", provider.getText().toString());
                    LiObj.put("circle", circle.getText().toString());
                    LiObj.put("planType", planType.getText().toString());
                    jsonArrayLI.put(LiObj);
                } catch (JSONException e) {

                }


            }



        return jsonArrayLI;

    }

    public void saveRequestData(String dataArray) {
        String justTitle = "", draft_save = "", complete_save = "";
        //Toast.makeText(this, "Draft_complete_flag"+draft_complete_flag, Toast.LENGTH_SHORT).show();
        if (draft_complete_flag.equalsIgnoreCase("Draft")) {
            draft_save = "1";
            complete_save = "0";
        } else if (draft_complete_flag.equalsIgnoreCase("Complete")) {
            draft_save = "0";
            complete_save = "1";
        }
        if (selectedrequestTypeValue.equalsIgnoreCase("10")) {
            justTitle = spjustTitle.getSelectedItem().toString();
        }
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_SPECIAL_SEARCH);
        taskManager.setSpecilService(true);
       /* try {
            devicetoken = GCMRegistrar.getRegistrationId(this);
        } catch (Exception e) {

        }*/
        String reference = "";
        if (et_holder_refer.getText().toString().equalsIgnoreCase("Case")) {

            if( partialsavedLIform ){
                Toast.makeText(this, "PS NAME"+ps_name, Toast.LENGTH_SHORT).show();
                reference=partialSavedData.getResult().get(0).getCaseRef();
                if(ps_name.equalsIgnoreCase("")) {
                    ps_name = partialSavedData.getResult().get(0).getRequestPSName();
                }
            }
            else {
                reference = policeStationString + "_" + et_reference_year.getText().toString() + "_" + et_reference_caseNo.getText().toString();
            }

        } else if (et_holder_refer.getText().toString().equalsIgnoreCase("Enquiry")) {
            reference = et_reference_enquiry.getText().toString();
        }
        String[] keys = {"record_id","request_head", "requesting_officer", "request_reference", "user_name", "user_id", "user_email", "user_phone", "user_ps","ps_name", "state","user_div_code", "user_rank", "user_device_imei", "form_data","urgent_general",
                "only_central_mcell", "oc_approval", "ac_approval", "dc_approval", "jtcp_approval", "nodal_approval", "designated_officer_approval",
                "oc_name", "ac_name", "dc_name", "jtcp_name", "nodal_name", "designated_officer_name", "mail_to_oc", "mail_to_ac", "mail_to_dc", "mail_to_jtcp", "mail_to_nodal", "mail_to_designated_officer",
                "oc_mail_add", "ac_mail_add", "dc_mail_add", "jtcp_mail_add", "nodal_mail_add", "designated_mail_add", "liType", "group", "listener", "user", "just_title", "just_subject", "draft_save", "complete_save", "fcmID","li_circle","briefDataSuperiors"};
        String[] values = {partialSavedLIRequestMasterID,selectedrequestTypeValue, userName, reference, userName, Utility.getUserInfo(this).getUserId(), Utility.getUserInfo(this).getUserEmail(), Utility.getUserInfo(this).getUserPhoneNo(), Utility.getUserInfo(this).getUserPscode()
                ,ps_name,state, Utility.getUserInfo(this).getUserDivisionCode(), Utility.getUserInfo(this).getUserRank(), Utility.getImiNO(this), dataArray,urgent_general_flag,
                Utility.getSpecialServices(this).getRequestThruMcell(), Utility.getSpecialServices(this).getOcApproval(), Utility.getSpecialServices(this).getAcApproval(), Utility.getSpecialServices(this).getDcApproval(), Utility.getSpecialServices(this).getJtcpApproval(),
                Utility.getSpecialServices(this).getNodalApproval(), Utility.getSpecialServices(this).getDesignatedOfficerApproval(),
                Utility.getSpecialServices(this).getOcName(), Utility.getSpecialServices(this).getAcName(), Utility.getSpecialServices(this).getDcName(), Utility.getSpecialServices(this).getJtcpName(), Utility.getSpecialServices(this).getNodalName(),
                Utility.getSpecialServices(this).getDesignatedOfficerName(),
                Utility.getSpecialServices(this).getOcApprovalMail(), Utility.getSpecialServices(this).getAcApprovalMail(), Utility.getSpecialServices(this).getDcApprovalMail(), Utility.getSpecialServices(this).getJtcpApprovalMail(),
                Utility.getSpecialServices(this).getNodalOfficerApprovalMail(), Utility.getSpecialServices(this).getDesignatedOfficerApprovalMail(),
                Utility.getSpecialServices(this).getOcMailAddress(), Utility.getSpecialServices(this).getAcMailAddress(), Utility.getSpecialServices(this).getDcMailAddress(), Utility.getSpecialServices(this).getJtcpMailAddress(), Utility.getSpecialServices(this).getNodalOfficeMailAddress(),
                Utility.getSpecialServices(this).getDesignatedOfficerMailAddress(),
                selectedLIType,
                autoCompleteTextViewGroup.getText().toString(),
                et_listener.getText().toString(),
                et_user.getText().toString(),
                justTitle,
                et_justification.getText().toString(), draft_save, complete_save, fcmDeviceId,panIndiaFlag,et_enquiry_details.getText().toString().trim()};
        taskManager.doStartTask(keys, values, true, true);

    }


    public void parseSpecialServiceSavedResult(String result) {
        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    showAlertDialog(this, Constants.SUCCESS, jObj.optString("message"), true);

                } else {
                    Utility.showAlertDialog(this, Constants.ERROR, jObj.optString("message"), false);
                }


            } catch (JSONException e) {

                e.printStackTrace();
                //Utility.showToast(getActivity(), "Some error has been encountered", "long");
                showAlertDialog(this, " Search Error ", "Some error has been encountered", false);
            }
        }

    }

    private void showRequestTypedialog() {

        if (requestArrayList.size() > 0) {
            final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this, R.style.myDialog);
            builder.setTitle("Select Request Type")
                    .setItems(requestArray, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // The 'which' argument contains the index position
                            // of the selected item

                            et_request_type.setText(requestArrayList.get(which));
                            selectedrequestType = requestArray[which];
                          /*  if(et_request_type.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[9])){
                                textInputLayoutEnquirydetails.setVisibility(View.VISIBLE);
                            }
                            else {
                                textInputLayoutEnquirydetails.setVisibility(View.GONE);
                            }*/

                            setselectedrequestTypeValue();
                            if (ll_special_services_form_all.getChildCount() > 0) {
                                ll_special_services_form_all.removeAllViews();
                                rl_special_service_form.setVisibility(View.GONE);
                            }
                            checkingRequestPermission(); //
                            //    changeRequestFormLayout();

                        }
                    });

            android.support.v7.app.AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    public void checkingRequestPermission() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_SPECIAL_SERVICE_REQUEST_APPROVAL);
        taskManager.setSpecialServiceRequestPermission(true);
        String[] keys = {"user_id", "request_type"};
        if(selectedrequestType.equals("Recharge Details")) {
            String[] values = {userId, "RD"};
            taskManager.doStartTask(keys, values, true, true);
        }
        else if(selectedrequestType.equals("CELL-ID CHART")){
            String[] values = {userId, "CIDCHART"};
            taskManager.doStartTask(keys, values, true, true);
        }
        else
        {
            String[] values = {userId, selectedrequestType};
            taskManager.doStartTask(keys, values, true, true);

        }
      //  taskManager.doStartTask(keys, values, true, true);
    }

    public void parseSpecialServiceRequestResponse(String response) {
        if (response != null && !response.equals("")) {

            try {

                JSONObject jObj = new JSONObject(response);

                if (jObj != null && jObj.optString("status").equalsIgnoreCase("1")) {
                    // According to the selection of drop down new request form layout will be shown

                    JSONObject jsonObj = jObj.optJSONObject("result");
                    SpecialServicesModel specialServicesModel = new SpecialServicesModel();
                    if (jsonObj.optString("OC_APPROVAL") != null && !jsonObj.optString("OC_APPROVAL").equalsIgnoreCase("") && !jsonObj.optString("OC_APPROVAL").equalsIgnoreCase("null"))
                        specialServicesModel.setOcApproval(jsonObj.optString("OC_APPROVAL"));
                    else
                        specialServicesModel.setOcApproval("");
                    if (jsonObj.optString("AC_APPROVAL") != null && !jsonObj.optString("AC_APPROVAL").equalsIgnoreCase("") && !jsonObj.optString("AC_APPROVAL").equalsIgnoreCase("null"))
                        specialServicesModel.setAcApproval(jsonObj.optString("AC_APPROVAL"));
                    else
                        specialServicesModel.setAcApproval("");
                    if (jsonObj.optString("DC_APPROVAL") != null && !jsonObj.optString("DC_APPROVAL").equalsIgnoreCase("") && !jsonObj.optString("DC_APPROVAL").equalsIgnoreCase("null"))
                        specialServicesModel.setDcApproval(jsonObj.optString("DC_APPROVAL"));
                    else
                        specialServicesModel.setDcApproval("");
                    if (jsonObj.optString("JT_CP_APPROVAL") != null && !jsonObj.optString("JT_CP_APPROVAL").equalsIgnoreCase("") && !jsonObj.optString("JT_CP_APPROVAL").equalsIgnoreCase("null"))
                        specialServicesModel.setJtcpApproval(jsonObj.optString("JT_CP_APPROVAL"));
                    else
                        specialServicesModel.setJtcpApproval("");
                    if (jsonObj.optString("NODAL_APPROVAL") != null && !jsonObj.optString("NODAL_APPROVAL").equalsIgnoreCase("") && !jsonObj.optString("NODAL_APPROVAL").equalsIgnoreCase("null"))
                        specialServicesModel.setNodalApproval(jsonObj.optString("NODAL_APPROVAL"));
                    else
                        specialServicesModel.setNodalApproval("");
                    if (jsonObj.optString("DESIG_OFFC_APPV") != null && !jsonObj.optString("DESIG_OFFC_APPV").equalsIgnoreCase("") && !jsonObj.optString("DESIG_OFFC_APPV").equalsIgnoreCase("null"))
                        specialServicesModel.setDesignatedOfficerApproval(jsonObj.optString("DESIG_OFFC_APPV"));
                    else
                        specialServicesModel.setDesignatedOfficerApproval("");

                    if (jsonObj.optString("OC_APPROVAL_MAIL") != null && !jsonObj.optString("OC_APPROVAL_MAIL").equalsIgnoreCase("") && !jsonObj.optString("OC_APPROVAL_MAIL").equalsIgnoreCase("null"))
                        specialServicesModel.setOcApprovalMail(jsonObj.optString("OC_APPROVAL_MAIL"));
                    else
                        specialServicesModel.setOcApprovalMail("");
                    if (jsonObj.optString("AC_APPROVAL_MAIL") != null && !jsonObj.optString("AC_APPROVAL_MAIL").equalsIgnoreCase("") && !jsonObj.optString("AC_APPROVAL_MAIL").equalsIgnoreCase("null"))
                        specialServicesModel.setAcApprovalMail(jsonObj.optString("AC_APPROVAL_MAIL"));
                    else
                        specialServicesModel.setAcApprovalMail("");

                    if (jsonObj.optString("DC_APPROVAL_MAIL") != null && !jsonObj.optString("DC_APPROVAL_MAIL").equalsIgnoreCase("") && !jsonObj.optString("DC_APPROVAL_MAIL").equalsIgnoreCase("null"))
                        specialServicesModel.setDcApprovalMail(jsonObj.optString("DC_APPROVAL_MAIL"));
                    else
                        specialServicesModel.setDcApprovalMail("");
                    if (jsonObj.optString("JT_CP_APPROVAL_MAIL") != null && !jsonObj.optString("JT_CP_APPROVAL_MAIL").equalsIgnoreCase("") && !jsonObj.optString("JT_CP_APPROVAL_MAIL").equalsIgnoreCase("null"))
                        specialServicesModel.setJtcpApprovalMail(jsonObj.optString("JT_CP_APPROVAL_MAIL"));
                    else
                        specialServicesModel.setJtcpApprovalMail("");
                    if (jsonObj.optString("NODAL_OFFICER_APPROVAL_MAIL") != null && !jsonObj.optString("NODAL_OFFICER_APPROVAL_MAIL").equalsIgnoreCase("") && !jsonObj.optString("NODAL_OFFICER_APPROVAL_MAIL").equalsIgnoreCase("null"))
                        specialServicesModel.setNodalOfficerApprovalMail(jsonObj.optString("NODAL_OFFICER_APPROVAL_MAIL"));
                    else
                        specialServicesModel.setNodalOfficerApprovalMail("");
                    if (jsonObj.optString("DESIG_OFFC_APPV_MAIL_FLG") != null && !jsonObj.optString("DESIG_OFFC_APPV_MAIL_FLG").equalsIgnoreCase("") && !jsonObj.optString("DESIG_OFFC_APPV_MAIL_FLG").equalsIgnoreCase("null"))
                        specialServicesModel.setDesignatedOfficerApprovalMail(jsonObj.optString("DESIG_OFFC_APPV_MAIL_FLG"));
                    else
                        specialServicesModel.setDesignatedOfficerApprovalMail("");

                    if (jsonObj.optString("OC_MAIL_ADDRESS") != null && !jsonObj.optString("OC_MAIL_ADDRESS").equalsIgnoreCase("") && !jsonObj.optString("OC_MAIL_ADDRESS").equalsIgnoreCase("null"))
                        specialServicesModel.setOcMailAddress(jsonObj.optString("OC_MAIL_ADDRESS"));
                    else
                        specialServicesModel.setOcMailAddress("");
                    if (jsonObj.optString("AC_MAIL_ADDRESS") != null && !jsonObj.optString("AC_MAIL_ADDRESS").equalsIgnoreCase("") && !jsonObj.optString("AC_MAIL_ADDRESS").equalsIgnoreCase("null"))
                        specialServicesModel.setAcMailAddress(jsonObj.optString("AC_MAIL_ADDRESS"));
                    else
                        specialServicesModel.setOcMailAddress("");

                    if (jsonObj.optString("DC_MAIL_ADDRESS") != null && !jsonObj.optString("DC_MAIL_ADDRESS").equalsIgnoreCase("") && !jsonObj.optString("DC_MAIL_ADDRESS").equalsIgnoreCase("null"))
                        specialServicesModel.setDcMailAddress(jsonObj.optString("DC_MAIL_ADDRESS"));
                    else
                        specialServicesModel.setDcMailAddress("");
                    if (jsonObj.optString("JTCP_MAIL_ADDRESS") != null && !jsonObj.optString("JTCP_MAIL_ADDRESS").equalsIgnoreCase("") && !jsonObj.optString("JTCP_MAIL_ADDRESS").equalsIgnoreCase("null"))
                        specialServicesModel.setJtcpMailAddress(jsonObj.optString("JTCP_MAIL_ADDRESS"));
                    else
                        specialServicesModel.setJtcpMailAddress("");
                    if (jsonObj.optString("NODAL_OFFICE_MAIL_ADDRESS") != null && !jsonObj.optString("NODAL_OFFICE_MAIL_ADDRESS").equalsIgnoreCase("") && !jsonObj.optString("NODAL_OFFICE_MAIL_ADDRESS").equalsIgnoreCase("null"))
                        specialServicesModel.setNodalOfficeMailAddress(jsonObj.optString("NODAL_OFFICE_MAIL_ADDRESS"));
                    else
                        specialServicesModel.setNodalOfficeMailAddress("");
                    if (jsonObj.optString("DESIGNATED_OFFICER_MAIL_ADDRESS") != null && !jsonObj.optString("DESIGNATED_OFFICER_MAIL_ADDRESS").equalsIgnoreCase("") && !jsonObj.optString("DESIGNATED_OFFICER_MAIL_ADDRESS").equalsIgnoreCase("null"))
                        specialServicesModel.setDesignatedOfficerMailAddress(jsonObj.optString("DESIGNATED_OFFICER_MAIL_ADDRESS"));
                    else
                        specialServicesModel.setDesignatedOfficerMailAddress("");

                    if (jsonObj.optString("OC_NAME") != null && !jsonObj.optString("OC_NAME").equalsIgnoreCase("") && !jsonObj.optString("OC_NAME").equalsIgnoreCase("null"))
                        specialServicesModel.setOcName(jsonObj.optString("OC_NAME"));
                    else
                        specialServicesModel.setOcName("");
                    if (jsonObj.optString("AC_NAME") != null && !jsonObj.optString("AC_NAME").equalsIgnoreCase("") && !jsonObj.optString("AC_NAME").equalsIgnoreCase("null"))
                        specialServicesModel.setAcName(jsonObj.optString("AC_NAME"));
                    else
                        specialServicesModel.setAcName("");
                    if (jsonObj.optString("DC_NAME") != null && !jsonObj.optString("DC_NAME").equalsIgnoreCase("") && !jsonObj.optString("DC_NAME").equalsIgnoreCase("null"))
                        specialServicesModel.setDcName(jsonObj.optString("DC_NAME"));
                    else
                        specialServicesModel.setDcName("");
                    if (jsonObj.optString("JT_CP_NAME") != null && !jsonObj.optString("JT_CP_NAME").equalsIgnoreCase("") && !jsonObj.optString("JT_CP_NAME").equalsIgnoreCase("null"))
                        specialServicesModel.setJtcpName(jsonObj.optString("JT_CP_NAME"));
                    else
                        specialServicesModel.setJtcpName("");
                    if (jsonObj.optString("NODAL_APPROVAL_NAME") != null && !jsonObj.optString("NODAL_APPROVAL_NAME").equalsIgnoreCase("") && !jsonObj.optString("NODAL_APPROVAL_NAME").equalsIgnoreCase("null"))
                        specialServicesModel.setJtcpName(jsonObj.optString("NODAL_APPROVAL_NAME"));
                    else
                        specialServicesModel.setJtcpName("");
                    if (jsonObj.optString("NODAL_APPROVAL_NAME") != null && !jsonObj.optString("NODAL_APPROVAL_NAME").equalsIgnoreCase("") && !jsonObj.optString("NODAL_APPROVAL_NAME").equalsIgnoreCase("null"))
                        specialServicesModel.setNodalName(jsonObj.optString("NODAL_APPROVAL_NAME"));
                    else
                        specialServicesModel.setNodalName("");
                    if (jsonObj.optString("DESIGNATED_OFFICER_NAME") != null && !jsonObj.optString("DESIGNATED_OFFICER_NAME").equalsIgnoreCase("") && !jsonObj.optString("DESIGNATED_OFFICER_NAME").equalsIgnoreCase("null"))
                        specialServicesModel.setDesignatedOfficerName(jsonObj.optString("DESIGNATED_OFFICER_NAME"));
                    else
                        specialServicesModel.setDesignatedOfficerName("");

                    if (jsonObj.optString("ONLY_CENTRAL_M_CELL") != null && !jsonObj.optString("ONLY_CENTRAL_M_CELL").equalsIgnoreCase("") && !jsonObj.optString("ONLY_CENTRAL_M_CELL").equalsIgnoreCase("null"))
                        specialServicesModel.setRequestThruMcell(jsonObj.optString("ONLY_CENTRAL_M_CELL"));
                    else
                        specialServicesModel.setRequestThruMcell("");

                    if (jsonObj.optString("IS_OC") != null && !jsonObj.optString("IS_OC").equalsIgnoreCase("") && !jsonObj.optString("IS_OC").equalsIgnoreCase("null"))
                        specialServicesModel.setIS_OC(jsonObj.optString("IS_OC"));
                    else
                        specialServicesModel.setIS_OC("");

                    if (jsonObj.optString("IS_DC") != null && !jsonObj.optString("IS_DC").equalsIgnoreCase("") && !jsonObj.optString("IS_DC").equalsIgnoreCase("null"))
                        specialServicesModel.setIS_DC(jsonObj.optString("IS_DC"));
                    else
                        specialServicesModel.setIS_DC("");


                   // IS_OC":"0","IS_DC":"0","IS_AC":"0","IS_JTCP":"0","IS_ADCP":"1","IS_SPLCP":"0","IS_CP":"0"

                    Utility.setSpecialServices(this, specialServicesModel);//setting the value in shared preference .
                    if (selectedrequestType.equals("LI")) {
                        fetchloggerinfo();
                    } else {
                        changeRequestFormLayout();
                    }


                } else {
                    showAlertDialog(this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    private void showReferenceTypedialog() {

        if (referenceArrayList.size() > 0) {
            final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this, R.style.myDialog);
            builder.setTitle("Select Reference Type")
                    .setItems(R.array.Reference_array, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // The 'which' argument contains the index position
                            // of the selected item
                            et_holder_refer.setText(referenceArrayList.get(which));
                            selectedreferenceType = referenceArrayList.get(which);
                            showReferenceLayout();                                  //

                        }
                    });

            android.support.v7.app.AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    protected void showSelectPoliceStationsDialog() {
        boolean[] checkedPoliceStations = new boolean[array_policeStation.length];
        int count = array_policeStation.length;

        for (int i = 0; i < count; i++) {
            checkedPoliceStations[i] = selectedPoliceStations.contains(array_policeStation[i]);
        }

        DialogInterface.OnMultiChoiceClickListener policeStationsDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    ps_name=array_policeStation[which];
                    selectedPoliceStations.add(array_policeStation[which]);
                    selectedPoliceStationsId.add((CharSequence) allContentList.getObj_policeStationList().get(which).getPs_code());
                } else {
                    selectedPoliceStations.remove(array_policeStation[which]);
                    selectedPoliceStationsId.remove((CharSequence) allContentList.getObj_policeStationList().get(which).getPs_code());
                }

//				onChangeSelectedPoliceStations();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Police Stations");
        builder.setMultiChoiceItems(array_policeStation, checkedPoliceStations, policeStationsDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedPoliceStations();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                //tv_ps_value.setText("Select Police Stations");
                et_reference_ps.setText("");
                policeStationString = "";
                selectedPoliceStations.clear();
                selectedPoliceStationsId.clear();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedPoliceStations() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for (CharSequence policeStation : selectedPoliceStations) {
            stringBuilder.append(policeStation + ",");
        }

        for (CharSequence policeStation : selectedPoliceStationsId) {
            stringBuilderId.append("\'" + policeStation + "\',");
        }

        if (selectedPoliceStations.size() == 0) {
            et_reference_ps.setText("Select Police Stations");
            policeStationString = "";
        } else {
            et_reference_ps.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
            policeStationString = stringBuilderId.toString().substring(0, stringBuilderId.toString().length() - 1);
        }

    }

    public void updateNotificationCount(String notificationCount) {

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        } else {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }

    public void showAlertDialog(final Context context, String title, String message, final Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);


        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }
                        if (status) {
                            View view = SpecialServiceActivity.this.getCurrentFocus();
                            if (view != null) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            }
                            Intent it = new Intent(context, SpecialServiceList.class);
                            context.startActivity(it);
                            finish();
                        }


                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    public void setselectedrequestTypeValue() {
        if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[0])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[0];
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[1])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[1];
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[2])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[2];
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[3])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[3];
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[4])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[4];
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[5])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[5];
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[6])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[6];
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[7])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[7];
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[8])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[8];
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[9])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[9];
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[10])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[10];
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[11])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[11];
        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[12])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[12];
        }

    }

    public void changeRequestFormLayout() {
        //Toast.makeText(this, "SelectedrequestType:---" + selectedrequestType, Toast.LENGTH_SHORT).show();

        if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[0])) {
            Rl_reqest_process_type.setVisibility(View.GONE);
            rl_special_service_form.setVisibility(View.VISIBLE);
            ll_special_services_form_all.setVisibility(View.VISIBLE);
            ll_callDump_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_below_addmore.setVisibility(View.GONE);
            bttn_save_request.setVisibility(View.VISIBLE);

        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[1])) {
            Rl_reqest_process_type.setVisibility(View.GONE);
            rl_special_service_form.setVisibility(View.VISIBLE);
            ll_special_services_form_all.setVisibility(View.VISIBLE);
            ll_callDump_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_below_addmore.setVisibility(View.GONE);
            bttn_save_request.setVisibility(View.VISIBLE);

        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[2])) {
            Rl_reqest_process_type.setVisibility(View.GONE);
            rl_special_service_form.setVisibility(View.VISIBLE);
            ll_special_services_form_all.setVisibility(View.VISIBLE);
            ll_callDump_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_below_addmore.setVisibility(View.GONE);
            bttn_save_request.setVisibility(View.VISIBLE);

        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[3])) {
            Rl_reqest_process_type.setVisibility(View.GONE);
            rl_special_service_form.setVisibility(View.VISIBLE);
            ll_special_services_form_all.setVisibility(View.VISIBLE);
            ll_callDump_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_below_addmore.setVisibility(View.GONE);
            bttn_save_request.setVisibility(View.VISIBLE);


        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[4])) {
            Rl_reqest_process_type.setVisibility(View.GONE);
            rl_special_service_form.setVisibility(View.VISIBLE);
            ll_special_services_form_all.setVisibility(View.VISIBLE);
            ll_callDump_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_below_addmore.setVisibility(View.GONE);
            bttn_save_request.setVisibility(View.VISIBLE);

        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[5])) {
            Rl_reqest_process_type.setVisibility(View.GONE);
            rl_special_service_form.setVisibility(View.VISIBLE);
            ll_special_services_form_all.setVisibility(View.VISIBLE);
            ll_callDump_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_below_addmore.setVisibility(View.GONE);
            bttn_save_request.setVisibility(View.VISIBLE);

        }
        else if(selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[6])){
            Rl_reqest_process_type.setVisibility(View.GONE);
            rl_special_service_form.setVisibility(View.VISIBLE);
            ll_special_services_form_all.setVisibility(View.VISIBLE);
            ll_callDump_addtional_layout_above_addmore.setVisibility(View.VISIBLE);
            ll_LI_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_below_addmore.setVisibility(View.GONE);
            bttn_save_request.setVisibility(View.VISIBLE);

        }
        else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[7])) {
            Rl_reqest_process_type.setVisibility(View.GONE);
            rl_special_service_form.setVisibility(View.VISIBLE);
            ll_special_services_form_all.setVisibility(View.VISIBLE);
            ll_callDump_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_below_addmore.setVisibility(View.GONE);
            bttn_save_request.setVisibility(View.VISIBLE);

        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[9])) {
            Rl_reqest_process_type.setVisibility(View.VISIBLE);
            rl_special_service_form.setVisibility(View.VISIBLE);
            ll_special_services_form_all.setVisibility(View.VISIBLE);
            ll_callDump_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_above_addmore.setVisibility(View.VISIBLE);
            ll_LI_addtional_layout_below_addmore.setVisibility(View.VISIBLE);
            bttn_save_request.setVisibility(View.GONE);
            if (partialsavedLIform) {                                                  //-------------------------------------IF request for complete the LI form for Patial saved Data-------------//
                getLIPartialSavedFormData();
            }

        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[10])) {
            Rl_reqest_process_type.setVisibility(View.GONE);
            rl_special_service_form.setVisibility(View.VISIBLE);
            ll_special_services_form_all.setVisibility(View.VISIBLE);
            ll_callDump_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_below_addmore.setVisibility(View.GONE);
            bttn_save_request.setVisibility(View.VISIBLE);

        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[11])) {
            Rl_reqest_process_type.setVisibility(View.GONE);
            rl_special_service_form.setVisibility(View.VISIBLE);
            ll_special_services_form_all.setVisibility(View.VISIBLE);
            ll_callDump_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_below_addmore.setVisibility(View.GONE);
            bttn_save_request.setVisibility(View.VISIBLE);

        } else if (selectedrequestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[12])) {
            Rl_reqest_process_type.setVisibility(View.GONE);
            rl_special_service_form.setVisibility(View.VISIBLE);
            ll_special_services_form_all.setVisibility(View.VISIBLE);
            ll_callDump_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_above_addmore.setVisibility(View.GONE);
            ll_LI_addtional_layout_below_addmore.setVisibility(View.GONE);
            bttn_save_request.setVisibility(View.VISIBLE);

        }



    }

    private void getLIPartialSavedFormData() {
        //partialSavedLIRequestMasterID
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_LI_PARTIAL_SAVE);
        taskManager.setLIPartialSavedRecord(true);
        String[] keys = {"master_id"};
        String[] values = {partialSavedLIRequestMasterID};
        taskManager.doStartTask(keys, values, true, true);
    }

    public void parseLIPartialSavedResult(String response) {
        Log.e("DAPL", response);
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            partialSavedData = objectMapper.readValue(response, LIPartialSavedData.class);
        } catch (JsonMappingException e) {
            Log.e("DAPL", e+"");
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("DAPL", e+"");
            e.printStackTrace();
        }
        if (partialSavedData.getStatus()==1) {
            if (partialSavedData.getResult().size() > 0) {
                Rl_reqest_process_type.setVisibility(View.VISIBLE);
                if(partialSavedData.getResult().get(0).getUrgentGeneralFlag().equalsIgnoreCase("1")){
                    RbUrgent.setChecked(true);
                }
                else {
                    RbGeneral.setChecked(true);
                }
                if(!partialSavedData.getResult().get(0).getCaseRef().isEmpty() && !partialSavedData.getResult().get(0).getCaseRef().equals("")) {

                   if(partialSavedData.getResult().get(0).getCaseRef().contains("_")) {
                       String someString = partialSavedData.getResult().get(0).getCaseRef();
                       char someChar = '_';
                       int count = 0;
                       for (int i = 0; i < someString.length(); i++) {
                           if (someString.charAt(i) == someChar) {
                               count++;
                           }
                       }
                       if (count == 2) {
                           et_holder_refer.setText(referenceArry[0]);
                           ll_reference_case.setVisibility(View.VISIBLE);
                           ll_reference_enquiry.setVisibility(View.GONE);
                           String[] parts = partialSavedData.getResult().get(0).getCaseRef().split("_");
                          // int psCodeposition=Constants.policeStationIDArrayList.indexOf(parts[0]);
                           et_reference_ps.setText(partialSavedData.getResult().get(0).getRequestPSName());
                           et_reference_year.setText(parts[1]);
                           et_reference_caseNo.setText(parts[2]);

                       }
                       else {
                           et_holder_refer.setText(referenceArry[1]);
                           ll_reference_case.setVisibility(View.GONE);
                           ll_reference_enquiry.setVisibility(View.VISIBLE);
                           et_reference_enquiry.setText(partialSavedData.getResult().get(0).getCaseRef());
                           if(et_request_type.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[9])){
                               //textInputLayoutEnquirydetails.setVisibility(View.VISIBLE);
                               et_enquiry_details.setText(partialSavedData.getResult().get(0).getRequestBriefDataSuperiors());
                           }
                           else {
                              // textInputLayoutEnquirydetails.setVisibility(View.GONE);
                           }

                       }
                   }
                    else {
                        et_holder_refer.setText(referenceArry[1]);
                        ll_reference_case.setVisibility(View.GONE);
                        ll_reference_enquiry.setVisibility(View.VISIBLE);
                        et_reference_enquiry.setText(partialSavedData.getResult().get(0).getCaseRef());
                       if(et_request_type.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[9])){
                          // textInputLayoutEnquirydetails.setVisibility(View.VISIBLE);
                           et_enquiry_details.setText(partialSavedData.getResult().get(0).getRequestBriefDataSuperiors());
                       }
                       else {
                           //textInputLayoutEnquirydetails.setVisibility(View.GONE);
                       }
                    }
                }
                autoCompleteTextViewGroup.setText(partialSavedData.getResult().get(0).getGroup());
                et_state.setText(partialSavedData.getResult().get(0).getRequeststateLI());
                et_listener.setText(partialSavedData.getResult().get(0).getListener());
                et_user.setText(partialSavedData.getResult().get(0).getUser());
                int pos=Arrays.asList(justificationTitle).indexOf(partialSavedData.getResult().get(0).getJustTitle());
                spjustTitle.setSelection(pos); ;
                et_justification.setText(partialSavedData.getResult().get(0).getJustSubject());

                if (partialSavedData.getResult().get(0).getDetails().size() > 0) {
                    if (partialSavedData.getResult().get(0).getDetails().get(0).getMobileNumber().equals("")) {
                        selectedLIType = liArray[1];
                    } else if (partialSavedData.getResult().get(0).getDetails().get(0).getImeiNumber().equals("")) {
                        selectedLIType = liArray[0];
                    }
                }
                if(selectedLIType.equals(liArray[0])) {
                    et_phone_imei.setText(liArray[0]);
                }
                else if(selectedLIType.equals(liArray[1])){
                    et_phone_imei.setText(liArray[1]);

                }

                    for (int i = 0; i < partialSavedData.getResult().get(0).getDetails().size(); i++) {
                        Log.e("DAPL", "DETAILS ARRAY LENGTH"+partialSavedData.getResult().get(0).getDetails().size());
                        Log.e("DAPL",  "Phone_imei_flag:----------"+phone_imei_flag);
                        final LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View addviewLi = layoutInflater.inflate(R.layout.legal_interception_item_layout, null);
                        final TextView tv_phone_label = (TextView) addviewLi.findViewById(R.id.tv_phone);
                        final TextView tv_phone_text = (TextView) addviewLi.findViewById(R.id.tv_phone_text);
                        final TextView tv_officer_phone =(TextView) addviewLi.findViewById(R.id.tv_officer_phone);
                        final EditText et_officer_phone = (EditText) addviewLi.findViewById(R.id.et_officer_phone);
                        final EditText et_officer_name = (EditText) addviewLi.findViewById(R.id.et_officer_name);
                        final EditText et_subGroup = (EditText) addviewLi.findViewById(R.id.et_subGroup);
                        final EditText et_sdr_name = (EditText) addviewLi.findViewById(R.id.et_sdr_name);
                        final EditText et_sdr_address = (EditText) addviewLi.findViewById(R.id.et_sdr_address);
                        final EditText et_provider = (EditText) addviewLi.findViewById(R.id.et_provider);
                        final EditText et_circle = (EditText) addviewLi.findViewById(R.id.et_circle);
                        final EditText et_plan_type = (EditText) addviewLi.findViewById(R.id.et_plan_type);
                        final EditText et_phone = (EditText) addviewLi.findViewById(R.id.et_phone);
                        final EditText et_imei = (EditText) addviewLi.findViewById(R.id.et_imei);

                        if(selectedLIType.equals(liArray[0])) {
                            et_imei.setVisibility(View.GONE);
                            tv_phone_label.setText("Phone:");
                            tv_phone_text.setVisibility(View.VISIBLE);
                            et_phone.setText(partialSavedData.getResult().get(0).getDetails().get(i).getMobileNumber());
                            et_phone.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                        }
                        else if(selectedLIType.equals(liArray[1])) {
                            tv_phone_label.setText("Imei:");
                            tv_phone_text.setVisibility(View.GONE);
                            et_phone.setVisibility(View.GONE);
                            et_imei.setVisibility(View.VISIBLE);
                            et_imei.setText(partialSavedData.getResult().get(0).getDetails().get(i).getImeiNumber());
                            et_imei.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
                        }
                        et_phone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (!hasFocus) {

                                    //   Toast.makeText(SpecialServiceActivity.this, "LOST FOCUS", Toast.LENGTH_SHORT).show();
                                    String Value = "";
                                    if (selectedLIType.equalsIgnoreCase("Phone")) {
                                        Value = et_phone.getText().toString();
                                        if (Value.length() == 10) {
                                            setSDRofRow(addviewLi, Value);
                                            checkPhoneforBlacklist(addviewLi, Value);
                                        } else {
                                            Utility.showAlertDialog(SpecialServiceActivity.this, "Error", "Phone no should be 10 digit", false);
                                        }

                                    }

                                }
                            }
                        });
                        et_imei.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (!hasFocus) {
                                    String Value = "";
                                    // if(selectedLIType.equalsIgnoreCase("Imei")){
                                    Value = et_imei.getText().toString();
                                    if (Value.length() > 13 && Value.length() < 16) {
                                        setSDRofRow(addviewLi, Value);
                                        checkPhoneforBlacklist(addviewLi, Value);
                                    } else {
                                        Utility.showAlertDialog(SpecialServiceActivity.this, "Error", "Imei no should be 14 or 15 digit", false);
                                    }


                                }
                            }
                        });


                    /*    if (selectedLIType.equalsIgnoreCase("Phone")) {
                            et_phone.setVisibility(View.VISIBLE);
                            tv_phone_label.setText("Phone");
                            tv_phone_text.setVisibility(View.VISIBLE);
                            et_imei.setVisibility(View.GONE);
                            et_phone.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                        } else if (selectedLIType.equalsIgnoreCase("Imei")) {
                            tv_phone_label.setText("Imei");
                            tv_phone_text.setVisibility(View.GONE);
                            et_phone.setVisibility(View.GONE);
                            et_imei.setVisibility(View.VISIBLE);
                            et_imei.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
                        }
*/
                        final Spinner spLoggerNo = (Spinner) addviewLi.findViewById(R.id.sp_loggerNo);
                        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, logger_arr_str);
                        arrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                        spLoggerNo.setAdapter(arrayAdapter);
                        spLoggerNo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                ((TextView) spLoggerNo.getSelectedView()).setTextColor(getResources().getColor(R.color.color_deep_blue));
                                if(spLoggerNo.getItemAtPosition(position).toString().equalsIgnoreCase("MOBILE DIVERSION")){
                                    tv_officer_phone.setVisibility(View.VISIBLE);
                                    et_officer_phone.setVisibility(View.VISIBLE);
                                }
                                else{
                                    tv_officer_phone.setVisibility(View.GONE);
                                    et_officer_phone.setVisibility(View.GONE);
                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                        int pos2=Arrays.asList(logger_arr_str).indexOf(partialSavedData.getResult().get(0).getDetails().get(i).getLoggerNumber());
                        spLoggerNo.setSelection(pos2);
                        et_officer_phone.setText(partialSavedData.getResult().get(0).getDetails().get(i).getOfficerPhone());
                        et_officer_name.setText(partialSavedData.getResult().get(0).getDetails().get(i).getOfficerName());
                        et_subGroup.setText(partialSavedData.getResult().get(0).getDetails().get(i).getLoggerSubGroup());
                        et_sdr_name.setText(partialSavedData.getResult().get(0).getDetails().get(i).getLoggerSdrHolder());
                        et_sdr_address.setText(partialSavedData.getResult().get(0).getDetails().get(i).getLoggerSdrAddress());
                        et_provider.setText(partialSavedData.getResult().get(0).getDetails().get(i).getLoggerSdrProvider());
                        et_circle.setText(partialSavedData.getResult().get(0).getDetails().get(i).getLoggerSdrCircle());
                        et_plan_type.setText(partialSavedData.getResult().get(0).getDetails().get(i).getLoggerSdrPlanType());
                        TextView tv_delete = (TextView) addviewLi.findViewById(R.id.tv_delete_row);
                        tv_delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ((LinearLayout) addviewLi.getParent()).removeView(addviewLi);
                            }
                        });

                        ll_special_services_form_all.addView(addviewLi);
                    }



            }

        }
    }


    public void clearAllFieldValue() {
        et_phone.setText("");
        et_circle.setText("");
        et_fromDate.setText("");
        et_toDate.setText("");
    }

    public void showReferenceLayout() {

        if (selectedreferenceType.equalsIgnoreCase("Case")) {
            ll_reference_case.setVisibility(View.VISIBLE);
            ll_reference_enquiry.setVisibility(View.GONE);
        } else if (selectedreferenceType.equalsIgnoreCase("Enquiry")) {
            ll_reference_enquiry.setVisibility(View.VISIBLE);
            ll_reference_case.setVisibility(View.GONE);
            /*if(et_request_type.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[9])){
                textInputLayoutEnquirydetails.setVisibility(View.VISIBLE);
            }
            else {
                textInputLayoutEnquirydetails.setVisibility(View.GONE);
            }
*/
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
