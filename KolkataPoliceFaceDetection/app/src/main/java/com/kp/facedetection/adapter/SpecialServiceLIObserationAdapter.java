package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.ChkNoAppList;
import com.kp.facedetection.model.Detail;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by user on 22-05-2018.
 */

public class SpecialServiceLIObserationAdapter extends RecyclerView.Adapter<SpecialServiceLIObserationAdapter.ViewHolder> {

    private List<ChkNoAppList> detailsList;
    Context context;

    public SpecialServiceLIObserationAdapter(Context context, List<ChkNoAppList> detailsList) {
        this.context = context;
        this.detailsList = detailsList;

    }

    @Override
    public SpecialServiceLIObserationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.special_service_li_observation_item_layout, parent, false);
        SpecialServiceLIObserationAdapter.ViewHolder viewHolder = new SpecialServiceLIObserationAdapter.ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(SpecialServiceLIObserationAdapter.ViewHolder holder, int position) {
        Constants.changefonts(holder.requestId_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.requestDetailsId_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.Tv_request_datetime, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.requestBy_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.request_type_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.period_TV_label, context, "Calibri Bold.ttf");

        Constants.changefonts(holder.requestId_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.requestDetailsId_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.Tv_request_datetime_value, context, "Calibri.ttf");
        Constants.changefonts(holder.requestBy_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.request_type_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.period_TV, context, "Calibri.ttf");

        holder.requestId_TV.setText(detailsList.get(position).getRequestId());
        holder.requestDetailsId_TV.setText(detailsList.get(position).getRequestDetailsId());
        holder.Tv_request_datetime_value.setText(detailsList.get(position).getSubmitDatetime());
        holder.requestBy_TV.setText(detailsList.get(position).getSubmitBy());
        holder.request_type_TV.setText(detailsList.get(position).getRequestType());
        holder.period_TV.setText(detailsList.get(position).getPeriod());





    }

    @Override
    public int getItemCount() {
        return detailsList.size()>0 ? detailsList.size():0 ;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView requestId_TV_label,requestDetailsId_TV_label,Tv_request_datetime,requestBy_TV_label,request_type_TV_label,period_TV_label;
        TextView requestId_TV, requestDetailsId_TV, Tv_request_datetime_value, requestBy_TV, request_type_TV, period_TV;

        public ViewHolder(View itemView) {
            super(itemView);
            requestId_TV=(TextView) itemView.findViewById(R.id.requestId_TV);
            requestDetailsId_TV=(TextView) itemView.findViewById(R.id.requestDetailsId_TV);
            Tv_request_datetime_value=(TextView) itemView.findViewById(R.id.Tv_request_datetime_value);
            requestBy_TV=(TextView) itemView.findViewById(R.id.requestBy_TV);
            request_type_TV=(TextView) itemView.findViewById(R.id.request_type_TV);
            period_TV=(TextView) itemView.findViewById(R.id.period_TV);

        }

        @Override
        public void onClick(View v) {

        }
    }
}
