package com.kp.facedetection.model;

import java.io.Serializable;

public class NotificationDetails implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String notificationId,senderName,subject,description;

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}
	
	
}
