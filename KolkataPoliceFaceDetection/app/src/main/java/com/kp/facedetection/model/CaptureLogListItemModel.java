package com.kp.facedetection.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DAT-165 on 12-09-2017.
 */

public class CaptureLogListItemModel implements Parcelable {

    public String psName, psCode, caseNO, caseDate, recordDate, po_lat,po_long, ioName, address, log_id, note, fir_no, fir_yr, tag_status;


    public CaptureLogListItemModel(Parcel in) {

        psName = in.readString();
        psCode = in.readString();
        caseNO = in.readString();
        caseDate = in.readString();
        recordDate = in.readString();
        po_lat = in.readString();
        po_long = in.readString();
        ioName = in.readString();
        address = in.readString();
        log_id = in.readString();
        note = in.readString();
        fir_no = in.readString();
        fir_yr = in.readString();
        tag_status = in.readString();
    }

    public static final Creator<CaptureLogListItemModel> CREATOR = new Creator<CaptureLogListItemModel>() {
        @Override
        public CaptureLogListItemModel createFromParcel(Parcel in) {
            return new CaptureLogListItemModel(in);
        }

        @Override
        public CaptureLogListItemModel[] newArray(int size) {
            return new CaptureLogListItemModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(psName);
        dest.writeString(psCode);
        dest.writeString(caseNO);
        dest.writeString(caseDate);
        dest.writeString(recordDate);
        dest.writeString(po_lat);
        dest.writeString(po_long);
        dest.writeString(ioName);
        dest.writeString(address);
        dest.writeString(log_id);
        dest.writeString(note);
        dest.writeString(fir_no);
        dest.writeString(fir_yr);
        dest.writeString(tag_status);
    }
}
