package com.kp.facedetection.utility;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.kp.facedetection.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by DAT-Asset-131 on 22-03-2016.
 */
public class DateUtils {

    public static void setDate(Context con,final TextView tvDate,boolean isMaxCurrentDate){
        Calendar calender = Calendar.getInstance();
        DatePickerDialog mDialog = new DatePickerDialog(con,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        month++;
                        if(day<=9){
                            tvDate.setText("0"+day+"-"+changeMonth(Integer.toString(month))+"-"+year);
                        }
                        else {
                            tvDate.setText(day+"-"+changeMonth(Integer.toString(month))+"-"+year);
                        }

                    }
                }, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH),
                calender.get(Calendar.DAY_OF_MONTH));
        if(isMaxCurrentDate){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                mDialog.getDatePicker().setMaxDate(System.currentTimeMillis()-100);
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                mDialog.getDatePicker().setMinDate(System.currentTimeMillis()-100);
            }
        }


        mDialog.show();
    }
    public static void setTime(Context con,final TextView tvTime){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        String time = sdf.format(calendar.getTime());
        String inputTime = time, inputHours, inputMinutes;

        inputHours = inputTime.substring(0, 2);
        inputMinutes = inputTime.substring(3, 5);

        TimePickerDialog mTimePicker = new TimePickerDialog(con, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String timeFormat="";
                if (selectedHour == 0) {
                    selectedHour += 12;
                    timeFormat = "AM";
                } else if (selectedHour == 12) {
                    timeFormat = "PM";
                } else if (selectedHour > 12) {
                    selectedHour -= 12;
                    timeFormat = "PM";
                } else {
                    timeFormat = "AM";
                }

                String selectedTime = selectedHour + ":" + selectedMinute + " " + timeFormat;

                tvTime.setText(selectedTime);

            }
        }, Integer.parseInt(inputHours), Integer.parseInt(inputMinutes), true);//mention true for 24 hour's time format,false for 12 hour's time format
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private static String changeMonth(String month) {

        switch(month) {
            case "1" :
                month = "JAN";
                break;
            case "2" :
                month = "FEB";
                break;
            case "3" :
                month = "MAR";
                break;
            case "4" :
                month = "APR";
                break;
            case "5" :
                month = "MAY";
                break;
            case "6" :
                month = "JUN";
                break;
            case "7" :
                month = "JUL";
                break;
            case "8" :
                month = "AUG";
                break;
            case "9" :
                month = "SEP";
                break;
            case "10" :
                month = "OCT";
                break;
            case "11" :
                month = "NOV";
                break;
            case "12" :
                month = "DEC";
                break;
            default:
                break;

        }

        return month;

    }

    private static String changeMonthForDateComparison(String month){

        switch(month) {
            case "JAN" :
                month = "01";
                break;

            case "FEB" :
                month = "02";
                break;

            case "MAR" :
                month = "03";
                break;

            case "APR" :
                month = "04";
                break;

            case "MAY" :
                month = "05";
                break;

            case "JUN" :
                month = "06";
                break;

            case "JUL" :
                month = "07";
                break;

            case "AUG" :
                month = "08";
                break;

            case "SEP" :
                month = "09";
                break;

            case "OCT" :
                month = "10";
                break;

            case "NOV" :
                month = "11";
                break;

            case "DEC" :
                month = "12";
                break;
        }

        return month;
    }
    public static boolean timeComarison(String fromtime,String totime){
        boolean flag=true;
        if(fromtime.contains("PM") && totime.contains("AM")){
            flag=false;
        }
        else if(fromtime.contains("AM")&& totime.contains("AM")){
            String modFrom =fromtime.replace("AM","");
            String modTo=totime.replace("AM","");
            String[] fromparts = modFrom.split(":");
            String fromHour=fromparts[0];
            String fromMinute=fromparts[1];
            String[] toparts = totime.split(":");
            String toHour=toparts[0];
            String toMinute=toparts[1];
            Log.e("DAPL","fromHour----"+fromHour+"fromMinute---"+fromMinute);
            Log.e("DAPL","toHour----"+toHour+"toMinute---"+toMinute);

            /*if(Integer.parseInt(fromHour)>Integer.parseInt(toHour)){


            }*/


        }
        return flag;
    }

    public static boolean dateComparison(String date_start, String date_end){


        boolean flag=true;

        try{

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            String str1 = date_start.replace(date_start.substring(3,6),changeMonthForDateComparison(date_start.substring(3, 6)));
            System.out.println("Start Date: "+str1);
            Date date1 = formatter.parse(str1);
            if(!date_end.equals("")) {

                String str2 = date_end.replace(date_end.substring(3, 6), changeMonthForDateComparison(date_end.substring(3, 6)));
                System.out.println("END Date: " + str2);
                Date date2 = formatter.parse(str2);

                if (date2.compareTo(date1) < 0) {
                    System.out.println("End date can't greater than start date");
                    flag = false;
                }
            }

        }catch (ParseException e1){
            e1.printStackTrace();
        }

      return flag;

    }
   public static void  overDueDay(Context context,String returnAbleDate,TextView textView,String status)
   {
       SimpleDateFormat spdf2=new SimpleDateFormat("dd-MMM-yy");
       long day=0;
       try {
           Date currdate = new Date();
           Date retunDate = spdf2.parse(returnAbleDate);
           long diff = currdate.getTime() - retunDate.getTime();
           day = (diff / (1000 * 60 * 60 * 24));

       }  catch(ParseException e){
           e.printStackTrace();

       }
       if(!status.equalsIgnoreCase("null") && status.equalsIgnoreCase("1")) {
           //  setDateOverdue(day,)
           if (day > 0) {
               if (day == 1) {
                   textView.setText(returnAbleDate + " (Overdue by " + String.valueOf(day) + "day)");
               } else {
                   textView.setText(returnAbleDate + " (Overdue by " + String.valueOf(day) + "days)");
               }
           } else {
               textView.setText(returnAbleDate);
           }
           textView.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));
       }
       else if(!status.equalsIgnoreCase("null") && status.equalsIgnoreCase("2")){
           textView.setText(returnAbleDate);
           textView.setTextColor(context.getResources().getColor(android.R.color.holo_green_dark));
       }
       else if(!status.equalsIgnoreCase("null") && status.equalsIgnoreCase("0")){
           textView.setText(returnAbleDate);
           textView.setTextColor(context.getResources().getColor(R.color.color_light_blue));

           /*if (day > 0) {
               if (day == 1) {
                   textView.setText(returnAbleDate + " (Overdue by " + String.valueOf(day) + "day)");
                   textView.setTextColor(context.getResources().getColor(android.R.color.holo_blue_dark));

               } else {
                   textView.setText(returnAbleDate + " (Overdue by " + String.valueOf(day) + "days)");
                   textView.setTextColor(context.getResources().getColor(android.R.color.holo_blue_dark));


               }
           } else {
               textView.setText(returnAbleDate);
               textView.setTextColor(context.getResources().getColor(android.R.color.holo_green_dark));
           }*/
       }

   }
   public static void setStatusColor(Context context,TextView textView,String status,String statusText)
   {
       if(status.equalsIgnoreCase("1"))
       {
           textView.setText(statusText);
           textView.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));
       }
       else  if(status.equalsIgnoreCase("2"))
       {
           textView.setText(statusText);
           textView.setTextColor(context.getResources().getColor(android.R.color.holo_green_dark));
       }
       else  if(status.equalsIgnoreCase("0"))
       {
           textView.setText(statusText);
           textView.setTextColor(context.getResources().getColor(R.color.color_light_blue));

       }
   }
   public static String changeDateFormat(String datestring){
       String formatedDate = "";
       try {

           SimpleDateFormat simpledatafo = new SimpleDateFormat("yyyy-MM-dd");
           Date date = simpledatafo.parse(datestring);
           SimpleDateFormat simpledatafonew = new SimpleDateFormat("dd-MMM-yyyy");
            formatedDate = simpledatafonew.format(date);
       }catch(Exception e){

       }
       return  formatedDate;
   }


}