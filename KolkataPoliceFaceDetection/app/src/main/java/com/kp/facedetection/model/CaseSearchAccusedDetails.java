package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 26-07-2016.
 */
public class CaseSearchAccusedDetails implements Serializable {

    private String nameAccused="";
    private String addressAccused="";

    public String getNameAccused() {
        return nameAccused;
    }

    public void setNameAccused(String nameAccused) {
        this.nameAccused = nameAccused;
    }

    public String getAddressAccused() {
        return addressAccused;
    }

    public void setAddressAccused(String addressAccused) {
        this.addressAccused = addressAccused;
    }
}
