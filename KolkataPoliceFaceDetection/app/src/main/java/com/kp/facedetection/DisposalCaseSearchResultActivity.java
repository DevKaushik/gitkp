package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.kp.facedetection.adapter.ModifiedCaseSearchAdapter;
import com.kp.facedetection.interfaces.OnMapIconChangeListener;
import com.kp.facedetection.model.CaseSearchDetails;
import com.kp.facedetection.model.CriminalDetails;
import com.kp.facedetection.model.LatLongDistance;
import com.kp.facedetection.singletone_classes.CommunicationViews;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by DAT-165 on 14-07-2017.
 */
public class DisposalCaseSearchResultActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, OnMapIconChangeListener, Observer {

    private TextView tv_resultCount;
    private ListView lv_caseSearchList;
    private Button btnLoadMore;

    private String[] keys;
    private String[] values;
    private String pageno;
    private String totalResult="";
    private String searchCase = "";
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private TextView tv_watermark;

    ArrayList<CriminalDetails> criminalList;
    ModifiedCaseSearchAdapter caseSearchAdapter;

    List<CaseSearchDetails> caseSearchDetailsList;
    private ImageView iv_showMap;

    private String[] key_map;
    private String[] value_map;

    private String[] keyMap_parse;
    private String[] valueMap_parse;
    private String totlaResultMap = "";
    private String pagenoMap = "";
    private ArrayList<CaseSearchDetails> mapAllList = new ArrayList<CaseSearchDetails>();
    private ArrayList<LatLongDistance> latLongList = new ArrayList<LatLongDistance>();
    KPFaceDetectionApplication kpFaceDetectionApplication;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    public int mapCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changed_case_search_result_layout);
        ObservableObject.getInstance().addObserver(this);

        initView();

        clickEvents();

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);


        keys = getIntent().getStringArrayExtra("keys");
        values = getIntent().getStringArrayExtra("value");
        pageno = getIntent().getStringExtra("pageno");
        totalResult = getIntent().getStringExtra("totalResult");
        caseSearchDetailsList = (List<CaseSearchDetails>) getIntent().getSerializableExtra("MODIFIED_CASE_SEARCH_LIST");

        key_map = getIntent().getStringArrayExtra("key_map");
        value_map = getIntent().getStringArrayExtra("value_map");

        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }

        caseSearchAdapter = new ModifiedCaseSearchAdapter(this,caseSearchDetailsList);
        lv_caseSearchList.setAdapter(caseSearchAdapter);
        caseSearchAdapter.notifyDataSetChanged();

        if (Integer.parseInt(totalResult) > 20) {

            // Adding Load More button to listview at bottom
            lv_caseSearchList.addFooterView(btnLoadMore);
        }

        lv_caseSearchList.setOnItemClickListener(this);

        /*  Set user name as Watermark  */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-55);
    }

    private void initView() {

        tv_resultCount=(TextView)findViewById(R.id.tv_resultCount);
        lv_caseSearchList=(ListView)findViewById(R.id.lv_caseSearchList);
        iv_showMap = (ImageView)findViewById(R.id.iv_showMap);
        iv_showMap.setVisibility(View.VISIBLE);

        // LoadMore button
        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");

        tv_watermark = (TextView)findViewById(R.id.tv_watermark);

        CommunicationViews.getInstance().setOnMapIconChangeListener(this);

    }


    private void clickEvents(){

        iv_showMap.setOnClickListener(this);

        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // Starting a new async task
                fetchCaseSearchResultPagination();
            }
        });

    }

    private void fetchCaseSearchResultPagination() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CASE_SEARCH_FROM_DASHBOARD);
        taskManager.setDisposalToCaseSearchPagination(true);

        values[8] = (Integer.parseInt(pageno) + 1) + "";

        taskManager.doStartTask(keys, values, true);
    }

    public void parseDisposalsToCaseSearchResultPagination(String result, String[] keys, String[] values) {

        //System.out.println("parseDisposalsToCaseSearchResultPagination: " + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("pageno").toString();
                    totalResult = jObj.opt("totalresult").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseDisposalCaseSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(DisposalCaseSearchResultActivity.this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }


            } catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(DisposalCaseSearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG
                        , false);

            }

        }

    }

    private void parseDisposalCaseSearchResponse(JSONArray result_array){

        for(int i=0;i<result_array.length();i++){

            JSONObject obj = null;

            try {

                obj = result_array.getJSONObject(i);

                CaseSearchDetails caseSearchDetails = new CaseSearchDetails();

                if(!obj.optString("ROWNUMBER").equalsIgnoreCase("null"))
                    caseSearchDetails.setRowNumber(obj.optString("ROWNUMBER"));
                if(!obj.optString("PSCODE").equalsIgnoreCase("null"))
                    caseSearchDetails.setPsCode(obj.optString("PSCODE"));
                if(!obj.optString("PS").equalsIgnoreCase("null"))
                    caseSearchDetails.setPs(obj.optString("PS"));
                if(!obj.optString("CASENO").equalsIgnoreCase("null"))
                    caseSearchDetails.setCaseNo(obj.optString("CASENO"));
                if(!obj.optString("CASEDATE").equalsIgnoreCase("null"))
                    caseSearchDetails.setCaseDate(obj.optString("CASEDATE"));
                if(!obj.optString("UNDER_SECTION").equalsIgnoreCase("null"))
                    caseSearchDetails.setUnderSection(obj.optString("UNDER_SECTION"));
                if(!obj.optString("CATEGORY").equalsIgnoreCase("null"))
                    caseSearchDetails.setCategory(obj.optString("CATEGORY"));
                if(!obj.optString("CASE_YR").equalsIgnoreCase("null"))
                    caseSearchDetails.setCaseYr(obj.optString("CASE_YR"));
                if(!obj.optString("FIR_STATUS").equalsIgnoreCase("null"))
                    caseSearchDetails.setFirStatus(obj.optString("FIR_STATUS"));
                if(!obj.optString("OCCUR_TIMING").equalsIgnoreCase("null"))
                    caseSearchDetails.setOccurTime(obj.optString("OCCUR_TIMING"));
                if(!obj.optString("MOD_OPER").equalsIgnoreCase("null"))
                    caseSearchDetails.setModOper(obj.optString("MOD_OPER"));
                if(!obj.optString("PO_LAT").equalsIgnoreCase("null") && !obj.optString("PO_LAT").equalsIgnoreCase("") && obj.optString("PO_LAT") != null)
                    caseSearchDetails.setPoLat(obj.optString("PO_LAT"));
                if(!obj.optString("PO_LONG").equalsIgnoreCase("null") && !obj.optString("PO_LONG").equalsIgnoreCase("") && obj.optString("PO_LONG") != null)
                    caseSearchDetails.setPoLong(obj.optString("PO_LONG"));

                JSONArray briefMatch_array = obj.getJSONArray("brief_match");
                if(briefMatch_array.length() > 0){
                    List<String> briefMatch_list = new ArrayList<>();
                    for(int j=0;j<briefMatch_array.length();j++){
                        briefMatch_list.add(briefMatch_array.optString(j));
                    }
                    caseSearchDetails.setBriefMatch_list(briefMatch_list);
                }

                caseSearchDetailsList.add(caseSearchDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        int currentPosition = lv_caseSearchList.getFirstVisiblePosition();
        int lastVisiblePosition = lv_caseSearchList.getLastVisiblePosition();

        // Setting new scroll position
        lv_caseSearchList.setSelectionFromTop(currentPosition + 1, 0);

        if((Integer.parseInt(totalResult)-lastVisiblePosition)<20){
            lv_caseSearchList.removeFooterView(btnLoadMore);
        }

    }

    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     * */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        TextView tv_caseName = (TextView) view.findViewById(R.id.tv_caseName);

        String headerValue = caseSearchDetailsList.get(position).getPs()+" C/No. "+caseSearchDetailsList.get(position).getCaseNo()+" dated "+caseSearchDetailsList.get(position).getCaseDate();
        String ps_code = caseSearchDetailsList.get(position).getPsCode();
        String case_no = caseSearchDetailsList.get(position).getCaseNo();
        String case_yr = caseSearchDetailsList.get(position).getCaseYr();

        Intent in = new Intent(DisposalCaseSearchResultActivity.this,CaseSearchFIRDetailsActivity.class);
        in.putExtra("ITEM_NAME", tv_caseName.getText().toString().trim());
        in.putExtra("HEADER_VALUE", headerValue);
        in.putExtra("PS_CODE",ps_code);
        in.putExtra("CASE_NO", case_no);
        in.putExtra("CASE_YR", case_yr);
        in.putExtra("PS_NAME", "");
        in.putExtra("POSITION",position+"");
        in.putExtra("FROM_PAGE","DisposalCaseSearchResultActivity");

        startActivity(in);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }


    public void disableView(View v) {

        v.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0; i < vg.getChildCount(); i++) {
                View child = vg.getChildAt(i);
                disableView(child);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_showMap:

                fetchMapView();
                break;
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }


    private void fetchMapView() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_MAP_VIEW_DISPOSAL);
        taskManager.setMapViewCaseForDisposal(true);

        taskManager.doStartTask(key_map, value_map, true);
    }

    public void parseMapViewCaseForDisposal(String result, String[] keysMap, String[] valuesMap) {

        //System.out.println("parseMapViewCaseForDisposal: " + result);

        if (result != null && !result.equals("")) {
            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keyMap_parse = keysMap;
                    this.valueMap_parse = valuesMap;
                    pagenoMap = jObj.opt("page").toString();
                    totlaResultMap = jObj.opt("count").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseMapViewAllResponse(resultArray);
                }
                else {
                    showAlertDialog(DisposalCaseSearchResultActivity.this, Constants.ERROR_DETAILS_TITLE, "Sorry! No location found", false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(DisposalCaseSearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }

    }


    private void parseMapViewAllResponse(JSONArray result_array) {

        kpFaceDetectionApplication = KPFaceDetectionApplication.getApplication();
        mapAllList.clear();
        latLongList.clear();
        mapCount = 0;

        for (int i = 0; i < result_array.length(); i++) {

            JSONObject obj = null;
            try {
                obj = result_array.getJSONObject(i);

                LatLongDistance latLongDistance = new LatLongDistance();
                CaseSearchDetails caseMapDetails = new CaseSearchDetails();

                if (!obj.optString("PS").equalsIgnoreCase("null") && !obj.optString("PS").equalsIgnoreCase("") && obj.optString("PS") != null){
                    caseMapDetails.setPs(obj.optString("PS"));
                }

                if (!obj.optString("UNDER_SECTION").equalsIgnoreCase("null") && !obj.optString("UNDER_SECTION").equalsIgnoreCase("") && obj.optString("UNDER_SECTION") != null){
                    caseMapDetails.setUnderSection(obj.optString("UNDER_SECTION"));
                }

                if (!obj.optString("CASEDATE").equalsIgnoreCase("null") && !obj.optString("CASEDATE").equalsIgnoreCase("") && obj.optString("CASEDATE") != null){
                    caseMapDetails.setCaseDate(obj.optString("CASEDATE"));
                }

                if (!obj.optString("CASE_YR").equalsIgnoreCase("null"))
                    caseMapDetails.setCaseYr(obj.optString("CASE_YR"));

                if (!obj.optString("CASENO").equalsIgnoreCase("null"))
                    caseMapDetails.setCaseNo(obj.optString("CASENO"));

                if (!obj.optString("PSCODE").equalsIgnoreCase("null"))
                    caseMapDetails.setPsCode(obj.optString("PSCODE"));

                if (!obj.optString("CATEGORY").equalsIgnoreCase("null"))
                    caseMapDetails.setCategory(obj.optString("CATEGORY"));

                if (!obj.optString("PO_LAT").equalsIgnoreCase("null") && obj.optString("PO_LAT") != null && !obj.optString("PO_LAT").equalsIgnoreCase("")) {
                    caseMapDetails.setPoLat(obj.optString("PO_LAT"));                    //latLongList.(obj.optString("PO_LAT"));

                }

                if (!obj.optString("PO_LONG").equalsIgnoreCase("null") && obj.optString("PO_LONG") != null && !obj.optString("PO_LONG").equalsIgnoreCase("")) {
                    caseMapDetails.setPoLong(obj.optString("PO_LONG"));
                    //latLongList.add(obj.optString("PO_LONG"));
                }

                if (!obj.optString("COLOR_CODE").equalsIgnoreCase("null"))
                    caseMapDetails.setColorCode(obj.optString("COLOR_CODE"));

                mapAllList.add(caseMapDetails);


                if(!obj.optString("PO_LAT").equalsIgnoreCase("") && !obj.optString("PO_LAT").equalsIgnoreCase("null") && obj.optString("PO_LAT") != null){
                    latLongDistance.setLatitude(obj.optString("PO_LAT"));
                    //Log.e("Lat- CaseSearch",obj.optString("PO_LAT"));
                    mapCount++;

                }

                latLongList.add(latLongDistance);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }



        if(mapCount > 0){

            Intent intent = new Intent(DisposalCaseSearchResultActivity.this, DisposalMapSearchDetailsActivity.class);
            intent.putExtra("keysMap", keyMap_parse);
            intent.putExtra("valueMap", valueMap_parse);
            intent.putExtra("totalResultMap", totlaResultMap);
            intent.putExtra("pagenoMap", pagenoMap);
            intent.putExtra("MAPCOUNT",mapCount);

            // when huge data need to intent using parcellable can not possible, required setter getter class in Application class
            kpFaceDetectionApplication.setCaseMapAllList(mapAllList);

            startActivity(intent);
        }
        else{
            Log.e("Latlong list Size: ",latLongList.size()+"");
            showAlertDialog(DisposalCaseSearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, "Sorry! Location not avaialable", false);
        }
    }


    @Override
    public void mapIconChange(int pos, String po_lat, String po_long) {
        caseSearchDetailsList.get(pos).setPoLat(po_lat);
        caseSearchDetailsList.get(pos).setPoLong(po_long);
        caseSearchAdapter.notifyDataSetChanged();
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }


    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
