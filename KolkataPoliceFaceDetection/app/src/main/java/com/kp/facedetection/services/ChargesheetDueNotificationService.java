package com.kp.facedetection.services;


import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.Provider;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by DAT-Asset-128 on 05-12-2017.
 */

public class ChargesheetDueNotificationService extends Service {
    Intent intent;
    Timer timer;
    String user_id="";
    @Override
    public void onCreate() {
        super.onCreate();
        L.e("Service onCreate callerd---------------");
        intent=new Intent();

        user_id=Utility.getUserInfo(this).getUserId();

       // callChargesheetNotificationUpdate();

    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        L.e("Service onStartCommand callerd---------------");
        callChargesheetNotificationUpdate();

        return START_STICKY;
    }
    public void  callChargesheetNotificationUpdate(){

        final long period = 5000;
        timer=new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                // do your task here
                try{
                    //callNetworkOperation();

                    String update_chargesheet_notification = Constants.API_URL + Constants.METHOD_CHARGESHEET_NOTIFICATION_UPDATE ;

                    URL url = new URL(update_chargesheet_notification);

                    JSONObject postDataParams = new JSONObject();
                    postDataParams.put("user_id",user_id);
                    Log.e("params", postDataParams.toString());

                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(30000 /* milliseconds */);
                    conn.setConnectTimeout(30000 /* milliseconds */);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);

                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(getPostDataString(postDataParams));

                    writer.flush();
                    writer.close();
                    os.close();

                    int responseCode=conn.getResponseCode();

                    if (responseCode == HttpsURLConnection.HTTP_OK) {

                        String readStream = readStream(conn.getInputStream());
                        L.e("ChargeSheet NotificationCount response------- "+readStream);
                        JSONObject jsonObj = new JSONObject(readStream);
                        String notificationValue="";
                        if(jsonObj.optString("result")!=null && !jsonObj.optString("result").equalsIgnoreCase("")&& !jsonObj.optString("result").equalsIgnoreCase("null"))
                                notificationValue=jsonObj.optString("result");
                                 //notificationValue="0";
                        else
                               notificationValue="0";

                        //System.out.println("Response "+readStream);

                        // parseAppInfoResponse(readStream);
                        intent=new Intent();
                        intent.setAction("com.kp.facedetection.ChargeSheetDueNotificationUpdate");
                        intent.putExtra("data",notificationValue);
                        sendBroadcast(intent);

                    }
                    else {
                        L.e("ChargeSheet NotificationCount response failed------- "+responseCode);
                    }

                }catch(Exception e){
                    L.e(e);
                }
            }
        }, 0, period);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        stopForeground(true);
        timer.cancel();


    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    public String readStream(InputStream in) {

        StringBuilder sb = new StringBuilder();

        try {
            Reader reader  = new InputStreamReader(in);
            int data = reader.read();
            while (data != -1) {
                char theChar = (char) data;
                data = reader.read();
                sb.append(theChar);

            }

            reader.close();
        }
        catch (Exception e) {

        }
        return sb.toString();
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }



}
