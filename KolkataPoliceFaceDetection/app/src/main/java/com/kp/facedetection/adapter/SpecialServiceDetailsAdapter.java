package com.kp.facedetection.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnPartialDisconnectionListener;
import com.kp.facedetection.interfaces.OnSpecialServiceDesignatedActionClickListener;
import com.kp.facedetection.model.Detail;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by user on 27-03-2018.
 */

public class SpecialServiceDetailsAdapter extends RecyclerView.Adapter<SpecialServiceDetailsAdapter.ViewHolder> {
    private List<Detail> detailsList;
    Context context;
    String requestType = "";
    boolean partialCancelflag;
    boolean allDisconnectionFlag;
    String nodalApprovalDone="";
    OnPartialDisconnectionListener onPartialDisconnectionListener;
    OnSpecialServiceDesignatedActionClickListener onSpecialServiceDesignatedActionClickListener;

    public SpecialServiceDetailsAdapter(Context context, List<Detail> detailsList, String requestType,boolean partialCancelflag,boolean allDisconnectionFlag,String nodalApprovalDone) {
        this.context = context;
        this.detailsList = detailsList;
        this.requestType = requestType;
        this.allDisconnectionFlag=allDisconnectionFlag;
        this.partialCancelflag=partialCancelflag;
        this.nodalApprovalDone=nodalApprovalDone;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.special_service_details_all_item_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }
    public void setOnPartialDisconnectionListener(OnPartialDisconnectionListener onPartialDisconnectionListener){
        this.onPartialDisconnectionListener=onPartialDisconnectionListener;

    }
    public void setOnSpecialServiceDesignatedActionClickListener(OnSpecialServiceDesignatedActionClickListener onSpecialServiceDesignatedActionClickListener){
        this.onSpecialServiceDesignatedActionClickListener=onSpecialServiceDesignatedActionClickListener;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Log.e("DAPL","RquestType:"+requestType+"record:"+detailsList.get(position).getMobileNumber()+","+detailsList.get(position).getLoggerNumber()+","+ detailsList.get(position).getOfficerPhone());
        Constants.changefonts(holder.phone_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.circle_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.imi_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.fromDate_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.toDate_TV_label, context, "Calibri Bold.ttf");
      //  Constants.changefonts(holder.ip_TV_label, context, "Calibri Bold.ttf");
      //  Constants.changefonts(holder.date_TV_label, context, "Calibri Bold.ttf");
      //  Constants.changefonts(holder.time_TV_label, context, "Calibri Bold.ttf");

        Constants.changefonts(holder.phone_TV_label_LI, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.logger_TV_label_LI, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.officerNo_TV_label_LI, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.officerName_TV_label_LI, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.subgroup_TV_label_LI, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.sdrname_TV_label_LI, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.sdraddress_TV_label_LI, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.sdrprovider_TV_label_LI, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.sdrcircle_TV_label_LI, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.plantype_TV_label_LI, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.partial_diconnection_text, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.phone_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.circle_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.imi_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.fromDate_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.toDate_TV, context, "Calibri.ttf");

        Constants.changefonts(holder.ip_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.date_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.time_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.phone_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.phone_TV_LI, context, "Calibri.ttf");
        Constants.changefonts(holder.logger_TV_LI, context, "Calibri.ttf");
        Constants.changefonts(holder.officerNo_TV_LI, context, "Calibri.ttf");
        Constants.changefonts(holder.officerName_TV_LI, context, "Calibri.ttf");
        Constants.changefonts(holder.subgroup_TV_LI, context, "Calibri.ttf");
        Constants.changefonts(holder.sdraddress_TV_LI, context, "Calibri.ttf");
        Constants.changefonts(holder.sdrprovider_TV_LI, context, "Calibri.ttf");
        Constants.changefonts(holder.sdrcircle_TV_LI, context, "Calibri.ttf");
        Constants.changefonts(holder.plantype_TV_LI, context, "Calibri.ttf");

        Constants.changefonts(holder.cellDump_cell_id_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.cellDump_provider_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.cellDump_circle_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.cellDump_cell_id_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.cellDump_provider_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.cellDump_circle_TV, context, "Calibri.ttf");

        if (requestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[0])|| requestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[11])) {
            holder.ll_special_service_all_item.setVisibility(View.VISIBLE);
            holder.ll_phone_holder.setVisibility(View.VISIBLE);
            if(requestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[0])) {
                holder.ll_circle_holder.setVisibility(View.VISIBLE);
            }
            holder.ll_date_holder.setVisibility(View.VISIBLE);
            if(detailsList.get(position).getPartialReplyFlag().equalsIgnoreCase("1")){
                holder.Iv_partial_replied.setVisibility(View.VISIBLE);
            }
            else{
                holder.Iv_partial_replied.setVisibility(View.GONE);
            }
            holder.phone_TV.setText(detailsList.get(position).getMobileNumber());
            if(requestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[0])) {
                holder.circle_TV.setText(detailsList.get(position).getCircle());
            }
            holder.fromDate_TV.setText(detailsList.get(position).getFromDate());
            holder.toDate_TV.setText(detailsList.get(position).getToDate());

        } else if (requestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[1])|| requestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[10])) {
            holder.ll_special_service_all_item.setVisibility(View.VISIBLE);
            holder.ll_phone_holder.setVisibility(View.VISIBLE);
            holder.phone_TV.setText(detailsList.get(position).getMobileNumber());
            if(detailsList.get(position).getPartialReplyFlag().equalsIgnoreCase("1")){
                holder.Iv_partial_replied.setVisibility(View.VISIBLE);
            }
            else{
                holder.Iv_partial_replied.setVisibility(View.GONE);
            }
        } else if (requestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[2])) {
            holder.ll_special_service_all_item.setVisibility(View.VISIBLE);
            holder.ll_imi_holder.setVisibility(View.VISIBLE);
            holder.ll_circle_holder.setVisibility(View.VISIBLE);
            holder.ll_date_holder.setVisibility(View.VISIBLE);
            holder.imi_TV.setText(detailsList.get(position).getImeiNumber());
            holder.circle_TV.setText(detailsList.get(position).getCircle());
            holder.fromDate_TV.setText(detailsList.get(position).getFromDate());
            holder.toDate_TV.setText(detailsList.get(position).getToDate());
            if(detailsList.get(position).getPartialReplyFlag().equalsIgnoreCase("1")){
                holder.Iv_partial_replied.setVisibility(View.VISIBLE);
            }
            else{
                holder.Iv_partial_replied.setVisibility(View.GONE);
            }

        } else if (requestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[3])) {
            holder.ll_special_service_all_item.setVisibility(View.VISIBLE);
            holder.ll_phone_holder.setVisibility(View.VISIBLE);
            holder.phone_TV.setText(detailsList.get(position).getMobileNumber());
            if(detailsList.get(position).getPartialReplyFlag().equalsIgnoreCase("1")){
                holder.Iv_partial_replied.setVisibility(View.VISIBLE);
            }
            else{
                holder.Iv_partial_replied.setVisibility(View.GONE);
            }
        } else if (requestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[4])) {
            holder.ll_special_service_all_item.setVisibility(View.VISIBLE);
            holder.ll_phone_holder.setVisibility(View.VISIBLE);
            holder.ll_date_holder.setVisibility(View.VISIBLE);
            holder.phone_TV.setText(detailsList.get(position).getMobileNumber());
            holder.fromDate_TV.setText(detailsList.get(position).getFromDate());
            holder.toDate_TV.setText(detailsList.get(position).getToDate());
            if(detailsList.get(position).getPartialReplyFlag().equalsIgnoreCase("1")){
                holder.Iv_partial_replied.setVisibility(View.VISIBLE);
            }
            else{
                holder.Iv_partial_replied.setVisibility(View.GONE);
            }

        } else if (requestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[5])) {
            holder.ll_special_service_all_item.setVisibility(View.VISIBLE);
            holder.ll_phone_holder.setVisibility(View.VISIBLE);
            holder.ll_date_holder.setVisibility(View.VISIBLE);
            holder.phone_TV.setText(detailsList.get(position).getMobileNumber());
            holder.fromDate_TV.setText(detailsList.get(position).getFromDate());
            holder.toDate_TV.setText(detailsList.get(position).getToDate());
            if(detailsList.get(position).getPartialReplyFlag().equalsIgnoreCase("1")){
                holder.Iv_partial_replied.setVisibility(View.VISIBLE);
            }
            else{
                holder.Iv_partial_replied.setVisibility(View.GONE);
            }

        } else if (requestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[7])) {
            holder.ll_special_service_all_item.setVisibility(View.VISIBLE);
            holder.ll_phone_holder.setVisibility(View.VISIBLE);
            holder.phone_TV.setText(detailsList.get(position).getMobileNumber());
            if(detailsList.get(position).getPartialReplyFlag().equalsIgnoreCase("1")){
                holder.Iv_partial_replied.setVisibility(View.VISIBLE);
            }
            else{
                holder.Iv_partial_replied.setVisibility(View.GONE);
            }
        }
        else if(requestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[6])){
            holder.ll_special_service_all_item.setVisibility(View.VISIBLE);
            holder.ll_CallDump_holder.setVisibility(View.VISIBLE);
            holder.ll_cellId_holder.setVisibility(View.VISIBLE);
            holder.cellDump_cell_id_TV.setText(detailsList.get(position).getCellId());
            holder.cellDump_provider_TV.setText(detailsList.get(position).getProvider());
            holder.cellDump_circle_TV.setText(detailsList.get(position).getCircle());

        }
        else if(requestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[12])){
            holder.ll_special_service_all_item.setVisibility(View.VISIBLE);
            holder.ll_CallDump_holder.setVisibility(View.VISIBLE);
            holder.cellDump_provider_TV.setText(detailsList.get(position).getProvider());
            holder.cellDump_circle_TV.setText(detailsList.get(position).getCircle());

        }
        else if(requestType.equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[9])){
           if(Utility.getUserInfo(context).getUserRank().equals("DESIGNATED OFFICER")&&(detailsList.get(position).getEnableDisableDesignated().equals("0") && detailsList.get(position).getParallalConnectionDesignated().equals("0") && detailsList.get(position).getReConnectionDesignated().equals("0"))){ //DESIGNATED OFFICER
             // detailsList.get(position).
               if(nodalApprovalDone.equalsIgnoreCase("1")) {
                   holder.Iv_designated_action.setVisibility(View.VISIBLE);
               }
               else{
                   holder.Iv_designated_action.setVisibility(View.GONE);
               }
            }
            if(Utility.getUserInfo(context).getUserRank().equals("DESIGNATED OFFICER")||Utility.getUserInfo(context).getUserRank().equals("NODAL OFFICER")){
                holder.Iv_info.setVisibility(View.VISIBLE);
            }
            holder.ll_LI_record.setVisibility(View.VISIBLE);
            if(detailsList.get(position).getMobileNumber().equalsIgnoreCase("") && !detailsList.get(position).getImeiNumber().equalsIgnoreCase("")){
                holder.ll_LI_imi.setVisibility(View.VISIBLE);
                holder.ll_LI_phone.setVisibility(View.GONE);
                holder.imi_TV_LI.setText(detailsList.get(position).getImeiNumber());

            }
            else if(!detailsList.get(position).getMobileNumber().equalsIgnoreCase("") && detailsList.get(position).getImeiNumber().equalsIgnoreCase("")){
                holder.ll_LI_imi.setVisibility(View.GONE);
                holder.ll_LI_phone.setVisibility(View.VISIBLE);
                holder.phone_TV_LI.setText(detailsList.get(position).getMobileNumber());
            }
            if (partialCancelflag){
                holder.RL_partialReconnection.setVisibility(View.VISIBLE);

                holder.partial_diconnection_text.setText("select the request number");
            }
            else {
                holder.RL_partialReconnection.setVisibility(View.GONE);

            }
            if(allDisconnectionFlag){
                holder.Cb_partial_diconnection.setChecked(true);
            }
            else
            {
                holder.Cb_partial_diconnection.setChecked(false);
            }
            holder.phone_TV_LI.setText(detailsList.get(position).getMobileNumber());
            holder.logger_TV_LI.setText(detailsList.get(position).getLoggerNumber());
            holder.officerNo_TV_LI.setText(detailsList.get(position).getOfficerPhone());
            holder.officerName_TV_LI.setText(detailsList.get(position).getOfficerName());
            holder.subgroup_TV_LI.setText(detailsList.get(position).getLoggerSubGroup());
            holder.sdrname_TV_LI.setText(detailsList.get(position).getLoggerSdrHolder());
            holder.sdraddress_TV_LI.setText(detailsList.get(position).getLoggerSdrAddress());
            holder.sdrprovider_TV_LI.setText(detailsList.get(position).getLoggerSdrProvider());
            holder.sdrcircle_TV_LI.setText(detailsList.get(position).getLoggerSdrCircle());
            holder.plantype_TV_LI.setText(detailsList.get(position).getLoggerSdrPlanType());
            if(detailsList.get(position).getPartialReplyFlag().equalsIgnoreCase("1")){
                holder.Iv_partial_replied_LI.setVisibility(View.VISIBLE);
            }
            else{
                holder.Iv_partial_replied_LI.setVisibility(View.GONE);
            }

           // holder.phone_TV.setText(detailsList.get(position).getMobileNumber());

        }

    }

    @Override
    public int getItemCount() {
        return (detailsList != null ? detailsList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView phone_TV, imi_TV, circle_TV, fromDate_TV, toDate_TV;
        TextView phone_TV_label, imi_TV_label, circle_TV_label, fromDate_TV_label, toDate_TV_label;
        TextView cellDump_cell_id_TV,cellDump_provider_TV,cellDump_circle_TV;
        TextView cellDump_cell_id_TV_label,cellDump_provider_TV_label,cellDump_circle_TV_label;
        TextView ip_TV, date_TV, time_TV;
        TextView ip_TV_label, date_TV_label, time_TV_label;
        LinearLayout ll_CallDump_holder,ll_cellId_holder;
        LinearLayout ll_special_service_all_item, ll_phone_holder, ll_imi_holder, ll_circle_holder, ll_date_holder;
        LinearLayout ll_special_service_ip_item;
        LinearLayout ll_LI_record, ll_LI_phone, ll_LI_imi;
        TextView phone_TV_label_LI, imi_TV_label_LI, logger_TV_label_LI, officerNo_TV_label_LI, officerName_TV_label_LI, subgroup_TV_label_LI, sdrname_TV_label_LI, sdraddress_TV_label_LI, sdrprovider_TV_label_LI, sdrcircle_TV_label_LI, plantype_TV_label_LI;
        TextView phone_TV_LI, imi_TV_LI, logger_TV_LI, officerNo_TV_LI, officerName_TV_LI, subgroup_TV_LI, sdrname_TV_LI, sdraddress_TV_LI, sdrprovider_TV_LI, sdrcircle_TV_LI, plantype_TV_LI;
        RelativeLayout RL_partialReconnection;
        RelativeLayout Rl_action;
         TextView partial_diconnection_text;
        CheckBox  Cb_partial_diconnection;
        ImageView Iv_partial_replied,Iv_partial_replied_ip,Iv_partial_replied_LI,Iv_designated_action,Iv_info;
        public ViewHolder(View itemView) {
            super(itemView);
            RL_partialReconnection=(RelativeLayout) itemView.findViewById(R.id.RL_partialReconnection);
            Iv_partial_replied=(ImageView) itemView.findViewById(R.id.Iv_partial_replied);
            Iv_partial_replied_ip=(ImageView) itemView.findViewById(R.id.Iv_partial_replied_ip);
            Iv_partial_replied_LI=(ImageView) itemView.findViewById(R.id.Iv_partial_replied_LI);
            Iv_designated_action=(ImageView) itemView.findViewById(R.id.Iv_designated_action);
            Iv_designated_action.setOnClickListener(this);
            Iv_info=(ImageView) itemView.findViewById(R.id.Iv_info);
            Iv_info.setOnClickListener(this);

            Cb_partial_diconnection=(CheckBox)itemView.findViewById(R.id.Cb_partial_diconnection);
            ll_special_service_all_item = (LinearLayout) itemView.findViewById(R.id.ll_special_service_all_item);
            ll_phone_holder = (LinearLayout) itemView.findViewById(R.id.ll_phone_holder);
            ll_imi_holder = (LinearLayout) itemView.findViewById(R.id.ll_imi_holder);
            ll_circle_holder = (LinearLayout) itemView.findViewById(R.id.ll_circle_holder);
            ll_date_holder = (LinearLayout) itemView.findViewById(R.id.ll_date_holder);
            ll_special_service_ip_item = (LinearLayout) itemView.findViewById(R.id.ll_special_service_ip_item);
            phone_TV_label = (TextView) itemView.findViewById(R.id.phone_TV_label);
            imi_TV_label = (TextView) itemView.findViewById(R.id.imi_TV_label);
            circle_TV_label = (TextView) itemView.findViewById(R.id.circle_TV_label);
            fromDate_TV_label = (TextView) itemView.findViewById(R.id.fromDate_TV_label);
            toDate_TV_label = (TextView) itemView.findViewById(R.id.toDate_TV_label);
            phone_TV = (TextView) itemView.findViewById(R.id.phone_TV);
            imi_TV = (TextView) itemView.findViewById(R.id.imi_TV);
            circle_TV = (TextView) itemView.findViewById(R.id.circle_TV);
            fromDate_TV = (TextView) itemView.findViewById(R.id.fromDate_TV);
            toDate_TV = (TextView) itemView.findViewById(R.id.toDate_TV);
            ip_TV = (TextView) itemView.findViewById(R.id.ip_TV);
            date_TV = (TextView) itemView.findViewById(R.id.date_TV);
            time_TV = (TextView) itemView.findViewById(R.id.time_TV);
            ll_LI_record = (LinearLayout) itemView.findViewById(R.id.ll_LI_record);
            ll_LI_phone = (LinearLayout) itemView.findViewById(R.id.ll_LI_phone);
            ll_LI_imi = (LinearLayout) itemView.findViewById(R.id.ll_LI_imi);

            phone_TV_label_LI = (TextView) itemView.findViewById(R.id.phone_TV_label_LI);
            imi_TV_label_LI = (TextView) itemView.findViewById(R.id.imi_TV_label_LI);
            logger_TV_label_LI = (TextView) itemView.findViewById(R.id.logger_TV_label_LI);
            officerNo_TV_label_LI=(TextView) itemView.findViewById(R.id.officerNo_TV_label_LI);
            officerName_TV_label_LI=(TextView) itemView.findViewById(R.id.officerName_TV_label_LI);
            subgroup_TV_label_LI=(TextView) itemView.findViewById(R.id.subgroup_TV_label_LI);
            sdrname_TV_label_LI=(TextView) itemView.findViewById(R.id.sdrname_TV_label_LI);
            sdraddress_TV_label_LI=(TextView) itemView.findViewById(R.id.sdraddress_TV_label_LI);
            sdrprovider_TV_label_LI=(TextView) itemView.findViewById(R.id.sdrprovider_TV_label_LI);
            sdrcircle_TV_label_LI=(TextView) itemView.findViewById(R.id.sdrcircle_TV_label_LI);
            plantype_TV_label_LI=(TextView) itemView.findViewById(R.id.plantype_TV_label_LI);

            phone_TV_LI = (TextView) itemView.findViewById(R.id.phone_TV_LI);
            imi_TV_LI = (TextView) itemView.findViewById(R.id.imi_TV_LI);
            logger_TV_LI = (TextView) itemView.findViewById(R.id.logger_TV_LI);
            officerNo_TV_LI=(TextView) itemView.findViewById(R.id.officerNo_TV_LI);
            officerName_TV_LI=(TextView) itemView.findViewById(R.id.officerName_TV_LI);
            subgroup_TV_LI=(TextView) itemView.findViewById(R.id.subgroup_TV_LI);
            sdrname_TV_LI=(TextView) itemView.findViewById(R.id.sdrname_TV_LI);
            sdraddress_TV_LI=(TextView) itemView.findViewById(R.id.sdraddress_TV_LI);
            sdrprovider_TV_LI=(TextView) itemView.findViewById(R.id.sdrprovider_TV_LI);
            sdrcircle_TV_LI=(TextView) itemView.findViewById(R.id.sdrcircle_TV_LI);
            plantype_TV_LI=(TextView) itemView.findViewById(R.id.plantype_TV_LI);
            partial_diconnection_text=(TextView)itemView.findViewById(R.id.partial_diconnection_text);
            Cb_partial_diconnection.setOnClickListener(this);
            ll_CallDump_holder=(LinearLayout)itemView.findViewById(R.id.ll_CallDump_holder);
            ll_cellId_holder=(LinearLayout)itemView.findViewById(R.id.ll_cellId_holder);
            cellDump_cell_id_TV_label=(TextView)itemView.findViewById(R.id.TV_label_cell_id);
            cellDump_provider_TV_label=(TextView)itemView.findViewById(R.id.TV_label_provider);
            cellDump_circle_TV_label=(TextView)itemView.findViewById(R.id.TV_label_circle);
            cellDump_cell_id_TV=(TextView)itemView.findViewById(R.id.TV_cell_id);
            cellDump_provider_TV=(TextView)itemView.findViewById(R.id.TV_provider);
            cellDump_circle_TV=(TextView)itemView.findViewById(R.id.TV_circle);


        }

        @Override
        public void onClick(View v) {
             int id=v.getId();
            switch(id){
                case R.id.Cb_partial_diconnection:
                    boolean itemFlag;
                    if(Cb_partial_diconnection.isChecked()) {
                        itemFlag=true;
                        onPartialDisconnectionListener.OnPartialDisconnectionItemClick(getLayoutPosition(),itemFlag);
                    }
                    else{
                        itemFlag=false;
                        onPartialDisconnectionListener.OnPartialDisconnectionItemClick(getLayoutPosition(),itemFlag);
                       // Toast.makeText(context, "Position:" + getLayoutPosition()+"unchecked", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.Iv_designated_action:
                    final AlertDialog.Builder alertadd = new AlertDialog.Builder(context,R.style.CustomDialogTheme);
                    LayoutInflater factory = LayoutInflater.from(context);
                    final View view = factory.inflate(R.layout.special_service_details_activity_dialog, null);
                    ImageView imageViewparallel=(ImageView)view.findViewById(R.id.Iv_parallal);
                    ImageView imageViewrevoke=(ImageView)view.findViewById(R.id.Iv_partial_revoke);
                    ImageView imageViewreconnection=(ImageView)view.findViewById(R.id.Iv_reconnection);

                    alertadd.setView(view);
                   final Dialog dialog1=alertadd.create();
                    imageViewparallel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onSpecialServiceDesignatedActionClickListener.onSpecialServiceDesignatedItemClick(getLayoutPosition(),"parallalconnectivity");
                            dialog1.dismiss();
                        }
                    });
                    imageViewrevoke.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onSpecialServiceDesignatedActionClickListener.onSpecialServiceDesignatedItemClick(getLayoutPosition(),"partlyrevoked");
                            dialog1.dismiss();
                        }
                    });
                    imageViewreconnection.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onSpecialServiceDesignatedActionClickListener.onSpecialServiceDesignatedItemClick(getLayoutPosition(),"reconnectivity");
                            dialog1.dismiss();
                        }
                    });
                    dialog1.show();

                   break;
                case R.id.Iv_info:
                   // Toast.makeText(context, "Position:"+getLayoutPosition(), Toast.LENGTH_SHORT).show();
                    final AlertDialog.Builder alertaddinfo = new AlertDialog.Builder(context,R.style.CustomDialogTheme);
                   // alertaddinfo.setTitle("APP DATA HISTORY");
                    LayoutInflater factoryinfo = LayoutInflater.from(context);
                    final View viewInfo = factoryinfo.inflate(R.layout.special_service_request_item_observation_layout, null);
                    RecyclerView rvTest = (RecyclerView) viewInfo.findViewById(R.id.Rv_sp_service_appdata);
                    TextView norecord=(TextView)viewInfo.findViewById(R.id.Tv_norecord);
                    alertaddinfo.setView(viewInfo);
                    final Dialog dialog=alertaddinfo.create();
                    /*Dialog dialog = new Dialog(context);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.WHITE));
                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    dialog.setContentView(R.layout.special_service_request_item_observation_layout);
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.setCancelable(true);*/

                    if(detailsList.get(getLayoutPosition()).getChkNoAppList().size()>0) {
                        norecord.setVisibility(View.GONE);
                        rvTest.setVisibility(View.VISIBLE);
                        rvTest.setHasFixedSize(true);
                        rvTest.setLayoutManager(new LinearLayoutManager(context));
                        SpecialServiceLIObserationAdapter specialServiceLIStatusChangeAdapter = new SpecialServiceLIObserationAdapter(context, detailsList.get(getLayoutPosition()).getChkNoAppList());
                        rvTest.setAdapter(specialServiceLIStatusChangeAdapter);
                    }else{
                        norecord.setVisibility(View.VISIBLE);
                        rvTest.setVisibility(View.GONE);
                    }
                    dialog.show();
                    break;



            }
        }
    }
}
