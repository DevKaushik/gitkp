package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kp.facedetection.adapter.DeailRecylcerAdapterCP;
import com.kp.facedetection.adapter.DetailRecylcerAdapter;
import com.kp.facedetection.adapter.DetailRecylcerAdapterAC;
import com.kp.facedetection.adapter.DetailRecylcerAdapterADDLCP;
import com.kp.facedetection.adapter.DetailRecylcerAdapterDC;
import com.kp.facedetection.adapter.DetailRecylcerAdapterJTCP;
import com.kp.facedetection.adapter.DetailRecylcerAdapterOC;
import com.kp.facedetection.databinding.ActivitySpecialServiceListBinding;
import com.kp.facedetection.interfaces.OnItemClickListenerForAC;
import com.kp.facedetection.interfaces.OnItemClickListenerForADDLCP;
import com.kp.facedetection.interfaces.OnItemClickListenerForCP;
import com.kp.facedetection.interfaces.OnItemClickListenerForDC;
import com.kp.facedetection.interfaces.OnItemClickListenerForIO;
import com.kp.facedetection.interfaces.OnItemClickListenerForJTCP;
import com.kp.facedetection.interfaces.OnItemClickListenerForOC;
import com.kp.facedetection.interfaces.OnItemClickListenerforLIPartialSaveAC;
import com.kp.facedetection.interfaces.OnItemClickListenerforLIPartialSaveADDLCP;
import com.kp.facedetection.interfaces.OnItemClickListenerforLIPartialSaveCP;
import com.kp.facedetection.interfaces.OnItemClickListenerforLIPartialSaveDC;
import com.kp.facedetection.interfaces.OnItemClickListenerforLIPartialSaveIO;
import com.kp.facedetection.interfaces.OnItemClickListenerforLIPartialSaveJTCP;
import com.kp.facedetection.interfaces.OnItemClickListenerforLIPartialSaveOC;
import com.kp.facedetection.model.AC;
import com.kp.facedetection.model.ADDLCP;
import com.kp.facedetection.model.CP;
import com.kp.facedetection.model.DC;
import com.kp.facedetection.model.Example;
import com.kp.facedetection.model.IO;
import com.kp.facedetection.model.JTCP;
import com.kp.facedetection.model.OC;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONObject;

import java.io.IOException;

public class
SpecialServiceList extends BaseActivity implements View.OnClickListener,OnItemClickListenerForIO,OnItemClickListenerForOC,OnItemClickListenerForDC,OnItemClickListenerForAC,OnItemClickListenerForJTCP,OnItemClickListenerForADDLCP,OnItemClickListenerForCP,OnItemClickListenerforLIPartialSaveIO,OnItemClickListenerforLIPartialSaveOC,OnItemClickListenerforLIPartialSaveAC,OnItemClickListenerforLIPartialSaveDC,OnItemClickListenerforLIPartialSaveJTCP,OnItemClickListenerforLIPartialSaveADDLCP,OnItemClickListenerforLIPartialSaveCP {
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";
    TextView chargesheetNotificationCount;
    String notificationCount="";
    private ActivitySpecialServiceListBinding activitySpecialServiceListBinding;
    private RecyclerView.LayoutManager mLayoutManagerIo;
    private RecyclerView.LayoutManager mLayoutManagerOc;
    private RecyclerView.LayoutManager mLayoutManagerAc;
    private RecyclerView.LayoutManager mLayoutManagerDc;
    private RecyclerView.LayoutManager mLayoutManagerJtcp;
    private RecyclerView.LayoutManager mLayoutManagerAddlcp;
    private RecyclerView.LayoutManager mLayoutManagerCp;
    Example modelService = null;
    private boolean doubleBackToExit;
    public static int REQUEST_CODE=200;
    String search_request_type="",search_from_date="",search_to_date="";
    boolean fromSearchPage=false;
    JSONObject SVCAllowuserListJSONObj;
    String SVCAllowuserListString="";

    @Override
    protected void onResume() {
        super.onResume();
       // Toast.makeText(this, "onResume called", Toast.LENGTH_SHORT).show();
        if (fromSearchPage){
            if (!search_request_type.isEmpty() && !search_from_date.isEmpty() && !search_to_date.isEmpty()) {
                activitySpecialServiceListBinding.ivReset.setVisibility(View.VISIBLE);
                activitySpecialServiceListBinding.ivReset.setOnClickListener(this);
                getFilteredList();
            } else {
                activitySpecialServiceListBinding.ivReset.setVisibility(View.GONE);
                callGetServicesApi();
            }


        } else {

            activitySpecialServiceListBinding.ivReset.setVisibility(View.GONE);
            callGetServicesApi();

        }
    }
    @Override
    public void onBackPressed(){
        if (doubleBackToExit) {
            Intent it=new Intent(this,DashboardActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK );
            startActivity(it);
           // super.onBackPressed();
            return;
        }
        Toast.makeText(this, "Double tap to exit, On exit you have to do the authentication" , Toast.LENGTH_SHORT).show();
        this.doubleBackToExit = true;
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExit = false;
            }
        }, 2000);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySpecialServiceListBinding = DataBindingUtil.setContentView(this, R.layout.activity_special_service_list);
        setToolBar();
        initViews();

       // callGetServicesApi();
    }
    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }
    private void initViews(){

        mLayoutManagerIo = new LinearLayoutManager(this);
        activitySpecialServiceListBinding.ioRV.setLayoutManager(mLayoutManagerIo);
        activitySpecialServiceListBinding.ioTV.setOnClickListener(this);
        activitySpecialServiceListBinding.ioRV.setOnClickListener(this);
        mLayoutManagerOc = new LinearLayoutManager(this);
        activitySpecialServiceListBinding.ocRV.setLayoutManager(mLayoutManagerOc);
        activitySpecialServiceListBinding.ocTV.setOnClickListener(this);
        activitySpecialServiceListBinding.ocRV.setOnClickListener(this);
        mLayoutManagerAc = new LinearLayoutManager(this);
        activitySpecialServiceListBinding.acRV.setLayoutManager(mLayoutManagerAc);
        mLayoutManagerDc = new LinearLayoutManager(this);
        activitySpecialServiceListBinding.dcRV.setLayoutManager(mLayoutManagerDc);
        activitySpecialServiceListBinding.dcTV.setOnClickListener(this);
        activitySpecialServiceListBinding.dcRV.setOnClickListener(this);

        mLayoutManagerJtcp = new LinearLayoutManager(this);
        activitySpecialServiceListBinding.jtcpRV.setLayoutManager(mLayoutManagerJtcp);
        activitySpecialServiceListBinding.jtcpRV.setOnClickListener(this);
        activitySpecialServiceListBinding.jtcpRV.setOnClickListener(this);
        mLayoutManagerAddlcp = new LinearLayoutManager(this);
        activitySpecialServiceListBinding.addlCpRV.setLayoutManager(mLayoutManagerAddlcp);
        activitySpecialServiceListBinding.addlCpRV.setOnClickListener(this);
        mLayoutManagerCp = new LinearLayoutManager(this);
        activitySpecialServiceListBinding.CpRV.setLayoutManager(mLayoutManagerCp);
        activitySpecialServiceListBinding.CpRV.setOnClickListener(this);
        activitySpecialServiceListBinding.ivRefresh.setOnClickListener(this);

        activitySpecialServiceListBinding.bttnAddNewRequest.setOnClickListener(this);
        activitySpecialServiceListBinding.ivCustomizeSearch.setOnClickListener(this);

    }
    public void svcallowuserlist(){
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_SPECIAL_SERVICE_ALLOW_USER_LIST);
        taskManager.setSpecilServiceAllowUserList(true);
        String[] keys = {"user_id"};
        String[] values = {/*Utility.getUserInfo(this).getUserId()*/"1080"};
        taskManager.doStartTask(keys, values, true, true);
    }
    public void parseSpecialServiceAllowUserListResult(String response){
        Log.e("DAPL", response);

        try {
            if (response != null || !response.equalsIgnoreCase("")) {
                JSONObject jobj = new JSONObject(response);
                if(jobj.optString("status").equalsIgnoreCase("1")){
                    JSONObject jsonObjectResult=jobj.optJSONObject("result");
                    if(jsonObjectResult!=null){

                        SVCAllowuserListJSONObj=jobj.optJSONObject("result");
                        SVCAllowuserListString=SVCAllowuserListJSONObj.toString();
                        callGetServicesApi();

                    }
                }
                else{
                    Utility.showAlertDialog(this,Constants.ERROR,Constants.ERROR_EXCEPTION_MSG,false);
                }
            }
            else{

                    Utility.showAlertDialog(this,Constants.ERROR,Constants.ERROR_EXCEPTION_MSG,false);

            }
        }
        catch(Exception e){

        }
    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
    private void callGetServicesApi() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_SPECIAL_SERVICE_LIST);
        taskManager.setSpecilServiceListData(true);
        String[] keys = {"user_id", "div_code", "ps_code", "rank" ,"svcAllowUserList"};
        String[] values = {Utility.getUserInfo(this).getUserId(), Utility.getUserInfo(this).getUserDivisionCode(), Utility.getUserInfo(this).getUserPscode()
                ,Utility.getUserInfo(this).getUserRank(),SVCAllowuserListString};
        taskManager.doStartTask(keys, values, true, true);

    }
    private void getFilteredList(){
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_SPECIAL_SERVICE_SEARCH_LIST);
        taskManager.setSpecilServiceSearchListData(true);
        String[] keys = {"user_id", "div_code", "ps_code", "rank","request_type" ,"from_date" ,"to_date","svcAllowUserList"};
        String[] values = {Utility.getUserInfo(this).getUserId(), Utility.getUserInfo(this).getUserDivisionCode(), Utility.getUserInfo(this).getUserPscode()
                ,Utility.getUserInfo(this).getUserRank(),search_request_type,search_from_date,search_to_date,SVCAllowuserListString};
        taskManager.doStartTask(keys, values, true, true);

    }

    public void parseSpecialServiceListResult(String response) {

        Log.e("DAPL", response);


        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            modelService = mapper.readValue(response, Example.class);
        } catch (JsonMappingException e) {
            Log.e("JAY", e+"");
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("JAY", e+"");
            e.printStackTrace();
        }
     //   Log.e("JAY", "IO:"+modelService.getIO());
        if (modelService != null) {
            if(modelService.getStatus().equalsIgnoreCase("1")) {
                activitySpecialServiceListBinding.tvNoRecord.setVisibility(View.GONE);
                if (modelService.getIO().size() > 0) {
                    //todo show IO layout
                    Log.e("JAY", "IO:" + modelService.getIO());
                    activitySpecialServiceListBinding.ioLL.setVisibility(View.VISIBLE);
                    DetailRecylcerAdapter detailRecylcerAdapter = new DetailRecylcerAdapter(this, modelService.getIO());
                    detailRecylcerAdapter.setOnItemClickListenerforLIPartialSave(this);
                    detailRecylcerAdapter.setOnItemClickListenerForIO(this);
                    activitySpecialServiceListBinding.ioRV.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
                    activitySpecialServiceListBinding.ioRV.setAdapter(detailRecylcerAdapter);


                }
                else{
                    activitySpecialServiceListBinding.ioLL.setVisibility(View.GONE);

                }

                if (modelService.getOC().size() > 0) {
                    //todo show OC layout
                    activitySpecialServiceListBinding.ocLL.setVisibility(View.VISIBLE);
                    DetailRecylcerAdapterOC detailRecylcerAdapter = new DetailRecylcerAdapterOC(this, modelService.getOC());
                    detailRecylcerAdapter.setOnItemClickListenerforLIPartialSave(this);
                    detailRecylcerAdapter.setOnItemClickListenerForOC(this);
                    activitySpecialServiceListBinding.ocRV.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
                    activitySpecialServiceListBinding.ocRV.setAdapter(detailRecylcerAdapter);
                }
                else{
                    activitySpecialServiceListBinding.ocLL.setVisibility(View.GONE);

                }

                if (modelService.getAC().size() > 0) {
                    //todo show AC layout
                    activitySpecialServiceListBinding.acLL.setVisibility(View.VISIBLE);
                    DetailRecylcerAdapterAC detailRecylcerAdapter = new DetailRecylcerAdapterAC(this, modelService.getAC());
                    detailRecylcerAdapter.setOnItemClickListenerforLIPartialSave(this);
                    detailRecylcerAdapter.setOnItemClickListenerForAc(this);
                    activitySpecialServiceListBinding.acRV.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
                    activitySpecialServiceListBinding.acRV.setAdapter(detailRecylcerAdapter);
                }  else{
                    activitySpecialServiceListBinding.acLL.setVisibility(View.GONE);

                }

                if (modelService.getDC().size() > 0) {
                    //todo show DC layout
                    activitySpecialServiceListBinding.dcLL.setVisibility(View.VISIBLE);
                    DetailRecylcerAdapterDC detailRecylcerAdapter = new DetailRecylcerAdapterDC(this, modelService.getDC());
                    detailRecylcerAdapter.setOnItemClickListenerforLIPartialSave(this);
                    detailRecylcerAdapter.setOnItemClickListenerForDC(this);
                    activitySpecialServiceListBinding.dcRV.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
                    activitySpecialServiceListBinding.dcRV.setAdapter(detailRecylcerAdapter);
                }
                else{
                    activitySpecialServiceListBinding.dcLL.setVisibility(View.GONE);

                }

                if (modelService.getJtCp().size() > 0) {
                    //todo show DC layout
                    activitySpecialServiceListBinding.jtcpLL.setVisibility(View.VISIBLE);
                    DetailRecylcerAdapterJTCP detailRecylcerAdapter = new DetailRecylcerAdapterJTCP(this, modelService.getJtCp());
                     detailRecylcerAdapter.setOnItemClickListenerforLIPartialSave(this);
                    detailRecylcerAdapter.setOnItemClickListenerForJTCP(this);
                    activitySpecialServiceListBinding.jtcpRV.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
                    activitySpecialServiceListBinding.jtcpRV.setAdapter(detailRecylcerAdapter);
                }
                else{
                    activitySpecialServiceListBinding.jtcpLL.setVisibility(View.GONE);

                }

                if (modelService.getADDLCP().size() > 0) {
                    //todo show ADDLCP layout
                    activitySpecialServiceListBinding.addlCpLL.setVisibility(View.VISIBLE);
                    DetailRecylcerAdapterADDLCP detailRecylcerAdapter = new DetailRecylcerAdapterADDLCP(this, modelService.getADDLCP());
                    detailRecylcerAdapter.setOnItemClickListenerforLIPartialSave(this);
                    detailRecylcerAdapter.setOnItemClickListenerForADDLCP(this);
                    activitySpecialServiceListBinding.addlCpRV.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
                    activitySpecialServiceListBinding.addlCpRV.setAdapter(detailRecylcerAdapter);
                }
                else{
                    activitySpecialServiceListBinding.addlCpLL.setVisibility(View.GONE);

                }

                if (modelService.getCP().size() > 0) {
                    //todo show ADDLCP layout
                    activitySpecialServiceListBinding.CpLL.setVisibility(View.VISIBLE);
                    DeailRecylcerAdapterCP detailRecylcerAdapter = new DeailRecylcerAdapterCP(this, modelService.getCP());
                    detailRecylcerAdapter.setOnItemClickListenerforLIPartialSave(this);
                    detailRecylcerAdapter.setOnItemClickListenerForCP(this);
                    activitySpecialServiceListBinding.CpRV.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
                    activitySpecialServiceListBinding.CpRV.setAdapter(detailRecylcerAdapter);
                }
                else{
                    activitySpecialServiceListBinding.CpLL.setVisibility(View.GONE);

                }

            }
            else{
                activitySpecialServiceListBinding.ioLL.setVisibility(View.GONE);
                activitySpecialServiceListBinding.ocLL.setVisibility(View.GONE);
                activitySpecialServiceListBinding.dcLL.setVisibility(View.GONE);
                activitySpecialServiceListBinding.addlCpLL.setVisibility(View.GONE);
                activitySpecialServiceListBinding.CpLL.setVisibility(View.GONE);
                activitySpecialServiceListBinding.tvNoRecord.setVisibility(View.VISIBLE);
                activitySpecialServiceListBinding.tvNoRecord.setText(Constants.ERROR_MSG_NO_RECORD);

            }

        }


    }


    @Override
    public void onClick(View v) {
        int id =v.getId();
        switch(id){
            case R.id.bttn_add_new_request:
                Intent it=new Intent(this,SpecialServiceActivity.class);
                it.putExtra("REQUEST_TYPE","NEW_REQUEST");
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it);
                break;
            case R.id.iv_refresh:
                if(!search_request_type.isEmpty() && !search_from_date.isEmpty() && !search_to_date.isEmpty() ) {
                    getFilteredList();
                }else{
                    callGetServicesApi();
                }
              /*  if(!fromSearchPage) {

                }else{
                    showAlertDialog(this,"INFO","Want to continue with search parameter",true);
                }*/

                break;
            case R.id.iv_reset:
                final android.app.AlertDialog ad = new android.app.AlertDialog.Builder(this).create();
                // Setting Dialog Title
                ad.setTitle("Info");

                // Setting Dialog Message
                ad.setMessage("Are you sure that you want to clear search filter?");

                // Setting alert dialog icon
                ad.setIcon((true) ? R.drawable.success : R.drawable.fail);


                ad.setButton(DialogInterface.BUTTON_POSITIVE,
                        "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (ad != null && ad.isShowing()) {
                                    ad.dismiss();
                                }
                                search_request_type="";
                                search_from_date="";
                                search_to_date="";
                                callGetServicesApi();
                                activitySpecialServiceListBinding.ivReset.setVisibility(View.GONE);



                            }
                        });
                ad.setButton(DialogInterface.BUTTON_NEGATIVE,
                        "CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (ad != null && ad.isShowing()) {
                                    ad.dismiss();
                                }


                            }
                        });

                // Showing Alert Message
                ad.show();
                ad.setCanceledOnTouchOutside(false);


                break;
            case R.id.iv_customize_search:
                Intent itsearch=new Intent(this,SpecialServiceSearch.class);
                startActivityForResult(itsearch,REQUEST_CODE);

        }

    }
    @Override
    protected  void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==REQUEST_CODE)
        {

            fromSearchPage=true;
                  search_request_type=data.getStringExtra("REQUEST_TYPE");
                  search_from_date=data.getStringExtra("FROM_DATE");
                  search_to_date=data.getStringExtra("TO_DATE");
                 if(!search_request_type.isEmpty() && !search_from_date.isEmpty() && !search_from_date.isEmpty() ) {
                     getFilteredList();
                 }

        }

    }


    @Override
    public void onItemClickForIO(View v, int pos) {

        IO io=modelService.getIO().get(pos);
        Intent it =new Intent(this,SpecialServiceDetailsActivity.class);
        it.putExtra("IO_DETAILS",io);
        startActivity(it);
    }

    @Override
    public void onItemClickForOC(View v, int pos) {
        OC oc=modelService.getOC().get(pos);
        Intent it =new Intent(this,SpecialServiceDetailsActivity.class);
        it.putExtra("OC_DETAILS",oc);
        startActivity(it);
    }
    @Override
    public void onItemClickForAC(View v, int pos) {
        AC ac=modelService.getAC().get(pos);
        Intent it =new Intent(this,SpecialServiceDetailsActivity.class);
        it.putExtra("AC_DETAILS",ac);
        startActivity(it);
    }
    @Override
    public void onItemClickForDC(View v, int pos) {
        DC dc=modelService.getDC().get(pos);
        Intent it =new Intent(this,SpecialServiceDetailsActivity.class);
        it.putExtra("DC_DETAILS",dc);
        startActivity(it);
    }

    @Override
    public void onItemClickForJTCP(View v, int pos) {
        JTCP jtcp=modelService.getJtCp().get(pos);
        Intent it =new Intent(this,SpecialServiceDetailsActivity.class);
        it.putExtra("JTCP_DETAILS",jtcp);
        startActivity(it);
    }

    @Override
    public void onItemClickForADDLCP(View v, int pos) {
        ADDLCP addlcp=modelService.getADDLCP().get(pos);
        Intent it =new Intent(this,SpecialServiceDetailsActivity.class);
        it.putExtra("ADDLCP_DETAILS",addlcp);
        startActivity(it);

    }

    @Override
    public void onItemClickForCP(View v, int pos) {
        //Toast.makeText(this, "CLICKED ON"+pos, Toast.LENGTH_SHORT).show();
        CP cp=modelService.getCP().get(pos);
        Intent it =new Intent(this,SpecialServiceDetailsActivity.class);
        it.putExtra("CP_DETAILS",cp);
        startActivity(it);

    }

    @Override
    public void onItemClickforLIPartialSaveIO(int position) {
         if(modelService.getIO().get(position).getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
             Intent it = new Intent(this, SpecialServiceActivity.class);
             it.putExtra("REQUEST_TYPE", "LI_PARTIAL_SAVE");
             it.putExtra("REQUEST_MAASTER_ID", modelService.getIO().get(position).getId());
             it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
             startActivity(it);
         }
         else{
             Utility.showAlertDialog(this,Constants.ERROR,"Sorry you can complete your request only",false);
         }
    }
    @Override
    public void onItemClickforLIPartialSaveOC(int position) {
        if(modelService.getOC().get(position).getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
            Intent it = new Intent(this, SpecialServiceActivity.class);
            it.putExtra("REQUEST_TYPE", "LI_PARTIAL_SAVE");
            it.putExtra("REQUEST_MAASTER_ID", modelService.getOC().get(position).getId());
            it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(it);
        }
        else{
            Utility.showAlertDialog(this,Constants.ERROR,"Sorry you can complete your request only",false);
        }
    }
    @Override
    public void onItemClickforLIPartialSaveAC(int position) {
        if(modelService.getAC().get(position).getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
            Intent it = new Intent(this, SpecialServiceActivity.class);
            it.putExtra("REQUEST_TYPE", "LI_PARTIAL_SAVE");
            it.putExtra("REQUEST_MAASTER_ID", modelService.getAC().get(position).getId());
            it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(it);
        }
        else{
            Utility.showAlertDialog(this,Constants.ERROR,"Sorry you can complete your request only",false);
        }
    }
    @Override
    public void onItemClickforLIPartialSaveDC(int position) {
        if(modelService.getDC().get(position).getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
            Intent it = new Intent(this, SpecialServiceActivity.class);
            it.putExtra("REQUEST_TYPE", "LI_PARTIAL_SAVE");
            it.putExtra("REQUEST_MAASTER_ID", modelService.getDC().get(position).getId());
            it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(it);
        }
        else{
            Utility.showAlertDialog(this,Constants.ERROR,"Sorry you can complete your request only",false);
        }
    }
    @Override
    public void onItemClickforLIPartialSaveJTCP(int position) {
        if(modelService.getJtCp().get(position).getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
            Intent it = new Intent(this, SpecialServiceActivity.class);
            it.putExtra("REQUEST_TYPE", "LI_PARTIAL_SAVE");
            it.putExtra("REQUEST_MAASTER_ID", modelService.getJtCp().get(position).getId());
            it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(it);
        }
        else{
            Utility.showAlertDialog(this,Constants.ERROR,"Sorry you can complete your request only",false);
        }

    }
    @Override
    public void onItemClickforLIPartialSaveADDLCP(int position) {
        if(modelService.getADDLCP().get(position).getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
            Intent it = new Intent(this, SpecialServiceActivity.class);
            it.putExtra("REQUEST_TYPE", "LI_PARTIAL_SAVE");
            it.putExtra("REQUEST_MAASTER_ID", modelService.getADDLCP().get(position).getId());
            it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(it);
        }
        else{
            Utility.showAlertDialog(this,Constants.ERROR,"Sorry you can complete your request only",false);
        }

    }
    @Override
    public void onItemClickforLIPartialSaveCP(int position) {
        if(modelService.getCP().get(position).getRequestUserId().equals(Utility.getUserInfo(this).getUserId())) {
            Intent it = new Intent(this, SpecialServiceActivity.class);
            it.putExtra("REQUEST_TYPE", "LI_PARTIAL_SAVE");
            it.putExtra("REQUEST_MAASTER_ID", modelService.getCP().get(position).getId());
            it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(it);
        }
        else{
            Utility.showAlertDialog(this,Constants.ERROR,"Sorry you can complete your request only",false);
        }

    }
    public void showAlertDialog(final Context context, String title, String message, final Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);


        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        getFilteredList();

                    }
                });
        ad.setButton(DialogInterface.BUTTON_NEGATIVE,
                "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        callGetServicesApi();

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


}
