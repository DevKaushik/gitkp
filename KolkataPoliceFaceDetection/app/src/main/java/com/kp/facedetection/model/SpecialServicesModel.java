package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by user on 04-04-2018.
 */

public class SpecialServicesModel  implements Serializable {
    private String ocApproval;
    private String acApproval;
    private String dcApproval;
    private String jtcpApproval;
    private String nodalApproval;
    private String designatedOfficerApproval;
    private String ocApprovalMail;
    private String acApprovalMail;
    private String dcApprovalMail;
    private String jtcpApprovalMail;
    private String nodalOfficerApprovalMail;
    private String designatedOfficerApprovalMail;
    private String ocMailAddress;
    private String acMailAddress;
    private String dcMailAddress;
    private String jtcpMailAddress;
    private String nodalOfficeMailAddress;
    private String designatedOfficerMailAddress;
    private String ocName;
    private String acName;
    private String dcName;
    private String jtcpName;
    private String nodalName;
    private String designatedOfficerName;
    private String requestThruMcell;
    private String IS_OC,IS_DC,IS_AC,IS_JTCP,IS_ADCP,IS_SPLCP,IS_CP;


    public String getOcApproval() {
        return ocApproval;
    }

    public void setOcApproval(String ocApproval) {
        this.ocApproval = ocApproval;
    }

    public String getAcApproval() {
        return acApproval;
    }

    public void setAcApproval(String acApproval) {
        this.acApproval = acApproval;
    }

    public String getDcApproval() {
        return dcApproval;
    }

    public void setDcApproval(String dcApproval) {
        this.dcApproval = dcApproval;
    }

    public String getJtcpApproval() {
        return jtcpApproval;
    }

    public void setJtcpApproval(String jtcpApproval) {
        this.jtcpApproval = jtcpApproval;
    }

    public String getNodalApproval() {
        return nodalApproval;
    }

    public void setNodalApproval(String nodalApproval) {
        this.nodalApproval = nodalApproval;
    }

    public String getDesignatedOfficerApproval() {
        return designatedOfficerApproval;
    }

    public void setDesignatedOfficerApproval(String designatedOfficerApproval) {
        this.designatedOfficerApproval = designatedOfficerApproval;
    }

    public String getOcApprovalMail() {
        return ocApprovalMail;
    }


    public void setOcApprovalMail(String ocApprovalMail) {
        this.ocApprovalMail = ocApprovalMail;
    }

    public String getAcApprovalMail() {
        return acApprovalMail;
    }

    public void setAcApprovalMail(String acApprovalMail) {
        this.acApprovalMail = acApprovalMail;
    }

    public String getDcApprovalMail() {
        return dcApprovalMail;
    }

    public void setDcApprovalMail(String dcApprovalMail) {
        this.dcApprovalMail = dcApprovalMail;
    }

    public String getJtcpApprovalMail() {
        return jtcpApprovalMail;
    }

    public void setJtcpApprovalMail(String jtcpApprovalMail) {
        this.jtcpApprovalMail = jtcpApprovalMail;
    }

    public String getNodalOfficerApprovalMail() {
        return nodalOfficerApprovalMail;
    }

    public void setNodalOfficerApprovalMail(String nodalOfficerApprovalMail) {
        this.nodalOfficerApprovalMail = nodalOfficerApprovalMail;
    }

    public String getDesignatedOfficerApprovalMail() {
        return designatedOfficerApprovalMail;
    }

    public void setDesignatedOfficerApprovalMail(String designatedOfficerApprovalMail) {
        this.designatedOfficerApprovalMail = designatedOfficerApprovalMail;
    }

    public String getOcMailAddress() {
        return ocMailAddress;
    }

    public void setOcMailAddress(String ocMailAddress) {
        this.ocMailAddress = ocMailAddress;
    }

    public String getAcMailAddress() {
        return acMailAddress;
    }

    public void setAcMailAddress(String acMailAddress) {
        this.acMailAddress = acMailAddress;
    }

    public String getDcMailAddress() {
        return dcMailAddress;
    }

    public void setDcMailAddress(String dcMailAddress) {
        this.dcMailAddress = dcMailAddress;
    }

    public String getJtcpMailAddress() {
        return jtcpMailAddress;
    }

    public void setJtcpMailAddress(String jtcpMailAddress) {
        this.jtcpMailAddress = jtcpMailAddress;
    }

    public String getNodalOfficeMailAddress() {
        return nodalOfficeMailAddress;
    }

    public void setNodalOfficeMailAddress(String nodalOfficeMailAddress) {
        this.nodalOfficeMailAddress = nodalOfficeMailAddress;
    }

    public String getDesignatedOfficerMailAddress() {
        return designatedOfficerMailAddress;
    }

    public void setDesignatedOfficerMailAddress(String designatedOfficerMailAddress) {
        this.designatedOfficerMailAddress = designatedOfficerMailAddress;
    }

    public String getOcName() {
        return ocName;
    }

    public void setOcName(String ocName) {
        this.ocName = ocName;
    }

    public String getAcName() {
        return acName;
    }

    public void setAcName(String acName) {
        this.acName = acName;
    }

    public String getDcName() {
        return dcName;
    }

    public void setDcName(String dcName) {
        this.dcName = dcName;
    }

    public String getJtcpName() {
        return jtcpName;
    }

    public void setJtcpName(String jtcpName) {
        this.jtcpName = jtcpName;
    }

    public String getNodalName() {
        return nodalName;
    }

    public void setNodalName(String nodalName) {
        this.nodalName = nodalName;
    }

    public String getDesignatedOfficerName() {
        return designatedOfficerName;
    }

    public void setDesignatedOfficerName(String designatedOfficerName) {
        this.designatedOfficerName = designatedOfficerName;
    }

    public String getRequestThruMcell() {
        return requestThruMcell;
    }

    public void setRequestThruMcell(String requestThruMcell) {
        this.requestThruMcell = requestThruMcell;
    }

    public String getIS_OC() {
        return IS_OC;
    }

    public void setIS_OC(String IS_OC) {
        this.IS_OC = IS_OC;
    }

    public String getIS_DC() {
        return IS_DC;
    }

    public void setIS_DC(String IS_DC) {
        this.IS_DC = IS_DC;
    }

    public String getIS_AC() {
        return IS_AC;
    }

    public void setIS_AC(String IS_AC) {
        this.IS_AC = IS_AC;
    }

    public String getIS_JTCP() {
        return IS_JTCP;
    }

    public void setIS_JTCP(String IS_JTCP) {
        this.IS_JTCP = IS_JTCP;
    }

    public String getIS_ADCP() {
        return IS_ADCP;
    }

    public void setIS_ADCP(String IS_ADCP) {
        this.IS_ADCP = IS_ADCP;
    }

    public String getIS_SPLCP() {
        return IS_SPLCP;
    }

    public void setIS_SPLCP(String IS_SPLCP) {
        this.IS_SPLCP = IS_SPLCP;
    }

    public String getIS_CP() {
        return IS_CP;
    }

    public void setIS_CP(String IS_CP) {
        this.IS_CP = IS_CP;
    }
}
