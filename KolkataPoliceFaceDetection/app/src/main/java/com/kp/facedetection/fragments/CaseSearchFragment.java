package com.kp.facedetection.fragments;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.kp.facedetection.R;
import com.kp.facedetection.SearchResultActivity;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.model.CriminalDetails;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.model.DivisionWisePoliceStationList;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by DAT-Asset-131 on 22-03-2016.
 */
public class CaseSearchFragment extends Fragment {

    DivisionWisePoliceStationList divisionWisePoliceStationList;
    private JSONArray divisionWisePoliceStationArray;
    List<String> policeStationNameArrayList = new ArrayList<String>();
    List<String> policeStationCodeArrayList = new ArrayList<String>();

    private Spinner sp_modus_operandi;
    private TextView tv_divison_value;
    private TextView tv_time_crime_value;
    private TextView tv_ps_value;
    private TextView tv_date_start_value;
    private TextView tv_date_end_value;
    private TextView tv_category_crime_value;
    private EditText et_case_value;
    private Button btn_Case_search;
    private Button btn_reset;

    private int timeSetStatus= 0;

    protected String[] array_policeStation ;


    protected String[] array_crimeCategory ;
    protected ArrayList<CharSequence> selectedCrimeCategory = new ArrayList<CharSequence>();

    protected String[] array_division ;
    protected ArrayList<CharSequence> selectedDivision = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedDivisionCode = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedPoliceStations = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedPoliceStationsCode = new ArrayList<CharSequence>();


    private ArrayAdapter<CharSequence> modusOperandiAdapter;

    private String divisionCodeString="";
    private String divisionWisePoliceStationCodeString="";
    private String crimeCategoryString="";


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.case_search_layout, container, false);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        sp_modus_operandi = (Spinner)view.findViewById(R.id.sp_modus_operandi);
        et_case_value = (EditText)view.findViewById(R.id.et_case_value);
        tv_divison_value = (TextView)view.findViewById(R.id.tv_divison_value);
        tv_ps_value = (TextView)view.findViewById(R.id.tv_ps_value);
        tv_category_crime_value = (TextView)view.findViewById(R.id.tv_category_crime_value);
        tv_date_start_value = (TextView)view.findViewById(R.id.tv_date_start_value);
        tv_date_end_value = (TextView)view.findViewById(R.id.tv_date_end_value);
        tv_time_crime_value = (TextView)view.findViewById(R.id.tv_time_crime_value);
        btn_Case_search = (Button)view.findViewById(R.id.btn_Case_search);
        btn_reset = (Button)view.findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_Case_search);
        Constants.buttonEffect(btn_reset);


        modusOperandiAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.modus_operandiNew_array, R.layout.simple_spinner_item);
        modusOperandiAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_modus_operandi.setAdapter(modusOperandiAdapter);



        array_crimeCategory = new String[Constants.crimeCategoryArrayList.size()];
        array_crimeCategory = Constants.crimeCategoryArrayList.toArray(array_crimeCategory);

        array_division = new String[Constants.divisionArrayList.size()];
        array_division = Constants.divisionArrayList.toArray(array_division);

        clickEvents();

    }


    private void clickEvents() {

         /* Division  Click Event */

        tv_divison_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showSelectDivisionDialog();
                tv_ps_value.setText("Select Police Stations");
                divisionWisePoliceStationCodeString = "";

            }
        });

        /* Police stations Click Event */

        tv_ps_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                divisionWisePoliceStationCodeString = "";
                tv_ps_value.setText("Select Police Stations");
                policeStationNameArrayList.clear();
                policeStationCodeArrayList.clear();
                selectedPoliceStations.clear();
                selectedPoliceStationsCode.clear();
                if (tv_divison_value.getText().toString().equalsIgnoreCase("Select Divisons")) {
                    Toast.makeText(getActivity(), "Please select a division", Toast.LENGTH_SHORT).show();
                } else {
                    fetchDivisionWisePoliceStations();
                }


            }
        });

         /* Crime Category Click Event */

        tv_category_crime_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showSelectCrimeCategoryDialog();

            }
        });


        /* Start Date field click event */

        tv_date_start_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DateUtils.setDate(getActivity(), tv_date_start_value, true);
            }
        });

        /* End Date field click event */

        tv_date_end_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DateUtils.setDate(getActivity(), tv_date_end_value, true);
            }
        });

        /* Time of crime field click event */

        tv_time_crime_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialog();
            }
        });

          /* Search Button click event */

        btn_Case_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String startDate = tv_date_start_value.getText().toString().trim().replace("Select a Date", "");
                String endDate = tv_date_end_value.getText().toString().trim().replace("Select a Date", "");

                if (!endDate.equalsIgnoreCase("") && startDate.equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please provide both dates", Toast.LENGTH_LONG).show();
                } else if (endDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please provide both dates", Toast.LENGTH_LONG).show();
                } else if (!endDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("")) {
                    boolean date_result_flag = DateUtils.dateComparison(startDate, endDate);
                    System.out.println("DateComparison: " + date_result_flag);
                    if (!date_result_flag) {
                        Toast.makeText(getActivity(), "End date can't be greater than the start date", Toast.LENGTH_LONG).show();
                    } else {
                        fetchCaseSearchResult();
                    }
                } else {
                    fetchCaseSearchResult();
                }


            }
        });

          /* Resetting all fields to original state */

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("Reset Button Click");

                /* for divisions part */
                tv_divison_value.setText("Select Divisons");
                selectedDivision.clear();
                selectedDivisionCode.clear();

                 /* for police stations part */
                tv_ps_value.setText("Select Police Stations");
                divisionWisePoliceStationCodeString="";

                /* for case number part */
                et_case_value.setText("");

                /* for Start Date part */
                tv_date_start_value.setText("Select a Date");

                /* for End Date part */
                tv_date_end_value.setText("Select a Date");

                /* for Category of crime part */
                tv_category_crime_value.setText("Select a Category");
                crimeCategoryString="";
                selectedCrimeCategory.clear();

               /* for modus operandi part */
                modusOperandiAdapter = ArrayAdapter.createFromResource(getActivity(),
                        R.array.modus_operandiNew_array, R.layout.simple_spinner_item);
                modusOperandiAdapter
                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sp_modus_operandi.setAdapter(modusOperandiAdapter);
                modusOperandiAdapter.notifyDataSetChanged();

                /* for time part */
                tv_time_crime_value.setText("Select a Time");

            }
        });

    }

    /* This method shows a time picker dialog */

    private void timePickerDialog(){
        timeSetStatus = 0;

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        //final int sec =  mcurrentTime.get(Calendar.SECOND);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                if(selectedMinute<10&&selectedHour<10){
                    tv_time_crime_value.setText( "0"+selectedHour + ":" + "0"+selectedMinute);
                }
                else if(selectedHour<10){
                    tv_time_crime_value.setText( "0"+selectedHour + ":" + selectedMinute);
                }
                else if(selectedMinute<10){
                    tv_time_crime_value.setText( selectedHour + ":" + "0"+selectedMinute);
                }
                else {
                    tv_time_crime_value.setText( selectedHour + ":" + selectedMinute);
                }

                timeSetStatus = 1;

            }
        }, hour, minute, true);//Yes 24 hour time

        mTimePicker.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                timeSetStatus = 2;
                // Actions on clicks outside the dialog.
            }
        });

        mTimePicker.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                if (timeSetStatus!=1) {
                    tv_time_crime_value.setText("Select a Time");
                }


            }
        });

        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }



   /* This method shows Division list in a pop-up */

    protected void showSelectDivisionDialog() {

        boolean[] checkedDivisionCategory = new boolean[array_division.length];
        int count = array_division.length;

        for(int i = 0; i < count; i++)
            checkedDivisionCategory[i] = selectedDivision.contains(array_division[i]);

        DialogInterface.OnMultiChoiceClickListener divisionDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if(isChecked){
                    selectedDivision.add(array_division[which]);
                    selectedDivisionCode.add(Constants.divCodeArrayList.get(which));
                }
                else{
                    selectedDivision.remove(array_division[which]);
                    selectedDivisionCode.remove(Constants.divCodeArrayList.get(which));
                }


                onChangeSelectedDivision();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Divisions");
        builder.setMultiChoiceItems(array_division, checkedDivisionCategory, divisionDialogListener);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedDivision() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderDivCode = new StringBuilder();

        for(CharSequence divName : selectedDivision)
            stringBuilder.append(divName + ",");

        for(CharSequence divisionCode : selectedDivisionCode){
            stringBuilderDivCode.append("\'"+divisionCode.toString().toLowerCase() + "\',");
        }

        if(selectedDivision.size()==0) {
            tv_divison_value.setText("Select Divisons");
            tv_ps_value.setText("Select Police Stations");
            divisionWisePoliceStationCodeString="";
            divisionCodeString="";
        }
        else {
            tv_divison_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
            divisionCodeString = stringBuilderDivCode.toString().substring(0, stringBuilderDivCode.toString().length() - 1);
            System.out.println("DivCode: " + divisionCodeString);
        }



    }


    /* Fetch police stations according to the division */

    private void fetchDivisionWisePoliceStations() {

        if (!Constants.internetOnline(getActivity())) {
            Utility.showAlertDialog(getActivity(), Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_DIVISIONWISE_POLICESTATION);
        taskManager.setDivisionWisePoliceStation(true);

        String[] keys = { "divcode" };
        String[] values = { divisionCodeString.trim() };
        taskManager.doStartTask(keys, values, true);

    }

    public void parseDivisionWisePoliceStationResponse(String response){

        //System.out.println("DivisionWisePoliceStationResponse: "+response);

        if (response != null && !response.equals("")) {

            policeStationNameArrayList.clear();

            try {
                JSONObject obj = new JSONObject(response);
                divisionWisePoliceStationList = new DivisionWisePoliceStationList();

                divisionWisePoliceStationArray = obj.getJSONArray("result");

                for(int i=0;i<divisionWisePoliceStationArray.length();i++){

                    JSONObject row = divisionWisePoliceStationArray.getJSONObject(i);
                    divisionWisePoliceStationList.setPs_code(row.optString("PSCODE"));
                    divisionWisePoliceStationList.setPs_name(row.optString("PSNAME"));
                    divisionWisePoliceStationList.setDiv_code(row.optString("DIVCODE"));
                    divisionWisePoliceStationList.setOc_name(row.optString("OCNAME"));

                    policeStationNameArrayList.add(row.optString("PSNAME"));
                    policeStationCodeArrayList.add(row.optString("PSCODE"));

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            array_policeStation = new String[policeStationNameArrayList.size()];
            array_policeStation = policeStationNameArrayList.toArray(array_policeStation);

            showSelectPoliceStationsDialog();

        }

    }


     /* This method shows police station list in a pop-up */

    protected void showSelectPoliceStationsDialog() {
        boolean[] checkedPoliceStations = new boolean[array_policeStation.length];
        int count = array_policeStation.length;

        for(int i = 0; i < count; i++)
            checkedPoliceStations[i] = selectedPoliceStations.contains(array_policeStation[i]);

        DialogInterface.OnMultiChoiceClickListener policeStationsDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if(isChecked) {
                    selectedPoliceStations.add(array_policeStation[which]);
                    selectedPoliceStationsCode.add(policeStationCodeArrayList.get(which));

                }
                else{
                    selectedPoliceStations.remove(array_policeStation[which]);
                    selectedPoliceStationsCode.remove(policeStationCodeArrayList.get(which));
                }


                onChangeSelectedPoliceStations();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Police Stations");
        builder.setMultiChoiceItems(array_policeStation, checkedPoliceStations, policeStationsDialogListener);

        AlertDialog dialog = builder.create();
        dialog.show();
    }



    protected void onChangeSelectedPoliceStations() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderPSCode = new StringBuilder();

        for(CharSequence policeStation : selectedPoliceStations)
            stringBuilder.append(policeStation + ",");

        for(CharSequence policeStationCode : selectedPoliceStationsCode){
            stringBuilderPSCode.append("\'"+policeStationCode.toString().toLowerCase() + "\',");
        }

        if(selectedPoliceStations.size()==0) {
            tv_ps_value.setText("Select Police Stations");
            divisionWisePoliceStationCodeString="";
        }
        else {
            tv_ps_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
            divisionWisePoliceStationCodeString = stringBuilderPSCode.toString().substring(0,stringBuilderPSCode.toString().length()-1);
        }

        System.out.println("Selected PS code: " + divisionWisePoliceStationCodeString);

    }


    /* This method shows Crime Category list in a pop-up */

    protected void showSelectCrimeCategoryDialog() {

        boolean[] checkedCrimeCategory = new boolean[array_crimeCategory.length];
        int count = array_crimeCategory.length;

        for(int i = 0; i < count; i++)
            checkedCrimeCategory[i] = selectedCrimeCategory.contains(array_crimeCategory[i]);

        DialogInterface.OnMultiChoiceClickListener crimeCategoryDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if(isChecked)
                    selectedCrimeCategory.add(array_crimeCategory[which]);
                else
                    selectedCrimeCategory.remove(array_crimeCategory[which]);

                onChangeSelectedCrimeCategory();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Category of Crime");
        builder.setMultiChoiceItems(array_crimeCategory, checkedCrimeCategory, crimeCategoryDialogListener);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedCrimeCategory() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderCrimeCategory = new StringBuilder();

        for(CharSequence crimeCategory : selectedCrimeCategory) {
            stringBuilder.append(crimeCategory + ",");

            String crimeCategoryModifiedString = stringBuilderCrimeCategory.append("\'"+crimeCategory.toString().toLowerCase() + "\',").toString();

            crimeCategoryString = crimeCategoryModifiedString.substring(0,crimeCategoryModifiedString.length()-1);
        }

        if(selectedCrimeCategory.size()==0) {
            tv_category_crime_value.setText("Select a Category");
        }
        else {
            tv_category_crime_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
        }

    }



    /* Fetch Case Search Result */

    private void fetchCaseSearchResult(){


        String caseNumber = et_case_value.getText().toString().trim().toLowerCase();
        String startDate = tv_date_start_value.getText().toString().trim().replace("Select a Date", "");
        String endDate = tv_date_end_value.getText().toString().trim().replace("Select a Date", "");
        String crimeTime = tv_time_crime_value.getText().toString().trim().replace("Select a Time","");

        if(!divisionWisePoliceStationCodeString.equalsIgnoreCase("") || !caseNumber.equalsIgnoreCase("") || !startDate.equalsIgnoreCase("") || !endDate.equalsIgnoreCase("") || !crimeCategoryString.equalsIgnoreCase("") || !crimeTime.equalsIgnoreCase("")){


            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_CASE_SEARCH);
            taskManager.setCaseSearch(true);


            String[] keys = {"policestations","caseno","datefrom","dateto","crimecategory","crimetime","pageno"};
            String[] values = {divisionWisePoliceStationCodeString.trim(),caseNumber.trim(),startDate.trim(),endDate.trim(),crimeCategoryString.trim(),crimeTime.trim(),"1" };
            taskManager.doStartTask(keys, values, true);

        }
        else {
            Toast.makeText(getActivity(),"Please provide atleast one value for search",Toast.LENGTH_LONG).show();
        }


    }

    public void parseCaseSearchResultResponse(String jsonStr, String[] keys, String[] values){

        //System.out.println("Case Search Result: "+jsonStr);

        try {
            if (jsonStr != null && !jsonStr.equals("")) {

                JSONObject jObj = new JSONObject(jsonStr);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {
                    String pageno=jObj.opt("pageno").toString();
                    String totalResult=jObj.opt("totalresult").toString();


                    JSONArray jsonArray = jObj.optJSONArray("result");
                    ArrayList<CriminalDetails> criminalList = Utility
                            .parseCrimiinalRecords(getActivity(), jsonArray);
                    if (criminalList != null && criminalList.size() > 0) {
                        Intent intent = new Intent(getActivity(),
                                SearchResultActivity.class);
                        intent.putExtra(Constants.OBJECT, criminalList);
                        intent.putExtra("keys",keys);
                        intent.putExtra("value",values);
                        intent.putExtra("pageno",pageno);
                        intent.putExtra("totalResult",totalResult);
                        intent.putExtra("searchCase","caseSearch");

                        getActivity().startActivity(intent);
                    }else{
                        Toast.makeText(getActivity(), "No result found",
                                Toast.LENGTH_LONG).show();
                    }

                }
                if(!jObj.optString("message").toString().equalsIgnoreCase("Success"))
                    Toast.makeText(getActivity(), jObj.optString("message"),
                            Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Some error has been encountered", Toast.LENGTH_LONG).show();
        }catch (OutOfMemoryError outOfMemoryError){
            outOfMemoryError.printStackTrace();
            Toast.makeText(getActivity(), "Some error has been encountered", Toast.LENGTH_LONG).show();
        }

    }

}
