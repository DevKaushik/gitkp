package com.kp.facedetection.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.kp.facedetection.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-Asset-131 on 10-05-2016.
 */
public class SwipeFragment extends Fragment {

    private ProgressBar progress_load;
    private ImageView imageView;
    private static List<String> imgList = new ArrayList<String>();
    private int position;
    private List<String> imageExtensionList = new ArrayList<String>();

    com.kp.facedetection.imageloader.ImageLoader imageLoader;

   /* public SwipeFragment(List<String> imgList){
        this.imgList = imgList;
    }*/

    public void setImgList(List<String> imgList) {
        this.imgList = imgList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View swipeView = inflater.inflate(R.layout.swipe_fragment, container, false);
        imageView = (ImageView) swipeView.findViewById(R.id.imageView);
        progress_load = (ProgressBar) swipeView.findViewById(R.id.progress_load);

        imageLoader = new com.kp.facedetection.imageloader.ImageLoader(getActivity());

        Bundle bundle = getArguments();
        position = bundle.getInt("position");

        imageExtensionList.clear();

        imageExtensionList.add("bmp");
        imageExtensionList.add("gif");
        imageExtensionList.add("jpg");
        imageExtensionList.add("jpeg");
        imageExtensionList.add("png");


        final String original_url = imgList.get(position);
        final String extension = original_url.substring(original_url.lastIndexOf(".") + 1);
        System.out.println("Extension: " + extension);
        imageExtensionList.remove(imageExtensionList.indexOf(extension));

        new Thread(new Runnable() {
            @Override
            public void run() {

                if (imageLoader.getBitmap(original_url) != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("TAG","URL: "+original_url);
                            displayImage(original_url, imageView);
                        }
                    });

                } else {

                    for (int i = 0; i < imageExtensionList.size(); i++) {

                        if (imageLoader.getBitmap(original_url.replace(extension, imageExtensionList.get(i))) != null) {

                            final int finalI = i;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("TAG","URL: Change extension "+original_url.replace(extension, imageExtensionList.get(finalI)));
                                    displayImage(original_url.replace(extension, imageExtensionList.get(finalI)), imageView);
                                }
                            });
                            break;
                        }else{

                        }

                    }
                }

            }
        }).start();


        clickEvents();

        return swipeView;
    }

    public static SwipeFragment newInstance(int position) {
        SwipeFragment swipeFragment = new SwipeFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        swipeFragment.setArguments(bundle);
        return swipeFragment;
    }

    private void clickEvents() {

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showImagePopup();

            }
        });

    }

    public void showImagePopup() {

        Dialog imageDialog = new Dialog(getActivity());
        imageDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View inflatedView;

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        inflatedView = layoutInflater.inflate(R.layout.image_layout, null, false);
        imageDialog.setContentView(inflatedView);

        final ImageView iv_popup = (ImageView) inflatedView.findViewById(R.id.iv_popup);

        imageExtensionList.clear();

        imageExtensionList.add("bmp");
        imageExtensionList.add("gif");
        imageExtensionList.add("jpg");
        imageExtensionList.add("jpeg");
        imageExtensionList.add("png");


        final String original_url = imgList.get(position);
        final String extension = original_url.substring(original_url.lastIndexOf(".") + 1);
        System.out.println("Extension: " + extension);
        imageExtensionList.remove(imageExtensionList.indexOf(extension));
        new Thread(new Runnable() {
            @Override
            public void run() {


                if (imageLoader.getBitmap(original_url) != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Log.e("TAG:","Swipe Fragment 2:" +original_url);
                            displayImage(original_url, iv_popup);

                        }
                    });
                } else {

                    Log.e("TAG:","Swipe Fragment 3:" +original_url);
                    for (int i = 0; i < imageExtensionList.size(); i++) {

                        if (imageLoader.getBitmap(original_url.replace(extension, imageExtensionList.get(i))) != null) {
                            final int finalI = i;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    displayImage(original_url.replace(extension, imageExtensionList.get(finalI)), iv_popup);
                                }
                            });


                            break;
                        }

                    }
                }

            }
        }).start();
        imageDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //iv_image.setVisibility(View.VISIBLE);
            }
        });

        imageDialog.show();

    }

    private void displayImage(String url, ImageView iv) {

        Log.e("TAG:","Swipe Fragment 1:" +url);

        /*ImageLoader.getInstance().displayImage(url, iv, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                progress_load.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                progress_load.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                progress_load.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                progress_load.setVisibility(View.GONE);
            }
        });
*/

        imageLoader.DisplayImage(url, iv);

    }
}