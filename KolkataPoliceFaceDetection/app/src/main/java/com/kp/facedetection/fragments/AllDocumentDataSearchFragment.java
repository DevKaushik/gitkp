package com.kp.facedetection.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kp.facedetection.AllDocumentDataSearchResultActivity;
import com.kp.facedetection.HotelSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.adapter.CustomDialogAdapterForCategoryCrimeList;
import com.kp.facedetection.adapter.CustomDialogAdapterForCategoryCrimeList2;
import com.kp.facedetection.adapter.CustomDialogAdapterForModusOperandi;
import com.kp.facedetection.interfaces.OnShowAlertForProceedResult;
import com.kp.facedetection.model.AllContentList;
import com.kp.facedetection.model.AllDocumentSearchType;
import com.kp.facedetection.model.ClassListCriminal;
import com.kp.facedetection.model.CrimeCategoryList;
import com.kp.facedetection.model.CrimeCategoryList2;
import com.kp.facedetection.model.DivisionList;
import com.kp.facedetection.model.ModusOperandiList;
import com.kp.facedetection.model.PoliceStationList;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllDocumentDataSearchFragment extends Fragment implements OnShowAlertForProceedResult {

    private EditText et_name, et_address, et_contact;
    String name = "", address = "", contact = "";
    CheckBox chk_dl,chk_vehicle,chk_sdr,chk_cable,chk_kmc,chk_gas,chk_select_all;
    private String pageno = "";
    private String totalResult = "";
    private String[] keys;
    private String[] values;
    String userId = "";
    private Button btn_Search, btn_reset;
    String devicetoken = "";
    String auth_key = "";
    ArrayList<AllDocumentSearchType> allDocumentSearchTypesArrayList = new ArrayList<AllDocumentSearchType>();
    String datafields ="";
    public AllContentList allContentList;
    public JSONArray policeStationListArray;
    public JSONArray crimeCategoryListArray;
    public JSONArray divisionListArray;
    public JSONArray modusListArray;
    public JSONArray ioListArray;
    public JSONArray firStatusListArray;
    public JSONArray classListArray;

    protected String[] array_policeStation ;
    protected ArrayList<CharSequence> selectedPoliceStations = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedPoliceStationsId = new ArrayList<CharSequence>();

    protected String[] array_crimeCategory ;
    protected ArrayList<CharSequence> selectedCrimeCategory = new ArrayList<CharSequence>();

    protected String[] array_identifyCategory ;
    protected ArrayList<CharSequence> selectedIdentifyCategory = new ArrayList<CharSequence>();

    protected String[] array_identifySubCategory ;
    protected ArrayList<CharSequence> selectedIdentifySubCategory = new ArrayList<CharSequence>();
    protected ArrayList<String> customSelectedIdentifySubCategory = new ArrayList<String>();

    protected String[] array_modusOperandi ;
    protected ArrayList<CharSequence> selectedModusOperandi = new ArrayList<CharSequence>();

    protected String[] array_class;
    protected ArrayList<CharSequence> selectedClassList = new ArrayList<CharSequence>();

    protected String[] array_subClass;
    protected ArrayList<CharSequence> selectedSubClass = new ArrayList<CharSequence>();
    protected ArrayList<String> customSelectedSubClass = new ArrayList<String>();

    List<String> policeStationArrayList = new ArrayList<String>();
    public static List<String> identityCategoryList = new ArrayList<String>();
    private List<String> identitySubCategoryList = new ArrayList<String>();
    private List<String> customIdentitySubCategoryList = new ArrayList<String>();
    private List<String> modusOperandiArrayList = new ArrayList<String>();
    public static List<String> classArrayList = new ArrayList<String>();
    private List<String> subClassList = new ArrayList<String>();
    private List<String> customSubClassList = new ArrayList<String>();

    ArrayList<PoliceStationList> obj_policeStationList;
    ArrayList<ClassListCriminal> obj_classList;

    private String[] inputMarkArray;
    private String[] inputClassArray;

    private String identity_category_name="";
    private String policeStationString="";
    private String crimeCategoryString="";
    private String identifyMarkCategoryString="";
    private String identifySubCategoryString="";
    private String identifyCategoryString="";
    private String identifyCategorySubCategoryString="";
    private String modusOperandiString="";
    private String classString = "";
    private String newClassString = "";
    private String subClassString = "";
    private String modifiedClassSubClassString = "";

    private String subClassStringShow = "";

    private String customidentifySubCategoryString="";

    CustomDialogAdapterForModusOperandi customDialogAdapterForModusOperandi;
    CustomDialogAdapterForCategoryCrimeList customDialogAdapterForCategoryCrimeList;
    CustomDialogAdapterForCategoryCrimeList2 customDialogAdapterForCategoryCrimeList2;

    ArrayList<CrimeCategoryList> obj_categoryCrimeList;
    //ArrayList<CrimeCategoryList2> obj_categoryCrimeList2;

    private boolean crimeCategory_status=false;
    private boolean modusOperandi_status=false;

    private ListView lv_dialog;
    private List<String> modified_crimeCategoryList = new ArrayList<String>();


    public static ArrayList<CrimeCategoryList2> obj_categoryCrimeList2;
    public static ArrayList<ModusOperandiList> obj_modusOperandiList;

    private List<String> yearList = new ArrayList<String>();
    private String[] yearArray;

    private String[] ageArray= {"5-10", "11-20", "21-30", "31-40", "41-50","51-60","61-70","71-80","81-90","91-100"};
    private ArrayList<String> ageArrayList;
    protected String[] array_age;


    public AllDocumentDataSearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_all_document_data_search, container, false);
        return rootView;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userId = Utility.getUserInfo(getContext()).getUserId();
        et_name = (EditText) view.findViewById(R.id.et_holderName);
        et_address = (EditText) view.findViewById(R.id.et_address);
        et_contact = (EditText) view.findViewById(R.id.et_Phone);

        chk_dl=(CheckBox)view.findViewById(R.id.chk_dl);
        chk_dl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_dl.setChecked(true);
                else
                    chk_dl.setChecked(false);
            }
        });
        chk_vehicle=(CheckBox)view.findViewById(R.id.chk_vehicle);
        chk_vehicle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_vehicle.setChecked(true);
                else
                    chk_vehicle.setChecked(false);
            }
        });

        chk_sdr=(CheckBox)view.findViewById(R.id.chk_sdr);
        chk_sdr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_sdr.setChecked(true);
                else
                    chk_sdr.setChecked(false);
            }
        });


        chk_cable=(CheckBox)view.findViewById(R.id.chk_cable);
        chk_cable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_cable.setChecked(true);
                else
                    chk_cable.setChecked(false);
            }
        });


        chk_kmc=(CheckBox)view.findViewById(R.id.chk_kmc);
        chk_kmc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_kmc.setChecked(true);
                else
                    chk_kmc.setChecked(false);
            }
        });


        chk_gas=(CheckBox)view.findViewById(R.id.chk_gas);

        chk_gas.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_gas.setChecked(true);
                else
                    chk_gas.setChecked(false);
            }
        });

        chk_select_all=(CheckBox)view.findViewById(R.id.chk_select_all);
        chk_select_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    chk_dl.setChecked(true);
                    chk_vehicle.setChecked(true);
                    chk_sdr.setChecked(true);
                    chk_cable.setChecked(true);
                    chk_kmc.setChecked(true);
                    chk_gas.setChecked(true);

                }
                else{
                    chk_dl.setChecked(false);
                    chk_vehicle.setChecked(false);
                    chk_sdr.setChecked(false);
                    chk_cable.setChecked(false);
                    chk_kmc.setChecked(false);
                    chk_gas.setChecked(false);
                }
            }
        });


        btn_Search = (Button) view.findViewById(R.id.btn_Search);
        btn_reset = (Button) view.findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_Search);
        Constants.buttonEffect(btn_reset);
        backgroundSetForBelowApi();
        ageArrayList = new ArrayList<String>(Arrays.asList(ageArray));
        fetchAllContent();

        clickEvents();
    }
    private void fetchAllContent(){

        if (!Constants.internetOnline(getActivity())) {
            Utility.showAlertDialog(getActivity(), Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_GETCONTENT);
        taskManager.setAllContentAllDocumentSearch(true);

        String[] keys = {  };
        String[] values = { };
        taskManager.doStartTask(keys, values, true);
    }
    public void parseGetAllContentResponse(String response){

        //System.out.println("Get All Content Result: " + response);

        //fetchIdentityCategory();

        if (response != null && !response.equals("")) {

            try {
                Constants.divisionArrayList.clear();
                Constants.crimeCategoryArrayList.clear();
                Constants.policeStationIDArrayList.clear();
                Constants.policeStationNameArrayList.clear();
                //Constants.IOCodeArrayList.clear();
                //Constants.IONameArrayList.clear();
                Constants.firStatusArrayList.clear();

                //	Constants.firStatusArrayList.add("Select a status");

                JSONObject jObj = new JSONObject(response);
                allContentList = new AllContentList();
                if(jObj != null && jObj.optString("status").equalsIgnoreCase("1")){

                    allContentList.setStatus(jObj.optString("status"));
                    allContentList.setMessage(jObj.optString("message"));

                    policeStationListArray = jObj.getJSONObject("result").getJSONArray("policestationlist");
                    crimeCategoryListArray = jObj.getJSONObject("result").getJSONArray("crimecategorylist");
                    divisionListArray = jObj.getJSONObject("result").getJSONArray("divisionlist");
                    modusListArray = jObj.getJSONObject("result").getJSONArray("modlist");
                    //	ioListArray = jObj.getJSONObject("result").getJSONArray("iolist");
                    firStatusListArray = jObj.getJSONObject("result").getJSONArray("firstatuslist");
                    classListArray = jObj.getJSONObject("result").getJSONArray("classList");

                    for(int i=0;i<policeStationListArray.length();i++){

                        PoliceStationList policeStationList = new PoliceStationList();

                        JSONObject row = policeStationListArray.getJSONObject(i);
                        policeStationList.setPs_code(row.optString("PSCODE"));
                        policeStationList.setPs_name(row.optString("PSNAME"));
                        policeStationList.setDiv_code(row.optString("DIVCODE"));
                        policeStationList.setOc_name(row.optString("OCNAME"));

                        policeStationArrayList.add(row.optString("PSNAME"));

                        Constants.policeStationNameArrayList.add(row.optString("PSNAME"));
                        Constants.policeStationIDArrayList.add(row.optString("PSCODE"));

                        allContentList.setObj_policeStationList(policeStationList);

                    }

                    for(int j=0;j<crimeCategoryListArray.length();j++){

                        CrimeCategoryList crimeCategoryList = new CrimeCategoryList();

                        JSONObject row = crimeCategoryListArray.getJSONObject(j);
                        crimeCategoryList.setCategory(row.optString("CATEGORY"));
                        crimeCategoryList.setCategory_desc(row.optString("CATEGORYDESC"));
                        crimeCategoryList.setBroad_category(row.optString("BROAD_CATEGORY"));
                        crimeCategoryList.setIPC_SSl(row.optString("IPC_SLL"));
                        crimeCategoryList.setSections(row.optString("SECTIONS"));
                        crimeCategoryList.setVerified(row.optString("VERIFIED"));
                        crimeCategoryList.setFavourite(row.optString("FAVOURITE"));

                        Constants.crimeCategoryArrayList.add(row.optString("CATEGORY"));


                        allContentList.setObj_categoryCrimeList(crimeCategoryList);

                    }

                    createModifiedCrimeCategoryList(allContentList.getObj_categoryCrimeList());

                    for(int k=0;k<divisionListArray.length();k++){

                        DivisionList divisionList = new DivisionList();
                        JSONObject row = divisionListArray.getJSONObject(k);
                        divisionList.setDiv_name(row.optString("DIVNAME"));
                        divisionList.setDiv_code(row.optString("DIVCODE"));

                        Constants.divisionArrayList.add(row.optString("DIVNAME"));
                        Constants.divCodeArrayList.add(row.optString("DIVCODE"));

                        allContentList.setObj_DivisionList(divisionList);

                    }

                    for(int l=0;l<modusListArray.length();l++){

                        ModusOperandiList modusOperandiList = new ModusOperandiList();
                        JSONObject row = modusListArray.getJSONObject(l);
                        modusOperandiList.setMod_operand(row.optString("MOD_OPER"));
                        modusOperandiList.setSelected(false);

                        modusOperandiArrayList.add(row.optString("MOD_OPER"));


                        allContentList.setObj_modusOperandiList(modusOperandiList);

                    }

					/*for(int m=0;m<ioListArray.length();m++){

						IOList ioList = new IOList();
						JSONObject row = ioListArray.getJSONObject(m);

						ioList.setIo_code(row.optString("IOCODE"));
						ioList.setIo_name(row.optString("IONAME"));
						ioList.setActive_flag(row.optString("ACTIVE_FLAG"));
						ioList.setPs(row.optString("PS"));
						ioList.setGf_no(row.optString("GFNO"));

						Constants.IOCodeArrayList.add(row.optString("IOCODE"));
						Constants.IONameArrayList.add(row.optString("IONAME"));

					}*/

                    for(int n=0;n<firStatusListArray.length(); n++){

                        JSONObject row = firStatusListArray.getJSONObject(n);
                        Constants.firStatusArrayList.add(row.optString("FIR_STATUS"));

                    }

                    for(int m=0; m<classListArray.length(); m++){

                        ClassListCriminal classList = new ClassListCriminal();
                        JSONObject row = classListArray.getJSONObject(m);
                        classList.setClassName(row.optString("CLASS"));
                        classArrayList.add(row.optString("CLASS"));

                        allContentList.setObj_classList(classList);
                    }


                    obj_categoryCrimeList=allContentList.getObj_categoryCrimeList();
                    obj_modusOperandiList=allContentList.getObj_modusOperandiList();
                    obj_policeStationList=allContentList.getObj_policeStationList();
                    obj_classList = allContentList.getObj_classList();
                }
                else {
                  //  showAlertDialog(getActivity(), Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        array_policeStation = new String[policeStationArrayList.size()];
        array_policeStation = policeStationArrayList.toArray(array_policeStation);

        array_crimeCategory = new String[Constants.crimeCategoryArrayList.size()];
        array_crimeCategory = Constants.crimeCategoryArrayList.toArray(array_crimeCategory);

        array_modusOperandi = new String[modusOperandiArrayList.size()];
        array_modusOperandi = modusOperandiArrayList.toArray(array_modusOperandi);

        array_class = new String[classArrayList.size()];
        array_class = classArrayList.toArray(array_class);

        array_age = new String[ageArrayList.size()];
        array_age = ageArrayList.toArray(array_age);
    }
    private void createModifiedCrimeCategoryList(List<CrimeCategoryList> ccList){

        modified_crimeCategoryList.clear();
        System.out.println("CCList Size: "+ccList.size());

        obj_categoryCrimeList2 = new ArrayList<CrimeCategoryList2>();

        List<String> ccFavouriteList = new ArrayList<String>();
        List<String> ccNonFavouriteList = new ArrayList<String>();

        for(int i=0;i<ccList.size();i++){

            if(ccList.get(i).getFavourite().equalsIgnoreCase("Y")){
                ccFavouriteList.add(ccList.get(i).getCategory());
            }
            else{
                ccNonFavouriteList.add(ccList.get(i).getCategory());
            }

        }
        modified_crimeCategoryList.add("Frequently Used");
        modified_crimeCategoryList.addAll(ccFavouriteList);
        modified_crimeCategoryList.add("Others");
        modified_crimeCategoryList.addAll(ccNonFavouriteList);


        System.out.println("CC FAV Size: " + ccFavouriteList.size());
        System.out.println("CC FAV List: "+ccFavouriteList);
        System.out.println("CC Non FAV Size: "+ccNonFavouriteList.size());
        System.out.println("CC Non FAV List: "+ccNonFavouriteList);
        System.out.println("CC Non FAV List: "+modified_crimeCategoryList.size());

        for(int i=0;i<modified_crimeCategoryList.size();i++){

            CrimeCategoryList2 crimeCategoryList2 = new CrimeCategoryList2();
            crimeCategoryList2.setCategory(modified_crimeCategoryList.get(i));

            obj_categoryCrimeList2.add(crimeCategoryList2);
        }

    }
   /* private void fetchIdentityCategory(){

        if (!Constants.internetOnline(getActivity())) {
            Utility.showAlertDialog(getActivity(), Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_IDENTITY_CATEGORY);
        taskManager.setIdentityCategory(true);

        String[] keys = {  };
        String[] values = { };
        taskManager.doStartTask(keys, values, true);
    }
*/
    private void backgroundSetForBelowApi() {
        if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.LOLLIPOP) {
            // Do something for lollipop and above versions
            et_name.setBackgroundResource(R.drawable.text_underline_selector);
            et_address.setBackgroundResource(R.drawable.text_underline_selector);
            et_contact.setBackgroundResource(R.drawable.text_underline_selector);


        } else {
            Log.e("DAPL:", "API above LOLLIPOP");
        }
    }

    public void clickEvents() {
        btn_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name = et_name.getText().toString().trim();

                address = et_address.getText().toString().trim();

                contact = et_contact.getText().toString().trim();
                if(chk_dl.isChecked()){
                    datafields="1";
                }else{
                    datafields=datafields.replace("1","");
                }

                if(chk_vehicle.isChecked()){
                    datafields=datafields+","+"2";
                }
                else{
                    datafields=datafields.replace(",2","");
                }

                if(chk_sdr.isChecked()){
                    datafields=datafields+","+"3";
                }
                else{
                    datafields=datafields.replace(",3","");
                }


                if(chk_cable.isChecked()){
                    datafields=datafields+","+"4";
                }
                else{
                    datafields=datafields.replace(",4","");

                }

                if(chk_kmc.isChecked()){
                    datafields=datafields+","+"5";
                }
                else{
                    datafields=datafields.replace(",5","");

                }

                if(chk_gas.isChecked()){
                    datafields=datafields+","+"6";
                }
                else{
                    datafields=datafields.replace(",6","");

                }
                if(datafields.equalsIgnoreCase("")){
                    datafields="1,2,3,4,5,6";
                }
                if (!name.isEmpty() || !address.isEmpty() || !contact.isEmpty()) {
                    Utility utility = new Utility();
                    utility.setDelegate(AllDocumentDataSearchFragment.this);
                    Utility.showAlertForProceed(getActivity());
                } else {

                    Utility.showAlertDialog(getActivity(), " Search Error!!! ", "Please provide at least one value for search", false);

                }


            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_name.setText("");
                et_address.setText("");
                et_contact.setText("");
                chk_dl.setChecked(false);
                chk_vehicle.setChecked(false);
                chk_sdr.setChecked(false);
                chk_cable.setChecked(false);
                chk_kmc.setChecked(false);
                chk_gas.setChecked(false);
                chk_select_all.setChecked(false);



            }
        });
    }

    @Override
    public void onShowAlertForProceedResultSuccess(String success) {
        fetchSearchResult();

    }

    private void fetchSearchResult() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_DOCUMENT_SEARCH);
        taskManager.setDocumentAllDataSearch(true);
        //user_id,device_token,device_type,imei,auth_key,name,address,pageno
        try{
            //devicetoken = GCMRegistrar.getRegistrationId(getActivity());
            if(FirebaseInstanceId.getInstance().getToken()!=null) {
                devicetoken = FirebaseInstanceId.getInstance().getToken();
            }
            else{
                devicetoken=Utility.getFCMToken(getActivity());
            }

            auth_key=Utility.getDocumentSearchSesionId(getActivity());
        }
        catch (Exception e){

        }
        String[] keys = {"name", "address", "phoneNumber", "dataFields", "page_no", "user_id", "device_type", "device_token","imei","auth_key"};
        String[] values = {name, address, contact,datafields, "1",userId,"android", devicetoken,Utility.getImiNO(getActivity()),auth_key};
        taskManager.doStartTask(keys, values, true, true);
    }

    public void parseAllDocumentDataSearchResultResponse(String result, String[] keys, String[] values) {
        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("page").toString();
                    totalResult = jObj.opt("count").toString();

                    JSONObject resultObject = jObj.getJSONObject("result");

                   createAllDocumentSearchTypeList(resultObject);

                }
                else if(jObj.opt("status").toString().equalsIgnoreCase("0"))
                {
                    Utility.showAlertDialog(getActivity(), " Search Error ", Constants.ERROR_MSG_NO_RECORD, false);
                }
                else {
                    Utility.showAlertDialog(getActivity(), " Search Error ", Constants.ERROR_MSG_TO_RELOAD, false);
                }


            } catch (JSONException e) {

                e.printStackTrace();
                //Utility.showToast(getActivity(), "Some error has been encountered", "long");
                Utility.showAlertDialog(getActivity(), " Search Error ", "Some error has been encountered", false);
            }
        }

    }

    public void createAllDocumentSearchTypeList(JSONObject resultArray) {
        allDocumentSearchTypesArrayList.clear();
        if (resultArray.has("1")) {
            try {
                JSONObject jsonObjectDL = resultArray.getJSONObject("1");
                if (jsonObjectDL.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[0]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("2")) {
            try {
                JSONObject jsonObjectVehicle = resultArray.getJSONObject("2");
                if (jsonObjectVehicle.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[1]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("3")) {
            try {
                JSONObject jsonObjectSDR = resultArray.getJSONObject("3");
                if (jsonObjectSDR.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[2]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("4")) {
            try {
                JSONObject jsonObjectCable = resultArray.getJSONObject("4");
                if (jsonObjectCable.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[3]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("5")) {
            try {
                JSONObject jsonObjectKMC = resultArray.getJSONObject("5");
                if (jsonObjectKMC.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[4]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("6")) {
            try {
                JSONObject jsonObjectGas = resultArray.getJSONObject("6");
                if (jsonObjectGas.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[5]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }

        }
        Intent intent = new Intent(getActivity(), AllDocumentDataSearchResultActivity.class);
        intent.putExtra("keys", keys);
        intent.putExtra("value", values);
        intent.putExtra("ALL_DOCUMENT_SEARCH_TYPE", (Serializable) allDocumentSearchTypesArrayList);
        intent.putExtra("ALL_DOCUMENT_SEARCH_LIST", String.valueOf(resultArray));
        startActivity(intent);

    }


    @Override
    public void onShowAlertForProceedResultError(String error) {

    }
}
