package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.adapter.PSListForAllDashboardTabAdapter;
import com.kp.facedetection.adapter.WarrentFailDueAdapter;
import com.kp.facedetection.interfaces.OnItemClickListenerForAllFIRView;
import com.kp.facedetection.interfaces.OnItemClickListenerForWarrentDue;
import com.kp.facedetection.interfaces.OnItemClickListenerForWarrentFail;
import com.kp.facedetection.model.CrimeReviewDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class WarrentFallingDuePsActivity extends BaseActivity implements OnItemClickListenerForAllFIRView,OnItemClickListenerForWarrentFail,OnItemClickListenerForWarrentDue, View.OnClickListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String selectedDate="";
    private String selected_div="";
    private String selected_ps="";
    private String warrantType="";

    private RecyclerView recycler_psList;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<CrimeReviewDetails> psList = new ArrayList<>();
    private WarrentFailDueAdapter psListAdapter;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warrent_falling_due_ps);
        ObservableObject.getInstance().addObserver(this);
        setToolBar();
        initViews();
    }


    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }


    private void initViews() {
        selectedDate = getIntent().getExtras().getString("SELECTED_DATE_WA_FALL");
        selected_div = getIntent().getExtras().getString("SELECTED_DIV_WA_FALL");
        selected_ps = getIntent().getExtras().getString("SELECTED_PS_WA_FALL");
        warrantType = getIntent().getExtras().getString("WARRANT_TYPE");
        recycler_psList = (RecyclerView) findViewById(R.id.recycler_psList);
        mLayoutManager = new LinearLayoutManager(this);
        recycler_psList.setLayoutManager(mLayoutManager);
        //recycler_psList.setItemAnimator(new DefaultItemAnimator());
        recycler_psList.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));

        allWarrantFallingDuePSListCall();

        psListAdapter = new WarrentFailDueAdapter(this, psList);
        recycler_psList.setAdapter(psListAdapter);
        psListAdapter.setClickListener(this);
       // psListAdapter.setClickListenerWarrentFail(this);
       // psListAdapter.setClickListenerWarrentDue(this);
    }


    private void allWarrantFallingDuePSListCall() {
       TaskManager taskManager = new TaskManager(this);
        if(warrantType.equalsIgnoreCase("fail")) {
            taskManager.setMethod(Constants.METHOD_ALL_WA_FALL);
        }
        else if(warrantType.equalsIgnoreCase("due"))
        {
            taskManager.setMethod(Constants.METHOD_ALL_WA_DUE);
        }
        taskManager.setWaFallDueItemSearch(true);

        String[] keys = {"currDate","div","ps","user_id"};
        String[] values = {selectedDate.trim(), selected_div.trim(), selected_ps.trim(),Utility.getUserInfo(this).getUserId()};
        taskManager.doStartTask(keys, values, true);
    }


    public void parseWarrantRecvPSResult(String response, String[] keys, String[] values) {
        //System.out.println("parseWarrantExecPSResult" + response);

        if (response != null && !response.equals("")) {

            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.opt("status").toString().equalsIgnoreCase("1")) {
                    JSONArray result = jobj.optJSONArray("result");
                    parsePSResponse(result);
                } else {
                    Utility.showAlertDialog(WarrentFallingDuePsActivity.this, Constants.ERROR_DETAILS_TITLE, jobj.optString("message"), false);
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(WarrentFallingDuePsActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }

    private void parsePSResponse(JSONArray resultArray) {

        psList.clear();
        for (int i = 0; i < resultArray.length(); i++) {
            CrimeReviewDetails psDetails = new CrimeReviewDetails(Parcel.obtain());
            JSONObject object = resultArray.optJSONObject(i);
            psDetails.setCode(object.optString("PSCODE"));
            psDetails.setCrimeCategory(object.optString("PSNAME"));
            if(warrantType.equalsIgnoreCase("fail")) {
                psDetails.setCountOfCrime(object.optString("COUNT")+ "" + "/" + "" + object.optString("TOTAL_COUNT"));
            }else if(warrantType.equalsIgnoreCase("due")){
                psDetails.setCountOfCrime(object.optString("COUNT"));
            }
            psList.add(psDetails);
        }
        psListAdapter.notifyDataSetChanged();
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View view, int position) {

        CrimeReviewDetails details = psList.get(position);
        Intent intent = new Intent(WarrentFallingDuePsActivity.this, WarrentFallingDueDetailActivityNew.class);
        intent.putExtra("PS_CODE", details.getCode());
        intent.putExtra("SELECT_DATE", selectedDate);
        intent.putExtra("SELECT_DIV", selected_div);
        intent.putExtra("WA_POSITION",position);
        if(warrantType.equalsIgnoreCase("fail"))
        {
            intent.putExtra("WARRANT_TYPE",Constants.WARRENT_TYPE_FAIL);

        }
        else if(warrantType.equalsIgnoreCase("due"))
        {
            intent.putExtra("WARRANT_TYPE", Constants.WARRENT_TYPE_DUE);

        }
        startActivity(intent);
    }

    @Override
    public void onItemClickListenerWarrentDue(int position) {
     /*   CrimeReviewDetails details = psList.get(position);
        Intent intent = new Intent(WarrentFallingDuePsActivity.this, WarrentFallingDueDetailActivityNew.class);
        intent.putExtra("PS_CODE", details.getCode());
        intent.putExtra("SELECT_DATE", selectedDate);
        intent.putExtra("SELECT_DIV", selected_div);
        intent.putExtra("WARRANT_TYPE", Constants.WARRENT_TYPE_DUE);


        startActivity(intent);
*/
    }

    @Override
    public void onItemClickListenerWarrentFail(int position) {
       /* CrimeReviewDetails details = psList.get(position);
        Intent intent = new Intent(WarrentFallingDuePsActivity.this, WarrentFallingDueDetailActivityNew.class);
        intent.putExtra("PS_CODE", details.getCode());
        intent.putExtra("SELECT_DATE", selectedDate);
        intent.putExtra("SELECT_DIV", selected_div);
        intent.putExtra("WARRANT_TYPE",Constants.WARRENT_TYPE_FAIL);
        startActivity(intent);
*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
                break;
        }

    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
