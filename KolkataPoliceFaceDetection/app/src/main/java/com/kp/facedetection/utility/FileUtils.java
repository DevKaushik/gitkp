package com.kp.facedetection.utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Kaushik on 02/02/2017.
 */
public class FileUtils {
    public static final String INTERNAL_DIR = "KP";
    public static final String IMG_PREFIX = "img_kp";

    ////////////////////////////for camera capture image orientation
    public static float getOrientation(int rotation){
        float returnRotation = 0;
        switch (rotation){
            case ExifInterface.ORIENTATION_ROTATE_90 :
                returnRotation = 90f;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180 :
                returnRotation = 180f;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270 :
                returnRotation = 270f;
                break;
            default:
                returnRotation = 0;
                break;
        }
        return returnRotation;
    }

    /** Create a file Uri for saving an image or video */
    public static Uri getOutputMediaFileUri(int type, String name){
        return Uri.fromFile(getOutputMediaFile(type, name));
    }

    /** Create a File for saving an image or video */
    public static File getOutputMediaFile(int type, String fName){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), INTERNAL_DIR);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.e("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        File mediaFile;
        if (type == Constants.MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    IMG_PREFIX+ fName +".jpg");
        }  else {
            return null;
        }

        return mediaFile;
    }

    public static String getFileName(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyy_hhmmss");
        return simpleDateFormat.format(new Date());
    }

    /*Decoding large bitmap image*/
    public static Bitmap decodeSampledBitmap(String filePath, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filePath, options);
    }

    /*Calculating large bitmap image*/
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }

    /*For rotating bitmap image*/
    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        mtx.setRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }
}
