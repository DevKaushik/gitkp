package com.kp.facedetection.interfaces;

/**
 * Created by DAT-Asset-128 on 13-10-2017.
 */

public interface OnMapIconClickListener {
    void onMapClick(int pos);
}
