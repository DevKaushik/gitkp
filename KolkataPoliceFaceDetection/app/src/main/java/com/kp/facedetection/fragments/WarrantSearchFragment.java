package com.kp.facedetection.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.kp.facedetection.R;
import com.kp.facedetection.WarrantSearchResultActivity;
import com.kp.facedetection.model.WarrantSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kaushik -131 on 27-07-2016.
 */
public class WarrantSearchFragment extends Fragment implements View.OnClickListener{

    private TextView tv_fromDateValue;
    private TextView tv_toDateValue;
    private Spinner sp_statusValue;
    private TextView tv_psValue;
    private Spinner sp_typeValue;
    private Button btn_warrant_search;
    private Button btn_reset;

    private TextView tv_divisionValue;
    private EditText et_warranteeValue;
    private EditText et_warrantNoValue;
    private EditText et_courtValue;
    private TextView tv_ioValue;

    private String pageno="";
    private String totalResult="";
    private String[] keys;
    private String[] values;


    ArrayAdapter<CharSequence> warrantStatusAdapter;
    ArrayAdapter<CharSequence> warrantTypeAdapter;

    protected String[] array_policeStation ;
    protected ArrayList<CharSequence> selectedPoliceStations = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedPoliceStationsId = new ArrayList<CharSequence>();
    private String policeStationString="";

    protected String[] array_division ;
    protected ArrayList<CharSequence> selectedDivision = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedDivisionsCode = new ArrayList<CharSequence>();
    private String divisionString="";

    private String[] divWisePSArray;
    protected ArrayList<String> divWisePSNameList;// = new ArrayList<String>();
    protected ArrayList<String> divWisePSCodeList;// = new ArrayList<String>();

    protected ArrayList<String> selectedPSName = new ArrayList<String>();
    protected ArrayList<String> selectedPSCode = new ArrayList<String>();
    private String psString="";


    private String[] psWiseIOArray;
    protected ArrayList<String> psWiseIONameList;
    protected ArrayList<String> psWiseIOCodeList;

    protected ArrayList<String> selectedIOName = new ArrayList<String>();
    protected ArrayList<String> selectedIOCode = new ArrayList<String>();
    private String ioString="";

    private ScrollView scrollView1;
    public boolean ioAfterPS = false;
    public boolean psAfterDiv = false;
    private String userId = "";
    ImageView toggleButtonSearchType;
    String ownjurisdictionFlag="1";
    boolean search_own_global_flag=false;



    private List<WarrantSearchDetails> warrantSearchDeatilsList;

    public static WarrantSearchFragment newInstance( ) {

        WarrantSearchFragment warrantSearchFragment = new WarrantSearchFragment();
        //warrantSearchFragment.setArguments(args);
        return warrantSearchFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // Inflate the fragment layout we defined above for this fragment
    // Set the associated text for the title
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.warrant_search_layout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        userId = Utility.getUserInfo(getContext()).getUserId();
        toggleButtonSearchType=(ImageView) view.findViewById(R.id.Tb_search_type);
        toggleButtonSearchType.setOnClickListener(this);
        tv_fromDateValue=(TextView)view.findViewById(R.id.tv_fromDateValue);
        tv_toDateValue=(TextView)view.findViewById(R.id.tv_toDateValue);
        sp_statusValue=(Spinner) view.findViewById(R.id.sp_statusValue);
        tv_psValue=(TextView)view.findViewById(R.id.tv_psValue);
        sp_typeValue=(Spinner) view.findViewById(R.id.sp_typeValue);

        tv_divisionValue=(TextView)view.findViewById(R.id.tv_divisionValue);
        et_warranteeValue=(EditText) view.findViewById(R.id.et_warranteeValue);
        et_warrantNoValue=(EditText) view.findViewById(R.id.et_warrantNoValue);
        et_courtValue=(EditText) view.findViewById(R.id.et_courtValue);
        tv_ioValue=(TextView)view.findViewById(R.id.tv_ioValue);

        scrollView1 = (ScrollView) view.findViewById(R.id.scrollView1);

        btn_warrant_search = (Button)view.findViewById(R.id.btn_warrantSearch);
        btn_reset = (Button)view.findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_warrant_search);
        Constants.buttonEffect(btn_reset);

        array_policeStation = new String[Constants.policeStationNameArrayList.size()];
        array_policeStation = Constants.policeStationNameArrayList.toArray(array_policeStation);

        array_division=new String[Constants.divisionArrayList.size()];
        array_division=Constants.divisionArrayList.toArray(array_division);

        /** spiner set to warrant status*/
        warrantStatusAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.WarrantStatus_Array, R.layout.simple_spinner_item);
        warrantStatusAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_statusValue.setAdapter(warrantStatusAdapter);

        sp_statusValue.setSelection(0);

        /** spiner set to warrant type*/
        warrantTypeAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.WarrantType_Array, R.layout.simple_spinner_item);
        warrantTypeAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_typeValue.setAdapter(warrantTypeAdapter);

        sp_typeValue.setSelection(0);


        clickEvents();

    }

    private void clickEvents() {

        /* Start Date field click event */
        tv_fromDateValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DateUtils.setDate(getActivity(), tv_fromDateValue, true);
            }
        });


        /* End Date field click event */
        tv_toDateValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DateUtils.setDate(getActivity(), tv_toDateValue, true);
            }
        });


         /* Police stations Click Event */
        tv_psValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(selectedDivisionsCode.size()>0 && !divisionString.isEmpty()) {
                    tv_psValue.setError(null);
                    onChangeSelectedDivisions();
                    fetchPsName(divisionString);
                    psAfterDiv=false;
                }
                else{
                    psAfterDiv = true;
                   // showAlertDialog(getActivity(), "Error!!! ", "Please select division", false);
                    fetchPsName("");
                }

             /*   if(Constants.policeStationNameArrayList.size()>0) {
                    tv_psValue.setError(null);
                    showSelectPoliceStationsDialog();
                }
                else {
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }*/

            }
        });


        tv_divisionValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.divisionArrayList.size()>0) {
                    //showSelectPoliceStationsDialog();

                    Log.e("Division size",""+Constants.divisionArrayList.size());
                    showSelectDivisionDialog();
                }
                else {
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }

            }
        });


        tv_ioValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!policeStationString.isEmpty() && selectedPSCode.size()>0){

                    tv_ioValue.setError(null);
                    fetchIOName(policeStationString);
                    ioAfterPS = false;

                }
                else{
                    ioAfterPS = true;
                   // fetchIOName(policeStationString);
                   showAlertDialog(getActivity(), "Alert !!! ", "Please select police stations", false);
                }

            }
        });

        /* Resetting all fields to original state */

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resetDataFieldValues();

            }
        });

        btn_warrant_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String startDate = tv_fromDateValue.getText().toString().trim();
                String endDate = tv_toDateValue.getText().toString().trim();

                if (!endDate.equalsIgnoreCase("") && startDate.equalsIgnoreCase("")) {
                    showAlertDialog(getActivity(), "Alert !!! ", "Please provide both dates", false);
                } else if (endDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("")) {
                    showAlertDialog(getActivity(), "Alert !!! ", "Please provide both dates", false);
                } else if (!endDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("")) {
                    boolean date_result_flag = DateUtils.dateComparison(startDate, endDate);
                    System.out.println("DateComparison: " + date_result_flag);
                    if (!date_result_flag) {
                        showAlertDialog(getActivity(), "Alert !!! ", "From date can't be greater than To date", false);
                    } else {
                        fetchWarrantSearchResult();
                    }
                }/*else if(ioAfterPS == true && ioString.equalsIgnoreCase("")){

                    showAlertDialog(getActivity(), "Alert !!! ", "Please select IO", false);
                }*/
                else {
                    fetchWarrantSearchResult();
                }

            }
        });
    }
    private void fetchPsName(String divisionString){
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_DIV_WISE_PS);
        taskManager.setDivWisePsListForWarrantSearch(true);
        String[] keys = {"divcode"};
        String[] values = {divisionString.trim()};
        taskManager.doStartTask(keys, values, true);
    }
    public void parseDivisionWisePoliceStationResponse(String response){
        L.e("RESPONSE called:--------"+response);
        if(response != null && !response.equals("")){

            JSONObject jobj = null;
            try{

                jobj = new JSONObject(response);
                if(jobj != null && jobj.optString("status").equalsIgnoreCase("1")){

                    JSONArray div_wise_ps_array = jobj.getJSONArray("result");
                    divWisePSNameList = new ArrayList<String>();
                    divWisePSCodeList = new ArrayList<String>();

                    for(int i=0; i<div_wise_ps_array.length(); i++){

                        JSONObject ps_list = div_wise_ps_array.getJSONObject(i);
                        divWisePSNameList.add(ps_list.optString("PSNAME"));
                        divWisePSCodeList.add(ps_list.optString("PSCODE"));

                    }
                    L.e("PS_LIST:--------"+divWisePSNameList);

                }else{
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }

            }catch (JSONException e){
                e.printStackTrace();
            }

            showSelectPSDialog();
        }

    }
    protected void showSelectPSDialog() {
        L.e("showSelectPSDialog called:--------");

        selectedPSName = new ArrayList<String>();
        selectedPSCode = new ArrayList<String>();

        divWisePSArray=new String[divWisePSNameList.size()];
        divWisePSArray=divWisePSNameList.toArray(divWisePSArray);
        L.e("DIV_WISE_PS:------"+divWisePSArray);

        boolean[] checkedPS = new boolean[divWisePSArray.length];
        int count = divWisePSArray.length;

        for(int i = 0; i < count; i++) {
            checkedPS[i] = selectedPSName.contains(divWisePSArray[i]);
        }

        DialogInterface.OnMultiChoiceClickListener ioDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedPSName.add(divWisePSArray[which]);
                    selectedPSCode.add((String)divWisePSCodeList .get(which));

                } else{
                    selectedPSName.remove(divWisePSArray[which]);
                    selectedPSCode.remove((String) divWisePSCodeList.get(which));

                }
                //onChangeSelectedPoliceStations();
                //onChangeSelectedPS();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select PS");
        builder.setMultiChoiceItems(divWisePSArray, checkedPS, ioDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                // onChangeSelectedIO();
                onChangeSelectedPS();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                //tv_io_value.setText("Select IO");
                policeStationString="";
                tv_psValue.setText("Select Police Station");
                tv_ioValue.setText("Select IO");
                // ioString="";
                selectedPSName.clear();
                selectedPSCode.clear();
                selectedIOName.clear();
                selectedIOCode.clear();
                psAfterDiv = true;
               // tv_psValue.setError("");
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }
    protected void onChangeSelectedPS() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        /*stringBuilderNew is used for setting data to tv_io_value*/
        StringBuilder stringBuilderNew = new StringBuilder();

        for(CharSequence ps : selectedPSName){
            //stringBuilder.append(io + ",");
            stringBuilder.append("\'"+ps + "\',");
            stringBuilderNew.append(ps + ",");
        }

        for(CharSequence ps : selectedPSCode){
            stringBuilderId.append("\'"+ps + "\',");
        }

        if(selectedPSName.size()==0) {
            //tv_io_value.setText("Select IO");
            tv_psValue.setText("");
            ioString="";
            psAfterDiv = true;
            tv_psValue.setError("");
        }
        else {
            //tv_io_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
            //ioString=stringBuilderId.toString().substring(0,stringBuilderId.toString().length()-1);
            tv_psValue.setText(stringBuilderNew.toString().substring(0,stringBuilderNew.toString().length()-1));
            policeStationString=stringBuilderId.toString().substring(0,stringBuilderId.toString().length()-1);
        }

    }

    private void resetDataFieldValues(){

        //* for police stations part*//*
        tv_psValue.setText("Select Police Stations");
        policeStationString="";
        selectedPoliceStations.clear();
        selectedPoliceStationsId.clear();
        tv_psValue.setError(null);

        //* for Division part*//*
        tv_divisionValue.setText("Select Division");
        divisionString="";
        selectedDivision.clear();
        selectedDivisionsCode.clear();

        //* for IO part*//*
        tv_ioValue.setText("Select IO");
        ioString="";
        tv_ioValue.setError(null);
        ioAfterPS = false;


        //* for From Date part *//*
        tv_fromDateValue.setText("");

        //* for To Date part *//*
        tv_toDateValue.setText("");

        //*  for Status part *//*
        sp_statusValue.setSelection(0);

        //*  for Type part *//*
        sp_typeValue.setSelection(0);

        //* for Warrantee part *//*
        et_warranteeValue.setText("");

        //* for Warrant No part *//*
        et_warrantNoValue.setText("");

        //* for Court part *//*
        et_courtValue.setText("");


    }

     	 /* This method shows Police Station list in a pop-up */

    protected void showSelectPoliceStationsDialog() {
        boolean[] checkedPoliceStations = new boolean[array_policeStation.length];
        int count = array_policeStation.length;

        for(int i = 0; i < count; i++) {
            checkedPoliceStations[i] = selectedPoliceStations.contains(array_policeStation[i]);
        }

        DialogInterface.OnMultiChoiceClickListener policeStationsDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedPoliceStations.add(array_policeStation[which]);
                    selectedPoliceStationsId.add((CharSequence) Constants.policeStationIDArrayList.get(which));
                } else{
                    selectedPoliceStations.remove(array_policeStation[which]);
                    selectedPoliceStationsId.remove((CharSequence) Constants.policeStationIDArrayList.get(which));
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Police Stations");
        builder.setMultiChoiceItems(array_policeStation, checkedPoliceStations, policeStationsDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedPoliceStations();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_psValue.setText("Select Police Stations");
                tv_ioValue.setText("Select IO");
                policeStationString="";
                selectedPoliceStations.clear();
                selectedPoliceStationsId.clear();

            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedPoliceStations() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        if(ioAfterPS){

            scrollView1.scrollTo(0,scrollView1.getBottom());
            tv_ioValue.requestFocus();
            tv_ioValue.setError("");
        }

        for(CharSequence policeStation : selectedPoliceStations){
            stringBuilder.append(policeStation + ",");
        }

        for(CharSequence policeStation : selectedPoliceStationsId){
            stringBuilderId.append("\'"+policeStation + "\',");
        }

        if(selectedPoliceStations.size()==0) {
            tv_psValue.setText("Select Police Stations");
            policeStationString="";
        }
        else {
            tv_psValue.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
            policeStationString=stringBuilderId.toString().substring(0,stringBuilderId.toString().length()-1);
            ioString="";
            tv_ioValue.setText("Select IO");

        }

    }

    private void fetchIOName(String policeStationString){

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_PS_WISE_IO);
        taskManager.setPsWiseIOListForWarrant(true);

        String[] keys = {"ps"};
        String[] values = {policeStationString.trim()};
        taskManager.doStartTask(keys, values, true);
    }

    public void parsePSWiseIOForWarrantResponse(String response){

        //System.out.println("PS wise IO name: " + response);

        if(response != null && !response.equals("")){

            JSONObject jobj = null;
            try{

                jobj = new JSONObject(response);
                if(jobj != null && jobj.optString("status").equalsIgnoreCase("1")){


                    JSONArray ps_wise_io_array = jobj.getJSONArray("result");

                    psWiseIONameList = new ArrayList<String>();
                    psWiseIOCodeList = new ArrayList<String>();

                    for(int i=0; i<ps_wise_io_array.length(); i++){

                        JSONObject io_list = ps_wise_io_array.getJSONObject(i);
                        psWiseIONameList.add(io_list.optString("IONAME"));
                        psWiseIOCodeList.add(io_list.optString("IOCODE"));

                    }
                }else{
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }

            }catch (JSONException e){
                e.printStackTrace();
            }

            System.out.println("IO list: " + psWiseIONameList);
            System.out.println("IO List Size: " + psWiseIONameList.size());

            showSelectIODialog();

        }
    }


    protected void showSelectIODialog() {

        selectedIOName = new ArrayList<String>();
        selectedIOCode = new ArrayList<String>();

        psWiseIOArray=new String[psWiseIONameList.size()];
        psWiseIOArray=psWiseIONameList.toArray(psWiseIOArray);

        boolean[] checkedIO = new boolean[psWiseIOArray.length];
        int count = psWiseIOArray.length;

        for(int i = 0; i < count; i++) {
            checkedIO[i] = selectedIOName.contains(psWiseIOArray[i]);
        }

        DialogInterface.OnMultiChoiceClickListener ioDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedIOName.add(psWiseIOArray[which]);
                    selectedIOCode.add((String)psWiseIOCodeList .get(which));

                } else{
                    selectedIOName.remove(psWiseIOArray[which]);
                    selectedIOCode.remove((String) psWiseIOCodeList.get(which));

                }


//				onChangeSelectedPoliceStations();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select IO");
        builder.setMultiChoiceItems(psWiseIOArray, checkedIO, ioDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedIO();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_ioValue.setText("Select IO");
                ioString="";
                selectedIOName.clear();
                selectedIOCode.clear();
                ioAfterPS = true;
                tv_ioValue.setError("");
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedIO() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for(CharSequence io : selectedIOName){
            stringBuilder.append(io + ",");

        }

        for(CharSequence io : selectedIOCode){
            stringBuilderId.append("\'"+io + "\',");
        }

        if(selectedIOName.size()==0) {
            tv_ioValue.setText("Select IO");
            ioString="";
            ioAfterPS = true;
            tv_ioValue.setError("");
        }
        else {
            tv_ioValue.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
            ioString=stringBuilderId.toString().substring(0,stringBuilderId.toString().length()-1);

        }

    }


    protected void showSelectDivisionDialog() {

        boolean[] checkedDivisions = new boolean[array_division.length];
        int count = array_division.length;

        for(int i = 0; i < count; i++) {
            checkedDivisions[i] = selectedDivision.contains(array_division[i]);
        }

        DialogInterface.OnMultiChoiceClickListener divisionDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedDivision.add(array_division[which]);
                    selectedDivisionsCode.add((CharSequence) Constants.divCodeArrayList.get(which));
                } else{
                    selectedDivision.remove(array_division[which]);
                    selectedDivisionsCode.remove((CharSequence) Constants.divCodeArrayList.get(which));
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Division");
        builder.setMultiChoiceItems(array_division, checkedDivisions, divisionDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedDivisions();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_divisionValue.setText("Select Division");
                tv_psValue.setText("Select Police Stations");
                tv_ioValue.setText("Select IO");
                divisionString="";
                selectedDivision.clear();
                selectedDivisionsCode.clear();
                selectedPSName.clear();
                selectedPSCode.clear();
                selectedIOName.clear();
                selectedIOCode.clear();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedDivisions() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for(CharSequence division : selectedDivision){
            stringBuilder.append(division + ",");
        }

        for(CharSequence division : selectedDivisionsCode){
            stringBuilderId.append("\'"+division + "\',");
        }

        if(selectedDivision.size()==0) {
            tv_divisionValue.setText("Select Division");
            divisionString="";
        }
        else {
            tv_divisionValue.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
            divisionString=stringBuilderId.toString().substring(0,stringBuilderId.toString().length()-1);
            policeStationString="";
            tv_psValue.setText("Select Police Stations");
            ioString="";
            tv_ioValue.setText("Select IO");
        }

    }



    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     * */
    private void showAlertDialog(Context context, String title,final String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                        if(message.equalsIgnoreCase("Please provide atleast one value for search")){
                            tv_psValue.setError(null);
                        }
                        else if(!policeStationString.equalsIgnoreCase("") && ioString.equalsIgnoreCase("")){
                            tv_psValue.setError(null);
                        }
                        else if(ioString.equalsIgnoreCase("") && ioAfterPS == true){
                            scrollView1.scrollTo(tv_ioValue.getScrollX(),tv_psValue.getScrollY());
                            tv_psValue.requestFocus();
                            tv_psValue.setError("");
                            //ioAfterPS = false;
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    private void fetchWarrantSearchResult() {

        String date_from=tv_fromDateValue.getText().toString().trim();
        String date_to=tv_toDateValue.getText().toString().trim();
        String status_value=sp_statusValue.getSelectedItem().toString().trim().replace("Select the Status","") ;;
        String type_value=sp_typeValue.getSelectedItem().toString().trim().replace("Select the Type","");
        String warrantee_value=et_warranteeValue.getText().toString().trim().replace("Warrantee","");
        String warrantNo_value=et_warrantNoValue.getText().toString().trim().replace("Warrant No","");
        String court_value=et_courtValue.getText().toString().trim().replace("Court","");

        if(!policeStationString.equalsIgnoreCase("") || !date_from.equalsIgnoreCase("") || !date_to.equalsIgnoreCase("")
                || !status_value.equalsIgnoreCase("") || !type_value.equalsIgnoreCase("") || !divisionString.equalsIgnoreCase("")
                || !ioString.equalsIgnoreCase("") || !warrantee_value.equalsIgnoreCase("") || !warrantNo_value.equalsIgnoreCase("") || !court_value.equalsIgnoreCase("")){

            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_WARRANT_SEARCH);
            taskManager.setWarrantSearch(true);

            String[] keys={ "datefrom", "dateto", "status", "policestations", "type", "pageno", "warrantee", "wano", "court", "io", "divisions","user_id","own_jurisdiction" };

            String[] values={ date_from.trim(), date_to.trim(), status_value.trim(), policeStationString.trim(), type_value.trim(), "1", warrantee_value.trim(), warrantNo_value.trim(), court_value.trim(), ioString.trim(), divisionString.trim(),userId,ownjurisdictionFlag};

            //String[] values={ "03-JAN-2016", "28-JUL-2016", "Recalled", "'MTZ'", "NBW", "1" };

            taskManager.doStartTask(keys, values, true);

        }
        else{

            showAlertDialog(getActivity(), "Alert !!!", "Please provide atleast one value for search", false);

        }
    }

    public void parseWarrantSearchResultResponse(String result, String[] keys, String[] values){

        //System.out.println("parseWarrantSearchResultResponse: " + result);

        if(result != null && !result.equals("")){

            try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.opt("status").toString().equalsIgnoreCase("1")){

                    this.keys=keys;
                    this.values=values;
                    pageno=jObj.opt("pageno").toString();
                    totalResult=jObj.opt("totalresult").toString();

                    JSONArray resultArray=jObj.getJSONArray("result");

                    parseWarrantSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(getActivity(),Constants.ERROR_DETAILS_TITLE,Constants.ERROR_MSG_DETAIL,false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }

    }

    private void parseWarrantSearchResponse(JSONArray resultArray) {

        warrantSearchDeatilsList=new ArrayList<WarrantSearchDetails>();

        for(int i=0;i<resultArray.length();i++){

            JSONObject jsonObj=null;

            try{

                jsonObj=resultArray.getJSONObject(i);

                WarrantSearchDetails warrantSearchDetails=new WarrantSearchDetails();

                if(!jsonObj.optString("ROWNUMBER").equalsIgnoreCase("null")){
                    warrantSearchDetails.setRowNumber(jsonObj.optString("ROWNUMBER"));
                }
                if(!jsonObj.optString("NAME").equalsIgnoreCase("null") && !jsonObj.optString("NAME").equalsIgnoreCase("") && jsonObj.optString("NAME") != null ){
                    warrantSearchDetails.setWaName(jsonObj.optString("NAME"));
                }
                if(!jsonObj.optString("WANO").equalsIgnoreCase("null")){
                    warrantSearchDetails.setWaNo(jsonObj.optString("WANO"));
                }
                if(!jsonObj.optString("PS_RECV_DATE").equalsIgnoreCase("null")){
                    warrantSearchDetails.setPsRecvDate(jsonObj.optString("PS_RECV_DATE"));
                }
                if(!jsonObj.optString("UNDER_SECTIONS").equalsIgnoreCase("null")){
                    warrantSearchDetails.setUnderSections(jsonObj.optString("UNDER_SECTIONS"));
                }
                if(!jsonObj.optString("WA_STATUS").equalsIgnoreCase("null")){
                    warrantSearchDetails.setWaStatus(jsonObj.optString("WA_STATUS"));
                }
                if(!jsonObj.optString("WATYPE").equalsIgnoreCase("null")){
                    warrantSearchDetails.setWaType(jsonObj.optString("WATYPE"));
                }
                if (!jsonObj.optString("PS").equalsIgnoreCase("null")) {
                    warrantSearchDetails.setPs(jsonObj.optString("PS"));
                }
                if (!jsonObj.optString("WA_SLNO").equalsIgnoreCase("null")) {
                    warrantSearchDetails.setWaSlNo(jsonObj.optString("WA_SLNO"));
                }
                if (!jsonObj.optString("WA_YEAR").equalsIgnoreCase("null")) {
                    warrantSearchDetails.setWa_yr(jsonObj.optString("WA_YEAR"));
                }
                if (!jsonObj.optString("SLNO").equalsIgnoreCase("null")) {
                    warrantSearchDetails.setSl_no(jsonObj.optString("SLNO"));
                }

                warrantSearchDeatilsList.add(warrantSearchDetails);


            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        Intent intent=new Intent(getActivity(), WarrantSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putExtra("WARRANT_SEARCH_LIST", (Serializable) warrantSearchDeatilsList);
        startActivity(intent);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Tb_search_type:
                if(!search_own_global_flag){
                    toggleButtonSearchType.setImageResource(R.drawable.toggle_right);
                    ownjurisdictionFlag="0";
                    search_own_global_flag=true;
                }
                else{
                    toggleButtonSearchType.setImageResource(R.drawable.toggle_left);
                    ownjurisdictionFlag="1";
                    search_own_global_flag=false;
                }

                break;
        }

    }
}
