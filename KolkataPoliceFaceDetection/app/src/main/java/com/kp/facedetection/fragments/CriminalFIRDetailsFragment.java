package com.kp.facedetection.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Selection;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kp.facedetection.CaseSearchFIRDetailsActivity;
import com.kp.facedetection.CriminalCrimeMapActivity;
import com.kp.facedetection.CriminalDetailActivity;
import com.kp.facedetection.PDFLoadActivity;
import com.kp.facedetection.PopUpActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.SearchResultActivity;
import com.kp.facedetection.model.FIRDetails;
import com.kp.facedetection.model.FIRWarrantDetails;
import com.kp.facedetection.model.FirLogListDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class CriminalFIRDetailsFragment extends Fragment implements View.OnClickListener, LocationListener {

    private TextView tv_listName, tv_complainantAddress, tv_category, tv_BriefFact, tv_complaint, tv_gde, tv_CGR, tv_warrant, tv_court, tv_presentStatus, tv_person_arrested, tv_watermark, tv_io_name;
    private RelativeLayout rl_fir;
    private RelativeLayout rl_pm;
    private RelativeLayout rl_log;
    private RelativeLayout rl_case_log;
    private RelativeLayout rl_FIR_Details;
    private LinearLayout linear_comments, linear_otherAccused, linear_person_arrested;
    private ImageView bt_crimeMapForOF;
    private ImageView bt_noCrimeMapForOF;
    private ImageView bt_crimeMapForCF;
    private ImageView bt_noCrimeMapForCF;
    private ImageView bt_crimeMap;
    private ImageView bt_nocrimeMap;
    private ImageView iv_pdf,iv_pm_pdf,iv_log,iv_case_log;


    private ImageView iv_edit_comment;
    private Button btn_comment_post;

    private String user_Id = "";
    private String userName = "";

    private String item_name = "";
    private String flag_value = "";
    private String criminal_name = "";
    private int position;
    private String pdf_download_url = "";
    private String pmreport_pdf_download_url = "";
    private String chargeSheetDetails = "";
    private String log_response = "";

    private String ps_code = "";
    private String case_no = "";
    private String case_year = "";
    private String pm_no = "";
    private String pm_yr = "";
    private boolean isFIR_PDF=false;
    private boolean isDownloadPDF=false;

    LinearLayout linear_caseTranfer_CF;
    View inflatedViewCaseTransfer;
    private PopupWindow popWindowForCaseTransLog;

    private boolean isCaseTransferAdded = false;
    private boolean isOtherAccuesedAdded = false;

    View inflatedViewDownloadLog;
    private PopupWindow popWindowForCaseDownloadLog;

    View inflatedFIRviewChargeSheet;
    private PopupWindow popWindowForChargeSheet;
    private boolean isPopWindowForChargeSheet = false;

    private LinearLayout linear_downloadLog_CF;
    private LinearLayout linear_downloadLog;

    ArrayList<FirLogListDetails> firDownloadLogList = new ArrayList<>();
    private int length;

    FIRDetails firDetails_model_recv = new FIRDetails();

    // Flag for GPS status
    boolean isGPSEnabled = false;

    // Flag for network status
    boolean isNetworkEnabled = false;

    // Flag for GPS status
    boolean canGetLocation = false;

    Location location; // Location
    double latitude; // Latitude
    double longitude; // Longitude

    // Declaring a Location Manager
    LocationManager locationManager;

    String latitudeVal = "", longitudeVal = "";

    private String[] keys;
    private String[] values;

    private List<String> CommentList = new ArrayList<>();
    int pos = 0;
    private boolean isNoCrimeMapShow = false;

    private List<FIRDetails> firDetailsList = new ArrayList<FIRDetails>();

    public static final String TAG = CriminalFIRDetailsFragment.class.getSimpleName();


    public CriminalFIRDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.layout_fir_details_popup_modified, container, false);
    }

    @Override
    public void onViewCreated(View inflatedFIRview, @Nullable Bundle savedInstanceState) {
        //super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            position = bundle.getInt("ITEM_POSITION", 0);
            item_name = bundle.getString("ITEM_NAME_SHOW");
            flag_value = bundle.getString("FLAG_VALUE");
            criminal_name = bundle.getString("CRIMINAL_NAME");
            ps_code = bundle.getString("PS_CODE");
            case_no = bundle.getString("CASE_NO");
            case_year = bundle.getString("CASE_YEAR");
            pm_no = bundle.getString("PM_NO");
            pm_yr = bundle.getString("PM_YEAR");
            firDetails_model_recv = (FIRDetails) bundle.getSerializable("FIR_DETAILS_LIST");

        }

        firDetailsList.add(firDetails_model_recv);

        tv_listName = (TextView) inflatedFIRview.findViewById(R.id.tv_itemName);
        tv_listName.setText(item_name);

        tv_io_name = (TextView) inflatedFIRview.findViewById(R.id.tv_io_name);
        tv_complainantAddress = (TextView) inflatedFIRview.findViewById(R.id.tv_underSection);
        tv_category = (TextView) inflatedFIRview.findViewById(R.id.tv_category);
        tv_BriefFact = (TextView) inflatedFIRview.findViewById(R.id.tv_BriefFact);
        tv_complaint = (TextView) inflatedFIRview.findViewById(R.id.tv_complaint);
        tv_gde = (TextView) inflatedFIRview.findViewById(R.id.tv_gde);
        tv_CGR = (TextView) inflatedFIRview.findViewById(R.id.tv_CGR);
        tv_warrant = (TextView) inflatedFIRview.findViewById(R.id.tv_warrant);
        tv_court = (TextView) inflatedFIRview.findViewById(R.id.tv_court);
        tv_presentStatus = (TextView) inflatedFIRview.findViewById(R.id.tv_presentStatus);
        linear_comments = (LinearLayout) inflatedFIRview.findViewById(R.id.linear_comments);
        linear_otherAccused = (LinearLayout) inflatedFIRview.findViewById(R.id.linear_otherAccused);
        rl_FIR_Details = (RelativeLayout) inflatedFIRview.findViewById(R.id.rl_FIR_Details);

        tv_person_arrested = (TextView) inflatedFIRview.findViewById(R.id.tv_person_arrested);
        tv_person_arrested.setVisibility(View.GONE);

        linear_person_arrested = (LinearLayout) inflatedFIRview.findViewById(R.id.linear_person_arrested);
        linear_person_arrested.setVisibility(View.GONE);


        rl_fir = (RelativeLayout) inflatedFIRview.findViewById(R.id.rl_fir);
        rl_fir.setOnClickListener(this);

        rl_pm = (RelativeLayout) inflatedFIRview.findViewById(R.id.rl_pm);
        rl_pm.setOnClickListener(this);

        rl_case_log = (RelativeLayout) inflatedFIRview.findViewById(R.id.rl_case_log);
        rl_case_log.setOnClickListener(this);

        rl_log = (RelativeLayout) inflatedFIRview.findViewById(R.id.rl_log);
        rl_log.setOnClickListener(this);

        bt_crimeMapForCF = (ImageView) inflatedFIRview.findViewById(R.id.bt_crimeMap);
        bt_noCrimeMapForCF = (ImageView) inflatedFIRview.findViewById(R.id.bt_noCrimeMap);

        bt_crimeMapForOF = (ImageView) inflatedFIRview.findViewById(R.id.bt_crimeMap);
        bt_noCrimeMapForOF = (ImageView) inflatedFIRview.findViewById(R.id.bt_noCrimeMap);

        bt_crimeMap = (ImageView) inflatedFIRview.findViewById(R.id.bt_crimeMap);
        bt_nocrimeMap = (ImageView) inflatedFIRview.findViewById(R.id.bt_noCrimeMap);

        iv_pdf = (ImageView) inflatedFIRview.findViewById(R.id.iv_pdf);
        iv_pm_pdf = (ImageView) inflatedFIRview.findViewById(R.id.iv_pm_pdf);
        iv_log = (ImageView) inflatedFIRview.findViewById(R.id.iv_log);
        iv_case_log = (ImageView) inflatedFIRview.findViewById(R.id.iv_case_log);

        iv_edit_comment = (ImageView) inflatedFIRview.findViewById(R.id.iv_edit_comment);
        btn_comment_post = (Button) inflatedFIRview.findViewById(R.id.btn_comment_post);
        Constants.buttonEffect(btn_comment_post);

        // change font
        changeFontOfAllTextViews();

        tv_watermark = (TextView) inflatedFIRview.findViewById(R.id.tv_watermark);
        setWatermark();
        linear_downloadLog=(LinearLayout)inflatedFIRview.findViewById(R.id.linear_downloadLog);

        // iv_edit_comment_visibility handle depend on flag_value
        if (flag_value.equalsIgnoreCase("OF") || flag_value.equalsIgnoreCase("CF"))
            iv_edit_comment.setVisibility(View.VISIBLE);
        else
            iv_edit_comment.setVisibility(View.GONE);

        // call method depend on flag_value
        if (flag_value.equalsIgnoreCase("OC")) {
            Log.e("TAG", "OC:: " + flag_value);
            popUpWindowForFirOCListItem(position, item_name);
        } else if (flag_value.equalsIgnoreCase("OF")) {
            Log.e("TAG", "OF:: " + flag_value);
            popUpWindowForFirOFListItem(position, item_name);
        } else if (flag_value.equalsIgnoreCase("CF")) {
            Log.e("TAG", "CF::" + flag_value);
            popUpWindowForFirCFListItem(position, item_name);
        }

    }


    //change font text of TextView
    private void changeFontOfAllTextViews() {

        Constants.changefonts(tv_listName, getContext(), "Calibri Bold.ttf");

        Utility.changefonts(tv_io_name, getActivity(), "Calibri.ttf");
        Utility.changefonts(tv_complainantAddress, getActivity(), "Calibri.ttf");
        Utility.changefonts(tv_category, getActivity(), "Calibri.ttf");
        Utility.changefonts(tv_BriefFact, getActivity(), "Calibri.ttf");

        // color set
        tv_presentStatus.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
    }

    private void setWatermark() {
        try {
            userName = Utility.getUserInfo(getActivity()).getName();
            user_Id = Utility.getUserInfo(getActivity()).getUserId();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);
    }


    private void popUpWindowForFirOCListItem(int position, String itemName) {

        final int item_pos = position;

        fetchAllDetail(position, itemName);


        bt_crimeMap.setVisibility(View.GONE);
        bt_nocrimeMap.setVisibility(View.VISIBLE);

        bt_nocrimeMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MAP_MSG, false);
            }
        });

    }

    private void popUpWindowForFirOFListItem(final int position, String itemName) {

        fetchAllDetail(position, itemName);

        setWarrantValue();

        setPresentStatus();

        setAllAccusedList();

        setCommentInstructionPart();

        //OF flag
        if (((firDetails_model_recv.getPoLat() != null) && (!firDetails_model_recv.getPoLat().equalsIgnoreCase("")) && (!firDetails_model_recv.getPoLat().equalsIgnoreCase("null")))
                && ((firDetails_model_recv.getPoLong() != null) && (!firDetails_model_recv.getPoLong().equalsIgnoreCase("")) && (!firDetails_model_recv.getPoLong().equalsIgnoreCase("null")))) {

            //bt_crimeMap.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.view_map));
            bt_crimeMapForOF.setVisibility(View.VISIBLE);
            bt_noCrimeMapForOF.setVisibility(View.GONE);
            bt_crimeMapForOF.setClickable(true);
            Log.e("OF Flag :", "  - TRUE ");

        } else if (firDetails_model_recv.getFirUnderSectionDetailsList().size() > 0) {

            //bt_crimeMap.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.load_map));
            bt_noCrimeMapForOF.setVisibility(View.VISIBLE);
            bt_crimeMapForOF.setVisibility(View.GONE);
            bt_noCrimeMapForOF.setClickable(true);
            //bt_crimeMap.setEnabled(false);
            Log.e("OF Flag :", "  - FALSE ");
        } else {

            isNoCrimeMapShow = true; // boolean value maintain for to handle those records which will not give to fetch map location
            bt_noCrimeMapForOF.setVisibility(View.VISIBLE);
            bt_crimeMapForOF.setVisibility(View.GONE);
            bt_noCrimeMapForOF.setClickable(true);
            Log.e("OF Flag :", "  gone");
        }

        bt_crimeMapForOF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("TAG", "FLAG2 " + flag_value);
                Intent intent = new Intent(getActivity(), CriminalCrimeMapActivity.class);
                intent.putExtra("MAP_SEARCH", (Serializable) firDetailsList);
                intent.putExtra("POSITION", position);
                intent.putExtra("isSingle", true);
                startActivity(intent);
            }
        });

        bt_noCrimeMapForOF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("TAG", "FLAG3 " + flag_value);

                if(isNoCrimeMapShow){
                    Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MAP_MSG, false);
                    isNoCrimeMapShow = false;
                }else{

                    getCurrentLocation();
                    showAlertForFlagProceed("OF", position);
                }
            }
        });

    }

    private void popUpWindowForFirCFListItem(final int position, String itemName) {

        btn_comment_post.setVisibility(View.GONE);

        fetchAllDetail(position, itemName);


        // CF Flag
        if (((firDetails_model_recv.getPoLat() != null) && (!firDetails_model_recv.getPoLat().equalsIgnoreCase("")) && (!firDetails_model_recv.getPoLat().equalsIgnoreCase("null")))
                && ((firDetails_model_recv.getPoLong() != null) && (!firDetails_model_recv.getPoLong().equalsIgnoreCase("")) && (!firDetails_model_recv.getPoLong().equalsIgnoreCase("null")))) {
            Log.e("", "aa");
            //bt_crimeMap.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.view_map));
            bt_crimeMapForCF.setVisibility(View.VISIBLE);
            bt_noCrimeMapForCF.setVisibility(View.GONE);
            bt_crimeMapForCF.setClickable(true);
        } else if (firDetails_model_recv.getFirUnderSectionDetailsList().size() > 0) {
            Log.e("", "adfca");
            //bt_crimeMap.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.load_map));
            bt_noCrimeMapForCF.setVisibility(View.VISIBLE);
            bt_crimeMapForCF.setVisibility(View.GONE);
            bt_noCrimeMapForCF.setClickable(true);
            //bt_crimeMap.setEnabled(false);
        } else {
            Log.e("", "afdgjkha");
            //bt_crimeMap.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.load_map));
            bt_noCrimeMapForCF.setVisibility(View.GONE);
            bt_crimeMapForCF.setVisibility(View.GONE);
            bt_noCrimeMapForCF.setClickable(true);
            //bt_crimeMap.setEnabled(false);
        }


        bt_crimeMapForCF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("TAG", "FLAG5 " + flag_value);
                Intent intent = new Intent(getActivity(), CriminalCrimeMapActivity.class);
                intent.putExtra("MAP_SEARCH", (Serializable) firDetailsList);
                intent.putExtra("POSITION", position);
                intent.putExtra("isSingle", true);
                startActivity(intent);
            }
        });

        bt_noCrimeMapForCF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("TAG", "FLAG4 " + flag_value);
                getCurrentLocation();
                showAlertForFlagProceed("CF", position);
            }
        });

        //setWarrant value
        setWarrantValue();

        //set status
        setPresentStatus();

        // set Instruction part
        setCommentInstructionPart();

        // set All Accused list
        setAllAccusedList();


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.rl_fir:
                isFIR_PDF = true;
                isDownloadPDF =true;
                //pdf_download_url="http://182.71.240.211/crimebabuapp/uploads/pmreport/CMCH-2017-1093.pdf";
                if (pdf_download_url.length() > 0){


                        Intent intent = new Intent(getActivity(), PDFLoadActivity.class);
                        intent.putExtra("PDF_DOWNLOAD_URL", pdf_download_url);
                        intent.putExtra("DOWNLOAD_PDF", isDownloadPDF);
                        intent.putExtra("REPORT_TYPE", isFIR_PDF);
                        intent.putExtra("USER_ID", user_Id);
                        intent.putExtra("PS_CODE", ps_code);
                        intent.putExtra("CASE_NO", case_no);
                        intent.putExtra("CASE_YEAR", case_year);
                        startActivity(intent);

                } else {
                    Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_PREVIEW_NOT_FOUND, false);
                }
                break;

            case R.id.rl_pm:
                isFIR_PDF = false;
                isDownloadPDF =true;
               // pmreport_pdf_download_url="http://182.71.240.211/crimebabuapp/uploads/pmreport/CMCH-2017-1093.pdf";

                if (pmreport_pdf_download_url.length() > 0) {

                            Intent intent = new Intent(getActivity(), PDFLoadActivity.class);
                            intent.putExtra("PDF_DOWNLOAD_URL", pmreport_pdf_download_url);
                            intent.putExtra("DOWNLOAD_PDF", isDownloadPDF);
                            intent.putExtra("REPORT_TYPE", isFIR_PDF);
                            intent.putExtra("USER_ID", user_Id);
                            intent.putExtra("PS_CODE", ps_code);
                            intent.putExtra("PM_NO", pm_no);
                            intent.putExtra("PM_YEAR", pm_yr);
                            startActivity(intent);

                } else {
                    Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_PREVIEW_NOT_FOUND, false);
                }
                break;

            case R.id.rl_case_log:
                popUpWindowForCaseTransferLog(position);
                break;

            case R.id.rl_log:
                downloadLog();
                break;

        }
    }


    //case transfer data show in pop up
    private void popUpWindowForCaseTransferLog(int item_pos) {

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflatedViewCaseTransfer = layoutInflater.inflate(R.layout.log_layout, null, false);

        linear_caseTranfer_CF = (LinearLayout) inflatedViewCaseTransfer.findViewById(R.id.linear_caseTranfer);

        int pos = item_pos;
        if (firDetails_model_recv.getCaseTransferList().size() > 0) {

            int sl_no = 0;
            int length = firDetails_model_recv.getCaseTransferList().size();
            for (int i = 0; i < length; i++) {

                String fromPS = "";
                String toPS = "";
                String fromIoName = "";
                String toIoName = "";
                String toUnit = "";
                String caseNo = "";
                String caseTransYr = "";
                String remarks = "";
                String transferDt = "";

                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = inflater.inflate(R.layout.fir_case_transfer_list_item, null);

                final TextView tv_SlNo = (TextView) v.findViewById(R.id.tv_SlNo);
                final LinearLayout linear_case_transfer_details = (LinearLayout) v.findViewById(R.id.linear_case_transfer_details);


                List<String> caseTransferItemDetailsList = new ArrayList<>();

                if (!firDetails_model_recv.getCaseTransferList().get(i).getFromPS().equals(null)) {
                    fromPS = "From PS :" + firDetails_model_recv.getCaseTransferList().get(i).getFromPS();
                    caseTransferItemDetailsList.add(fromPS);
                }

                /*if (!firDetails_model_recv.getCaseTransferList().get(i).getToPS().equals(null)) {
                    toPS = "To PS :" + firDetails_model_recv.getCaseTransferList().get(i).getToPS();
                    caseTransferItemDetailsList.add(toPS);
                }*/

                if (firDetails_model_recv.getCaseTransferList().get(i).getPsName() != null && !firDetails_model_recv.getCaseTransferList().get(i).getPsName().equalsIgnoreCase("null")
                        && !firDetails_model_recv.getCaseTransferList().get(i).getPsName().equalsIgnoreCase("")) {
                    toPS = "To PS :" + firDetails_model_recv.getCaseTransferList().get(i).getPsName();
                    caseTransferItemDetailsList.add(toPS);
                }

                if (!firDetails_model_recv.getCaseTransferList().get(i).getFromIoName().equals(null)) {
                    fromIoName = "From IO Name :" + firDetails_model_recv.getCaseTransferList().get(i).getFromIoName();
                    caseTransferItemDetailsList.add(fromIoName);
                }

                if (!firDetails_model_recv.getCaseTransferList().get(i).getToIoName().equals(null)) {
                    toIoName = "To IO Name :" + firDetails_model_recv.getCaseTransferList().get(i).getToIoName();
                    caseTransferItemDetailsList.add(toIoName);
                }

                if (!firDetails_model_recv.getCaseTransferList().get(i).getToUnit().equals(null)) {
                    toUnit = "To Unit :" + firDetails_model_recv.getCaseTransferList().get(i).getToUnit();
                    caseTransferItemDetailsList.add(toUnit);
                }

                if (!firDetails_model_recv.getCaseTransferList().get(i).getCaseNo().equals(null)) {
                    caseNo = "Case No :" + firDetails_model_recv.getCaseTransferList().get(i).getCaseNo();
                    caseTransferItemDetailsList.add(caseNo);
                }

                if (!firDetails_model_recv.getCaseTransferList().get(i).getCaseYr().equals(null)) {
                    caseTransYr = "Case Year :" + firDetails_model_recv.getCaseTransferList().get(i).getCaseYr();
                    caseTransferItemDetailsList.add(caseTransYr);
                }

                if (!firDetails_model_recv.getCaseTransferList().get(i).getTranferDt().equals(null)) {
                    transferDt = "Transfer Date :" + firDetails_model_recv.getCaseTransferList().get(i).getTranferDt();
                    caseTransferItemDetailsList.add(transferDt);
                }

                if (!firDetails_model_recv.getCaseTransferList().get(i).getRemarks().equals(null)) {
                    remarks = "Remarks :" + firDetails_model_recv.getCaseTransferList().get(i).getRemarks();
                    caseTransferItemDetailsList.add(remarks);
                }
                sl_no++;
                tv_SlNo.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_light_blue));
                tv_SlNo.setText("" + sl_no);

                for (int j = 0; j < caseTransferItemDetailsList.size(); j++) {

                    View itemDetailsView = inflater.inflate(R.layout.fir_case_transfer_list_item_details, null);

                    TextView tv_key = (TextView) itemDetailsView.findViewById(R.id.tv_key);
                    TextView tv_value = (TextView) itemDetailsView.findViewById(R.id.tv_value);
                    RelativeLayout relative_case_transfer_item_details = (RelativeLayout) itemDetailsView.findViewById(R.id.relative_case_transfer_item_details);

                    String data = caseTransferItemDetailsList.get(j);
                    String key = data.substring(0, data.indexOf(":"));
                    String value = data.substring(data.indexOf(":") + 1);

                    tv_key.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_light_blue));
                    tv_value.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_light_blue));

                    tv_key.setText(key);
                    tv_value.setText(value);

                    linear_case_transfer_details.addView(relative_case_transfer_item_details, j);
                }


                linear_caseTranfer_CF.addView(v);
                isCaseTransferAdded = true;

            }
            if (!isCaseTransferAdded) {
                Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_CASE_LOG, false);
            } else {
                openPopupWindowForCaseTransLog();
            }
        } else {

            Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_CASE_LOG, false);
        }
    }


    private void openPopupWindowForCaseTransLog() {

        rl_FIR_Details.setAlpha(0.1f);

        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // set height depends on the device size
        popWindowForCaseTransLog = new PopupWindow(inflatedViewCaseTransfer, width, height - 50, true);

        popWindowForCaseTransLog.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindowForCaseTransLog.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindowForCaseTransLog.setAnimationStyle(R.style.PopupAnimation);

        //to generate popUpWindow at bottom of ActionBar
        popWindowForCaseTransLog.showAtLocation(getActivity().getActionBar().getCustomView(), Gravity.CENTER, 0, 20);

        popWindowForCaseTransLog.setOutsideTouchable(true);
        popWindowForCaseTransLog.setFocusable(true);
        popWindowForCaseTransLog.getContentView().setFocusableInTouchMode(true);
        popWindowForCaseTransLog.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    if (popWindowForCaseTransLog != null && popWindowForCaseTransLog.isShowing()) {
                        popWindowForCaseTransLog.dismiss();

                    }

                    return true;
                }
                return false;
            }
        });

        popWindowForCaseTransLog.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //Do Something here
                rl_FIR_Details.setAlpha(1.0f);
            }
        });

    }


    private void downloadLog() {
        
        if(flag_value.equalsIgnoreCase("CF")){

            String psCode_CF = "";
            String caseNo_CF = "";
            String caseYr_CF = "";


            if (firDetails_model_recv.getPs() != null && !firDetails_model_recv.getPs().equalsIgnoreCase("")
                    && !firDetails_model_recv.getPs().equalsIgnoreCase("null")) {

                psCode_CF = firDetails_model_recv.getPs();
            }

            if (firDetails_model_recv.getCaseNo() != null && !firDetails_model_recv.getCaseNo().equalsIgnoreCase("")
                    && !firDetails_model_recv.getCaseNo().equalsIgnoreCase("null")) {

                caseNo_CF = firDetails_model_recv.getCaseNo();
            }

            if (firDetails_model_recv.getCaseYr() != null && !firDetails_model_recv.getCaseYr().equalsIgnoreCase("")
                    && !firDetails_model_recv.getCaseYr().equalsIgnoreCase("null")) {

                caseYr_CF = firDetails_model_recv.getCaseYr();
            } else if (firDetails_model_recv.getFirYear() != null && !firDetails_model_recv.getFirYear().equalsIgnoreCase("")
                    && !firDetails_model_recv.getFirYear().equalsIgnoreCase("null")) {

                caseYr_CF = firDetails_model_recv.getFirYear();
            }
            getDownloadLogForFIR(psCode_CF, caseNo_CF, caseYr_CF);
        }
        
        if(flag_value.equalsIgnoreCase("OF")){

            String psCode_OF = "";
            String caseNo_OF = "";
            String caseYr_OF = "";

            if (firDetails_model_recv.getPs() != null && !firDetails_model_recv.getPs().equalsIgnoreCase("")
                    && !firDetails_model_recv.getPs().equalsIgnoreCase("null")) {

                psCode_OF = firDetails_model_recv.getPs();
            }

            if (firDetails_model_recv.getCaseNo() != null && !firDetails_model_recv.getCaseNo().equalsIgnoreCase("")
                    && !firDetails_model_recv.getCaseNo().equalsIgnoreCase("null")) {

                caseNo_OF = firDetails_model_recv.getCaseNo();
            }

            if (firDetails_model_recv.getCaseYr() != null && !firDetails_model_recv.getCaseYr().equalsIgnoreCase("")
                    && !firDetails_model_recv.getCaseYr().equalsIgnoreCase("null")) {

                caseYr_OF = firDetails_model_recv.getCaseYr();
            }
            else if(firDetails_model_recv.getFirYear() != null && !firDetails_model_recv.getFirYear().equalsIgnoreCase("")
                    && !firDetails_model_recv.getFirYear().equalsIgnoreCase("null")){
                caseYr_OF = firDetails_model_recv.getFirYear();
            }

            getDownloadLogForFIR(psCode_OF, caseNo_OF, caseYr_OF);
        }
        
        if(flag_value.equalsIgnoreCase("OC")){

            String psCode_OC = "";
            String caseNo_OC = "";
            String caseYr_OC = "";

            if (firDetails_model_recv.getPsNameAsCode() != null &&
                    !firDetails_model_recv.getPsNameAsCode().equalsIgnoreCase("") &&
                    !firDetails_model_recv.getPsNameAsCode().equalsIgnoreCase("null")) {
                psCode_OC = firDetails_model_recv.getPsNameAsCode();
            }
            if (firDetails_model_recv.getCase_ref() != null &&
                    !firDetails_model_recv.getCase_ref().equalsIgnoreCase("") &&
                    !firDetails_model_recv.getCase_ref().equalsIgnoreCase("null")) {
                caseNo_OC = firDetails_model_recv.getCase_ref();
            }
            if (firDetails_model_recv.getCrimeYear() != null &&
                    !firDetails_model_recv.getCrimeYear().equalsIgnoreCase("") &&
                    !firDetails_model_recv.getCrimeYear().equalsIgnoreCase("null")) {
                caseYr_OC = firDetails_model_recv.getCrimeYear();
            }
            getDownloadLogForFIR(psCode_OC, caseNo_OC, caseYr_OC);
        }

        
    }


    private void getDownloadLogForFIR(String psCode_Flag, String caseNo_Flag, String caseYr_Flag) {

        Log.e("Log Dwnload-- ", "Called Criminal");
        Log.e("value: ", "value1 " + psCode_Flag + " cNo:" + caseNo_Flag + " caseYr: " + caseYr_Flag);
        //Log.e();

        if (!psCode_Flag.equalsIgnoreCase("") && !caseNo_Flag.equalsIgnoreCase("") && !caseYr_Flag.equalsIgnoreCase("")) {

            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_CASE_FIRLOGLIST);
            taskManager.setCriminalFIRLogList(true);

            String keys[] = new String[]{"ps", "case_no", "case_yr"};
            String values[] = new String[]{psCode_Flag.trim(), caseNo_Flag.trim(), caseYr_Flag.trim()};

            taskManager.doStartTask(keys, values, true);
        } else {

            Log.e("Log Dwnload-- ", "not Called ");
            Log.e("value: ", "value2 " + psCode_Flag + " cNo:" + caseNo_Flag + " caseYr: " + caseYr_Flag);
            Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_DOWNLOAD_LOG, false);
        }
    }


    public void parseCriminalFIRLogListResponse(String response) {

        //System.out.println("parseCriminalFIRLogListResponse -" + response);
        //tv_downloadLog.setVisibility(View.VISIBLE);
        //linear_downloadLog.setVisibility(View.VISIBLE);

        log_response = response;

        if (response != null && !response.equals("")) {

            try {
                JSONObject jObj = new JSONObject(response);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    JSONArray resultAry = jObj.getJSONArray("result");

                    parseCaseFIRDownloadLog(resultAry);

                } else {

                    Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_DOWNLOAD_LOG, false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
            }
        }
    }


    private void parseCaseFIRDownloadLog(JSONArray resultAry) {

        firDownloadLogList.clear();
        for (int i = 0; i < resultAry.length(); i++) {

            //int slNo = 0;
            length = resultAry.length();

            FirLogListDetails firLogListDetails = new FirLogListDetails();
            try {
                JSONObject jObj = resultAry.getJSONObject(i);

                if (jObj.optString("CASENO") != null && !jObj.optString("CASENO").equalsIgnoreCase("") && !jObj.optString("CASENO").equalsIgnoreCase("null")) {
                    firLogListDetails.setCaseNo(jObj.optString("CASENO"));
                }

                if (jObj.optString("CASE_YR") != null && !jObj.optString("CASE_YR").equalsIgnoreCase("") && !jObj.optString("CASE_YR").equalsIgnoreCase("null")) {
                    firLogListDetails.setCaseYr(jObj.optString("CASE_YR"));
                }

                if (jObj.optString("EMAIL") != null && !jObj.optString("EMAIL").equalsIgnoreCase("") && !jObj.optString("EMAIL").equalsIgnoreCase("null")) {
                    firLogListDetails.setEmailId(jObj.optString("EMAIL"));
                }

                if (jObj.optString("LINK") != null && !jObj.optString("LINK").equalsIgnoreCase("") && !jObj.optString("LINK").equalsIgnoreCase("null")) {
                    firLogListDetails.setLink(jObj.optString("LINK"));
                }

                if (jObj.optString("MOBILE") != null && !jObj.optString("MOBILE").equalsIgnoreCase("") && !jObj.optString("MOBILE").equalsIgnoreCase("null")) {
                    firLogListDetails.setMobileNo(jObj.optString("MOBILE"));
                }

                if (jObj.optString("OTP") != null && !jObj.optString("OTP").equalsIgnoreCase("") && !jObj.optString("OTP").equalsIgnoreCase("null")) {
                    firLogListDetails.setOtp(jObj.optString("OTP"));
                }

                if (jObj.optString("PS") != null && !jObj.optString("PS").equalsIgnoreCase("") && !jObj.optString("PS").equalsIgnoreCase("null")) {
                    firLogListDetails.setPsCode(jObj.optString("PS"));
                }

                if (jObj.optString("REASON") != null && !jObj.optString("REASON").equalsIgnoreCase("") && !jObj.optString("REASON").equalsIgnoreCase("null")) {
                    firLogListDetails.setReason(jObj.optString("REASON"));
                }

                if (jObj.optString("RECORD_DATE") != null && !jObj.optString("RECORD_DATE").equalsIgnoreCase("") && !jObj.optString("RECORD_DATE").equalsIgnoreCase("null")) {
                    firLogListDetails.setRecordDate(jObj.optString("RECORD_DATE"));
                }

                if (jObj.optString("RECORD_TIME") != null && !jObj.optString("RECORD_TIME").equalsIgnoreCase("") && !jObj.optString("RECORD_TIME").equalsIgnoreCase("null")) {
                    firLogListDetails.setRecordTime(jObj.optString("RECORD_TIME"));
                }

                if (jObj.optString("RECORD_ID") != null && !jObj.optString("RECORD_ID").equalsIgnoreCase("") && !jObj.optString("RECORD_ID").equalsIgnoreCase("null")) {
                    firLogListDetails.setRecordId(jObj.optString("RECORD_ID"));
                }

                if (jObj.optString("RELATIONSHIP") != null && !jObj.optString("RELATIONSHIP").equalsIgnoreCase("") && !jObj.optString("RELATIONSHIP").equalsIgnoreCase("null")) {
                    firLogListDetails.setRelationShip(jObj.optString("RELATIONSHIP"));
                }

                if (jObj.optString("USERNAME") != null && !jObj.optString("USERNAME").equalsIgnoreCase("") && !jObj.optString("USERNAME").equalsIgnoreCase("null")) {
                    firLogListDetails.setUserName(jObj.optString("USERNAME"));
                }

                firDownloadLogList.add(firLogListDetails);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ShowContentFirLog(firDownloadLogList, length);
    }


    private void ShowContentFirLog(ArrayList<FirLogListDetails> firLogList, int length) {

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflatedViewDownloadLog = layoutInflater.inflate(R.layout.log_layout, null, false);

        linear_downloadLog_CF = (LinearLayout) inflatedViewDownloadLog.findViewById(R.id.linear_caseTranfer);
        final TextView tv_downloadLog = (TextView) inflatedViewDownloadLog.findViewById(R.id.tv_showCaseTransfer);
        tv_downloadLog.setText("FIR Download Log");

        int slNo = 0;
        if (firLogList.size() > 0) {

           // linear_downloadLog.setVisibility(View.VISIBLE);
            for (int i = 0; i < length; i++) {

                String name = "";
                String email = "";
                String mobile = "";
                String downloadDt_tm = "";
                String relationship = "";
                String reason = "";

                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = inflater.inflate(R.layout.fir_case_transfer_list_item, null);

                final TextView tv_SlNo = (TextView) v.findViewById(R.id.tv_SlNo);
                final LinearLayout linear_fir_log_details = (LinearLayout) v.findViewById(R.id.linear_case_transfer_details);

                List<String> criminalFirLogItemDetailsList = new ArrayList<>();

                if (firLogList.get(i).getUserName() != null && !firLogList.get(i).getUserName().equalsIgnoreCase("") && !firLogList.get(i).getUserName().equalsIgnoreCase("null")) {
                    name = "Name :" + firLogList.get(i).getUserName();
                    criminalFirLogItemDetailsList.add(name);
                }

                if (firLogList.get(i).getEmailId() != null && !firLogList.get(i).getEmailId().equalsIgnoreCase("") && !firLogList.get(i).getEmailId().equalsIgnoreCase("null")) {
                    email = "Email :" + firLogList.get(i).getEmailId();
                    criminalFirLogItemDetailsList.add(email);
                }

                if (firLogList.get(i).getMobileNo() != null && !firLogList.get(i).getMobileNo().equalsIgnoreCase("") && !firLogList.get(i).getMobileNo().equalsIgnoreCase("null")) {
                    mobile = "Mobile :" + firLogList.get(i).getMobileNo();
                    criminalFirLogItemDetailsList.add(mobile);
                }

                if (firLogList.get(i).getRecordDate() != null && !firLogList.get(i).getRecordDate().equalsIgnoreCase("") && !firLogList.get(i).getRecordDate().equalsIgnoreCase("null")
                        && firLogList.get(i).getRecordTime() != null && !firLogList.get(i).getRecordTime().equalsIgnoreCase("") && !firLogList.get(i).getRecordTime().equalsIgnoreCase("")) {
                    downloadDt_tm = "Download Dt & Tm :" + firLogList.get(i).getRecordDate() + " " + firLogList.get(i).getRecordTime();
                    criminalFirLogItemDetailsList.add(downloadDt_tm);
                }

                if (firLogList.get(i).getRelationShip() != null && !firLogList.get(i).getRelationShip().equalsIgnoreCase("") && !firLogList.get(i).getRelationShip().equalsIgnoreCase("null")) {
                    relationship = "Relationship :" + firLogList.get(i).getRelationShip();
                    criminalFirLogItemDetailsList.add(relationship);
                }

                if (firLogList.get(i).getReason() != null && !firLogList.get(i).getReason().equalsIgnoreCase("") && !firLogList.get(i).getReason().equalsIgnoreCase("null")) {
                    reason = "Reason :" + firLogList.get(i).getReason();
                    criminalFirLogItemDetailsList.add(reason);
                }

                slNo++;
                tv_SlNo.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_light_blue));
                tv_SlNo.setText("" + slNo);

                for (int j = 0; j < criminalFirLogItemDetailsList.size(); j++) {

                    View itemDetailsView = inflater.inflate(R.layout.fir_case_transfer_list_item_details, null);

                    TextView tv_key = (TextView) itemDetailsView.findViewById(R.id.tv_key);
                    TextView tv_value = (TextView) itemDetailsView.findViewById(R.id.tv_value);
                    RelativeLayout relative_fir_log_item_details = (RelativeLayout) itemDetailsView.findViewById(R.id.relative_case_transfer_item_details);

                    String data = criminalFirLogItemDetailsList.get(j);
                    String key = data.substring(0, data.indexOf(":"));
                    String value = data.substring(data.indexOf(":") + 1);

                    tv_key.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_light_blue));
                    tv_value.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_light_blue));

                    tv_key.setText(key);
                    tv_value.setText(value);

                    linear_fir_log_details.addView(relative_fir_log_item_details, j);
                }

              //  linear_downloadLog.addView(v);
            }
        }
        openPopupWindowForCaseDownloadLog();
    }


    private void openPopupWindowForCaseDownloadLog() {

        rl_FIR_Details.setAlpha(0.1f);

        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // set height depends on the device size
        popWindowForCaseDownloadLog = new PopupWindow(inflatedViewDownloadLog, width, height - 50, true);

        popWindowForCaseDownloadLog.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindowForCaseDownloadLog.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindowForCaseDownloadLog.setAnimationStyle(R.style.PopupAnimation);

        //to generate popUpWindow at bottom of ActionBar
        popWindowForCaseDownloadLog.showAtLocation(getActivity().getActionBar().getCustomView(), Gravity.CENTER, 0, 20);

        popWindowForCaseDownloadLog.setOutsideTouchable(true);
        popWindowForCaseDownloadLog.setFocusable(true);
        popWindowForCaseDownloadLog.getContentView().setFocusableInTouchMode(true);
        popWindowForCaseDownloadLog.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    if (popWindowForCaseDownloadLog != null && popWindowForCaseDownloadLog.isShowing()) {
                        popWindowForCaseDownloadLog.dismiss();

                    }
                    return true;
                }
                return false;
            }
        });

        popWindowForCaseDownloadLog.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //Do Something here
                rl_FIR_Details.setAlpha(1.0f);
            }
        });
    }


    /* Fetch current location */
    public Location getCurrentLocation() {

        try {
            locationManager = (LocationManager) getActivity()
                    .getSystemService(LOCATION_SERVICE);


            // Getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // Getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // No network provider is enabled
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
                    Log.d("Network", "Network");

                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }

                // If GPS enabled, get latitude/longitude using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }

            latitudeVal = latitude + "";
            longitudeVal = longitude + "";
            Log.e("Lat  -  Long: ", latitudeVal + " - " + longitudeVal);
            //Toast.makeText(getActivity(),"Lat  -  Long: "+latitudeVal+ " - "+ longitudeVal, Toast.LENGTH_SHORT).show();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }


    private void showAlertForFlagProceed(final String flag, final int position) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setTitle("Alert!");
        alertDialog.setMessage("Do you want to capture lat/long of current location and tag it to the P. O.?");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                //staticPosition = position;
                if (flag.equals("OF"))
                    fetchedLatLongSetForOF(position);
                else if (flag.equals("CF"))
                    fetchedLatLongSetForCF(position);
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        alertDialog.show();
    }


    private void fetchedLatLongSetForOF(int position) {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CAPTURE_LATONG);
        taskManager.setCaptureLatLongForOF(true);

        keys = new String[]{"ps", "case_no", "case_yr", "lat", "long", "userId"};
        values = new String[]{firDetails_model_recv.getPs_code().replace(" PS", "").trim(), firDetails_model_recv.getCaseNo().trim(), firDetails_model_recv.getFirUnderSectionDetailsList().get(0).getCaseYr().trim(), latitudeVal.trim(), longitudeVal.trim(), user_Id.trim()};

        taskManager.doStartTask(keys, values, true);
    }


    public void parseCapturedLatLongOFModifiedFIRDetailsResponse(String result) {

        //System.out.println("parseCapturedLatLongOFModifiedFIRDetailsResponse: " + result);

        if (result != null && !result.equals("")) {

            bt_crimeMapForOF.setVisibility(View.VISIBLE);
            bt_noCrimeMapForOF.setVisibility(View.GONE);

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {
                    JSONObject result_obj = jObj.getJSONObject("result");

                    firDetails_model_recv.setPoLat(result_obj.optString("PO_LAT"));
                    firDetails_model_recv.setPoLong(result_obj.optString("PO_LONG"));

                }
            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        } else {
            Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
        }
    }


    private void fetchedLatLongSetForCF(int position) {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CAPTURE_LATONG);
        taskManager.setCaptureLatLongForCF(true);

        keys = new String[]{"ps", "case_no", "case_yr", "lat", "long", "userId"};
        values = new String[]{firDetails_model_recv.getPs_code().replace(" PS", "").trim(), firDetails_model_recv.getCaseNo().trim(), firDetails_model_recv.getFirUnderSectionDetailsList().get(0).getCaseYr().trim(), latitudeVal.trim(), longitudeVal.trim(), user_Id.trim()};

        taskManager.doStartTask(keys, values, true);
    }


    public void parseCapturedLatLongCFModifiedFIRDetailsResponse(String result) {

        //System.out.println("parseCapturedLatLongCFModifiedFIRDetailsResponse: " + result);

        if (result != null && !result.equals("")) {

            bt_crimeMapForCF.setVisibility(View.VISIBLE);
            bt_noCrimeMapForCF.setVisibility(View.GONE);

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {
                    JSONObject result_obj = jObj.getJSONObject("result");

                    firDetails_model_recv.setPoLat(result_obj.optString("PO_LAT"));
                    firDetails_model_recv.setPoLong(result_obj.optString("PO_LONG"));

                }
            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        } else {
            Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }



    private void fetchAllDetail(int position, String item_name){

        String io_name ="";
        final int item_pos = position;
        String under_section = "";
        String warrentDetails = "";
        String category = "";
        //String chargeSheetDetails = "";
        String complainant = "";
        String complainant_address = "";
        String brief_fact = "";
        String cgr_details = "";
        String court = "";
        int under_section_data_length = firDetails_model_recv.getFirUnderSectionDetailsList().size();

        for (int i = 0; i < under_section_data_length; i++) {
            if (i == 0) {
                under_section = firDetails_model_recv.getFirUnderSectionDetailsList().get(i).getUnder_section().trim();
            } else {
                under_section = under_section + ", " + firDetails_model_recv.getFirUnderSectionDetailsList().get(i).getUnder_section().trim();
            }
        }


        try {
            category = firDetails_model_recv.getFirUnderSectionDetailsList().get(0).getCategory().trim();
            chargeSheetDetails = firDetails_model_recv.getChargeSheetDetailsList().get(0).getCharge_sheet_details().trim();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (firDetails_model_recv.getComplaint() != null && !firDetails_model_recv.getComplaint().toString().equalsIgnoreCase("null")
                && !firDetails_model_recv.getComplaint().toString().equalsIgnoreCase("")) {
            complainant = firDetails_model_recv.getComplaint().toString().trim();
        }

        if (firDetails_model_recv.getComplaint_address() != null && !firDetails_model_recv.getComplaint_address().toString().equalsIgnoreCase("null")
                && !firDetails_model_recv.getComplaint_address().toString().equalsIgnoreCase("")) {
            complainant_address = firDetails_model_recv.getComplaint_address().toString().trim();
        }

        if (firDetails_model_recv.getIo() != null && !firDetails_model_recv.getIo().toString().equalsIgnoreCase("null")
                && !firDetails_model_recv.getIo().toString().equalsIgnoreCase("")) {
            io_name = firDetails_model_recv.getIo().toString().trim();
        }

        String gde_details = firDetails_model_recv.getGd_details().toString().trim();

        if (firDetails_model_recv.getBrief_fact() != null && !firDetails_model_recv.getBrief_fact().toString().equalsIgnoreCase("null")
                && !firDetails_model_recv.getBrief_fact().toString().equalsIgnoreCase("")) {
            brief_fact = firDetails_model_recv.getBrief_fact().toString().trim();
        } else {
            brief_fact = "No data available";
        }

        if (firDetails_model_recv.getCgr_details() != null && !firDetails_model_recv.getCgr_details().toString().equalsIgnoreCase("null")
                && !firDetails_model_recv.getCgr_details().equalsIgnoreCase("")) {
            cgr_details = firDetails_model_recv.getCgr_details().trim();
        }

        if (firDetails_model_recv.getCourt() != null && !firDetails_model_recv.getCourt().toString().equalsIgnoreCase("null")
                && !firDetails_model_recv.getCourt().toString().equalsIgnoreCase("")) {
            court = firDetails_model_recv.getCourt().trim();
        }


        if (firDetails_model_recv.getWarrantDetailsList().size() > 0) {
            warrentDetails = firDetails_model_recv.getWarrantDetailsList().get(0).getWaType() + "-" + firDetails_model_recv.getWarrantDetailsList().get(0).getWaNo() + " of " + firDetails_model_recv.getWarrantDetailsList().get(0).getPs_name();
        }

        tv_listName.setText(item_name + " u/s " + under_section);
        tv_category.setText(category);
        tv_complaint.setText("Complainant: " + complainant);
        tv_gde.setText(gde_details);

        tv_BriefFact.setPadding(10, 5, 5, 5);
        tv_BriefFact.setText(brief_fact);

        tv_court.setText(court);
        tv_CGR.setText(cgr_details);

        if (!complainant_address.equalsIgnoreCase("")) {
            tv_complainantAddress.setVisibility(View.VISIBLE);
            tv_complainantAddress.setText(complainant_address);
        } else {
            tv_complainantAddress.setVisibility(View.GONE);
        }

        if(!io_name.equalsIgnoreCase("")){
            tv_io_name.setVisibility(View.VISIBLE);
            tv_io_name.setText("I.O.: "+io_name);
        }else{
            tv_io_name.setVisibility(View.GONE);
        }

        pdf_download_url = firDetails_model_recv.getPdf_url();

        if(pdf_download_url.length() == 0)
        {
            iv_pdf.setImageResource(R.drawable.pdf_fir_detail_deactivate);
            rl_fir.setOnClickListener(null);
        }
        pmreport_pdf_download_url = firDetails_model_recv.getPmReport_pdf_url();

        if(pmreport_pdf_download_url.length() == 0)
        {
            iv_pm_pdf.setImageResource(R.drawable.pdf_fir_detail_deactivate);
            rl_pm.setOnClickListener(null);
        }

        if(log_response.length() == 0)
        {
            iv_log.setImageResource(R.drawable.log_deactivate);
            rl_log.setOnClickListener(null);
        }

        if(firDetails_model_recv.getCaseTransferList().size() == 0)
        {
            iv_case_log.setImageResource(R.drawable.case_log_deactivate);
            rl_case_log.setOnClickListener(null);
        }
    }


    public void setWarrantValue() {

        String warrentDetails = "";

        if (firDetails_model_recv.getWarrantDetailsList().size() > 0) {
            warrentDetails = firDetails_model_recv.getWarrantDetailsList().get(0).getWaType() + "-" + firDetails_model_recv.getWarrantDetailsList().get(0).getWaNo() + " of " + firDetails_model_recv.getWarrantDetailsList().get(0).getPs_name();
        }

        if (firDetails_model_recv.getWarrantDetailsList().size() > 0) {

            String wa_status = "";

            if (firDetails_model_recv.getWarrantDetailsList().get(0).getWaStatus() != null && !firDetails_model_recv.getWarrantDetailsList().get(0).getWaStatus().equalsIgnoreCase("null") &&
                    firDetails_model_recv.getWarrantDetailsList().get(0).getWaStatus().equalsIgnoreCase("")) {

                wa_status = firDetails_model_recv.getWarrantDetailsList().get(0).getWaStatus();
            }

            tv_warrant.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
            Typeface boldTypeface = Typeface.defaultFromStyle(Typeface.BOLD);
            tv_warrant.setTypeface(boldTypeface);
            SpannableString mySpannableString = new SpannableString(warrentDetails);
            mySpannableString.setSpan(new UnderlineSpan(), 0, mySpannableString.length(), 0);

            if (!wa_status.equalsIgnoreCase(""))
                tv_warrant.setText(wa_status + " / " + mySpannableString);
            else
                tv_warrant.setText(mySpannableString);

        } else {
            tv_warrant.setText("No warrant found");
        }

        if (tv_warrant.getText().toString().trim().length() > 0 && !tv_warrant.getText().toString().trim().equalsIgnoreCase("No warrant found")) {

            final String item_name = tv_warrant.getText().toString().trim();

            tv_warrant.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    List<FIRWarrantDetails> firTotalWarrentDetailsList = new ArrayList<FIRWarrantDetails>();

                    firTotalWarrentDetailsList = firDetails_model_recv.getWarrantDetailsList();

                    Intent in = new Intent(getContext(), PopUpActivity.class);
                    in.putExtra("FIR_WARRANT_DETAILS_LIST", (Serializable) firTotalWarrentDetailsList);
                    in.putExtra("ITEM_NAME", item_name);
                    in.putExtra("FROM", "ModifiedFIRFragment_WarrantDetails");
                    startActivity(in);

                }
            });
        }
    }

    /*
        * Present status value set
        * */
    private void setPresentStatus() {

        String fir_status = firDetails_model_recv.getFir_status().toString().trim();

        // STATUS name change
        if (fir_status.equalsIgnoreCase(Constants.STATUS_INVESTIGATION_IN_PROGRESS))
            fir_status = Constants.STATUS_PENDING;
        else if (fir_status.equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_NON_COGNIZABLE))
            fir_status = Constants.STATUS_FR_NON_COG;
        else if (fir_status.equalsIgnoreCase(Constants.STATUS_CHARGESHEETED))
            fir_status = Constants.STATUS_CS;
        else if (fir_status.equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_CIVIL))
            fir_status = Constants.STATUS_FR_CIVIL;
        else if (fir_status.equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_TRUE))
            fir_status = Constants.STATUS_FRT;
        else if (fir_status.equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_FALSE))
            fir_status = Constants.STATUS_FR_FALSE;
        else if (fir_status.equalsIgnoreCase(Constants.STATUS_TRANSFERRED_TO_OTHER_UNIT)) {

            if (firDetails_model_recv.getCaseTransferList().size() > 0) {

                String trfd_psDetails = "";
                if (firDetails_model_recv.getCaseTransferList().get(0).getPsName() != null
                        && !firDetails_model_recv.getCaseTransferList().get(0).getPsName().equalsIgnoreCase("null")
                        && !firDetails_model_recv.getCaseTransferList().get(0).getPsName().equalsIgnoreCase("")) {

                    trfd_psDetails = firDetails_model_recv.getCaseTransferList().get(0).getPsName();

                    if (firDetails_model_recv.getCaseTransferList().get(0).getToUnit() != null
                            && !firDetails_model_recv.getCaseTransferList().get(0).getToUnit().equalsIgnoreCase("null")
                            && !firDetails_model_recv.getCaseTransferList().get(0).getToUnit().equalsIgnoreCase("")) {

                        trfd_psDetails = trfd_psDetails + ", " + firDetails_model_recv.getCaseTransferList().get(0).getToUnit();
                    }
                    fir_status = trfd_psDetails;
                } else if (firDetails_model_recv.getCaseTransferList().get(0).getToUnit() != null
                        && !firDetails_model_recv.getCaseTransferList().get(0).getToUnit().equalsIgnoreCase("null")
                        && !firDetails_model_recv.getCaseTransferList().get(0).getToUnit().equalsIgnoreCase("")) {

                    trfd_psDetails = firDetails_model_recv.getCaseTransferList().get(0).getToUnit();
                    fir_status = trfd_psDetails;
                } else {
                    fir_status = Constants.STATUS_TRFD;
                }
            } else {
                fir_status = Constants.STATUS_TRFD;
            }
        } else
            fir_status = firDetails_model_recv.getFir_status();


        if (!chargeSheetDetails.equalsIgnoreCase("")) {

            chargeSheetDetails = fir_status + " / " + chargeSheetDetails;
            SpannableString mySpannableString = new SpannableString(chargeSheetDetails);
            mySpannableString.setSpan(new UnderlineSpan(), 0, mySpannableString.length(), 0);
            tv_presentStatus.setText(mySpannableString);

            if (tv_presentStatus.getText().toString().trim().contains("on")) {

                tv_presentStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isPopWindowForChargeSheet = true;
                        popUpWindowForChargeSheetDetails(position, item_name);
                    }
                });
            }


        } else {

            tv_presentStatus.setText(fir_status);
        }
    }


    private void popUpWindowForChargeSheetDetails(int position, String itemName) {

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popUpWindow layout
        inflatedFIRviewChargeSheet = layoutInflater.inflate(R.layout.chargesheet_popup, null, false);

        TextView tv_itemName = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_itemName);
        TextView tv_chargesheetNo = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_chargesheetNo);
        TextView tv_courtValue = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_courtValue);
        TextView tv_actsValue = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_actsValue);
        TextView tv_typeValue = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_typeValue);
        TextView tv_supplementaryValue = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_supplementaryValue);
        TextView tv_briefFactValue = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_briefFactValue);
        TextView tv_watermark = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_watermark);

        LinearLayout linear_witness_row = (LinearLayout) inflatedFIRviewChargeSheet.findViewById(R.id.linear_witness_row);
        linear_witness_row.setVisibility(View.GONE);

        LinearLayout linear_propertySeizure_row = (LinearLayout) inflatedFIRviewChargeSheet.findViewById(R.id.linear_propertySeizure_row);
        linear_propertySeizure_row.setVisibility(View.GONE);

        LinearLayout linear_accusedChargesheeted_row = (LinearLayout) inflatedFIRviewChargeSheet.findViewById(R.id.linear_accusedChargesheeted_row);
        linear_accusedChargesheeted_row.setVisibility(View.GONE);

        LinearLayout relative_accusedNotChargesheeted = (LinearLayout) inflatedFIRviewChargeSheet.findViewById(R.id.relative_accusedNotChargesheeted);
        relative_accusedNotChargesheeted.setVisibility(View.GONE);

         /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);


        Constants.changefonts(tv_itemName, getContext(), "Calibri Bold.ttf");

        Utility.changefonts(tv_chargesheetNo, getActivity(), "Calibri Bold.ttf");

        tv_itemName.setText(itemName);
        //tv_chargesheetNo.setText(csHeaderString);

        String chargeSheetDetails = "";
        if (!firDetails_model_recv.getChargeSheetDetailsList().get(0).getFir_chgNo().equalsIgnoreCase("")) {
            chargeSheetDetails = "Chargesheet No. " + firDetails_model_recv.getChargeSheetDetailsList().get(0).getFir_chgNo();
        }

        if (!firDetails_model_recv.getChargeSheetDetailsList().get(0).getFir_date().equalsIgnoreCase("")) {
            chargeSheetDetails = chargeSheetDetails + " Dated " + firDetails_model_recv.getChargeSheetDetailsList().get(0).getFir_date();
        }

        tv_chargesheetNo.setText(chargeSheetDetails);
        tv_courtValue.setText(firDetails_model_recv.getChargeSheetDetailsList().get(0).getCourtOf());
        tv_actsValue.setText(firDetails_model_recv.getChargeSheetDetailsList().get(0).getActsSection());
        tv_typeValue.setText(firDetails_model_recv.getChargeSheetDetailsList().get(0).getFrType());
        tv_supplementaryValue.setText(firDetails_model_recv.getChargeSheetDetailsList().get(0).getSuppltmentaryOriginal());
        tv_briefFactValue.setText(firDetails_model_recv.getChargeSheetDetailsList().get(0).getBriefFacts());

        openPopupWindowForChargeSheet();
    }


    /*This method is used
  * to open a popup window for Charge Sheet
  * */
    private void openPopupWindowForChargeSheet() {

        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // set height depends on the device size
        popWindowForChargeSheet = new PopupWindow(inflatedFIRviewChargeSheet, width, height - 50, true);

        popWindowForChargeSheet.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindowForChargeSheet.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindowForChargeSheet.setAnimationStyle(R.style.PopupAnimation);

        //to generate popUpWindow at bottom of ActionBar
        // popWindowForChargeSheet.showAsDropDown(getActivity().getActionBar().getCustomView(), 0, 20);
        popWindowForChargeSheet.showAtLocation(getActivity().getActionBar().getCustomView(), Gravity.CENTER, 0, 20);

        popWindowForChargeSheet.setOutsideTouchable(true);
        popWindowForChargeSheet.setFocusable(true);
        popWindowForChargeSheet.getContentView().setFocusableInTouchMode(true);
        popWindowForChargeSheet.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    if (popWindowForChargeSheet != null && popWindowForChargeSheet.isShowing()) {
                        System.out.println("Popup window for charge sheet");
                        popWindowForChargeSheet.dismiss();
                    }
                    return true;
                }
                return false;
            }
        });
    }


    private void setCommentInstructionPart(){

        TextView tv_instruction_header = new TextView(getActivity());
        tv_instruction_header.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_black));
        Utility.changefonts(tv_instruction_header, getActivity(), "Calibri.ttf");

        CommentList.clear();
        CommentList.add("OC Comment:- " + firDetails_model_recv.getOc_comment().replace("null", "N/A"));
        CommentList.add("AC Comment:- " + firDetails_model_recv.getAc_comment().replace("null", "N/A"));
        CommentList.add("DC Comment:- " + firDetails_model_recv.getDc_comment().replace("null", "N/A"));

        //CommentList = totalCommentList.get(position);

        if (CommentList.size() > 0) {


            tv_instruction_header.setText("Officers as per hierarchy will be able to read/give their views/instructions.\n" +
                    "To be viewed/edited by:-\n ");

            linear_comments.addView(tv_instruction_header);
            linear_comments.setId(0);
        } else {
            tv_instruction_header.setText("No comment found");

            linear_comments.addView(tv_instruction_header);
            linear_comments.setId(0);
        }

        for (int i = 0; i < CommentList.size(); i++) {

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            EditText et_comment = new EditText(getActivity());
            et_comment.setText("  " + "\u2022" + "   " + CommentList.get(i).trim());
            et_comment.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_black));
            et_comment.setTextSize(14);

            params.setMargins(0, 0, 0, 10);
            et_comment.setLayoutParams(params);
            et_comment.setEnabled(false);
            Utility.changefonts(et_comment, getActivity(), "Calibri.ttf");
            linear_comments.addView(et_comment);
            linear_comments.setId(i + 1);

        }


        iv_edit_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

                    btn_comment_post.setVisibility(View.VISIBLE);

                    for (int i = 1; i <= 3; i++) {
                        ((EditText) linear_comments.getChildAt(i)).setEnabled(true);
                        ((EditText) linear_comments.getChildAt(i)).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.border));

                    }

                    final EditText et_OC_comment = ((EditText) linear_comments.getChildAt(1));
                    final EditText et_AC_comment = ((EditText) linear_comments.getChildAt(2));
                    final EditText et_DC_comment = ((EditText) linear_comments.getChildAt(3));

                    et_OC_comment.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count,
                                                      int after) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            if (!s.toString().contains("OC Comment:- ")) {
                                et_OC_comment.setText("  " + "\u2022" + "   " + "OC Comment:- ");
                                Selection.setSelection(et_OC_comment.getText(), et_OC_comment.getText().length());

                            }
                        }
                    });

                    et_AC_comment.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count,
                                                      int after) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                            if (!s.toString().contains("AC Comment:- ")) {
                                et_AC_comment.setText("  " + "\u2022" + "   " + "AC Comment:- ");
                                Selection.setSelection(et_AC_comment.getText(), et_AC_comment.getText().length());

                            }
                        }
                    });

                    et_DC_comment.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count,
                                                      int after) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                            if (!s.toString().contains("DC Comment:- ")) {
                                et_DC_comment.setText("  " + "\u2022" + "   " + "DC Comment:- ");
                                Selection.setSelection(et_DC_comment.getText(), et_DC_comment.getText().length());

                            }

                        }
                    });

                }

            }
        });

        btn_comment_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn_comment_post.setVisibility(View.GONE);

                String oc_comment = ((EditText) linear_comments.getChildAt(1)).getText().toString().trim();
                String ac_comment = ((EditText) linear_comments.getChildAt(2)).getText().toString().trim();
                String dc_comment = ((EditText) linear_comments.getChildAt(3)).getText().toString().trim();

                //Utility.showToast(getActivity(),oc_comment+"\n"+ac_comment+"\n"+dc_comment,"long");

                for (int i = 1; i <= 3; i++) {
                    ((EditText) linear_comments.getChildAt(i)).setEnabled(false);
                    ((EditText) linear_comments.getChildAt(i)).setBackgroundResource(0);

                }

                postCommentData(position, ps_code, case_no, case_year, oc_comment, ac_comment, dc_comment);

            }
        });

    }


    private void postCommentData(int position, String ps_code, String case_no, String case_year, String oc_comment, String ac_comment, String dc_comment) {

        pos = position;

        String modified_ocComment = oc_comment.substring(oc_comment.indexOf(":-") + 2).trim();
        String modified_acComment = ac_comment.substring(ac_comment.indexOf(":-") + 2).trim();
        String modified_dcComment = dc_comment.substring(dc_comment.indexOf(":-") + 2).trim();

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_POST_COMMENT);
        taskManager.setEditComment(true);
        String[] keys = {"ps", "caseno", "caseyr", "oc_comment", "ac_comment", "dc_comment"};
        //String[] values = {"HDV","288","2015","test","test","test"};
        String[] values = {ps_code.trim(), case_no.trim(), case_year.trim(), modified_ocComment.trim(), modified_acComment.trim(), modified_dcComment.trim()};
        taskManager.doStartTask(keys, values, true);

    }

    public void parseEditCommentDataResponse(String response) {

       // System.out.println("EditCommentDataResponse: " + response);

        if (response != null && !response.equals("")) {
            try {
                JSONObject jObj = new JSONObject(response);

                Utility.showToast(getActivity(), jObj.optString("message"), "long");

                if (jObj.optString("status").equalsIgnoreCase("1")) {

                    JSONObject result_obj = jObj.getJSONObject("result");
                    parseEditCommentResponse(result_obj);
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void parseEditCommentResponse(JSONObject result_obj) {

        firDetails_model_recv.setOc_comment(result_obj.optString("OC_COMMENT"));
        firDetails_model_recv.setAc_comment(result_obj.optString("AC_COMMENT"));
        firDetails_model_recv.setDc_comment(result_obj.optString("DC_COMMENT"));

    }


    private void setAllAccusedList(){

        if (firDetails_model_recv.getFirOtherAccusedDetailList().size() > 0) {

            int sl_no = 0;
            // add view to the layout
            int length = firDetails_model_recv.getFirOtherAccusedDetailList().size();
            for (int i = 0; i < length; i++) {


                String accused_detail = "";

                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View v = inflater.inflate(R.layout.fir_other_accused_list_item, null);

                final RelativeLayout relative_otherAccused = (RelativeLayout) v.findViewById(R.id.relative_otherAccused);
                relative_otherAccused.setId(i);
                final TextView tv_SlNo = (TextView) v.findViewById(R.id.tv_SlNo);
                final TextView tv_accused_details = (TextView) v.findViewById(R.id.tv_accused_details);
                final View divider_firItem = v.findViewById(R.id.divider_firItem);

                Utility.changefonts(tv_accused_details, getActivity(), "Calibri Bold.ttf");
                tv_accused_details.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_light_blue));

                if (!firDetails_model_recv.getFirOtherAccusedDetailList().get(i).getName().toUpperCase().contains("UNKNOWN") && !firDetails_model_recv.getFirOtherAccusedDetailList().get(i).getName().equalsIgnoreCase(criminal_name)) {

                    sl_no++;

                    accused_detail = firDetails_model_recv.getFirOtherAccusedDetailList().get(i).getName() + " " + firDetails_model_recv.getFirOtherAccusedDetailList().get(i).getAlias();

                    if (!firDetails_model_recv.getFirOtherAccusedDetailList().get(i).getFatherName().equalsIgnoreCase("null")) {
                        accused_detail = accused_detail + ", S/o " + firDetails_model_recv.getFirOtherAccusedDetailList().get(i).getFatherName();
                    }

                    if (!firDetails_model_recv.getFirOtherAccusedDetailList().get(i).getAddress().equalsIgnoreCase("null")) {
                        accused_detail = accused_detail + ", " + firDetails_model_recv.getFirOtherAccusedDetailList().get(i).getAddress() + ".";
                    }

              /*  accused_detail = firDetails_model_recv.getFirOtherAccusedDetailList().get(i).getName()+" "+firDetails_model_recv.getFirOtherAccusedDetailList().get(i).getAlias()+ ", S/o "+firDetails_model_recv.getFirOtherAccusedDetailList().get(i).getFatherName()
                        +", "+firDetails_model_recv.getFirOtherAccusedDetailList().get(i).getAddress()+"."  ;
*/
                    //tv_SlNo.setText("" + sl_no);
                    tv_SlNo.setVisibility(View.GONE);
                    tv_accused_details.setBackgroundResource(0);
                    tv_accused_details.setText(sl_no + ". " +accused_detail);

                    if (length == sl_no){
                        divider_firItem.setVisibility(View.GONE);
                    }
                    else{
                        if(sl_no == length -1)
                            divider_firItem.setVisibility(View.GONE);
                        else
                            divider_firItem.setVisibility(View.VISIBLE);
                    }

                    linear_otherAccused.addView(v);
                    isOtherAccuesedAdded = true;

                    final int clickPosition = i;
                    relative_otherAccused.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(firDetails_model_recv.getFirOtherAccusedDetailList().get(clickPosition).getOtherAccusedListItemData().get(clickPosition).getProvCrmNo().isEmpty()||firDetails_model_recv.getFirOtherAccusedDetailList().get(clickPosition).getOtherAccusedListItemData().get(clickPosition).getProvCrmNo().equalsIgnoreCase("null")||firDetails_model_recv.getFirOtherAccusedDetailList().get(clickPosition).getOtherAccusedListItemData().get(clickPosition).getProvCrmNo().equalsIgnoreCase(null)){
                                Utility.showAlertDialog(getActivity(),Constants.ERROR,Constants.ERROR_PRV_CRIMINAL_NO,false);

                            }else {

                                Intent intent = new Intent(getActivity(), CriminalDetailActivity.class);
                                intent.putExtra(Constants.OBJECT, firDetails_model_recv.getFirOtherAccusedDetailList().get(clickPosition).getOtherAccusedListItemData().get(clickPosition));
                                intent.putExtra("searchCase", "criminalSearch");
                                startActivity(intent);
                            }

                        }
                    });

                }
            }

            if (!isOtherAccuesedAdded) {
                TextView tv_noOtherAccussed = new TextView(getActivity());
                tv_noOtherAccussed.setText("  No data available");
                tv_noOtherAccussed.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_black));
                Utility.changefonts(tv_noOtherAccussed, getActivity(), "Calibri.ttf");


                linear_otherAccused.addView(tv_noOtherAccussed);
                linear_otherAccused.setGravity(Gravity.CENTER);
            }


        } else {
            TextView tv_noOtherAccussed = new TextView(getActivity());
            tv_noOtherAccussed.setText("  No data available");
            tv_noOtherAccussed.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_black));
            Utility.changefonts(tv_noOtherAccussed, getActivity(), "Calibri.ttf");

            linear_otherAccused.addView(tv_noOtherAccussed);
            linear_otherAccused.setGravity(Gravity.CENTER);
        }
    }
}
