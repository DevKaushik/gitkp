package com.kp.facedetection.model;

/**
 * Created by DAT-Asset-131 on 29-04-2016.
 */
public class ArrestDetails {
    public String getArrest_date() {
        return arrest_date;
    }

    public void setArrest_date(String arrest_date) {
        this.arrest_date = arrest_date;
    }

    public String getHalf() {
        return half;
    }

    public void setHalf(String half) {
        this.half = half;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }

    public String getArrestee() {
        return arrestee;
    }

    public void setArrestee(String arrestee) {
        this.arrestee = arrestee;
    }

    public String getAlias_name() {
        return alias_name;
    }

    public void setAlias_name(String alias_name) {
        this.alias_name = alias_name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getFather_hushband() {
        return father_hushband;
    }

    public void setFather_hushband(String father_hushband) {
        this.father_hushband = father_hushband;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCase_ref() {
        return case_ref;
    }

    public void setCase_ref(String case_ref) {
        this.case_ref = case_ref;
    }

    public String getPlace_arrest() {
        return place_arrest;
    }

    public void setPlace_arrest(String place_arrest) {
        this.place_arrest = place_arrest;
    }

    public String getCrime_head() {
        return crime_head;
    }

    public void setCrime_head(String crime_head) {
        this.crime_head = crime_head;
    }

    public String getTrnid() {
        return trnid;
    }

    public void setTrnid(String trnid) {
        this.trnid = trnid;
    }

    public String getPs_code() {
        return ps_code;
    }

    public void setPs_code(String ps_code) {
        this.ps_code = ps_code;
    }

    public String getRep_date() {
        return rep_date;
    }

    public void setRep_date(String rep_date) {
        this.rep_date = rep_date;
    }

    public String getCase_date() {
        return case_date;
    }

    public void setCase_date(String case_date) {
        this.case_date = case_date;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getLocal_outside() {
        return local_outside;
    }

    public void setLocal_outside(String local_outside) {
        this.local_outside = local_outside;
    }

    public String getUnder_section() {
        return under_section;
    }

    public void setUnder_section(String under_section) {
        this.under_section = under_section;
    }

    public String getArragainst() {
        return arragainst;
    }

    public void setArragainst(String arragainst) {
        this.arragainst = arragainst;
    }

    public String getCase_yr() {
        return case_yr;
    }

    public void setCase_yr(String case_yr) {
        this.case_yr = case_yr;
    }

    private String arrest_date="";
    private String half="";
    private String unit="";
    private String ps="";
    private String arrestee="";
    private String alias_name="";
    private String sex="";
    private String age="";
    private String father_hushband="";
    private String address="";
    private String case_ref="";
    private String place_arrest="";
    private String crime_head="";
    private String trnid="";
    private String ps_code="";
    private String rep_date="";
    private String case_date="";
    private String religion="";
    private String profession="";
    private String local_outside="";
    private String under_section="";
    private String arragainst="";
    private String case_yr="";

}
