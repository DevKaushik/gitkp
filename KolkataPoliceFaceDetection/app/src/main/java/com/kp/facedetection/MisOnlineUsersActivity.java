package com.kp.facedetection;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MisOnlineUsersActivity extends MisActivity {

    ArrayList<misDataModel> msDataModel;
    ListView listView;
    private static misCustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_online_users);

        listView=(ListView)findViewById(R.id.list);


        msDataModel= new ArrayList<>();

        onlineuserApiCall();
//        ParseOnlineUser();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                misDataModel misDataModel=  msDataModel.get(position);


            }
        });
    }

    private void onlineuserApiCall() {
        TaskManager taskManager = new TaskManager(MisOnlineUsersActivity.this);
        taskManager.setMethod(Constants.METHOD_ONLINE_USERS);
        taskManager.setOnlineUser(true);

        String[] keys = {};
        String[] values = {};
        taskManager.doStartTask(keys, values, true);
    }

    public void ParseOnlineUser(String result) {
        if (result != null && !result.equals("")) {
            Log.e("ONLINEUSERSTOTAL",result);
            try {
                JSONObject jObj = new JSONObject(result);

                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {
//                    JSONObject o = jObj.getJSONObject("result");
                    JSONArray resultArray=jObj.getJSONArray("result");
                    parseOnlineUserData(resultArray);
                }
                else{
                    Utility.showAlertDialog(MisOnlineUsersActivity.this,Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL,false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(MisOnlineUsersActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }

    private void parseOnlineUserData(JSONArray resultArray) {
        JSONObject jsonObj = null;

        for (int i = 0; i < resultArray.length(); i++) {

            try {
                jsonObj = resultArray.getJSONObject(i);

                String USER_NAME = jsonObj.getString("USER_NAME");
                String RANK = jsonObj.getString("RANK");
                String PSNAME = jsonObj.getString("PSNAME");
                String LOGIN_TIME = jsonObj.getString("LOGIN_TIME");

                Log.e("LIST", USER_NAME + RANK + PSNAME + LOGIN_TIME);
                msDataModel.add(new misDataModel(USER_NAME,PSNAME,RANK,LOGIN_TIME));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter= new misCustomAdapter(msDataModel,getApplicationContext());

        listView.setAdapter(adapter);

    }
}
