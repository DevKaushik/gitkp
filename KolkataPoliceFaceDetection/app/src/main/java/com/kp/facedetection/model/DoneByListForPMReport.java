package com.kp.facedetection.model;

import android.os.Parcel;
import android.os.Parcelable;


public class DoneByListForPMReport implements Parcelable{

    private String doneBy_op_code="";
    private String doneBy_op_name="";
    private String doneBy_station_code="";


    public DoneByListForPMReport(Parcel in) {
        doneBy_op_code = in.readString();
        doneBy_op_name = in.readString();
        doneBy_station_code = in.readString();
    }

    public static final Creator<DoneByListForPMReport> CREATOR = new Creator<DoneByListForPMReport>() {
        @Override
        public DoneByListForPMReport createFromParcel(Parcel in) {
            return new DoneByListForPMReport(in);
        }

        @Override
        public DoneByListForPMReport[] newArray(int size) {
            return new DoneByListForPMReport[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(doneBy_op_code);
        dest.writeString(doneBy_op_name);
        dest.writeString(doneBy_station_code);
    }

    public String getDoneBy_op_code() {
        return doneBy_op_code;
    }

    public void setDoneBy_op_code(String doneBy_op_code) {
        this.doneBy_op_code = doneBy_op_code;
    }

    public String getDoneBy_op_name() {
        return doneBy_op_name;
    }

    public void setDoneBy_op_name(String doneBy_op_name) {
        this.doneBy_op_name = doneBy_op_name;
    }

    public String getDoneBy_station_code() {
        return doneBy_station_code;
    }

    public void setDoneBy_station_code(String doneBy_station_code) {
        this.doneBy_station_code = doneBy_station_code;
    }
}
