package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.ModifiedDocumentGasDetails;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by DAT-165 on 05-12-2016.
 */
public class GasSearchAdapter extends BaseAdapter {

    private Context context;
    private List<ModifiedDocumentGasDetails> gasSearchDetailsList;

    public GasSearchAdapter(Context context, List<ModifiedDocumentGasDetails> gasSearchDetailsList) {
        this.context = context;
        this.gasSearchDetailsList = gasSearchDetailsList;
    }


    @Override
    public int getCount() {
        int size = (gasSearchDetailsList.size() > 0) ? gasSearchDetailsList.size() : 0;
        return size;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.modified_sdr_search_list_item, null);

            holder.tv_slNo = (TextView) convertView.findViewById(R.id.tv_slNo);
            holder.tv_consumerId = (TextView) convertView.findViewById(R.id.tv_name);  // set consumer id to name field
            holder.tv_consumerName = (TextView) convertView.findViewById(R.id.tv_address); //set name to address field
            holder.tv_consumerAddress = (TextView) convertView.findViewById(R.id.tv_mobileNo);  // set address to mobile field

            Constants.changefonts(holder.tv_slNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_consumerId, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_consumerName, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_consumerAddress, context, "Calibri.ttf");

            convertView.setTag(holder);
        }
        else{

            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);

        holder.tv_slNo.setText(sl_no);

        holder.tv_consumerId.setText("CONSUMER ID: "+gasSearchDetailsList.get(position).getConsumer_id().trim());
        holder.tv_consumerName.setText("CONSUMER NAME: "+gasSearchDetailsList.get(position).getConsumer_name().trim());
        holder.tv_consumerAddress.setText("ADDRESS: "+gasSearchDetailsList.get(position).getAddress().trim());

        return convertView;
    }


    class ViewHolder {

        TextView tv_slNo,tv_consumerName,tv_consumerAddress,tv_consumerId;


    }
}
