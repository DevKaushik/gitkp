package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.kp.facedetection.adapter.MapSearchAdapter;
import com.kp.facedetection.model.MapSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class MapSearchResultActivity extends BaseActivity implements View.OnClickListener, Observer {


    private String[] keys;
    private String[] values;
    private String pageno;
    private String totalResult = "";

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private List<MapSearchDetails> mapSearchDetailsList;
    private MapSearchAdapter mapSearchAdapter;

    private TextView tv_resultCount;
    private ListView lv_mapSearchList;
    private Button btnLoadMore;

    private TextView tv_watermark;
    private ImageView iv_showMap;

    private String[] key_map;
    private String[] value_map;

    private String[] keyMap_parse;
    private String[] valueMap_parse;
    private String totlaResultMap = "";
    private String pagenoMap = "";
    private ArrayList<MapSearchDetails> mapAllList = new ArrayList<MapSearchDetails>();

    KPFaceDetectionApplication kpFaceDetectionApplication;
    Context context;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_search_list_result_layout);
        ObservableObject.getInstance().addObserver(this);

        initialization();
        clickEvents();

    }

    private void initialization() {

        tv_resultCount = (TextView) findViewById(R.id.tv_resultCount);
        lv_mapSearchList = (ListView) findViewById(R.id.lv_mapSearchList);
        iv_showMap = (ImageView) findViewById(R.id.iv_showMap);

        // LoadMore button
        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");


        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);


        keys = getIntent().getStringArrayExtra("keys");
        values = getIntent().getStringArrayExtra("value");
        pageno = getIntent().getStringExtra("pageno");
        totalResult = getIntent().getStringExtra("totalResult");

        key_map = getIntent().getStringArrayExtra("key_map");
        value_map = getIntent().getStringArrayExtra("value_map");

        Bundle bundle = getIntent().getExtras();
        mapSearchDetailsList = bundle.getParcelableArrayList("MAP_SEARCH");

        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }


        mapSearchAdapter = new MapSearchAdapter(this, mapSearchDetailsList);
        lv_mapSearchList.setAdapter(mapSearchAdapter);
        mapSearchAdapter.notifyDataSetChanged();


        if (Integer.parseInt(totalResult) > 20) {

            // Adding Load More button to listview at bottom
            lv_mapSearchList.addFooterView(btnLoadMore);

        }


         /*  Set user name as Watermark  */
        tv_watermark = (TextView) findViewById(R.id.tv_watermark);
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-55);
    }


    private void clickEvents() {

        iv_showMap.setOnClickListener(this);

        btnLoadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Starting a new async task
                fetchMapSearchResultPagination();
            }
        });

    }


    private void fetchMapSearchResultPagination() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_MAP_SEARCH);
        taskManager.setMapSearchPagination(true);

        values[5] = (Integer.parseInt(pageno) + 1) + "";

        taskManager.doStartTask(keys, values, true);
    }


    public void parseMapSearchPagination(String result, String[] keys, String[] values) {

        //System.out.println("parseMapSearchPagination: " + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("page").toString();
                    totalResult = jObj.opt("count").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseMapResponse(resultArray);

                } else {
                    showAlertDialog(MapSearchResultActivity.this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }


            } catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(MapSearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }


    private void parseMapResponse(JSONArray result_array) {

        for (int i = 0; i < result_array.length(); i++) {

            JSONObject obj = null;

            try {

                obj = result_array.getJSONObject(i);

                MapSearchDetails mapDetails = new MapSearchDetails(Parcel.obtain());

                if (!obj.optString("CASENO").equalsIgnoreCase("null"))
                    mapDetails.setCaseNo(obj.optString("CASENO"));

                if (!obj.optString("CASEDATE").equalsIgnoreCase("null"))
                    mapDetails.setCaseDate(obj.optString("CASEDATE"));

                if (!obj.optString("PSNAME").equalsIgnoreCase("null"))
                    mapDetails.setPsName(obj.optString("PSNAME"));

                if (!obj.optString("PS").equalsIgnoreCase("null"))
                    mapDetails.setPsCode(obj.optString("PS"));

                if (!obj.optString("CATEGORY").equalsIgnoreCase("null"))
                    mapDetails.setCrimeCat(obj.optString("CATEGORY"));

                if (!obj.optString("PO_LAT_DEGREE").equalsIgnoreCase("null"))
                    mapDetails.setLatitudeValue(obj.optString("PO_LAT_DEGREE"));

                if (!obj.optString("PO_LAT_MINUTE").equalsIgnoreCase("null"))
                    mapDetails.setLatMin(obj.optString("PO_LAT_MINUTE"));

                if (!obj.optString("PO_LAT_SECOND").equalsIgnoreCase("null"))
                    mapDetails.setLatSec(obj.optString("PO_LAT_SECOND"));

                if (!obj.optString("PO_LONG_DEGREE").equalsIgnoreCase("null"))
                    mapDetails.setLongitudeValue(obj.optString("PO_LONG_DEGREE"));

                if (!obj.optString("PO_LONG_MINUTE").equalsIgnoreCase("null"))
                    mapDetails.setLngMin(obj.optString("PO_LONG_MINUTE"));

                if (!obj.optString("PO_LONG_SECOND").equalsIgnoreCase("null"))
                    mapDetails.setLngSec(obj.optString("PO_LONG_SECOND"));

                if (!obj.optString("FIR_YR").equalsIgnoreCase("null"))
                    mapDetails.setFirYr(obj.optString("FIR_YR"));

                if (!obj.optString("PO_LAT").equalsIgnoreCase("null"))
                    mapDetails.setPoLat(obj.optString("PO_LAT"));

                if (!obj.optString("PO_LONG").equalsIgnoreCase("null"))
                    mapDetails.setPoLong(obj.optString("PO_LONG"));

                if (!obj.optString("COLOR_CODE").equalsIgnoreCase("null"))
                    mapDetails.setColorCode(obj.optString("COLOR_CODE"));

                mapSearchDetailsList.add(mapDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        int currentPosition = lv_mapSearchList.getFirstVisiblePosition();
        int lastVisiblePosition = lv_mapSearchList.getLastVisiblePosition();

        // Setting new scroll position
        lv_mapSearchList.setSelectionFromTop(currentPosition + 1, 0);

        if ((Integer.parseInt(totalResult) - lastVisiblePosition) < 20) {
            lv_mapSearchList.removeFooterView(btnLoadMore);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:

                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        ImageLoader.getInstance().destroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.iv_showMap:

                fetchMapView();
                break;
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }


    private void fetchMapView() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_MAP_VIEW);
        taskManager.setMapView(true);

        taskManager.doStartTask(key_map, value_map, true);
    }

    public void parseMapViewAll(String result, String[] keysMap, String[] valuesMap) {

        //System.out.println("parseMapViewAll: " + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keyMap_parse = keysMap;
                    this.valueMap_parse = valuesMap;
                    pagenoMap = jObj.opt("page").toString();
                    totlaResultMap = jObj.opt("count").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseMapViewAllResponse(resultArray);

                } else {
                    showAlertDialog(MapSearchResultActivity.this, Constants.ERROR_DETAILS_TITLE, "Sorry! No location available.", false);
                }


            } catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(MapSearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }

        }
    }


    private void parseMapViewAllResponse(JSONArray result_array) {

        kpFaceDetectionApplication = KPFaceDetectionApplication.getApplication();

        mapAllList.clear();
        for (int i = 0; i < result_array.length(); i++) {

            JSONObject obj = null;

            try {

                obj = result_array.getJSONObject(i);

                MapSearchDetails mapDetails = new MapSearchDetails(Parcel.obtain());

                if (!obj.optString("FIR_YR").equalsIgnoreCase("null"))
                    mapDetails.setFirYr(obj.optString("FIR_YR"));

                if (!obj.optString("CASENO").equalsIgnoreCase("null"))
                    mapDetails.setCaseNo(obj.optString("CASENO"));

                if (!obj.optString("PS").equalsIgnoreCase("null"))
                    mapDetails.setPsCode(obj.optString("PS"));

                if (!obj.optString("CATEGORY").equalsIgnoreCase("null"))
                    mapDetails.setCrimeCat(obj.optString("CATEGORY"));

                if (!obj.optString("PO_LAT").equalsIgnoreCase("null"))
                    mapDetails.setPoLat(obj.optString("PO_LAT"));

                if (!obj.optString("PO_LONG").equalsIgnoreCase("null"))
                    mapDetails.setPoLong(obj.optString("PO_LONG"));

                if (!obj.optString("COLOR_CODE").equalsIgnoreCase("null"))
                    mapDetails.setColorCode(obj.optString("COLOR_CODE"));

                mapAllList.add(mapDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.e("Map all list", mapAllList.size() + "");
        Intent intent = new Intent(MapSearchResultActivity.this, MapSearchDetailsActivity.class);
        intent.putExtra("keysMap", keyMap_parse);
        intent.putExtra("valueMap", valueMap_parse);
        intent.putExtra("totalResultMap", totlaResultMap);
        intent.putExtra("pagenoMap", pagenoMap);

        // when huge data need to intent using parcellable can not possible, required setter getter class in Application class
        kpFaceDetectionApplication.setMapAllListData(mapAllList);

        startActivity(intent);
    }


    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
