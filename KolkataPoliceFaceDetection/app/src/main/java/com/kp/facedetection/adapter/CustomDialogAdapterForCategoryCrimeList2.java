package com.kp.facedetection.adapter;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.model.CrimeCategoryList2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-Asset-145 on 19-05-2016.
 */
public class CustomDialogAdapterForCategoryCrimeList2 extends BaseAdapter {

    private List<CrimeCategoryList2> itemList = new ArrayList<CrimeCategoryList2>();
    private List<CrimeCategoryList2> searchItemList = new ArrayList<CrimeCategoryList2>();
    private Context context;
    private boolean[] thumbnailsselection;
    public static List<String> selectedItemList = new ArrayList<>();

    public CustomDialogAdapterForCategoryCrimeList2(Context c, List<CrimeCategoryList2> itemList) {

        selectedItemList.clear();
        searchItemList.clear();
        context = c;
        this.itemList = itemList;
        searchItemList.addAll(itemList);

        for (int i = 0; i < searchItemList.size(); i++) {
            searchItemList.get(i).setSelected(false);
        }

        thumbnailsselection = new boolean[searchItemList.size()];

        System.out.println("List Size: " + itemList.size());
    }


    @Override
    public int getCount() {
        if (searchItemList.size() > 0) {
            return searchItemList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.dialog_crime_category_list_item, null);


            holder.tv_itemName = (CheckedTextView) convertView
                    .findViewById(R.id.tv_itemName);

            holder.tv_itemHeader=(TextView)convertView.findViewById(R.id.tv_itemHeader);
            Constants.changefonts(holder.tv_itemHeader, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_itemName, context, "Calibri.ttf");

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_itemName.setId(position);


        holder.tv_itemName.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub

                CheckedTextView cb = (CheckedTextView) v;
                int id = cb.getId();
                int getPosition = (Integer) cb.getTag();

                if (thumbnailsselection[getPosition]) {
                    cb.setChecked(false);
                    thumbnailsselection[getPosition] = false;
                    selectedItemList.remove(searchItemList.get(getPosition).getCategory());
                    searchItemList.get(getPosition).setSelected(false);

                } else {
                    cb.setChecked(true);
                    thumbnailsselection[getPosition] = true;
                    selectedItemList.add(searchItemList.get(getPosition).getCategory());
                    searchItemList.get(getPosition).setSelected(true);
                }
            }
        });

        holder.tv_itemName.setTag(position);
        holder.tv_itemHeader.setVisibility(View.GONE);
        holder.tv_itemName.setVisibility(View.GONE);

        if(searchItemList.get(position).getCategory().equalsIgnoreCase("Frequently Used")||searchItemList.get(position).getCategory().equalsIgnoreCase("Others")){
            holder.tv_itemHeader.setVisibility(View.VISIBLE);
            holder.tv_itemName.setVisibility(View.GONE);
            //holder.tv_itemHeader.setText(searchItemList.get(position).getCategory());
            holder.content=new SpannableString(searchItemList.get(position).getCategory());
            holder.content.setSpan(new UnderlineSpan(), 0, searchItemList.get(position).getCategory().length(), 0);
            holder.tv_itemHeader.setText(holder.content);
        }else{
            holder.tv_itemHeader.setVisibility(View.GONE);
            holder.tv_itemName.setVisibility(View.VISIBLE);

            holder.tv_itemName.setText(searchItemList.get(position).getCategory());
            holder.tv_itemName.setChecked(searchItemList.get(position).isSelected());
        }
        //holder.tv_itemName.setText(searchItemList.get(position).getCategory());
        //holder.tv_itemName.setChecked(searchItemList.get(position).isSelected());
        holder.id = position;

        return convertView;

    }

    class ViewHolder {

        CheckedTextView tv_itemName;
        TextView tv_itemHeader;
        SpannableString content;
        int id;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        searchItemList.clear();
        if (charText.length() == 0) {
            searchItemList.addAll(itemList);
        } else {
            /*for (CrimeCategoryList2 item : itemList) {
                if (item.getCategory().toLowerCase().contains(charText)) {
                    searchItemList.add(item);
                }
            }*/

            for(int i=1;i<itemList.size();i++){
                CrimeCategoryList2 item = itemList.get(i);
                if(!item.getCategory().equalsIgnoreCase("Others")){
                    if (item.getCategory().toLowerCase().contains(charText)) {
                        searchItemList.add(item);
                    }
                }

            }
        }
        notifyDataSetChanged();
    }
}
