package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnCaptureLogDltListener;
import com.kp.facedetection.interfaces.OnCaptureTagClickForListener;
import com.kp.facedetection.interfaces.OnCaptureUnTagClickListener;
import com.kp.facedetection.interfaces.OnItemClickListenerForAllFIRView;
import com.kp.facedetection.model.CaptureLogListItemModel;
import com.kp.facedetection.model.CrimeReviewDetails;
import com.kp.facedetection.utility.Constants;

import java.util.ArrayList;

/**
 * Created by DAT-165 on 07-06-2017.
 */

public class CaptureLogListAdapter extends RecyclerView.Adapter<CaptureLogListAdapter.MyViewHolder> {

    Context con;
    ArrayList<CaptureLogListItemModel> captureLogList = new ArrayList<>();
    private OnCaptureTagClickForListener onCaptureTagClickForListener;
    private OnCaptureLogDltListener onCaptureLogDltListener;
    private OnCaptureUnTagClickListener onCaptureUnTagClickListener;

    public CaptureLogListAdapter(Context con, ArrayList<CaptureLogListItemModel> captureLogList) {
        this.con = con;
        this.captureLogList = captureLogList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.capture_log_list_item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final CaptureLogListItemModel captureDetails = captureLogList.get(position);


        // tag button set with text & enable or disable
        if (captureDetails.tag_status.equalsIgnoreCase("Untag")) {
            holder.bt_tag.setText("Untag");
        } else if (captureDetails.tag_status.equalsIgnoreCase("Tag")) {
            holder.bt_tag.setText("Tag");
        }

        if (captureDetails.address != null)
            holder.tv_address.setText(captureDetails.address);

        if (captureDetails.psName != null && !captureDetails.psName.equalsIgnoreCase(""))
            holder.tv_ps.setText(captureDetails.psName);
        else
            holder.tv_ps.setText("NA");

        if (captureDetails.note != null && !captureDetails.note.equalsIgnoreCase("null") && !captureDetails.note.equalsIgnoreCase(""))
            holder.tv_note.setText(captureDetails.note);
        else
            holder.tv_note.setText("NA");

    }

    @Override
    public int getItemCount() {
        return captureLogList == null ? 0 : captureLogList.size();
    }

    public void setCaptureTagClickListener(OnCaptureTagClickForListener onCaptureTagClickForListener) {
        this.onCaptureTagClickForListener = onCaptureTagClickForListener;
    }

    public void setCaptureUnTagListener(OnCaptureUnTagClickListener onCaptureUnTagClickListener) {
        this.onCaptureUnTagClickListener = onCaptureUnTagClickListener;
    }

    public void setCaptureLogDltListener(OnCaptureLogDltListener onCaptureLogDltListener) {
        this.onCaptureLogDltListener = onCaptureLogDltListener;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_address, tv_note, tv_ps;
        public Button bt_tag, bt_dlt;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_address = (TextView) itemView.findViewById(R.id.tv_address);
            tv_note = (TextView) itemView.findViewById(R.id.tv_note);
            tv_ps = (TextView) itemView.findViewById(R.id.tv_ps);

            bt_dlt = (Button) itemView.findViewById(R.id.bt_dlt);
            bt_dlt.setOnClickListener(this);

            bt_tag = (Button) itemView.findViewById(R.id.bt_tag);
            bt_tag.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            switch (view.getId()){

                case R.id.bt_tag:
                    if (bt_tag.getText().toString().equalsIgnoreCase("Tag")) {
                        if (onCaptureTagClickForListener != null) {
                            onCaptureTagClickForListener.onCaptureTag(view, getAdapterPosition());
                        }
                    }
                    else{
                        Log.e("UNTAG","UNTAG CALL ");
                        if(onCaptureUnTagClickListener != null){
                            onCaptureUnTagClickListener.onCaptureUnTag(view,getAdapterPosition());
                        }
                    }
                    break;

                case R.id.bt_dlt:
                    if(onCaptureLogDltListener != null){
                        onCaptureLogDltListener.onCaptureLogDlt(view,getAdapterPosition());
                    }
                    break;
            }
        }
    }
}
