package com.kp.facedetection.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.fragments.WarrantExcNBWFragment;
import com.kp.facedetection.interfaces.OnWarrantExcListListener;
import com.kp.facedetection.model.WarrantExecDetailModel;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by DAT-Asset-131 on 10-09-2016.
 */
public class DashboardWarrantExcPagerAdapterNEW extends PagerAdapter {

    Context context;
    private ArrayList<WarrantExecDetailModel> detailsList = new ArrayList<>();
    LayoutInflater mLayoutInflater;

    private TextView tv_waNoValue;
    private TextView tv_returnDateValue;
    private TextView tv_statusValue;
    private TextView tv_caseRefValue;

    private String caseRef = "";
    private int size;

    private String[] keysNBW;
    private String[] valuesNBW;
    String psCode, selectedDate;


    public DashboardWarrantExcPagerAdapterNEW(Context context, ArrayList<WarrantExecDetailModel> detailsList, int size, String psCode, String selectedDate) {
        this.context = context;
        this.detailsList = detailsList;
        this.size = size;
        this.psCode = psCode;
        this.selectedDate = selectedDate;
    }


    @Override
    public int getItemPosition (Object object)
    {
       return 1;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.nbw_item_layout, container, false);

        tv_waNoValue = (TextView) view.findViewById(R.id.tv_waNoValue);
        tv_returnDateValue = (TextView) view.findViewById(R.id.tv_returnDateValue);
        tv_statusValue = (TextView) view.findViewById(R.id.tv_statusValue);
        tv_caseRefValue = (TextView) view.findViewById(R.id.tv_caseRefValue);

        if(detailsList.get(position) != null){
            WarrantExecDetailModel warrantExecDetailModel = detailsList.get(position);
            if (!warrantExecDetailModel.getWaNo().equalsIgnoreCase("null")) {
                tv_waNoValue.setText(warrantExecDetailModel.getWaNo());
            }
            if (!warrantExecDetailModel.getReturnableDtaeToCourt().equalsIgnoreCase("null")) {
                tv_returnDateValue.setText(warrantExecDetailModel.getReturnableDtaeToCourt());
            }
            if (!warrantExecDetailModel.getWaStatus().equalsIgnoreCase("null")) {
                tv_statusValue.setText(warrantExecDetailModel.getWaStatus());
            }

            if (!warrantExecDetailModel.getPsCode().equalsIgnoreCase("null")) {
                caseRef = "Sec. " + warrantExecDetailModel.getPsCode();
            }
            if (!warrantExecDetailModel.getWaCaseNo().equalsIgnoreCase("null")) {
                caseRef = caseRef + " C/No. " + warrantExecDetailModel.getWaCaseNo();
            }
            if (!warrantExecDetailModel.getWaYear().equalsIgnoreCase("null")) {
                caseRef = caseRef + "\nof " + warrantExecDetailModel.getWaYear();
            }
            if (!warrantExecDetailModel.getUnderSec().equalsIgnoreCase("null")) {
                caseRef = caseRef + "\nu/s " + warrantExecDetailModel.getUnderSec();
            }

            tv_caseRefValue.setText(caseRef);
        }
        else{
            //warrantNbwAPICall();
            //listener.callWarranrDetailsApi(detailsList.get(position));


        }

        container.addView (view);
        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return size;
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


}
