package com.kp.facedetection.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.kp.facedetection.CaptureLogListActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.model.CaptureLogListItemModel;
import com.kp.facedetection.singletone_classes.CommunicationViews;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsForCaptureFIRFragment extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private Spinner sp_firYear;
    private TextView tv_ps_value;
    private EditText et_firNo;
    private Button btn_submit;
    private String firYear="";
    private String psCodeFromLogin = "";
    private String psName;
    private String psCode;


    private int position_val;
    private CaptureLogListItemModel captureDetails ;
    private List<String> caseYearList = new ArrayList<String>();
    ArrayAdapter<String> caseYearAdapter;

    public static final String TAG = DetailsForCaptureFIRFragment.class.getSimpleName();
    private String[] policeStationNameList = new String[Constants.policeStationNameArrayList.size()];
    private String[] policeStationIdList = new String[Constants.policeStationIDArrayList.size()];

    CaptureLogListActivity captureLogListActivity_Obj;


    public DetailsForCaptureFIRFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details_for_capture_fir, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        if(bundle != null){
            captureDetails = bundle.getParcelable(CaptureLogListActivity.KEY_DETAIL);
            position_val = bundle.getInt("POSITION_VAL",0);
        }


        sp_firYear = (Spinner) view.findViewById(R.id.sp_firYear);
        tv_ps_value = (TextView) view.findViewById(R.id.tv_ps_value);
        et_firNo = (EditText) view.findViewById(R.id.et_firNo);
        btn_submit = (Button) view.findViewById(R.id.btn_submit);
        Constants.buttonEffect(btn_submit);

        firYearValueSet();
        tv_ps_value.setText(captureDetails.psName);
        psCode = captureDetails.psCode;
        //Utility.showToast(getActivity(), "ID: "+captureDetails.log_id, "short");

        getPSFromLogin();
        setPSname();
        clickEvents();
    }

    private void getPSFromLogin(){

        try {

            if(Utility.getUserInfo(getActivity()).getUserPscode() != null && !Utility.getUserInfo(getActivity()).getUserPscode().equalsIgnoreCase("") && !Utility.getUserInfo(getActivity()).getUserPscode().equalsIgnoreCase("null")) {
                psCodeFromLogin = Utility.getUserInfo(getActivity()).getUserPscode();
            }
            else{
                psCodeFromLogin = "";
            }

            Log.e("CAPTURE:"," psCode: "+ psCode + " : "+ psCodeFromLogin );
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setPSname(){

        if(!psCodeFromLogin.equalsIgnoreCase("") && !psCodeFromLogin.equalsIgnoreCase("null")){

            if(psCodeFromLogin.equals("ORS")){
                //tv_ps_value.setText("Other Ps");
                tv_ps_value.setEnabled(true);
            }
            else {

                tv_ps_value.setEnabled(false);
                psCode = psCodeFromLogin;

                if((Constants.policeStationIDArrayList.size() > 0) && (Constants.policeStationNameArrayList.size() > 0)) {
                    int itemPossLoginPs = Constants.policeStationIDArrayList.indexOf(psCodeFromLogin);
                    tv_ps_value.setText(Constants.policeStationNameArrayList.get(itemPossLoginPs));
                }
                else{
                    //nothing to do
                }
            }
        }
        else {

        }
    }

    private void firYearValueSet() {

        Calendar calendar = Calendar.getInstance();
        int current_year = calendar.get(Calendar.YEAR);

        /* Case year Spinner set */
        caseYearList.clear();
        caseYearList.add("Enter FIR Year");

        for(int i=current_year;i>=1960;i--){
            caseYearList.add(Integer.toString(i));
        }

        caseYearAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_custom_textcolor, caseYearList);

        caseYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_firYear.setAdapter(caseYearAdapter);

        caseYearAdapter.notifyDataSetChanged();
        sp_firYear.setSelection(0);

        sp_firYear.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        firYear = sp_firYear.getSelectedItem().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        sp_firYear.setSelection(0);
    }


    private void clickEvents(){

        tv_ps_value.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tv_ps_value:
                showPSdialog();
                break;

            case R.id.btn_submit:
                submitToServer();
                break;
        }
    }


    private void showPSdialog() {

        if(Constants.policeStationNameArrayList.size() > 0){
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.myDialog);
            builder.setTitle("Select Police Station ");

            Object[] psNameListArray = Constants.policeStationNameArrayList.toArray();
            Object[] psIDListArray = Constants.policeStationIDArrayList.toArray();

            for(int i = 0; i < psNameListArray.length ; i++){
                policeStationNameList[i] = (String)psNameListArray[i];
                policeStationIdList[i] = (String)psIDListArray[i];
            }

            builder.setSingleChoiceItems(policeStationNameList, -1, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int pos) {

                    String selectedPSName = Arrays.asList(policeStationNameList).get(pos);
                    String selectedPSId = Arrays.asList(policeStationIdList).get(pos);
                    psName = selectedPSName;
                    psCode = selectedPSId;
                    tv_ps_value.setText(psName);
                }
            });

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void submitToServer(){

        String firNo ="";
        String firYr = "";
        firNo = et_firNo.getText().toString().trim();
        firYr = sp_firYear.getSelectedItem().toString().trim().replace("Enter FIR Year", "");

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CAPTURE_ADD_TO_TAG);
        taskManager.setCapturedTagAdd(true);


        if(!firNo.equalsIgnoreCase("") && !firYr.equalsIgnoreCase("") && !psCode.equalsIgnoreCase("")) {
            String[] keys = {"fir_no", "fir_year", "fir_ps", "id"};
            String[] values = {firNo.trim(), firYr.trim(), psCode.trim(), captureDetails.log_id.trim()};
            taskManager.doStartTask(keys, values, true);
            Log.e("All Data:","DATA:: "+firNo +" "+ firYr+ " "+psCode+" "+captureDetails.log_id);
        }
        else{
            Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MANDATE_FIELD_MSG, false);
            Log.e("All Data:","DATA:: "+firNo +" "+ firYr+ " "+psCode+" "+captureDetails.log_id);
        }
    }

    public void parseCaptureLogTaggedResponse(String response){
        //System.out.println("parseCaptureLogTaggedResponse: "+response);

        if(response != null && !response.equals("")) {

            try {
                JSONObject jObj = new JSONObject(response);

                if (jObj != null && jObj.optString("status").equalsIgnoreCase("1")) {

                    ((CaptureLogListActivity)getActivity()).updateTagStatus(position_val, psName, psCode);

                    getFragmentManager().beginTransaction().remove(this).commit();

                    //getActivity().finish();
                }
                else{
                    Utility.showAlertDialog(getActivity(), Constants.ERROR_CAPTURE_ALERT_TITLE, Constants.ERROR_CAPTURE_ALERT_MSG, false);
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
