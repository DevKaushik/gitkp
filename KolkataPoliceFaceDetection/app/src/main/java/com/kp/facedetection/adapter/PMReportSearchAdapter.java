package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.PMReportSearchDetails;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by DAT-165 on 01-11-2016.
 */
public class PMReportSearchAdapter extends BaseAdapter {


    private Context context;
    private List<PMReportSearchDetails> pmReportSearchDetailsList;

    public PMReportSearchAdapter(Context context, List<PMReportSearchDetails> pmReportSearchDetailsList) {
        this.context = context;
        this.pmReportSearchDetailsList = pmReportSearchDetailsList;
    }


    @Override
    public int getCount() {

        int size = (pmReportSearchDetailsList.size()>0)? pmReportSearchDetailsList.size():0;
        return size;

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.pm_report_list_item, null);


            holder.tv_slNo=(TextView)convertView.findViewById(R.id.tv_slNo);
            holder.tv_name=(TextView)convertView.findViewById(R.id.tv_name);
            holder.tv_ageInfo=(TextView)convertView.findViewById(R.id.tv_ageInfo);
            holder.tv_pmInfo=(TextView)convertView.findViewById(R.id.tv_pmInfo);
            holder.tv_stationInfo= (TextView)convertView.findViewById(R.id.tv_stationInfo);

            Constants.changefonts(holder.tv_slNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_name, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_ageInfo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_pmInfo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_stationInfo, context, "Calibri.ttf");

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);

        holder.tv_slNo.setText(sl_no);
        holder.tv_name.setText("Name: "+pmReportSearchDetailsList.get(position).getNameDeceased().trim());
        holder.tv_ageInfo.setText("Gender: "+pmReportSearchDetailsList.get(position).getGender().trim()+ "  Age: "+ pmReportSearchDetailsList.get(position).getAgeVal()+" "+pmReportSearchDetailsList.get(position).getAgeIndicator());
        holder.tv_pmInfo.setText("PM No: "+pmReportSearchDetailsList.get(position).getPmNo().trim()+"  PM Date: "+pmReportSearchDetailsList.get(position).getPmDt());
        holder.tv_stationInfo.setText("Station: "+pmReportSearchDetailsList.get(position).getStationName().trim());

        return convertView;
    }

    class ViewHolder {

        TextView tv_slNo,tv_name,tv_ageInfo,tv_pmInfo,tv_stationInfo,tv_opnameInfo;

    }
}
