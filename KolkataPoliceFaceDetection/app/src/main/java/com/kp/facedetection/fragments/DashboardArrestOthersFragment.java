package com.kp.facedetection.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kp.facedetection.CaseSearchFIRDetailsActivity;
import com.kp.facedetection.ModifiedCaseSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.adapter.ArrestSpecificListRecyclerAdapter;
import com.kp.facedetection.interfaces.OnArrestItemListener;
import com.kp.facedetection.model.CaseSearchDetails;
import com.kp.facedetection.model.SpecificArrestDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class DashboardArrestOthersFragment extends Fragment implements View.OnClickListener, OnArrestItemListener {

    private RecyclerView recycler_arrestOtherValue;
    private Button btnLoadMore;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrestSpecificListRecyclerAdapter otherAdapter;
    private TextView tv_noOtherArrestData;
    private View iv_dividerShow;

    ArrayList<SpecificArrestDetails> otherArrestDetailsArrayList = new ArrayList<>();
    int total;
    String[] keys;
    String[] values;
    String pageNo;
    String totalResult;

    private String caseRef = "";
    private String psCode = "";
    private String caseYear = "";
    private String psName = "";
    private String arrestDate = "";
    private String underSec = "";
    private String itemName = "";
    private String headerShow = "";
    private int position;

    public static DashboardArrestOthersFragment newInstance() {

        DashboardArrestOthersFragment DLSearchFragment = new DashboardArrestOthersFragment();
        return DLSearchFragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.specific_arrest_layout, container, false);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        iv_dividerShow = view.findViewById(R.id.iv_dividerShow);
        recycler_arrestOtherValue = (RecyclerView) view.findViewById(R.id.recycler_arrestSpecificValue);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_arrestOtherValue.setLayoutManager(mLayoutManager);
        recycler_arrestOtherValue.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        btnLoadMore = (Button) view.findViewById(R.id.bt_loadMore);
        tv_noOtherArrestData = (TextView) view.findViewById(R.id.tv_showNODetail);
    }



    public void addAllOtherData(ArrayList<SpecificArrestDetails> otherArrestDetailsArrayList, String countResult, String pageNo, String[] keys, String[] values) {
        this.otherArrestDetailsArrayList = otherArrestDetailsArrayList;
        otherAdapter = new ArrestSpecificListRecyclerAdapter(getActivity(), otherArrestDetailsArrayList);
        recycler_arrestOtherValue.setAdapter(otherAdapter);
        otherAdapter.notifyDataSetChanged();

        otherAdapter.setArrestItemClickListener(this);

        total = Integer.parseInt(countResult);
        this.keys = keys;
        this.values = values;
        this.pageNo = pageNo;

        if (total > 20) {
            btnLoadMore.setVisibility(View.VISIBLE);
        } else {
            btnLoadMore.setVisibility(View.GONE);
        }
        btnLoadMore.setOnClickListener(this);
    }


    public void setTextViewVisibleForOther(){
        if (otherArrestDetailsArrayList.size() > 0) {
            tv_noOtherArrestData.setVisibility(View.GONE);
            iv_dividerShow.setVisibility(View.VISIBLE);
        }
        else {
            iv_dividerShow.setVisibility(View.GONE);
            tv_noOtherArrestData.setVisibility(View.VISIBLE);
            tv_noOtherArrestData.setText("No other arrest detail found");
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_loadMore:
                fetchOtherDataPagination();
                break;
        }
    }


    private void fetchOtherDataPagination() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_OTHER_ARREST_VIEW);
        taskManager.setAllOtherArrestViewPagination(true);

        values[1] = (Integer.parseInt(pageNo) + 1) + "";

        taskManager.doStartTask(keys, values, true);
    }

    public void parseArrestOtherSearchPagination(String response) {
        //System.out.println("parseArrestOtherSearchPagination" + response);

        if (response != null && !response.equals("")) {
            try {
                JSONObject jObj = new JSONObject(response);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    //totalResult = jObj.opt("totalresult").toString();
                    pageNo = jObj.opt("pageno").toString();
                    JSONArray resultArray = jObj.optJSONArray("result");
                    parseAllSpecificArrestResponse(resultArray);
                } else {
                    Utility.showAlertDialog(getActivity(), Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }

    private void parseAllSpecificArrestResponse(JSONArray resultArray) {
        for (int i = 0; i < resultArray.length(); i++) {

            SpecificArrestDetails specificArrestDetails = new SpecificArrestDetails(Parcel.obtain());
            try {
                JSONObject jObj = resultArray.getJSONObject(i);
                if (jObj.optString("ADDRESS") != null && !jObj.optString("ADDRESS").equalsIgnoreCase("null") && !jObj.optString("ADDRESS").equalsIgnoreCase("")) {
                    specificArrestDetails.setAddress(jObj.optString("ADDRESS"));
                }

                if(jObj.optString("AGE") != null && !jObj.optString("AGE").equalsIgnoreCase("null") && !jObj.optString("AGE").equalsIgnoreCase("") && !jObj.optString("AGE").equalsIgnoreCase("NA")){
                    specificArrestDetails.setAge(jObj.optString("AGE"));
                }else{
                    specificArrestDetails.setAge("");
                }

                if (jObj.optString("ALIASNAME") != null && !jObj.optString("ALIASNAME").equalsIgnoreCase("null") && !jObj.optString("ALIASNAME").equalsIgnoreCase("")
                        && !jObj.optString("ALIASNAME").equalsIgnoreCase("NA") && !jObj.optString("ALIASNAME").equalsIgnoreCase("NIL")) {
                    specificArrestDetails.setAliasName(jObj.optString("ALIASNAME"));
                }
                if (jObj.optString("ARRAGAINST") != null && !jObj.optString("ARRAGAINST").equalsIgnoreCase("null") && !jObj.optString("ARRAGAINST").equalsIgnoreCase("")) {
                    specificArrestDetails.setArrestAgainst(jObj.optString("ARRAGAINST"));
                }
                if (jObj.optString("ARRESTEE") != null && !jObj.optString("ARRESTEE").equalsIgnoreCase("null") && !jObj.optString("ARRESTEE").equalsIgnoreCase("")) {
                    specificArrestDetails.setArrestee(jObj.optString("ARRESTEE"));
                }
                if (jObj.optString("ARREST_DATE") != null && !jObj.optString("ARREST_DATE").equalsIgnoreCase("null") && !jObj.optString("ARREST_DATE").equalsIgnoreCase("")) {
                    specificArrestDetails.setArrestDate(jObj.optString("ARREST_DATE"));
                }
                if (jObj.optString("CASEDATE") != null && !jObj.optString("CASEDATE").equalsIgnoreCase("null") && !jObj.optString("CASEDATE").equalsIgnoreCase("")) {
                    specificArrestDetails.setCaseDate(jObj.optString("CASEDATE"));
                }
                if (jObj.optString("CASEREF") != null && !jObj.optString("CASEREF").equalsIgnoreCase("null") && !jObj.optString("CASEREF").equalsIgnoreCase("")) {
                    specificArrestDetails.setCaseRef(jObj.optString("CASEREF"));
                }
                if (jObj.optString("CRIME_HEAD") != null && !jObj.optString("CRIME_HEAD").equalsIgnoreCase("null") && !jObj.optString("CRIME_HEAD").equalsIgnoreCase("")) {
                    specificArrestDetails.setCrimeHead(jObj.optString("CRIME_HEAD"));
                }
                if (jObj.optString("FATHER_HUSBAND") != null && !jObj.optString("FATHER_HUSBAND").equalsIgnoreCase("null") && !jObj.optString("FATHER_HUSBAND").equalsIgnoreCase("")) {
                    specificArrestDetails.setFatherName(jObj.optString("FATHER_HUSBAND"));
                }
                if (jObj.optString("PSNAME") != null && !jObj.optString("PSNAME").equalsIgnoreCase("null") && !jObj.optString("PSNAME").equalsIgnoreCase("")) {
                    specificArrestDetails.setPsName(jObj.optString("PSNAME"));
                }
                if (jObj.optString("SEX") != null && !jObj.optString("SEX").equalsIgnoreCase("null") && !jObj.optString("SEX").equalsIgnoreCase("") && !jObj.optString("SEX").equalsIgnoreCase("NA")) {
                    specificArrestDetails.setGender(jObj.optString("SEX"));
                }
                if (jObj.optString("UNDER_SECTION") != null && !jObj.optString("UNDER_SECTION").equalsIgnoreCase("null") && !jObj.optString("UNDER_SECTION").equalsIgnoreCase("")) {
                    specificArrestDetails.setUnderSection(jObj.optString("UNDER_SECTION"));
                }
                if(jObj.optString("PSCODE") != null && !jObj.optString("PSCODE").equalsIgnoreCase("null") && !jObj.optString("PSCODE").equalsIgnoreCase("")){
                    specificArrestDetails.setPsCode(jObj.optString("PSCODE"));
                }
                if(jObj.optString("CASE_YR") != null && !jObj.optString("CASE_YR").equalsIgnoreCase("null") && !jObj.optString("CASE_YR").equalsIgnoreCase("")){
                    specificArrestDetails.setCaseYr(jObj.optString("CASE_YR"));
                }
                if(jObj.optString("UNIT") != null && !jObj.optString("UNIT").equalsIgnoreCase("null") && !jObj.optString("UNIT").equalsIgnoreCase("")){
                    specificArrestDetails.setUNIT(jObj.optString("UNIT"));
                }
                if(jObj.optString("PS") != null && !jObj.optString("PS").equalsIgnoreCase("null") && !jObj.optString("PS").equalsIgnoreCase("")){
                    specificArrestDetails.setPS(jObj.optString("PS"));
                }


                JSONArray pic_url_array = jObj.optJSONArray("PICTURE_URL");
                List<String> pic_list = new ArrayList<>();
                if(pic_url_array != null) {
                    for (int k = 0; k < pic_url_array.length(); k++) {
                        pic_list.add(pic_url_array.optString(k));
                    }
                    specificArrestDetails.setPicture_list(pic_list);
                }

                otherArrestDetailsArrayList.add(specificArrestDetails);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        otherAdapter.notifyDataSetChanged();
        if (otherArrestDetailsArrayList.size() >= total) {
            btnLoadMore.setVisibility(View.GONE);
        }
    }

    @Override
    public void onArrestItemClick(View v, int pos) {

/*        SpecificArrestDetails otherDetails = otherArrestDetailsArrayList.get(pos);

        if(otherDetails.getPsName() != null && !otherDetails.getPsName().equalsIgnoreCase("") && !otherDetails.getPsName().equalsIgnoreCase("null")){
            psName = otherDetails.getPsName().toString().trim();
        }
        if(otherDetails.getArrestDate() != null && !otherDetails.getArrestDate().equalsIgnoreCase("") && !otherDetails.getArrestDate().equalsIgnoreCase("null")){
            arrestDate =" dated "+ otherDetails.getArrestDate().toString().trim();
        }
        if(otherDetails.getUnderSection() != null && !otherDetails.getUnderSection().equalsIgnoreCase("") && !otherDetails.getUnderSection().equalsIgnoreCase("null")){
            underSec = " u/s "+ otherDetails.getUnderSection().toString().trim();
        }

        if(otherDetails.getCaseRef() != null && !otherDetails.getCaseRef().equalsIgnoreCase("") && !otherDetails.getCaseRef().equalsIgnoreCase("null")) {
            caseRef = otherDetails.getCaseRef().toString().trim();
        }

        if(otherDetails.getPsCode() != null && !otherDetails.getPsCode().equalsIgnoreCase("") && !otherDetails.getPsCode().equalsIgnoreCase("null")){
            psCode = otherDetails.getPsCode().toString().trim();
        }
        if(otherDetails.getCaseYr() != null && !otherDetails.getCaseYr().equalsIgnoreCase("") && !otherDetails.getCaseYr().equalsIgnoreCase("null")){
            caseYear = otherDetails.getCaseYr().toString().trim();
        }

        itemName = psName+" C/No. "+caseRef + arrestDate + underSec;
        headerShow =psName+" C/No. "+caseRef + arrestDate;
        position = pos;

        if(!caseRef.equalsIgnoreCase("") && !caseYear.equalsIgnoreCase("") && !psCode.equalsIgnoreCase("")){

            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_MODIFIED_CASE_SEARCH_FIR_DETAILS);
            taskManager.setArrestOtherViewToCaseSearch(true);

            String[] keys = { "pscode", "caseno", "caseyr" };
            String[] values = {psCode, caseRef, caseYear};

            taskManager.doStartTask(keys, values, true);
        }
        else{
            Utility.showAlertDialog(getActivity(),Constants.ERROR_DETAILS_TITLE,Constants.ERROR_MSG_DETAIL,false);
        }*/
    }


    public  void parseArrestOtherDetailsForCase(String result){
        //System.out.println("parseArrestSpecificDetailsForCase"+result);
        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    Intent in = new Intent(getActivity(),CaseSearchFIRDetailsActivity.class);
                    in.putExtra("ITEM_NAME",itemName);
                    in.putExtra("HEADER_VALUE", headerShow);
                    in.putExtra("PS_CODE",psCode);
                    in.putExtra("CASE_NO", caseRef);
                    in.putExtra("CASE_YR", caseYear);
                    in.putExtra("POSITION",position+"");
                    in.putExtra("FROM_PAGE","DashboardArrestOtherFragment");

                    getActivity().startActivity(in);
                }
                else{
                    Utility.showAlertDialog(getActivity(),Constants.ERROR_DETAILS_TITLE,Constants.ERROR_MSG_DETAIL,false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(getActivity(),Constants.SEARCH_ERROR_TITLE,Constants.ERROR_EXCEPTION_MSG,false);
            }
        }

    }
}