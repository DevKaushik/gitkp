package com.kp.facedetection.interfaces;

import android.view.View;

/**
 * Created by DAT-165 on 08-06-2017.
 */

public interface OnCaptureLogDltListener {
    void onCaptureLogDlt(View view, int position);
}
