package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by user on 26-02-2018.
 */

public class HotelsList implements Serializable {
    private String hotel_name="";

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    private  boolean selected = false;

    public String getHotel_name() {
        return hotel_name;
    }

    public void setHotel_name(String hotel_name) {
        this.hotel_name = hotel_name;
    }
}
