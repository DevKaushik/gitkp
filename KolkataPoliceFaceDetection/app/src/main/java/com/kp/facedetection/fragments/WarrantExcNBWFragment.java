package com.kp.facedetection.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kp.facedetection.R;

/**
 * Created by Kaushik on 10-09-2016.
 */
public class WarrantExcNBWFragment extends Fragment {


    public static WarrantExcNBWFragment newInstance() {

        WarrantExcNBWFragment DLSearchFragment = new WarrantExcNBWFragment();
        return DLSearchFragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.nbw_item_layout, container, false);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

    }


}