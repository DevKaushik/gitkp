package com.kp.facedetection.interfaces;

import android.view.View;

/**
 * Created by user on 28-02-2018.
 */

public interface OnAllDocumentSearchTypeListener {
    void OnAllDocumentSearchTypeItemClick(View v, int pos);
}
