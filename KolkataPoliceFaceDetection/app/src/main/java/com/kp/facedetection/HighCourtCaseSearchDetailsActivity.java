package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.model.HCourtCaseDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class HighCourtCaseSearchDetailsActivity extends BaseActivity implements View.OnClickListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String sideName = "";
    private String caseDate = "";
    private String courtNo = "";
    private String justice = "";
    private String category = "";
    private String caseType = "";
    private String caseNo = "";
    private String caseYr = "";
    private String party = "";
    private String addLCase = "";
    private String petitioner = "";
    private String briefNote = "";

    private TextView tv_heading;
    private TextView tv_watermark;

    private TextView tv_sideValue, tv_CaseDtValue, tv_CourtNoValue, tv_Justice1Value, tv_CategoryValue,
            tv_CaseTypeValue, tv_CaseNoValue, tv_CaseYrValue, tv_PartyValue, tv_AddlCaseValue, tv_PetitionerValue, tv_BriefnoteValue;

    private List<HCourtCaseDetails> hCourtCaseDetailsList;
    int positionValue;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highcourt_case_details);
        ObservableObject.getInstance().addObserver(this);

        initialization();
    }


    /*
    *  Mapping all views
    * */
    private void initialization() {

        tv_sideValue = (TextView) findViewById(R.id.tv_sideValue);
        tv_CaseDtValue = (TextView) findViewById(R.id.tv_CaseDtValue);
        tv_CourtNoValue = (TextView) findViewById(R.id.tv_CourtNoValue);
        tv_Justice1Value = (TextView) findViewById(R.id.tv_Justice1Value);
        tv_CategoryValue = (TextView) findViewById(R.id.tv_CategoryValue);
        tv_CaseTypeValue = (TextView) findViewById(R.id.tv_CaseTypeValue);
        tv_CaseNoValue = (TextView) findViewById(R.id.tv_CaseNoValue);
        tv_CaseYrValue = (TextView) findViewById(R.id.tv_CaseYrValue);
        tv_PartyValue = (TextView) findViewById(R.id.tv_PartyValue);
        tv_AddlCaseValue = (TextView) findViewById(R.id.tv_AddlCaseValue);
        tv_PetitionerValue = (TextView) findViewById(R.id.tv_PetitionerValue);
        tv_BriefnoteValue = (TextView) findViewById(R.id.tv_BriefnoteValue);


        tv_heading = (TextView)findViewById(R.id.tv_heading);
        tv_watermark = (TextView)findViewById(R.id.tv_watermark);

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);


        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        Bundle bundle = getIntent().getExtras();
        hCourtCaseDetailsList = bundle.getParcelableArrayList("COURT_CASE_SEARCH_RESULT");
        positionValue = getIntent().getExtras().getInt("POSITION");

        if(hCourtCaseDetailsList.get(positionValue).getSideValue() != null)
            sideName = hCourtCaseDetailsList.get(positionValue).getSideValue().trim();

        if(hCourtCaseDetailsList.get(positionValue).getCaseDate() != null)
            caseDate = hCourtCaseDetailsList.get(positionValue).getCaseDate().trim();

        if(hCourtCaseDetailsList.get(positionValue).getCourtNo() != null)
            courtNo = hCourtCaseDetailsList.get(positionValue).getCourtNo().trim();

        if(hCourtCaseDetailsList.get(positionValue).getJustice1() != null)
            justice = hCourtCaseDetailsList.get(positionValue).getJustice1().trim();

        if(hCourtCaseDetailsList.get(positionValue).getJustice2() != null)
            justice = justice + ", " + hCourtCaseDetailsList.get(positionValue).getJustice2().trim();

        if(hCourtCaseDetailsList.get(positionValue).getJustice3() != null)
            justice = justice + ", " + hCourtCaseDetailsList.get(positionValue).getJustice3().trim();

        if(hCourtCaseDetailsList.get(positionValue).getCategory() != null)
            category = hCourtCaseDetailsList.get(positionValue).getCategory().trim();

        if(hCourtCaseDetailsList.get(positionValue).getCaseType() != null)
            caseType = hCourtCaseDetailsList.get(positionValue).getCaseType().trim();

        if(hCourtCaseDetailsList.get(positionValue).getCaseNo() != null)
            caseNo = hCourtCaseDetailsList.get(positionValue).getCaseNo().trim();

        if(hCourtCaseDetailsList.get(positionValue).getCaseYr() != null)
            caseYr = hCourtCaseDetailsList.get(positionValue).getCaseYr().trim();

        if(hCourtCaseDetailsList.get(positionValue).getParty() != null)
            party = hCourtCaseDetailsList.get(positionValue).getParty().trim();

        if(hCourtCaseDetailsList.get(positionValue).getAddLcase() != null)
            addLCase = hCourtCaseDetailsList.get(positionValue).getAddLcase().trim();

        if(hCourtCaseDetailsList.get(positionValue).getAdvPetitioner() != null)
            petitioner = hCourtCaseDetailsList.get(positionValue).getAdvPetitioner().trim();

        if(hCourtCaseDetailsList.get(positionValue).getBriefNote() != null)
            briefNote = hCourtCaseDetailsList.get(positionValue).getBriefNote().trim();


        tv_sideValue.setText(sideName);
        tv_CaseDtValue.setText(caseDate);
        tv_CourtNoValue.setText(courtNo);
        tv_Justice1Value.setText(justice);
        tv_CategoryValue.setText(category);
        tv_CaseTypeValue.setText(caseType);
        tv_CaseNoValue.setText(caseNo);
        tv_CaseYrValue.setText(caseYr);
        tv_PartyValue.setText(party);
        tv_AddlCaseValue.setText(addLCase);
        tv_PetitionerValue.setText(petitioner);
        tv_BriefnoteValue.setText(briefNote);


        Constants.changefonts(tv_sideValue, this, "Calibri.ttf");
        Constants.changefonts(tv_CaseDtValue, this, "Calibri.ttf");
        Constants.changefonts(tv_CourtNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_Justice1Value, this, "Calibri.ttf");
        Constants.changefonts(tv_CategoryValue, this, "Calibri.ttf");
        Constants.changefonts(tv_CaseTypeValue, this, "Calibri.ttf");
        Constants.changefonts(tv_CaseNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_CaseYrValue, this, "Calibri.ttf");
        Constants.changefonts(tv_PartyValue, this, "Calibri.ttf");
        Constants.changefonts(tv_AddlCaseValue, this, "Calibri.ttf");
        Constants.changefonts(tv_PetitionerValue, this, "Calibri.ttf");
        Constants.changefonts(tv_BriefnoteValue, this, "Calibri.ttf");

        Constants.changefonts(tv_heading, this, "Calibri Bold.ttf");

        /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this,Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }


    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
