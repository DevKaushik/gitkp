package com.kp.facedetection.interfaces;

import android.view.View;

/**
 * Created by DAT-Asset-128 on 04-12-2017.
 */

public interface OnItemClickListenerForChargesheetDue {

        void onClickChargesheetDue(View view, int position);

}
