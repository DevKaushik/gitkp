package com.kp.facedetection.fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.kp.facedetection.CaptureLogListActivity;
import com.kp.facedetection.MapActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.model.AllContentList;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.LOCATION_SERVICE;


public class CaptureFIRFragment extends Fragment implements LocationListener, View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, AdapterView.OnItemSelectedListener {

    private String[] policeStationNameList;
    private String[] policeStationIdList;
    private EditText et_note_value;
    private TextView tv_lat_value, tv_Long_value, tv_ps_value, tv_address_value;
    private Button btn_submit, btn_reset, btn_log;
    private LinearLayout linear_main, linear_footer;
    private RelativeLayout relative_noDetails;
    private Spinner sp_firYear;
    private EditText et_firNo;


    private String ioName;
    private String psCode = "";
    private String psCodeFromLogin = "";
    private String userId;
    private String psName;
    private String address;
    private String firYear="";

    double latitude; // Latitude
    double longitude; // Longitude


    private int itemPossLoginPs;
    public static final int MAP_ADDRESS_REQUEST_CODE = 123;
    private static final int LOCATION_REQUEST = 1414;
    private static final int ENABLE_GPS_REQUEST = 1515;
    private LatLng latLng;

    // Declaring a Location Manager
    protected LocationManager locationManager;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    public static final String TAG = CaptureFIRFragment.class.getSimpleName();
    public JSONArray policeStationListArray;

    private String[] keys;
    private String[] values;
    private List<String> caseYearList = new ArrayList<String>();
    ArrayAdapter<String> caseYearAdapter;


    public static CaptureFIRFragment newInstance() {
        CaptureFIRFragment captureFIRFragment = new CaptureFIRFragment();
        return captureFIRFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.capture_fir_layout, container, false);
        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        try {
            userId = Utility.getUserInfo(getActivity()).getUserId();
            ioName = Utility.getUserInfo(getActivity()).getName();

            if (Utility.getUserInfo(getActivity()).getUserPscode() != null && !Utility.getUserInfo(getActivity()).getUserPscode().equalsIgnoreCase("") && !Utility.getUserInfo(getActivity()).getUserPscode().equalsIgnoreCase("null")) {
                psCodeFromLogin = Utility.getUserInfo(getActivity()).getUserPscode();
            }

            Log.e("CAPTURE:", "ID: " + userId + " ioName: " + ioName + " psCode: " + psCodeFromLogin);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(psCodeFromLogin.equalsIgnoreCase("ORS")){
            fetchAllContent();
        }
        /*else{
            if (!Constants.internetOnline(getActivity())) {
                Utility.showAlertDialog(getActivity(), Constants.ERROR_TITLE, "Plese Connect Internet & Reload Page", false);
            }
        }*/

        sp_firYear = (Spinner) view.findViewById(R.id.sp_firYear);
        et_firNo = (EditText) view.findViewById(R.id.et_firNo);

        tv_address_value = (TextView) view.findViewById(R.id.tv_address_value);
        et_note_value = (EditText) view.findViewById(R.id.et_note_value);
        tv_lat_value = (TextView) view.findViewById(R.id.tv_lat_value);
        tv_Long_value = (TextView) view.findViewById(R.id.tv_Long_value);
        tv_ps_value = (TextView) view.findViewById(R.id.tv_ps_value);
        btn_submit = (Button) view.findViewById(R.id.btn_submit);
        btn_reset = (Button) view.findViewById(R.id.btn_reset);
        btn_log = (Button) view.findViewById(R.id.btn_log);
        Constants.buttonEffect(btn_submit);
        Constants.buttonEffect(btn_reset);
        Constants.buttonEffect(btn_log);

        firYearValueSet();
        //setPSname();
        setAddressLatLong();
        clickEvents();
    }

    private void firYearValueSet() {

        Calendar calendar = Calendar.getInstance();
        int current_year = calendar.get(Calendar.YEAR);

        /* Case year Spinner set */
        caseYearList.clear();
        caseYearList.add("Enter FIR Year");

        for(int i=current_year;i>=1960;i--){
            caseYearList.add(Integer.toString(i));
        }

        caseYearAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_custom_textcolor, caseYearList);

        caseYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_firYear.setAdapter(caseYearAdapter);

        caseYearAdapter.notifyDataSetChanged();
        sp_firYear.setSelection(0);

        sp_firYear.setOnItemSelectedListener(this);
    }




    private void setPSname() {

        if (!psCodeFromLogin.equalsIgnoreCase("") && !psCodeFromLogin.equalsIgnoreCase("null")) {

            if (psCodeFromLogin.equalsIgnoreCase("ORS")) {
                //tv_ps_value.setText("Other Ps");
                tv_ps_value.setEnabled(true);

            } else {

                psCode = psCodeFromLogin;
                tv_ps_value.setEnabled(false);

                if ((Constants.policeStationIDArrayList.size() > 0) && (Constants.policeStationNameArrayList.size() > 0)) {
                    itemPossLoginPs = Constants.policeStationIDArrayList.indexOf(psCodeFromLogin);
                    tv_ps_value.setText(Constants.policeStationNameArrayList.get(itemPossLoginPs));
                } else {
                    //nothing to do
                }
            }
        } else {
            //tv_ps_value.setEnabled(true);
        }
    }


    private void setAddressLatLong() {

        initLocationPermission();
        initLocationManager();
    }


    private void initLocationPermission() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            initGoogleApiClient();
            initLocationRequest();

        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
        }
    }


    private void initGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    // The next two lines tell the new client that “this” current class will handle connection stuff
                    //.addConnectionCallbacks(this)
                    //.enableAutoManage(getActivity(), 0 /* clientId */, this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    //fourth line adds the LocationServices API endpoint from GooglePlayServices
                    .addApi(Places.GEO_DATA_API)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    private void initLocationManager() {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
    }


    private void initLocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1000); // 1 second, in milliseconds
    }


    private void clickEvents() {
        tv_ps_value.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        btn_reset.setOnClickListener(this);
        btn_log.setOnClickListener(this);
        tv_address_value.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_ps_value:
                showPSdialog();
                break;

            case R.id.tv_address_value:
                mapViewOpen();
                break;

            case R.id.btn_submit:
                sendCapturedFIR();
                break;

            case R.id.btn_reset:
                resetData();
                break;

            case R.id.btn_log:
                openLogList();
                break;
        }
    }


    private void resetData() {
        sp_firYear.setSelection(0);
        et_firNo.setText("");
        et_note_value.setText("");
        if (psCodeFromLogin.equalsIgnoreCase("ORS")) {
            tv_ps_value.setText("");
            psCode = "";
        }

        initLocationWithGoogleApiClient();
    }


    private void mapViewOpen() {

        Intent intent = new Intent(getActivity(), MapActivity.class);
        intent.putExtra("LATITUDE", latitude + "");
        intent.putExtra("LONGITUDE", longitude + "");
        intent.putExtra("ADDRESS", tv_address_value.getText().toString().trim());
        //startActivity(intent);
        startActivityForResult(intent, MAP_ADDRESS_REQUEST_CODE);
    }


    private void sendCapturedFIR() {

        String note = et_note_value.getText().toString().trim();
        String address = tv_address_value.getText().toString().trim();
        String latVal = tv_lat_value.getText().toString().trim();
        String longVal = tv_Long_value.getText().toString().trim();
        String firNo = et_firNo.getText().toString().trim();
        String firYr = sp_firYear.getSelectedItem().toString().trim().replace("Enter FIR Year", "");

        Log.e("TAG1: **", "SHOW CAPTURE :" + address + " - " + latVal + " - " + longVal);


        if (!address.equalsIgnoreCase("")  ) {

            if(!psCode.equalsIgnoreCase("")){
                TaskManager taskManager = new TaskManager(this);
                taskManager.setMethod(Constants.METHOD_TEMP_FIR_CAPTURE);
                taskManager.setTempCapturedFIR(true);
                String[] keys = {"ps", "address", "io", "lat", "long", "note", "captured_by", "fir_no", "fir_year"};
                String[] values = {psCode.trim(), address.trim(), ioName.trim(), latVal.trim(), longVal.trim(), note.trim(), userId.trim(), firNo.trim(), firYr.trim()};
                taskManager.doStartTask(keys, values, true);
            }else {
                Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, "Please select police station", false);
            }

        } else {
            Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, "Please provide the address", false);
        }
    }


    public void parseCaptureFIRResponse(String response) {

        //System.out.println("parseCaptureFIRResponse" + response);
        JSONObject jobj = null;
        try {
            jobj = new JSONObject(response);
            if (jobj != null && jobj.optString("status").equalsIgnoreCase("1")) {

                alertForCaptureLocationSuccess(getActivity(), Constants.SUCCESS_CAPTURE_LOG_TITLE, Constants.SUCCESS_CAPTURE_LOG_MSG, true);

            } else {
                Utility.showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_CAPTURE_LOG_MSG, false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void alertForCaptureLocationSuccess(Context context, String title, String message, Boolean status) {
        final android.app.AlertDialog ad = new android.app.AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);


        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                            resetData();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    /* Address fetch */
    private String getCompleteAddressString(double lat, double lng) {

        String strAdd = "";
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.e("Current Aaddress", "" + strReturnedAddress.toString());
            } else {
                Log.e("Current Address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Current Address", "Canont get Address!");
        }
        return strAdd;
    }


    private void showPSdialog() {

        if (Constants.policeStationNameArrayList.size() > 0) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.myDialog);
            builder.setTitle("Select Police Station ");

            Object[] psNameListArray = Constants.policeStationNameArrayList.toArray();
            Object[] psIDListArray = Constants.policeStationIDArrayList.toArray();

            for (int i = 0; i < psNameListArray.length; i++) {
                policeStationNameList[i] = (String) psNameListArray[i];
                policeStationIdList[i] = (String) psIDListArray[i];
            }

            builder.setSingleChoiceItems(policeStationNameList, -1, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int pos) {

                    String selectedPSName = Arrays.asList(policeStationNameList).get(pos);
                    String selectedPSId = Arrays.asList(policeStationIdList).get(pos);
                    psName = selectedPSName;
                    psCode = selectedPSId;
                    tv_ps_value.setText(psName);
                }
            });

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case CaptureFIRFragment.MAP_ADDRESS_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    latLng = data.getExtras().getParcelable(MapActivity.LAT_LONG);
                    tv_lat_value.setText(latLng.latitude + "");
                    tv_Long_value.setText(latLng.longitude + "");
                    tv_address_value.setText(data.getExtras().getString(MapActivity.CHOSEN_ADDRESS));
                }
                break;

            case ENABLE_GPS_REQUEST:

                if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    Utility.showToast(getActivity(), "Please enable location", "short");
                    //openGpsSettings();
                }


                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0, this);

                break;
        }


        /*if (latLng != null) {
            L.e(latLng.latitude + " || " + latLng.longitude);
        }*/
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        initLocationWithGoogleApiClient();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    private void initLocationWithGoogleApiClient() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location != null) {

            latitude = location.getLatitude();
            longitude = location.getLongitude();

            address = getCompleteAddressString(latitude, longitude);

            tv_lat_value.setText(latitude + "");
            tv_Long_value.setText(longitude + "");
            tv_address_value.setText(address);

            /*if (address.equalsIgnoreCase("")) {
                //Utility.showAlertDialog(getActivity(), Constants.ERROR_CAPTURE_ALERT_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                return;
            } else {
                tv_address_value.setText(address);
            }*/

        } else {
            L.e("initLocationWithGoogleApiClient else");
            openGpsSettings();
        }
    }


    private void openGpsSettings() {
        L.e("openGpsSettings");

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        // **************************
        builder.setAlwaysShow(true); // this is the key ingredient
        // **************************

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                .checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        L.e("success");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        L.e("RESOLUTION_REQUIRED");
                        // Location settings are not satisfied. But could be
                        // fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling
                            // startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(getActivity(), ENABLE_GPS_REQUEST);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        L.e("SETTINGS_CHANGE_UNAVAILABLE");
                        // Location settings are not satisfied. However, we have
                        // no way to fix the
                        // settings so we won't show the dialog.
                        break;
                    case LocationSettingsStatusCodes.CANCELED:

                        L.e("CANCELED");

                        break;
                    case LocationSettingsStatusCodes.ERROR:

                        L.e("ERROR");
                        break;
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();

    }


    /*
    * for Log list API parse
    * */
    private void openLogList() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_LIST_CAPTURE);
        taskManager.setLogListCapturedFIR(true);

        keys = new String[]{"captured_by", "pageno"};
        values = new String[]{userId.trim(), "1"};
        //values = new String[]{"41", "1"};
        taskManager.doStartTask(keys, values, true);
    }

    public void parseLogListCaptureResponse(String response) {

        if(response == null && response.equals("")){
            Utility.showAlertDialog(getActivity(), Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
        }else {

            //System.out.println("parseLogListCaptureResponse::" + response);
            Intent i = new Intent(getActivity(), CaptureLogListActivity.class);
            i.putExtra("JSON_RESPONSE", response);
            i.putExtra("KEYS_PARAM", keys);
            i.putExtra("VALUES_PARAM", values);
            getActivity().startActivity(i);
        }
    }


    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }


    private void fetchAllContent() {

        if (!Constants.internetOnline(getActivity())) {
            Utility.showAlertDialog(getActivity(), Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_GETCONTENT);
        taskManager.setAllContentForCaptureLog(true);

        String[] keys = {};
        String[] values = {};
        taskManager.doStartTask(keys, values, true);
    }


    public void parseGetAllContentInCaptureFragmentResponse(String response) {

        //System.out.println("Get All Content Result: " + response);

        if (response != null && !response.equals("")) {

            try {

                Constants.policeStationIDArrayList.clear();
                Constants.policeStationNameArrayList.clear();

                JSONObject jObj = new JSONObject(response);

                if (jObj != null && jObj.optString("status").equalsIgnoreCase("1")) {

                    policeStationListArray = jObj.getJSONObject("result").getJSONArray("policestationlist");

                    for (int i = 0; i < policeStationListArray.length(); i++) {

                        JSONObject row = policeStationListArray.getJSONObject(i);
                        if(row.optString("PSNAME")!=null && !row.optString("PSNAME").equalsIgnoreCase("") && !row.optString("PSNAME").equalsIgnoreCase("")) {
                            Constants.policeStationNameArrayList.add(row.optString("PSNAME"));
                        }
                        if(row.optString("PSCODE")!=null && !row.optString("PSCODE").equalsIgnoreCase("") && !row.optString("PSCODE").equalsIgnoreCase("")) {
                            Constants.policeStationIDArrayList.add(row.optString("PSCODE"));
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        policeStationNameList = new String[Constants.policeStationNameArrayList.size()];
        policeStationIdList = new String[Constants.policeStationIDArrayList.size()];
        setPSname();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        firYear = sp_firYear.getSelectedItem().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        sp_firYear.setSelection(0);
    }
}
