package com.kp.facedetection.db;

/**
 * Created by Kaushik on 26-09-2016.
 */
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class KPHelperDB extends SQLiteOpenHelper {

    // The Android's default system path of your application database.

    //	private static String DB_PATH = "/data/data/com.dispensa.app/databases/";
    private static String DB_PATH = "";

    // Database name is defined here which is present in the assets folder
    private static String DB_NAME = "kp.db";

    // Making object of SQLiteDatabase
    private SQLiteDatabase myDataBase;

    // Making object of Context which is required to call this class
    private final Context myContext;

    // Constructor of this current class
    public KPHelperDB(Context context) {
        super(context, DB_NAME, null, 2);
        this.myContext = context;
    }

    public SQLiteDatabase MyDB() {
        return myDataBase;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * Check if the database already exist to avoid re-copying the file each
     * time you open the application.
     *
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase() {

        SQLiteDatabase checkDB = null;

        DB_PATH = myContext.getApplicationInfo().dataDir + "/databases/";
        String myPath = DB_PATH + DB_NAME;
        File file = new File(myPath);

        if (file.exists() && !file.isDirectory()) {

            try {
                //String myPath = DB_PATH + DB_NAME;
                checkDB = SQLiteDatabase.openDatabase(myPath, null,
                        SQLiteDatabase.CREATE_IF_NECESSARY);

            } catch (SQLiteException e) {
                // database does't exist yet.

            } catch (Exception e) {
                return false;
            }
        }
        else{
                checkDB = null;
        }

            if (checkDB != null) {

                checkDB.close();

            }


        return checkDB != null ? true : false;
    }

    private void copyDataBase() throws IOException {

        // Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DB_NAME);

        // Path to the just created empty db
        String outFileName = DB_PATH + DB_NAME;

        // Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        // transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public void openDataBase() throws SQLException {

        // Open the database
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READWRITE);

    }

    /**
     * Creates a empty database on the system and rewrites it with your own
     * database.
     * */
    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();

        if (dbExist) {
            // do nothing - database already exist

        } else {

            // By calling this method and empty database will be created into
            // the default system path
            // of your application so we are gonna be able to overwrite that
            // database with our database.
            this.getReadableDatabase();

            try {

                copyDataBase();

            } catch (IOException e) {

                throw new Error("Error copying database");

            }
        }

    }

}

