package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnAllDoumentSearchValueListener;
import com.kp.facedetection.model.CableSearchDetails;
import com.kp.facedetection.model.DLSearchDetails;
import com.kp.facedetection.model.KMCSearchDetails;
import com.kp.facedetection.utility.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 05-12-2016.
 */
public class KMCSearchAdapterModified extends BaseAdapter {

    private Context context;
    private List<KMCSearchDetails> kmcSearchDetailsList;
    List<KMCSearchDetails> searchItemList = new ArrayList<KMCSearchDetails>();
    OnAllDoumentSearchValueListener onAllDoumentSearchValueListener;


    public KMCSearchAdapterModified(Context context, List<KMCSearchDetails> kmcSearchDetailsList) {
        this.context = context;
        this.kmcSearchDetailsList = kmcSearchDetailsList;
        searchItemList.addAll(kmcSearchDetailsList);
    }

    public void setOnAllDocumentSearchValue(OnAllDoumentSearchValueListener onAllDoumentSearchValueListener){
        this.onAllDoumentSearchValueListener=onAllDoumentSearchValueListener;
    }
    @Override
    public int getCount() {
        int size = (searchItemList.size()>0)? searchItemList.size():0;
        return size;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.modified_sdr_search_list_item, null);
            holder.relative_main=(RelativeLayout)convertView.findViewById(R.id.relative_main);
            holder.tv_slNo = (TextView) convertView.findViewById(R.id.tv_slNo);
            holder.tv_wardNo = (TextView) convertView.findViewById(R.id.tv_name);  // set consumer id to name field
            holder.tv_ownerName = (TextView) convertView.findViewById(R.id.tv_address); //set name to address field
            holder.tv_address = (TextView) convertView.findViewById(R.id.tv_mobileNo);  // set address to mobile field
            holder.tv_id_number=(TextView) convertView.findViewById(R.id.tv_id_number);
            holder.tv_id_type=(TextView)convertView.findViewById(R.id.tv_id_type);
           // holder.tv_id_number.setVisibility(View.GONE);
           // holder.tv_id_type.setVisibility(View.GONE);
            Constants.changefonts(holder.tv_slNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_wardNo, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_ownerName, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_address, context, "Calibri.ttf");

            convertView.setTag(holder);
        }
        else{

            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);

        holder.tv_slNo.setText(sl_no);

        if(kmcSearchDetailsList.get(position).getKmc_wardNo() != null)
            holder.tv_wardNo.setText("WARD NO: "+searchItemList.get(position).getKmc_wardNo().trim());

        if(kmcSearchDetailsList.get(position).getKmc_owner() != null)
            holder.tv_ownerName.setText("OWNER NAME: "+searchItemList.get(position).getKmc_owner().trim());

        if(kmcSearchDetailsList.get(position).getKmc_mailingAddress() != null)
            holder.tv_address.setText("ADDRESS: "+searchItemList.get(position).getKmc_mailingAddress().trim());

        holder.relative_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onAllDoumentSearchValueListener!=null){
                    String uk=searchItemList.get(position).getKmc_mailingAddress();
                    onAllDoumentSearchValueListener.OnAllDoumentSearchValueItemClick(uk);
                }

            }
        });

        return convertView;
    }


    class ViewHolder {

        TextView tv_slNo,tv_ownerName,tv_address,tv_wardNo,tv_id_number,tv_id_type;
        RelativeLayout relative_main;
    }
    public void filter(String charText) {
        Log.d("JAY",charText);
        charText = charText.toLowerCase();
        searchItemList.clear();
        if (charText.length() == 0) {
            searchItemList.addAll(kmcSearchDetailsList);
        } else {
            for (KMCSearchDetails item : kmcSearchDetailsList) {
                if (item.getKmc_wardNo().contains(charText)|| item.getKmc_owner().toLowerCase().contains(charText)||item.getKmc_mailingAddress().toLowerCase().contains(charText)) {
                    searchItemList.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }
}
