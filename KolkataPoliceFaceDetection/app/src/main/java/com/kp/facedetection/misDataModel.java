package com.kp.facedetection;

public class misDataModel {

    String name;
    String posting;
    String rank;
    String logInTime;


    public misDataModel(String name, String posting, String rank, String logInTime ) {
        this.name=name;
        this.posting=posting;
        this.rank=rank;
        this.logInTime=logInTime;

    }


    public String getName() {
        return name;
    }


    public String getPosting() {
        return posting;
    }


    public String getRank() {
        return rank;
    }


    public String getLogInTime() {
        return logInTime;
    }
}


