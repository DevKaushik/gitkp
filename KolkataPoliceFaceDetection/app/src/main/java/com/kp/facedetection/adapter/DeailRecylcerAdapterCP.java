package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnItemClickListenerForADDLCP;
import com.kp.facedetection.interfaces.OnItemClickListenerForCP;
import com.kp.facedetection.interfaces.OnItemClickListenerforLIPartialSaveCP;
import com.kp.facedetection.model.ADDLCP;
import com.kp.facedetection.model.CP;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;

import java.util.List;

/**
 * Created by user on 10-04-2018.
 */

public class DeailRecylcerAdapterCP extends RecyclerView.Adapter<DeailRecylcerAdapterCP.ViewHolder>  {
    List<CP> dataCP;
    OnItemClickListenerForCP onItemClickListenerForCP;
    Context mContext;
    OnItemClickListenerforLIPartialSaveCP onItemClickListenerforLIPartialSaveCP;

    public DeailRecylcerAdapterCP(Context context, List<CP> dataCP) {
        this.mContext = context;
        this.dataCP = dataCP;
    }
    public void setOnItemClickListenerForCP(OnItemClickListenerForCP onItemClickListenerForCP){
        this.onItemClickListenerForCP=onItemClickListenerForCP;
    }
    public void setOnItemClickListenerforLIPartialSave( OnItemClickListenerforLIPartialSaveCP onItemClickListenerforLIPartialSaveCP){
        this.onItemClickListenerforLIPartialSaveCP = onItemClickListenerforLIPartialSaveCP;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.special_services_inflater, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.caseRef_TV.setText(dataCP.get(position).getCaseRef());
        holder.id_TV.setText(dataCP.get(position).getId());
        holder.reqType_TV.setText(dataCP.get(position).getRequestType());
        if(dataCP.get(position).getReconnection().equalsIgnoreCase("1")){
            holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[0]);
        }
        else if(dataCP.get(position).getDisconnection().equalsIgnoreCase("1")){
            holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[1]);
        }
        else {
            if(dataCP.get(position).getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(URGENT)");
            }
            else {
                holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(GENERAL)");
            }
          //  holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2]);
        }

        holder.div_TV.setText(dataCP.get(position).getDiv()+"/"+dataCP.get(position).getPs());
       /* String status=dataCP.get(position).getStatusMsg();
        if(status.equalsIgnoreCase(Constants.SPECIAL_SERVICE_REQUEST_STATUS_ARRAY[0])) {
            holder.status.setBackgroundColor(Color.parseColor("#d9534f"));
        }
        else if(status.equalsIgnoreCase(Constants.SPECIAL_SERVICE_REQUEST_STATUS_ARRAY[1])) {
            holder.status.setBackgroundColor(Color.parseColor("#d9534f"));
        }
        else if(status.equalsIgnoreCase(Constants.SPECIAL_SERVICE_REQUEST_STATUS_ARRAY[2])) {
            holder.status.setBackgroundColor(Color.parseColor("#d9534f"));
        }
        else if(status.equalsIgnoreCase(Constants.SPECIAL_SERVICE_REQUEST_STATUS_ARRAY[3])) {
            holder.status.setBackgroundColor(Color.parseColor("#5bc0de"));
        }
        else if(status.equalsIgnoreCase(Constants.SPECIAL_SERVICE_REQUEST_STATUS_ARRAY[4])) {
            holder.status.setBackgroundColor(Color.parseColor("#FFEA00"));
        }
        else if(status.equalsIgnoreCase(Constants.SPECIAL_SERVICE_REQUEST_STATUS_ARRAY[5])) {
            holder.status.setBackgroundColor(Color.parseColor("#01579B"));
        }
        else if(status.equalsIgnoreCase(Constants.SPECIAL_SERVICE_REQUEST_STATUS_ARRAY[6])) {
            holder.status.setBackgroundColor(Color.parseColor("#5cb85c"));
        }
        holder.status.setText(dataCP.get(position).getStatusMsg());
*/
        holder.status.setBackgroundColor(Color.parseColor(dataCP.get(position).getStatusColour()));
        holder.status.setText(dataCP.get(position).getStatusMsg());
        String date_time=dataCP.get(position).getRequestTime();
        String date="";
        if(date_time.contains(" ")) {
            String[] dateTimeArray= date_time.split(" ");
            date=dateTimeArray[0];
            String formatedDate= DateUtils.changeDateFormat(date);
            holder.date_time.setText(formatedDate);
        }
        Constants.changefonts(holder.name_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.id_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.caseRef_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.reqType_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.div_request_subtype_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.div_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.status_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.date_time_label, mContext,"Calibri Bold.ttf");

        Constants.changefonts(holder.name_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.id_TV, mContext,"Calibri.ttf");
        Constants.changefonts(holder.caseRef_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.reqType_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.div_request_subtype_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.div_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.status, mContext, "Calibri.ttf");
        Constants.changefonts(holder.date_time, mContext, "Calibri.ttf");

       /* if (position % 2 == 0) {
            holder.ll_special_service_request_item.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            holder.ll_special_service_request_item.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }*/



    }


    @Override
    public int getItemCount() {
        return (dataCP != null ? dataCP.size() : 0);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {
        TextView id_TV,caseRef_TV,name_TV,reqType_TV,div_TV,status,div_request_subtype_TV,date_time;
        TextView id_TV_label,caseRef_TV_label,name_TV_label,reqType_TV_label,div_TV_label,status_label,div_request_subtype_label,date_time_label;
        LinearLayout ll_special_service_request_item;


        public ViewHolder(View itemView) {
            super(itemView);
            ll_special_service_request_item=(LinearLayout)itemView.findViewById(R.id.ll_special_service_request_item);
            name_TV = (TextView) itemView.findViewById(R.id.name_TV);
            id_TV = (TextView) itemView.findViewById(R.id.id_TV);
            caseRef_TV = (TextView) itemView.findViewById(R.id.caseRef_TV);
            reqType_TV = (TextView) itemView.findViewById(R.id.reqType_TV);
            div_request_subtype_TV=(TextView) itemView.findViewById(R.id.div_request_subtype_TV);
            div_request_subtype_label=(TextView)itemView.findViewById(R.id.div_request_subtype_label);
            div_TV =(TextView) itemView.findViewById(R.id.div_TV);
            status = (TextView) itemView.findViewById(R.id.status);
            date_time = (TextView) itemView.findViewById(R.id.date_time);
            id_TV_label = (TextView) itemView.findViewById(R.id.id_TV_label);
            caseRef_TV_label=(TextView) itemView.findViewById(R.id.caseRef_TV_label);
            name_TV_label=(TextView) itemView.findViewById(R.id.name_TV_label);
            reqType_TV_label=(TextView) itemView.findViewById(R.id.reqType_TV_label);
            div_TV_label=(TextView) itemView.findViewById(R.id.div_TV_label);
            status_label=(TextView) itemView.findViewById(R.id.status_label);
            date_time_label=(TextView) itemView.findViewById(R.id.date_time_label);
            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            //onItemClickListenerForCP.onItemClickForCP(v,getLayoutPosition());
            if(dataCP.get(getLayoutPosition()).getDraftSave().equalsIgnoreCase("1")&& dataCP.get(getLayoutPosition()).getCompleteSave().equalsIgnoreCase("0")){
                onItemClickListenerforLIPartialSaveCP.onItemClickforLIPartialSaveCP(getLayoutPosition());
            }
            else if(dataCP.get(getLayoutPosition()).getDraftSave().equalsIgnoreCase("1")&& dataCP.get(getLayoutPosition()).getCompleteSave().equalsIgnoreCase("1")){
                onItemClickListenerForCP.onItemClickForCP(v,getLayoutPosition());
            }
            else {
                onItemClickListenerForCP.onItemClickForCP(v,getLayoutPosition());
            }
        }
    }
}
