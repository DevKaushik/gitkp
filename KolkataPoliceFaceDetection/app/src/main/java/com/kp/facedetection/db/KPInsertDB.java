package com.kp.facedetection.db;

import android.content.Context;
import android.database.SQLException;

/**
 * Created by DAT-Asset-131 on 27-09-2016.
 */
public class KPInsertDB {

    static KPHelperDB dbhlpr_obj;

    public boolean insertLatestAppVersion(Context mContext,String app_version,String time_stamp) {

        boolean check=false;

        try{

            dbhlpr_obj = new KPHelperDB(mContext);
            dbhlpr_obj.openDataBase();
            String query = "insert into app_info ('app_version','time_stamp') values('"+app_version+"','"+time_stamp+"')";
            dbhlpr_obj.MyDB().execSQL(
                    query);
            dbhlpr_obj.close();
            check=true;

            System.out.println("Data Inserted Successfully");


        } catch (SQLException e) {
            e.printStackTrace();
            dbhlpr_obj.close();
            return false;
        }
        return check;

    }

}
