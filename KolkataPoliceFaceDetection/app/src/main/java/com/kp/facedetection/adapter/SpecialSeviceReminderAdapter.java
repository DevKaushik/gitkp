package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.Detail;
import com.kp.facedetection.model.NotificationDetail;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by user on 09-05-2018.
 */

public class SpecialSeviceReminderAdapter extends RecyclerView.Adapter<SpecialSeviceReminderAdapter.ViewHolder> {
    private List<NotificationDetail> reminderList;
    Context context;

    public SpecialSeviceReminderAdapter(Context context,List<NotificationDetail> reminderList) {
        this.reminderList = reminderList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reminder_item_layout, parent, false);
        SpecialSeviceReminderAdapter.ViewHolder viewHolder = new SpecialSeviceReminderAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Constants.changefonts(holder.reminder_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.reminder_date_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.reminder_by_TV_label, context, "Calibri Bold.ttf");

        Constants.changefonts(holder.reminder_TV_label_right, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.reminder_date_TV_label_right, context, "Calibri Bold.ttf");
      //  Constants.changefonts(holder.reminder_by_TV_label_right, context, "Calibri Bold.ttf");

        Constants.changefonts(holder.reminder_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.reminder_date_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.reminder_TV_right, context, "Calibri.ttf");
        Constants.changefonts(holder.reminder_date_TV_right, context, "Calibri.ttf");
      //  Constants.changefonts(holder.reminder_by_TV_right, context, "Calibri.ttf");
        if(reminderList.get(position).getNotifyFlag().equalsIgnoreCase("2")){
            holder.ll_Left.setVisibility(View.VISIBLE);
            holder.ll_Right.setVisibility(View.GONE);
            holder.reminder_TV.setText(reminderList.get(position).getNotifyContent());
            holder.reminder_date_TV.setText(reminderList.get(position).getNotifyDateTime());
            holder.reminder_by_TV.setText(reminderList.get(position).getNotifyBy());
        }
        else if(reminderList.get(position).getNotifyFlag().equalsIgnoreCase("1")){
            holder.ll_Left.setVisibility(View.GONE);
            holder.ll_Right.setVisibility(View.VISIBLE);
            holder.reminder_TV_right.setText(reminderList.get(position).getNotifyContent());
            holder.reminder_date_TV_right.setText(reminderList.get(position).getNotifyDateTime());
           // holder.reminder_by_TV_right.setText(reminderList.get(position).getNotifyBy());
        }


    }

    @Override
    public int getItemCount() {
        return (reminderList != null ? reminderList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView reminder_TV_label,reminder_date_TV_label,reminder_by_TV_label,reminder_TV_label_right,reminder_date_TV_label_right;
       TextView reminder_TV,reminder_date_TV,reminder_by_TV,reminder_TV_right,reminder_date_TV_right,reminder_by_TV_right;
        LinearLayout ll_Right,ll_Left;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_Left=(LinearLayout) itemView.findViewById(R.id.ll_Left);
            ll_Right=(LinearLayout) itemView.findViewById(R.id.ll_Right);
            reminder_TV=(TextView)itemView.findViewById(R.id.reminder_TV);
            reminder_date_TV=(TextView)itemView.findViewById(R.id.reminder_date_TV);
            reminder_by_TV=(TextView)itemView.findViewById(R.id.reminder_by_TV);
            reminder_TV_label=(TextView)itemView.findViewById(R.id.reminder_TV_label);
            reminder_date_TV_label=(TextView)itemView.findViewById(R.id.reminder_date_TV_label);
            reminder_by_TV_label=(TextView)itemView.findViewById(R.id.reminder_by_TV_label);
            reminder_TV_label_right=(TextView)itemView.findViewById(R.id.reminder_TV_label_right);
            reminder_date_TV_label_right=(TextView)itemView.findViewById(R.id.reminder_date_TV_label_right);
            /*reminder_by_TV_label_right=(TextView)itemView.findViewById(R.id.reminder_by_TV_label_right);*/

            reminder_TV_right=(TextView)itemView.findViewById(R.id.reminder_TV_right);
            reminder_date_TV_right=(TextView)itemView.findViewById(R.id.reminder_date_TV_right);
           // reminder_by_TV_right=(TextView)itemView.findViewById(R.id.reminder_by_TV_right);
        }
    }
}

