package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.model.CableSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class CableSearchDetailsActivity extends BaseActivity implements View.OnClickListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private TextView tv_heading;
    private TextView tv_watermark;

    private TextView tv_OwnerNameValue, tv_AddressValue, tv_zoneValue, tv_phoneNoValue, tv_officeNoValue, tv_stbNoValue, tv_VCNoValue;

    private List<CableSearchDetails> cableSearchDeatilsList;
    int positionValue;

    private String holderName = "";
    private String holderAddress = "";
    private String zone = "";
    private String phoneNo = "";
    private String officeNo = "";
    private String stbNo = "";
    private String vcNo = "";
    TextView chargesheetNotificationCount;
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cable_search_details);
        ObservableObject.getInstance().addObserver(this);

        initialization();
    }


    /*
   *  Mapping all views
   * */
    private void initialization() {

        tv_OwnerNameValue = (TextView)findViewById(R.id.tv_OwnerNameValue);
        tv_AddressValue = (TextView)findViewById(R.id.tv_AddressValue);
        tv_zoneValue = (TextView)findViewById(R.id.tv_zoneValue);
        tv_phoneNoValue = (TextView)findViewById(R.id.tv_phoneNoValue);
        tv_officeNoValue = (TextView)findViewById(R.id.tv_officeNoValue);
        tv_stbNoValue = (TextView)findViewById(R.id.tv_stbNoValue);
        tv_VCNoValue = (TextView)findViewById(R.id.tv_VCNoValue);

        tv_heading = (TextView)findViewById(R.id.tv_heading);
        tv_watermark = (TextView)findViewById(R.id.tv_watermark);

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        Bundle bundle = getIntent().getExtras();
        cableSearchDeatilsList = bundle.getParcelableArrayList("CABLE_SEARCH_RESULT");
        positionValue = getIntent().getExtras().getInt("POSITION");

        if(cableSearchDeatilsList.get(positionValue).getCable_holderName() != null )
            holderName = cableSearchDeatilsList.get(positionValue).getCable_holderName().trim();

        if(cableSearchDeatilsList.get(positionValue).getCable_holderAddress() != null)
            holderAddress = cableSearchDeatilsList.get(positionValue).getCable_holderAddress().trim();

        if (cableSearchDeatilsList.get(positionValue).getCable_holderCity() != null)
            holderAddress = holderAddress + ", " + cableSearchDeatilsList.get(positionValue).getCable_holderCity().trim();

        if(cableSearchDeatilsList.get(positionValue).getCable_holderState() != null)
            holderAddress = holderAddress + ", "+ cableSearchDeatilsList.get(positionValue).getCable_holderState().trim();

        if (cableSearchDeatilsList.get(positionValue).getCable_holderPincode() != null)
            holderAddress = holderAddress + ", "+ cableSearchDeatilsList.get(positionValue).getCable_holderPincode().trim();

        if(cableSearchDeatilsList.get(positionValue).getCable_holderZone() != null)
            zone = cableSearchDeatilsList.get(positionValue).getCable_holderZone().trim();

        if(cableSearchDeatilsList.get(positionValue).getCable_holderMobileNo() != null)
            phoneNo = cableSearchDeatilsList.get(positionValue).getCable_holderMobileNo().trim();

        if(cableSearchDeatilsList.get(positionValue).getCable_holderOfficeNo()!=null)
            officeNo = cableSearchDeatilsList.get(positionValue).getCable_holderOfficeNo().trim();

        if(cableSearchDeatilsList.get(positionValue).getCable_holderSTBNo() != null)
            stbNo = cableSearchDeatilsList.get(positionValue).getCable_holderSTBNo().trim();

        if(cableSearchDeatilsList.get(positionValue).getCable_VCNo() != null)
            vcNo = cableSearchDeatilsList.get(positionValue).getCable_VCNo().trim();

        tv_OwnerNameValue.setText(holderName);
        tv_AddressValue.setText(holderAddress);
        tv_zoneValue.setText(zone);
        tv_phoneNoValue.setText(phoneNo);
        tv_officeNoValue.setText(officeNo);
        tv_stbNoValue.setText(stbNo);
        tv_VCNoValue.setText(vcNo);

        Constants.changefonts(tv_OwnerNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_AddressValue, this, "Calibri.ttf");
        Constants.changefonts(tv_zoneValue, this, "Calibri.ttf");
        Constants.changefonts(tv_phoneNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_officeNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_stbNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_VCNoValue, this, "Calibri.ttf");

        Constants.changefonts(tv_heading, this, "Calibri Bold.ttf");

 /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this,Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
