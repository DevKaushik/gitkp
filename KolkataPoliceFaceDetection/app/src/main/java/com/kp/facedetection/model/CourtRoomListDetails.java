package com.kp.facedetection.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DAT-165 on 24-03-2017.
 */

public class CourtRoomListDetails implements Parcelable {

    private String courtNo = "";
    private String numValue = "";

    public CourtRoomListDetails(Parcel in) {
        courtNo = in.readString();
        numValue = in.readString();
    }

    public static final Creator<CourtRoomListDetails> CREATOR = new Creator<CourtRoomListDetails>() {
        @Override
        public CourtRoomListDetails createFromParcel(Parcel in) {
            return new CourtRoomListDetails(in);
        }

        @Override
        public CourtRoomListDetails[] newArray(int size) {
            return new CourtRoomListDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(courtNo);
        dest.writeString(numValue);
    }

    public String getCourtNo() {
        return courtNo;
    }

    public void setCourtNo(String courtNo) {
        this.courtNo = courtNo;
    }

    public String getNumValue() {
        return numValue;
    }

    public void setNumValue(String numValue) {
        this.numValue = numValue;
    }
}
