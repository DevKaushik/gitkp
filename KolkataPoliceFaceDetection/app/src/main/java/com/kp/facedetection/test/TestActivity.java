package com.kp.facedetection.test;

import android.app.ActionBar;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.kp.facedetection.BaseActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.utility.Constants;

public class TestActivity extends BaseActivity {

    View inflatedFIRview;   //view used for popUpWindow
    private PopupWindow popWindow;

    private Button btn_popup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);


        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);


        initialization();
        clickEvents();

    }


    private void initialization(){

        btn_popup = (Button)findViewById(R.id.btn_popup);


    }


    private void clickEvents(){

        btn_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpWindowForFirListItem(v);
            }
        });

    }

    /*   Method to show PopUpWindow  */
    private void popUpWindowForFirListItem(View view) {

        LayoutInflater layoutInflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popUpWindow layout
        inflatedFIRview=layoutInflater.inflate(R.layout.warrent_details_popup,null,false);

        // get device size
        Display display = this.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // set height depends on the device size
        popWindow = new PopupWindow(inflatedFIRview, width,height-50, true );

        popWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindow.setHeight(WindowManager.LayoutParams.MATCH_PARENT);

        popWindow.setAnimationStyle(R.style.PopupAnimation);

        //to generate popUpWindow at bottom of ActionBar
        popWindow.showAsDropDown(this.getActionBar().getCustomView(),0,20);

        popWindow.setOutsideTouchable(true);
        popWindow.setFocusable(true);
        popWindow.getContentView().setFocusableInTouchMode(true);
        popWindow.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    System.out.println("back pressed while opened popup");
                    popWindow.dismiss();
                    return true;
                }
                return false;
            }
        });
    }

}
