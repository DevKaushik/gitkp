package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.model.KMCSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class KMCSearchDetailsActivity extends BaseActivity implements View.OnClickListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private  String slNo = "";
    private String wardNo = "";
    private String ownerName = "";
    private String premises = "";
    private String mailingAddress = "";

    private TextView tv_heading;
    private TextView tv_watermark;

    private TextView tv_SlNoValue, tv_WardNoValue, tv_OwnerNameValue, tv_PremisesValue, tv_addressValue;

    private List<KMCSearchDetails> kmcSearchDeatilsList;
    int positionValue;
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kmcsearch_details);
        ObservableObject.getInstance().addObserver(this);
        initialization();
    }


    /*
    *  Mapping all views
    * */
    private void initialization() {

        tv_SlNoValue = (TextView) findViewById(R.id.tv_SlNoValue);
        tv_WardNoValue = (TextView) findViewById(R.id.tv_WardNoValue);
        tv_OwnerNameValue = (TextView) findViewById(R.id.tv_OwnerNameValue);
        tv_PremisesValue = (TextView) findViewById(R.id.tv_PremisesValue);
        tv_addressValue = (TextView) findViewById(R.id.tv_addressValue);

        tv_heading = (TextView)findViewById(R.id.tv_heading);
        tv_watermark = (TextView)findViewById(R.id.tv_watermark);

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        Bundle bundle = getIntent().getExtras();
        kmcSearchDeatilsList = bundle.getParcelableArrayList("KMC_SEARCH_RESULT");
        positionValue = getIntent().getExtras().getInt("POSITION");

        if(kmcSearchDeatilsList.get(positionValue).getKmc_slNo() != null)
            slNo = kmcSearchDeatilsList.get(positionValue).getKmc_slNo().trim();

        if(kmcSearchDeatilsList.get(positionValue).getKmc_wardNo() != null)
            wardNo = kmcSearchDeatilsList.get(positionValue).getKmc_wardNo().trim();

        if(kmcSearchDeatilsList.get(positionValue).getKmc_owner() != null)
            ownerName = kmcSearchDeatilsList.get(positionValue).getKmc_owner().trim();

        if(kmcSearchDeatilsList.get(positionValue).getKmc_premises() != null)
            premises = kmcSearchDeatilsList.get(positionValue).getKmc_premises().trim();

        if(kmcSearchDeatilsList.get(positionValue).getKmc_mailingAddress() != null)
            mailingAddress = kmcSearchDeatilsList.get(positionValue).getKmc_mailingAddress().trim();

        tv_SlNoValue.setText(slNo);
        tv_WardNoValue.setText(wardNo);
        tv_OwnerNameValue.setText(ownerName);
        tv_PremisesValue.setText(premises);
        tv_addressValue.setText(mailingAddress);

        Constants.changefonts(tv_SlNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_WardNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_OwnerNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_PremisesValue, this, "Calibri.ttf");
        Constants.changefonts(tv_addressValue, this, "Calibri.ttf");

        Constants.changefonts(tv_heading, this, "Calibri Bold.ttf");

        /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
           /* case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this,Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }


    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
