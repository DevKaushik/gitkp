package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;

import java.util.Observable;
import java.util.Observer;

public class GasSearchDetailsActivity extends BaseActivity implements View.OnClickListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String distributorId = "";
    private String distributorName = "";
    private String consumerId = "";
    private String consumerName = "";
    private String consumerAddress = "";
    private String pincode = "";
    private String phoneNo = "";
    private String consumerStatus = "";
    private String svDate = "";
    private String hasAadhar = "";

    private TextView tv_DistributorIdValue;
    private TextView tv_DistributorNameValue;
    private TextView tv_ConsumerIdValue;
    private TextView tv_ConsumerNameValue;
    private TextView tv_addressValue;
    private TextView tv_pincodeValue;
    private TextView tv_statusValue;
    private TextView tv_mobileNoValue;
    private TextView tv_SvDateValue;
    private TextView tv_aadharValue;

    private TextView tv_heading;

    private TextView tv_watermark;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gas_search_details);
        ObservableObject.getInstance().addObserver(this);

        initialization();
    }


    /*
    *  Mapping all views
    * */
    private void initialization() {

        tv_DistributorIdValue = (TextView)findViewById(R.id.tv_DistributorIdValue);
        tv_DistributorNameValue = (TextView)findViewById(R.id.tv_DistributorNameValue);
        tv_ConsumerIdValue = (TextView)findViewById(R.id.tv_ConsumerIdValue);
        tv_ConsumerNameValue = (TextView)findViewById(R.id.tv_ConsumerNameValue);
        tv_addressValue = (TextView)findViewById(R.id.tv_addressValue);
        tv_pincodeValue = (TextView)findViewById(R.id.tv_pincodeValue);
        tv_statusValue = (TextView)findViewById(R.id.tv_statusValue);
        tv_mobileNoValue = (TextView)findViewById(R.id.tv_mobileNoValue);
        tv_SvDateValue = (TextView)findViewById(R.id.tv_SvDateValue);
        tv_aadharValue = (TextView)findViewById(R.id.tv_aadharValue);

        tv_heading = (TextView)findViewById(R.id.tv_heading);

        tv_watermark = (TextView)findViewById(R.id.tv_watermark);

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        distributorId = getIntent().getStringExtra("DISTRIBUTOR_ID").trim();
        distributorName = getIntent().getStringExtra("DISTRIBUTOR_NAME").trim();
        consumerId = getIntent().getStringExtra("CONSUMER_ID").trim();
        consumerName = getIntent().getStringExtra("CONSUMER_NAME").trim();
        consumerAddress = getIntent().getStringExtra("CONSUMER_ADDRESS").trim();
        pincode = getIntent().getStringExtra("CONSUMER_PINCODE").trim();
        phoneNo = getIntent().getStringExtra("CONSUMER_MOBILE").trim();
        consumerStatus = getIntent().getStringExtra("CONSUMER_STATUS").trim();
        svDate = getIntent().getStringExtra("CONSUMER_SV_DATE").trim();
        hasAadhar = getIntent().getStringExtra("CONSUMER_AADHAR").trim();

        tv_DistributorIdValue.setText(distributorId);
        tv_DistributorNameValue.setText(distributorName);
        tv_ConsumerIdValue.setText(consumerId);
        tv_ConsumerNameValue.setText(consumerName);
        tv_addressValue.setText(consumerAddress);
        tv_pincodeValue.setText(pincode);
        tv_statusValue.setText(consumerStatus);
        tv_mobileNoValue.setText(phoneNo);
        tv_SvDateValue.setText(svDate);
        tv_aadharValue.setText(hasAadhar);


        Constants.changefonts(tv_DistributorIdValue, this, "Calibri.ttf");
        Constants.changefonts(tv_DistributorNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_ConsumerIdValue, this, "Calibri.ttf");
        Constants.changefonts(tv_ConsumerNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_addressValue, this, "Calibri.ttf");
        Constants.changefonts(tv_pincodeValue, this, "Calibri.ttf");
        Constants.changefonts(tv_statusValue, this, "Calibri.ttf");
        Constants.changefonts(tv_mobileNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_SvDateValue, this, "Calibri.ttf");
        Constants.changefonts(tv_aadharValue, this, "Calibri.ttf");


        Constants.changefonts(tv_heading, this, "Calibri Bold.ttf");

        /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);

    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this,Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
