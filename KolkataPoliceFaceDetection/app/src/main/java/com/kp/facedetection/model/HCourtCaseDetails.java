package com.kp.facedetection.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DAT-165 on 27-03-2017.
 */

public class HCourtCaseDetails implements Parcelable {

    private String rowNo = "";
    private String sideValue = "";
    private String caseDate = "";
    private String courtNo = "";
    private String justice1 = "";
    private String justice2 = "";
    private String justice3 = "";
    private String category = "";
    private String slNo = "";
    private String caseType = "";
    private String caseNo = "";
    private String caseYr = "";
    private String party = "";
    private String addLcase = "";
    private String advPetitioner = "";
    private String advResPdt = "";
    private String briefNote = "";
    private String outCome = "";
    private String nextDate = "";

    public HCourtCaseDetails(Parcel in) {
        rowNo = in.readString();
        sideValue = in.readString();
        caseDate = in.readString();
        courtNo = in.readString();
        justice1 = in.readString();
        justice2 = in.readString();
        justice3 = in.readString();
        category = in.readString();
        slNo = in.readString();
        caseType = in.readString();
        caseNo = in.readString();
        caseYr = in.readString();
        party = in.readString();
        addLcase = in.readString();
        advPetitioner = in.readString();
        advResPdt = in.readString();
        briefNote = in.readString();
        outCome = in.readString();
        nextDate = in.readString();
    }

    public static final Creator<HCourtCaseDetails> CREATOR = new Creator<HCourtCaseDetails>() {
        @Override
        public HCourtCaseDetails createFromParcel(Parcel in) {
            return new HCourtCaseDetails(in);
        }

        @Override
        public HCourtCaseDetails[] newArray(int size) {
            return new HCourtCaseDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(rowNo);
        dest.writeString(sideValue);
        dest.writeString(caseDate);
        dest.writeString(courtNo);
        dest.writeString(justice1);
        dest.writeString(justice2);
        dest.writeString(justice3);
        dest.writeString(category);
        dest.writeString(slNo);
        dest.writeString(caseType);
        dest.writeString(caseNo);
        dest.writeString(caseYr);
        dest.writeString(party);
        dest.writeString(addLcase);
        dest.writeString(advPetitioner);
        dest.writeString(advResPdt);
        dest.writeString(briefNote);
        dest.writeString(outCome);
        dest.writeString(nextDate);
    }

    public String getCourtNo() {
        return courtNo;
    }

    public void setCourtNo(String courtNo) {
        this.courtNo = courtNo;
    }

    public String getRowNo() {
        return rowNo;
    }

    public void setRowNo(String rowNo) {
        this.rowNo = rowNo;
    }

    public String getSideValue() {
        return sideValue;
    }

    public void setSideValue(String sideValue) {
        this.sideValue = sideValue;
    }

    public String getCaseDate() {
        return caseDate;
    }

    public void setCaseDate(String caseDate) {
        this.caseDate = caseDate;
    }

    public String getJustice1() {
        return justice1;
    }

    public void setJustice1(String justice1) {
        this.justice1 = justice1;
    }

    public String getJustice2() {
        return justice2;
    }

    public void setJustice2(String justice2) {
        this.justice2 = justice2;
    }

    public String getJustice3() {
        return justice3;
    }

    public void setJustice3(String justice3) {
        this.justice3 = justice3;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSlNo() {
        return slNo;
    }

    public void setSlNo(String slNo) {
        this.slNo = slNo;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getCaseYr() {
        return caseYr;
    }

    public void setCaseYr(String caseYr) {
        this.caseYr = caseYr;
    }

    public String getParty() {
        return party;
    }

    public void setParty(String party) {
        this.party = party;
    }

    public String getAddLcase() {
        return addLcase;
    }

    public void setAddLcase(String addLcase) {
        this.addLcase = addLcase;
    }

    public String getAdvPetitioner() {
        return advPetitioner;
    }

    public void setAdvPetitioner(String advPetitioner) {
        this.advPetitioner = advPetitioner;
    }

    public String getAdvResPdt() {
        return advResPdt;
    }

    public void setAdvResPdt(String advResPdt) {
        this.advResPdt = advResPdt;
    }

    public String getBriefNote() {
        return briefNote;
    }

    public void setBriefNote(String briefNote) {
        this.briefNote = briefNote;
    }

    public String getOutCome() {
        return outCome;
    }

    public void setOutCome(String outCome) {
        this.outCome = outCome;
    }

    public String getNextDate() {
        return nextDate;
    }

    public void setNextDate(String nextDate) {
        this.nextDate = nextDate;
    }
}
