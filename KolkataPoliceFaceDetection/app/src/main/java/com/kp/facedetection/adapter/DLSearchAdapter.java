package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.R;
import com.kp.facedetection.model.DLSearchDetails;
import com.kp.facedetection.model.HotelsList;
import com.kp.facedetection.utility.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 28-07-2016.
 */
public class DLSearchAdapter extends BaseAdapter {

    private Context context;
    private List<DLSearchDetails> dlSearchDetailsList;
    List<DLSearchDetails> searchItemList = new ArrayList<DLSearchDetails>();

    public DLSearchAdapter(Context context, List<DLSearchDetails> dlSearchDetailsList) {
        this.context = context;
        this.dlSearchDetailsList = dlSearchDetailsList;
        searchItemList.addAll(dlSearchDetailsList);
    }


    @Override
    public int getCount() {

        int size = (dlSearchDetailsList.size()>0)? dlSearchDetailsList.size():0;
        return size;

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.modified_sdr_search_list_item, null);

            holder.relative_main=(RelativeLayout)convertView.findViewById(R.id.relative_main);
            holder.tv_slNo=(TextView)convertView.findViewById(R.id.tv_slNo);
            holder.tv_name=(TextView)convertView.findViewById(R.id.tv_name);
            holder.tv_address=(TextView)convertView.findViewById(R.id.tv_address);
            holder.tv_mobileNo=(TextView)convertView.findViewById(R.id.tv_mobileNo);

            holder.tv_mobileNo.setVisibility(View.GONE);

            Constants.changefonts(holder.tv_slNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_name, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_address, context, "Calibri.ttf");

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);

        holder.tv_slNo.setText(sl_no);
        holder.tv_name.setText("NAME: "+dlSearchDetailsList.get(position).getDl_Name().trim());
        holder.tv_address.setText("ADDRESS: "+dlSearchDetailsList.get(position).getDl_PAddress().trim());
       /* holder.relative_main.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
              // Toast.makeText(context, "Clicked position"+dlSearchDetailsList.get(position).getDl_No(), Toast.LENGTH_SHORT).show();
           }
       });
*/
        return convertView;
    }

    class ViewHolder {

        TextView tv_slNo,tv_name,tv_address,tv_mobileNo;
        RelativeLayout relative_main;


    }
    public void filter(String charText) {
        Log.d("JAY",charText);
        charText = charText.toLowerCase();
        searchItemList.clear();
        if (charText.length() == 0) {
            searchItemList.addAll(dlSearchDetailsList);
        } else {
            for (DLSearchDetails item : dlSearchDetailsList) {
                if (item.getDl_Name().toLowerCase().contains(charText)) {
                    searchItemList.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }
}
