package com.kp.facedetection.model;

import java.io.Serializable;

public class UserInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userId;
    private String name;
    private String userBatchNo;
    private String password;
    private String passwordStatus;
    private String token;
    private String userPhoneNo;
    private String userEmail;
    private String userStatus;
    private String userRank;
    private String userDivisionCode;
    private String userPscode;
    private String userCreateDate;
    private String userLastLogin;
    private String userMacId;
    private String loginNumber;
    private String user_logid;
    private String app_version;
    private String sdr_allow;
    private String kmc_allow;
    private String dl_allow;
    private String vehicle_allow;
    private String gas_allow;
    private String cable_allow;
    private String screenshot_allow;
    private String userDesignation;

    private String splSvcAllow;

    private  String cdrRequestAccess;
    private  String sdrRequestAccess;
    private  String imeiRequestAccess;
    private  String towerRequestAccess;
    private  String ipdrRequestAccess;
    private  String ildRequestAccess;
    private  String cdumpRequestAccess;
    private  String cafRequestAccess;
    private  String ipRequestAccess;
    private  String loggerRequestAccess;
    private  String mnpRequestAccess;
    private  String rdRequestAccess;
    private  String calldumpRequestAccess;
    private  String cellIdChart;
    private String IS_OC,IS_DC,IS_AC,IS_JTCP,IS_ADCP,IS_SPLCP,IS_CP;



    public String getSplSvcAllow() {
        return splSvcAllow;
    }

    public void setSplSvcAllow(String splSvcAllow) {
        this.splSvcAllow = splSvcAllow;
    }

    public String getCdrRequestAccess() {
        return cdrRequestAccess;
    }

    public void setCdrRequestAccess(String cdrRequestAccess) {
        this.cdrRequestAccess = cdrRequestAccess;
    }

    public String getSdrRequestAccess() {
        return sdrRequestAccess;
    }

    public void setSdrRequestAccess(String sdrRequestAccess) {
        this.sdrRequestAccess = sdrRequestAccess;
    }

    public String getImeiRequestAccess() {
        return imeiRequestAccess;
    }

    public void setImeiRequestAccess(String imeiRequestAccess) {
        this.imeiRequestAccess = imeiRequestAccess;
    }

    public String getTowerRequestAccess() {
        return towerRequestAccess;
    }

    public void setTowerRequestAccess(String towerRequestAccess) {
        this.towerRequestAccess = towerRequestAccess;
    }

    public String getIpdrRequestAccess() {
        return ipdrRequestAccess;
    }

    public void setIpdrRequestAccess(String ipdrRequestAccess) {
        this.ipdrRequestAccess = ipdrRequestAccess;
    }

    public String getIldRequestAccess() {
        return ildRequestAccess;
    }

    public void setIldRequestAccess(String ildRequestAccess) {
        this.ildRequestAccess = ildRequestAccess;
    }

    public String getCdumpRequestAccess() {
        return cdumpRequestAccess;
    }

    public void setCdumpRequestAccess(String cdumpRequestAccess) {
        this.cdumpRequestAccess = cdumpRequestAccess;
    }

    public String getCafRequestAccess() {
        return cafRequestAccess;
    }

    public void setCafRequestAccess(String cafRequestAccess) {
        this.cafRequestAccess = cafRequestAccess;
    }

    public String getIpRequestAccess() {
        return ipRequestAccess;
    }

    public void setIpRequestAccess(String ipRequestAccess) {
        this.ipRequestAccess = ipRequestAccess;
    }

    public String getLoggerRequestAccess() {
        return loggerRequestAccess;
    }

    public void setLoggerRequestAccess(String loggerRequestAccess) {
        this.loggerRequestAccess = loggerRequestAccess;
    }

    public String getMnpRequestAccess() {
        return mnpRequestAccess;
    }

    public void setMnpRequestAccess(String mnpRequestAccess) {
        this.mnpRequestAccess = mnpRequestAccess;
    }

    public String getRdRequestAccess() {
        return rdRequestAccess;
    }

    public void setRdRequestAccess(String rdRequestAccess) {
        this.rdRequestAccess = rdRequestAccess;
    }

    public String getCalldumpRequestAccess() {
        return calldumpRequestAccess;
    }

    public void setCalldumpRequestAccess(String calldumpRequestAccess) {
        this.calldumpRequestAccess = calldumpRequestAccess;
    }

    public String getCellIdChart() {
        return cellIdChart;
    }

    public void setCellIdChart(String cellIdChart) {
        this.cellIdChart = cellIdChart;
    }
    /*  "SPLSVC_ALLOW": "0",
              "REQUEST_THRU_OC": "0",
              "REQUEST_THRU_AC": "0",
              "REQUEST_THRU_DC": "0",
              "REQUEST_THRU_JTCP": "0",
              "REQUEST_THRU_NODAL": "0",
              "EMAIL_TO_OC": "0",
              "EMAIL_TO_DC": "0",
              "EMAIL_O_JTCP": "0",
              "EMAIL_TO_NODAL": "0",
              */

    public String getUserDesignation() {
        return userDesignation;
    }

    public void setUserDesignation(String userDesignation) {
        this.userDesignation = userDesignation;
    }
    public String getUserRank() {
        return userRank;
    }

    public void setUserRank(String userRank) {
        this.userRank = userRank;
    }

    public String getScreenshot_allow() {
        return screenshot_allow;
    }

    public void setScreenshot_allow(String screenshot_allow) {
        this.screenshot_allow = screenshot_allow;
    }

    public String getSdr_allow() {
        return sdr_allow;
    }

    public void setSdr_allow(String sdr_allow) {
        this.sdr_allow = sdr_allow;
    }

    public String getKmc_allow() {
        return kmc_allow;
    }

    public void setKmc_allow(String kmc_allow) {
        this.kmc_allow = kmc_allow;
    }

    public String getDl_allow() {
        return dl_allow;
    }

    public void setDl_allow(String dl_allow) {
        this.dl_allow = dl_allow;
    }

    public String getVehicle_allow() {
        return vehicle_allow;
    }

    public void setVehicle_allow(String vehicle_allow) {
        this.vehicle_allow = vehicle_allow;
    }

    public String getGas_allow() {
        return gas_allow;
    }

    public void setGas_allow(String gas_allow) {
        this.gas_allow = gas_allow;
    }

    public String getCable_allow() {
        return cable_allow;
    }

    public void setCable_allow(String cable_allow) {
        this.cable_allow = cable_allow;
    }

    public String getApp_version() {
        return app_version;
    }

    public void setApp_version(String app_version) {
        this.app_version = app_version;
    }

    public String getUser_logid() {
        return user_logid;
    }

    public void setUser_logid(String user_logid) {
        this.user_logid = user_logid;
    }



    public String getLoginNumber() {
        return loginNumber;
    }

    public void setLoginNumber(String loginNumber) {
        this.loginNumber = loginNumber;
    }

    public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserBatchNo() {
		return userBatchNo;
	}

	public void setUserBatchNo(String userBatchNo) {
		this.userBatchNo = userBatchNo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}


    public String getUserPhoneNo() {
        return userPhoneNo;
    }

    public void setUserPhoneNo(String userPhoneNo) {
        this.userPhoneNo = userPhoneNo;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserDivisionCode() {
        return userDivisionCode;
    }

    public void setUserDivisionCode(String userDivisionCode) {
        this.userDivisionCode = userDivisionCode;
    }

    public String getUserPscode() {
        return userPscode;
    }

    public void setUserPscode(String userPscode) {
        this.userPscode = userPscode;
    }

    public String getUserCreateDate() {
        return userCreateDate;
    }

    public void setUserCreateDate(String userCreateDate) {
        this.userCreateDate = userCreateDate;
    }

    public String getUserLastLogin() {
        return userLastLogin;
    }

    public void setUserLastLogin(String userLastLogin) {
        this.userLastLogin = userLastLogin;
    }

    public String getUserMacId() {
        return userMacId;
    }

    public void setUserMacId(String userMacId) {
        this.userMacId = userMacId;
    }

    public void setPasswordStatus(String passwordStatus) {
        this.passwordStatus = passwordStatus;
    }

    public String getPasswordStatus() {
        return passwordStatus;
    }

    public String getIS_OC() {
        return IS_OC;
    }

    public void setIS_OC(String IS_OC) {
        this.IS_OC = IS_OC;
    }

    public String getIS_DC() {
        return IS_DC;
    }

    public void setIS_DC(String IS_DC) {
        this.IS_DC = IS_DC;
    }

    public String getIS_AC() {
        return IS_AC;
    }

    public void setIS_AC(String IS_AC) {
        this.IS_AC = IS_AC;
    }

    public String getIS_JTCP() {
        return IS_JTCP;
    }

    public void setIS_JTCP(String IS_JTCP) {
        this.IS_JTCP = IS_JTCP;
    }

    public String getIS_ADCP() {
        return IS_ADCP;
    }

    public void setIS_ADCP(String IS_ADCP) {
        this.IS_ADCP = IS_ADCP;
    }

    public String getIS_SPLCP() {
        return IS_SPLCP;
    }

    public void setIS_SPLCP(String IS_SPLCP) {
        this.IS_SPLCP = IS_SPLCP;
    }

    public String getIS_CP() {
        return IS_CP;
    }

    public void setIS_CP(String IS_CP) {
        this.IS_CP = IS_CP;
    }
}
