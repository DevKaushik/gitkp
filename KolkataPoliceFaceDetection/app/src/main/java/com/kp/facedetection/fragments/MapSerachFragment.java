package com.kp.facedetection.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.kp.facedetection.MapSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.adapter.CustomDialogAdapterForCategoryCrimeList2;
import com.kp.facedetection.model.AllContentList;
import com.kp.facedetection.model.CrimeCategoryList;
import com.kp.facedetection.model.CrimeCategoryList2;
import com.kp.facedetection.model.DivisionList;
import com.kp.facedetection.model.MapSearchDetails;
import com.kp.facedetection.model.PoliceStationList;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by DAT-165 on 20-03-2017.
 */

public class MapSerachFragment extends Fragment implements View.OnClickListener, SearchView.OnQueryTextListener{

    View rootLayout;
    private AllContentList allContentList;
    private JSONArray policeStationListArray;
    private JSONArray crimeCategoryListArray;
    private JSONArray divisionListArray;
    List<String> policeStationArrayList = new ArrayList<String>();
    List<String> divisionArrayList = new ArrayList<String>();
    private ArrayList<CrimeCategoryList> obj_categoryCrimeList;
    private ArrayList<PoliceStationList> obj_policeStationList;
    private ArrayList<DivisionList> obj_divisionList;
    private String[] array_modusOperandi;
    protected String[] array_class;
    private List<String> modified_crimeCategoryList = new ArrayList<String>();
    public static ArrayList<CrimeCategoryList2> obj_categoryCrimeList2;

    private TextView tv_ps_value;
    private TextView tv_crime_value;
    private TextView tv_division_value;
    private TextView tv_date_start_value;
    private TextView tv_date_end_value;
    Button btn_search;
    private Button btn_reset;
    protected String[] array_policeStation;
    protected String[] array_division ;
    protected ArrayList<CharSequence> selectedPoliceStations = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedPoliceStationsId = new ArrayList<CharSequence>();

    protected ArrayList<CharSequence> selectedDivision = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedDivisionId = new ArrayList<CharSequence>();

    protected String[] array_crimeCategory ;
    protected ArrayList<CharSequence> selectedCrimeCategory = new ArrayList<CharSequence>();
    private String policeStationString="";
    private String divisionString="";
    private String crimeCategoryString="";
    CustomDialogAdapterForCategoryCrimeList2 customDialogAdapterForCategoryCrimeList2;
    private boolean crimeCategory_status=false;
    private ListView lv_dialog;

    private String pageno="";
    private String totalResult="";
    private String[] keys;
    private String[] values;

    private String[] key_map;
    private String[] value_map;

    private List<MapSearchDetails> mapSearchList = new ArrayList<MapSearchDetails>();

    public static MapSerachFragment newInstance( ) {

        MapSerachFragment mapSearchFragment = new MapSerachFragment();
        //warrantSearchFragment.setArguments(args);
        return mapSearchFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootLayout = inflater.inflate(R.layout.map_search_layout, null);
        initView();
        return rootLayout;
    }

    private void initView() {

        tv_ps_value = (TextView) rootLayout.findViewById(R.id.tv_ps_value);
        tv_crime_value = (TextView) rootLayout.findViewById(R.id.tv_crime_value);
        tv_division_value = (TextView) rootLayout.findViewById(R.id.tv_division_value);

        btn_search = (Button) rootLayout.findViewById(R.id.btn_search);
        btn_reset = (Button) rootLayout.findViewById(R.id.btn_reset);

        btn_search.setOnClickListener(this);
        btn_reset.setOnClickListener(this);

        tv_date_start_value = (TextView)rootLayout.findViewById(R.id.tv_fromDateValue);
        tv_date_end_value = (TextView)rootLayout.findViewById(R.id.tv_toDateValue);

        Constants.buttonEffect(btn_search);
        Constants.buttonEffect(btn_reset);


        array_policeStation = new String[Constants.policeStationNameArrayList.size()];
        array_policeStation = Constants.policeStationNameArrayList.toArray(array_policeStation);

        array_crimeCategory = new String[Constants.crimeCategoryArrayList.size()];
        array_crimeCategory = Constants.crimeCategoryArrayList.toArray(array_crimeCategory);

        array_division=new String[Constants.divisionArrayList.size()];
        array_division=Constants.divisionArrayList.toArray(array_division);


        clickEvents();
    }
    private void clickEvents(){

        tv_ps_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.policeStationNameArrayList.size()>0) {
                    showSelectPoliceStationsDialog();
                }
                else {
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }

            }
        });


        tv_crime_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.crimeCategoryArrayList.size()!=0){

                    tv_crime_value.setText("Select a Category");
                    crimeCategoryString="";
                    crimeCategory_status = true;

                    customDialogAdapterForCategoryCrimeList2 = new CustomDialogAdapterForCategoryCrimeList2(getActivity(),CriminalSearchFragment.obj_categoryCrimeList2);
                    customDialogForCategoryCrimeList();
                }
                else {
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }


            }
        });

        tv_division_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Constants.divisionArrayList.size()>0) {
                    showSelectDivisionDialog();
                }
                else {
                    showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }


            }
        });

        /* Start Date field click event */

        tv_date_start_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DateUtils.setDate(getActivity(), tv_date_start_value, true);
            }
        });

        /* End Date field click event */

        tv_date_end_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DateUtils.setDate(getActivity(), tv_date_end_value, true);
            }
        });

    }


    //---------------------------------------------------------------------------------------------------------------//


	                                  /* Custom Dialog for Crime Category */

    //---------------------------------------------------------------------------------------------------------------//

    private void customDialogForCategoryCrimeList(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);

        TextView tv_title = (TextView)dialogView.findViewById(R.id.tv_title);
        Button btn_cancel = (Button)dialogView.findViewById(R.id.btn_cancel);
        Button btn_done = (Button)dialogView.findViewById(R.id.btn_done);
        lv_dialog = (ListView)dialogView.findViewById(R.id.lv_dialog);
        SearchView searchView = (SearchView) dialogView.findViewById(R.id.searchView);


        Constants.changefonts(tv_title, getActivity(), "Calibri Bold.ttf");
        tv_title.setText("Select Crime Category");

        customDialogAdapterForCategoryCrimeList2.notifyDataSetChanged();
        lv_dialog.setAdapter(customDialogAdapterForCategoryCrimeList2);
        lv_dialog.setTextFilterEnabled(true);

        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(MapSerachFragment.this);
        searchView.setSubmitButtonEnabled(false);
        searchView.setQueryHint("Search Here");



        final AlertDialog alertDialog = builder.show();

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                crimeCategory_status = false;
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(alertDialog != null && alertDialog.isShowing()){
                    alertDialog.dismiss();

                }
                crimeCategory_status = false;

            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                System.out.println("No of Selected Items: "+ CustomDialogAdapterForCategoryCrimeList2.selectedItemList.size());
                System.out.println("Selected Items: "+ CustomDialogAdapterForCategoryCrimeList2.selectedItemList);
                crimeCategory_status = false;

                onSelectedCategoryOfCrime(CustomDialogAdapterForCategoryCrimeList2.selectedItemList);

                if(alertDialog != null && alertDialog.isShowing()){
                    alertDialog.dismiss();

                }

            }
        });

    }

    private void onSelectedCategoryOfCrime(List<String> crimeCategoryList){

        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderCrimeCategory = new StringBuilder();

        for(CharSequence crimeCategory : crimeCategoryList) {
            stringBuilder.append(crimeCategory + ",");

            String crimeCategoryModifiedString = stringBuilderCrimeCategory.append("\'"+crimeCategory.toString() + "\',").toString();

            crimeCategoryString = crimeCategoryModifiedString.substring(0,crimeCategoryModifiedString.length()-1);
        }

        if(crimeCategoryList.size()==0) {
            tv_crime_value.setText("Select a Category");
        }
        else {
            System.out.println("Crime Category String: "+crimeCategoryString);
            tv_crime_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
        }

    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(crimeCategory_status){
            customDialogAdapterForCategoryCrimeList2.filter(newText.toString());
        }
        return true;
    }

     /* This method shows Police Station list in a pop-up */

    protected void showSelectPoliceStationsDialog() {
        boolean[] checkedPoliceStations = new boolean[array_policeStation.length];
        int count = array_policeStation.length;

        for(int i = 0; i < count; i++) {
            checkedPoliceStations[i] = selectedPoliceStations.contains(array_policeStation[i]);
        }

        DialogInterface.OnMultiChoiceClickListener policeStationsDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedPoliceStations.add(array_policeStation[which]);
                    selectedPoliceStationsId.add((CharSequence) Constants.policeStationIDArrayList.get(which));
                } else{
                    selectedPoliceStations.remove(array_policeStation[which]);
                    selectedPoliceStationsId.remove((CharSequence) Constants.policeStationIDArrayList.get(which));
                }

            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Police Stations");
        builder.setMultiChoiceItems(array_policeStation, checkedPoliceStations, policeStationsDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedPoliceStations();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_ps_value.setText("Select Police Stations");
                policeStationString="";
                selectedPoliceStations.clear();
                selectedPoliceStationsId.clear();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedPoliceStations() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for(CharSequence policeStation : selectedPoliceStations){
            stringBuilder.append(policeStation + ",");
        }

        for(CharSequence policeStation : selectedPoliceStationsId){
            stringBuilderId.append("\'"+policeStation + "\',");
        }

        if(selectedPoliceStations.size()==0) {
            tv_ps_value.setText("Select Police Stations");
            policeStationString="";
        }
        else {
            tv_ps_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
            policeStationString=stringBuilderId.toString().substring(0,stringBuilderId.toString().length()-1);
        }

    }

    //Show division list
     /* This method shows division list in a pop-up */

    protected void showSelectDivisionDialog() {
        boolean[] checkedDivision = new boolean[array_division.length];
        int count = array_division.length;

        for(int i = 0; i < count; i++) {
            checkedDivision[i] = selectedDivision.contains(array_division[i]);
        }

        DialogInterface.OnMultiChoiceClickListener divisionDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedDivision.add(array_division[which]);
                    selectedDivisionId.add((CharSequence) Constants.divCodeArrayList.get(which));
                } else{
                    selectedDivision.remove(array_division[which]);
                    selectedDivisionId.remove((CharSequence) Constants.divCodeArrayList.get(which));
                }

            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Division");
        builder.setMultiChoiceItems(array_division, checkedDivision, divisionDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedDivision();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_division_value.setText("Select Division");
                divisionString="";
                selectedDivision.clear();
                selectedDivisionId.clear();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedDivision() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for(CharSequence division : selectedDivision){
            stringBuilder.append(division + ",");
        }

        for(CharSequence division : selectedDivisionId){
            stringBuilderId.append("\'"+division + "\',");
        }

        if(selectedDivision.size()==0) {
            tv_division_value.setText("Select Division");
            divisionString="";
        }
        else {
            tv_division_value.setText(stringBuilder.toString().substring(0,stringBuilder.toString().length()-1));
            divisionString=stringBuilderId.toString().substring(0,stringBuilderId.toString().length()-1);
        }
    }






    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_search:

                String startDate = tv_date_start_value.getText().toString().trim();
                String endDate = tv_date_end_value.getText().toString().trim();

                if (!endDate.equalsIgnoreCase("") && startDate.equalsIgnoreCase("")) {
                    showAlertDialog(getActivity(), "Alert !!!", "Please provide both dates", false);
                } else if (endDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("")) {
                    showAlertDialog(getActivity(), "Alert !!!", "Please provide both dates", false);
                } else if (!endDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("")) {
                    boolean date_result_flag = DateUtils.dateComparison(startDate, endDate);
                    System.out.println("DateComparison: " + date_result_flag);
                    if (!date_result_flag) {
                        showAlertDialog(getActivity(), "Alert !!!", "From date can't be greater than To date", false);
                    } else {
                        fetchMapSearchResult();
                    }
                } else {
                    fetchMapSearchResult();
                }

                break;

            case R.id.btn_reset:
                resetDataFieldValues();
                break;


            default:
                break;
        }
    }



    private void fetchMapSearchResult() {

        String date_from=tv_date_start_value.getText().toString().trim();
        String date_to=tv_date_end_value.getText().toString().trim();

        if(!divisionString.equalsIgnoreCase("") || !policeStationString.equalsIgnoreCase("") ||
                !crimeCategoryString.equalsIgnoreCase("") || !date_from.equalsIgnoreCase("") ||
                !date_to.equalsIgnoreCase("") ){

            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_MAP_SEARCH);
            taskManager.setMapSearch(true);

            String[] keys = { "div", "ps", "crime_cat", "from_date", "to_date", "pageno"};

            String[] values = { divisionString.trim(), policeStationString.trim(), crimeCategoryString.trim(), date_from.trim(), date_to.trim(), "1"};

            taskManager.doStartTask(keys, values, true);

            key_map = new String[]{"div", "ps", "crime_cat", "from_date", "to_date", "pageno"};
            value_map = new String[]{divisionString, policeStationString, crimeCategoryString, date_from, date_to, "1"};

        }else{

            showAlertDialog(getActivity(), "Alert !!!", "Please provide atleast one value for search", false);
        }

    }


    public void parseMapSearchResult(String result, String[] keys, String[] values) {

       // System.out.println("parseMapSearchResult: " + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("page").toString();
                    totalResult = jObj.opt("count").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseMapResponse(resultArray);

                }
                else{
                    showAlertDialog(getActivity(),Constants.ERROR_DETAILS_TITLE,Constants.ERROR_MSG_DETAIL,false);
                }


            } catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(getActivity(), Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }

        }
    }


    private void parseMapResponse(JSONArray result_array) {

        mapSearchList.clear();
        for(int i=0;i<result_array.length();i++) {

            JSONObject obj = null;

            try {

                obj = result_array.getJSONObject(i);

                MapSearchDetails mapDetails = new MapSearchDetails(Parcel.obtain());

                if(!obj.optString("CASENO").equalsIgnoreCase("null"))
                    mapDetails.setCaseNo(obj.optString("CASENO"));

                if(!obj.optString("CASEDATE").equalsIgnoreCase("null"))
                    mapDetails.setCaseDate(obj.optString("CASEDATE"));

                if(!obj.optString("PSNAME").equalsIgnoreCase("null"))
                    mapDetails.setPsName(obj.optString("PSNAME"));

                if(!obj.optString("PS").equalsIgnoreCase("null"))
                    mapDetails.setPsCode(obj.optString("PS"));

                if(!obj.optString("CATEGORY").equalsIgnoreCase("null"))
                    mapDetails.setCrimeCat(obj.optString("CATEGORY"));

                if(!obj.optString("PO_LAT_DEGREE").equalsIgnoreCase("null"))
                    mapDetails.setLatitudeValue(obj.optString("PO_LAT_DEGREE"));

                if(!obj.optString("PO_LAT_MINUTE").equalsIgnoreCase("null"))
                    mapDetails.setLatMin(obj.optString("PO_LAT_MINUTE"));

                if(!obj.optString("PO_LAT_SECOND").equalsIgnoreCase("null"))
                    mapDetails.setLatSec(obj.optString("PO_LAT_SECOND"));

                if(!obj.optString("PO_LONG_DEGREE").equalsIgnoreCase("null"))
                    mapDetails.setLongitudeValue(obj.optString("PO_LONG_DEGREE"));

                if(!obj.optString("PO_LONG_MINUTE").equalsIgnoreCase("null"))
                    mapDetails.setLngMin(obj.optString("PO_LONG_MINUTE"));

                if(!obj.optString("PO_LONG_SECOND").equalsIgnoreCase("null"))
                    mapDetails.setLngSec(obj.optString("PO_LONG_SECOND"));

                if(!obj.optString("FIR_YR").equalsIgnoreCase("null"))
                    mapDetails.setFirYr(obj.optString("FIR_YR"));

                if(!obj.optString("PO_LAT").equalsIgnoreCase("null"))
                    mapDetails.setPoLat(obj.optString("PO_LAT"));

                if(!obj.optString("PO_LONG").equalsIgnoreCase("null"))
                    mapDetails.setPoLong(obj.optString("PO_LONG"));

                if(!obj.optString("COLOR_CODE").equalsIgnoreCase("null"))
                    mapDetails.setColorCode(obj.optString("COLOR_CODE"));

                mapSearchList.add(mapDetails);

            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Intent intent=new Intent(getActivity(), MapSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult",totalResult);
        intent.putExtra("key_map",key_map);
        intent.putExtra("value_map",value_map);
        intent.putExtra("MAP_SEARCH", (ArrayList<? extends Parcelable>) mapSearchList);
        startActivity(intent);
    }




    private void resetDataFieldValues(){

		/* for police station part */
        tv_ps_value.setText("Select Police Stations");
        policeStationString="";
        selectedPoliceStations.clear();
        selectedPoliceStationsId.clear();

        /* for division part */
        tv_division_value.setText("Select Division");
        divisionString="";
        selectedDivision.clear();
        selectedDivisionId.clear();

		/* for Category of crime part */
        tv_crime_value.setText("Select a Category");
        crimeCategoryString="";
        //selectedCrimeCategory.clear();

        /* for Start Date part */
        tv_date_start_value.setText("");

        /* for End Date part */
        tv_date_end_value.setText("");


    }

    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     * */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }



}
