package com.kp.facedetection;


import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kp.facedetection.fragments.ModifiedDocumentsFragment;
import com.kp.facedetection.interfaces.OnShowAlertForProceedResult;
import com.kp.facedetection.model.BARDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class BarSearchActivity extends BaseActivity implements OnShowAlertForProceedResult,View.OnClickListener {

    private EditText et_holderName,et_address;
    String holder_name ="",holder_address="";
    private String pageno="";
    private String totalResult="";
    private String[] keys;
    private String[] values;
    String userId="";
    private Button btn_Search, btn_reset;
    String devicetoken ="";
    String auth_key="";
    ArrayList<BARDetails> barDetailsList ;
    TextView chargesheetNotificationCount;
    String notificationCount="";
    private String userName = "";
    private String loginNumber = "";
    private String user_Id = "";
    private String appVersion = "";
    private String log_response="";
    public BarSearchActivity() {
        // Required empty public constructor
    }
    public static BarSearchActivity newInstance() {

        BarSearchActivity barSearchAcitivity = new BarSearchActivity();
        return barSearchAcitivity;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_bar_search);

        initView();
        //return rootLayout;
    }

    public void initView() {

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
            user_Id = Utility.getUserInfo(this).getUserId();

        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        userId= Utility.getUserInfo(this).getUserId();
        et_holderName = (EditText) findViewById(R.id.et_holderName);
        et_address = (EditText) findViewById(R.id.et_address);
        btn_Search = (Button) findViewById(R.id.btn_Search);
        btn_reset = (Button) findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_Search);
        Constants.buttonEffect(btn_reset);
        getAllSearchSecurityCode();
        clickEvents();

    }
    public void getAllSearchSecurityCode()
    {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_DOCUMENT_SESSION_ID);
        taskManager.setSessionForBarSearch(true);
//		String devicetoken = PushUtility.fetchC2DMToken(this);
//      String devicetoken = GCMRegistrar.getRegistrationId(this);
        if(FirebaseInstanceId.getInstance().getToken()!=null) {
            devicetoken = FirebaseInstanceId.getInstance().getToken();
        }
        else{
            devicetoken=Utility.getFCMToken(this);
        }

        String[] keys = {"user_id","device_type", "device_token","imei","login_count"};
       /* String[] values = {et_userid.getText().toString().trim(),
                et_password.getText().toString(), "android", devicetoken,Constants.device_IMEI_No};*///Constants.device_IMEI_No
        String[] values = {userId,"android",devicetoken,Utility.getImiNO(this),loginNumber};//Utility.getImiNO(this)
        taskManager.doStartTask(keys, values, true, true);


    }
    public void parseBarSearchAunthicatedResult(String response){
        if (response != null && !response.equals("")) {
            try {
                JSONObject jObj = new JSONObject(response);
                // L.e("response---------"+response);
                if (jObj.optString("status").equalsIgnoreCase("1")) {
                    if(jObj.optString("session_id")!=null && !jObj.optString("session_id").equalsIgnoreCase("")&& !jObj.optString("session_id").equalsIgnoreCase("null")) {
                        String session_id = jObj.optString("session_id");
                        //L.e("session_id---------"+session_id);
                        Utility.setDocumentSearchSesionId(BarSearchActivity.this, session_id);

                    }
                }
                else{
                    Utility.showAlertDialog(this," Search Error ",Constants.ERROR_MSG_TO_RELOAD,false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }

    private void clickEvents(){



        btn_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder_name = et_holderName.getText().toString().trim();
                holder_address = et_address.getText().toString().trim();

                if (!holder_name.equalsIgnoreCase("") || !holder_address.equalsIgnoreCase("")) {
                    Utility utility = new Utility();
                    utility.setDelegate(BarSearchActivity.this);
                    Utility.showAlertForProceed(BarSearchActivity.this);
                } else {

                    showAlertDialog(BarSearchActivity.this, " Search Error!!! ", "Please provide at least one value for search", false);

                }



            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_holderName.setText("");
                et_address.setText("");

            }
        });

    }

    @Override
    public void onShowAlertForProceedResultSuccess(String success) {
        fetchSearchResult(holder_name,holder_address);
    }
    private void fetchSearchResult(String holder_name,String holder_address) {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_BAR_SEARCH);
        taskManager.setBARSearch(true);
        try{
            //devicetoken = GCMRegistrar.getRegistrationId(this);
            auth_key= Utility.getDocumentSearchSesionId(this);
        }
        catch (Exception e){

        }
        //user_id,device_token,device_type,imei,auth_key,name,address,pageno
        String[] keys = {"name", "address","pageno","user_id","device_token","device_type","imei","auth_key"};

        String[] values = {holder_name, holder_address,"1",userId,devicetoken,"android", Utility.getImiNO(this),auth_key};

        taskManager.doStartTask(keys, values, true,true);


    }

    @Override
    public void onShowAlertForProceedResultError(String error) {

    }
    public void parseBARSearchResultResponse(String result, String[] keys, String[] values){
        if(result != null && !result.equals("")){

            try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.opt("status").toString().equalsIgnoreCase("1")){

                    this.keys=keys;
                    this.values=values;
                    pageno=jObj.opt("page").toString();
                    totalResult=jObj.opt("count").toString();

                    JSONArray resultArray=jObj.getJSONArray("result");

                    parseBARSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(this," Search Error ",jObj.optString("message"),false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                Utility.showToast(this, "Some error has been encountered", "long");
            }
        }
    }
    private void parseBARSearchResponse(JSONArray resultArray) {
        barDetailsList = new ArrayList<BARDetails>();
        for(int i=0;i<resultArray.length();i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj=resultArray.getJSONObject(i);

                BARDetails barDetails = new BARDetails();

                if(!jsonObj.optString("name").equalsIgnoreCase("null") && !jsonObj.optString("name").equalsIgnoreCase("")){
                    barDetails.setName(jsonObj.optString("name"));
                }
                if(!jsonObj.optString("address").equalsIgnoreCase("null") && !jsonObj.optString("address").equalsIgnoreCase("")){
                    barDetails.setAddress(jsonObj.optString("address"));
                }




                barDetailsList.add(barDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Intent intent=new Intent(this, BARSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putExtra("BAR_SEARCH_LIST", (Serializable) barDetailsList);
        startActivity(intent);

    }
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onClick(View v) {

    }
}
