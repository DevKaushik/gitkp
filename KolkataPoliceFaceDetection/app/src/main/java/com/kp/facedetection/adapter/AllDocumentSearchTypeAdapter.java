package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnAllDocumentSearchTypeListener;
import com.kp.facedetection.model.AllDocumentSearchType;
import com.kp.facedetection.model.CrimeReviewDetails;
import com.kp.facedetection.utility.Constants;

import java.util.ArrayList;

/**
 * Created by user on 27-02-2018.
 */

public class AllDocumentSearchTypeAdapter extends RecyclerView.Adapter<AllDocumentSearchTypeAdapter.MyViewHolder>  {
    int selected_position=0;
    Context con;
    OnAllDocumentSearchTypeListener onAllDocumentSearchTypeListener;
    ArrayList<AllDocumentSearchType> allDocumentSearchTypeList = new ArrayList<>();
    public AllDocumentSearchTypeAdapter(Context con, ArrayList<AllDocumentSearchType> allDocumentSearchTypeList) {
        this.con = con;
        this.allDocumentSearchTypeList = allDocumentSearchTypeList;
    }
    @Override
    public AllDocumentSearchTypeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_document_search_type_item_layout, parent, false);
        return new AllDocumentSearchTypeAdapter.MyViewHolder(itemView);
    }
    public void setOnclickListener(OnAllDocumentSearchTypeListener onAllDocumentSearchTypeListener){
        this.onAllDocumentSearchTypeListener=onAllDocumentSearchTypeListener;
    }
    @Override
    public void onBindViewHolder(AllDocumentSearchTypeAdapter.MyViewHolder holder, int position) {

        holder.itemView.setSelected(selected_position==position);
        holder.itemView.setBackgroundColor(selected_position==position? Color.WHITE :Color.TRANSPARENT);
        final AllDocumentSearchType allDocumentSearchType = allDocumentSearchTypeList.get(position);
        if (allDocumentSearchType.getName() != null && !allDocumentSearchType.getName().equalsIgnoreCase("")
                && !allDocumentSearchType.getName().equalsIgnoreCase("null")) {
            /*if (allDocumentSearchType.getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[0])) {
                holder.tv_categoryName.setText("DL \n SEARCH");
            } else if (allDocumentSearchType.getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[1])) {
                holder.tv_categoryName.setText("VEHICLE \n SEARCH");
            } else if (allDocumentSearchType.getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[2])) {
                holder.tv_categoryName.setText("SDR \n SEARCH");
            } else if (allDocumentSearchType.getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[3])) {
                holder.tv_categoryName.setText("CABLE \n SEARCH");
            } else if (allDocumentSearchType.getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[4])) {
                holder.tv_categoryName.setText("KMC \n SEARCH");
            } else if (allDocumentSearchType.getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[5])) {
                holder.tv_categoryName.setText("GAS \n SEARCH");
            }*/
            holder.tv_categoryName.setText(allDocumentSearchType.getName());

        }
    }

    @Override
    public int getItemCount() {
        return allDocumentSearchTypeList == null ? 0 : allDocumentSearchTypeList.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_categoryName;
        public MyViewHolder(View itemView) {
            super(itemView);
            tv_categoryName = (TextView) itemView.findViewById(R.id.tv_categoryName);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (getAdapterPosition() == RecyclerView.NO_POSITION){
                return;
            }
            notifyItemChanged(selected_position);
            selected_position=getAdapterPosition();
            notifyItemChanged(selected_position);
            if(onAllDocumentSearchTypeListener!=null){
                onAllDocumentSearchTypeListener.OnAllDocumentSearchTypeItemClick(v,getAdapterPosition());
            }

        }
    }
}
