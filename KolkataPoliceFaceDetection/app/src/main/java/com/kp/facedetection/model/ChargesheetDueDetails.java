package com.kp.facedetection.model;

/**
 * Created by DAT-Asset-128 on 04-12-2017.
 */

public class ChargesheetDueDetails {
    String psName;
    String psCode;
    String noOfCase;
    String caseNo;
    String caseDate;
    String underSection;
    String caseYear;

    public String getCaseYear() {
        return caseYear;
    }

    public void setCaseYear(String caseYear) {
        this.caseYear = caseYear;
    }

    public String getPsCode() {
        return psCode;
    }

    public void setPsCode(String psCode) {
        this.psCode = psCode;
    }

    public String getPsName() {
        return psName;
    }

    public void setPsName(String psName) {
        this.psName = psName;
    }

    public String getNoOfCase() {
        return noOfCase;
    }

    public void setNoOfCase(String noOfCase) {
        this.noOfCase = noOfCase;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getCaseDate() {
        return caseDate;
    }

    public void setCaseDate(String caseDate) {
        this.caseDate = caseDate;
    }

    public String getUnderSection() {
        return underSection;
    }

    public void setUnderSection(String underSection) {
        this.underSection = underSection;
    }
}
