package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.WarrantSearchDetails;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by DAT-165 on 28-07-2016.
 */
public class WarrantSearchAdapter extends BaseAdapter {

    private Context context;
    private List<WarrantSearchDetails> WarrantSearchDetailsList;

    public WarrantSearchAdapter(Context context, List<WarrantSearchDetails> WarrantSearchDetailsList) {
        this.context = context;
        this.WarrantSearchDetailsList = WarrantSearchDetailsList;
        Log.e("WarrantSize",""+WarrantSearchDetailsList.size());
    }


    @Override
    public int getCount() {

        if(WarrantSearchDetailsList.size()>0) {
            return WarrantSearchDetailsList.size();
        }
        else {
            return 0;
        }

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.modified_casesearch_list_item, null);


            holder.tv_caseName=(TextView)convertView.findViewById(R.id.tv_caseName);
            holder.tv_caseStatus=(TextView)convertView.findViewById(R.id.tv_caseStatus);
            holder.tv_caseSlno=(TextView)convertView.findViewById(R.id.tv_caseSlno);

            Constants.changefonts(holder.tv_caseName, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_caseStatus, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_caseSlno, context, "Calibri.ttf");

            holder.tv_caseName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String warrantDetails="";
        String caseDetails="";
        String warrantDate="";
        String underSection="";
        String warrantyName = "";

        if(!WarrantSearchDetailsList.get(position).getWaName().equalsIgnoreCase("") && !WarrantSearchDetailsList.get(position).getWaName().equalsIgnoreCase("null") && WarrantSearchDetailsList.get(position).getWaName() != null){
            warrantyName ="Name: "+ WarrantSearchDetailsList.get(position).getWaName();
        }
        if(!WarrantSearchDetailsList.get(position).getWaNo().equalsIgnoreCase("")){
            caseDetails="Warrant No. "+ WarrantSearchDetailsList.get(position).getWaNo();
        }
        if(!WarrantSearchDetailsList.get(position).getPsRecvDate().equalsIgnoreCase("")){
            warrantDate=" dated "+ WarrantSearchDetailsList.get(position).getPsRecvDate();
        }
        if(!WarrantSearchDetailsList.get(position).getUnderSections().equalsIgnoreCase("")){
            underSection=" u/s "+ WarrantSearchDetailsList.get(position).getUnderSections();
        }


        warrantDetails = warrantyName + "\n"+caseDetails + warrantDate + underSection;
        //warrantDetails = caseDetails + warrantDate + underSection;

        String warrantDetailsString = warrantDetails.trim().substring(0, 1).toUpperCase() + warrantDetails.trim().substring(1);

        holder.tv_caseSlno.setText(WarrantSearchDetailsList.get(position).getRowNumber());
        holder.tv_caseStatus.setText(WarrantSearchDetailsList.get(position).getWaStatus());
        holder.tv_caseName.setText(warrantDetailsString);


        return convertView;
    }

    class ViewHolder {

        TextView tv_caseName,tv_caseStatus,tv_caseSlno;

    }
}
