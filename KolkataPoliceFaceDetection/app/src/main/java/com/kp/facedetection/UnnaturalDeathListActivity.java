package com.kp.facedetection;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.adapter.UnnaturalDeathAdapter;
import com.kp.facedetection.interfaces.OnItemClickListenerForUnnaturalDeath;
import com.kp.facedetection.model.CrimeReviewDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

public class UnnaturalDeathListActivity extends BaseActivity implements OnItemClickListenerForUnnaturalDeath, View.OnClickListener, Observer {
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String selectedDate="";
    private String selected_div="";
    private String selected_ps="";


    private RecyclerView rv_unnatural_death;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<CrimeReviewDetails> psList = new ArrayList<>();
    UnnaturalDeathAdapter unnaturalDeathAdapter;
    TextView chargesheetNotificationCount;
    TextView chargesheetNotificationTotalCount;
    ImageView dateFilter;
    TextView tv_fromDate,tv_toDate;
    String fromDate="",toDate="";

    String unnaturalDeathTotalCount="";
    String notificationCount="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unnatural_death_list);
        ObservableObject.getInstance().addObserver(this);
        setToolBar();
        initView();
    }
    public void setToolBar(){
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }
    public void initView() {
        selectedDate = getIntent().getExtras().getString("SELECTED_DATE_UND");
        selected_div = getIntent().getExtras().getString("SELECTED_DIV_UND");
        selected_ps = getIntent().getExtras().getString("SELECTED_PS_UND");
        unnaturalDeathTotalCount = getIntent().getExtras().getString("UNNATURALDEATH_TOTAL_COUNT");
        rv_unnatural_death=(RecyclerView)findViewById(R.id.rv_unnatural_death);
        chargesheetNotificationTotalCount =(TextView)findViewById(R.id.tv_heading_unnatural_death_list);
        dateFilter=(ImageView)findViewById(R.id.date_filter);
        dateFilter.setOnClickListener(this);
        mLayoutManager=new LinearLayoutManager(this);
        rv_unnatural_death.setLayoutManager(mLayoutManager);
        rv_unnatural_death.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
        getAllunnaturalDeath();
        unnaturalDeathAdapter=new UnnaturalDeathAdapter(this,psList);
        rv_unnatural_death.setAdapter(unnaturalDeathAdapter);
        unnaturalDeathAdapter.setOnClickListener(this);



    }
    public void   getAllunnaturalDeath(){
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_UNNATURAL_DEATH_LIST);
        taskManager.setUnnaturalDeath(true);
        String[] keys = {"currDate","div","ps","fromDate","toDate","user_id"};
        String[] values = {selectedDate.trim(), selected_div.trim(), selected_ps.trim(), fromDate, toDate,Utility.getUserInfo(this).getUserId()};
        taskManager.doStartTask(keys, values, true);
    }
    public  void parseUnnaturalDeathResponse(String response){
        if (response != null && !response.equals("")) {
            try
            {
                JSONObject jobj = new JSONObject(response);
                if (jobj.opt("status").toString().equalsIgnoreCase("1")) {
                    if(jobj.optString("count")!=null && !jobj.optString("count").equalsIgnoreCase("") && !jobj.optString("count").equalsIgnoreCase("")) {
                        unnaturalDeathTotalCount = jobj.optString("count");
                        chargesheetNotificationTotalCount.setText("Total count: " + unnaturalDeathTotalCount);
                    }
                    else
                    {
                        chargesheetNotificationTotalCount.setText("Total count: ");
                    }
                    JSONArray result = jobj.optJSONArray("result");
                    parsePSResponse(result);
                } else {
                    Utility.showAlertDialog(UnnaturalDeathListActivity.this, Constants.ERROR_DETAILS_TITLE, jobj.optString("message"), false);
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(UnnaturalDeathListActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }

    }
    private void parsePSResponse(JSONArray resultArray) {

        psList.clear();
        for (int i = 0; i < resultArray.length(); i++) {
            CrimeReviewDetails psDetails = new CrimeReviewDetails(Parcel.obtain());
            JSONObject object = resultArray.optJSONObject(i);
            psDetails.setCode(object.optString("PSCODE"));
            psDetails.setCrimeCategory(object.optString("PSNAME"));
            psDetails.setCountOfCrime(object.optString("COUNT"));
            psList.add(psDetails);
        }
        unnaturalDeathAdapter.notifyDataSetChanged();
    }


    @Override
    public void onClick(View view, int position) {
        Intent it=new Intent(this,UnnaturalDeathDetailsActivity.class);
        it.putExtra("SELECTED_DATE_UND",selectedDate);
        it.putExtra("SELECTED_FROM_DATE_UND",fromDate);
        it.putExtra("SELECTED_TO_DATE_UND",toDate);
        it.putExtra("SELECTED_PS_UND",psList.get(position).getCode());
        startActivity(it);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
            case R.id.date_filter:
                openFilterDialog();
                break;
            case R.id.ll_fromDate:
                setFromDate(tv_fromDate);
                break;
            case R.id.ll_toDate:
                setToDate(tv_toDate, true);
                break;
        }
    }
    public  void openFilterDialog(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.MyFilterDialogTheme));
        final View filterView = LayoutInflater.from(this).inflate(R.layout.filter_from_date_to_date_layout, null);
        final LinearLayout ll_fromDate = (LinearLayout)filterView.findViewById(R.id.ll_fromDate);
        final LinearLayout ll_toDate = (LinearLayout)filterView.findViewById(R.id.ll_toDate);
        tv_fromDate = (TextView) filterView.findViewById(R.id.tv_fromDate);
        tv_toDate = (TextView) filterView.findViewById(R.id.tv_toDate);
        if(!fromDate.equalsIgnoreCase("")) {
            tv_fromDate.setText(fromDate);
        }
        else {
            tv_fromDate.setText(selectedDate);
        }
        if(!toDate.equalsIgnoreCase("")) {
            tv_toDate.setText(toDate);
        }
        else
        {
            tv_toDate.setText(selectedDate);
        }
        ll_fromDate.setOnClickListener(UnnaturalDeathListActivity.this);
        ll_toDate.setOnClickListener(UnnaturalDeathListActivity.this);
        //alertDialog.setTitle("Filter by date range");
        alertDialog.setView(filterView);
        alertDialog.setPositiveButton("APPLY", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                fromDate = tv_fromDate.getText().toString().trim();
                toDate = tv_toDate.getText().toString().trim();

                if (!toDate.equalsIgnoreCase("") && !fromDate.equalsIgnoreCase("")) {
                    boolean date_result_flag = DateUtils.dateComparison(fromDate, toDate);
                    System.out.println("DateComparison: " + date_result_flag);
                    if (!date_result_flag) {
                        Utility.showAlertDialog(UnnaturalDeathListActivity.this, "Alert !!!", "From date can't be greater than To date", false);
                    } else {
                        getAllunnaturalDeath();
                    }
                }
                dialog.dismiss();


            }
        });
        alertDialog.setNegativeButton("RESET", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                 fromDate ="";
                 toDate = "";
                 getAllunnaturalDeath();
/*
                if (!toDate.equalsIgnoreCase("") && !fromDate.equalsIgnoreCase("")) {
                    boolean date_result_flag = DateUtils.dateComparison(fromDate, toDate);
                    System.out.println("DateComparison: " + date_result_flag);
                    if (!date_result_flag) {
                        Utility.showAlertDialog(UnnaturalDeathListActivity.this, "Alert !!!", "From date can't be greater than To date", false);
                    } else {
                        getAllunnaturalDeath();
                    }
                }
                */
                 dialog.dismiss();
            }
        });
        alertDialog.show();

    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
    private void setToDate(final TextView tv_view, boolean isMaxCurrentDate) {
        Calendar calender = Calendar.getInstance();
        DatePickerDialog mDialog = new DatePickerDialog(UnnaturalDeathListActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        month++;

                        String dt = dateFormat(day, month, year);
                        tv_view.setText(dt);
                    }
                }, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender
                .get(Calendar.DAY_OF_MONTH));
        if (isMaxCurrentDate) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                mDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                mDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            }
        }
        mDialog.show();
    }

    private void setFromDate(final TextView tv_view) {
        Calendar calender = Calendar.getInstance();
        DatePickerDialog mDialog = new DatePickerDialog(UnnaturalDeathListActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        month++;

                        String dt = dateFormat(day, month, year);
                        tv_view.setText(dt);
                    }
                }, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender
                .get(Calendar.DAY_OF_MONTH));
        mDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDialog.show();
    }


    private String dateFormat(int day, int month, int year) {
        String dateStr;
        if (day <= 9) {
            if (month <= 9)
                dateStr = "0" + day + "-0" + month + "-" + year;
            else
                dateStr = "0" + day + "-" + month + "-" + year;
        } else {
            if (month <= 9)
                dateStr = day + "-0" + month + "-" + year;
            else
                dateStr = day + "-" + month + "-" + year;
        }
        return dateStr;
    }

}
