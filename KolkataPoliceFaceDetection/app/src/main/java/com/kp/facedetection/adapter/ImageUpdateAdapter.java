package com.kp.facedetection.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.kp.facedetection.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 29-05-2017.
 */

public class ImageUpdateAdapter extends PagerAdapter {

    private Context context;
    private List<String> criminal_imageList = new ArrayList<String>();

    public ImageUpdateAdapter(Context context, List<String> criminal_imageList) {
        this.context = context;
        this.criminal_imageList = criminal_imageList;

    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View swipeView = LayoutInflater.from(context).inflate(R.layout.swipe_fragment, container, false);
        final ImageView imageView = (ImageView) swipeView.findViewById(R.id.imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImagePopup(criminal_imageList.get(position));
            }
        });

        ImageLoader.getInstance().displayImage(criminal_imageList.get(position),imageView);
        container.addView(swipeView);

        return swipeView;
    }

    @Override
    public int getCount() {
        return criminal_imageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {

        return view==object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((View) object);
    }



    private void showImagePopup(String imageUrl) {

        Dialog imageDialog = new Dialog(context);
        imageDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View inflatedView;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        inflatedView = layoutInflater.inflate(R.layout.image_layout, null, false);
        imageDialog.setContentView(inflatedView);

        final ImageView iv_popup = (ImageView) inflatedView.findViewById(R.id.iv_popup);

        ImageLoader.getInstance().displayImage(imageUrl,iv_popup);
        imageDialog.show();
    }
}
