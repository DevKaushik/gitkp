package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 25-07-2016.
 */
public class CaseSearchFIRAllAccused implements Serializable {

    private String name="";
    private String father="";
    private String address="";
    private String alias="";
    private String psCase = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPsCase() {
        return psCase;
    }

    public void setPsCase(String psCase) {
        this.psCase = psCase;
    }
}
