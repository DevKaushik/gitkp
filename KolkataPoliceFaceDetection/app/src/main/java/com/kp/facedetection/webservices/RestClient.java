package com.kp.facedetection.webservices;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.util.Log;

import com.kp.facedetection.utility.Constants;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

//import org.apache.http.entity.mime.content.ByteArrayBody;

public class RestClient extends AsyncTask<Void, Void, String> {
	private String[] keys, values;
	IResponseCallback callback;
	String methodName;
	private boolean isPost;
	ProgressDialog dialog;
	String appendInServiceUrl;
	Context mContext;
	boolean isUploadImage;
	String imagePath;
	boolean direct_url = false;
	Bitmap bitmap;
	private String[] keysImg = {""}, valuesImg= {""};
	boolean isUpdateImage;

	public RestClient(Context context, String[] keys, String[] values,
			IResponseCallback callback, String methodName,
			String appendInServiceUrl, boolean isPost) {
		this.keys = keys;
		this.values = values;
		this.callback = callback;
		this.appendInServiceUrl = appendInServiceUrl;
		this.methodName = methodName;
		this.isPost = isPost;
		this.mContext = context;
		this.bitmap = null;
	}

	public RestClient(Context context, String[] keys, String[] values,
			IResponseCallback callback, String methodName,
			String appendInServiceUrl, boolean isPost, boolean isUploadImage) {
		this.keys = keys;
		this.values = values;
		this.callback = callback;
		this.appendInServiceUrl = appendInServiceUrl;
		this.methodName = methodName;
		this.isPost = isPost;
		this.mContext = context;
		this.isUploadImage = isUploadImage;
		this.bitmap = null;
	}

	public RestClient(Context context, String[] keys, String[] values,
					  IResponseCallback callback, String methodName,
					  String appendInServiceUrl, boolean isPost, boolean isUploadImage,boolean direct_url) {

		Log.e("TAG","DIRECT URL CALLING");

		this.keys = keys;
		this.values = values;
		this.callback = callback;
		this.appendInServiceUrl = appendInServiceUrl;
		this.methodName = methodName;
		this.isPost = isPost;
		this.mContext = context;
		this.isUploadImage = isUploadImage;
		this.bitmap = null;
		this.direct_url = direct_url;
	}

	public RestClient(Context context, IResponseCallback callback,
			boolean isUploadImage, String imagePath, Bitmap bitmap) {
		this.keys = null;
		this.values = null;
		this.callback = callback;
		this.appendInServiceUrl = null;
		this.methodName = null;
		this.isPost = false;
		this.mContext = context;
		this.imagePath = imagePath;
		this.isUploadImage = isUploadImage;
		this.bitmap = bitmap;
	}

	public RestClient(Context context, String[] keys, String[] values, IResponseCallback callback,
					  boolean isUploadImage,boolean isUpdateImage, String imagePath, Bitmap bitmap) {
		this.keysImg = keys;
		this.valuesImg = values;
		this.callback = callback;
		this.appendInServiceUrl = null;
		this.methodName = null;
		this.isPost = false;
		this.mContext = context;
		this.imagePath = imagePath;
		this.isUploadImage = isUploadImage;
		this.bitmap = bitmap;
		this.isUpdateImage = isUpdateImage;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		try {
			dialog = new ProgressDialog(mContext);
			dialog.setMessage("Please wait...");
			dialog.setCancelable(false);
			dialog.show();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected String doInBackground(Void... arg0) {
		if (isUpdateImage){
			return executeMultipartPostForImageUpdate();
		}
		else if (isUploadImage)
			return executeMultipartPost();
		else
			return isPost ? postApi(direct_url) : sendGet();
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		try {
			dialog.dismiss();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		Log.e("Result", "Result " + result);

		if (result != null) {
			callback.onSuccess(result,keys,values);
		} else {
			callback.onFailure(result);
		}
	}

	private String getRequest() {

		String getUrl = getServiceUrl();
		StringBuffer response = new StringBuffer();
		if (keys.length > 0) {
			getUrl = getUrl + "?";
			for (int i = 0; i < keys.length; i++) {
				getUrl = getUrl + keys[i] + "=" + values[i];
			}
		}
		Log.e("Request", "Request " + getUrl);
		URL url = null;
		try {
			url = new URL(getUrl);
		} catch (MalformedURLException e) {
			System.out.println("MalformedUrlException: " + e.getMessage());
			e.printStackTrace();
			return "-1";
		}

		URLConnection uc = null;
		try {
			uc = url.openConnection();
		} catch (IOException e) {
			System.out.println("IOException: " + e.getMessage());
			e.printStackTrace();
			return "-12";
		}

		String userpass = "uaaUser" + ":" + "uaaPassword";
		// String basicAuth = "Basic " +
		// Base64.encodeBase64(userpass.getBytes());

		// uc.setRequestProperty("Authorization", basicAuth);

		InputStream is = null;
		try {
			is = uc.getInputStream();
		} catch (IOException e) {
			System.out.println("IOException: " + e.getMessage());
			e.printStackTrace();
			return "-13";
		}
		// if (returnResponse) {
		BufferedReader buffReader = new BufferedReader(
				new InputStreamReader(is));
		// StringBuffer response = new StringBuffer();

		String line = null;
		try {
			line = buffReader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			return "-1";
		}
		while (line != null) {
			response.append(line);
			response.append('\n');
			try {
				line = buffReader.readLine();
			} catch (IOException e) {
				System.out.println(" IOException: " + e.getMessage());
				e.printStackTrace();
				return "-14";
			}
		}
		try {
			buffReader.close();
		} catch (IOException e) {
			e.printStackTrace();
			return "-15";
		}
		System.out.println("Response: " + response.toString());
		return response.toString();
		// }

	}

	@SuppressLint("NewApi")
	private String postRequest() {
		String postUrl = getServiceUrl();
		HttpPost httpRequest = new HttpPost(postUrl);
		String headerAuthValue = "1234" + ":cvpClientTrustedSecret";
		byte[] headerData = headerAuthValue.getBytes();
		//
		HttpClient httpClient = new DefaultHttpClient();
		String params = "";
		for (int i = 0; i < keys.length; i++) {
			if (i == 0) {
				params = params + "{";
			} else {
				params = params + ",";
			}
			params = params + "\"" + keys[i] + "\":\"" + values[i] + "\"";
			if (i == (keys.length - 1)) {
				params = params + "}";
			}
		}
		try {
			Log.e("RequestURL", "RequestURL " + postUrl);
			Log.e("RequestParams", "RequestParams " + params);

			StringEntity paramsEntity = new StringEntity(params);

			httpRequest.setEntity(paramsEntity);
			HttpResponse httpResponse = httpClient.execute(httpRequest);
			Log.e("Response Code ", "Response Code "
					+ httpResponse.getStatusLine().getStatusCode());
			if (httpResponse.getStatusLine().getStatusCode() == 200)
				return EntityUtils.toString(httpResponse.getEntity(), "UTF_8");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String sendGet() {

		String getUrl = getServiceUrl();
		// StringBuffer response = new StringBuffer();
		if (keys.length > 0) {
			getUrl = getUrl + "?";
			for (int i = 0; i < keys.length; i++) {
				if (i == 0)
					getUrl = getUrl + keys[i] + "=" + values[i];
				else
					getUrl = getUrl + "&" + keys[i] + "=" + values[i];
			}
		}
		Log.e("Request", "Request " + getUrl);

		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(getUrl);

		String headerAuthValue = "uaaUser:uaaPassword";
		byte[] headerData = headerAuthValue.getBytes();
		request.addHeader("Content-Type", "application/json; charset=UTF-8");
		BufferedReader rd;
		StringBuffer result = null;
		try {
			HttpResponse response = client.execute(request);

			System.out.println("\nSending 'GET' request to URL : " + getUrl);
			System.out.println("Response Code : "
					+ response.getStatusLine().getStatusCode());

			rd = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));

			result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(result.toString());
		return result.toString();
	}

	private String postApi(boolean url) {

		StringBuffer requestString;
		if(!url){

			requestString = new StringBuffer(getServiceUrl());
			Log.e("URL", "URL " + requestString);
		}
		else{
			requestString = new StringBuffer(methodName);
			Log.e("URL", "URL " + requestString);
		}

		ArrayList<BasicNameValuePair> nameValuePairList = new ArrayList<BasicNameValuePair>();
		if (keys != null && keys.length > 0) {
			for (int i = 0; i < keys.length; i++) {
				if (values[i] instanceof String)
					nameValuePairList.add(new BasicNameValuePair(keys[i],
							(String) values[i]));
				Log.e("Parameter" + i, keys[i] + ":" + (String) values[i]);
			}

		}
		String jsonString = null;
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(requestString.toString());
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairList));
			HttpResponse response1 = httpclient.execute(httppost);
			HttpEntity entity = response1.getEntity();
			jsonString = EntityUtils.toString(response1.getEntity());
			return jsonString;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}


	public String executeMultipartPost() {
		this.isUploadImage = false;
		/* Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap(); */
		try {

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.JPEG, 75, bos);
			byte[] data = bos.toByteArray();
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(
					"http://111.93.227.164/upload.php");
			ByteArrayBody bab = new ByteArrayBody(data, imagePath); // File
			System.out.println("Image path = "+imagePath);												// file= new
//			File("/mnt/sdcard/forest.png"); // FileBody bin = new
//			FileBody(file);
			MultipartEntity reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE); //
//			reqEntity.addFilePart("image", bab, "latest.png");
			reqEntity.addPart("image", bab); //
			postRequest.setEntity(reqEntity);
			HttpResponse response = httpClient.execute(postRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent(), "UTF-8"));
			String sResponse;
			StringBuilder s = new StringBuilder();

			while ((sResponse = reader.readLine()) != null) {
				s = s.append(sResponse);
			}
			System.out.println("Response: " + s);
			return s.toString();
		} catch (Exception e) {

			Log.e(e.getClass().getName(), e.getMessage());
			return "-1";
		}
	}



	public String executeMultipartPostForImageUpdate() {
		this.isUpdateImage = false;
		/* Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap(); */
		try {

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.JPEG, 75, bos);
			byte[] data = bos.toByteArray();
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(Constants.METHOD_CRIMINAL_PIC_UPDATE);
			Log.e("URL :","URL:"+Constants.METHOD_CRIMINAL_PIC_UPDATE);
			ByteArrayBody bab = new ByteArrayBody(data, imagePath); // File
            System.out.println("Image path = "+imagePath);
			MultipartEntity reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
//			reqEntity.addFilePart("image", bab, "latest.png");
			Log.e("Image Path ++2++ :",bab+"");
			//reqEntity.addPart("image", bab); //

			Log.e("---before PROV_CRM",valuesImg[0]);
			Log.e("---before USER ID",valuesImg[1]);

			if(valuesImg[0].contains(":"))
				valuesImg[0] = valuesImg[0].replaceAll(":","");
			if(valuesImg[1].contains(":"))
				valuesImg[1] = valuesImg[1].replace(":","");
			Log.e("---after PROV_CRM",valuesImg[0]);
			Log.e("---after USER ID",valuesImg[1]);
			Log.e("---ps",valuesImg[2]);
			reqEntity.addPart("prov_cr_no",new StringBody(valuesImg[0].trim()));
			reqEntity.addPart("user_id",new StringBody(valuesImg[1].trim()));
			reqEntity.addPart("pic", bab);


			postRequest.setEntity(reqEntity);

			HttpResponse response = httpClient.execute(postRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent(), "UTF-8"));
			String sResponse;
			StringBuilder s = new StringBuilder();

			while ((sResponse = reader.readLine()) != null) {
				s = s.append(sResponse);
			}
			System.out.println("Response: " + s);
			return s.toString();
		} catch (Exception e) {

			Log.e(e.getClass().getName(), e.getMessage());
			return "-1";
		}
	}

	private String getServiceUrl() {
		return Constants.API_URL + methodName;
		// if (isVSBCall)
		// return ConfigSettings.VSB_SERVICE_URL + methodName
		// + appendInServiceUrl;
		// else {
		// return ConfigSettings.MDB_SERVICE_URL + methodName
		// + appendInServiceUrl;
		// }
	}
}
