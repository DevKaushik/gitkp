package com.kp.facedetection.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DAT-165 on 23-12-2016.
 */
public class PMReportSearchDetails implements Parcelable{

    private String psName = "";
    private String psCode = "";
    private String inquestNo = "";
    private String inquestYr = "";
    private String inquestDt = "";
    private String pmNo = "";
    private String pmDt = "";
    private String pmYr = "";
    private String nameDeceased = "";
    private String gender = "";
    private String ageVal = "";
    private String ageIndicator = "";
    private String deadHouse = "";
    private String stationCode = "";
    private String stationName = "";
    private String doneBy = "";
    private String firNo = "";
    private String firYr = "";
    private String firPS = "";
    private String opName = "";

    private String religion = "";
    private String when_ce_brought = "";
    private String name_const_relatives = "";
    private String date_dispatch = "";
    private String date_arrive = "";
    private String date_exam = "";
    private String info_by_police = "";
    private String indent_bef_no ="";
    private String length_ft = "";
    private String length_inch = "";
    private String subject_condition = "";
    private String wounds = "";
    private String bruises = "";
    private String Ligature_marks = "";
    private String sculp_skull_vertebrae = "";
    private String membrane = "";
    private String brain_sp_cord = "";
    private String walls_ribs_cartilage = "";
    private String laryncs = "";
    private String right_lung = "";
    private String left_lung = "";
    private String pericardium = "";
    private String heart = "";
    private String vessels = "";
    private String peritoneum = "";
    private String esophagus = "";
    private String stomach = "";
    private String small_intestine = "";
    private String large_intestine = "";
    private String liver = "";
    private String spleen = "";
    private String kidneys = "";
    private String bladder= "";
    private String generation_external = "";
    private String generation_internal = "";
    private String mbj_injury = "";
    private String mbj_disease_deformity = "";
    private String mbj_fracture = "";
    private String mbj_delocation = "";
    private String additional_remarks = "";
    private String option_mo = "";
    private String option_cs = "";
    private String modi_user = "";
    private String modi_dt= "";
    private String submitted = "";
    private String mp_viscerae = "";
    private String mp_blood = "";
    private String mp_scalp_hair = "";
    private String mp_nails = "";
    private String mp_apparels = "";
    private String mp_ligature = "";
    private String mp_foreign = "";
    private String mp_skin = "";
    private String mp_vaginal_anal = "";
    private String mp_others = "";
    private String mp_other_details = "";
    private String station_billNo = "";
    private String station_billDt = "";
    private String sent_on = "";
    private String released = "";
    private String neft_done = "";
    private String neft_dt ="";
    private String ref_by = "";
    private String ref_date="";
    private String ref_no = "";
    private String tranId = "";
    private String remarks = "";
    private String file_size = "";
    private String mime_type ="";
    private String file_name = "";
    private String pdf_url = "";
    private String criminal_pdf_url = "";

    private String firDate = "";


    public PMReportSearchDetails(Parcel in) {
        psName = in.readString();
        psCode = in.readString();
        inquestNo = in.readString();
        inquestYr = in.readString();
        inquestDt = in.readString();
        pmNo = in.readString();
        pmDt = in.readString();
        pmYr = in.readString();
        nameDeceased = in.readString();
        gender = in.readString();
        ageVal = in.readString();
        ageIndicator = in.readString();
        deadHouse = in.readString();
        stationCode = in.readString();
        stationName = in.readString();
        doneBy = in.readString();
        firNo = in.readString();
        firYr = in.readString();
        firPS = in.readString();
        opName = in.readString();
        religion = in.readString();
        when_ce_brought = in.readString();
        name_const_relatives = in.readString();
        date_dispatch = in.readString();
        date_arrive = in.readString();
        date_exam = in.readString();
        info_by_police = in.readString();
        indent_bef_no = in.readString();
        length_ft = in.readString();
        length_inch = in.readString();
        subject_condition = in.readString();
        wounds = in.readString();
        bruises = in.readString();
        Ligature_marks = in.readString();
        sculp_skull_vertebrae = in.readString();
        membrane = in.readString();
        brain_sp_cord = in.readString();
        walls_ribs_cartilage = in.readString();
        laryncs = in.readString();
        right_lung = in.readString();
        left_lung = in.readString();
        pericardium = in.readString();
        heart = in.readString();
        vessels = in.readString();
        peritoneum = in.readString();
        esophagus = in.readString();
        stomach = in.readString();
        small_intestine = in.readString();
        large_intestine = in.readString();
        liver = in.readString();
        spleen = in.readString();
        kidneys = in.readString();
        bladder = in.readString();
        generation_external = in.readString();
        generation_internal = in.readString();
        mbj_injury = in.readString();
        mbj_disease_deformity = in.readString();
        mbj_fracture = in.readString();
        mbj_delocation = in.readString();
        additional_remarks = in.readString();
        option_mo = in.readString();
        option_cs = in.readString();
        modi_user = in.readString();
        modi_dt = in.readString();
        submitted = in.readString();
        mp_viscerae = in.readString();
        mp_blood = in.readString();
        mp_scalp_hair = in.readString();
        mp_nails = in.readString();
        mp_apparels = in.readString();
        mp_ligature = in.readString();
        mp_foreign = in.readString();
        mp_skin = in.readString();
        mp_vaginal_anal = in.readString();
        mp_others = in.readString();
        mp_other_details = in.readString();
        station_billNo = in.readString();
        station_billDt = in.readString();
        sent_on = in.readString();
        released = in.readString();
        neft_done = in.readString();
        neft_dt = in.readString();
        ref_by = in.readString();
        ref_date = in.readString();
        ref_no = in.readString();
        tranId = in.readString();
        remarks = in.readString();
        file_size = in.readString();
        mime_type = in.readString();
        file_name = in.readString();
        pdf_url = in.readString();
        criminal_pdf_url = in.readString();
        firDate = in.readString();
    }

    public static final Creator<PMReportSearchDetails> CREATOR = new Creator<PMReportSearchDetails>() {
        @Override
        public PMReportSearchDetails createFromParcel(Parcel in) {
            return new PMReportSearchDetails(in);
        }

        @Override
        public PMReportSearchDetails[] newArray(int size) {
            return new PMReportSearchDetails[size];
        }
    };

    public String getPsName() {
        return psName;
    }

    public void setPsName(String psName) {
        this.psName = psName;
    }

    public String getPsCode() {
        return psCode;
    }

    public void setPsCode(String psCode) {
        this.psCode = psCode;
    }

    public String getInquestNo() {
        return inquestNo;
    }

    public void setInquestNo(String inquestNo) {
        this.inquestNo = inquestNo;
    }

    public String getInquestYr() {
        return inquestYr;
    }

    public void setInquestYr(String inquestYr) {
        this.inquestYr = inquestYr;
    }

    public String getInquestDt() {
        return inquestDt;
    }

    public void setInquestDt(String inquestDt) {
        this.inquestDt = inquestDt;
    }

    public String getPmNo() {
        return pmNo;
    }

    public void setPmNo(String pmNo) {
        this.pmNo = pmNo;
    }

    public String getPmDt() {
        return pmDt;
    }

    public void setPmDt(String pmDt) {
        this.pmDt = pmDt;
    }

    public String getPmYr() {
        return pmYr;
    }

    public void setPmYr(String pmYr) {
        this.pmYr = pmYr;
    }

    public String getNameDeceased() {
        return nameDeceased;
    }

    public void setNameDeceased(String nameDeceased) {
        this.nameDeceased = nameDeceased;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAgeVal() {
        return ageVal;
    }

    public void setAgeVal(String ageVal) {
        this.ageVal = ageVal;
    }

    public String getAgeIndicator() {
        return ageIndicator;
    }

    public void setAgeIndicator(String ageIndicator) {
        this.ageIndicator = ageIndicator;
    }

    public String getDeadHouse() {
        return deadHouse;
    }

    public void setDeadHouse(String deadHouse) {
        this.deadHouse = deadHouse;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getDoneBy() {
        return doneBy;
    }

    public void setDoneBy(String doneBy) {
        this.doneBy = doneBy;
    }

    public String getFirNo() {
        return firNo;
    }

    public void setFirNo(String firNo) {
        this.firNo = firNo;
    }

    public String getFirYr() {
        return firYr;
    }

    public void setFirYr(String firYr) {
        this.firYr = firYr;
    }

    public String getFirPS() {
        return firPS;
    }

    public void setFirPS(String firPS) {
        this.firPS = firPS;
    }

    public String getOpName() {
        return opName;
    }

    public void setOpName(String opName) {
        this.opName = opName;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getWhen_ce_brought() {
        return when_ce_brought;
    }

    public void setWhen_ce_brought(String when_ce_brought) {
        this.when_ce_brought = when_ce_brought;
    }

    public String getName_const_relatives() {
        return name_const_relatives;
    }

    public void setName_const_relatives(String name_const_relatives) {
        this.name_const_relatives = name_const_relatives;
    }

    public String getDate_dispatch() {
        return date_dispatch;
    }

    public void setDate_dispatch(String date_dispatch) {
        this.date_dispatch = date_dispatch;
    }

    public String getDate_arrive() {
        return date_arrive;
    }

    public void setDate_arrive(String date_arrive) {
        this.date_arrive = date_arrive;
    }

    public String getDate_exam() {
        return date_exam;
    }

    public void setDate_exam(String date_exam) {
        this.date_exam = date_exam;
    }

    public String getInfo_by_police() {
        return info_by_police;
    }

    public void setInfo_by_police(String info_by_police) {
        this.info_by_police = info_by_police;
    }

    public String getIndent_bef_no() {
        return indent_bef_no;
    }

    public void setIndent_bef_no(String indent_bef_no) {
        this.indent_bef_no = indent_bef_no;
    }

    public String getLength_ft() {
        return length_ft;
    }

    public void setLength_ft(String length_ft) {
        this.length_ft = length_ft;
    }

    public String getLength_inch() {
        return length_inch;
    }

    public void setLength_inch(String length_inch) {
        this.length_inch = length_inch;
    }

    public String getSubject_condition() {
        return subject_condition;
    }

    public void setSubject_condition(String subject_condition) {
        this.subject_condition = subject_condition;
    }

    public String getWounds() {
        return wounds;
    }

    public void setWounds(String wounds) {
        this.wounds = wounds;
    }

    public String getBruises() {
        return bruises;
    }

    public void setBruises(String bruises) {
        this.bruises = bruises;
    }

    public String getLigature_marks() {
        return Ligature_marks;
    }

    public void setLigature_marks(String ligature_marks) {
        Ligature_marks = ligature_marks;
    }

    public String getSculp_skull_vertebrae() {
        return sculp_skull_vertebrae;
    }

    public void setSculp_skull_vertebrae(String sculp_skull_vertebrae) {
        this.sculp_skull_vertebrae = sculp_skull_vertebrae;
    }

    public String getMembrane() {
        return membrane;
    }

    public void setMembrane(String membrane) {
        this.membrane = membrane;
    }

    public String getBrain_sp_cord() {
        return brain_sp_cord;
    }

    public void setBrain_sp_cord(String brain_sp_cord) {
        this.brain_sp_cord = brain_sp_cord;
    }

    public String getWalls_ribs_cartilage() {
        return walls_ribs_cartilage;
    }

    public void setWalls_ribs_cartilage(String walls_ribs_cartilage) {
        this.walls_ribs_cartilage = walls_ribs_cartilage;
    }

    public String getLaryncs() {
        return laryncs;
    }

    public void setLaryncs(String laryncs) {
        this.laryncs = laryncs;
    }

    public String getRight_lung() {
        return right_lung;
    }

    public void setRight_lung(String right_lung) {
        this.right_lung = right_lung;
    }

    public String getLeft_lung() {
        return left_lung;
    }

    public void setLeft_lung(String left_lung) {
        this.left_lung = left_lung;
    }

    public String getPericardium() {
        return pericardium;
    }

    public void setPericardium(String pericardium) {
        this.pericardium = pericardium;
    }

    public String getHeart() {
        return heart;
    }

    public void setHeart(String heart) {
        this.heart = heart;
    }

    public String getVessels() {
        return vessels;
    }

    public void setVessels(String vessels) {
        this.vessels = vessels;
    }

    public String getPeritoneum() {
        return peritoneum;
    }

    public void setPeritoneum(String peritoneum) {
        this.peritoneum = peritoneum;
    }

    public String getEsophagus() {
        return esophagus;
    }

    public void setEsophagus(String esophagus) {
        this.esophagus = esophagus;
    }

    public String getStomach() {
        return stomach;
    }

    public void setStomach(String stomach) {
        this.stomach = stomach;
    }

    public String getSmall_intestine() {
        return small_intestine;
    }

    public void setSmall_intestine(String small_intestine) {
        this.small_intestine = small_intestine;
    }

    public String getLarge_intestine() {
        return large_intestine;
    }

    public void setLarge_intestine(String large_intestine) {
        this.large_intestine = large_intestine;
    }

    public String getLiver() {
        return liver;
    }

    public void setLiver(String liver) {
        this.liver = liver;
    }

    public String getSpleen() {
        return spleen;
    }

    public void setSpleen(String spleen) {
        this.spleen = spleen;
    }

    public String getKidneys() {
        return kidneys;
    }

    public void setKidneys(String kidneys) {
        this.kidneys = kidneys;
    }

    public String getBladder() {
        return bladder;
    }

    public void setBladder(String bladder) {
        this.bladder = bladder;
    }

    public String getGeneration_external() {
        return generation_external;
    }

    public void setGeneration_external(String generation_external) {
        this.generation_external = generation_external;
    }

    public String getGeneration_internal() {
        return generation_internal;
    }

    public void setGeneration_internal(String generation_internal) {
        this.generation_internal = generation_internal;
    }

    public String getMbj_injury() {
        return mbj_injury;
    }

    public void setMbj_injury(String mbj_injury) {
        this.mbj_injury = mbj_injury;
    }

    public String getMbj_disease_deformity() {
        return mbj_disease_deformity;
    }

    public void setMbj_disease_deformity(String mbj_disease_deformity) {
        this.mbj_disease_deformity = mbj_disease_deformity;
    }

    public String getMbj_fracture() {
        return mbj_fracture;
    }

    public void setMbj_fracture(String mbj_fracture) {
        this.mbj_fracture = mbj_fracture;
    }

    public String getMbj_delocation() {
        return mbj_delocation;
    }

    public void setMbj_delocation(String mbj_delocation) {
        this.mbj_delocation = mbj_delocation;
    }

    public String getAdditional_remarks() {
        return additional_remarks;
    }

    public void setAdditional_remarks(String additional_remarks) {
        this.additional_remarks = additional_remarks;
    }

    public String getOption_mo() {
        return option_mo;
    }

    public void setOption_mo(String option_mo) {
        this.option_mo = option_mo;
    }

    public String getOption_cs() {
        return option_cs;
    }

    public void setOption_cs(String option_cs) {
        this.option_cs = option_cs;
    }

    public String getModi_user() {
        return modi_user;
    }

    public void setModi_user(String modi_user) {
        this.modi_user = modi_user;
    }

    public String getModi_dt() {
        return modi_dt;
    }

    public void setModi_dt(String modi_dt) {
        this.modi_dt = modi_dt;
    }

    public String getSubmitted() {
        return submitted;
    }

    public void setSubmitted(String submitted) {
        this.submitted = submitted;
    }

    public String getMp_viscerae() {
        return mp_viscerae;
    }

    public void setMp_viscerae(String mp_viscerae) {
        this.mp_viscerae = mp_viscerae;
    }

    public String getMp_blood() {
        return mp_blood;
    }

    public void setMp_blood(String mp_blood) {
        this.mp_blood = mp_blood;
    }

    public String getMp_scalp_hair() {
        return mp_scalp_hair;
    }

    public void setMp_scalp_hair(String mp_scalp_hair) {
        this.mp_scalp_hair = mp_scalp_hair;
    }

    public String getMp_nails() {
        return mp_nails;
    }

    public void setMp_nails(String mp_nails) {
        this.mp_nails = mp_nails;
    }

    public String getMp_apparels() {
        return mp_apparels;
    }

    public void setMp_apparels(String mp_apparels) {
        this.mp_apparels = mp_apparels;
    }

    public String getMp_ligature() {
        return mp_ligature;
    }

    public void setMp_ligature(String mp_ligature) {
        this.mp_ligature = mp_ligature;
    }

    public String getMp_foreign() {
        return mp_foreign;
    }

    public void setMp_foreign(String mp_foreign) {
        this.mp_foreign = mp_foreign;
    }

    public String getMp_skin() {
        return mp_skin;
    }

    public void setMp_skin(String mp_skin) {
        this.mp_skin = mp_skin;
    }

    public String getMp_vaginal_anal() {
        return mp_vaginal_anal;
    }

    public void setMp_vaginal_anal(String mp_vaginal_anal) {
        this.mp_vaginal_anal = mp_vaginal_anal;
    }

    public String getMp_others() {
        return mp_others;
    }

    public void setMp_others(String mp_others) {
        this.mp_others = mp_others;
    }

    public String getMp_other_details() {
        return mp_other_details;
    }

    public void setMp_other_details(String mp_other_details) {
        this.mp_other_details = mp_other_details;
    }

    public String getStation_billNo() {
        return station_billNo;
    }

    public void setStation_billNo(String station_billNo) {
        this.station_billNo = station_billNo;
    }

    public String getStation_billDt() {
        return station_billDt;
    }

    public void setStation_billDt(String station_billDt) {
        this.station_billDt = station_billDt;
    }

    public String getSent_on() {
        return sent_on;
    }

    public void setSent_on(String sent_on) {
        this.sent_on = sent_on;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getNeft_done() {
        return neft_done;
    }

    public void setNeft_done(String neft_done) {
        this.neft_done = neft_done;
    }

    public String getNeft_dt() {
        return neft_dt;
    }

    public void setNeft_dt(String neft_dt) {
        this.neft_dt = neft_dt;
    }

    public String getRef_by() {
        return ref_by;
    }

    public void setRef_by(String ref_by) {
        this.ref_by = ref_by;
    }

    public String getRef_date() {
        return ref_date;
    }

    public void setRef_date(String ref_date) {
        this.ref_date = ref_date;
    }

    public String getRef_no() {
        return ref_no;
    }

    public void setRef_no(String ref_no) {
        this.ref_no = ref_no;
    }

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getFile_size() {
        return file_size;
    }

    public void setFile_size(String file_size) {
        this.file_size = file_size;
    }

    public String getMime_type() {
        return mime_type;
    }

    public void setMime_type(String mime_type) {
        this.mime_type = mime_type;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getPdf_url() {
        return pdf_url;
    }

    public void setPdf_url(String pdf_url) {
        this.pdf_url = pdf_url;
    }

    public String getCriminal_pdf_url() {
        return criminal_pdf_url;
    }

    public void setCriminal_pdf_url(String criminal_pdf_url) {
        this.criminal_pdf_url = criminal_pdf_url;
    }

    public String getFirDate() {
        return firDate;
    }

    public void setFirDate(String firDate) {
        this.firDate = firDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(psName);
        dest.writeString(psCode);
        dest.writeString(inquestNo);
        dest.writeString(inquestYr);
        dest.writeString(inquestDt);
        dest.writeString(pmNo);
        dest.writeString(pmDt);
        dest.writeString(pmYr);
        dest.writeString(nameDeceased);
        dest.writeString(gender);
        dest.writeString(ageVal);
        dest.writeString(ageIndicator);
        dest.writeString(deadHouse);
        dest.writeString(stationCode);
        dest.writeString(stationName);
        dest.writeString(doneBy);
        dest.writeString(firNo);
        dest.writeString(firYr);
        dest.writeString(firPS);
        dest.writeString(opName);
        dest.writeString(religion);
        dest.writeString(when_ce_brought);
        dest.writeString(name_const_relatives);
        dest.writeString(date_dispatch);
        dest.writeString(date_arrive);
        dest.writeString(date_exam);
        dest.writeString(info_by_police);
        dest.writeString(indent_bef_no);
        dest.writeString(length_ft);
        dest.writeString(length_inch);
        dest.writeString(subject_condition);
        dest.writeString(wounds);
        dest.writeString(bruises);
        dest.writeString(Ligature_marks);
        dest.writeString(sculp_skull_vertebrae);
        dest.writeString(membrane);
        dest.writeString(brain_sp_cord);
        dest.writeString(walls_ribs_cartilage);
        dest.writeString(laryncs);
        dest.writeString(right_lung);
        dest.writeString(left_lung);
        dest.writeString(pericardium);
        dest.writeString(heart);
        dest.writeString(vessels);
        dest.writeString(peritoneum);
        dest.writeString(esophagus);
        dest.writeString(stomach);
        dest.writeString(small_intestine);
        dest.writeString(large_intestine);
        dest.writeString(liver);
        dest.writeString(spleen);
        dest.writeString(kidneys);
        dest.writeString(bladder);
        dest.writeString(generation_external);
        dest.writeString(generation_internal);
        dest.writeString(mbj_injury);
        dest.writeString(mbj_disease_deformity);
        dest.writeString(mbj_fracture);
        dest.writeString(mbj_delocation);
        dest.writeString(additional_remarks);
        dest.writeString(option_mo);
        dest.writeString(option_cs);
        dest.writeString(modi_user);
        dest.writeString(modi_dt);
        dest.writeString(submitted);
        dest.writeString(mp_viscerae);
        dest.writeString(mp_blood);
        dest.writeString(mp_scalp_hair);
        dest.writeString(mp_nails);
        dest.writeString(mp_apparels);
        dest.writeString(mp_ligature);
        dest.writeString(mp_foreign);
        dest.writeString(mp_skin);
        dest.writeString(mp_vaginal_anal);
        dest.writeString(mp_others);
        dest.writeString(mp_other_details);
        dest.writeString(station_billNo);
        dest.writeString(station_billDt);
        dest.writeString(sent_on);
        dest.writeString(released);
        dest.writeString(neft_done);
        dest.writeString(neft_dt);
        dest.writeString(ref_by);
        dest.writeString(ref_date);
        dest.writeString(ref_no);
        dest.writeString(tranId);
        dest.writeString(remarks);
        dest.writeString(file_size);
        dest.writeString(mime_type);
        dest.writeString(file_name);
        dest.writeString(pdf_url);
        dest.writeString(criminal_pdf_url);
        dest.writeString(firDate);
    }
}
