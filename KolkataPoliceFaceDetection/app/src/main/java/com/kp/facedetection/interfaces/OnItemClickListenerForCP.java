package com.kp.facedetection.interfaces;

import android.view.View;

/**
 * Created by user on 10-04-2018.
 */

public interface OnItemClickListenerForCP {
    public void  onItemClickForCP(View v, int pos);
}
