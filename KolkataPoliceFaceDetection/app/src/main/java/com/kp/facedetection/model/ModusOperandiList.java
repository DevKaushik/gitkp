package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-Asset-131 on 06-05-2016.
 */
public class ModusOperandiList implements Serializable {

    private String mod_operand="";

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    private  boolean selected = false;

    public String getMod_operand() {
        return mod_operand;
    }

    public void setMod_operand(String mod_operand) {
        this.mod_operand = mod_operand;
    }


}
