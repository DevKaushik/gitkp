package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.HCourtCaseDetails;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by DAT-165 on 01-11-2016.
 */
public class HighCourtCaseSearchAdapter extends BaseAdapter {


    private Context context;
    private List<HCourtCaseDetails> hCourtCaseDetailsList;

    public HighCourtCaseSearchAdapter(Context context, List<HCourtCaseDetails> hCourtCaseDetailsList) {
        this.context = context;
        this.hCourtCaseDetailsList = hCourtCaseDetailsList;
    }


    @Override
    public int getCount() {

        int size = (hCourtCaseDetailsList.size()>0)? hCourtCaseDetailsList.size():0;
        return size;

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.court_case_list_item, null);


            holder.tv_slNo=(TextView)convertView.findViewById(R.id.tv_slNo);
            holder.tv_caseDate=(TextView)convertView.findViewById(R.id.tv_caseDate);
            holder.tv_CourtNo=(TextView)convertView.findViewById(R.id.tv_CourtNo);
            holder.tv_briefNote=(TextView)convertView.findViewById(R.id.tv_briefNote);

            Constants.changefonts(holder.tv_slNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_caseDate, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_CourtNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_briefNote, context, "Calibri.ttf");

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);

        holder.tv_slNo.setText(sl_no);
        holder.tv_caseDate.setText("Case Date: "+ hCourtCaseDetailsList.get(position).getCaseDate().trim());
        holder.tv_CourtNo.setText("Court Room No: "+hCourtCaseDetailsList.get(position).getCourtNo().trim());
        if(hCourtCaseDetailsList.get(position).getBriefNote() != null){
            holder.tv_briefNote.setText("Brief Note: "+hCourtCaseDetailsList.get(position).getBriefNote().trim());
        }
        else
            holder.tv_briefNote.setText("Brief Note: No notes available.");


        return convertView;
    }

    class ViewHolder {

        TextView tv_slNo,tv_caseDate,tv_CourtNo,tv_briefNote;

    }
}
