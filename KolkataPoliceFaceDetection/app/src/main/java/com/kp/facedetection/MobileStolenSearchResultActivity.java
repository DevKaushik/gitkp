package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kp.facedetection.adapter.MobileStolenSearchAdapter;
import com.kp.facedetection.model.MobileStolenSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class MobileStolenSearchResultActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, Observer {

    private String[] keys;
    private String[] values;
    private String pageno;
    private String totalResult = "";
    private String searchCase = "";
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private List<MobileStolenSearchDetails> mobileStolenSearchDetailsList;
    MobileStolenSearchAdapter mobileStolenSearchAdapter;

    private TextView tv_resultCount;
    private ListView lv_mobileStolenSearchList;
    private Button btnLoadMore;

    private TextView tv_watermark;
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile_stolen_search_result_layout);
        ObservableObject.getInstance().addObserver(this);

        initialization();
        clickEvents();

    }

    private void initialization() {

        tv_resultCount = (TextView) findViewById(R.id.tv_resultCount);
        lv_mobileStolenSearchList = (ListView) findViewById(R.id.lv_mobileStolenSearchList);

        // LoadMore button
        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        keys = getIntent().getStringArrayExtra("keys");
        values = getIntent().getStringArrayExtra("value");
        pageno = getIntent().getStringExtra("pageno");
        totalResult = getIntent().getStringExtra("totalResult");

        mobileStolenSearchDetailsList = (List<MobileStolenSearchDetails>) getIntent().getSerializableExtra("MOBILE_STOLEN_SEARCH_LIST");

        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }


        mobileStolenSearchAdapter = new MobileStolenSearchAdapter(this, mobileStolenSearchDetailsList);
        lv_mobileStolenSearchList.setAdapter(mobileStolenSearchAdapter);
        mobileStolenSearchAdapter.notifyDataSetChanged();


        if (Integer.parseInt(totalResult) > 20) {

            // Adding Load More button to listview at bottom
            lv_mobileStolenSearchList.addFooterView(btnLoadMore);
        }

           lv_mobileStolenSearchList.setOnItemClickListener(this);


        /*  Set user name as Watermark  */
        tv_watermark = (TextView) findViewById(R.id.tv_watermark);
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-55);

    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:

                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                // Utility.logout(this);
                break;
        }
        return true;
    }

    private void clickEvents() {

        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // Starting a new async task
                fetchMobileStolenSearchResultPagination();
            }
        });

    }



    private void fetchMobileStolenSearchResultPagination() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_MOBILE_STOLEN_SEARCH);
        taskManager.setMobileStolenSearchPagination(true);

        values[7] = (Integer.parseInt(pageno) + 1) + "";

        taskManager.doStartTask(keys, values, true);
    }

    public void parseMobileStolenSearchPagination(String result, String[] keys, String[] values) {

        //System.out.println("Mobile Stolen Search Result Pagination: " + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("page").toString();
                    totalResult = jObj.opt("count").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseMobileStolenSearchResponse(resultArray);

                } else {
                    showAlertDialog(this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }


            } catch (JSONException e) {

                e.printStackTrace();
                showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }

    private void parseMobileStolenSearchResponse(JSONArray resultArray) {

        for (int i = 0; i < resultArray.length(); i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj = resultArray.getJSONObject(i);

                MobileStolenSearchDetails mobileStolenSearchDetails = new MobileStolenSearchDetails();

                if (!jsonObj.optString("ROWNUMBER").equalsIgnoreCase("null")) {
                    mobileStolenSearchDetails.setRowNo(jsonObj.optString("ROWNUMBER"));
                }

                if (!jsonObj.optString("TRNID").equalsIgnoreCase("null")) {
                    mobileStolenSearchDetails.setTrnId(jsonObj.optString("TRNID"));
                }

                if (!jsonObj.optString("DTMISSING").equalsIgnoreCase("null")) {
                    mobileStolenSearchDetails.setDtMissing(jsonObj.optString("DTMISSING"));
                }

                if (!jsonObj.optString("PSCODE").equalsIgnoreCase("null")) {
                    mobileStolenSearchDetails.setPsCode(jsonObj.optString("PSCODE"));
                }

                if (!jsonObj.optString("MOBILENO").equalsIgnoreCase("null")) {
                    mobileStolenSearchDetails.setMobileNo(jsonObj.optString("MOBILENO"));
                }

                if (!jsonObj.optString("IMEI").equalsIgnoreCase("null")) {
                    mobileStolenSearchDetails.setImeiNo(jsonObj.optString("IMEI"));
                }

                if (!jsonObj.optString("COMPLNTNAME").equalsIgnoreCase("null")) {
                    mobileStolenSearchDetails.setComplainantName(jsonObj.optString("COMPLNTNAME"));
                }

                if (!jsonObj.optString("CASEREF").equalsIgnoreCase("null")) {
                    mobileStolenSearchDetails.setCaseRef(jsonObj.optString("CASEREF"));
                }

                if (!jsonObj.optString("OPNAME").equalsIgnoreCase("null")) {
                    mobileStolenSearchDetails.setPsName(jsonObj.optString("OPNAME"));
                }

                if(!jsonObj.optString("COMPLNTADDR").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setComplainantAddress(jsonObj.optString("COMPLNTADDR"));
                }

                if(!jsonObj.optString("PLMISSING").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setPlMissing(jsonObj.optString("PLMISSING"));
                }

                if(!jsonObj.optString("IONAME").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setIoName(jsonObj.optString("IONAME"));
                }

                if(!jsonObj.optString("GDEDATE").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setGdeDate(jsonObj.optString("GDEDATE"));
                }

                if(!jsonObj.optString("BRFACT").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setBrFact(jsonObj.optString("BRFACT"));
                }

                if(!jsonObj.optString("PRSTATUS").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setPrStatus(jsonObj.optString("PRSTATUS"));
                }

                if(!jsonObj.optString("REFERENCE").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setReference(jsonObj.optString("REFERENCE"));
                }

                if(!jsonObj.optString("FIR_GD").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setFirGD(jsonObj.optString("FIR_GD"));
                }

                if(!jsonObj.optString("CONTACTNO").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setContactNo(jsonObj.optString("CONTACTNO"));
                }

                if(!jsonObj.optString("USEC").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setUsec(jsonObj.optString("USEC"));
                }

                if(!jsonObj.optString("SPROVIDER").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setsProvider(jsonObj.optString("SPROVIDER"));
                }

                if(!jsonObj.optString("MAKE").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setMake(jsonObj.optString("MAKE"));
                }

                if(!jsonObj.optString("MODEL").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setModel(jsonObj.optString("MODEL"));
                }

                if(!jsonObj.optString("DTHANDEDOVER").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setDtHandedOver(jsonObj.optString("DTHANDEDOVER"));
                }

                if(!jsonObj.optString("SOURCENAME").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setSourceName(jsonObj.optString("SOURCENAME"));
                }

                if(!jsonObj.optString("TRACED_ON").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setTracedOn(jsonObj.optString("TRACED_ON"));
                }

                if(!jsonObj.optString("TRACED_MOBNO").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setTracedMobNo(jsonObj.optString("TRACED_MOBNO"));
                }

                if(!jsonObj.optString("NAME_USER").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setNameUser(jsonObj.optString("NAME_USER"));
                }

                if(!jsonObj.optString("ADDR").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setAddr(jsonObj.optString("ADDR"));
                }

                if(!jsonObj.optString("RECOVDT").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setRecoverDate(jsonObj.optString("RECOVDT"));
                }

                if(!jsonObj.optString("RECOVDBY").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setRecoverBy(jsonObj.optString("RECOVDBY"));
                }

                if(!jsonObj.optString("ESN").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setEsn(jsonObj.optString("ESN"));
                }

                if(!jsonObj.optString("DEVTYPE").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setDevType(jsonObj.optString("DEVTYPE"));
                }

                if(!jsonObj.optString("MOBILENO2").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setMobileNo2(jsonObj.optString("MOBILENO2"));
                }

                if(!jsonObj.optString("IMEI2").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setImei2(jsonObj.optString("IMEI2"));
                }

                if(!jsonObj.optString("ESN2").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setEsn2(jsonObj.optString("ESN2"));
                }

                if(!jsonObj.optString("SPROVIDER2").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setSprovider2(jsonObj.optString("SPROVIDER2"));
                }

                mobileStolenSearchDetailsList.add(mobileStolenSearchDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        int currentPosition = lv_mobileStolenSearchList.getFirstVisiblePosition();
        int lastVisiblePosition = lv_mobileStolenSearchList.getLastVisiblePosition();

        // Setting new scroll position
        lv_mobileStolenSearchList.setSelectionFromTop(currentPosition + 1, 0);

        if((Integer.parseInt(totalResult)-lastVisiblePosition)<20){
            lv_mobileStolenSearchList.removeFooterView(btnLoadMore);
        }
    }


    private void showAlertDialog(Context context, String title, final String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        passData(position);

    }

    private void passData(int position){

        Intent in = new Intent(MobileStolenSearchResultActivity.this, MobileStolenSearchDetailsActivity.class);

        in.putExtra("Holder_MobileNo",mobileStolenSearchDetailsList.get(position).getMobileNo());
        in.putExtra("Holder_IMEINo",mobileStolenSearchDetailsList.get(position).getImeiNo());
        in.putExtra("Holder_MissingDt",mobileStolenSearchDetailsList.get(position).getDtMissing());
        in.putExtra("Holder_ComplainantName",mobileStolenSearchDetailsList.get(position).getComplainantName());
        in.putExtra("Holder_ComplainantAddress",mobileStolenSearchDetailsList.get(position).getComplainantAddress());
        in.putExtra("Holder_MissingPlace",mobileStolenSearchDetailsList.get(position).getPlMissing());
        in.putExtra("Holder_IOName",mobileStolenSearchDetailsList.get(position).getIoName());
        in.putExtra("Holder_PSName",mobileStolenSearchDetailsList.get(position).getPsName());
        in.putExtra("Holder_CaseRef",mobileStolenSearchDetailsList.get(position).getCaseRef());
        in.putExtra("Holder_FIRGD",mobileStolenSearchDetailsList.get(position).getFirGD());
        in.putExtra("Holder_GdDate",mobileStolenSearchDetailsList.get(position).getGdeDate());
        in.putExtra("Holder_PrStatus",mobileStolenSearchDetailsList.get(position).getPrStatus());
        in.putExtra("Holder_Make",mobileStolenSearchDetailsList.get(position).getMake());
        in.putExtra("Holder_Model",mobileStolenSearchDetailsList.get(position).getModel());
        in.putExtra("Holder_ContactNo",mobileStolenSearchDetailsList.get(position).getContactNo());
        in.putExtra("Holder_BriefFact",mobileStolenSearchDetailsList.get(position).getBrFact());
        in.putExtra("Holder_ServiceProvider",mobileStolenSearchDetailsList.get(position).getsProvider());
        in.putExtra("Holder_TracedMobNo",mobileStolenSearchDetailsList.get(position).getTracedMobNo());
        in.putExtra("Holder_NameOfUser",mobileStolenSearchDetailsList.get(position).getNameUser());
        in.putExtra("Holder_TracedOn",mobileStolenSearchDetailsList.get(position).getTracedOn());
        in.putExtra("Holder_RecoveryDate",mobileStolenSearchDetailsList.get(position).getRecoverDate());
        in.putExtra("Holder_RecoverBy",mobileStolenSearchDetailsList.get(position).getRecoverBy());
        in.putExtra("Holder_handover_date",mobileStolenSearchDetailsList.get(position).getDtHandedOver());
        in.putExtra("Holder_IMEINo2",mobileStolenSearchDetailsList.get(position).getImei2());
        in.putExtra("Holder_MobileNo2",mobileStolenSearchDetailsList.get(position).getMobileNo2());
        in.putExtra("Holder_ServiceProvider2",mobileStolenSearchDetailsList.get(position).getSprovider2());
        in.putExtra("Holder_ESN",mobileStolenSearchDetailsList.get(position).getEsn());
        in.putExtra("Holder_ESN2",mobileStolenSearchDetailsList.get(position).getEsn2());

        startActivity(in);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }

    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
