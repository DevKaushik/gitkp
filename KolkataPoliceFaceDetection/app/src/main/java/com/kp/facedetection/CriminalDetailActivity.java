package com.kp.facedetection;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.kp.facedetection.adapter.ImageUpdateAdapter;
import com.kp.facedetection.adapter.SampleFragmentPagerAdapter;
import com.kp.facedetection.fragments.CriminalFIRDetailsFragment;
import com.kp.facedetection.interfaces.OnCameraListener;
import com.kp.facedetection.model.CriminalDetails;
import com.kp.facedetection.singletone_classes.CommunicationViews;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class CriminalDetailActivity extends BaseActivity implements OnCameraListener, View.OnClickListener, Observer {

    private ImageView iv_image, iv_captureImage, iv_captureImageNew;
    private TextView tv_name, tv_age, tv_location, tv_alias,
            tv_detail;
    CriminalDetails criminalRecord;
   // ImageLoader imageLoader;
    //ImageFragmentPagerAdapter imageFragmentPagerAdapter;
   ImageUpdateAdapter imageFragmentPagerAdapter;

    private static String userName="";
    private String loginNumber="";
    private String user_id= "";
    private String appVersion = "";

    private TextView tv_name_label;
    private TextView tv_caseName_label;
    private TextView tv_caseName;
    private TextView tv_caseNo;
    private TextView tv_PS;
    private String searchCase="";
    private int list_position;

    private RelativeLayout rl_name;
    private RelativeLayout rl_age;
    private RelativeLayout rl_address;
    private RelativeLayout rl_alias;

    private RelativeLayout rl_caseName;
    private RelativeLayout rl_caseNo;
    private RelativeLayout rl_PS;

    private RelativeLayout relative_imageSlider;

    private LinearLayout linear_details;
    private RelativeLayout relative_tabs;
    private RelativeLayout relative_header_without_image;

    private PagerSlidingTabStrip tabsStrip;
    private ViewPager viewPager;
    private ViewPager imagePager;
    private String trnid="";
    private String ps_code="";
    private String wa_year="";
    private String wasl_no="";
    private String crime_no="";
    private String crime_year="";
    private String case_no="";
    private String case_year="";
    private String sl_no="";
    private String criminal_flag = "";
    private String alias_name="";
    private String father_name="";
    private String age="";
    private String sex="";
    private String nationality="";
    private String religion="";
    private String crs_general_id="";
    private String criminal_name="";
    private String prov_crm_no = "";

    private ImageView iv_left_arrow;
    private ImageView iv_right_arrow;
    private TextView tv_watermark, tv_watermark2,tv_watermark3;
    private int current_page = 0;

    private List<String> criminal_imageList = new ArrayList<String>();

    private String capturedImgFileName = "";
    private Bitmap userImageBitmap = null;
    private String path = "";

    private String[] keys;
    private String[] values;

    AlertDialog.Builder alertDialog;

    private String uploadUrl = "";
    private LinearLayout linear_loadCriminalDetails;
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.criminal_details_layout2);
        ObservableObject.getInstance().addObserver(this);
        Log.e("LOG","hi");
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
            user_id = Utility.getUserInfo(this).getUserId();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome "+userName);
        tv_loginCount.setText("Login Count: "+loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        searchCase = getIntent().getStringExtra("searchCase");
        list_position = getIntent().getIntExtra("List_Position",0);
        Log.e("TAG:","Criminal- listPosition: "+list_position);
        Log.e("Search Case:-----" , searchCase);
        System.out.println("Search Case:------------- " + searchCase);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        initView();
    }



    private void initView() {
        iv_image = (ImageView) findViewById(R.id.iv_image);
        iv_captureImage = (ImageView) findViewById(R.id.iv_captureImage);
        iv_captureImageNew = (ImageView) findViewById(R.id.iv_captureImageNew);

        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_age = (TextView) findViewById(R.id.tv_age);
        tv_location = (TextView) findViewById(R.id.tv_location);
        tv_alias = (TextView) findViewById(R.id.tv_alias);

        tv_name_label =  (TextView) findViewById(R.id.tv_name_label);
        tv_detail = (TextView) findViewById(R.id.tv_detail);
        tv_caseName_label = (TextView) findViewById(R.id.tv_caseName_label);
        tv_caseName = (TextView) findViewById(R.id.tv_caseName);
        tv_caseNo =  (TextView) findViewById(R.id.tv_caseNo);
        tv_PS = (TextView) findViewById(R.id.tv_PS);

        rl_name = (RelativeLayout)findViewById(R.id.rl_name);
        rl_age = (RelativeLayout)findViewById(R.id.rl_age);
        rl_address = (RelativeLayout)findViewById(R.id.rl_address);
        rl_alias = (RelativeLayout)findViewById(R.id.rl_address);

        rl_caseName = (RelativeLayout)findViewById(R.id.rl_caseName);
        rl_caseNo = (RelativeLayout)findViewById(R.id.rl_caseNo);
        rl_PS = (RelativeLayout)findViewById(R.id.rl_PS);

        relative_imageSlider = (RelativeLayout)findViewById(R.id.relative_imageSlider);
        relative_header_without_image = (RelativeLayout)findViewById(R.id.relative_header_without_image);

        linear_details = (LinearLayout)findViewById(R.id.linear_details);
        relative_tabs =  (RelativeLayout)findViewById(R.id.relative_tabs);

        imagePager = (ViewPager) findViewById(R.id.pager);

        iv_left_arrow = (ImageView)findViewById(R.id.iv_left_arrow);
        iv_right_arrow = (ImageView)findViewById(R.id.iv_right_arrow);

        tv_watermark = (TextView)findViewById(R.id.tv_watermark);
        tv_watermark2 = (TextView)findViewById(R.id.tv_watermark2);
        tv_watermark3 = (TextView)findViewById(R.id.tv_watermark3);

        iv_image.setVisibility(View.GONE);
        relative_imageSlider.setVisibility(View.GONE);
        tv_watermark.setVisibility(View.VISIBLE);

        tv_watermark.setText(userName);
        tv_watermark.setRotation(-45);


        if(searchCase.equalsIgnoreCase("caseSearch")){
            linear_details.setVisibility(View.VISIBLE);
            relative_tabs.setVisibility(View.GONE);
        }
        else {
            linear_details.setVisibility(View.GONE);
            relative_tabs.setVisibility(View.VISIBLE);
        }

        //imageLoader = new ImageLoader(this);
        criminalRecord = (CriminalDetails) getIntent().getSerializableExtra(
                Constants.OBJECT);


        setUpView();

    }

    private void setUpView() {


        clickEvents();

        /* Add custom fonts to text view */

        Constants.changefonts(tv_name, this, "Calibri.ttf");
        Constants.changefonts(tv_age, this, "Calibri.ttf");
        Constants.changefonts(tv_location, this, "Calibri.ttf");
        Constants.changefonts(tv_alias, this, "Calibri.ttf");
        Constants.changefonts(tv_name_label, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_caseName_label, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_caseName, this, "Calibri.ttf");
        Constants.changefonts(tv_caseNo, this, "Calibri.ttf");
        Constants.changefonts(tv_PS, this, "Calibri.ttf");


        if(searchCase.equalsIgnoreCase("caseSearch")){
           // iv_image.setVisibility(View.GONE);

            rl_name.setVisibility(View.GONE);
            rl_age.setVisibility(View.GONE);
            rl_address.setVisibility(View.GONE);
            rl_alias.setVisibility(View.GONE);

            rl_caseName.setVisibility(View.VISIBLE);
            rl_caseNo.setVisibility(View.VISIBLE);
            rl_PS.setVisibility(View.VISIBLE);
            L.e("NAME:"+criminalRecord.getNameAccused());
            tv_caseName.setText(criminalRecord.getNameAccused().replace("NAME_ACCUSED: ", ""));


            if (criminalRecord.getCaseNo() != null
                    && !criminalRecord.getCaseNo().equals("")){

                tv_caseNo.setText(criminalRecord.getCaseNo().replace("CASENO: ",""));

            }

            if (criminalRecord.getPs() != null
                    && !criminalRecord.getPs().equals("")){

                tv_PS.setText(criminalRecord.getPs().replace("PS: ",""));
            }

        }
        else{

            //iv_image.setVisibility(View.VISIBLE);

            rl_name.setVisibility(View.VISIBLE);
          //  rl_age.setVisibility(View.VISIBLE);
            rl_age.setVisibility(View.GONE);
            rl_address.setVisibility(View.VISIBLE);
            rl_alias.setVisibility(View.VISIBLE);

            rl_caseName.setVisibility(View.GONE);
            rl_caseNo.setVisibility(View.GONE);
            rl_PS.setVisibility(View.GONE);


           /* iv_image.setImageDrawable(null);
            imageLoader.DisplayImage(
                    Constants.IMAGE_BASE_URL + criminalRecord.getPictureUrl(),
                    iv_image);*/

            Log.e("Image Array Size:-----" , String.valueOf(criminalRecord.getPicture_list().size()));
            if(criminalRecord.getPicture_list().size()>0){

                /*imageLoader.DisplayImage(
                        criminalRecord.getPicture_list().get(0),
                        iv_image);*/

                iv_left_arrow.setVisibility(View.INVISIBLE);
                iv_image.setVisibility(View.GONE);
                relative_imageSlider.setVisibility(View.VISIBLE);
                relative_header_without_image.setVisibility(View.GONE);

                tv_watermark2.setVisibility(View.VISIBLE);
                tv_watermark2.setText(userName);
                tv_watermark2.setRotation(-25);

                criminal_imageList = criminalRecord.getPicture_list();

                if(criminal_imageList.size()>1){
                    iv_right_arrow.setVisibility(View.VISIBLE);
                }
                else {
                    iv_right_arrow.setVisibility(View.INVISIBLE);
                }

                for(int i=0;i<criminalRecord.getPicture_list().size();i++){

                    System.out.println("Criminal Picture "+i+" "+criminalRecord.getPicture_list().get(i));
                }

                /*imageFragmentPagerAdapter = new ImageFragmentPagerAdapter(getSupportFragmentManager(),criminal_imageList);
                imagePager.setAdapter(imageFragmentPagerAdapter);*/
                imageFragmentPagerAdapter = new ImageUpdateAdapter(CriminalDetailActivity.this,criminal_imageList);
                imagePager.setAdapter(imageFragmentPagerAdapter);


            }
            else {

                relative_header_without_image.setVisibility(View.VISIBLE);
                iv_image.setVisibility(View.VISIBLE);
                relative_imageSlider.setVisibility(View.GONE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    iv_image.setImageDrawable(getResources().getDrawable(R.drawable.human3, getApplicationContext().getTheme()));
                } else {
                    iv_image.setImageDrawable(getResources().getDrawable(R.drawable.human3));
                }


                tv_watermark3.setVisibility(View.VISIBLE);
                tv_watermark3.setText(userName);
                tv_watermark3.setRotation(-25);

            }
            L.e("NAME:"+criminalRecord.getNameAccused());

            tv_name.setText(criminalRecord.getNameAccused().replace("NAME_ACCUSED: ", ""));
          //  tv_age.setText(criminalRecord.getAge().replace("AGE_ACCUSED: ", ""));
            tv_location.setText(criminalRecord.getAddressAccused().replace("ADDR_ACCUSED: ", ""));
            tv_alias.setText(criminalRecord.getAliasName().replace("ALIAS: ", ""));

        }


        String details = "";

        if (criminalRecord.getPs() != null
                && !criminalRecord.getPs().equals(""))
            details = details + "\n\n" + criminalRecord.getPs();
        if (criminalRecord.getCaseNo() != null
                && !criminalRecord.getCaseNo().equals(""))
            details = details + "\n\n" + criminalRecord.getCaseNo();
        if (criminalRecord.getSexAccused() != null
                && !criminalRecord.getSexAccused().equals(""))
            details = details + "\n\n" + criminalRecord.getSexAccused();
        if (criminalRecord.getFatherAccused() != null
                && !criminalRecord.getFatherAccused().equals(""))
            details = details + "\n\n" + criminalRecord.getFatherAccused();
        if (criminalRecord.getFrChgno() != null
                && !criminalRecord.getFrChgno().equals(""))
            details = details + "\n\n" + criminalRecord.getFrChgno();
        if (criminalRecord.getChgyear() != null
                && !criminalRecord.getChgyear().equals(""))
            details = details + "\n\n" + criminalRecord.getChgyear();
        if (criminalRecord.getNationality() != null
                && !criminalRecord.getNationality().equals(""))
            details = details + "\n\n" + criminalRecord.getNationality();
        if (criminalRecord.getReligion() != null
                && !criminalRecord.getReligion().equals(""))
            details = details + "\n\n" + criminalRecord.getReligion();
        if (criminalRecord.getScStObc() != null
                && !criminalRecord.getScStObc().equals(""))
            details = details + "\n\n" + criminalRecord.getScStObc();
        if (criminalRecord.getOccupation() != null
                && !criminalRecord.getOccupation().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getOccupation();
        if (criminalRecord.getProvCrmNo() != null
                && !criminalRecord.getProvCrmNo().equals(""))
            details = details + "\n\n" + criminalRecord.getProvCrmNo();
        if (criminalRecord.getRegCrmNo() != null
                && !criminalRecord.getRegCrmNo().equals(""))
            details = details + "\n\n" + criminalRecord.getRegCrmNo();
        if (criminalRecord.getDtArrest() != null
                && !criminalRecord.getDtArrest().equals(""))
            details = details + "\n\n" + criminalRecord.getDtArrest();
        if (criminalRecord.getDtReleaseBail() != null
                && !criminalRecord.getDtReleaseBail().equals(""))
            details = details + "\n\n" + criminalRecord.getDtReleaseBail();
        if (criminalRecord.getDtForwardCourt() != null
                && !criminalRecord.getDtForwardCourt().equals(""))
            details = details + "\n\n" + criminalRecord.getDtForwardCourt();
        if (criminalRecord.getActsSections() != null
                && !criminalRecord.getActsSections().equals(""))
            details = details + "\n\n" + criminalRecord.getActsSections();
        if (criminalRecord.getNamesSureties() != null
                && !criminalRecord.getNamesSureties().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getNamesSureties();
        if (criminalRecord.getAddressSureties() != null
                && !criminalRecord.getAddressSureties().equals(
                ""))
            details = details + "\n\n"
                    + criminalRecord.getAddressSureties();
        if (criminalRecord.getPrevConvcCaseref() != null
                && !criminalRecord.getPrevConvcCaseref().equals(""))
            details = details + "\n\n" + criminalRecord.getPrevConvcCaseref();
        if (criminalRecord.getStatus() != null
                && !criminalRecord.getStatus().equals(""))
            details = details + "\n\n" + criminalRecord.getStatus();

        if (criminalRecord.getSuspicionAppvd() != null
                && !criminalRecord.getSuspicionAppvd().equals(""))
            details = details + "\n\n" + criminalRecord.getSuspicionAppvd();
        if (criminalRecord.getCaseYr() != null
                && !criminalRecord.getCaseYr().equals(""))
            details = details + "\n\n" + criminalRecord.getCaseYr();
        if (criminalRecord.getSlno() != null
                && !criminalRecord.getSlno().equals(""))
            details = details + "\n\n" + criminalRecord.getSlno();

        if (criminalRecord.getFircat() != null
                && !criminalRecord.getFircat().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getFircat();
        if (criminalRecord.getFatherHusband() != null
                && !criminalRecord.getFatherHusband().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getFatherHusband();
        if (criminalRecord.getCrsgeneralid() != null
                && !criminalRecord.getCrsgeneralid().equals(""))
            details = details + "\n\n" + criminalRecord.getCrsgeneralid();

        if (criminalRecord.getFadd() != null
                && !criminalRecord.getFadd().equals(""))
            details = details + "\n\n" + criminalRecord.getFadd();
        if (criminalRecord.getFoccu() != null
                && !criminalRecord.getFoccu().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getFoccu();
        if (criminalRecord.getBirthyr() != null
                && !criminalRecord.getBirthyr().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getBirthyr();
        if (criminalRecord.getHeightFeet() != null
                && !criminalRecord.getHeightFeet().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getHeightFeet();
        if (criminalRecord.getHeightInch() != null
                && !criminalRecord.getHeightInch().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getHeightInch();
        if (criminalRecord.getPsRoadPsplace() != null
                && !criminalRecord.getPsRoadPsplace().equals(""))
            details = details + "\n\n" + criminalRecord.getPsRoadPsplace();
        if (criminalRecord.getPspsKpj() != null
                && !criminalRecord.getPspsKpj().equals(""))
            details = details + "\n\n" + criminalRecord.getPspsKpj();
        if (criminalRecord.getPspsOut() != null
                && !criminalRecord.getPspsOut().equals(""))
            details = details + "\n\n" + criminalRecord.getPspsOut();
        if (criminalRecord.getPsdist() != null
                && !criminalRecord.getPsdist().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getPsdist();
        if (criminalRecord.getPsdistOut() != null
                && !criminalRecord.getPsdistOut().equals(""))
            details = details + "\n\n" + criminalRecord.getPsdistOut();
        if (criminalRecord.getPspin() != null
                && !criminalRecord.getPspin().equals(""))
            details = details + "\n\n" + criminalRecord.getPspin();
        if (criminalRecord.getPsstate() != null
                && !criminalRecord.getPsstate().equals(""))
            details = details + "\n\n" + criminalRecord.getPsstate();
        if (criminalRecord.getPrroadPreplace() != null
                && !criminalRecord.getPrroadPreplace().equals(""))
            details = details + "\n\n" + criminalRecord.getPrroadPreplace();
        if (criminalRecord.getPrpsKpj() != null
                && !criminalRecord.getPrpsKpj().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getPrpsKpj();
        if (criminalRecord.getPrpsOut() != null
                && !criminalRecord.getPrpsOut().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getPrpsOut();
        if (criminalRecord.getPrdist() != null
                && !criminalRecord.getPrdist().equals(""))
            details = details + "\n\n" + criminalRecord.getPrdist();
        if (criminalRecord.getPrdistOut() != null
                && !criminalRecord.getPrdistOut().equals(""))
            details = details + "\n\n" + criminalRecord.getPrdistOut();
        if (criminalRecord.getPrpin() != null
                && !criminalRecord.getPrpin().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getPrpin();
        if (criminalRecord.getPrstate() != null
                && !criminalRecord.getPrstate().equals(""))
            details = details + "\n\n" + criminalRecord.getPrstate();
        if (criminalRecord.getClassa() != null
                && !criminalRecord.getClassa().equals(""))
            details = details + "\n\n" + criminalRecord.getClassa();
        if (criminalRecord.getSubclass() != null
                && !criminalRecord.getSubclass().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getSubclass();
        if (criminalRecord.getProperty() != null
                && !criminalRecord.getProperty().equals(""))
            details = details + "\n\n" + criminalRecord.getProperty();
        if (criminalRecord.getTransport() != null
                && !criminalRecord.getTransport().equals(""))
            details = details + "\n\n" + criminalRecord.getTransport();
        if (criminalRecord.getGround() != null
                && !criminalRecord.getGround().equals(""))
            details = details + "\n\n" + criminalRecord.getGround();
        if (criminalRecord.getHsno() != null
                && !criminalRecord.getHsno().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getHsno();
        if (criminalRecord.getProddate() != null
                && !criminalRecord.getProddate().equals(""))
            details = details + "\n\n" + criminalRecord.getProddate();
        if (criminalRecord.getBeard() != null
                && !criminalRecord.getBeard().equals(""))
            details = details + "\n\n" + criminalRecord.getBeard();
        if (criminalRecord.getBirthmark() != null
                && !criminalRecord.getBirthmark().equals(""))
            details = details + "\n\n" + criminalRecord.getBirthmark();
        if (criminalRecord.getBuilt() != null
                && !criminalRecord.getBuilt().equals(""))
            details = details + "\n\n" + criminalRecord.getBuilt();
        if (criminalRecord.getBurnmark() != null
                && !criminalRecord.getBurnmark().equals(""))
            details = details + "\n\n"
                    + criminalRecord.getBurnmark();
        if (criminalRecord.getTtcriminallid() != null
                && !criminalRecord.getTtcriminallid().equals(""))
            details = details + "\n\n" + criminalRecord.getTtcriminallid();
        if (criminalRecord.getPhotono() != null
                && !criminalRecord.getPhotono().equals(""))
            details = details + "\n\n" + criminalRecord.getPhotono();
        if (criminalRecord.getRoughpart() != null
                && !criminalRecord.getRoughpart().equals(""))
            details = details + "\n\n" + criminalRecord.getRoughpart();
        if (criminalRecord.getRoughsec() != null
                && !criminalRecord.getRoughsec().equals(""))
            details = details + "\n\n" + criminalRecord.getRoughsec();
        if (criminalRecord.getIdentifier() != null
                && !criminalRecord.getIdentifier().equals(""))
            details = details + "\n\n" + criminalRecord.getIdentifier();
        if (criminalRecord.getTarget() != null
                && !criminalRecord.getTarget().equals(""))
            details = details + "\n\n" + criminalRecord.getTarget();
        if (criminalRecord.getOccurno() != null
                && !criminalRecord.getOccurno().equals(""))
            details = details + "\n\n" + criminalRecord.getOccurno();
        if (criminalRecord.getCrimeno() != null
                && !criminalRecord.getCrimeno().equals(""))
            details = details + "\n\n" + criminalRecord.getCrimeno();
        if (criminalRecord.getDivision() != null
                && !criminalRecord.getDivision().equals(""))
            details = details + "\n\n" + criminalRecord.getDivision();
        if (criminalRecord.getWartMark() != null
                && !criminalRecord.getWartMark().equals(""))
            details = details + "\n\n" + criminalRecord.getWartMark();
        if (criminalRecord.getTattoomark() != null
                && !criminalRecord.getTattoomark().equals(""))
            details = details + "\n\n" + criminalRecord.getTattoomark();
        if (criminalRecord.getScarmark() != null
                && !criminalRecord.getScarmark().equals(""))
            details = details + "\n\n" + criminalRecord.getScarmark();
        if (criminalRecord.getNose() != null
                && !criminalRecord.getNose().equals(""))
            details = details + "\n\n" + criminalRecord.getNose();
        if (criminalRecord.getMoustache() != null
                && !criminalRecord.getMoustache().equals(""))
            details = details + "\n\n" + criminalRecord.getMoustache();
        if (criminalRecord.getMole() != null
                && !criminalRecord.getMole().equals(""))
            details = details + "\n\n" + criminalRecord.getMole();
        if (criminalRecord.getHair() != null
                && !criminalRecord.getHair().equals(""))
            details = details + "\n\n" + criminalRecord.getHair();
        if (criminalRecord.getForehead() != null
                && !criminalRecord.getForehead().equals(""))
            details = details + "\n\n" + criminalRecord.getForehead();
        if (criminalRecord.getFace() != null
                && !criminalRecord.getFace().equals(""))
            details = details + "\n\n" + criminalRecord.getFace();
        if (criminalRecord.getEyebrow() != null
                && !criminalRecord.getEyebrow().equals(""))
            details = details + "\n\n" + criminalRecord.getEyebrow();
        if (criminalRecord.getEye() != null
                && !criminalRecord.getEye().equals(""))
            details = details + "\n\n" + criminalRecord.getEye();
        if (criminalRecord.getEar() != null
                && !criminalRecord.getEar().equals(""))
            details = details + "\n\n" + criminalRecord.getEar();
        if (criminalRecord.getDeformity() != null
                && !criminalRecord.getDeformity().equals(""))
            details = details + "\n\n" + criminalRecord.getDeformity();
        if (criminalRecord.getCutmark() != null
                && !criminalRecord.getCutmark().equals(""))
            details = details + "\n\n" + criminalRecord.getCutmark();
        if (criminalRecord.getComplexion() != null
                && !criminalRecord.getComplexion().equals(""))
            details = details + "\n\n" + criminalRecord.getComplexion();
        if (criminalRecord.getTrnid() != null
                && !criminalRecord.getTrnid().equals(""))
            details = details + "\n\n" + criminalRecord.getTrnid();
        if (criminalRecord.getPs_code() != null
                && !criminalRecord.getPs_code().equals(""))
            details = details + "\n\n" + criminalRecord.getPs_code();
        if (criminalRecord.getWa_year() != null
                && !criminalRecord.getWa_year().equals(""))
            details = details + "\n\n" + criminalRecord.getWa_year();
        if (criminalRecord.getWaslno() != null
                && !criminalRecord.getWaslno().equals(""))
            details = details + "\n\n" + criminalRecord.getWaslno();
        if (criminalRecord.getCrime_no() != null
                && !criminalRecord.getCrime_no().equals(""))
            details = details + "\n\n" + criminalRecord.getCrime_no();
        if (criminalRecord.getCrime_year() != null
                && !criminalRecord.getCrime_year().equals(""))
            details = details + "\n\n" + criminalRecord.getCrime_year();
        if (criminalRecord.getCase_no() != null
                && !criminalRecord.getCase_no().equals(""))
            details = details + "\n\n" + criminalRecord.getCase_no();
        if (criminalRecord.getCase_year() != null
                && !criminalRecord.getCase_year().equals(""))
            details = details + "\n\n" + criminalRecord.getCase_year();
        if (criminalRecord.getSl_no() != null
                && !criminalRecord.getSl_no().equals(""))
            details = details + "\n\n" + criminalRecord.getSl_no();

        tv_detail.setText(details.trim());

        System.out.println("Details: " + details.trim());

        if (criminalRecord.getTrnid() != null
                && !criminalRecord.getTrnid().equals("")){

            trnid = criminalRecord.getTrnid();
            L.e("trnid"+trnid);
        }

        if (criminalRecord.getPs_code() != null
                && !criminalRecord.getPs_code().equals("")){

            ps_code = criminalRecord.getPs_code();
            L.e("ps_code"+ps_code);


        }

        if (criminalRecord.getWa_year() != null
                && !criminalRecord.getWa_year().equals("")){

            wa_year = criminalRecord.getWa_year();
            L.e("wa_year"+wa_year);


        }

        if (criminalRecord.getWaslno() != null
                && !criminalRecord.getWaslno().equals("")){

            wasl_no = criminalRecord.getWaslno();
            L.e("wasl_no"+wasl_no);

        }

        if (criminalRecord.getCrime_no() != null
                && !criminalRecord.getCrime_no().equals("")){

            crime_no = criminalRecord.getCrime_no();
            L.e("crime_no"+crime_no);


        }

        if (criminalRecord.getCrime_year() != null
                && !criminalRecord.getCrime_year().equals("")){

            crime_year = criminalRecord.getCrime_year();
            L.e("crime_year"+crime_year);

        }

       /* if (!criminalRecord.getProv_crm_no_for_image().equalsIgnoreCase("") &&
                !criminalRecord.getProv_crm_no_for_image().equalsIgnoreCase("null")){

            prov_crm_no = criminalRecord.getProv_crm_no_for_image();
            L.e("prov_crm_no"+prov_crm_no);


        }*/
        if (!criminalRecord.getProvCrmNo().equalsIgnoreCase("") &&
                !criminalRecord.getProvCrmNo().equalsIgnoreCase("null")){
            if(criminalRecord.getProvCrmNo().contains("PROV_CRM_NO: ")) {

                prov_crm_no = criminalRecord.getProvCrmNo().replace("PROV_CRM_NO: ","").trim();
            }
            else {

                prov_crm_no = criminalRecord.getProvCrmNo().trim();
            }
            L.e("prov_crm_no criminalDetails--------"+prov_crm_no);


        }

        if (criminalRecord.getCase_no() != null
                && !criminalRecord.getCase_no().equals("")){

            case_no = criminalRecord.getCase_no();
            L.e("case_no"+case_no);


        }

        if (criminalRecord.getCase_year() != null
                && !criminalRecord.getCase_year().equals("")){

            case_year = criminalRecord.getCase_year();
            L.e("case_year"+case_year);


        }

        if (criminalRecord.getSl_no() != null
                && !criminalRecord.getSl_no().equals("")){

            sl_no = criminalRecord.getSl_no();
            L.e("sl_no"+sl_no);


        }

        if (criminalRecord.getFlag() != null
                && !criminalRecord.getFlag().equals("")){

            criminal_flag = criminalRecord.getFlag().replace("FLAG: ", "");
            L.e("criminal_flag"+criminal_flag);


        }

        if (criminalRecord.getAliasName() != null
                && !criminalRecord.getAliasName().equals("")){

            alias_name = criminalRecord.getAliasName();
            L.e("alias_name"+criminal_flag);


        }

        if (criminalRecord.getFatherName() != null
                && !criminalRecord.getFatherName().equals("")){

            father_name = criminalRecord.getFatherName();
            L.e("father_name"+father_name);

        }

        if (criminalRecord.getDob_age() != null
                && !criminalRecord.getDob_age().equals("")){

            age = criminalRecord.getDob_age();
            L.e("age"+age);


        }

        if (criminalRecord.getSex() != null
                && !criminalRecord.getSex().equals("")){

            sex = criminalRecord.getSex();
            L.e("sex"+sex);


        }

        if (criminalRecord.getNationality() != null
                && !criminalRecord.getNationality().equals("")){

            nationality = criminalRecord.getNationality();
            L.e("nationality"+nationality);


        }

        if (criminalRecord.getReligion() != null
                && !criminalRecord.getReligion().equals("")){

            religion = criminalRecord.getReligion();
            L.e("religion"+religion);


        }

        if (criminalRecord.getCrs_generalID() != null
                && !criminalRecord.getCrs_generalID().equals("")){

            crs_general_id = criminalRecord.getCrs_generalID();
            L.e("crs_general_id"+crs_general_id);



        }

        if (criminalRecord.getNameAccused() != null
                && !criminalRecord.getNameAccused().equals("")){

            criminal_name = criminalRecord.getNameAccused().replace("NAME_ACCUSED: ", "");
            L.e("criminal_name"+criminal_name);

        }


        // camera icon visible after checking PROV_CRM_NO null
        if(!criminalRecord.getProv_crm_no_for_image().equalsIgnoreCase("") && !criminalRecord.getProv_crm_no_for_image().equalsIgnoreCase("null")){
            iv_captureImage.setVisibility(View.VISIBLE);
            iv_captureImageNew.setVisibility(View.VISIBLE);
        }
        else{
            iv_captureImage.setVisibility(View.INVISIBLE);
            iv_captureImageNew.setVisibility(View.INVISIBLE);
        }


        // Get the ViewPager and set it's PagerAdapter so that it can display items
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager(),criminal_flag,trnid,ps_code,wa_year,wasl_no,crime_no,crime_year,case_no,case_year,sl_no,alias_name,father_name,age,sex,nationality,religion,crs_general_id,criminal_name, prov_crm_no));

        // Give the PagerSlidingTabStrip the ViewPager
        tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);

        tabsStrip.setDividerColorResource(R.color.colorWhite);
        tabsStrip.setIndicatorHeight(10);
        tabsStrip.setTextColorResource(R.color.colorWhite);
        tabsStrip.setIndicatorColorResource(R.color.colorWhite);
      //  tabsStrip.setTextSize(40);

        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(viewPager);

        // Attach the page change listener to tab strip and **not** the view pager inside the activity
        tabsStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // This method will be invoked when a new page becomes selected.
            @Override
            public void onPageSelected(int position) {


            }

            // This method will be invoked when the current page is scrolled
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Code goes here
            }

            // Called when the scroll state changes:
            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
           /* case R.id.menu_send_push:
                //tabHost.setCurrentTab(2);
                //startActivity(new Intent(this, SendPushActivity.class));
                //	pushFragments("Push", new SendPushActivity(), true, false);
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this,Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                // Utility.logout(this);
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        ImageLoader.getInstance().destroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);

    }

    private void clickEvents(){

        iv_captureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                        /*
                        *  abstract method of interface call; before call need to set the listener;
                        *  @param context.this -> if within any view
                        *  @param this  -> if within onCreate()
                        * */

                setCameraListener(CriminalDetailActivity.this);
                onCallCameraButton();

            }
        });


        iv_captureImageNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCameraListener(CriminalDetailActivity.this);
                onCallCameraButton();
            }
        });



        iv_left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagePager.setCurrentItem(--current_page,true);
            }
        });

        iv_right_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imagePager.setCurrentItem(++current_page, true);
            }
        });

        imagePager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                current_page = position;

                if (position == 0) {
                    iv_left_arrow.setVisibility(View.INVISIBLE);
                } else {
                    iv_left_arrow.setVisibility(View.VISIBLE);
                }

                if (position == criminal_imageList.size() - 1) {
                    iv_right_arrow.setVisibility(View.INVISIBLE);
                } else {
                    iv_right_arrow.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void onBitmapReceivedFromCamera(Bitmap mBitmap, String mPath) {
        userImageBitmap = mBitmap;
        path = mPath;
        iv_image.setImageBitmap(mBitmap);
        showAlertForSaveImage();

    }

    @Override
    public void onBitmapReceivedFromGallery(Bitmap mBitmap, String mPath) {
        userImageBitmap = mBitmap;
        path = mPath;
        iv_image.setImageBitmap(mBitmap);
        showAlertForSaveImage();
    }



    public void showAlertForSaveImage() {

        alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        // alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setTitle("Alert!");
        alertDialog.setMessage("Do you want to save image?");
        alertDialog.setPositiveButton("SAVE", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                imageSaveToServer();

            }
        });

        alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        alertDialog.create().show();
    }


    private void imageSaveToServer(){

        Log.e("Image Path ++1 :",path);

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_PIC_UPDATE);
        taskManager.setCriminalPicUpdate(true);
        keys = new String[]{"prov_cr_no", "user_id", "pic"};
        values = new String[]{prov_crm_no.trim(), user_id.trim(), path.trim()};
        taskManager.doStartTask(keys, values, false, true, path, userImageBitmap);
    }


    public void parseCriminalPicUpdateResponse(String response){

        Log.e("TAG:","parseCriminalPicUpdateResponse- Response"+ response);
        JSONObject jobj = null;
        try{
            jobj = new JSONObject(response);
            if(jobj != null && jobj.optString("status").equalsIgnoreCase("1")){

                JSONArray resultArray = jobj.getJSONArray("result");

                uploadUrl = resultArray.get(resultArray.length() - 1).toString();


                if(criminal_imageList.size() > 0) {

                    Log.e("TAG:","UPLOAD URL new Image add: "+uploadUrl);
                    criminal_imageList.add(uploadUrl);
                    imageFragmentPagerAdapter.notifyDataSetChanged();
                }
                else{
                    Log.e("TAG:","UPLOAD URL 1st Image add: "+uploadUrl);
                    criminalRecord.getPicture_list().add(uploadUrl);
                    criminal_imageList.add(uploadUrl);
                }

                if(!uploadUrl.equalsIgnoreCase("")){
                    Utility.showAlertDialog(this,"Result !!!","Image uploaded successfully", true);
                    CommunicationViews.getInstance().uploadImage(list_position, uploadUrl);
                }
                else{
                    Utility.showAlertDialog(this,"Search Error !!!","Please capture image again", false);
                }

            }
            else{
                Utility.showAlertDialog(this,"Search Error !!!","Please try again.", false);
            }
        }
        catch(JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {

        Fragment frag = getSupportFragmentManager().findFragmentByTag(CriminalFIRDetailsFragment.TAG);
        if(frag != null){
            getSupportFragmentManager().beginTransaction().remove(frag).commit();
            linear_loadCriminalDetails = (LinearLayout) findViewById(R.id.linear_loadCriminalDetails);
            linear_loadCriminalDetails.setVisibility(View.GONE);

        }else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
