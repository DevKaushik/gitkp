package com.kp.facedetection.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 23-05-2018.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "request_id",
        "request_details_id",
        "submit_datetime",
        "request_type",
        "submit_by",
        "period"
})
public class ChkNoAppList implements Serializable{
    @JsonProperty("request_id")
    private String requestId;
    @JsonProperty("request_details_id")
    private String requestDetailsId;
    @JsonProperty("submit_datetime")
    private String submitDatetime;
    @JsonProperty("request_type")
    private String requestType;
    @JsonProperty("submit_by")
    private String submitBy;
    @JsonProperty("period")
    private String period;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("request_id")
    public String getRequestId() {
        return requestId;
    }

    @JsonProperty("request_id")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @JsonProperty("request_details_id")
    public String getRequestDetailsId() {
        return requestDetailsId;
    }

    @JsonProperty("request_details_id")
    public void setRequestDetailsId(String requestDetailsId) {
        this.requestDetailsId = requestDetailsId;
    }

    @JsonProperty("submit_datetime")
    public String getSubmitDatetime() {
        return submitDatetime;
    }

    @JsonProperty("submit_datetime")
    public void setSubmitDatetime(String submitDatetime) {
        this.submitDatetime = submitDatetime;
    }

    @JsonProperty("request_type")
    public String getRequestType() {
        return requestType;
    }

    @JsonProperty("request_type")
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    @JsonProperty("submit_by")
    public String getSubmitBy() {
        return submitBy;
    }

    @JsonProperty("submit_by")
    public void setSubmitBy(String submitBy) {
        this.submitBy = submitBy;
    }

    @JsonProperty("period")
    public String getPeriod() {
        return period;
    }

    @JsonProperty("period")
    public void setPeriod(String period) {
        this.period = period;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
