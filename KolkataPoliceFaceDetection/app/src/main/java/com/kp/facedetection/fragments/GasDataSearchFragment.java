package com.kp.facedetection.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.kp.facedetection.DocumentSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.model.DocumentGasDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-Asset-131 on 10-09-2016.
 */
public class GasDataSearchFragment extends Fragment {

    private Spinner sp_choose_search_field;
    private EditText et_toDateValue;
    private Button btn_Search, btn_reset;

    private List<DocumentGasDetails> documentGasDetailsList;
    protected ArrayList<CharSequence> documentGasDetailsListName = new ArrayList<CharSequence>();
    private ArrayAdapter<CharSequence> documentGasAdapter;
    private String alias = "";

    public static GasDataSearchFragment newInstance() {

        GasDataSearchFragment gasDataSearchFragment = new GasDataSearchFragment();
        return gasDataSearchFragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gas_data_search, container, false);

        // Inflate the layout for this fragment

        return rootView;
    }

    private void clickEvents() {
        btn_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //fetchSearchResult();
                String dataValue = et_toDateValue.getText().toString().trim();

                if (!dataValue.equalsIgnoreCase("") && !alias.equalsIgnoreCase("")) {

                    showAlertForProceed();
                }
                else{

                    showAlertDialog(getActivity(), " Search Error!!! ", "Please provide both values for search", false);

                }
            }
        });
        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_toDateValue.setText("");
                sp_choose_search_field.setSelection(0);


            }
        });
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        btn_Search = (Button) view.findViewById(R.id.btn_Search);
        btn_reset = (Button) view.findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_Search);
        Constants.buttonEffect(btn_reset);

        sp_choose_search_field = (Spinner) view.findViewById(R.id.sp_choose_search_field);
        et_toDateValue = (EditText) view.findViewById(R.id.et_fieldValue);

        sp_choose_search_field.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                alias = documentGasDetailsList.get(position).getGasAlias();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        getGasSearchContents();
        clickEvents();
    }

    private void getGasSearchContents() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_GET_GAS_SEARCH_CONTENT);
        taskManager.setGasContent(true);

        String[] keys = {};
        String[] values = {};
        taskManager.doStartTask(keys, values, true, true);

    }

    public void parseGetGasContentResponse(String response) {

        //System.out.println("Get Gas Content Result: " + response);

        if (response != null && !response.equals("")) {

            documentGasDetailsList = new ArrayList<DocumentGasDetails>();
            documentGasDetailsList.clear();
            documentGasDetailsListName = new ArrayList<CharSequence>();
            documentGasDetailsListName.clear();

            JSONObject jObj = null;
            try {
                jObj = new JSONObject(response);

                if (jObj != null && jObj.optString("status").equalsIgnoreCase("1")) {

                    JSONArray result = jObj.getJSONArray("result");

                    for (int i = 0; i < result.length(); i++) {

                        DocumentGasDetails documentGasDetails = new DocumentGasDetails();

                        JSONObject obj = result.getJSONObject(i);

                        documentGasDetails.setGasId(obj.optString("id"));
                        documentGasDetails.setGasAlias(obj.optString("field_alias"));
                        documentGasDetails.setGasName(obj.optString("field_name"));

                        documentGasDetailsList.add(documentGasDetails);
                        documentGasDetailsListName.add(obj.optString("field_name"));


                    }
                    documentGasAdapter = new ArrayAdapter<CharSequence>(getActivity(), R.layout.simple_spinner_item, documentGasDetailsListName);
                    documentGasAdapter
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    sp_choose_search_field.setAdapter(documentGasAdapter);
                    documentGasAdapter.notifyDataSetChanged();

                } else {
                    Utility.showToast(getActivity(), jObj.optString("message"), "long");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void showAlertForProceed() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setTitle("Alert!");
        alertDialog.setMessage("It will take considerable time, proceed");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                fetchSearchResult();
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        alertDialog.show();
    }

    private void fetchSearchResult() {

        String dataValue = et_toDateValue.getText().toString().trim();

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_GAS_SEARCH);
        taskManager.setGasSearch(true);

        String[] keys = {"data_field", "data_value"};

        String[] values = {alias.trim(), dataValue.trim()};

        taskManager.doStartTask(keys, values, true, true);

        //NetworkOperation();


    }

    /**
     * Function to display simple Alert Dialog
     *
     * @param context - application context
     * @param title   - alert dialog title
     * @param message - alert message
     * @param status  - success/failure (used to set icon)
     */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    public void parseGasSearch(String jsonStr) {

        System.out.println("Gas Search Result: " + jsonStr);

        try {

            JSONObject jObj = new JSONObject(jsonStr);

            if (jObj.opt("status").toString().equalsIgnoreCase("0")) {
                if (!jObj.optString("message").toString().equalsIgnoreCase("Success"))
                    Utility.showToast(getActivity(), jObj.optString("message"), "long");
            } else {

                Constants.documentSearchDLResponse = jsonStr;

                System.out.println("Response 1" + Constants.documentSearchDLResponse);

                Intent i = new Intent(getActivity(), DocumentSearchResultActivity.class);
                // i.putExtra("htmlresponse", html_response);
                startActivity(i);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
