package com.kp.facedetection.asynctasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.kp.facedetection.interfaces.OnPDFDownload;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Kaushik on 20-12-2016.
 */
public class AsynctaskForDownloadPDF extends AsyncTask<Void, String, Void> {

    private ProgressDialog dialog;
    private Context context;
    private String pdf_url;
    private boolean forWhich;

    OnPDFDownload delegate;

    private String pdf_file_name;

    public void setDelegate(OnPDFDownload delegate) {
        this.delegate = delegate;
    }



    public AsynctaskForDownloadPDF(Context context, String pdf_url, boolean forWhich){

        this.context = context;
        this.pdf_url = pdf_url;
        this.forWhich = forWhich;
        dialog = new ProgressDialog(context);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        try {
            dialog.setMessage("Downloading file. Please wait...");
            dialog.setIndeterminate(false);
            dialog.setMax(100);
            dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialog.setCancelable(false);
            dialog.show();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }


    @Override
    protected Void doInBackground(Void... params) {

        try {
            URL url = new URL(pdf_url);
            Log.e("PDF URL", pdf_url);

            pdf_file_name = pdf_url.substring(pdf_url.lastIndexOf("/"));

            URLConnection connection = url.openConnection();
            connection.connect();

            int lengthOfFile = connection.getContentLength();

            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream
            OutputStream output = new FileOutputStream(getPDFFilePath(pdf_file_name));

            byte data[] = new byte[1024];

            long total = 0;
            int count;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                publishProgress(""+(int)((total*100)/lengthOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
            delegate.onPDFDownloadError(e.toString());
            dismissDialog();
        } catch (IOException e) {
            e.printStackTrace();
            delegate.onPDFDownloadError(e.toString());
            dismissDialog();
        }


        return null;
    }

    /**
     * Updating progress bar
     * */
    @Override
    protected void onProgressUpdate(String... progress) {
        // setting progress percentage
        dialog.setProgress(Integer.parseInt(progress[0]));

        if(dialog.getProgress()==100){
            delegate.onPDFDownloadSuccess(getPDFFilePath(pdf_file_name),forWhich);
        }

    }

    @Override
    protected void onPostExecute(Void result) {

        dismissDialog();

    }

    private String getPDFFilePath(String file_name){

        String path = "";

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "KP");

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.e("KP", "failed to create directory");
            }
        }

        path = mediaStorageDir.getPath() + File.separator + file_name;

        return path;


    }

    private void dismissDialog(){

        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }

    }
}
