package com.kp.facedetection.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ModelRank implements Serializable {
    private String approvedACTime = "";
    private String approvedByAC = "";
    private String approvedByDC = "";
    private String approvedByDCTime = "";
    private String approvedByOC = "";
    private String caseRef = "";

    public String getApprovedACTime() {
        return approvedACTime;
    }

    public void setApprovedACTime(String approvedACTime) {
        this.approvedACTime = approvedACTime;
    }

    public String getApprovedByAC() {
        return approvedByAC;
    }

    public void setApprovedByAC(String approvedByAC) {
        this.approvedByAC = approvedByAC;
    }

    public String getApprovedByDC() {
        return approvedByDC;
    }

    public void setApprovedByDC(String approvedByDC) {
        this.approvedByDC = approvedByDC;
    }

    public String getApprovedByDCTime() {
        return approvedByDCTime;
    }

    public void setApprovedByDCTime(String approvedByDCTime) {
        this.approvedByDCTime = approvedByDCTime;
    }

    public String getApprovedByOC() {
        return approvedByOC;
    }

    public void setApprovedByOC(String approvedByOC) {
        this.approvedByOC = approvedByOC;
    }

    public String getCaseRef() {
        return caseRef;
    }

    public void setCaseRef(String caseRef) {
        this.caseRef = caseRef;
    }

    public String getDiv() {
        return div;
    }

    public void setDiv(String div) {
        this.div = div;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getApprovedByOCTime() {
        return approvedByOCTime;
    }

    public void setApprovedByOCTime(String approvedByOCTime) {
        this.approvedByOCTime = approvedByOCTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMcellRead() {
        return mcellRead;
    }

    public void setMcellRead(String mcellRead) {
        this.mcellRead = mcellRead;
    }

    public String getMcellReadTime() {
        return mcellReadTime;
    }

    public void setMcellReadTime(String mcellReadTime) {
        this.mcellReadTime = mcellReadTime;
    }

    public String getmCellToPsAction() {
        return mCellToPsAction;
    }

    public void setmCellToPsAction(String mCellToPsAction) {
        this.mCellToPsAction = mCellToPsAction;
    }

    public String getmCellToPsActionTime() {
        return mCellToPsActionTime;
    }

    public void setmCellToPsActionTime(String mCellToPsActionTime) {
        this.mCellToPsActionTime = mCellToPsActionTime;
    }

    public String getOfficerName() {
        return officerName;
    }

    public void setOfficerName(String officerName) {
        this.officerName = officerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProviderReply() {
        return providerReply;
    }

    public void setProviderReply(String providerReply) {
        this.providerReply = providerReply;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public String getRequest_type() {
        return request_type;
    }

    public void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

    public int getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(int sendStatus) {
        this.sendStatus = sendStatus;
    }

    public ArrayList<ModelServiceDetail> getDetails() {
        return details;
    }

    public void setDetails(ArrayList<ModelServiceDetail> details) {
        this.details = details;
    }

    private String div = "";
    private String emailId = "";
    private String approvedByOCTime = "";
    private int id;
    private String mcellRead = "";
    private String mcellReadTime = "";
    private String mCellToPsAction = "";
    private String mCellToPsActionTime = "";
    private String officerName = "";
    private String phoneNumber = "";
    private String providerReply = "";
    private String ps = "";
    private String rank = "";
    private String requestedBy = "";
    private String requestTime = "";
    private String request_type = "";
    private int sendStatus;
    private ArrayList<ModelServiceDetail> details = new ArrayList<>();

}
