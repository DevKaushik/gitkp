package com.kp.facedetection.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kp.facedetection.R;
import com.kp.facedetection.utility.Constants;

/**
 * Created by DAT-Asset-131 on 10-09-2016.
 */
public class DocumentEpicFragment extends Fragment {

    private Button btn_goTo_Epic;

    public static DocumentEpicFragment newInstance( ) {

        DocumentEpicFragment documentEpicFragment = new DocumentEpicFragment();
        return documentEpicFragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_epic_layout, container, false);

        // Inflate the layout for this fragment

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        btn_goTo_Epic = (Button)view.findViewById(R.id.btn_goTo_Epic);

        Constants.buttonEffect(btn_goTo_Epic);

        btn_goTo_Epic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://electoralsearch.in/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

    }
}
