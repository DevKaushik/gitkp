package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.MobileStolenSearchDetails;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by DAT-165 on 01-11-2016.
 */
public class MobileStolenSearchAdapter extends BaseAdapter {


    private Context context;
    private List<MobileStolenSearchDetails> mobileStolenSearchDetailsList;

    public MobileStolenSearchAdapter(Context context, List<MobileStolenSearchDetails> mobileStolenSearchDetailsList) {
        this.context = context;
        this.mobileStolenSearchDetailsList = mobileStolenSearchDetailsList;
    }


    @Override
    public int getCount() {

        int size = (mobileStolenSearchDetailsList.size()>0)? mobileStolenSearchDetailsList.size():0;
        return size;

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.mobile_stolen_list_item, null);


            holder.tv_slNo=(TextView)convertView.findViewById(R.id.tv_slNo);
            holder.tv_complainantName=(TextView)convertView.findViewById(R.id.tv_complainantName);
            holder.tv_psName=(TextView)convertView.findViewById(R.id.tv_psName);
            holder.tv_mobileNo=(TextView)convertView.findViewById(R.id.tv_mobileNo);
            holder.tv_dtMissing= (TextView)convertView.findViewById(R.id.tv_dtMissing);

            Constants.changefonts(holder.tv_slNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_mobileNo, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_complainantName, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_psName, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_dtMissing, context, "Calibri.ttf");

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);

        holder.tv_slNo.setText(sl_no);
        holder.tv_mobileNo.setText("Mobile No: "+mobileStolenSearchDetailsList.get(position).getMobileNo().trim());
        holder.tv_complainantName.setText("Complainant Name: "+mobileStolenSearchDetailsList.get(position).getComplainantName().trim());
        holder.tv_psName.setText("PS: "+mobileStolenSearchDetailsList.get(position).getPsName().trim());
        holder.tv_dtMissing.setText("Missing Date: "+mobileStolenSearchDetailsList.get(position).getDtMissing().trim());

        return convertView;
    }

    class ViewHolder {

        TextView tv_slNo,tv_complainantName,tv_psName,tv_mobileNo,tv_dtMissing;

    }
}
