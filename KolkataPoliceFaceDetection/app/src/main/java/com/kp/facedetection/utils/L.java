package com.kp.facedetection.utils;

/**
 * Created by DAT-165 on 02-06-2017.
 */

import android.util.Log;

/**
 * TODO: Created by Tanay Mondal on 02-03-2017
 */

public class L {

    private static final String TAG = "TANAY";
    private static boolean LOG_ENABLED = true;

    private L() {

    }

    public static void e(Object object) {
        if (LOG_ENABLED) {
            Log.e(TAG, String.valueOf(object));
        }
    }
}
