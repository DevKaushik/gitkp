package com.kp.facedetection.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 28-05-2018.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "suggestion_id",
        "request_id",
        "suggestion_details",
        "suggestion_by",
        "suggested_time"
})
public class SuggestionModel implements Serializable {
    @JsonProperty("suggestion_id")
    private String suggestionId;
    @JsonProperty("request_id")
    private String requestId;
    @JsonProperty("suggestion_details")
    private String suggestionDetails;
    @JsonProperty("suggestion_by")
    private String suggestionBy;
    @JsonProperty("suggested_time")
    private String suggestedTime;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("suggestion_id")
    public String getSuggestionId() {
        return suggestionId;
    }

    @JsonProperty("suggestion_id")
    public void setSuggestionId(String suggestionId) {
        this.suggestionId = suggestionId;
    }

    @JsonProperty("request_id")
    public String getRequestId() {
        return requestId;
    }

    @JsonProperty("request_id")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @JsonProperty("suggestion_details")
    public String getSuggestionDetails() {
        return suggestionDetails;
    }

    @JsonProperty("suggestion_details")
    public void setSuggestionDetails(String suggestionDetails) {
        this.suggestionDetails = suggestionDetails;
    }

    @JsonProperty("suggestion_by")
    public String getSuggestionBy() {
        return suggestionBy;
    }

    @JsonProperty("suggestion_by")
    public void setSuggestionBy(String suggestionBy) {
        this.suggestionBy = suggestionBy;
    }

    @JsonProperty("suggested_time")
    public String getSuggestedTime() {
        return suggestedTime;
    }

    @JsonProperty("suggested_time")
    public void setSuggestedTime(String suggestedTime) {
        this.suggestedTime = suggestedTime;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
