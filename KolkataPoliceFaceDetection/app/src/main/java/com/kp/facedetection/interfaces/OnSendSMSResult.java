package com.kp.facedetection.interfaces;

/**
 * Created by DAT-Asset-131 on 21-09-2016.
 */
public interface OnSendSMSResult {

    void onSendSMSResultSuccess(String success);
    void onSendSMSResultError(String error);

}
