package com.kp.facedetection;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.usage.ConfigurationStats;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.kp.facedetection.db.KPFetchDB;
import com.kp.facedetection.db.KPHelperDB;
import com.kp.facedetection.db.KPInsertDB;
import com.kp.facedetection.utility.BadgeUtils;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.TelephonyInfo;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.SessionManagement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends Activity implements View.OnClickListener {
	private static int SPALASH_TIME_OUT = 5000;
	private static int DIALOG_TIME_OUT = 8000;
	private String app_version = "";

	private static boolean activityVisible = false;

	private TextView tv_appVersion;
	private ImageView iv_datSiteLaunch;

	KPHelperDB dbhlpr_obj;
	KPInsertDB insertDB;
	KPFetchDB fetchDB;

	private List<String> appVersionList = new ArrayList<String>() ;
	SessionManagement sessionManagement;
	private boolean isInternetConnected;

	@Override
	protected void onStart() {
		super.onStart();
		activityVisible = true;
	}

	@Override
	public void onStop() {
		super.onStop();
		activityVisible = false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		isInternetConnected = Constants.internetOnline(this);

		activityVisible = true;

		setContentView(R.layout.content_main);
		Utility.setPushNotificationCount(this,"0");
		initialization();


		getDeviceInfo();

		if(!isInternetConnected){
			final AlertDialog ad = new AlertDialog.Builder(SplashActivity.this).create();
		ad.setTitle(Constants.ERROR_TITLE);
		ad.setMessage(Constants.ERROR_MSG_SPLASH);
		ad.setIcon(R.drawable.fail);
		ad.show();
		ad.setCanceledOnTouchOutside(false);
		}

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if (activityVisible) {
					if(isInternetConnected) {
						if (Utility.getUserInfo(SplashActivity.this) == null) {

							startActivity(new Intent(SplashActivity.this,
									LoginActivity.class));
						} else {

							startActivity(new Intent(SplashActivity.this,
									DashboardActivity.class));
						}
						finish();
					}
					else{
						finish();
					}
				} else {
					finish();
				}
			}
		}, SPALASH_TIME_OUT);



	}

	private void initialization(){

		tv_appVersion = (TextView)findViewById(R.id.tv_appVersion);
		Utility.changefonts(tv_appVersion, this, "Calibri.ttf");

		iv_datSiteLaunch = (ImageView) findViewById(R.id.iv_datSiteLaunch);
		iv_datSiteLaunch.setOnClickListener(this);


		try {

			dbhlpr_obj = new KPHelperDB(SplashActivity.this);
			dbhlpr_obj.createDataBase();

			if(!Utility.getTimestamp(this).equalsIgnoreCase("")){

				long lastTimeStamp = Long.valueOf(Utility.getTimestamp(this));
				String result = String.valueOf(Math.abs((System.currentTimeMillis() - lastTimeStamp)/1000));
				Utility.setLogMessage("TIME DIFF RESULT", result);
				if(Integer.valueOf(result)>=Constants.logout_interval){
					Utility.logout2(this);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void getDeviceInfo(){

		TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(SplashActivity.this);

		String imsiSIM1 = telephonyInfo.getImsiSIM1();
		String imsiSIM2 = telephonyInfo.getImsiSIM2();

		boolean isSIM1Ready = telephonyInfo.isSIM1Ready();
		boolean isSIM2Ready = telephonyInfo.isSIM2Ready();

		boolean isDualSIM = telephonyInfo.isDualSIM();

		System.out.println(""+" IME1 : " + imsiSIM1 + "\n" +
				" IME2 : " + imsiSIM2 + "\n" +
				" IS DUAL SIM : " + isDualSIM + "\n" +
				" IS SIM1 READY : " + isSIM1Ready + "\n" +
				" IS SIM2 READY : " + isSIM2Ready + "\n");

		Constants.device_IMEI_No = imsiSIM1;
		Utility.setImiNO(this,imsiSIM1);
		Log.d("JAY:------------","IMIE NO:------------"+Constants.device_IMEI_No);

		app_version = Utility.getAppVersion(SplashActivity.this);

		//tv_appVersion.setText("App Version " + app_version);
		tv_appVersion.setText("ver. " + app_version);

		fetchDB = new KPFetchDB();
		appVersionList = fetchDB.getInstalledAppVersionList(SplashActivity.this);

		for(String appVersion : appVersionList){
			Utility.setLogMessage("AppVersion",appVersion);
		}


		if(appVersionList.size()>0){

			if(!appVersionList.get(0).equalsIgnoreCase(app_version)){
				Utility.setUserInfo(this, null);
				sessionManagement = new SessionManagement(this);
				sessionManagement.storeEmailSession("");

				insertDB = new KPInsertDB();
				insertDB.insertLatestAppVersion(SplashActivity.this, app_version, Utility.getCurrentTimeStamp());
			}
		}
		else{
			insertDB = new KPInsertDB();
			insertDB.insertLatestAppVersion(SplashActivity.this, app_version, Utility.getCurrentTimeStamp());
		}

		AlphaAnimation fadeIn = new AlphaAnimation(0.0f , 1.0f );
		tv_appVersion.startAnimation(fadeIn);
		fadeIn.setDuration(4000);
		fadeIn.setFillAfter(true);
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	public static void activityResumed() {
		activityVisible = true;
	}

	public static void activityPaused() {
		activityVisible = false;
	}

	protected void onResume() {
		super.onResume();
		SplashActivity.activityResumed();
	}

	@Override
	protected void onPause() {
		super.onPause();
		SplashActivity.activityPaused();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.iv_datSiteLaunch:
				websiteLauch();
				break;
		}
	}


	private void websiteLauch(){

		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		intent.addCategory(Intent.CATEGORY_BROWSABLE);
		intent.setData(Uri.parse("http://www.digitalaptech.com/"));
		startActivity(intent);
	}

	private void showAlertDialogForFinish(){
		final AlertDialog ad = new AlertDialog.Builder(this).create();
		ad.setTitle(Constants.ERROR_DETAILS_TITLE);
		ad.setMessage(Constants.ERROR_MSG_DETAIL);
		ad.setIcon(R.drawable.fail);

		ad.show();
		ad.setCanceledOnTouchOutside(false);
	}
}
