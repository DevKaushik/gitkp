package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-Asset-131 on 23-05-2016.
 */
public class CriminalSearchData implements Serializable{

    private String name="";
    private String age="";
    private String ps_name="";
    private String ps_code="";
    private String alias_name="";
    private String address="";
    private String briefFact="";
    private String mark_categoryValue="";
    private String mark_subCategoryValue="";
    private String modified_categoryString="";
    private String categoryOfCrimeValue="";
    private String categoryOfCrimeString="";
    private String modusOperandiValue="";
    private String modusOperandiString="";
    private String classStr = "";
    private String classValue = "";
    private String subClassStr = "";
    private String subClassValue="";
    private String fName = "";
    private String caseYr = "";
    private String fromDt = "";
    private String toDt = "";



    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBriefFact() {
        return briefFact;
    }

    public void setBriefFact(String briefFact) {
        this.briefFact = briefFact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPs_name() {
        return ps_name;
    }

    public void setPs_name(String ps_name) {
        this.ps_name = ps_name;
    }

    public String getPs_code() {
        return ps_code;
    }

    public void setPs_code(String ps_code) {
        this.ps_code = ps_code;
    }

    public String getAlias_name() {
        return alias_name;
    }

    public void setAlias_name(String alias_name) {
        this.alias_name = alias_name;
    }

    public String getMark_categoryValue() {
        return mark_categoryValue;
    }

    public void setMark_categoryValue(String mark_categoryValue) {
        this.mark_categoryValue = mark_categoryValue;
    }

    public String getMark_subCategoryValue() {
        return mark_subCategoryValue;
    }

    public void setMark_subCategoryValue(String mark_subCategoryValue) {
        this.mark_subCategoryValue = mark_subCategoryValue;
    }

    public String getModified_categoryString() {
        return modified_categoryString;
    }

    public void setModified_categoryString(String modified_categoryString) {
        this.modified_categoryString = modified_categoryString;
    }

    public String getCategoryOfCrimeValue() {
        return categoryOfCrimeValue;
    }

    public void setCategoryOfCrimeValue(String categoryOfCrimeValue) {
        this.categoryOfCrimeValue = categoryOfCrimeValue;
    }

    public String getCategoryOfCrimeString() {
        return categoryOfCrimeString;
    }

    public void setCategoryOfCrimeString(String categoryOfCrimeString) {
        this.categoryOfCrimeString = categoryOfCrimeString;
    }

    public String getModusOperandiValue() {
        return modusOperandiValue;
    }

    public void setModusOperandiValue(String modusOperandiValue) {
        this.modusOperandiValue = modusOperandiValue;
    }

    public String getModusOperandiString() {
        return modusOperandiString;
    }

    public void setModusOperandiString(String modusOperandiString) {
        this.modusOperandiString = modusOperandiString;
    }

    public String getClassStr() {
        return classStr;
    }

    public void setClassStr(String classStr) {
        this.classStr = classStr;
    }

    public String getSubClassStr() {
        return subClassStr;
    }

    public void setSubClassStr(String subClassStr) {
        this.subClassStr = subClassStr;
    }

    public String getClassValue() {
        return classValue;
    }

    public void setClassValue(String classValue) {
        this.classValue = classValue;
    }

    public String getSubClassValue() {
        return subClassValue;
    }

    public void setSubClassValue(String subClassValue) {
        this.subClassValue = subClassValue;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getCaseYr() {
        return caseYr;
    }

    public void setCaseYr(String caseYr) {
        this.caseYr = caseYr;
    }

    public String getFromDt() {
        return fromDt;
    }

    public void setFromDt(String fromDt) {
        this.fromDt = fromDt;
    }

    public String getToDt() {
        return toDt;
    }

    public void setToDt(String toDt) {
        this.toDt = toDt;
    }
}
