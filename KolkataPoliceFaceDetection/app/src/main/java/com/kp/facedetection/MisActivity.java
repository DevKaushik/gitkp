package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.kp.facedetection.utility.Utility.showAlertDialog;

public class MisActivity extends BaseActivity implements View.OnClickListener {

    TextView chargesheetNotificationCount;
    String notificationCount="";
    private String userName = "";
    private String loginNumber = "";
    private String user_Id = "";
    private String appVersion = "";
    private String log_response="";
    private TextView online_user_badge,active_user_badge,firs_count_badge,udcase_count_badge;
    LinearLayout mis_online_users,mis_firs,mis_udcase,mis_active_users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis);

        MisCountApiCall();
        initView();
    }

    public void initView() {
        mis_online_users = (LinearLayout) findViewById(R.id.mis_online_users);
        mis_online_users.setOnClickListener(this);

        mis_firs = (LinearLayout) findViewById(R.id.mis_firs);
        mis_firs.setOnClickListener(this);

        mis_udcase = (LinearLayout) findViewById(R.id.mis_udcase);
        mis_udcase.setOnClickListener(this);

        mis_active_users = (LinearLayout) findViewById(R.id.mis_active_users);
        mis_active_users.setOnClickListener(this);

        online_user_badge = (TextView) findViewById(R.id.online_user_badge);
        active_user_badge = (TextView) findViewById(R.id.active_user_badge);
        firs_count_badge = (TextView) findViewById(R.id.firs_count_badge);
        udcase_count_badge = (TextView) findViewById(R.id.udcase_count_badge);

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
            user_Id = Utility.getUserInfo(this).getUserId();

        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout) mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this, charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount = (TextView) mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount = Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }

    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }

    private void MisCountApiCall() {
        TaskManager taskManager = new TaskManager(MisActivity.this);
        taskManager.setMethod(Constants.METHOD_COUNT_MIS_ONLINE_USERS);
        taskManager.setMiscount(true);

        String[] keys = {};
        String[] values = {};
        taskManager.doStartTask(keys, values, true);
    }

    public void ParseMisCount(String result) {
        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);

                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {
                    JSONObject o = jObj.getJSONObject("result");
                    String ONLINE_USERS = o.getString("ONLINE_USERS");
                    String ACTIVE_USERS = o.getString("ACTIVE_USERS");
                    String FIR_RECORDED = o.getString("FIR_RECORDED");
                    String UNNATURAL_DEATH = o.getString("UNNATURAL_DEATH");

                    /*Log.e("ONLINE_USERS","ONLINE_USERS"+ONLINE_USERS);*/
                    online_user_badge.setText(ONLINE_USERS);
                    active_user_badge.setText(ACTIVE_USERS);
                    firs_count_badge.setText(FIR_RECORDED);
                    udcase_count_badge.setText(UNNATURAL_DEATH);

                }
                else{
                    Utility.showAlertDialog(MisActivity.this,Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL,false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(MisActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {
            case R.id.mis_online_users :
                Intent misountent = new Intent(MisActivity.this, MisOnlineUsersActivity.class);
                startActivity(misountent);
                break;

            case R.id.mis_active_users :
                Intent misauntent = new Intent(MisActivity.this, MisActiveUsersActivity.class);
                startActivity(misauntent);
                break;

            case R.id.mis_firs:
                Toast.makeText(this, "Work in Progress", Toast.LENGTH_SHORT).show();
                break;

            case R.id.mis_udcase:
                Toast.makeText(this, "Work in Progress", Toast.LENGTH_SHORT).show();
                break;
        }

    }
}
