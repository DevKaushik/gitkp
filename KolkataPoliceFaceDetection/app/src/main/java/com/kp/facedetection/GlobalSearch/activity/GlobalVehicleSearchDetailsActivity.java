package com.kp.facedetection.GlobalSearch.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.kp.facedetection.AllDocumentDataSearchResultActivity;
import com.kp.facedetection.BaseActivity;
import com.kp.facedetection.KPFaceDetectionApplication;
import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnShowAlertForProceedResult;
import com.kp.facedetection.model.AllDocumentSearchType;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class GlobalVehicleSearchDetailsActivity extends BaseActivity implements View.OnClickListener, Observer,OnShowAlertForProceedResult {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private TextView tv_heading;

    private TextView tv_mobileNoValue;
    private TextView tv_regNoValue;
    private TextView tv_regDateValue;
    private TextView tv_holderNameValue;
    private TextView tv_fNameValue;
    private TextView tv_presentAddressValue;
    private TextView tv_permanentAddressValue;
    private TextView tv_engNoValue;
    private TextView tv_chasisNoValue;
    private TextView tv_garageAddValue;
    private TextView tv_colorValue;
    private TextView tv_makerValue;
    private TextView tv_clDescValue;

    private String holder_Veh_HolderMobileNo="";
    private String holder_Veh_RegNo="";
    private String holder_Veh_RegDate="";
    private String holder_Veh_HolderName="";
    private String holder_Veh_FatherName="";
    private String holder_Veh_PresentAddress="";
    private String holder_Veh_PermanentAddress="";
    private String holder_Veh_EngineNo="";
    private String holder_Veh_ChasisNo="";
    private String holder_Veh_GarageAddress="";
    private String holder_Veh_Color="";
    private String holder_Veh_Maker="";
    private String holder_Veh_Cldesc="";
    private TextView tv_watermark;
    TextView chargesheetNotificationCount;
    String notificationCount="";
    String name = "", address = "", contact = "";
    CheckBox chk_dl,chk_vehicle,chk_sdr,chk_cable,chk_kmc,chk_gas,chk_select_all;
    private String pageno = "";
    private String totalResult = "";
    private String[] keys;
    private String[] values;
    String userId = "";
    private Button btn_Search, btn_reset;
    String datafields ="";
    ArrayList<AllDocumentSearchType> allDocumentSearchTypesArrayList = new ArrayList<AllDocumentSearchType>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_vehiclesearch_details);
        ObservableObject.getInstance().addObserver(this);

        initialization();
    }

    private void initialization() {

        tv_mobileNoValue = (TextView)findViewById(R.id.tv_mobileNoValue);
        tv_regNoValue = (TextView)findViewById(R.id.tv_regNoValue);
        tv_regDateValue = (TextView)findViewById(R.id.tv_regDateValue);
        tv_holderNameValue = (TextView)findViewById(R.id.tv_holderNameValue);
        tv_fNameValue = (TextView)findViewById(R.id.tv_fNameValue);
        tv_presentAddressValue = (TextView)findViewById(R.id.tv_presentAddressValue);
        tv_permanentAddressValue = (TextView)findViewById(R.id.tv_permanentAddressValue);
        tv_engNoValue = (TextView)findViewById(R.id.tv_engNoValue);
        tv_chasisNoValue = (TextView)findViewById(R.id.tv_chasisNoValue);
        tv_garageAddValue = (TextView)findViewById(R.id.tv_garageAddValue);
        tv_colorValue = (TextView)findViewById(R.id.tv_colorValue);
        tv_makerValue = (TextView)findViewById(R.id.tv_makerValue);
        tv_clDescValue = (TextView)findViewById(R.id.tv_clDescValue);

        tv_heading = (TextView) findViewById(R.id.tv_heading);

        tv_watermark = (TextView)findViewById(R.id.tv_watermark);

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        holder_Veh_HolderMobileNo = getIntent().getStringExtra("Holder_Veh_HolderMobileNo").trim();
        holder_Veh_RegNo = getIntent().getStringExtra("Holder_Veh_RegNo").trim();
        holder_Veh_RegDate = getIntent().getStringExtra("Holder_Veh_RegDate").trim();
        holder_Veh_HolderName = getIntent().getStringExtra("Holder_Veh_HolderName").trim();
        holder_Veh_FatherName = getIntent().getStringExtra("Holder_Veh_FatherName").trim();
        holder_Veh_PresentAddress = getIntent().getStringExtra("Holder_Veh_PresentAddress").trim();
        holder_Veh_PermanentAddress = getIntent().getStringExtra("Holder_Veh_PermanentAddress").trim();
        holder_Veh_EngineNo = getIntent().getStringExtra("Holder_Veh_EngineNo").trim();
        holder_Veh_ChasisNo = getIntent().getStringExtra("Holder_Veh_ChasisNo").trim();
        holder_Veh_GarageAddress = getIntent().getStringExtra("Holder_Veh_GarageAddress").trim();
        holder_Veh_Color = getIntent().getStringExtra("Holder_Veh_Color").trim();
        holder_Veh_Maker = getIntent().getStringExtra("Holder_Veh_Maker").trim();
        holder_Veh_Cldesc = getIntent().getStringExtra("Holder_Veh_Cldesc").trim();

        tv_mobileNoValue.setText(holder_Veh_HolderMobileNo);
        tv_regNoValue.setText(holder_Veh_RegNo);
        tv_regDateValue.setText(holder_Veh_RegDate);
        tv_holderNameValue.setText(holder_Veh_HolderName);
        tv_fNameValue.setText(holder_Veh_FatherName);
        tv_presentAddressValue.setText(holder_Veh_PresentAddress);
        tv_permanentAddressValue.setText(holder_Veh_PermanentAddress);
        tv_engNoValue.setText(holder_Veh_EngineNo);
        tv_chasisNoValue.setText(holder_Veh_ChasisNo);
        tv_garageAddValue.setText(holder_Veh_GarageAddress);
        tv_colorValue.setText(holder_Veh_Color);
        tv_makerValue.setText(holder_Veh_Maker);
        tv_clDescValue.setText(holder_Veh_Cldesc);

        Constants.changefonts(tv_mobileNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_regNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_regDateValue, this, "Calibri.ttf");
        Constants.changefonts(tv_holderNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_fNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_presentAddressValue, this, "Calibri.ttf");
        Constants.changefonts(tv_permanentAddressValue, this, "Calibri.ttf");
        Constants.changefonts(tv_engNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_chasisNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_garageAddValue, this, "Calibri.ttf");
        Constants.changefonts(tv_colorValue, this, "Calibri.ttf");
        Constants.changefonts(tv_makerValue, this, "Calibri.ttf");
        Constants.changefonts(tv_clDescValue, this, "Calibri.ttf");

        Constants.changefonts(tv_heading, this, "Calibri Bold.ttf");

         /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);

        /* section for check box and search button  initilization */
        chk_dl=(CheckBox)findViewById(R.id.chk_dl);
        chk_dl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_dl.setChecked(true);
                else
                    chk_dl.setChecked(false);
            }
        });

        chk_sdr=(CheckBox)findViewById(R.id.chk_sdr);
        chk_sdr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_sdr.setChecked(true);
                else
                    chk_sdr.setChecked(false);
            }
        });


        chk_cable=(CheckBox)findViewById(R.id.chk_cable);
        chk_cable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_cable.setChecked(true);
                else
                    chk_cable.setChecked(false);
            }
        });


        chk_kmc=(CheckBox)findViewById(R.id.chk_kmc);
        chk_kmc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_kmc.setChecked(true);
                else
                    chk_kmc.setChecked(false);
            }
        });


        chk_gas=(CheckBox)findViewById(R.id.chk_gas);
        chk_gas.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_gas.setChecked(true);
                else
                    chk_gas.setChecked(false);
            }
        });

        chk_select_all=(CheckBox)findViewById(R.id.chk_select_all);
        chk_select_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    chk_dl.setChecked(true);
                    chk_sdr.setChecked(true);
                    chk_cable.setChecked(true);
                    chk_kmc.setChecked(true);
                    chk_gas.setChecked(true);

                }
                else{
                    chk_dl.setChecked(false);
                    chk_sdr.setChecked(false);
                    chk_cable.setChecked(false);
                    chk_kmc.setChecked(false);
                    chk_gas.setChecked(false);
                }
            }
        });
        btn_Search = (Button) findViewById(R.id.btn_Search);
        btn_reset = (Button) findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_Search);
        Constants.buttonEffect(btn_reset);
        clickEvents();

    }
    public void clickEvents() {
        btn_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name = holder_Veh_HolderName ;

                address = holder_Veh_PermanentAddress.toLowerCase();

                contact = holder_Veh_HolderMobileNo;
                if(chk_dl.isChecked()){
                    datafields="1";
                }else{
                    datafields=datafields.replace("1","");
                }

                if(chk_sdr.isChecked()){
                    datafields=datafields+","+"3";
                }
                else{
                    datafields=datafields.replace(",3","");
                }


                if(chk_cable.isChecked()){
                    datafields=datafields+","+"4";
                }
                else{
                    datafields=datafields.replace(",4","");

                }

                if(chk_kmc.isChecked()){
                    datafields=datafields+","+"5";
                }
                else{
                    datafields=datafields.replace(",5","");

                }

                if(chk_gas.isChecked()){
                    datafields=datafields+","+"6";
                }
                else{
                    datafields=datafields.replace(",6","");

                }
                if(datafields.equalsIgnoreCase("")){
                    datafields="1,3,4,5,6";
                }
                if (!name.isEmpty() || !address.isEmpty() || !contact.isEmpty()) {
                    Utility utility = new Utility();
                    utility.setDelegate(GlobalVehicleSearchDetailsActivity.this);
                    Utility.showAlertForProceed(GlobalVehicleSearchDetailsActivity.this);
                } else {

                    Utility.showAlertDialog(GlobalVehicleSearchDetailsActivity.this, " Search Error!!! ", "Please provide at least one value for search", false);

                }


            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                chk_dl.setChecked(false);
                chk_sdr.setChecked(false);
                chk_cable.setChecked(false);
                chk_kmc.setChecked(false);
                chk_gas.setChecked(false);
                chk_select_all.setChecked(false);



            }
        });
    }
    @Override
    public void onShowAlertForProceedResultSuccess(String success) {
        fetchSearchResult();
    }
    private void fetchSearchResult() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_DOCUMENT_RELATIONAL_SEARCH);
        taskManager.setVehicleAllDataRelationalSearch(true);
        //user_id,device_token,device_type,imei,auth_key,name,address,pageno
        String[] keys = {"name", "address", "phoneNumber", "dataFields", "page_no"};
        String[] values = {name, address, contact,datafields, "1"};
        taskManager.doStartTask(keys, values, true, true);
    }

    public void parseVehicleAllDataRelationalSearchResultResponse(String result, String[] keys, String[] values) {
        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("page").toString();
                    totalResult = jObj.opt("count").toString();

                    JSONObject resultObject = jObj.getJSONObject("result");

                    createAllDocumentSearchTypeList(resultObject);

                } else {
                    Utility.showAlertDialog(this, " Search Error ", jObj.optString("message"), false);
                }


            } catch (JSONException e) {

                e.printStackTrace();
                //Utility.showToast(getActivity(), "Some error has been encountered", "long");
                Utility.showAlertDialog(this, " Search Error ", "Some error has been encountered", false);
            }
        }

    }

    public void createAllDocumentSearchTypeList(JSONObject resultArray) {
        allDocumentSearchTypesArrayList.clear();
        if (resultArray.has("1")) {
            try {
                JSONObject jsonObjectDL = resultArray.getJSONObject("1");
                if (jsonObjectDL.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[0]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }

        if (resultArray.has("3")) {
            try {
                JSONObject jsonObjectSDR = resultArray.getJSONObject("3");
                if (jsonObjectSDR.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[2]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("4")) {
            try {
                JSONObject jsonObjectCable = resultArray.getJSONObject("4");
                if (jsonObjectCable.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[3]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("5")) {
            try {
                JSONObject jsonObjectKMC = resultArray.getJSONObject("5");
                if (jsonObjectKMC.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[4]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("6")) {
            try {
                JSONObject jsonObjectGas = resultArray.getJSONObject("6");
                if (jsonObjectGas.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[5]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }

        }
        Intent intent = new Intent(this, AllDocumentDataSearchResultActivity.class);
        intent.putExtra("keys", keys);
        intent.putExtra("value", values);
        intent.putExtra("ALL_DOCUMENT_SEARCH_TYPE", (Serializable) allDocumentSearchTypesArrayList);
        intent.putExtra("ALL_DOCUMENT_SEARCH_LIST", String.valueOf(resultArray));
        startActivity(intent);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }


    @Override
    public void onShowAlertForProceedResultError(String error) {

    }
}
