package com.kp.facedetection.model;

/**
 * Created by JAYDEEP on 5/6/2018.
 */

public class StateModel {
    String id;
    String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
