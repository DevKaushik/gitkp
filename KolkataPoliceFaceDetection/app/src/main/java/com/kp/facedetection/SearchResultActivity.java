package com.kp.facedetection;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.text.Layout;
import android.text.SpannableString;
import android.text.style.AlignmentSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.adapter.CriminalAdapter;
import com.kp.facedetection.adapter.CustomDialogAdapterForCategoryCrimeList;
import com.kp.facedetection.adapter.CustomDialogAdapterForCategoryCrimeList2;
import com.kp.facedetection.adapter.CustomDialogAdapterForModusOperandi;
import com.kp.facedetection.adapter.CustomFooterAdapter;
import com.kp.facedetection.fragments.CriminalSearchFragment;
import com.kp.facedetection.interfaces.OnImageUploadListener;
import com.kp.facedetection.model.CrimeCategoryList;
import com.kp.facedetection.model.CrimeCategoryList2;
import com.kp.facedetection.model.CriminalDetails;
import com.kp.facedetection.model.CriminalSearchData;
import com.kp.facedetection.model.ModusOperandiList;
import com.kp.facedetection.model.PoliceStationList;
import com.kp.facedetection.singletone_classes.CommunicationViews;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.HorizontalListView;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class SearchResultActivity extends BaseActivity implements OnItemClickListener, SearchView.OnQueryTextListener, AdapterView.OnItemSelectedListener, OnImageUploadListener, View.OnClickListener, Observer {

    ArrayList<CriminalDetails> criminalList;
    ArrayList<ModusOperandiList> obj_modusOperandiList;
    ArrayList<CrimeCategoryList> obj_categoryCrimeList;
    ArrayList<CrimeCategoryList2> obj_categoryCrimeList2;

    private List<String> identitySubCategoryList = new ArrayList<String>();
    private List<String> customIdentitySubCategoryList = new ArrayList<String>();
    private List<String> policeStationArrayList = new ArrayList<String>();
    ArrayList<PoliceStationList> obj_policeStationList;
    protected ArrayList<CharSequence> selectedPoliceStations = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedPoliceStationsId = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedIdentifyCategory = new ArrayList<CharSequence>();
    protected ArrayList<String> customSelectedIdentifySubCategory = new ArrayList<String>();
    protected ArrayList<CharSequence> selectedIdentifySubCategory = new ArrayList<CharSequence>();

    private List<String> subClassList = new ArrayList<String>();
    private List<String> customSubClassList = new ArrayList<String>();

    protected ArrayList<CharSequence> selectedClassList = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedSubClassList = new ArrayList<CharSequence>();
    protected ArrayList<String> customSelectedSubClass = new ArrayList<String>();

    CustomDialogAdapterForCategoryCrimeList customDialogAdapterForCategoryCrimeList;
    CustomDialogAdapterForCategoryCrimeList2 customDialogAdapterForCategoryCrimeList2;
    CustomDialogAdapterForModusOperandi customDialogAdapterForModusOperandi;
    CriminalAdapter adapter;
    private ArrayAdapter<CharSequence> ageAdapter;


    protected String[] array_policeStation;
    protected String[] array_identifyCategory;
    protected String[] array_identifySubCategory;
    private String[] inputMarkArray;

    protected String[] array_classValue;
    protected String[] array_subClassValue;
    private String[] inputClassArray;

    private static HashMap<String, List<String>> map;


    ListView lv_criminals;
    Button btnLoadMore;
    private TextView tv_resultCount;
    private ImageView iv_arrow;
    private RelativeLayout relative_main;
    private Spinner sp_age;
    private Button btn_done;
    private EditText et_search;
    private TextView tv_search;
    private TextView tv_identityMarkSubCategory;
    private HorizontalListView customListView;

    private Spinner sp_year;
    private TextView tv_subClass;

    private String[] keys;
    private String[] values;
    private String pageno;
    private String totalResult;
    private String searchCase = "";
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    CriminalSearchData criminalSearchData;


    /* serach item variable declaration */

    private String name = "";
    private String policeStationString = "";
    private String alias = "";
    private String age = "";
    private String address = "";
    private String brief_fact = "";
    private String identifyCategorySubCategoryString = "";
    private String crimeCategoryString = "";
    private String modusOperandiString = "";
    private String identifySubCategoryString = "";
    private String identifyCategoryString = "";

    private String fName = "";
    private String caseYr = "";
    private String fromDt = "";
    private String toDt = "";
    private String classValue = "";
    private String subClassValue = "";

    private String subClassStringShow = "";
    private String classStringShow = "";

    /* reset dialog variable declaration */

    private CheckedTextView chk_tv_name;
    private CheckedTextView chk_tv_age;
    private CheckedTextView chk_tv_ps;
    private CheckedTextView chk_tv_alias;
    private CheckedTextView chk_tv_markCategory;
    private CheckedTextView chk_tv_crimeCategory;
    private CheckedTextView chk_tv_modus;
    private CheckedTextView chk_tv_address;
    private CheckedTextView chk_tv_brief_fact;
    private Button btn_reset_search_done;

    private CheckedTextView chk_tv_fName;
    private CheckedTextView chk_tv_year;
    private CheckedTextView chk_tv_fromDate,chk_tv_toDate;
    private CheckedTextView chk_tv_class;

    /*previous search data stored value for reset list population */

    private String alreadySearchName = "";
    private String alreadySearchAge = "";
    private String alreadySearchAliasName = "";
    private String alreadySearchAddress = "";
    private String alreadySearchCategoryOfCrime = "";
    private String alreadySearchModusOperandi = "";
    private String alreadySearchPSString = "";
    private String alreadySearchModifiedCategoryString = "";
    private String alreadySearchBriefFact = "";

    private String advance_search_title = "";

    private String alreadySearchFname = "";
    private String alreadySearchCaseYr = "";
    private String alreadySearchFromDt = "";
    private String alreadySearchToDt = "";
    private String alreadySearchClass = "";
    private String alreadySearchSubClass = "";

    /* These variables are used to check status(enabled or disabled) of dropdown items */

    private boolean flag_modusOperandi = true;
    private boolean flag_name = true;
    private boolean flag_alias = true;
    private boolean flag_address = true;
    private boolean flag_categoryOfCrime = true;
    private boolean flag_policeStation = true;
    private boolean flag_modifiedCategory = true;
    private boolean flag_age = true;
    private boolean flag_brief_fact = true;

    private boolean flag_fName = true;
    private boolean flag_caseYr = true;
    private boolean flag_fromDt = true;
    private boolean flag_toDt = true;
    private boolean flag_class = true;

    /*  variable use to take reset string   */
    public StringBuilder resetString;


    /* These variables are used to check serach status of crime and modus part */

    public boolean crimeCategory_status = false;
    public boolean modusOperandi_status = false;

    /*  create modified_crimeCategoryList to add FAVOURITE & NON-FAVOURITE */
    private List<String> modified_crimeCategoryList = new ArrayList<String>();

    private List<String> footerListData = new ArrayList<String>();
    CustomFooterAdapter customFooterAdapter;

    private TextView tv_watermark;

    private List<String> yearList = new ArrayList<String>();
    ArrayAdapter<String> yearAdapter;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    String userId ="";


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.search_result_layout);
        ObservableObject.getInstance().addObserver(this);

        try {
            userId= Utility.getUserInfo(this).getUserId();
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        // singletone class intialize
        CommunicationViews.getInstance().setOnImageUploadListener(this);


        lv_criminals = (ListView) findViewById(R.id.lv_criminals);
        tv_resultCount = (TextView) findViewById(R.id.tv_resultCount);
        iv_arrow = (ImageView) findViewById(R.id.iv_arrow);
        relative_main = (RelativeLayout) findViewById(R.id.relative_main);
        customListView = (HorizontalListView) findViewById(R.id.customListView);

        tv_watermark = (TextView) findViewById(R.id.tv_watermark);

        criminalList = (ArrayList<CriminalDetails>) getIntent()
                .getSerializableExtra(Constants.OBJECT);
        obj_categoryCrimeList = (ArrayList<CrimeCategoryList>) getIntent()
                .getSerializableExtra("crimeCategoryList");

        obj_categoryCrimeList2 = (ArrayList<CrimeCategoryList2>) getIntent()
                .getSerializableExtra("modifiedcrimeCategoryList");

        obj_modusOperandiList = (ArrayList<ModusOperandiList>) getIntent().getSerializableExtra("modusOperandiList");

        obj_policeStationList = (ArrayList<PoliceStationList>) getIntent()
                .getSerializableExtra("policeStation");


        keys = getIntent().getStringArrayExtra("keys");
        values = getIntent().getStringArrayExtra("value");
        pageno = getIntent().getStringExtra("pageno");
        totalResult = getIntent().getStringExtra("totalResult");
        searchCase = getIntent().getStringExtra("searchCase");
        //classStringShow = getIntent().getStringExtra("classList");
        //subClassStringShow = getIntent().getStringExtra("subClassList");
        criminalSearchData = (CriminalSearchData) getIntent().getSerializableExtra("prev_data");

        array_identifyCategory = new String[CriminalSearchFragment.identityCategoryList.size()];
        array_identifyCategory = CriminalSearchFragment.identityCategoryList.toArray(array_identifyCategory);

        array_classValue = new String[CriminalSearchFragment.classArrayList.size()];
        array_classValue = CriminalSearchFragment.classArrayList.toArray(array_classValue);

        /*array_subClassValue = new String[CriminalSearchFragment.subClassList.size()];
        array_subClassValue = CriminalSearchFragment.subClassList.toArray(array_subClassValue);*/

        alreadySearchName = criminalSearchData.getName();
        alreadySearchAge = criminalSearchData.getAge();
        alreadySearchPSString = criminalSearchData.getPs_code();
        alreadySearchAliasName = criminalSearchData.getAlias_name();
        alreadySearchAddress = criminalSearchData.getAddress();
        alreadySearchBriefFact = criminalSearchData.getBriefFact();
        alreadySearchCategoryOfCrime = criminalSearchData.getCategoryOfCrimeString();
        alreadySearchModusOperandi = criminalSearchData.getModusOperandiString();
        alreadySearchModifiedCategoryString = criminalSearchData.getModified_categoryString();

        alreadySearchFname = criminalSearchData.getfName();
        alreadySearchCaseYr = criminalSearchData.getCaseYr();
        alreadySearchFromDt = criminalSearchData.getFromDt();
        alreadySearchToDt = criminalSearchData.getToDt();
        alreadySearchClass = criminalSearchData.getClassStr();
        alreadySearchSubClass = criminalSearchData.getSubClassStr();


        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }

        setFooterData();

        adapter = new CriminalAdapter(this, R.layout.search_listitem_layout,
                R.id.rl_container, criminalList);
        lv_criminals.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        // LoadMore button
        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");




        if (Integer.parseInt(totalResult) > 20) {
            // Adding Load More button to listview at bottom
            lv_criminals.addFooterView(btnLoadMore);
        }


        clickEvents();

        lv_criminals.setOnItemClickListener(this);

        /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        if(criminalList.get(position).getProvCrmNo().isEmpty()||criminalList.get(position).getProvCrmNo().equals(null)||criminalList.get(position).getProvCrmNo().equals("null"))
        {
            Utility.showAlertDialog(SearchResultActivity.this,Constants.ERROR,Constants.ERROR_PRV_CRIMINAL_NO,false);

        }
        else {
            Intent intent = new Intent(this, CriminalDetailActivity.class);
            intent.putExtra(Constants.OBJECT, criminalList.get(position));
            intent.putExtra("searchCase", searchCase);
            intent.putExtra("List_Position", position);
            startActivity(intent);
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
           /* case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                // Utility.logout(this);
                break;
        }
        return true;

    }

    private void fetchSearchResult() {

        if (!Constants.internetOnline(this)) {
            Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        TaskManager taskManager = new TaskManager(this);
        if (searchCase.equalsIgnoreCase("criminalSearch")) {
            taskManager.setMethod(Constants.METHOD_CRIMINAL_SEARCH_PAGING);
            //  values[7]=(Integer.parseInt(pageno)+1)+"";
            values[7] = (Integer.parseInt(pageno) + 1) + "";
        } else if (searchCase.equalsIgnoreCase("caseSearch")) {
            taskManager.setMethod(Constants.METHOD_CASE_SEARCH_PAGING);
            values[6] = (Integer.parseInt(pageno) + 1) + "";
        }

        taskManager.setDetailedSearchPaging(true);
        taskManager.doStartTask(keys, values, true);
    }


    public void parseSearchResultPaging(String jsonStr) {
        try {
            if (jsonStr != null && !jsonStr.equals("")) {

                JSONObject jObj = new JSONObject(jsonStr);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {
                    JSONArray jsonArray = jObj.optJSONArray("result");
                    pageno = jObj.opt("pageno").toString();

                    //totalResult=jObj.opt("totalresult").toString();
                    criminalList.addAll(Utility.parseCrimiinalRecords(SearchResultActivity.this, jsonArray));
                    // if (criminalList != null && criminalList.size() > 0) {
                    int currentPosition = lv_criminals.getFirstVisiblePosition();
                    int lastVisiblePosition = lv_criminals.getLastVisiblePosition();

                    System.out.println("Current Pos: " + lastVisiblePosition);

                    // Setting new scroll position
                    lv_criminals.setSelectionFromTop(currentPosition + 1, 0);

                    if ((Integer.parseInt(totalResult) - lastVisiblePosition) < 20) {
                        lv_criminals.removeFooterView(btnLoadMore);
                    }

                    /*if (!((Integer.parseInt(totalResult) / 20) > Integer.parseInt(pageno))) {
                        lv_criminals.removeFooterView(btnLoadMore);
                    }*/
                       /* Intent intent = new Intent(SearchResultActivity.this,
                                SearchResultActivity.class);
                        intent.putExtra(Constants.OBJECT, criminalList);
                        SearchResultActivity.this.startActivity(intent);*/
                    //}
                }
                if (!jObj.optString("message").toString().equalsIgnoreCase("Success"))
                    Toast.makeText(SearchResultActivity.this, jObj.optString("message"),
                            Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            showAlertDialog(SearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
        } catch (OutOfMemoryError outOfMemoryError) {
            outOfMemoryError.printStackTrace();
            showAlertDialog(SearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    private void getPreviousSearchData() {

        policeStationString = criminalSearchData.getPs_code();

        System.out.println("Name: " + criminalSearchData.getName());
        System.out.println("PS Name: " + criminalSearchData.getPs_name());
        System.out.println("Ps Code: " + criminalSearchData.getPs_code());
        System.out.println("Alias Name: " + criminalSearchData.getAlias_name());
        System.out.println("Age: " + criminalSearchData.getAge());
        System.out.println("Address: " + criminalSearchData.getAddress());
        System.out.println("Brief Fact: " + criminalSearchData.getBriefFact());
        System.out.println("Mark Cat Value: " + criminalSearchData.getMark_categoryValue());
        System.out.println("Mark Sub Cat value: " + criminalSearchData.getMark_subCategoryValue());
        System.out.println("Modified Identify String: " + criminalSearchData.getModified_categoryString());
        System.out.println("Crime Category Value: " + criminalSearchData.getCategoryOfCrimeValue());
        System.out.println("Crime Category String: " + criminalSearchData.getCategoryOfCrimeString());
        System.out.println("Modus Operandi Value: " + criminalSearchData.getModusOperandiValue());
        System.out.println("Modus Operandi String: " + criminalSearchData.getModusOperandiString());
        System.out.println("Father's Name: " + criminalSearchData.getfName());
        System.out.println("Case Year: " + criminalSearchData.getCaseYr());
        System.out.println("Class Name: " + criminalSearchData.getClassValue());
        System.out.println("From Date: " + criminalSearchData.getFromDt());
        System.out.println("To Date: " + criminalSearchData.getToDt());

    }

    /*on iv_arrow click drop-down popup list appear to do advance search*/
    private void clickEvents() {

        iv_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopUpList();

            }
        });

        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Starting a new async task
                fetchSearchResult();
            }
        });

    }

    public void showPopUpList() {

        getPreviousSearchData();

        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(SearchResultActivity.this, iv_arrow);

        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.popup_list, popup.getMenu());

        if (!criminalSearchData.getPs_code().equalsIgnoreCase("")) {

            popup.getMenu().findItem(R.id.psname).setEnabled(false);

        } else {
            if (!flag_policeStation) {
                popup.getMenu().findItem(R.id.psname).setEnabled(false);
            }
        }


        if (!criminalSearchData.getAge().equalsIgnoreCase("")) {
            //Inflating the Popup using xml file
            popup.getMenu().findItem(R.id.age).setEnabled(false);
        } else {
            if (!flag_age) {
                popup.getMenu().findItem(R.id.age).setEnabled(false);
            }
        }


        if (!criminalSearchData.getName().equalsIgnoreCase("")) {
            popup.getMenu().findItem(R.id.name).setEnabled(false);

        } else {
            if (!flag_name) {
                popup.getMenu().findItem(R.id.name).setEnabled(false);
            }
        }

        if (!criminalSearchData.getfName().equalsIgnoreCase("")) {
            popup.getMenu().findItem(R.id.fName).setEnabled(false);

        } else {
            if (!flag_fName) {
                popup.getMenu().findItem(R.id.fName).setEnabled(false);
            }
        }

        if (!criminalSearchData.getCaseYr().equalsIgnoreCase("")) {
            popup.getMenu().findItem(R.id.caseYr).setEnabled(false);

        } else {
            if (!flag_caseYr) {
                popup.getMenu().findItem(R.id.caseYr).setEnabled(false);
            }
        }

        if (!criminalSearchData.getFromDt().equalsIgnoreCase("")) {
            popup.getMenu().findItem(R.id.fromDate).setEnabled(false);

        } else {
            if (!flag_fromDt) {
                popup.getMenu().findItem(R.id.fromDate).setEnabled(false);
            }
        }

        if (!criminalSearchData.getToDt().equalsIgnoreCase("")) {
            popup.getMenu().findItem(R.id.toDate).setEnabled(false);

        } else {
            if (!flag_toDt) {
                popup.getMenu().findItem(R.id.toDate).setEnabled(false);
            }
        }

        if (!criminalSearchData.getAlias_name().equalsIgnoreCase("")) {
            popup.getMenu().findItem(R.id.alias).setEnabled(false);
        } else {
            if (!flag_alias) {
                popup.getMenu().findItem(R.id.alias).setEnabled(false);
            }
        }

        if (!criminalSearchData.getAddress().equalsIgnoreCase("")) {
            popup.getMenu().findItem(R.id.address).setEnabled(false);
        } else {
            if (!flag_address) {
                popup.getMenu().findItem(R.id.address).setEnabled(false);
            }
        }

        if (!criminalSearchData.getBriefFact().equalsIgnoreCase("")) {
            popup.getMenu().findItem(R.id.brief_fact).setEnabled(false);
        } else {
            if (!flag_brief_fact) {
                popup.getMenu().findItem(R.id.brief_fact).setEnabled(false);
            }
        }

        if (!criminalSearchData.getCategoryOfCrimeString().equalsIgnoreCase("")) {
            popup.getMenu().findItem(R.id.crimeCategory).setEnabled(false);

        } else {
            if (!flag_categoryOfCrime) {
                popup.getMenu().findItem(R.id.crimeCategory).setEnabled(false);
            }
        }

        if (!criminalSearchData.getModusOperandiString().equalsIgnoreCase("")) {
            popup.getMenu().findItem(R.id.modus).setEnabled(false);
        } else {
            if (!flag_modusOperandi) {
                popup.getMenu().findItem(R.id.modus).setEnabled(false);
            }
        }

        if (!criminalSearchData.getModified_categoryString().equalsIgnoreCase("")) {
            popup.getMenu().findItem(R.id.markCategory).setEnabled(false);
        } else {
            if (!flag_modifiedCategory) {
                popup.getMenu().findItem(R.id.markCategory).setEnabled(false);
            }
        }

        if (!criminalSearchData.getClassStr().equalsIgnoreCase("")) {
            popup.getMenu().findItem(R.id.classValue).setEnabled(false);
        } else {
            if (!flag_class) {
                popup.getMenu().findItem(R.id.classValue).setEnabled(false);
            }
        }


        MenuItem item_reset = popup.getMenu().findItem(R.id.reset);
        SpannableString s = new SpannableString(item_reset.getTitle());

        s.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER), 0, s.length(), 0);

        item_reset.setTitle(s);

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                if (item.getTitle().toString().equalsIgnoreCase("RESET")) {
                    customDialogForResetSearchList();
                } else {
                    customDialog(item.getTitle().toString());
                }

                return true;
            }
        });

        popup.show(); //showing popup menu
    }

    private void customDialog(final String title) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.advance_search_popup, null);
        builder.setView(dialogView);
        builder.setTitle("Criminal Record System");

        sp_age = (Spinner) dialogView.findViewById(R.id.sp_age);
        btn_done = (Button) dialogView.findViewById(R.id.btn_done);
        et_search = (EditText) dialogView.findViewById(R.id.et_search);
        tv_search = (TextView) dialogView.findViewById(R.id.tv_search);
        tv_identityMarkSubCategory = (TextView) dialogView.findViewById(R.id.tv_identityMarkSubCategory);
        sp_year = (Spinner) dialogView.findViewById(R.id.sp_year);
        tv_subClass = (TextView) dialogView.findViewById(R.id.tv_subClass);

        et_search.setVisibility(View.GONE);
        tv_search.setVisibility(View.GONE);
        tv_identityMarkSubCategory.setVisibility(View.GONE);
        btn_done.setVisibility(View.GONE);
        sp_age.setVisibility(View.GONE);
        sp_year.setVisibility(View.GONE);
        tv_subClass.setVisibility(View.GONE);

        if (title.equalsIgnoreCase("Name") || title.equalsIgnoreCase("Alias") || title.equalsIgnoreCase("Address") || title.equalsIgnoreCase("Brief Fact") || title.equalsIgnoreCase("Father's Name")) {
            et_search.setVisibility(View.VISIBLE);
            tv_search.setVisibility(View.GONE);
            tv_identityMarkSubCategory.setVisibility(View.GONE);
            et_search.setHint(title);
            btn_done.setVisibility(View.VISIBLE);
        }
        else if (title.equalsIgnoreCase("Age")) {
            btn_done.setVisibility(View.VISIBLE);
            et_search.setVisibility(View.GONE);
            tv_search.setVisibility(View.GONE);
            sp_age.setVisibility(View.VISIBLE);
            sp_year.setVisibility(View.GONE);

            setAgeData();
        }
        else if (title.equalsIgnoreCase("Arrest Year")) {
            btn_done.setVisibility(View.VISIBLE);
            et_search.setVisibility(View.GONE);
            tv_search.setVisibility(View.GONE);
            sp_age.setVisibility(View.GONE);
            sp_year.setVisibility(View.VISIBLE);

            setYearData();
        }
      /*  else if(title.equalsIgnoreCase("Arrest From Date") || title.equalsIgnoreCase("Arrest To Date")){
            btn_done.setVisibility(View.VISIBLE);
            tv_search.setVisibility(View.VISIBLE);
            tv_search.setText(title);
        }*/


        else {
            if (title.equalsIgnoreCase("Mark Category")) {
                btn_done.setVisibility(View.GONE);
            } else {
                btn_done.setVisibility(View.VISIBLE);

            }
            et_search.setVisibility(View.GONE);
            tv_search.setVisibility(View.VISIBLE);
            tv_search.setText(title);



            tv_search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (title.equalsIgnoreCase("Police Station")) {

                        policeStationString = "";
                        tv_search.setText(title);

                        if (obj_policeStationList.size() > 0) {

                            showSelectPoliceStationsDialog();
                        } else {
                            showAlertDialog(SearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                        }
                    }

                    if (title.equalsIgnoreCase("Crime Category")) {

                        crimeCategoryString = "";
                        tv_search.setText(title);
                        crimeCategory_status = true;

                        if (obj_categoryCrimeList2.size() > 0) {

                            customDialogAdapterForCategoryCrimeList2 = new CustomDialogAdapterForCategoryCrimeList2(SearchResultActivity.this, obj_categoryCrimeList2);
                            customDialogForCategoryCrimeList();

                        } else {
                            showAlertDialog(SearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                        }
                    }

                    if (title.equalsIgnoreCase("Modus Operandi")) {

                        modusOperandiString = "";
                        tv_search.setText(title);
                        modusOperandi_status = true;

                        if (obj_modusOperandiList.size() > 0) {

                            customDialogAdapterForModusOperandi = new CustomDialogAdapterForModusOperandi(SearchResultActivity.this, obj_modusOperandiList);
                            customDialogForModusOperandi();
                        } else {
                            showAlertDialog(SearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                        }
                    }

                    if (title.equalsIgnoreCase("Mark Category")) {

                        identifyCategorySubCategoryString = "";
                        btn_done.setVisibility(View.GONE);

                        if (CriminalSearchFragment.identityCategoryList.size() > 0) {
                            selectedIdentifyCategory.clear();
                            identifyCategoryString = "";
                            tv_search.setText("Mark Category");
                            tv_identityMarkSubCategory.setVisibility(View.GONE);
                            showSelectIdentifyCategoryDialog();
                        } else {
                            showAlertDialog(SearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                        }

                    }

                    if (title.equalsIgnoreCase("Class")) {

                        subClassValue = "";
                        btn_done.setVisibility(View.GONE);

                        if (CriminalSearchFragment.classArrayList.size() > 0) {
                            selectedClassList.clear();
                            classValue = "";
                            tv_search.setText("Select Class");
                            tv_subClass.setVisibility(View.GONE);
                            showSelectClassDialog();
                        } else {
                            showAlertDialog(SearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                        }

                    }

                    if(title.equalsIgnoreCase("Arrest From Date") || title.equalsIgnoreCase("Arrest To Date")){

                        DateUtils.setDate(SearchResultActivity.this, tv_search, true);
                    }

                }
            });

            if (title.equalsIgnoreCase("Mark Category")) {

                tv_identityMarkSubCategory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fetchIdentitySubCategory(identifyCategoryString);
                    }
                });
            }

            if (title.equalsIgnoreCase("Class")) {

                tv_search.setText("Select Class");
                btn_done.setVisibility(View.GONE);
                tv_subClass.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        fetchSubClass(classStringShow);
                    }
                });

            }


        }

        final AlertDialog alertDialog = builder.show();
        alertDialog.setCanceledOnTouchOutside(false);


        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (alertDialog != null && alertDialog.isShowing()) {

                    if (title.equalsIgnoreCase("Name")) {

                        if (!et_search.getText().toString().trim().equalsIgnoreCase("")) {

                            alertDialog.dismiss();
                            advanceSearch(title);
                        } else {
                            Utility.showToast(SearchResultActivity.this, "Please provide a name", "long");// 3rd parameter for duration of message like long or short
                        }
                    }

                    if (title.equalsIgnoreCase("Father's Name")) {

                        if (!et_search.getText().toString().trim().equalsIgnoreCase("")) {

                            alertDialog.dismiss();
                            advanceSearch(title);
                        } else {
                            Utility.showToast(SearchResultActivity.this, "Please provide a father's name", "long");// 3rd parameter for duration of message like long or short
                        }
                    }

                    if (title.equalsIgnoreCase("Arrest Year")) {
                        caseYr = sp_year.getSelectedItem().toString().replace("Enter Year", "");

                        if (!caseYr.equalsIgnoreCase("")) {
                            alertDialog.dismiss();
                            advanceSearch(title);
                        } else {
                            Utility.showToast(SearchResultActivity.this, "Please select an year", "long");// 3rd parameter for duration of message like long or short
                        }
                    }

                    if (title.equalsIgnoreCase("Alias")) {

                        if (!et_search.getText().toString().trim().equalsIgnoreCase("")) {

                            alertDialog.dismiss();
                            advanceSearch(title);
                        } else {
                            Utility.showToast(SearchResultActivity.this, "Please provide a alias", "long");// 3rd parameter for duration of message like long or short
                        }
                    }

                    if (title.equalsIgnoreCase("Address")) {

                        if (!et_search.getText().toString().trim().equalsIgnoreCase("")) {

                            alertDialog.dismiss();
                            advanceSearch(title);
                        } else {
                            Utility.showToast(SearchResultActivity.this, "Please provide an address", "long");// 3rd parameter for duration of message like long or short
                        }
                    }

                    if (title.equalsIgnoreCase("Brief Fact")) {

                        if (!et_search.getText().toString().trim().equalsIgnoreCase("")) {

                            alertDialog.dismiss();
                            advanceSearch(title);
                        } else {
                            Utility.showToast(SearchResultActivity.this, "Please provide a brief fact", "long");// 3rd parameter for duration of message like long or short
                        }
                    }

                    if (title.equalsIgnoreCase("Police Station")) {

                        if (!policeStationString.equalsIgnoreCase("")) {

                            alertDialog.dismiss();
                            advanceSearch(title);
                        } else {
                            Utility.showToast(SearchResultActivity.this, "Please select police station", "long");// 3rd parameter for duration of message like long or short
                        }
                    }
                    if (title.equalsIgnoreCase("Crime Category")) {

                        if (!crimeCategoryString.equalsIgnoreCase("")) {

                            alertDialog.dismiss();
                            advanceSearch(title);
                        } else {
                            Utility.showToast(SearchResultActivity.this, "Please select crime category", "long");// 3rd parameter for duration of message like long or short
                        }
                    }

                    if (title.equalsIgnoreCase("Modus Operandi")) {

                        if (!modusOperandiString.equalsIgnoreCase("")) {

                            alertDialog.dismiss();
                            advanceSearch(title);
                        } else {
                            Utility.showToast(SearchResultActivity.this, "Please select modus operandi", "long");// 3rd parameter for duration of message like long or short
                        }
                    }

                    if (title.equalsIgnoreCase("Mark Category")) {

                        if (!identifyCategorySubCategoryString.equalsIgnoreCase("")) {

                            alertDialog.dismiss();
                            advanceSearch(title);
                        } else {
                            Utility.showToast(SearchResultActivity.this, "Please select mark category and sub category", "long");// 3rd parameter for duration of message like long or short
                        }
                    }

                    if (title.equalsIgnoreCase("Age")) {
                        age = sp_age.getSelectedItem().toString().replace("Select Age Range", "");

                        if (!age.equalsIgnoreCase("")) {
                            alertDialog.dismiss();
                            advanceSearch(title);
                        } else {
                            Utility.showToast(SearchResultActivity.this, "Please select an age range", "long");// 3rd parameter for duration of message like long or short
                        }
                    }


                    if (title.equalsIgnoreCase("Class")) {

                        if (!classValue.equalsIgnoreCase("")) {

                            alertDialog.dismiss();
                            advanceSearch(title);
                        } else {
                            Utility.showToast(SearchResultActivity.this, "Please select class and sub class", "long");// 3rd parameter for duration of message like long or short
                        }
                    }

                    if (title.equalsIgnoreCase("Arrest From Date")) {

                        if (!tv_search.getText().toString().trim().equalsIgnoreCase("")) {

                            alertDialog.dismiss();
                            advanceSearch(title);
                        } else {
                            Utility.showToast(SearchResultActivity.this, "Please select a date", "long");// 3rd parameter for duration of message like long or short
                        }
                    }

                    if (title.equalsIgnoreCase("Arrest To Date")) {

                        if (!tv_search.getText().toString().trim().equalsIgnoreCase("")) {

                            alertDialog.dismiss();
                            advanceSearch(title);
                        } else {
                            Utility.showToast(SearchResultActivity.this, "Please select a date", "long");// 3rd parameter for duration of message like long or short
                        }
                    }

                }
            }
        });

    }

    private void advanceSearch(String item_value) {

        advance_search_title = item_value;

        name = criminalSearchData.getName();
        alias = criminalSearchData.getAlias_name();
        address = criminalSearchData.getAddress();
        age = criminalSearchData.getAge();
        brief_fact = criminalSearchData.getBriefFact();

        fName = criminalSearchData.getfName();
        caseYr = criminalSearchData.getCaseYr();
        fromDt =  criminalSearchData.getFromDt();
        toDt = criminalSearchData.getToDt();
        //classValue = criminalSearchData.getClassValue();


        if (item_value.equalsIgnoreCase("Name")) {
            name = et_search.getText().toString().trim();
        } else if (item_value.equalsIgnoreCase("Alias")) {
            alias = et_search.getText().toString().trim();
        } else if (item_value.equalsIgnoreCase("Address")) {
            address = et_search.getText().toString().trim();
        } else if (item_value.equalsIgnoreCase("Age")) {
            age = sp_age.getSelectedItem().toString().trim();
        } else if (item_value.equalsIgnoreCase("Brief Fact")) {
            brief_fact = et_search.getText().toString().trim();
        } else if (item_value.equalsIgnoreCase("Father's Name")) {
            fName = et_search.getText().toString().trim();
        } else if (item_value.equalsIgnoreCase("Arrest Year")) {
            caseYr = sp_year.getSelectedItem().toString().trim();
        } else if (item_value.equalsIgnoreCase("Arrest From Date")) {
            fromDt = tv_search.getText().toString().trim();
        } else if (item_value.equalsIgnoreCase("Arrest To Date")) {
            toDt = tv_search.getText().toString().trim();
        }

        if(!alreadySearchClass.equalsIgnoreCase("") || !alreadySearchSubClass.equalsIgnoreCase("")){
            classValue = alreadySearchClass;
            subClassValue = alreadySearchSubClass;
        }

        if (!alreadySearchModusOperandi.equalsIgnoreCase("")) {
            modusOperandiString = alreadySearchModusOperandi;
        }

        if (!alreadySearchCategoryOfCrime.equalsIgnoreCase("")) {
            crimeCategoryString = alreadySearchCategoryOfCrime;
        }

        if (!alreadySearchPSString.equalsIgnoreCase("")) {
            policeStationString = alreadySearchPSString;
        }

        if (!alreadySearchModifiedCategoryString.equalsIgnoreCase("")) {
            identifyCategorySubCategoryString = alreadySearchModifiedCategoryString;
        }


        if (!name.equalsIgnoreCase("") || !alias.equalsIgnoreCase("") || !crimeCategoryString.equalsIgnoreCase("") || !modusOperandiString.equalsIgnoreCase("") || !policeStationString.equalsIgnoreCase("") || !identifyCategorySubCategoryString.equalsIgnoreCase("") || !age.equalsIgnoreCase("") || !address.equalsIgnoreCase("") || !brief_fact.equalsIgnoreCase("")
                || !fName.equalsIgnoreCase("") || !classValue.equalsIgnoreCase("") || !subClassValue.equalsIgnoreCase("")
                || !fromDt.equalsIgnoreCase("") || !toDt.equalsIgnoreCase("") || !caseYr.equalsIgnoreCase("")) {

            if (!Constants.internetOnline(this)) {
                Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
                return;
            }

            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_CRIMINAL_SEARCH);
            taskManager.setAdvanceCriminalSearch(true);

            String[] keys = {"name_accused", "father_accused", "from_date", "to_date", "case_yr", "policestations",
                    "criminal_aliases_name", "pageno", "identitycategory", "crimecategory", "moduslist", "age_accused", "address", "brief_keyword", "class", "subclass", "user_id"};

            String[] values = {name.replace("reset_name", "").trim(), fName.replace("reset_fName", "").trim(), fromDt.replace("reset_from_date", "").trim(), toDt.replace("reset_to_date", "").trim(), caseYr.replace("reset_year", "").trim(), policeStationString.replace("reset_ps", "").trim(), alias.replace("reset_alias", "").trim(), "1", identifyCategorySubCategoryString.replace("reset_markCategory", "").trim(), crimeCategoryString.replace("reset_categoryOfCrime", "").trim(), modusOperandiString.replace("reset_modusOperandi", "").trim(), age.replace("reset_age", "").trim(), address.replace("reset_address", "").trim(), brief_fact.replace("reset_brief_fact", "").trim(), classValue.replace("reset_class", "").trim(), subClassValue.replace("reset_subClass", "").trim(),userId};

            taskManager.doStartTask(keys, values, true);

        }

    }

    private void ResetSearch(String item_value) {

        String[] resultString = item_value.split(",");

        for (String a : resultString) {

            if (a.contains("reset_alias")) {
                alias = a.replace("reset_alias", "");
            } else if (a.contains("reset_ps")) {
                policeStationString = a.replace("reset_ps", "");
            } else if (a.contains("reset_name")) {
                name = a.replace("reset_name", "");
            } else if (a.contains("reset_categoryOfCrime")) {
                crimeCategoryString = a.replace("reset_categoryOfCrime", "");
            } else if (a.contains("reset_modusOperandi")) {
                modusOperandiString = a.replace("reset_modusOperandi", "");
            } else if (a.contains("reset_markCategory")) {
                identifyCategorySubCategoryString = a.replace("reset_markCategory", "");
            } else if (a.contains("reset_age")) {
                age = a.replace("reset_age", "");
            } else if (a.contains("reset_address")) {
                address = a.replace("reset_address", "");
            } else if (a.contains("reset_brief_fact")) {
                brief_fact = a.replace("reset_brief_fact", "");
            } else if (a.contains("reset_fName")) {
                fName = a.replace("reset_fName", "");
            } else if (a.contains("reset_year")) {
                caseYr = a.replace("reset_year", "");
            } else if (a.contains("reset_class") ) {
                classValue = a.replace("reset_class", "");
            } else if (a.contains("reset_subClass")) {
                subClassValue = a.replace("reset_subClass", "");
            } else if (a.contains("reset_from_date")) {
                fromDt = a.replace("reset_from_date", "");
            }else if (a.contains("reset_to_date")) {
                toDt = a.replace("reset_to_date", "");
            }
        }


        if (!name.equalsIgnoreCase("") || !alias.equalsIgnoreCase("") || !crimeCategoryString.equalsIgnoreCase("") || !modusOperandiString.equalsIgnoreCase("") || !policeStationString.equalsIgnoreCase("") || !identifyCategorySubCategoryString.equalsIgnoreCase("") || !age.equalsIgnoreCase("") || !address.equalsIgnoreCase("") || !brief_fact.equalsIgnoreCase("")
                || !fName.equalsIgnoreCase("") || !classValue.equalsIgnoreCase("") || !subClassValue.equalsIgnoreCase("")
                || !fromDt.equalsIgnoreCase("") || !toDt.equalsIgnoreCase("") || !caseYr.equalsIgnoreCase("")) {


            if (!Constants.internetOnline(this)) {
                Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
                return;
            }

            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_CRIMINAL_SEARCH);
            taskManager.setAdvanceCriminalSearch(true);


            String[] keys = {"name_accused", "father_accused", "from_date", "to_date", "case_yr", "policestations",
                    "criminal_aliases_name", "pageno", "identitycategory", "crimecategory", "moduslist", "age_accused", "address", "brief_keyword", "class", "subclass","user_id"};


            String[] values = {name.replace("reset_name", "").trim(), fName.replace("reset_fName", "").trim(), fromDt.replace("reset_from_date", "").trim(), toDt.replace("reset_to_date", "").trim(), caseYr.replace("reset_year", "").trim(), policeStationString.replace("reset_ps", "").trim(), alias.replace("reset_alias", "").trim(), "1", identifyCategorySubCategoryString.replace("reset_markCategory", "").trim(), crimeCategoryString.replace("reset_categoryOfCrime", "").trim(), modusOperandiString.replace("reset_modusOperandi", "").trim(), age.replace("reset_age", "").trim(), address.replace("reset_address", "").trim(), brief_fact.replace("reset_brief_fact", "").trim(), classValue.replace("reset_class", "").trim(), subClassValue.replace("reset_subClass", "").trim(), userId};

            taskManager.doStartTask(keys, values, true);

        }

    }

    public void parseAdvanceSearchResult(String jsonStr, String[] keys, String[] values) {

        System.out.println("Advance Search Title " + advance_search_title);
        System.out.println("Advance Search Response: " + jsonStr);

        System.out.println("IdentifyString: " + identifyCategorySubCategoryString);

        CriminalSearchData criminalSearchData = new CriminalSearchData();

        criminalSearchData.setName(name.replace("reset_name", ""));
        criminalSearchData.setAge(age.replace("reset_age", ""));
        criminalSearchData.setAddress(address.replace("reset_address", ""));
        criminalSearchData.setBriefFact(brief_fact.replace("reset_brief_fact", ""));
        criminalSearchData.setPs_code(policeStationString.replace("reset_ps", ""));
        criminalSearchData.setAlias_name(alias.replace("reset_alias", ""));
        criminalSearchData.setCategoryOfCrimeString(crimeCategoryString.replace("reset_categoryOfCrime", ""));
        criminalSearchData.setModusOperandiString(modusOperandiString.replace("reset_modusOperandi", ""));
        criminalSearchData.setModified_categoryString(identifyCategorySubCategoryString.replace("reset_markCategory", ""));

        criminalSearchData.setfName(fName.replace("reset_fName", ""));
        criminalSearchData.setFromDt(fromDt.replace("reset_from_date", ""));
        criminalSearchData.setToDt(toDt.replace("reset_to_date", ""));
        criminalSearchData.setCaseYr(caseYr.replace("reset_year", ""));
        criminalSearchData.setClassStr(classValue.replace("reset_class", ""));
        criminalSearchData.setSubClassStr(subClassValue.replace("reset_subClass", ""));


        this.criminalSearchData = criminalSearchData;

        try {
            if (jsonStr != null && !jsonStr.equals("")) {

                JSONObject jObj = new JSONObject(jsonStr);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {
                    pageno = jObj.opt("pageno").toString();
                    totalResult = jObj.opt("totalresult").toString();

                    JSONArray jsonArray = jObj.optJSONArray("result");
                    ArrayList<CriminalDetails> criminalList = Utility
                            .parseCrimiinalRecords(SearchResultActivity.this, jsonArray);
                    if (criminalList != null && criminalList.size() > 0) {

                        this.criminalList = criminalList;
                        this.keys = keys;
                        this.values = values;

                        adapter = new CriminalAdapter(this, R.layout.search_listitem_layout,
                                R.id.rl_container, this.criminalList);

                        lv_criminals.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                        if (totalResult.equalsIgnoreCase("1")) {
                            tv_resultCount.setText("Total Result: " + totalResult);
                        } else {
                            tv_resultCount.setText("Total Results: " + totalResult);
                        }


                    } else {
                        Toast.makeText(SearchResultActivity.this, "No result found",
                                Toast.LENGTH_LONG).show();
                    }

                    int currentPosition = lv_criminals.getFirstVisiblePosition();
                    int lastVisiblePosition = lv_criminals.getLastVisiblePosition();

                    // Setting new scroll position
                    lv_criminals.setSelectionFromTop(currentPosition + 1, 0);

                    if ((Integer.parseInt(totalResult) - (currentPosition + 1)) < 20) {
                        lv_criminals.removeFooterView(btnLoadMore);
                    } else {
                        if (lv_criminals.getFooterViewsCount() == 0) {
                            lv_criminals.addFooterView(btnLoadMore);
                        }

                    }

                } else {

                    if (advance_search_title.equalsIgnoreCase("Modus Operandi")) {

                        modusOperandiString = "";
                        criminalSearchData.setModusOperandiString("reset_modusOperandi");
                        flag_modusOperandi = false;
                    }

                    if (advance_search_title.equalsIgnoreCase("Name")) {

                        name = "";
                        criminalSearchData.setName("reset_name");
                        flag_name = false;
                    }

                    if (advance_search_title.equalsIgnoreCase("Father's Name")) {

                        fName = "";
                        criminalSearchData.setfName("reset_fName");
                        flag_fName = false;
                    }

                    if (advance_search_title.equalsIgnoreCase("Class")) {

                        classStringShow = "";
                        subClassStringShow = "";
                        classValue = "";
                        subClassValue = "";
                        criminalSearchData.setClassStr("reset_class");
                        criminalSearchData.setSubClassStr("reset_subClass");
                        flag_class = false;
                    }

                    if (advance_search_title.equalsIgnoreCase("Arrest From Date")) {

                        fromDt = "";
                        criminalSearchData.setFromDt("reset_from_date");
                        flag_fromDt = false;
                    }

                    if (advance_search_title.equalsIgnoreCase("Arrest To Date")) {

                        toDt = "";
                        criminalSearchData.setToDt("reset_to_date");
                        flag_toDt = false;
                    }

                    if (advance_search_title.equalsIgnoreCase("Arrest Year")) {

                        caseYr = "";
                        criminalSearchData.setCaseYr("reset_year");
                        flag_caseYr = false;
                    }

                    if (advance_search_title.equalsIgnoreCase("Age")) {

                        age = "";
                        criminalSearchData.setAge("reset_age");
                        flag_age = false;
                    }

                    if (advance_search_title.equalsIgnoreCase("Address")) {

                        address = "";
                        criminalSearchData.setAddress("reset_address");
                        flag_address = false;
                    }

                    if (advance_search_title.equalsIgnoreCase("Brief Fact")) {

                        brief_fact = "";
                        criminalSearchData.setBriefFact("reset_brief_fact");
                        flag_brief_fact = false;
                    }

                    if (advance_search_title.equalsIgnoreCase("Alias")) {

                        alias = "";
                        criminalSearchData.setAlias_name("reset_alias");
                        flag_alias = false;

                    }

                    if (advance_search_title.equalsIgnoreCase("Police Station")) {
                        policeStationString = "";
                        criminalSearchData.setPs_code("reset_ps");
                        flag_policeStation = false;
                    }

                    if (advance_search_title.equalsIgnoreCase("Crime Category")) {
                        crimeCategoryString = "";
                        criminalSearchData.setCategoryOfCrimeString("reset_categoryOfCrime");
                        flag_categoryOfCrime = false;
                    }

                    if (advance_search_title.equalsIgnoreCase("Mark Category")) {
                        identifyCategorySubCategoryString = "";
                        criminalSearchData.setModified_categoryString("reset_markCategory");
                        flag_modifiedCategory = false;
                    }

                    showAlertDialog(SearchResultActivity.this, " No Data Found ", "Sorry, no matching found, please reset your data from above dropdown list.", false);

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            showAlertDialog(SearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
        } catch (OutOfMemoryError outOfMemoryError) {
            outOfMemoryError.printStackTrace();
            showAlertDialog(SearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
        }

    }

    private void customDialogForResetSearchList() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.reset_search_popup, null);
        builder.setView(dialogView);

        chk_tv_name = (CheckedTextView) dialogView.findViewById(R.id.chk_tv_name);
        chk_tv_age = (CheckedTextView) dialogView.findViewById(R.id.chk_tv_age);
        chk_tv_address = (CheckedTextView) dialogView.findViewById(R.id.chk_tv_address);
        chk_tv_ps = (CheckedTextView) dialogView.findViewById(R.id.chk_tv_ps);
        chk_tv_alias = (CheckedTextView) dialogView.findViewById(R.id.chk_tv_alias);
        chk_tv_markCategory = (CheckedTextView) dialogView.findViewById(R.id.chk_tv_markCategory);
        chk_tv_crimeCategory = (CheckedTextView) dialogView.findViewById(R.id.chk_tv_crimeCategory);
        chk_tv_modus = (CheckedTextView) dialogView.findViewById(R.id.chk_tv_modus);
        chk_tv_brief_fact = (CheckedTextView) dialogView.findViewById(R.id.chk_tv_brief_fact);

        chk_tv_fName = (CheckedTextView) dialogView.findViewById(R.id.chk_tv_fName);
        chk_tv_class = (CheckedTextView) dialogView.findViewById(R.id.chk_tv_class);
        chk_tv_year = (CheckedTextView) dialogView.findViewById(R.id.chk_tv_year);
        chk_tv_fromDate = (CheckedTextView) dialogView.findViewById(R.id.chk_tv_fromDate);
        chk_tv_toDate = (CheckedTextView) dialogView.findViewById(R.id.chk_tv_toDate);


        btn_reset_search_done = (Button) dialogView.findViewById(R.id.btn_reset_search_done);

        if (!alreadySearchAliasName.equalsIgnoreCase("")) {
            chk_tv_alias.setVisibility(View.GONE);
            flag_alias = false;
        } else {
            chk_tv_alias.setVisibility(View.VISIBLE);
            chk_tv_alias.setEnabled(false);

            if (!criminalSearchData.getAlias_name().equalsIgnoreCase("")) {
                chk_tv_alias.setChecked(true);
                chk_tv_alias.setEnabled(true);
            } else {
                if (!flag_alias) {
                    chk_tv_alias.setChecked(true);
                    chk_tv_alias.setEnabled(true);
                }
            }
        }

        if (!alreadySearchAddress.equalsIgnoreCase("")) {
            chk_tv_address.setVisibility(View.GONE);
            flag_address = false;
        } else {
            chk_tv_address.setVisibility(View.VISIBLE);
            chk_tv_address.setEnabled(false);

            if (!criminalSearchData.getAddress().equalsIgnoreCase("")) {
                chk_tv_address.setChecked(true);
                chk_tv_address.setEnabled(true);
            } else {
                if (!flag_address) {
                    chk_tv_address.setChecked(true);
                    chk_tv_address.setEnabled(true);
                }
            }
        }

        if (!alreadySearchBriefFact.equalsIgnoreCase("")) {
            chk_tv_brief_fact.setVisibility(View.GONE);
            flag_brief_fact = false;
        } else {
            chk_tv_brief_fact.setVisibility(View.VISIBLE);
            chk_tv_brief_fact.setEnabled(false);

            if (!criminalSearchData.getBriefFact().equalsIgnoreCase("")) {
                chk_tv_brief_fact.setChecked(true);
                chk_tv_brief_fact.setEnabled(true);
            } else {
                if (!flag_brief_fact) {
                    chk_tv_brief_fact.setChecked(true);
                    chk_tv_brief_fact.setEnabled(true);
                }
            }
        }

        if (!alreadySearchPSString.equalsIgnoreCase("")) {
            chk_tv_ps.setVisibility(View.GONE);
            flag_policeStation = false;
        } else {
            chk_tv_ps.setVisibility(View.VISIBLE);
            chk_tv_ps.setEnabled(false);
            if (!criminalSearchData.getPs_code().equalsIgnoreCase("")) {
                chk_tv_ps.setChecked(true);
                chk_tv_ps.setEnabled(true);
            } else {
                if (!flag_policeStation) {
                    chk_tv_ps.setChecked(true);
                    chk_tv_ps.setEnabled(true);
                }
            }
        }

        if (!alreadySearchName.equalsIgnoreCase("")) {
            chk_tv_name.setVisibility(View.GONE);
            flag_name = false;
        } else {
            chk_tv_name.setVisibility(View.VISIBLE);
            chk_tv_name.setEnabled(false);

            if (!criminalSearchData.getName().equalsIgnoreCase("")) {
                chk_tv_name.setChecked(true);
                chk_tv_name.setEnabled(true);
            } else {
                if (!flag_name) {
                    chk_tv_name.setChecked(true);
                    chk_tv_name.setEnabled(true);
                }
            }
        }

        //new check fields Added

        if (!alreadySearchFname.equalsIgnoreCase("")) {
            chk_tv_fName.setVisibility(View.GONE);
            flag_fName = false;
        } else {
            chk_tv_fName.setVisibility(View.VISIBLE);
            chk_tv_fName.setEnabled(false);

            if (!criminalSearchData.getfName().equalsIgnoreCase("")) {
                chk_tv_fName.setChecked(true);
                chk_tv_fName.setEnabled(true);
            } else {
                if (!flag_fName) {
                    chk_tv_fName.setChecked(true);
                    chk_tv_fName.setEnabled(true);
                }
            }
        }

        if (!alreadySearchCaseYr.equalsIgnoreCase("")) {
            chk_tv_year.setVisibility(View.GONE);
            flag_caseYr = false;
        } else {
            chk_tv_year.setVisibility(View.VISIBLE);
            chk_tv_year.setEnabled(false);

            if (!criminalSearchData.getCaseYr().equalsIgnoreCase("")) {
                chk_tv_year.setChecked(true);
                chk_tv_year.setEnabled(true);
            } else {
                if (!flag_caseYr) {
                    chk_tv_year.setChecked(true);
                    chk_tv_year.setEnabled(true);
                }
            }
        }

        if (!alreadySearchClass.equalsIgnoreCase("") /*|| !alreadySearchSubClass.equalsIgnoreCase("")*/) {
            chk_tv_class.setVisibility(View.GONE);
            flag_class = false;
        } else {
            chk_tv_class.setVisibility(View.VISIBLE);
            chk_tv_class.setEnabled(false);

            if (!criminalSearchData.getClassStr().equalsIgnoreCase("")) {

                chk_tv_class.setChecked(true);
                chk_tv_class.setEnabled(true);
            } else {
                if (!flag_class) {
                    chk_tv_class.setChecked(true);
                    chk_tv_class.setEnabled(true);
                }
            }
        }

        if (!alreadySearchFromDt.equalsIgnoreCase("")) {
            chk_tv_fromDate.setVisibility(View.GONE);
            flag_fromDt = false;

        } else {
            chk_tv_fromDate.setVisibility(View.VISIBLE);
            chk_tv_fromDate.setEnabled(false);

            if (!criminalSearchData.getFromDt().equalsIgnoreCase("")) {
                chk_tv_fromDate.setChecked(true);
                chk_tv_fromDate.setEnabled(true);
            } else {
                if (!flag_fromDt) {
                    chk_tv_fromDate.setChecked(true);
                    chk_tv_fromDate.setEnabled(true);
                }
            }
        }

        if (!alreadySearchToDt.equalsIgnoreCase("")) {
            chk_tv_toDate.setVisibility(View.GONE);
            flag_toDt = false;

        } else {
            chk_tv_toDate.setVisibility(View.VISIBLE);
            chk_tv_toDate.setEnabled(false);

            if (!criminalSearchData.getToDt().equalsIgnoreCase("")) {
                chk_tv_toDate.setChecked(true);
                chk_tv_toDate.setEnabled(true);
            } else {
                if (!flag_toDt) {
                    chk_tv_toDate.setChecked(true);
                    chk_tv_toDate.setEnabled(true);
                }
            }
        }
        // End

        if (!alreadySearchAge.equalsIgnoreCase("")) {
            chk_tv_age.setVisibility(View.GONE);
            flag_age = false;
        } else {
            chk_tv_age.setVisibility(View.VISIBLE);
            chk_tv_age.setEnabled(false);

            if (!criminalSearchData.getAge().equalsIgnoreCase("")) {
                chk_tv_age.setChecked(true);
                chk_tv_age.setEnabled(true);
            } else {
                if (!flag_age) {
                    chk_tv_age.setChecked(true);
                    chk_tv_age.setEnabled(true);
                }
            }
        }


        if (!alreadySearchCategoryOfCrime.equalsIgnoreCase("")) {
            chk_tv_crimeCategory.setVisibility(View.GONE);
        } else {
            chk_tv_crimeCategory.setVisibility(View.VISIBLE);
            chk_tv_crimeCategory.setEnabled(false);

            if (!criminalSearchData.getCategoryOfCrimeString().equalsIgnoreCase("")) {
                chk_tv_crimeCategory.setChecked(true);
                chk_tv_crimeCategory.setEnabled(true);
            } else {
                if (!flag_categoryOfCrime) {
                    chk_tv_crimeCategory.setChecked(true);
                    chk_tv_crimeCategory.setEnabled(true);
                }
            }
        }


        if (!alreadySearchModusOperandi.equalsIgnoreCase("")) {
            chk_tv_modus.setVisibility(View.GONE);
            flag_modusOperandi = false;
        } else {
            chk_tv_modus.setVisibility(View.VISIBLE);
            chk_tv_modus.setEnabled(false);

            if (!criminalSearchData.getModusOperandiString().equalsIgnoreCase("")) {
                chk_tv_modus.setChecked(true);
                chk_tv_modus.setEnabled(true);
            } else {
                if (!flag_modusOperandi) {
                    chk_tv_modus.setChecked(true);
                    chk_tv_modus.setEnabled(true);
                }
            }
        }

        if (!alreadySearchModifiedCategoryString.equalsIgnoreCase("")) {
            chk_tv_markCategory.setVisibility(View.GONE);
            flag_modifiedCategory = false;
        } else {
            chk_tv_markCategory.setVisibility(View.VISIBLE);
            chk_tv_markCategory.setEnabled(false);

            if (!criminalSearchData.getModified_categoryString().equalsIgnoreCase("")) {
                chk_tv_markCategory.setChecked(true);
                chk_tv_markCategory.setEnabled(true);
            } else {
                if (!flag_modifiedCategory) {
                    chk_tv_markCategory.setChecked(true);
                    chk_tv_markCategory.setEnabled(true);
                }
            }
        }


        final AlertDialog alertDialog = builder.show();
        alertDialog.setCanceledOnTouchOutside(false);

        btn_reset_search_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!chk_tv_name.isChecked() && !chk_tv_fName.isChecked() && !chk_tv_class.isChecked() && !chk_tv_year.isChecked() && !chk_tv_fromDate.isChecked() && !chk_tv_toDate.isChecked() && !chk_tv_alias.isChecked() && !chk_tv_ps.isChecked() && !chk_tv_crimeCategory.isChecked() && !chk_tv_markCategory.isChecked() && !chk_tv_modus.isChecked() && !chk_tv_age.isChecked() && !chk_tv_address.isChecked() && !chk_tv_brief_fact.isChecked()) {

                  Utility.showToast(SearchResultActivity.this, "Please check atleast one category", "long");

                }

                else if (alertDialog != null && alertDialog.isShowing()) {
                    getPreviousSearchData();
                    alertDialog.dismiss();

                    resetString = new StringBuilder("");

                    if (chk_tv_alias.isChecked()) {
                        chk_tv_alias.setChecked(false);
                        criminalSearchData.setAlias_name("");

                        if (!flag_alias) {
                            flag_alias = true;
                        }

                        //ResetSearch("reset_alias");
                        resetString.append("reset_alias");
                        resetString.append(",");

                    }


                    if (chk_tv_address.isChecked()) {
                        chk_tv_address.setChecked(false);
                        criminalSearchData.setAddress("");

                        if (!flag_address) {
                            flag_address = true;
                        }

                        //ResetSearch("reset_alias");
                        resetString.append("reset_address");
                        resetString.append(",");

                    }

                    if (chk_tv_brief_fact.isChecked()) {
                        chk_tv_brief_fact.setChecked(false);
                        criminalSearchData.setBriefFact("");

                        if (!flag_brief_fact) {
                            flag_brief_fact = true;
                        }

                        //ResetSearch("reset_alias");
                        resetString.append("reset_brief_fact");
                        resetString.append(",");

                    }

                    if (chk_tv_ps.isChecked()) {
                        chk_tv_ps.setChecked(false);
                        criminalSearchData.setPs_code("");

                        if (!flag_policeStation) {
                            flag_policeStation = true;
                        }

                        //ResetSearch("reset_ps");
                        resetString.append("reset_ps");
                        resetString.append(",");

                    }

                    if (chk_tv_name.isChecked()) {

                        chk_tv_name.setChecked(false);
                        criminalSearchData.setName("");
                        if (!flag_name) {
                            flag_name = true;
                        }
                        //ResetSearch("reset_name");
                        resetString.append("reset_name");
                        resetString.append(",");

                    }

                    if (chk_tv_fName.isChecked()) {

                        chk_tv_fName.setChecked(false);
                        criminalSearchData.setfName("");
                        if (!flag_fName) {
                            flag_fName = true;
                        }
                        //ResetSearch("reset_fName");
                        resetString.append("reset_fName");
                        resetString.append(",");

                    }

                    if (chk_tv_year.isChecked()) {

                        chk_tv_year.setChecked(false);
                        criminalSearchData.setCaseYr("");

                        if (!flag_caseYr) {
                            flag_caseYr = true;
                        }
                        //ResetSearch("reset_year");
                        resetString.append("reset_year");
                        resetString.append(",");

                    }

                    if (chk_tv_class.isChecked()) {

                        chk_tv_class.setChecked(false);
                        criminalSearchData.setClassStr("");
                        criminalSearchData.setSubClassStr("");

                        if (!flag_class) {
                            flag_class = true;
                        }
                        //ResetSearch("reset_name");
                        resetString.append("reset_class");
                        resetString.append(",");
                        resetString.append("reset_subClass");
                        resetString.append(",");

                    }

                    if (chk_tv_fromDate.isChecked()) {

                        chk_tv_fromDate.setChecked(false);
                        criminalSearchData.setFromDt("");
                        if (!flag_fromDt) {
                            flag_fromDt = true;
                        }
                        //ResetSearch("reset_name");
                        resetString.append("reset_from_date");
                        resetString.append(",");

                    }

                    if (chk_tv_toDate.isChecked()) {

                        chk_tv_toDate.setChecked(false);
                        criminalSearchData.setToDt("");
                        if (!flag_toDt) {
                            flag_toDt = true;
                        }
                        //ResetSearch("reset_name");
                        resetString.append("reset_to_date");
                        resetString.append(",");
                    }


                    if (chk_tv_age.isChecked()) {

                        chk_tv_age.setChecked(false);
                        criminalSearchData.setAge("");
                        if (!flag_age) {
                            flag_age = true;
                        }
                        //ResetSearch("reset_name");
                        resetString.append("reset_age");
                        resetString.append(",");

                    }

                    if (chk_tv_crimeCategory.isChecked()) {

                        chk_tv_crimeCategory.setChecked(false);
                        criminalSearchData.setCategoryOfCrimeString("");

                        if (!flag_categoryOfCrime) {
                            flag_categoryOfCrime = true;
                        }

                        //ResetSearch("reset_categoryOfCrime");
                        resetString.append("reset_categoryOfCrime");
                        resetString.append(",");

                    }

                    if (chk_tv_modus.isChecked()) {

                        chk_tv_modus.setChecked(false);

                        criminalSearchData.setModusOperandiString("");
                        if (!flag_modusOperandi) {
                            flag_modusOperandi = true;
                        }
                        //ResetSearch("reset_modusOperandi");
                        resetString.append("reset_modusOperandi");
                        resetString.append(",");

                    }

                    if (chk_tv_markCategory.isChecked()) {

                        chk_tv_markCategory.setChecked(false);

                        criminalSearchData.setModified_categoryString("");
                        if (!flag_modifiedCategory) {
                            flag_modifiedCategory = true;
                        }
                        //ResetSearch("reset_modusOperandi");
                        resetString.append("reset_markCategory");
                        resetString.append(",");

                    }

                    ResetSearch(resetString.toString());

                    getPreviousSearchData();


                }

            }
        });

        clickEventsForResetSearchPopup();
    }

    private void clickEventsForResetSearchPopup() {


        chk_tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tv_name.isChecked())
                    chk_tv_name.setChecked(false);
                else
                    chk_tv_name.setChecked(true);

            }
        });

        chk_tv_fName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tv_fName.isChecked())
                    chk_tv_fName.setChecked(false);
                else
                    chk_tv_fName.setChecked(true);

            }
        });

        chk_tv_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tv_year.isChecked())
                    chk_tv_year.setChecked(false);
                else
                    chk_tv_year.setChecked(true);

            }
        });

        chk_tv_fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tv_fromDate.isChecked())
                    chk_tv_fromDate.setChecked(false);
                else
                    chk_tv_fromDate.setChecked(true);

            }
        });

        chk_tv_toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tv_toDate.isChecked())
                    chk_tv_toDate.setChecked(false);
                else
                    chk_tv_toDate.setChecked(true);

            }
        });

        chk_tv_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tv_class.isChecked())
                    chk_tv_class.setChecked(false);
                else
                    chk_tv_class.setChecked(true);

            }
        });

        chk_tv_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tv_age.isChecked())
                    chk_tv_age.setChecked(false);
                else
                    chk_tv_age.setChecked(true);

            }
        });

        chk_tv_ps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tv_ps.isChecked())
                    chk_tv_ps.setChecked(false);
                else
                    chk_tv_ps.setChecked(true);

            }
        });

        chk_tv_alias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tv_alias.isChecked())
                    chk_tv_alias.setChecked(false);
                else
                    chk_tv_alias.setChecked(true);

            }
        });

        chk_tv_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tv_address.isChecked())
                    chk_tv_address.setChecked(false);
                else
                    chk_tv_address.setChecked(true);

            }
        });

        chk_tv_brief_fact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tv_brief_fact.isChecked())
                    chk_tv_brief_fact.setChecked(false);
                else
                    chk_tv_brief_fact.setChecked(true);

            }
        });

        chk_tv_markCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tv_markCategory.isChecked())
                    chk_tv_markCategory.setChecked(false);
                else
                    chk_tv_markCategory.setChecked(true);

            }
        });

        chk_tv_crimeCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tv_crimeCategory.isChecked())
                    chk_tv_crimeCategory.setChecked(false);
                else
                    chk_tv_crimeCategory.setChecked(true);

            }
        });

        chk_tv_modus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tv_modus.isChecked())
                    chk_tv_modus.setChecked(false);
                else
                    chk_tv_modus.setChecked(true);

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreviousSearchData();
    }


    /**
     * Function to display simple Alert Dialog
     *
     * @param context - application context
     * @param title   - alert dialog title
     * @param message - alert message
     * @param status  - success/failure (used to set icon)
     */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    //---------------------------------------------------------------------------------------------------------------//


	                                  /* Custom Dialog for Police Station */

    //---------------------------------------------------------------------------------------------------------------//

     /* This method shows Police Station list in a pop-up */

    protected void showSelectPoliceStationsDialog() {

        policeStationArrayList.clear();
        selectedPoliceStations.clear();
        selectedPoliceStationsId.clear();

        for (int i = 0; i < obj_policeStationList.size(); i++) {
            policeStationArrayList.add(obj_policeStationList.get(i).getPs_name());
        }

        array_policeStation = new String[policeStationArrayList.size()];
        array_policeStation = policeStationArrayList.toArray(array_policeStation);


        boolean[] checkedPoliceStations = new boolean[array_policeStation.length];
        int count = array_policeStation.length;

        for (int i = 0; i < count; i++) {
            checkedPoliceStations[i] = selectedPoliceStations.contains(array_policeStation[i]);
        }

        DialogInterface.OnMultiChoiceClickListener policeStationsDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedPoliceStations.add(array_policeStation[which]);
                    selectedPoliceStationsId.add(obj_policeStationList.get(which).getPs_code());
                } else {
                    selectedPoliceStations.remove(array_policeStation[which]);
                    selectedPoliceStationsId.remove(obj_policeStationList.get(which).getPs_code());
                }

//				onChangeSelectedPoliceStations();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(SearchResultActivity.this);
        builder.setTitle("Select Police Stations");
        builder.setMultiChoiceItems(array_policeStation, checkedPoliceStations, policeStationsDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedPoliceStations();

            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_search.setText("Select Police Stations");
                policeStationString = "";
                selectedPoliceStations.clear();
                selectedPoliceStationsId.clear();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedPoliceStations() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for (CharSequence policeStation : selectedPoliceStations) {
            stringBuilder.append(policeStation + ",");
        }

        for (CharSequence policeStation : selectedPoliceStationsId) {
            stringBuilderId.append("\'" + policeStation + "\',");
        }

        if (selectedPoliceStations.size() == 0) {
            tv_search.setText("Select Police Stations");
            policeStationString = "";
        } else {

            tv_search.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
            policeStationString = stringBuilderId.toString().substring(0, stringBuilderId.toString().length() - 1);
            System.out.println("Police Stattion String: " + policeStationString);
        }

    }

    //---------------------------------------------------------------------------------------------------------------//


	                                  /* Custom Dialog for Crime Category */

    //---------------------------------------------------------------------------------------------------------------//

    private void customDialogForCategoryCrimeList() {

        AlertDialog.Builder builder = new AlertDialog.Builder(SearchResultActivity.this);
        LayoutInflater inflater = SearchResultActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);

        TextView tv_title = (TextView) dialogView.findViewById(R.id.tv_title);
        Button btn_cancel = (Button) dialogView.findViewById(R.id.btn_cancel);
        Button btn_done = (Button) dialogView.findViewById(R.id.btn_done);
        ListView lv_dialog = (ListView) dialogView.findViewById(R.id.lv_dialog);
        SearchView searchView = (SearchView) dialogView.findViewById(R.id.searchView);


        Constants.changefonts(tv_title, SearchResultActivity.this, "Calibri Bold.ttf");
        tv_title.setText("Select Crime Category");

        customDialogAdapterForCategoryCrimeList2.notifyDataSetChanged();
        lv_dialog.setAdapter(customDialogAdapterForCategoryCrimeList2);
        //lv_dialog.setScrollingCacheEnabled(false);
        lv_dialog.setTextFilterEnabled(true);

        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(SearchResultActivity.this);
        searchView.setSubmitButtonEnabled(false);
        searchView.setQueryHint("Search Here");


        final AlertDialog alertDialog = builder.show();

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                crimeCategory_status = false;
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();

                }
                crimeCategory_status = false;

            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("No of Selected Items: " + CustomDialogAdapterForCategoryCrimeList2.selectedItemList.size());
                System.out.println("Selected Items: " + CustomDialogAdapterForCategoryCrimeList2.selectedItemList);
                crimeCategory_status = false;

                onSelectedCategoryOfCrime(CustomDialogAdapterForCategoryCrimeList2.selectedItemList);

                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();

                }

            }
        });

    }

    private void onSelectedCategoryOfCrime(List<String> crimeCategoryList) {

        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderCrimeCategory = new StringBuilder();

        for (CharSequence crimeCategory : crimeCategoryList) {
            stringBuilder.append(crimeCategory + ",");

            String crimeCategoryModifiedString = stringBuilderCrimeCategory.append("\'" + crimeCategory.toString().toLowerCase() + "\',").toString();

            crimeCategoryString = crimeCategoryModifiedString.substring(0, crimeCategoryModifiedString.length() - 1);
        }

        if (crimeCategoryList.size() == 0) {
            tv_search.setText("Select a Category");
        } else {

            System.out.println("Crime Category String: " + crimeCategoryString);
            tv_search.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
        }

    }


    //--------------------------------------------------------------------------------------------------------------//


                                    /* Custom Dialog for Modus Operandi */

    //-------------------------------------------------------------------------------------------------------------//


    private void customDialogForModusOperandi() {

        AlertDialog.Builder builder = new AlertDialog.Builder(SearchResultActivity.this);
        LayoutInflater inflater = SearchResultActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);

        TextView tv_title = (TextView) dialogView.findViewById(R.id.tv_title);
        Button btn_cancel = (Button) dialogView.findViewById(R.id.btn_cancel);
        Button btn_done = (Button) dialogView.findViewById(R.id.btn_done);
        ListView lv_dialog = (ListView) dialogView.findViewById(R.id.lv_dialog);
        SearchView searchView = (SearchView) dialogView.findViewById(R.id.searchView);


        Constants.changefonts(tv_title, SearchResultActivity.this, "Calibri Bold.ttf");
        tv_title.setText("Select Modus Operandi");

        customDialogAdapterForModusOperandi.notifyDataSetChanged();
        lv_dialog.setAdapter(customDialogAdapterForModusOperandi);
        //lv_dialog.setScrollingCacheEnabled(false);
        lv_dialog.setTextFilterEnabled(true);

        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(SearchResultActivity.this);
        searchView.setSubmitButtonEnabled(false);
        searchView.setQueryHint("Search Here");


        final AlertDialog alertDialog = builder.show();

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                modusOperandi_status = false;

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                modusOperandi_status = false;

                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();

                }

            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("No of Selected Items: " + CustomDialogAdapterForModusOperandi.selectedItemList.size());
                System.out.println("Selected Items: " + CustomDialogAdapterForModusOperandi.selectedItemList);

                modusOperandi_status = false;

                onSelectedModusOperandi(CustomDialogAdapterForModusOperandi.selectedItemList);

                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();

                }

            }
        });

    }


    private void onSelectedModusOperandi(List<String> modusOperandiList) {


        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderModusOperandi = new StringBuilder();


        for (CharSequence modusOperandi : modusOperandiList) {
            stringBuilder.append(modusOperandi + ",");

            String modusOperandiModifiedString = stringBuilderModusOperandi.append("\'" + modusOperandi.toString().toLowerCase() + "\',").toString();

            modusOperandiString = modusOperandiModifiedString.substring(0, modusOperandiModifiedString.length() - 1);

        }

        if (modusOperandiList.size() == 0) {
            tv_search.setText("Select an Operandi");
            modusOperandiString = "";
        } else {
            System.out.println("Modus Operandi String: " + modusOperandiString);
            tv_search.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (crimeCategory_status) {
            customDialogAdapterForCategoryCrimeList2.filter(newText.toString());
        } else if (modusOperandi_status) {
            customDialogAdapterForModusOperandi.filter(newText.toString());
        }

        return true;

    }



    /* Identify Category Dialog */

    protected void showSelectIdentifyCategoryDialog() {

        boolean[] checkedIdentifyCategory = new boolean[array_identifyCategory.length];
        int count = array_identifyCategory.length;


        for (int i = 0; i < count; i++) {
            checkedIdentifyCategory[i] = selectedIdentifyCategory.contains(array_identifyCategory[i]);
        }

        DialogInterface.OnMultiChoiceClickListener identifyCategoryDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedIdentifyCategory.add(array_identifyCategory[which]);
                } else {
                    selectedIdentifyCategory.remove(array_identifyCategory[which]);
                }

//				onChangeSelectedIdentifyCategory();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select a Mark Category");
        builder.setMultiChoiceItems(array_identifyCategory, checkedIdentifyCategory, identifyCategoryDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                //tv_subCategory_value.setText("Select a Sub Category");
                onChangeSelectedIdentifyCategory();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                selectedIdentifyCategory.clear();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();


    }

    protected void onChangeSelectedIdentifyCategory() {

        StringBuilder stringBuilder = new StringBuilder();

        for (CharSequence identifyCategory : selectedIdentifyCategory) {

            String identifyCategoryModifiedString = stringBuilder.append(identifyCategory + ",").toString();
            identifyCategoryString = identifyCategoryModifiedString.substring(0, identifyCategoryModifiedString.length() - 1);

        }

        if (selectedIdentifyCategory.size() == 0) {
            tv_search.setText("Select a Mark Category");
            identifyCategoryString = "";
        } else {
            tv_search.setText(identifyCategoryString);
            tv_identityMarkSubCategory.setVisibility(View.VISIBLE);
            tv_identityMarkSubCategory.setText("Select a Sub Category");
        }

    }

    private void fetchIdentitySubCategory(String category) {

        if (!Constants.internetOnline(this)) {
            Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_IDENTITY_SUB_CATEGORY);
        taskManager.setIdentitySubCategoryForAdvanceSearch(true);

        String[] keys = {"categoryname"};
        String[] values = {category.trim()};
        taskManager.doStartTask(keys, values, true);

        inputMarkArray = category.split(",");
        System.out.println("Input_Mark_Array: " + inputMarkArray);

    }

    public void parseIdentitySubCategoryResponse(String response) {

        //System.out.println("Identity Sub Category Response: " + response);

        identitySubCategoryList.clear();
        customIdentitySubCategoryList.clear();


        if (response != null && !response.equals("")) {


            JSONObject jObj = null;
            try {

                jObj = new JSONObject(response);
                if (jObj != null && jObj.optString("status").equalsIgnoreCase("1")) {

                    JSONArray identity_sub_category_resultArray = jObj.getJSONArray("result");


                    for (int i = 0; i < identity_sub_category_resultArray.length(); i++) {

                        JSONObject row = identity_sub_category_resultArray.getJSONObject(i);
                        System.out.println("Input: " + inputMarkArray[i]);
                        JSONArray jsonArray = row.getJSONArray(inputMarkArray[i]);

                        for (int j = 0; j < jsonArray.length(); j++) {
                            identitySubCategoryList.add(jsonArray.optString(j));
                            customIdentitySubCategoryList.add(inputMarkArray[i] + "_" + jsonArray.optString(j));
                        }


                    }


                } else {
                    showAlertDialog(SearchResultActivity.this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        System.out.println("Identity Sub Category List: " + identitySubCategoryList);
        System.out.println("Identity Sub Category List Size: " + identitySubCategoryList.size());

        System.out.println("Identity Custom Sub Category List: " + customIdentitySubCategoryList);
        System.out.println("Identity Custom Sub Category List Size: " + customIdentitySubCategoryList.size());

        //createJSONForCategorySubCategory();

        array_identifySubCategory = new String[identitySubCategoryList.size()];
        array_identifySubCategory = identitySubCategoryList.toArray(array_identifySubCategory);


        showSelectIdentifySubCategoryDialog();


    }

    /* Identify Sub Category Dialog */

    protected void showSelectIdentifySubCategoryDialog() {

        customSelectedIdentifySubCategory.clear();
        selectedIdentifySubCategory.clear();
        identifySubCategoryString = "";

        boolean[] checkedIdentifySubCategory = new boolean[array_identifySubCategory.length];
        int count = array_identifySubCategory.length;

        for (int i = 0; i < count; i++) {
            checkedIdentifySubCategory[i] = selectedIdentifySubCategory.contains(array_identifySubCategory[i]);
        }

        DialogInterface.OnMultiChoiceClickListener identifySubCategoryDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedIdentifySubCategory.add(array_identifySubCategory[which]);
                    customSelectedIdentifySubCategory.add(customIdentitySubCategoryList.get(which));
                } else {
                    selectedIdentifySubCategory.remove(array_identifySubCategory[which]);
                    customSelectedIdentifySubCategory.remove(customIdentitySubCategoryList.get(which));
                }

//				onChangeSelectedIdentifySubCategory();
            }
        };


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select a Sub Category");
        builder.setMultiChoiceItems(array_identifySubCategory, checkedIdentifySubCategory, identifySubCategoryDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                btn_done.setVisibility(View.VISIBLE);
                onChangeSelectedIdentifySubCategory();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                selectedIdentifySubCategory.clear();
                customSelectedIdentifySubCategory.clear();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    protected void onChangeSelectedIdentifySubCategory() {

        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderIdentifySubCategory = new StringBuilder();

        for (CharSequence identifySubCategory : selectedIdentifySubCategory) {
            stringBuilder.append(identifySubCategory + ",");

            String identifySubCategoryModifiedString = stringBuilderIdentifySubCategory.append("\'" + identifySubCategory.toString().toLowerCase() + "\',").toString();

            identifySubCategoryString = identifySubCategoryModifiedString.substring(0, identifySubCategoryModifiedString.length() - 1);
        }

        if (selectedIdentifySubCategory.size() == 0) {
            tv_identityMarkSubCategory.setText("Select a Sub Category");
        } else {
            System.out.println("Get Select Sub Category List: " + customSelectedIdentifySubCategory);
            tv_identityMarkSubCategory.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
            createJSONForCategorySubCategory();

        }

    }

    private void createJSONForCategorySubCategory() {

        map = new HashMap<String, List<String>>();

        List<String>[] aList = new List[inputMarkArray.length];
        JSONObject objCategorySubcategory = new JSONObject();

        for (int j = 0; j < inputMarkArray.length; j++) {
            System.out.println("IN: " + inputMarkArray[j]);
        }

        System.out.println("Size: " + customSelectedIdentifySubCategory.size());
        for (int i = 0; i < customSelectedIdentifySubCategory.size(); i++) {
            String a = customSelectedIdentifySubCategory.get(i);
            insert(a.substring(0, a.indexOf("_")), a.substring(a.indexOf("_") + 1));

            for (int j = 0; j < inputMarkArray.length; j++) {
                aList[j] = map.get(inputMarkArray[j]);
            }

        }
        for (int j = 0; j < inputMarkArray.length; j++) {

            System.out.println("List " + j + "  :" + aList[j]);

            JSONArray jsonArray = new JSONArray();
            try {
                for (int k = 0; k < aList[j].size(); k++) {
                    jsonArray.put(aList[j].get(k));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            try {
                if (jsonArray.length() > 0) {
                    objCategorySubcategory.put(inputMarkArray[j], jsonArray);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        identifyCategorySubCategoryString = "";
        identifyCategorySubCategoryString = objCategorySubcategory.toString();
        System.out.println("Created JSON For Category and SubCategory: " + identifyCategorySubCategoryString);

    }

    private void insert(String key, String value) {
        List<String> list = map.get(key);
        if (list == null) {
            list = new ArrayList<String>();
            map.put(key, list);
        }
        list.add(value);
    }

    private void setFooterData() {

        footerListData.add("CF: CRS & FIR");
        footerListData.add("OC: CRS");
        footerListData.add("OF: FIR");
        footerListData.add("OA: Arrest");
        footerListData.add("OW: Warrant");
        footerListData.add("WA: Warrant & Arrest");


        customFooterAdapter = new CustomFooterAdapter(SearchResultActivity.this, footerListData);
        customListView.setAdapter(customFooterAdapter);

    }

    private void setAgeData() {

        ageAdapter = ArrayAdapter.createFromResource(SearchResultActivity.this,
                R.array.Age_array, R.layout.simple_spinner_item);
        ageAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_age.setAdapter(ageAdapter);

        sp_age.setSelection(0);

    }


    private void setYearData() {

        Calendar calendar = Calendar.getInstance();
        int current_year = calendar.get(Calendar.YEAR);

        /* Case year Spinner set */
        yearList.clear();
        yearList.add("Enter Year");

        for (int i = current_year; i >= 1980; i--) {
            yearList.add(Integer.toString(i));
        }

        yearAdapter = new ArrayAdapter<String>(SearchResultActivity.this, R.layout.spinner_custom_textcolor, yearList);

        yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_year.setAdapter(yearAdapter);

        yearAdapter.notifyDataSetChanged();
        sp_year.setSelection(0);

        sp_year.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        caseYr = sp_year.getSelectedItem().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        sp_year.setSelection(0);
    }



     /* This method shows Class list in a pop-up */

    protected void showSelectClassDialog() {
        boolean[] checkedClass = new boolean[array_classValue.length];
        int count = array_classValue.length;

        for(int i = 0; i < count; i++) {
            checkedClass[i] = selectedClassList.contains(array_classValue[i]);
        }

        DialogInterface.OnMultiChoiceClickListener classDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedClassList.add(array_classValue[which]);
                } else{
                    selectedClassList.remove(array_classValue[which]);
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(SearchResultActivity.this);
        builder.setTitle("Select Class");
        builder.setMultiChoiceItems(array_classValue, checkedClass, classDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedClass();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                selectedClassList.clear();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedClass() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderClass = new StringBuilder();

        Log.e("Selected Class List",selectedClassList.toString());

        for(CharSequence classStr : selectedClassList){
            String classModifiedString = stringBuilderClass.append("\'"+classStr + "\',").toString();
            classValue = classModifiedString.substring(0,classModifiedString.length() - 1);

            String newClassModifiedString = stringBuilder.append(classStr + ",").toString();
            classStringShow = newClassModifiedString.substring(0,newClassModifiedString.length() - 1);
        }

        if(selectedClassList.size()==0) {
            tv_search.setText("Select Class");
            /*classValue = "";
            classStringShow = "";*/
        }
        else {

            tv_search.setText(classStringShow);
            tv_subClass.setVisibility(View.VISIBLE);
            tv_subClass.setText("Select Sub-Class");
            //fetchSubClass(classValue);
        }
    }


    // sub class fetch
    private void fetchSubClass(String classStr){

        if (!Constants.internetOnline(this)) {
            Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_GET_CLASS_SUBCLASS);
        taskManager.setSubClassForAdvanceSearch(true);

        String[] keys = {"class"};
        String[] values = {classStr.trim()};
        taskManager.doStartTask(keys, values, true);

        inputClassArray = classStr.split(",");
    }


    public void parseSubClassResponse(String response) {
        //System.out.println("Identity Sub Class Response: " + response);

        subClassList.clear();
        customSubClassList.clear();

        if (response != null && !response.equals("")) {

            JSONObject jObj = null;
            try {

                jObj = new JSONObject(response);
                if(jObj != null && jObj.optString("status").equalsIgnoreCase("1")) {

                    JSONArray sub_class_resultArray = jObj.getJSONArray("result");

                    for(int i=0;i<sub_class_resultArray.length();i++) {

                        JSONObject row = sub_class_resultArray.getJSONObject(i);
                        JSONArray jsonArray = row.getJSONArray(inputClassArray[i]);

                        for(int j=0;j<jsonArray.length();j++){

                            if(jsonArray.optString(j) != null && !jsonArray.optString(j).equalsIgnoreCase("null")) {
                                subClassList.add(jsonArray.optString(j));
                                customSubClassList.add(inputClassArray[i] + "_" + jsonArray.optString(j));
                            }
                        }
                    }
                }
                else {
                    showAlertDialog(SearchResultActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

        array_subClassValue = new String [subClassList.size()];
        array_subClassValue = subClassList.toArray(array_subClassValue);
        Log.e("subClassList size",subClassList.size()+"");
        Log.e("customSubClassList size",customSubClassList.size()+"");

        showSelectedSubClassDialog();
    }


     /* This method shows Sub-Class list in a pop-up */

    protected void showSelectedSubClassDialog() {

        customSelectedSubClass.clear();
        selectedSubClassList.clear();
        subClassValue = "";
        subClassStringShow = "";

        boolean[] checkedSubClass = new boolean[array_subClassValue.length];
        int count = array_subClassValue.length;

        for(int i = 0; i < count; i++) {
            checkedSubClass[i] = selectedSubClassList.contains(array_subClassValue[i]);
        }

        DialogInterface.OnMultiChoiceClickListener subClassDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedSubClassList.add(array_subClassValue[which]);
                    customSelectedSubClass.add(customSubClassList.get(which));
                } else{
                    selectedSubClassList.remove(array_subClassValue[which]);
                    customSelectedSubClass.remove(customSubClassList.get(which));
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(SearchResultActivity.this);
        builder.setTitle("Select Sub-Class");
        builder.setMultiChoiceItems(array_subClassValue, checkedSubClass, subClassDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedSubClass();
                btn_done.setVisibility(View.VISIBLE);
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                customSelectedSubClass.clear();
                selectedSubClassList.clear();
                tv_subClass.setText("Select Sub-Class");
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    protected void onChangeSelectedSubClass() {
        StringBuilder stringBuilderSubClass = new StringBuilder();
        StringBuilder stringBuilderSubClassShow = new StringBuilder();

        Log.e("SubClass",selectedSubClassList.toString());
        Log.e("SubClass size",selectedSubClassList.size()+"");
        for(CharSequence subClass : selectedSubClassList) {

            String subClassModifiedString = stringBuilderSubClass.append(subClass.toString()+ ",").toString();
            subClassStringShow = subClassModifiedString.substring(0,subClassModifiedString.length()-1);

            String subClassModifiedStringShow = stringBuilderSubClassShow.append("\'"+subClass.toString()+ "\',").toString();
            subClassValue = subClassModifiedStringShow.substring(0,subClassModifiedStringShow.length()-1);
        }
        if(selectedSubClassList.size()==0) {
            tv_subClass.setText("Select Sub-Class");
        } else {
            tv_subClass.setText(subClassStringShow);
            //subClassStringShow = subClassValue;
        }
    }


    @Override
    public void imageUpload(int position, String url) {

        if(criminalList.get(position).getPicture_list().size() > 0) {
            Log.e("TAG:","UPLOAD URL new Image add: "+url);
            criminalList.get(position).getPicture_list().add(url);
            adapter.notifyDataSetChanged();
        }
        else{
            Log.e("TAG:", "UPLOAD URL 1st Image add: " + url);
            criminalList.get(position).getPicture_list().add(url);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}

