package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kp.facedetection.adapter.SDRSearchAdapter;
import com.kp.facedetection.model.SDRDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class SDRSearchResultActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, Observer {

    private String[] keys;
    private String[] values;
    private String pageno;
    private String totalResult="";
    private String searchCase = "";
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private TextView tv_resultCount;
    private ListView lv_sdrSearchList;
    private Button btnLoadMore;

    private List<SDRDetails> sdrDetailsList;
    SDRSearchAdapter sdrSearchAdapter;
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sdrsearch_result);
        ObservableObject.getInstance().addObserver(this);
        initialization();

        clickEvents();


    }

    private void initialization(){


        tv_resultCount=(TextView)findViewById(R.id.tv_resultCount);
        lv_sdrSearchList = (ListView)findViewById(R.id.lv_sdrSearchList);

        // LoadMore button
        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        keys = getIntent().getStringArrayExtra("keys");
        values = getIntent().getStringArrayExtra("value");
        pageno = getIntent().getStringExtra("pageno");
        totalResult = getIntent().getStringExtra("totalResult");

        sdrDetailsList = (List<SDRDetails>) getIntent().getSerializableExtra("SDR_SEARCH_LIST");

        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }

        sdrSearchAdapter = new SDRSearchAdapter(this,sdrDetailsList);
        lv_sdrSearchList.setAdapter(sdrSearchAdapter);
        sdrSearchAdapter.notifyDataSetChanged();

        if (Integer.parseInt(totalResult) > 20) {

            // Adding Load More button to listview at bottom
            lv_sdrSearchList.addFooterView(btnLoadMore);
        }

        lv_sdrSearchList.setOnItemClickListener(this);

    }

    private void clickEvents(){

        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // Starting a new async task
                fetchSDRSearchResultPagination();
            }
        });

    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this,Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    private void fetchSDRSearchResultPagination() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.MODIFIED_METHOD_SDR_SEARCH);
        taskManager.setSDRSearchPagination(true);

        values[2] = (Integer.parseInt(pageno) + 1) + "";

        taskManager.doStartTask(keys, values, true, true);


    }

    public void parseSDRSearchResultPaginationResponse(String result, String[] keys, String[] values) {

        //System.out.println("parseSDRSearchResultResponse: " + result);

        if(result != null && !result.equals("")){

            try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.opt("status").toString().equalsIgnoreCase("1")){

                    this.keys=keys;
                    this.values=values;
                    pageno=jObj.opt("page").toString();
                  //  totalResult=jObj.opt("count").toString();

                    JSONArray resultArray=jObj.getJSONArray("result");

                    parseSDRSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(this," Search Error ",jObj.optString("message"),false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                showAlertDialog(this, " Search Error!!! ", "Some error has been encountered", false);
            }
        }

    }

    private void parseSDRSearchResponse(JSONArray resultArray) {

        for(int i=0;i<resultArray.length();i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj=resultArray.getJSONObject(i);

                SDRDetails sdrDetails = new SDRDetails();

                String present_address ="";
                String permanent_address = "";
                if(!jsonObj.optString("PROVIDER").equalsIgnoreCase("null") && !jsonObj.optString("PROVIDER").equalsIgnoreCase("")){
                    sdrDetails.setAcct(jsonObj.optString("PROVIDER"));
                }
                if(!jsonObj.optString("ACTIVATIONDATE").equalsIgnoreCase("null") && !jsonObj.optString("ACTIVATIONDATE").equalsIgnoreCase("")){
                    sdrDetails.setActDate(jsonObj.optString("ACTIVATIONDATE"));
                }
                if(!jsonObj.optString("ID_NUMBER").equalsIgnoreCase("null") && !jsonObj.optString("ID_NUMBER").equalsIgnoreCase("")){
                    sdrDetails.setId_Number(jsonObj.optString("ID_NUMBER"));
                }
                if(!jsonObj.optString("ID_TYPE").equalsIgnoreCase("null") && !jsonObj.optString("ID_TYPE").equalsIgnoreCase("")){
                    sdrDetails.setId_type(jsonObj.optString("ID_TYPE"));
                }
                if(!jsonObj.optString("MOBNO").equalsIgnoreCase("null") && !jsonObj.optString("MOBNO").equalsIgnoreCase("")){
                    sdrDetails.setMobNo(jsonObj.optString("MOBNO"));
                }

                if(!jsonObj.optString("ALTERNATEPHNUMBER").equalsIgnoreCase("null") && !jsonObj.optString("ALTERNATEPHNUMBER").equalsIgnoreCase("")){
                    sdrDetails.setPhone_no(jsonObj.optString("ALTERNATEPHNUMBER"));
                }

                if(!jsonObj.optString("NAME").equalsIgnoreCase("null") && !jsonObj.optString("NAME").equalsIgnoreCase("")){
                    sdrDetails.setName(jsonObj.optString("NAME"));
                }
                if(!jsonObj.optString("ADDRESS").equalsIgnoreCase("null") && !jsonObj.optString("ADDRESS").equalsIgnoreCase("")){
                    sdrDetails.setPresent_addres(jsonObj.optString("ADDRESS"));
                }

                if(!jsonObj.optString("PINCODE").equalsIgnoreCase("null") && !jsonObj.optString("PINCODE").equalsIgnoreCase("")){
                    sdrDetails.setPresent_pincode(jsonObj.optString("PINCODE"));
                }
                if(!jsonObj.optString("CITY").equalsIgnoreCase("null") && !jsonObj.optString("CITY").equalsIgnoreCase("")){
                    sdrDetails.setPresent_city(jsonObj.optString("CITY"));
                }
                if(!jsonObj.optString("STATE").equalsIgnoreCase("null") && !jsonObj.optString("STATE").equalsIgnoreCase("")){
                    sdrDetails.setPresent_state(jsonObj.optString("STATE"));
                }




              /*  if(!jsonObj.optString("ACCT").equalsIgnoreCase("null") && !jsonObj.optString("ACCT").equalsIgnoreCase("")){
                    sdrDetails.setAcct(jsonObj.optString("ACCT"));
                }
                if(!jsonObj.optString("ID_NUMBER").equalsIgnoreCase("null") && !jsonObj.optString("ID_NUMBER").equalsIgnoreCase("")){
                    sdrDetails.setId_Number(jsonObj.optString("ID_NUMBER"));
                }
                if(!jsonObj.optString("ID_TYPE").equalsIgnoreCase("null") && !jsonObj.optString("ID_TYPE").equalsIgnoreCase("")){
                    sdrDetails.setId_type(jsonObj.optString("ID_TYPE"));
                }

                if(!jsonObj.optString("MOBNO").equalsIgnoreCase("null") && !jsonObj.optString("MOBNO").equalsIgnoreCase("")){
                    sdrDetails.setMobNo(jsonObj.optString("MOBNO"));
                }

                if(!jsonObj.optString("PHONE").equalsIgnoreCase("null") && !jsonObj.optString("PHONE").equalsIgnoreCase("")){
                    sdrDetails.setPhone_no(jsonObj.optString("PHONE"));
                }


                if(!jsonObj.optString("NAME").equalsIgnoreCase("null") && !jsonObj.optString("NAME").equalsIgnoreCase("")){
                    sdrDetails.setName(jsonObj.optString("NAME"));
                }
                if(!jsonObj.optString("ADD1").equalsIgnoreCase("null") && !jsonObj.optString("ADD1").equalsIgnoreCase("")){
                    present_address = present_address + jsonObj.optString("ADD1");
                }
                if(!jsonObj.optString("ADD2").equalsIgnoreCase("null") && !jsonObj.optString("ADD2").equalsIgnoreCase("")){
                    present_address = present_address + ", " + jsonObj.optString("ADD2");
                }
                if(!jsonObj.optString("ADD3").equalsIgnoreCase("null") && !jsonObj.optString("ADD3").equalsIgnoreCase("")){
                    present_address = present_address + ", " + jsonObj.optString("ADD3");
                }

                sdrDetails.setPresent_addres(present_address);


                if(!jsonObj.optString("OSADD1").equalsIgnoreCase("null") && !jsonObj.optString("OSADD1").equalsIgnoreCase("")){
                    permanent_address = permanent_address + jsonObj.optString("OSADD1");
                }
                if(!jsonObj.optString("OSADD2").equalsIgnoreCase("null") && !jsonObj.optString("OSADD2").equalsIgnoreCase("")){
                    permanent_address = permanent_address + ", " + jsonObj.optString("OSADD2");
                }
                if(!jsonObj.optString("OSADD3").equalsIgnoreCase("null") && !jsonObj.optString("OSADD3").equalsIgnoreCase("")){
                    permanent_address = permanent_address + ", " + jsonObj.optString("OSADD3");
                }

                sdrDetails.setPermanent_address(permanent_address);

                if(!jsonObj.optString("PIN1").equalsIgnoreCase("null") && !jsonObj.optString("PIN1").equalsIgnoreCase("")){
                    sdrDetails.setPresent_pincode(jsonObj.optString("PIN1"));
                }
                if(!jsonObj.optString("PIN2").equalsIgnoreCase("null") && !jsonObj.optString("PIN2").equalsIgnoreCase("")){
                    sdrDetails.setPermanent_pincode(jsonObj.optString("PIN2"));
                }
                if(!jsonObj.optString("ACTDATE").equalsIgnoreCase("null") && !jsonObj.optString("ACTDATE").equalsIgnoreCase("")){
                    sdrDetails.setActDate(jsonObj.optString("ACTDATE"));
                }
                if(!jsonObj.optString("CITY1").equalsIgnoreCase("null") && !jsonObj.optString("CITY1").equalsIgnoreCase("")){
                    sdrDetails.setPresent_city(jsonObj.optString("CITY1"));
                }
                if(!jsonObj.optString("CITY2").equalsIgnoreCase("null") && !jsonObj.optString("CITY2").equalsIgnoreCase("")){
                    sdrDetails.setPermanent_city(jsonObj.optString("CITY2"));
                }
                if(!jsonObj.optString("STATE1").equalsIgnoreCase("null") && !jsonObj.optString("STATE1").equalsIgnoreCase("")){
                    sdrDetails.setPresent_state(jsonObj.optString("STATE1"));
                }
                if(!jsonObj.optString("STATE2").equalsIgnoreCase("null") && !jsonObj.optString("STATE2").equalsIgnoreCase("")){
                    sdrDetails.setPermanent_state(jsonObj.optString("STATE2"));
                }*/

                sdrDetailsList.add(sdrDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        int currentPosition = lv_sdrSearchList.getFirstVisiblePosition();
        int lastVisiblePosition = lv_sdrSearchList.getLastVisiblePosition();

        // Setting new scroll position
        lv_sdrSearchList.setSelectionFromTop(currentPosition + 1, 0);

        if((Integer.parseInt(totalResult)-lastVisiblePosition)<20){
            lv_sdrSearchList.removeFooterView(btnLoadMore);
        }

    }

    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     * */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent in=new Intent(SDRSearchResultActivity.this,SDRSearchDetailsActivity.class);
        in.putExtra("HOLDER_NAME",sdrDetailsList.get(position).getName());
        in.putExtra("ID_NUMBER",sdrDetailsList.get(position).getId_Number());
        in.putExtra("ID_TYPE",sdrDetailsList.get(position).getId_type());
        in.putExtra("HOLDER_PRESENT_ADDRESS",sdrDetailsList.get(position).getPresent_address());
        in.putExtra("HOLDER_PERMANENT_ADDRESS",sdrDetailsList.get(position).getPresent_address());
        in.putExtra("HOLDER_CITY",sdrDetailsList.get(position).getPresent_city());
        in.putExtra("HOLDER_STATE",sdrDetailsList.get(position).getPresent_state());
        in.putExtra("HOLDER_PINCODE",sdrDetailsList.get(position).getPresent_pincode());
        in.putExtra("HOLDER_ACT_PROVIDER",sdrDetailsList.get(position).getAcct());
        in.putExtra("HOLDER_ACT_DATE",sdrDetailsList.get(position).getActDate());
        in.putExtra("HOLDER_MOBILE",sdrDetailsList.get(position).getMobNo());
        in.putExtra("HOLDER_PHONE",sdrDetailsList.get(position).getPhone_no());
        startActivity(in);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
