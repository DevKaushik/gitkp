package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kp.facedetection.adapter.PMReportSearchAdapter;
import com.kp.facedetection.model.PMReportSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class PMReportSearchResultActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, Observer {

    private String[] keys;
    private String[] values;
    private String pageno;
    private String totalResult = "";
    private String searchCase = "";
    private String userName = "";
    private String user_Id = "";

    private String loginNumber = "";
    private String appVersion = "";

    private TextView tv_resultCount;
    private ListView lv_pmReportSearchList;
    private Button btnLoadMore;

    private TextView tv_watermark;

    private List<PMReportSearchDetails> pmReportSearchDetailsList;
    private PMReportSearchAdapter pmReportSearchAdapter;
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }

    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }

    public enum DataHolder {
        INSTANCE;

        private List<PMReportSearchDetails> mObjectList;

        public static boolean hasData() {
            return INSTANCE.mObjectList != null;
        }

        public static void setData(final List<PMReportSearchDetails> objectList) {
            INSTANCE.mObjectList = objectList;
        }

        public static List<PMReportSearchDetails> getData() {
            final List<PMReportSearchDetails> retList = INSTANCE.mObjectList;
            INSTANCE.mObjectList = null;
            return retList;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pmreport_search_result);
        ObservableObject.getInstance().addObserver(this);

        initialization();
        clickEvents();
    }

    private void initialization() {

        tv_resultCount = (TextView) findViewById(R.id.tv_resultCount);
        lv_pmReportSearchList = (ListView) findViewById(R.id.lv_pmReportSearchList);

        // LoadMore button
        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
            user_Id = Utility.getUserInfo(this).getUserId();
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        keys = getIntent().getStringArrayExtra("keys");
        values = getIntent().getStringArrayExtra("value");
        pageno = getIntent().getStringExtra("pageno");
        totalResult = getIntent().getStringExtra("totalResult");

        Bundle bundle = getIntent().getExtras();
        pmReportSearchDetailsList = bundle.getParcelableArrayList("PM_REPORT_SEARCH_LIST");

        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }

        pmReportSearchAdapter = new PMReportSearchAdapter(this, pmReportSearchDetailsList);
        lv_pmReportSearchList.setAdapter(pmReportSearchAdapter);
        pmReportSearchAdapter.notifyDataSetChanged();

        if (Integer.parseInt(totalResult) > 20) {

            // Adding Load More button to listview at bottom
            lv_pmReportSearchList.addFooterView(btnLoadMore);
        }

        lv_pmReportSearchList.setOnItemClickListener(this);

        /*  Set user name as Watermark  */
        tv_watermark = (TextView) findViewById(R.id.tv_watermark);
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-55);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                // Utility.logout(this);
                break;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    private void clickEvents() {

        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Starting a new async task
                fetchPMReportSearchResultPagination();
            }
        });
    }

    private void fetchPMReportSearchResultPagination() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_PMREPORT_SEARCH);
        taskManager.setPMReportSearchPagination(true);

        values[14] = (Integer.parseInt(pageno) + 1) + "";
        taskManager.doStartTask(keys, values, true, true);
    }

    public void parsePMReportSearchPagination(String result, String[] keys, String[] values) {
        //System.out.println("PM Report Search Result Pagination: " + result);
        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("page").toString();
                    totalResult = jObj.opt("count").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parsePMReportSearchResponse(resultArray);

                } else {
                    showAlertDialog(this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }


            } catch (JSONException e) {

                e.printStackTrace();
                showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }

    private void parsePMReportSearchResponse(JSONArray resultArray) {

        for (int i = 0; i < resultArray.length(); i++) {
            JSONObject jsonObj = null;
            try {
                jsonObj = resultArray.getJSONObject(i);
                PMReportSearchDetails pmReportSearchDetails = new PMReportSearchDetails(Parcel.obtain());

                if(!jsonObj.optString("PSCODE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPsCode(jsonObj.optString("PSCODE"));
                }

                if(!jsonObj.optString("PSNAME").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPsName(jsonObj.optString("PSNAME"));
                }

                if(!jsonObj.optString("INQUESTNO").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setInquestNo(jsonObj.optString("INQUESTNO"));
                }

                if(!jsonObj.optString("INQUESTDT").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setInquestDt(jsonObj.optString("INQUESTDT"));
                }

                if(!jsonObj.optString("INQUESTYR").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setInquestYr(jsonObj.optString("INQUESTYR"));
                }

                if(!jsonObj.optString("PMNO").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPmNo(jsonObj.optString("PMNO"));
                }

                if(!jsonObj.optString("PMDT").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPmDt(jsonObj.optString("PMDT"));
                }

                if(!jsonObj.optString("PMYEAR").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPmYr(jsonObj.optString("PMYEAR"));
                }

                if(!jsonObj.optString("NAME_DECEASED").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setNameDeceased(jsonObj.optString("NAME_DECEASED"));
                }

                if(!jsonObj.optString("SEX").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setGender(jsonObj.optString("SEX"));
                }

                if(!jsonObj.optString("AGEVAL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setAgeVal(jsonObj.optString("AGEVAL"));
                }

                if(!jsonObj.optString("AGE_INDICATOR").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setAgeIndicator(jsonObj.optString("AGE_INDICATOR"));
                }

                if(!jsonObj.optString("DTARRVDEADHOUSE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setDeadHouse(jsonObj.optString("DTARRVDEADHOUSE"));
                }

                if(!jsonObj.optString("STATIONCODE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setStationCode(jsonObj.optString("STATIONCODE"));
                }

                if(!jsonObj.optString("STATION").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setStationName(jsonObj.optString("STATION"));
                }

                if(!jsonObj.optString("DONEBY").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setDoneBy(jsonObj.optString("DONEBY"));
                }

                if(!jsonObj.optString("FIRNO").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setFirNo(jsonObj.optString("FIRNO"));
                }

                if(!jsonObj.optString("FIRYEAR").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setFirYr(jsonObj.optString("FIRYEAR"));
                }

                if(!jsonObj.optString("FIRDATE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setFirDate(jsonObj.optString("FIRDATE"));
                }

                if(!jsonObj.optString("FIRPSCODE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setFirPS(jsonObj.optString("FIRPSCODE"));
                }

                if(!jsonObj.optString("OPNAME").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setOpName(jsonObj.optString("OPNAME"));
                }

                if(!jsonObj.optString("RELIGION").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setReligion(jsonObj.optString("RELIGION"));
                }

                if(!jsonObj.optString("WHENCE_BROUGHT").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setWhen_ce_brought(jsonObj.optString("WHENCE_BROUGHT"));
                }

                if(!jsonObj.optString("NAME_CONST_RELATIVES").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setName_const_relatives(jsonObj.optString("NAME_CONST_RELATIVES"));
                }

                if(!jsonObj.optString("DTDESPATCHDBR").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setDate_dispatch(jsonObj.optString("DTDESPATCHDBR"));
                }

                if(!jsonObj.optString("DTARRVDEADHOUSE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setDate_arrive(jsonObj.optString("DTARRVDEADHOUSE"));
                }

                if(!jsonObj.optString("DTEXAM").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setDate_exam(jsonObj.optString("DTEXAM"));
                }

                if(!jsonObj.optString("INFOBYPOLICE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setInfo_by_police(jsonObj.optString("INFOBYPOLICE"));
                }

                if(!jsonObj.optString("INDENT_BEF_MO").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setIndent_bef_no(jsonObj.optString("INDENT_BEF_MO"));
                }

                if(!jsonObj.optString("LENGTH_FT").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setLength_ft(jsonObj.optString("LENGTH_FT"));
                }

                if(!jsonObj.optString("LENGTH_INCH").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setLength_inch(jsonObj.optString("LENGTH_INCH"));
                }

                if(!jsonObj.optString("SUBJECT_CONDITION").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setSubject_condition(jsonObj.optString("SUBJECT_CONDITION"));
                }

                if(!jsonObj.optString("WOUNDS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setWounds(jsonObj.optString("WOUNDS"));
                }

                if(!jsonObj.optString("BRUISES").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setBruises(jsonObj.optString("BRUISES"));
                }

                if(!jsonObj.optString("LIGATURE_MARKS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setLigature_marks(jsonObj.optString("LIGATURE_MARKS"));
                }

                if(!jsonObj.optString("SCALP_SKULL_VERTEBRAE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setSculp_skull_vertebrae(jsonObj.optString("SCALP_SKULL_VERTEBRAE"));
                }

                if(!jsonObj.optString("MEMBRANE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMembrane(jsonObj.optString("MEMBRANE"));
                }

                if(!jsonObj.optString("BRAIN_SPCORD").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setBrain_sp_cord(jsonObj.optString("BRAIN_SPCORD"));
                }

                if(!jsonObj.optString("WALLS_RIBS_CARTILAGE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setWalls_ribs_cartilage(jsonObj.optString("WALLS_RIBS_CARTILAGE"));
                }

                if(!jsonObj.optString("LARYNX_TRACHEA").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setLaryncs(jsonObj.optString("LARYNX_TRACHEA"));
                }

                if(!jsonObj.optString("RIGHT_LUNG").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setRight_lung(jsonObj.optString("RIGHT_LUNG"));
                }

                if(!jsonObj.optString("LEFT_LUNG").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setLeft_lung(jsonObj.optString("LEFT_LUNG"));
                }

                if(!jsonObj.optString("PERICARDIUM").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPericardium(jsonObj.optString("PERICARDIUM"));
                }

                if(!jsonObj.optString("HEART").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setHeart(jsonObj.optString("HEART"));
                }

                if(!jsonObj.optString("VESSELS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setVessels(jsonObj.optString("VESSELS"));
                }

                if(!jsonObj.optString("PERITONEUM").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPeritoneum(jsonObj.optString("PERITONEUM"));
                }

                if(!jsonObj.optString("MOUTH_PHARYNX_ESOPHAGUS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setEsophagus(jsonObj.optString("MOUTH_PHARYNX_ESOPHAGUS"));
                }

                if(!jsonObj.optString("STOMACH").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setStomach(jsonObj.optString("STOMACH"));
                }

                if(!jsonObj.optString("SMALL_INTESTINE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setSmall_intestine(jsonObj.optString("SMALL_INTESTINE"));
                }

                if(!jsonObj.optString("LARGE_INTESTINE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setLarge_intestine(jsonObj.optString("LARGE_INTESTINE"));
                }

                if(!jsonObj.optString("LIVER").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setLiver(jsonObj.optString("LIVER"));
                }

                if(!jsonObj.optString("SPLEEN").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setSpleen(jsonObj.optString("SPLEEN"));
                }

                if(!jsonObj.optString("KIDNEYS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setKidneys(jsonObj.optString("KIDNEYS"));
                }

                if(!jsonObj.optString("BLADDER").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setBladder(jsonObj.optString("BLADDER"));
                }

                if(!jsonObj.optString("GENERATION_EXTERNAL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setGeneration_external(jsonObj.optString("GENERATION_EXTERNAL"));
                }

                if(!jsonObj.optString("GENERATION_INTERNAL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setGeneration_internal(jsonObj.optString("GENERATION_INTERNAL"));
                }

                if(!jsonObj.optString("MBJ_INJURY").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMbj_injury(jsonObj.optString("MBJ_INJURY"));
                }

                if(!jsonObj.optString("MBJ_DISEASE_DEFORMITY").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMbj_disease_deformity(jsonObj.optString("MBJ_DISEASE_DEFORMITY"));
                }

                if(!jsonObj.optString("MBJ_FRACTURE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMbj_fracture(jsonObj.optString("MBJ_FRACTURE"));
                }

                if(!jsonObj.optString("MBJ_DISLOCATION").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMbj_delocation(jsonObj.optString("MBJ_DISLOCATION"));
                }

                if(!jsonObj.optString("ADDITIONAL_REMARKS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setAdditional_remarks(jsonObj.optString("ADDITIONAL_REMARKS"));
                }

                if(!jsonObj.optString("OPINION_MO").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setOption_mo(jsonObj.optString("OPINION_MO"));
                }

                if(!jsonObj.optString("OPINION_CS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setOption_cs(jsonObj.optString("OPINION_CS"));
                }

                if(!jsonObj.optString("MODIUSER").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setModi_user(jsonObj.optString("MODIUSER"));
                }

                if(!jsonObj.optString("MODIDT").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setModi_dt(jsonObj.optString("MODIDT"));
                }

                if(!jsonObj.optString("SUBMITTED").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setSubmitted(jsonObj.optString("SUBMITTED"));
                }

                if(!jsonObj.optString("MP_VISCERAE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_viscerae(jsonObj.optString("MP_VISCERAE"));
                }

                if(!jsonObj.optString("MP_BLOOD").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_blood(jsonObj.optString("MP_BLOOD"));
                }

                if(!jsonObj.optString("MP_SCALP_HAIR").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_scalp_hair(jsonObj.optString("MP_SCALP_HAIR"));
                }

                if(!jsonObj.optString("MP_NAIL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_nails(jsonObj.optString("MP_NAIL"));
                }

                if(!jsonObj.optString("MP_APPARELS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_apparels(jsonObj.optString("MP_APPARELS"));
                }

                if(!jsonObj.optString("MP_LIGATURE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_ligature(jsonObj.optString("MP_LIGATURE"));
                }

                if(!jsonObj.optString("MP_FOREIGN").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_foreign(jsonObj.optString("MP_FOREIGN"));
                }

                if(!jsonObj.optString("MP_SKIN").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_skin(jsonObj.optString("MP_SKIN"));
                }

                if(!jsonObj.optString("MP_VAGINAL_ANAL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_vaginal_anal(jsonObj.optString("MP_VAGINAL_ANAL"));
                }

                if(!jsonObj.optString("MP_OTHERS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_others(jsonObj.optString("MP_OTHERS"));
                }

                if(!jsonObj.optString("MP_OTHERS_DETAIL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMp_other_details(jsonObj.optString("MP_OTHERS_DETAIL"));
                }

                if(!jsonObj.optString("SENT_ON").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setSent_on(jsonObj.optString("SENT_ON"));
                }

                if(!jsonObj.optString("STN_BILL_NO").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setStation_billNo(jsonObj.optString("STN_BILL_NO"));
                }

                if(!jsonObj.optString("STN_BILL_DT").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setStation_billDt(jsonObj.optString("STN_BILL_DT"));
                }

                if(!jsonObj.optString("RELEASED").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setReleased(jsonObj.optString("RELEASED"));
                }

                if(!jsonObj.optString("NEFTDONE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setNeft_done(jsonObj.optString("NEFTDONE"));
                }

                if(!jsonObj.optString("NEFTDATE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setNeft_dt(jsonObj.optString("NEFTDATE"));
                }

                if(!jsonObj.optString("REFDBY").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setRef_by(jsonObj.optString("REFDBY"));
                }

                if(!jsonObj.optString("REFNO").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setRef_no(jsonObj.optString("REFNO"));
                }

                if(!jsonObj.optString("REFDT").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setRef_date(jsonObj.optString("REFDT"));
                }

                if(!jsonObj.optString("REMARKS").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setRemarks(jsonObj.optString("REMARKS"));
                }

                if(!jsonObj.optString("TRNID").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setTranId(jsonObj.optString("TRNID"));
                }

                if(!jsonObj.optString("FILESIZE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setFile_size(jsonObj.optString("FILESIZE"));
                }

                if(!jsonObj.optString("MIMETYPE").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setMime_type(jsonObj.optString("MIMETYPE"));
                }

                if(!jsonObj.optString("FILENAME").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setFile_name(jsonObj.optString("FILENAME"));
                }

                if(!jsonObj.optString("PDF_URL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setPdf_url(jsonObj.optString("PDF_URL"));
                }

                if(!jsonObj.optString("CRIMINAL_PDF_URL").equalsIgnoreCase("null")){
                    pmReportSearchDetails.setCriminal_pdf_url(jsonObj.optString("CRIMINAL_PDF_URL"));
                }


                pmReportSearchDetailsList.add(pmReportSearchDetails);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        int currentPosition = lv_pmReportSearchList.getFirstVisiblePosition();
        int lastVisiblePosition = lv_pmReportSearchList.getLastVisiblePosition();
        // Setting new scroll position
        lv_pmReportSearchList.setSelectionFromTop(currentPosition + 1, 0);

        if((Integer.parseInt(totalResult)-lastVisiblePosition)<20){
            lv_pmReportSearchList.removeFooterView(btnLoadMore);
        }
    }


    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

       // PMReportSearchDetails pmReportSearchDetails =  pmReportSearchDetailsList.get(position);

        /*List<PMReportSearchDetails> modifiedPMReportSearchList = new ArrayList<PMReportSearchDetails>();
        modifiedPMReportSearchList.add(pmReportSearchDetails);*/

        Intent in = new Intent(PMReportSearchResultActivity.this,PMReportSearchDetailsActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        in.putExtra(PMReportSearchDetailsActivity.KEY_POSITION, 0);
        in.putExtra(PMReportSearchDetailsActivity.KEY_PM_LIST_OBJ, pmReportSearchDetailsList.get(position));
        //in.putParcelableArrayListExtra("PMREPORT_SEARCH_RESULT", (ArrayList<? extends Parcelable>) modifiedPMReportSearchList);

        // Now we put the large data into our enum instead of using Intent extras
       // DataHolder.setData(pmReportSearchDetailsList);

        startActivity(in);
    }
}
