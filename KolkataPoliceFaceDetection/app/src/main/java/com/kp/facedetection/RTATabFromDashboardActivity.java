package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.adapter.RTAPSListAdapter;
import com.kp.facedetection.interfaces.OnItemClickListenerForAllRTAView;
import com.kp.facedetection.model.CaseSearchDetails;
import com.kp.facedetection.model.CrimeReviewDetails;
import com.kp.facedetection.model.LatLongDistance;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

public class RTATabFromDashboardActivity extends BaseActivity implements OnItemClickListenerForAllRTAView, View.OnClickListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String[] key_map;
    private String[] value_map;
    private String[] keyMap_parse;
    private String[] valueMap_parse;
    private String pagenoMap="";
    private String totlaResultMap="";

    private ArrayList<CaseSearchDetails> mapAllList = new ArrayList<CaseSearchDetails>();
    private ArrayList<LatLongDistance> latLongList = new ArrayList<LatLongDistance>();
    KPFaceDetectionApplication kpFaceDetectionApplication;
    public int mapCount = 0;

    private String selectedDate;
    private String selected_div;
    private String selected_ps;
    private String selected_crime;

    private RecyclerView recycler_psList;
    private LinearLayoutManager mLayoutManager;
    private RTAPSListAdapter rtaPsListAdapter;
    private ArrayList<CrimeReviewDetails> rtaPSList = new ArrayList<>();

    private ImageView iv_showMap;
    private TextView tv_resultCount;
    int totalMapCount = 0;
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rtatab_from_dashboard_layout);
        ObservableObject.getInstance().addObserver(this);
        setToolBar();
        initViews();
    }

    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }


    private void initViews() {

        selectedDate = getIntent().getExtras().getString("SELECTED_DATE_RTA");
        selected_div = getIntent().getExtras().getString("SELECTED_DIV_RTA");
        selected_ps = getIntent().getExtras().getString("SELECTED_PS_RTA");
        selected_crime = getIntent().getExtras().getString("SELECTED_CRIME_RTA");

        iv_showMap = (ImageView) findViewById(R.id.iv_showMap);
        iv_showMap.setOnClickListener(this);

        tv_resultCount = (TextView) findViewById(R.id.tv_resultCount);
        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        recycler_psList = (RecyclerView) findViewById(R.id.recycler_psList);
        mLayoutManager = new LinearLayoutManager(this);
        recycler_psList.setLayoutManager(mLayoutManager);
        recycler_psList.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));

        allRTAPSListCall();

        rtaPsListAdapter = new RTAPSListAdapter(this, rtaPSList);
        recycler_psList.setAdapter(rtaPsListAdapter);

        /*
        * click listener to all RTA list item
        * */
        //rtaPsListAdapter.setClickListener(this);


    }

    private void allRTAPSListCall() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_RTA_VIEW);
        taskManager.setAllRTAView(true);

        String[] keys = {"currDate","div","ps","category","user_id"};
        String[] values = {selectedDate.trim(), selected_div.trim(), selected_ps.trim(), selected_crime.trim(),Utility.getUserInfo(this).getUserId()};
        taskManager.doStartTask(keys, values, true);
    }

    public void parseAllRTAViewSearchResult(String response){
        //System.out.println("parseAllRTAViewSearchResult: "+response);
        try {
            JSONObject jObj = new JSONObject(response);
            if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                JSONArray resultArray = jObj.getJSONArray("result");
                parseAllRTAPSResponse(resultArray);
            } else {
                Utility.showAlertDialog(RTATabFromDashboardActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_DETAIL, false);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Utility.showAlertDialog(RTATabFromDashboardActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
        }
    }

    private void parseAllRTAPSResponse(JSONArray resultArray){
        rtaPSList.clear();
        for (int i = 0; i < resultArray.length(); i++) {

            CrimeReviewDetails crimeReviewDetails = new CrimeReviewDetails(Parcel.obtain());
            try {
                JSONObject jObj = resultArray.getJSONObject(i);

                if(jObj.optString("NON_FATAL_COUNT").equalsIgnoreCase("0") && jObj.optString("FATAL_COUNT").equalsIgnoreCase("0") ){

                }
                else{

                    if (jObj.optString("PSNAME") != null && !jObj.optString("PSNAME").equalsIgnoreCase("") && !jObj.optString("PSNAME").equalsIgnoreCase("null")) {
                        crimeReviewDetails.setCrimeCategory(jObj.optString("PSNAME"));
                    }
                    if (jObj.optString("PSCODE") != null && !jObj.optString("PSCODE").equalsIgnoreCase("") && !jObj.optString("PSCODE").equalsIgnoreCase("null")) {
                        crimeReviewDetails.setCode(jObj.optString("PSCODE"));
                    }

                    if (jObj.optString("NON_FATAL_COUNT") != null && !jObj.optString("NON_FATAL_COUNT").equalsIgnoreCase("") && !jObj.optString("NON_FATAL_COUNT").equalsIgnoreCase("null")
                            && !jObj.optString("NON_FATAL_COUNT").equalsIgnoreCase("0")) {
                        crimeReviewDetails.setNonFatalCount(jObj.optString("NON_FATAL_COUNT"));
                        totalMapCount = totalMapCount + Integer.parseInt(jObj.optString("NON_FATAL_COUNT"));
                    }else{
                        crimeReviewDetails.setNonFatalCount(jObj.optString(""));
                    }

                    if (jObj.optString("FATAL_COUNT") != null && !jObj.optString("FATAL_COUNT").equalsIgnoreCase("") && !jObj.optString("FATAL_COUNT").equalsIgnoreCase("null")
                            && !jObj.optString("FATAL_COUNT").equalsIgnoreCase("0")) {

                        crimeReviewDetails.setFatalCount(jObj.optString("FATAL_COUNT"));
                        totalMapCount = totalMapCount + Integer.parseInt(jObj.optString("FATAL_COUNT"));
                    }
                    else{
                        crimeReviewDetails.setFatalCount("");
                    }

                    rtaPSList.add(crimeReviewDetails);
                }


            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
        tv_resultCount.setText("Total Results: "+totalMapCount);
        rtaPsListAdapter.notifyDataSetChanged();
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
           /* case R.id.menu_send_push:
                //tabHost.setCurrentTab(2);
                //startActivity(new Intent(this, SendPushActivity.class));
                //	pushFragments("Push", new SendPushActivity(), true, false);
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View view, int position) {
       //  Toast.makeText(kpFaceDetectionApplication, "item clicked", Toast.LENGTH_SHORT).show();
        final CrimeReviewDetails crimeDetailsForRTA = rtaPSList.get(position);
        String category1="";
        String category2 = "";
        String category = "";

        if (!crimeDetailsForRTA.getNonFatalCount().equalsIgnoreCase("") && !crimeDetailsForRTA.getNonFatalCount().equalsIgnoreCase("null")
                && !crimeDetailsForRTA.getNonFatalCount().equalsIgnoreCase("0")){
            category1 = "'ROAD ACCIDENT (NON-FATAL)'";
        }

        if(!crimeDetailsForRTA.getFatalCount().equalsIgnoreCase("") && !crimeDetailsForRTA.getFatalCount().equalsIgnoreCase("null")
                && !crimeDetailsForRTA.getFatalCount().equalsIgnoreCase("0")){
            category2 = "'ROAD ACCIDENT (FATAL)'";
        }

        if(!category1.equalsIgnoreCase("") && !category2.equalsIgnoreCase("")){
            category = category1 + "," + category2;
        }
        else if(!category1.equalsIgnoreCase("")){
            category = category1;
        }
        else if(!category2.equalsIgnoreCase("")){
            category = category2;
        }
        else{
            category = "";
        }

        String psCode = crimeDetailsForRTA.getCode().toString().trim();

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
        Date dateFrom ;
        String strFromDate="";
        try {
            dateFrom = formatter.parse(selectedDate); //String to Date convert
            strFromDate = formatter1.format(dateFrom); //Date to String convert again for parsing
            System.out.println(" Date : " + strFromDate);
        }
        catch (ParseException e){
            e.printStackTrace();
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_MAP_VIEW);
        taskManager.setMapViewFromRTA(true);

        key_map = new String[]{"div", "ps", "crime_cat", "from_date", "to_date", "caseno", "caseyear", "crimetime", "mod_oper", "complaint", "status", "io", "brief_keyword", "pageno", "inv_unit", "inv_section"};
        value_map = new String[]{selected_div.trim(), "'"+psCode.trim()+"'", category.trim(), strFromDate.trim(), strFromDate.trim(), "", "", "", "", "", "", "", "","1", "", ""};
        taskManager.doStartTask(key_map, value_map, true);
    }

    public void parseAllRTAViewMapViewResult(String result, String[] keysMap, String[] valuesMap){
        //System.out.println("parseAllRTAViewMapViewResult: "+result);

        if (result != null && !result.equals("")) {
            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keyMap_parse = keysMap;
                    this.valueMap_parse = valuesMap;
                    pagenoMap = jObj.opt("page").toString();
                    totlaResultMap = jObj.opt("count").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseMapViewAllResponse(resultArray);
                }
                else {
                    Utility.showAlertDialog(RTATabFromDashboardActivity.this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_NO_LOCATION_FOUND_MSG, false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(RTATabFromDashboardActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }

    private void parseMapViewAllResponse(JSONArray result_array) {
        kpFaceDetectionApplication = KPFaceDetectionApplication.getApplication();
        mapAllList.clear();
        latLongList.clear();
        mapCount = 0;

        for (int i = 0; i < result_array.length(); i++) {

            JSONObject obj = null;
            try {
                obj = result_array.getJSONObject(i);

                LatLongDistance latLongDistance = new LatLongDistance();
                CaseSearchDetails caseMapDetails = new CaseSearchDetails();

                if (!obj.optString("PS").equalsIgnoreCase("null") && !obj.optString("PS").equalsIgnoreCase("") && obj.optString("PS") != null){
                    caseMapDetails.setPs(obj.optString("PS"));
                }

                if (!obj.optString("UNDER_SECTION").equalsIgnoreCase("null") && !obj.optString("UNDER_SECTION").equalsIgnoreCase("") && obj.optString("UNDER_SECTION") != null){
                    caseMapDetails.setUnderSection(obj.optString("UNDER_SECTION"));
                }

                if (!obj.optString("CASEDATE").equalsIgnoreCase("null") && !obj.optString("CASEDATE").equalsIgnoreCase("") && obj.optString("CASEDATE") != null){
                    caseMapDetails.setCaseDate(obj.optString("CASEDATE"));
                }

                if (!obj.optString("CASE_YR").equalsIgnoreCase("null"))
                    caseMapDetails.setCaseYr(obj.optString("CASE_YR"));

                if (!obj.optString("CASENO").equalsIgnoreCase("null"))
                    caseMapDetails.setCaseNo(obj.optString("CASENO"));

                if (!obj.optString("PSCODE").equalsIgnoreCase("null"))
                    caseMapDetails.setPsCode(obj.optString("PSCODE"));

                if (!obj.optString("CATEGORY").equalsIgnoreCase("null"))
                    caseMapDetails.setCategory(obj.optString("CATEGORY"));

                if (!obj.optString("PO_LAT").equalsIgnoreCase("null") && obj.optString("PO_LAT") != null && !obj.optString("PO_LAT").equalsIgnoreCase("")) {
                    caseMapDetails.setPoLat(obj.optString("PO_LAT"));
                }

                if (!obj.optString("PO_LONG").equalsIgnoreCase("null") && obj.optString("PO_LONG") != null && !obj.optString("PO_LONG").equalsIgnoreCase("")) {
                    caseMapDetails.setPoLong(obj.optString("PO_LONG"));
                }

                if (!obj.optString("COLOR_CODE").equalsIgnoreCase("null"))
                    caseMapDetails.setColorCode(obj.optString("COLOR_CODE"));

                mapAllList.add(caseMapDetails);


                if(!obj.optString("PO_LAT").equalsIgnoreCase("") && !obj.optString("PO_LAT").equalsIgnoreCase("null") && obj.optString("PO_LAT") != null){
                    latLongDistance.setLatitude(obj.optString("PO_LAT"));
                    mapCount++;
                }

                latLongList.add(latLongDistance);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(mapCount > 0){

            Intent intent = new Intent(RTATabFromDashboardActivity.this, CaseMapSearchDetailsActivity.class);
            intent.putExtra("keysMap", keyMap_parse);
            intent.putExtra("valueMap", valueMap_parse);
            intent.putExtra("totalResultMap", totlaResultMap);
            intent.putExtra("pagenoMap", pagenoMap);
            intent.putExtra("MAPCOUNT",mapCount);

            // when huge data need to intent using parcellable can not possible, required setter getter class in Application class
            kpFaceDetectionApplication.setCaseMapAllList(mapAllList);

            startActivity(intent);
        }
        else{
            Log.e("Latlong list Size: ",latLongList.size()+"");
            Utility.showAlertDialog(RTATabFromDashboardActivity.this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_NO_LOCATION_FOUND_MSG, false);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.iv_showMap:
                fetchMapView();
                break;
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
                break;

        }
    }


    private void fetchMapView(){

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
        Date dateFrom ;
        String strFromDate="";
        try {
            dateFrom = formatter.parse(selectedDate); //String to Date convert
            strFromDate = formatter1.format(dateFrom); //Date to String convert again for parsing
            System.out.println(" Date : " + strFromDate);
        }
        catch (ParseException e){
            e.printStackTrace();
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_MAP_VIEW);
        taskManager.setMapViewFromRTA(true);

        String[] keys = new String[]{"div", "ps", "crime_cat", "from_date", "to_date", "caseno", "caseyear", "crimetime", "mod_oper", "complaint", "status", "io", "brief_keyword", "pageno", "inv_unit", "inv_section"};
        String[] values = new String[]{selected_div.trim(), selected_ps.trim(), "'ROAD ACCIDENT (NON-FATAL)','ROAD ACCIDENT (FATAL)'", strFromDate.trim(), strFromDate.trim(), "", "", "", "", "", "", "", "","1", "", ""};
        taskManager.doStartTask(keys, values, true);
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
