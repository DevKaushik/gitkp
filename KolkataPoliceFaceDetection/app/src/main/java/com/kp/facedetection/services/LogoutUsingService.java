package com.kp.facedetection.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gcm.GCMRegistrar;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Kaushik on 06-09-2016.
 */
public class LogoutUsingService extends IntentService {


    private static final String TAG = "LogoutUsingService";
    private String responsebody="";


    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public LogoutUsingService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        System.out.println("LogoutUsingService start");
        NetworkOperation();

    }

    private void NetworkOperation(){


        try {

            String logout_url = Constants.API_URL + Constants.METHOD_LOGOUT ;

            URL url = new URL(logout_url);

            JSONObject postDataParams = new JSONObject();
            postDataParams.put("user_id", Utility.getUserInfo(this).getUserId());
            postDataParams.put("device_tokenid", GCMRegistrar.getRegistrationId(this));
            postDataParams.put("logid", Utility.getUserInfo(this).getUser_logid());
            Log.e("params", postDataParams.toString());

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(30000 /* milliseconds */);
            conn.setConnectTimeout(30000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();

            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {

                String readStream = readStream(conn.getInputStream());
                System.out.println("Response "+readStream);

                parseLogoutResponse(readStream);

            }
            else {
            }
        }
        catch(Exception e){

        }

    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }


    public String readStream(InputStream in) {

        StringBuilder sb = new StringBuilder();

        try {
            Reader reader  = new InputStreamReader(in);
            int data = reader.read();
            while (data != -1) {
                char theChar = (char) data;
                data = reader.read();
                sb.append(theChar);

            }

            reader.close();
        }
        catch (Exception e) {

        }
        return sb.toString();
    }

    private void parseLogoutResponse(String response) {
        Log.e("Response", "Response " + response);
        try {
            JSONObject jObj = new JSONObject(response);
            if (jObj != null && jObj.opt("status").toString().equalsIgnoreCase("1")) {
                Utility.logout2(this);
                System.out.println("Logut Successfull");
            }else{

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
