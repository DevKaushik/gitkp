package com.kp.facedetection;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Observable;
import java.util.Observer;

public class FeedbackActivity extends BaseActivity implements View.OnClickListener, Observer {

    private Spinner sp_feedbackTypeValue, sp_IssueTypeValue;
    private RelativeLayout relative_typeSuggestion;
    private RelativeLayout relative_typeIssueReport;
    private EditText et_suggestion, et_issueNote;
    private RatingBar ratingBar_sug, ratingBar_issue;
    private Button btn_suggestionSend, btn_reportSend;

    private String userName="";
    private String loginNumber="";
    private String appVersion = "";
    private String userId = "";

    private ArrayAdapter<CharSequence> feedbackTypeAdapter;
    private ArrayAdapter<CharSequence> issueReportAdapter;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ObservableObject.getInstance().addObserver(this);
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
            userId = Utility.getUserInfo(this).getUserId();

        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = this.getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome "+userName);
        tv_loginCount.setText("Login Count: "+loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        initViews();
    }

    private void initViews() {

        relative_typeSuggestion = (RelativeLayout) findViewById(R.id.relative_typeSuggestion);
        relative_typeIssueReport = (RelativeLayout) findViewById(R.id.relative_typeIssueReport);

        sp_feedbackTypeValue = (Spinner) findViewById(R.id.sp_feedbackTypeValue);
        sp_IssueTypeValue = (Spinner)findViewById(R.id.sp_IssueTypeValue);

        et_suggestion = (EditText)findViewById(R.id.et_suggestion);
        et_issueNote = (EditText) findViewById(R.id.et_issueNote);

        ratingBar_sug = (RatingBar) findViewById(R.id.ratingBar_sug);
        ratingBar_issue = (RatingBar)findViewById(R.id.ratingBar_issue);

        btn_suggestionSend = (Button)findViewById(R.id.btn_suggestionSend);
        btn_reportSend = (Button) findViewById(R.id.btn_reportSend);

        feedbackTypeAdapter = ArrayAdapter.createFromResource(FeedbackActivity.this,R.array.Feedback_Array,R.layout.simple_spinner_item);
        feedbackTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_feedbackTypeValue.setAdapter(feedbackTypeAdapter);
        sp_feedbackTypeValue.setSelection(0);

        Constants.buttonEffect(btn_suggestionSend);
        Constants.buttonEffect(btn_reportSend);

        clickEvent();
    }



    private void clickEvent() {

        sp_feedbackTypeValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 1){
                    relative_typeSuggestion.setVisibility(View.VISIBLE);
                    relative_typeIssueReport.setVisibility(View.GONE);

                }
                else if(position == 2){
                    relative_typeIssueReport.setVisibility(View.VISIBLE);
                    relative_typeSuggestion.setVisibility(View.GONE);

                    issueReportAdapter = ArrayAdapter.createFromResource(FeedbackActivity.this,R.array.Issue_Report_Array,R.layout.simple_spinner_item);
                    issueReportAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    sp_IssueTypeValue.setAdapter(issueReportAdapter);
                    sp_IssueTypeValue.setSelection(0);

                }
                else{
                    relative_typeSuggestion.setVisibility(View.GONE);
                    relative_typeIssueReport.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                relative_typeSuggestion.setVisibility(View.GONE);
                relative_typeIssueReport.setVisibility(View.GONE);
            }
        });

        btn_suggestionSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitSuggestionFeedback();
            }
        });

        btn_reportSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitIssueReportFeedback();
            }
        });
    }


    private void submitSuggestionFeedback(){

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_FEEDBACK);
        taskManager.setFeedbackMenu(true);

        String suggestionStr = et_suggestion.getText().toString().trim();
        String rating= "";

        if(ratingBar_sug.getRating() == 0.00){
            rating = "";
        }
        else{
            rating = String.valueOf(ratingBar_sug.getRating());
        }

        if(!suggestionStr.equalsIgnoreCase("") ){
            String[] keys = { "user_id", "type", "suggestion", "issue_type", "rating"};
            String[] values = {userId.trim(), "1", suggestionStr.trim() , "", rating.trim() };
            taskManager.doStartTask(keys, values, true);

        }
        else{
            Utility.showAlertDialog(this,"Search Error !!!","Please provide the suggestion ",false);
        }

    }


    private void submitIssueReportFeedback(){
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_FEEDBACK);
        taskManager.setFeedbackMenu(true);

        String issueTypeVal = String.valueOf(sp_IssueTypeValue.getSelectedItemPosition());
        String issue_note = et_issueNote.getText().toString().trim();
        String rating = "";

        if(ratingBar_issue.getRating() == 0.00){
            rating = "";
        }else{
            rating = String.valueOf(ratingBar_issue.getRating());
        }

        if (!issueTypeVal.equalsIgnoreCase("0")){
            String[] keys = { "user_id", "type", "suggestion", "issue_type", "rating"};
            String[] values = {userId.trim(), "2", issue_note.trim() , issueTypeVal.trim(), rating.trim() };
            taskManager.doStartTask(keys, values, true);
        }
        else{
            Utility.showAlertDialog(this,"Search Error !!!","Please provide the issue type",false);
        }
    }


    public void parseFeedbackResponse(String response){
        //Utility.showAlertDialog(this,"Result !!!","Successfully submitted",true);
        JSONObject jobj = null;
        try{
            jobj = new JSONObject(response);
            if(jobj != null && jobj.optString("status").equalsIgnoreCase("1")){
                Utility.showAlertDialog(this,"Result !!!","Successfully submitted", true);
            }
            else{
                Utility.showAlertDialog(this,Constants.SEARCH_ERROR_TITLE,Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
        catch(JSONException e){
            e.printStackTrace();
        }
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
           /* case R.id.menu_send_push:
                //tabHost.setCurrentTab(2);
                //startActivity(new Intent(this, SendPushActivity.class));
                //	pushFragments("Push", new SendPushActivity(), true, false);
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                //startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                // Utility.logout(this);
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }


    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
