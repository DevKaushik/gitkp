package com.kp.facedetection;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.LoginActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.SpecialServiceList;


/**
 * Created by user on 20-03-2018.
 */

@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerprintHandler_login extends FingerprintManagerCompat.AuthenticationCallback  {

    private Context context;


    // Constructor
    public FingerprintHandler_login(Context mContext) {
        context = mContext;
    }


    public void startAuth(FingerprintManagerCompat manager, FingerprintManagerCompat.CryptoObject cryptoObject) {
        android.support.v4.os.CancellationSignal cancellationSignal = new android.support.v4.os.CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject,0, cancellationSignal, this, null);
    }


    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
         this.update("Fingerprint Authentication error\n" + errString, false);
        //showAlertDialog(context,Constants.ERROR,"Fingerprint Authentication error",false);

    }


    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
       // this.update("Fingerprint Authentication help\n" + helpString, false);


    }


    @Override
    public void onAuthenticationFailed() {
          this.update("Fingerprint Authentication failed.", false);
        //showAlertDialog(context,Constants.ERROR,"Fingerprint Authentication failed.",false);
    }


    @Override
    public void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
       // this.update("Fingerprint Authentication succeeded.", true);
        ((LoginActivity)context).showAlertDialogFingerPrintSuccess(context,"Success","Fingerprint Authentication succeeded.",true);
    }


    public void update(String e, Boolean success){
        Toast.makeText(context,e,Toast.LENGTH_LONG).show();

    }

}

