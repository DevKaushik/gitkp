package com.kp.facedetection.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.ModifiedWarrantDetailsModel;
import com.kp.facedetection.utils.L;

import java.util.ArrayList;

/**
 * Created by DAT-Asset-128 on 21-11-2017.
 */

public class WarrantModiedListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ModifiedWarrantDetailsModel>modifiedWarrantDetailsModelArrayList;
    public WarrantModiedListAdapter(Context context, ArrayList<ModifiedWarrantDetailsModel>modifiedWarrantDetailsModelArrayList){
        this.context=context;
        this.modifiedWarrantDetailsModelArrayList=modifiedWarrantDetailsModelArrayList;
    }
    @Override
    public int getCount() {
        int size=(modifiedWarrantDetailsModelArrayList.size()>0)? modifiedWarrantDetailsModelArrayList.size():0;
        return size;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null)
        {
            holder = new ViewHolder();
            if(modifiedWarrantDetailsModelArrayList.get(position).getWarrantObjType().equalsIgnoreCase("Header"))
            {
                convertView= LayoutInflater.from(context).inflate(R.layout.warrant_modified_header_layout,parent,false);
                holder.tv_heading=(TextView)convertView.findViewById(R.id.tv_modified_header);
                holder.header_div=(View)convertView.findViewById(R.id.viewLine);
                if (position==0)
                {
                    holder.header_div.setVisibility(View.GONE);
                }
                if(modifiedWarrantDetailsModelArrayList.get(position).getWarrantHeaderType().equalsIgnoreCase("WARRANTEES")) {
                    holder.tv_heading.setText("Details of Warrantee(s)");
                }
                if(modifiedWarrantDetailsModelArrayList.get(position).getWarrantHeaderType().equalsIgnoreCase("WA_NER")) {
                    holder.tv_heading.setText("Non-Execution Reports");
                }
                else if (modifiedWarrantDetailsModelArrayList.get(position).getWarrantHeaderType().equalsIgnoreCase("WA_PROCESSES")){
                    holder.tv_heading.setText("Process Details (Proclamation / OA / Proclaimed Offender)");
                }
                else if(modifiedWarrantDetailsModelArrayList.get(position).getWarrantHeaderType().equalsIgnoreCase("WA_PROPERTIES")){
                    holder.tv_heading.setText("Details of Articles mentioned in O.A");
                }
            }
            if(modifiedWarrantDetailsModelArrayList.get(position).getWarrantObjType().equalsIgnoreCase("WARRANTEES"))
            {
                try {

                    convertView = LayoutInflater.from(context).inflate(R.layout.warrant_warantee_details_item_layout, null);
                    holder.tv_waranteeDetails = (TextView) convertView.findViewById(R.id.tv_warantee_details);
                    holder.tv_waranteeDetails.setText(modifiedWarrantDetailsModelArrayList.get(position).getWarranteeDetails());
                }catch(Exception e)
                {
                    e.printStackTrace();
                }

            }
             if(modifiedWarrantDetailsModelArrayList.get(position).getWarrantObjType().equalsIgnoreCase("WA_NER"))
            {

                convertView= LayoutInflater.from(context).inflate(R.layout.warrant_ner_item_layout,null);
                holder.tv_nerDate=(TextView)convertView.findViewById(R.id.tv_ner_date);
                holder.tv_withWarrant=(TextView)convertView.findViewById(R.id.tv_with_warrant);
                holder.tv_nerRemarks=(TextView)convertView.findViewById(R.id.tv_remarks);
                try {
                    if (!modifiedWarrantDetailsModelArrayList.get(position).getNerDate().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getNerDate().equalsIgnoreCase("null")) {
                        holder.tv_nerDate.setText(modifiedWarrantDetailsModelArrayList.get(position).getNerDate());
                    } else {
                        holder.tv_nerDate.setText("NA");
                    }
                    if (!modifiedWarrantDetailsModelArrayList.get(position).getWithWarrant().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getNerDate().equalsIgnoreCase("null")) {
                        if (modifiedWarrantDetailsModelArrayList.get(position).getWithWarrant().equalsIgnoreCase("Y")) {
                            holder.tv_withWarrant.setText("YES");
                        } else {
                            holder.tv_withWarrant.setText("No");
                        }
                    } else {
                        holder.tv_withWarrant.setText("NA");
                    }
                    L.e("Remarks------------- "+modifiedWarrantDetailsModelArrayList.get(position).getNerRemarks());
                    if (!modifiedWarrantDetailsModelArrayList.get(position).getNerRemarks().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getNerRemarks().equalsIgnoreCase("null")) {
                        holder.tv_nerRemarks.setText(modifiedWarrantDetailsModelArrayList.get(position).getNerRemarks());
                    } else {
                        holder.tv_nerRemarks.setText("NA");
                    }
                }catch(Exception e)
                {

                }
            }
            if(modifiedWarrantDetailsModelArrayList.get(position).getWarrantObjType().equalsIgnoreCase("WA_PROCESSES"))
            {

                convertView= LayoutInflater.from(context).inflate(R.layout.warrant_process_item_layout,null);
                holder.tv_processNo=(TextView)convertView.findViewById(R.id.tv_processNo);
                holder.tv_processType=(TextView)convertView.findViewById(R.id.tv_processType);
                holder.tv_processRequestDate=(TextView)convertView.findViewById(R.id.tv_requestDate);
                holder.tv_processCourtAppr=(TextView)convertView.findViewById(R.id.tv_courtApproved);
                holder.tv_processMemoNo=(TextView)convertView.findViewById(R.id.tv_memoNo);
                holder.tv_processMemodate=(TextView)convertView.findViewById(R.id.tv_memoDate);
                holder.tv_processExecuted=(TextView)convertView.findViewById(R.id.tv_Executed);
                holder.tv_processExecutedDate=(TextView)convertView.findViewById(R.id.tv_Executed_Date);
                holder.tv_processRemarks=(TextView)convertView.findViewById(R.id.tv_Remarks);
                try {
                    if (modifiedWarrantDetailsModelArrayList.get(position).getProcessNo() != null || !modifiedWarrantDetailsModelArrayList.get(position).getProcessNo().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getProcessNo().equalsIgnoreCase("null")) {
                        holder.tv_processNo.setText(modifiedWarrantDetailsModelArrayList.get(position).getProcessNo());
                    } else {
                        holder.tv_processNo.setText("NA");

                    }
                    if (modifiedWarrantDetailsModelArrayList.get(position).getProcessType() != null || !modifiedWarrantDetailsModelArrayList.get(position).getProcessType().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getProcessType().equalsIgnoreCase("null")) {

                        holder.tv_processType.setText(modifiedWarrantDetailsModelArrayList.get(position).getProcessType());
                    } else {
                        holder.tv_processType.setText("NA");
                    }

                    if (modifiedWarrantDetailsModelArrayList.get(position).getProcessRequestDate() != null || !modifiedWarrantDetailsModelArrayList.get(position).getProcessRequestDate().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getProcessRequestDate().equalsIgnoreCase("null")) {

                        holder.tv_processRequestDate.setText(modifiedWarrantDetailsModelArrayList.get(position).getProcessRequestDate());
                    } else {
                        holder.tv_processRequestDate.setText("NA");
                    }
                    if (modifiedWarrantDetailsModelArrayList.get(position).getProcessCourtAppr() != null || !modifiedWarrantDetailsModelArrayList.get(position).getProcessCourtAppr().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getProcessCourtAppr().equalsIgnoreCase("null")) {

                        holder.tv_processCourtAppr.setText(modifiedWarrantDetailsModelArrayList.get(position).getProcessCourtAppr());
                    } else {
                        holder.tv_processCourtAppr.setText("NA");
                    }
                    if (modifiedWarrantDetailsModelArrayList.get(position).getProcessMemoNo() != null || !modifiedWarrantDetailsModelArrayList.get(position).getProcessMemoNo().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getProcessMemoNo().equalsIgnoreCase("null")) {

                        holder.tv_processMemoNo.setText(modifiedWarrantDetailsModelArrayList.get(position).getProcessMemoNo());
                    } else {
                        holder.tv_processMemoNo.setText("NA");
                    }
                    if (modifiedWarrantDetailsModelArrayList.get(position).getProcessMemodate() != null || !modifiedWarrantDetailsModelArrayList.get(position).getProcessMemodate().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getProcessMemodate().equalsIgnoreCase("null")) {
                        holder.tv_processMemodate.setText(modifiedWarrantDetailsModelArrayList.get(position).getProcessMemodate());
                    } else {
                        holder.tv_processMemodate.setText("NA");

                    }
                    if (modifiedWarrantDetailsModelArrayList.get(position).getProcessExecuted() != null || !modifiedWarrantDetailsModelArrayList.get(position).getProcessExecuted().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getProcessExecuted().equalsIgnoreCase("null")) {
                        holder.tv_processExecuted.setText(modifiedWarrantDetailsModelArrayList.get(position).getProcessExecuted());
                    } else {
                        holder.tv_processExecuted.setText("NA");
                    }
                    if (modifiedWarrantDetailsModelArrayList.get(position).getProcessExecutedDate() != null || !modifiedWarrantDetailsModelArrayList.get(position).getProcessExecutedDate().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getProcessExecutedDate().equalsIgnoreCase("null")) {
                        holder.tv_processExecutedDate.setText(modifiedWarrantDetailsModelArrayList.get(position).getProcessExecutedDate());
                    } else {
                        holder.tv_processExecutedDate.setText("NA");

                    }
                    if (modifiedWarrantDetailsModelArrayList.get(position).getProcessRemarks() != null || !modifiedWarrantDetailsModelArrayList.get(position).getProcessRemarks().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getProcessRemarks().equalsIgnoreCase("null")) {
                        holder.tv_processRemarks.setText(modifiedWarrantDetailsModelArrayList.get(position).getProcessRemarks());
                    } else {
                        holder.tv_processRemarks.setText("NA");
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }

            }
             if(modifiedWarrantDetailsModelArrayList.get(position).getWarrantObjType().equalsIgnoreCase("WA_PROPERTIES"))
            {

                convertView= LayoutInflater.from(context).inflate(R.layout.warrant_properties_item_layout,null);
                holder.tv_propertiesSlNo=(TextView)convertView.findViewById(R.id.tv_properties_sl_no);
                holder.tv_propertiesArticle=(TextView)convertView.findViewById(R.id.tv_properties_article);
                holder.tv_propertiesValuation=(TextView)convertView.findViewById(R.id.tv_properties_valuation);
                try {
                    if (!modifiedWarrantDetailsModelArrayList.get(position).getPropertiesSlNo().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getPropertiesSlNo().equalsIgnoreCase("null")) {
                        holder.tv_propertiesSlNo.setText(modifiedWarrantDetailsModelArrayList.get(position).getPropertiesSlNo());
                    } else {
                        holder.tv_propertiesSlNo.setText("NA");

                    }
                    if (!modifiedWarrantDetailsModelArrayList.get(position).getPropertiesArticle().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getPropertiesArticle().equalsIgnoreCase("null")) {
                        holder.tv_propertiesArticle.setText(modifiedWarrantDetailsModelArrayList.get(position).getPropertiesArticle());
                    } else {
                        holder.tv_propertiesArticle.setText("NA");

                    }
                    if (!modifiedWarrantDetailsModelArrayList.get(position).getPropertiesValuation().equalsIgnoreCase("") || !modifiedWarrantDetailsModelArrayList.get(position).getPropertiesValuation().equalsIgnoreCase("null")) {
                        holder.tv_propertiesValuation.setText(modifiedWarrantDetailsModelArrayList.get(position).getPropertiesValuation());
                    } else {
                        holder.tv_propertiesValuation.setText("NA");
                    }
                }catch(Exception e){

                }


            }

        }

        return convertView;
    }
    public void setHeader(String header){

    }
    class ViewHolder {

        TextView tv_waranteeDetails,tv_heading,tv_nerDate,tv_withWarrant,tv_nerRemarks,tv_processNo,tv_processType,tv_processRequestDate,tv_processCourtAppr,tv_processMemoNo,tv_processMemodate,tv_processExecuted,tv_processExecutedDate,tv_processRemarks,tv_propertiesSlNo,tv_propertiesArticle,tv_propertiesValuation;
         View header_div;
    }
}
