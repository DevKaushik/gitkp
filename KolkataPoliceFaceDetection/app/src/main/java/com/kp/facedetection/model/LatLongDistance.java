package com.kp.facedetection.model;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

/**
 * Created by DAT-165 on 04-04-2017.
 */

public class LatLongDistance implements Serializable{

    private LatLng startLatLng;
    private LatLng endLatLng;
    private double distance;
    private String Latitude;
    private String longitude;

    public LatLng getStartLatLng() {
        return startLatLng;
    }

    public void setStartLatLng(LatLng startLatLng) {
        this.startLatLng = startLatLng;
    }

    public LatLng getEndLatLng() {
        return endLatLng;
    }

    public void setEndLatLng(LatLng endLatLng) {
        this.endLatLng = endLatLng;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
