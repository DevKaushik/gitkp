package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.kp.facedetection.model.WarrantExecDetailModel;
import com.kp.facedetection.model.WarrantiesDetailOfDashboardWarrantModel;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.OnSwipeTouchListener;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class WarrantPendingDetailsActivity extends BaseActivity implements View.OnClickListener {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String selectedDate;
    private String psCode;
    private String selected_div;

    private String[] keysNBW;
    private String[] valuesNBW;
    private String[] keysBW;
    private String[] valuesBW;
    private int pageNoNBW;
    private int pageNoBW;
    private int totalNBWCount;
    private int totalBWCount;

    private TextView tv_nbw;
    private TextView tv_bw;
    private View nbwIndicator, bwIndicator;

    // Views for NBW
    private TextView tv_waNoValue;
    private TextView tv_returnDateValue;
    private TextView tv_statusValue;
    private TextView tv_caseRefValue, tv_caseRef;
    private TextView tv_issuedByVal;
    private TextView tv_processNoValue;
    private TextView tv_officerNameValue;
    private TextView tv_actionDateValue;
    private TextView tv_actionDate;
    private LinearLayout ll_otherDetails;
    private ScrollView ll_dataShow;
    private LinearLayout ll_noDataShow;
    private LinearLayout ll_NBW;

    // Views for BW
    private TextView tv_waNoValue_BW;
    private TextView tv_returnDateValue_BW;
    private TextView tv_statusValue_BW;
    private TextView tv_caseRefValue_BW, tv_caseRef_BW;
    private TextView tv_issuedByVal_BW;
    private TextView tv_processNoValue_BW;
    private TextView tv_officerNameValue_BW;
    private TextView tv_actionDateValue_BW;
    private TextView tv_actionDate_BW;
    private LinearLayout ll_otherDetails_BW;
    private ScrollView ll_dataShow_BW;
    private LinearLayout ll_noDataShow_BW;
    private LinearLayout ll_BW;

    private RelativeLayout bt_prevWA, bt_nextWA, rl_navigate, bottomView;
    private TextView tv_next, tv_prev;

    private View includeView;
    private ArrayList<WarrantExecDetailModel> warrantDetailsList = new ArrayList<>();
    private ArrayList<WarrantiesDetailOfDashboardWarrantModel> warrantiesList = new ArrayList<>();
    private WarrantExecDetailModel warrantExecDetailModel;

    private ArrayList<WarrantExecDetailModel> warrantDetailsList_BW = new ArrayList<>();
    private ArrayList<WarrantiesDetailOfDashboardWarrantModel> warrantiesList_BW = new ArrayList<>();
    private WarrantExecDetailModel warrantExecDetailModel_BW;

    private boolean isNBWSelected = false;
    private boolean isBWSelected = false;

    private int pgNBW = 0;
    private int pgBW = 0;

    Boolean isDataInNbw = false;
    private TextView txt_count;

    private String tag = "";
    int wa_position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.warrant_exc_layout);

        setToolBar();
        initViews();
    }

    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }


    private void initViews() {
        selectedDate = getIntent().getExtras().getString("SELECT_DATE");
        psCode = getIntent().getExtras().getString("PS_CODE");
        selected_div = getIntent().getExtras().getString("SELECT_DIV");
        wa_position = getIntent().getIntExtra("WA_POSITION", 0);

        includeView = findViewById(R.id.warrantData);
        tv_waNoValue = (TextView) includeView.findViewById(R.id.tv_waNoValue);
        tv_returnDateValue = (TextView) includeView.findViewById(R.id.tv_returnDateValue);
        tv_statusValue = (TextView) includeView.findViewById(R.id.tv_statusValue);
        tv_caseRef = (TextView) includeView.findViewById(R.id.tv_caseRef);
        tv_caseRefValue = (TextView) includeView.findViewById(R.id.tv_caseRefValue);
        tv_issuedByVal = (TextView) includeView.findViewById(R.id.tv_issuedByVal);
        tv_processNoValue = (TextView) includeView.findViewById(R.id.tv_processNoValue);
        tv_officerNameValue = (TextView) includeView.findViewById(R.id.tv_officerNameValue);
        tv_actionDateValue = (TextView) includeView.findViewById(R.id.tv_actionDateValue);
        tv_actionDateValue.setVisibility(View.VISIBLE);

        tv_actionDate = (TextView) includeView.findViewById(R.id.tv_actionDate);
        tv_actionDate.setVisibility(View.VISIBLE);

        ll_otherDetails = (LinearLayout) includeView.findViewById(R.id.ll_otherDetails);
        ll_NBW = (LinearLayout) includeView.findViewById(R.id.ll_NBW);
        ll_dataShow = (ScrollView) includeView.findViewById(R.id.ll_dataShow);
        ll_noDataShow = (LinearLayout) includeView.findViewById(R.id.ll_noDataShow);

        tv_waNoValue_BW = (TextView) includeView.findViewById(R.id.tv_waNoValue_BW);
        tv_returnDateValue_BW = (TextView) includeView.findViewById(R.id.tv_returnDateValue_BW);
        tv_statusValue_BW = (TextView) includeView.findViewById(R.id.tv_statusValue_BW);
        tv_caseRefValue_BW = (TextView) includeView.findViewById(R.id.tv_caseRefValue_BW);
        tv_caseRef_BW = (TextView) includeView.findViewById(R.id.tv_caseRefBW);
        tv_issuedByVal_BW = (TextView) includeView.findViewById(R.id.tv_issuedByVal_BW);
        tv_processNoValue_BW = (TextView) includeView.findViewById(R.id.tv_processNoValue_BW);
        tv_officerNameValue_BW = (TextView) includeView.findViewById(R.id.tv_officerNameValue_BW);
        tv_actionDateValue_BW = (TextView) includeView.findViewById(R.id.tv_actionDateValue_BW);
        tv_actionDateValue_BW.setVisibility(View.VISIBLE);
        tv_actionDate_BW = (TextView) includeView.findViewById(R.id.tv_actionDate_BW);
        tv_actionDate_BW.setVisibility(View.VISIBLE);

        ll_otherDetails_BW = (LinearLayout) includeView.findViewById(R.id.ll_otherDetails_BW);
        ll_BW = (LinearLayout) includeView.findViewById(R.id.ll_BW);
        ll_dataShow_BW = (ScrollView) includeView.findViewById(R.id.ll_dataShow_BW);
        ll_noDataShow_BW = (LinearLayout) includeView.findViewById(R.id.ll_noDataShow_BW);

        nbwIndicator = findViewById(R.id.nbwIndicator);
        bwIndicator = findViewById(R.id.bwIndicator);
        nbwIndicator.setBackgroundColor(getResources().getColor(R.color.color_light_blue));
        bwIndicator.setBackgroundColor(getResources().getColor(R.color.color_indicator));

        txt_count = (TextView) findViewById(R.id.txt_count);
        tv_bw = (TextView) findViewById(R.id.tv_bw);
        tv_bw.setOnClickListener(this);

        tv_nbw = (TextView) findViewById(R.id.tv_nbw);
        tv_nbw.setOnClickListener(this);

        // first show NBW as default
        isNBWSelected = true;
        isBWSelected = false;
        ll_NBW.setVisibility(View.VISIBLE);
        ll_BW.setVisibility(View.GONE);

        tv_next = (TextView) findViewById(R.id.tv_next);
        tv_prev = (TextView) findViewById(R.id.tv_prev);
        rl_navigate = (RelativeLayout) findViewById(R.id.rl_navigate);
        bottomView = (RelativeLayout) findViewById(R.id.bottomView);

        bt_nextWA = (RelativeLayout) findViewById(R.id.bt_nextWA);
        bt_nextWA.setVisibility(View.GONE);
        bt_nextWA.setOnClickListener(this);

        bt_prevWA = (RelativeLayout) findViewById(R.id.bt_prevWA);
        bt_prevWA.setVisibility(View.GONE);
        bt_prevWA.setOnClickListener(this);

        warrantNbwAPICall(1);
        warrantBWAPICall(1);

        swipePagerCall();

    }

    private void warrantNbwAPICall(int pageNo) {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_WA_PENDING_DETAILS);
        taskManager.setWaPendingDetailSearchForNBW(true);

        keysNBW = new String[]{"ps", "currDate", "watype", "pageno", "div"};
        valuesNBW = new String[]{psCode.trim(), selectedDate.trim(), "NBW", "" + pageNo, selected_div.trim()};
        taskManager.doStartTask(keysNBW, valuesNBW, true);
    }

    public void parseWarrantPendingResultForNBW(String response, String[] keys, String[] values) {
        //System.out.println("parseWarrantPendingResultForNBW" + response);

        try {
            JSONObject jObj = new JSONObject(response);
            if (jObj.optString("status").equalsIgnoreCase("1")) {

                isDataInNbw = true;
                if (isNBWSelected) {
                    showDataNBW();

                } else {
                    // NBW not selected,and if data exists set all data
                    parentHideWithAlldataSetNBW();
                }

                JSONArray resultArray = jObj.optJSONArray("result");
                pageNoNBW = Integer.parseInt(jObj.optString("pageno"));
                totalNBWCount = Integer.parseInt(jObj.optString("total"));

                parseAllWarrantExecData(resultArray);
                buttonVisibilityForNbw();
            } else {

                noDataTextSet_NBW();
                if (isNBWSelected) {

                    showNoDataFoundNBW();
                    isDataInNbw = false;
                } else {
                    // NBW not selected,and no data set text
                    parentInvisibleWithNoDataNBW();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void parseAllWarrantExecData(JSONArray resultArray) {

        if (resultArray != null && !resultArray.equals("")) {
            warrantDetailsList.clear();
            warrantiesList.clear();

            warrantExecDetailModel = new WarrantExecDetailModel();

            for (int i = 0; i < resultArray.length(); i++) {
                JSONObject object = resultArray.optJSONObject(i);


                warrantExecDetailModel.setWaNo(object.optString("WANO"));
                warrantExecDetailModel.setReturnableDtaeToCourt(object.optString("RETURNABLE_DATE_TO_COURT"));
                warrantExecDetailModel.setActionDate(object.optString("ACTION_DATE"));
                warrantExecDetailModel.setWaStatus(object.optString("WA_STATUS"));
                warrantExecDetailModel.setPsCode(object.optString("PS"));
                warrantExecDetailModel.setWaCaseNo(object.optString("CASENO"));
                warrantExecDetailModel.setWaYear(object.optString("WA_YEAR"));
                warrantExecDetailModel.setFirYear(object.optString("FIR_YR"));
                warrantExecDetailModel.setUnderSec(object.optString("UNDER_SECTIONS"));
                warrantExecDetailModel.setIssueCourt(object.optString("ISSUE_COURT"));
                warrantExecDetailModel.setProcessNo(object.optString("PROCESS_NO"));
                warrantExecDetailModel.setOfficerName(object.optString("IONAME"));
                warrantExecDetailModel.setWaCat(object.optString("WACAT"));
                warrantExecDetailModel.setColorStatus(object.optString("fail_normal"));
                warrantExecDetailModel.setWaDate(object.optString("casedate"));


                JSONArray warrantiesArray = object.optJSONArray("warrenties");

                for (int j = 0; j < warrantiesArray.length(); j++) {
                    JSONObject warrantiesObj = warrantiesArray.optJSONObject(j);
                    WarrantiesDetailOfDashboardWarrantModel warrantiesModel = new WarrantiesDetailOfDashboardWarrantModel();
                    warrantiesModel.setWaName(warrantiesObj.optString("NAME"));
                    warrantiesModel.setWaAddress(warrantiesObj.optString("ADDRESS"));
                    warrantiesModel.setWaFatherName(warrantiesObj.optString("FATHER_NAME"));
                    warrantExecDetailModel.setWarranties(warrantiesModel);
                }
            }

            showAllData();
        }
    }


    private void showAllData() {

        String caseRef = "";

        if (!warrantExecDetailModel.getWaNo().equalsIgnoreCase("null")) {
            String waNo = "";
            if (!warrantExecDetailModel.getWaCat().equalsIgnoreCase("null")) {
                waNo = warrantExecDetailModel.getWaCat() + "-";
            }
            if (!warrantExecDetailModel.getWaNo().equalsIgnoreCase("null")) {
                waNo = waNo + warrantExecDetailModel.getWaNo();
            }
            if (!warrantExecDetailModel.getActionDate().equalsIgnoreCase("null")) {
                waNo = waNo + " issued on " + warrantExecDetailModel.getActionDate();
            }
            tv_waNoValue.setText(waNo);
//            tv_waNoValue.setText(warrantExecDetailModel.getWaNo());
        } else {
            tv_waNoValue.setText("NA");
        }

        if (!warrantExecDetailModel.getReturnableDtaeToCourt().equalsIgnoreCase("null")) {
            //  tv_returnDateValue.setText(warrantExecDetailModel.getReturnableDtaeToCourt());
            //     long day= DateUtils.overDueDay(warrantExecDetailModel.getReturnableDtaeToCourt());

            DateUtils.overDueDay(this, warrantExecDetailModel.getReturnableDtaeToCourt(), tv_returnDateValue, warrantExecDetailModel.getColorStatus());

        } else {
            tv_returnDateValue.setText("NA");
        }

        if (warrantExecDetailModel.getActionDate() != null && !warrantExecDetailModel.getActionDate().equalsIgnoreCase("null")) {
            tv_actionDateValue.setText(warrantExecDetailModel.getActionDate());
        } else {
            tv_actionDateValue.setText("NA");
        }

        if (!warrantExecDetailModel.getWaStatus().equalsIgnoreCase("null")) {
            // tv_statusValue.setText(warrantExecDetailModel.getWaStatus());

            if (!warrantExecDetailModel.getColorStatus().equalsIgnoreCase("null")) {
                DateUtils.setStatusColor(this, tv_statusValue, warrantExecDetailModel.getColorStatus(), warrantExecDetailModel.getWaStatus());

            } else {
                tv_statusValue.setText(warrantExecDetailModel.getWaStatus());
            }
        } else {
            tv_statusValue.setText("NA");
        }

        // In the warrant details - Case Ref., the value of CASEPS should be displayed, if case no is not null. 17-07-17
        if (warrantExecDetailModel.getWaCaseNo().equalsIgnoreCase("null")) {
            tv_caseRefValue.setVisibility(View.GONE);
            tv_caseRef.setVisibility(View.GONE);
        } else {
            if (!warrantExecDetailModel.getPsCode().equalsIgnoreCase("null")) {
                caseRef = "Sec. " + warrantExecDetailModel.getPsCode();
            }
            if (!warrantExecDetailModel.getWaCaseNo().equalsIgnoreCase("null")) {
                caseRef = caseRef + " C/No. " + warrantExecDetailModel.getWaCaseNo();
            }
            if (!warrantExecDetailModel.getWaDate().equalsIgnoreCase("null") && !warrantExecDetailModel.getWaDate().equalsIgnoreCase("") /*&& !warrantExecDetailModel.getWaDate().equalsIgnoreCase(null)*/) {
                caseRef = caseRef + " of " + warrantExecDetailModel.getWaDate();
            }
            else if (!warrantExecDetailModel.getFirYear().equalsIgnoreCase("null")) {
                caseRef = caseRef + " of " + warrantExecDetailModel.getFirYear();
            }
            if (!warrantExecDetailModel.getUnderSec().equalsIgnoreCase("null")) {
                caseRef = caseRef + "\nu/s " + warrantExecDetailModel.getUnderSec() + " IPC";
            }
            tv_caseRefValue.setText(caseRef);
        }
        tv_caseRefValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callCaseFirDetails(warrantExecDetailModel.getPsCode(), warrantExecDetailModel.getWaCaseNo(), warrantExecDetailModel.getFirYear());

                // Toast.makeText(WarrantExcDetailsActivityNew.this, "Warrent executed clicked C/No:"+ warrantExecDetailModel.getWaCaseNo(), Toast.LENGTH_SHORT).show();
            }
        });


        if (!warrantExecDetailModel.getIssueCourt().equalsIgnoreCase("null")) {
            tv_issuedByVal.setText(warrantExecDetailModel.getIssueCourt());
        } else {
            tv_issuedByVal.setText("NA");
        }

        if (!warrantExecDetailModel.getProcessNo().equalsIgnoreCase("null")) {
            tv_processNoValue.setText(warrantExecDetailModel.getProcessNo());
        } else {
            tv_processNoValue.setText("NA");
        }

        if (!warrantExecDetailModel.getOfficerName().equalsIgnoreCase("null")) {
            tv_officerNameValue.setText(warrantExecDetailModel.getOfficerName());
        } else {
            tv_officerNameValue.setText("NA");
        }

        ll_otherDetails.removeAllViews();
        if (warrantExecDetailModel.getWarranties().size() > 0) {

            int length = warrantExecDetailModel.getWarranties().size();

            for (int i = 0; i < length; i++) {

                Log.e("TAG", "TAG TAG " + i);
                WarrantiesDetailOfDashboardWarrantModel model = warrantExecDetailModel.getWarranties().get(i);
                TextView warrantiesNameAddressDetail = new TextView(WarrantPendingDetailsActivity.this);

                String detail = "";
                if (!model.getWaName().equalsIgnoreCase("null"))
                    detail = model.getWaName().toString().trim();
                if (!model.getWaFatherName().equalsIgnoreCase("null"))
                    detail = detail + " S/O Of " + model.getWaFatherName().toString().trim();
                if (!model.getWaAddress().equalsIgnoreCase("null"))
                    detail = detail + ", " + model.getWaAddress().toString().trim();

                warrantiesNameAddressDetail.setText(i + 1 + ". " + detail + "\n");
                warrantiesNameAddressDetail.setTextColor(ContextCompat.getColor(WarrantPendingDetailsActivity.this, R.color.color_black));
                ll_otherDetails.addView(warrantiesNameAddressDetail);
            }
        } else {
            TextView noDetail = new TextView(WarrantPendingDetailsActivity.this);
            noDetail.setText("No details for warrantee");
            noDetail.setTextColor(ContextCompat.getColor(WarrantPendingDetailsActivity.this, R.color.color_black));
            ll_otherDetails.addView(noDetail);
            ll_otherDetails.setGravity(Gravity.CENTER);
        }
    }


    //API call for BW
    private void warrantBWAPICall(int pageNo) {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_WA_PENDING_DETAILS);
        taskManager.setWaPendingDetailSearchForBW(true);

        keysBW = new String[]{"ps", "currDate", "watype", "pageno", "div"};
        valuesBW = new String[]{psCode.trim(), selectedDate.trim(), "BW", "" + pageNo, selected_div.trim()};
        taskManager.doStartTask(keysBW, valuesBW, true);
    }

    //Response handle for BW
    public void parseWarrantPendingResultForBW(String response, String[] keys, String[] values) {
        //System.out.println("parseWarrantPendingResultForBW" + response);

        try {
            JSONObject jObj = new JSONObject(response);

            if (jObj.optString("status").equalsIgnoreCase("1")) {

                // select BW tab i.e. no data in NBW
                if (!isDataInNbw && isNBWSelected) {
                    isBWSelected = true;
                    isNBWSelected = false;

                    ll_NBW.setVisibility(View.GONE);
                    ll_BW.setVisibility(View.VISIBLE);

                    tv_bw.setEnabled(false);
                    tv_nbw.setEnabled(true);

                    bwIndicator.setBackgroundColor(getResources().getColor(R.color.color_light_blue));
                    nbwIndicator.setBackgroundColor(getResources().getColor(R.color.color_indicator));

                    buttonVisibilityForBw();
                }

                if (isBWSelected) {
                    showDataBW();
                } else {
                    parentHideWithAlldataSetBW();
                }

                JSONArray resultArray = jObj.optJSONArray("result");
                pageNoBW = Integer.parseInt(jObj.optString("pageno"));
                totalBWCount = Integer.parseInt(jObj.optString("total"));

                parseAllWarrantExecDataForBW(resultArray);
                if (isBWSelected) {
                    buttonVisibilityForBw();
                } else {

                }

            } else {
                noDataTextSet_BW();

                if (isBWSelected) {

                    isDataInNbw = false;
                    showNoDataFoundBW();
                } else {
                    parentInvisibleWithNoDataBW();
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseAllWarrantExecDataForBW(JSONArray resultArray) {
        if (resultArray != null && !resultArray.equals("")) {

            warrantDetailsList_BW.clear();
            warrantiesList_BW.clear();

            warrantExecDetailModel_BW = new WarrantExecDetailModel();

            for (int i = 0; i < resultArray.length(); i++) {

                JSONObject object = resultArray.optJSONObject(i);

                warrantExecDetailModel_BW.setWaNo(object.optString("WANO"));
                warrantExecDetailModel_BW.setReturnableDtaeToCourt(object.optString("RETURNABLE_DATE_TO_COURT"));
                warrantExecDetailModel_BW.setWaStatus(object.optString("WA_STATUS"));
                warrantExecDetailModel_BW.setPsCode(object.optString("PS"));
                warrantExecDetailModel_BW.setWaCaseNo(object.optString("CASENO"));
                warrantExecDetailModel_BW.setWaYear(object.optString("WA_YEAR"));
                warrantExecDetailModel_BW.setFirYear(object.optString("FIR_YR"));
                warrantExecDetailModel_BW.setUnderSec(object.optString("UNDER_SECTIONS"));
                warrantExecDetailModel_BW.setIssueCourt(object.optString("ISSUE_COURT"));
                warrantExecDetailModel_BW.setProcessNo(object.optString("PROCESS_NO"));
                warrantExecDetailModel_BW.setOfficerName(object.optString("IONAME"));
                warrantExecDetailModel_BW.setWaCat(object.optString("WACAT"));
                warrantExecDetailModel_BW.setActionDate(object.optString("ACTION_DATE"));
                warrantExecDetailModel_BW.setColorStatus(object.optString("fail_normal"));
                warrantExecDetailModel_BW.setWaDate(object.optString("casedate"));

                JSONArray warrantiesArray = object.optJSONArray("warrenties");

                for (int j = 0; j < warrantiesArray.length(); j++) {

                    JSONObject warrantiesObj = warrantiesArray.optJSONObject(j);

                    WarrantiesDetailOfDashboardWarrantModel warrantiesModel = new WarrantiesDetailOfDashboardWarrantModel();
                    warrantiesModel.setWaName(warrantiesObj.optString("NAME"));
                    warrantiesModel.setWaAddress(warrantiesObj.optString("ADDRESS"));
                    warrantiesModel.setWaFatherName(warrantiesObj.optString("FATHER_NAME"));
                    warrantExecDetailModel_BW.setWarranties(warrantiesModel);
                }
            }

            showAllData_BW();
        }
    }


    private void showAllData_BW() {

        String caseRef = "";

        if (!warrantExecDetailModel_BW.getWaNo().equalsIgnoreCase("null")) {
            tv_waNoValue_BW.setText(warrantExecDetailModel_BW.getWaNo());
        } else {
            tv_waNoValue_BW.setText("NA");
        }

        if (!warrantExecDetailModel_BW.getReturnableDtaeToCourt().equalsIgnoreCase("null")) {
            //tv_returnDateValue_BW.setText(warrantExecDetailModel_BW.getReturnableDtaeToCourt());

            DateUtils.overDueDay(this, warrantExecDetailModel_BW.getReturnableDtaeToCourt(), tv_returnDateValue_BW, warrantExecDetailModel_BW.getColorStatus());

        } else {
            tv_returnDateValue_BW.setText("NA");
        }

        if (warrantExecDetailModel_BW.getActionDate() != null && !warrantExecDetailModel_BW.getActionDate().equalsIgnoreCase("null")) {
            tv_actionDateValue_BW.setText(warrantExecDetailModel_BW.getActionDate());
        } else {
            tv_actionDateValue_BW.setText("NA");
        }

        if (!warrantExecDetailModel_BW.getWaStatus().equalsIgnoreCase("null")) {
            // tv_statusValue_BW.setText(warrantExecDetailModel_BW.getWaStatus());
            if (!warrantExecDetailModel_BW.getColorStatus().equalsIgnoreCase("null")) {
                DateUtils.setStatusColor(this, tv_statusValue_BW, warrantExecDetailModel_BW.getColorStatus(), warrantExecDetailModel_BW.getWaStatus());

            } else {
                tv_statusValue_BW.setText(warrantExecDetailModel_BW.getWaStatus());
            }

        } else {
            tv_statusValue_BW.setText("NA");
        }

        // In the warrant details - Case Ref., the value of CASEPS should be displayed, if case no is not null. 17-07-17
        if (warrantExecDetailModel_BW.getWaCaseNo().equalsIgnoreCase("null")) {
            tv_caseRefValue_BW.setVisibility(View.GONE);
            tv_caseRef_BW.setVisibility(View.GONE);
        } else {
            if (!warrantExecDetailModel_BW.getPsCode().equalsIgnoreCase("null")) {
                caseRef = "Sec. " + warrantExecDetailModel_BW.getPsCode();
            }
            if (!warrantExecDetailModel_BW.getWaCaseNo().equalsIgnoreCase("null")) {
                caseRef = caseRef + " C/No. " + warrantExecDetailModel_BW.getWaCaseNo();
            }
            if (!warrantExecDetailModel_BW.getWaDate().equalsIgnoreCase("null") && !warrantExecDetailModel_BW.getWaDate().equalsIgnoreCase("") /*&& !warrantExecDetailModel.getWaDate().equalsIgnoreCase(null)*/) {
                caseRef = caseRef + "of " + warrantExecDetailModel_BW.getWaDate();
            }
            else if (!warrantExecDetailModel_BW.getFirYear().equalsIgnoreCase("null")) {
                caseRef = caseRef + "of " + warrantExecDetailModel_BW.getFirYear();
            }
            if (!warrantExecDetailModel_BW.getUnderSec().equalsIgnoreCase("null")) {
                caseRef = caseRef + "\nu/s " + warrantExecDetailModel_BW.getUnderSec();
            }
            tv_caseRefValue_BW.setText(caseRef);
        }
        tv_caseRefValue_BW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callCaseFirDetails(warrantExecDetailModel_BW.getPsCode(), warrantExecDetailModel_BW.getWaCaseNo(), warrantExecDetailModel_BW.getFirYear());
            }
        });


        if (!warrantExecDetailModel_BW.getIssueCourt().equalsIgnoreCase("null")) {
            tv_issuedByVal_BW.setText(warrantExecDetailModel_BW.getIssueCourt());
        } else {
            tv_issuedByVal_BW.setText("NA");
        }

        if (!warrantExecDetailModel_BW.getProcessNo().equalsIgnoreCase("null")) {
            tv_processNoValue_BW.setText(warrantExecDetailModel_BW.getProcessNo());
        } else {
            tv_processNoValue_BW.setText("NA");
        }

        if (!warrantExecDetailModel_BW.getOfficerName().equalsIgnoreCase("null")) {
            tv_officerNameValue_BW.setText(warrantExecDetailModel_BW.getOfficerName());
        } else {
            tv_officerNameValue_BW.setText("NA");
        }

        ll_otherDetails_BW.removeAllViews();
        if (warrantExecDetailModel_BW.getWarranties().size() > 0) {

            int length = warrantExecDetailModel_BW.getWarranties().size();

            for (int i = 0; i < length; i++) {
                //ll_otherDetails.removeAllViews();
                Log.e("TAG", "TAG TAG " + i);
                WarrantiesDetailOfDashboardWarrantModel model = warrantExecDetailModel_BW.getWarranties().get(i);
                TextView warrantiesNameAddressDetail = new TextView(WarrantPendingDetailsActivity.this);

                String detail = "";
                if (!model.getWaName().equalsIgnoreCase("null"))
                    detail = model.getWaName().toString().trim();
                if (!model.getWaFatherName().equalsIgnoreCase("null"))
                    detail = detail + " S/O Of " + model.getWaFatherName().toString().trim();
                if (!model.getWaAddress().equalsIgnoreCase("null"))
                    detail = detail + ", " + model.getWaAddress().toString().trim();

                warrantiesNameAddressDetail.setText(i + 1 + ". " + detail + "\n");
                warrantiesNameAddressDetail.setTextColor(ContextCompat.getColor(WarrantPendingDetailsActivity.this, R.color.color_black));
                ll_otherDetails_BW.addView(warrantiesNameAddressDetail);
            }
        } else {
            TextView noDetail = new TextView(WarrantPendingDetailsActivity.this);
            noDetail.setText("No details for warrantee");
            noDetail.setTextColor(ContextCompat.getColor(WarrantPendingDetailsActivity.this, R.color.color_black));
            ll_otherDetails_BW.addView(noDetail);
            ll_otherDetails_BW.setGravity(Gravity.CENTER);
        }
    }

    public void callCaseFirDetails(String psCode, String caseNo, String caseYr) {
        Intent it = new Intent(this, CaseSearchFIRDetailsActivity.class);
        it.putExtra("FROM_PAGE", "WarrentActivityNew");
        if(isNBWSelected) {
            it.putExtra("ITEM_NAME", tv_caseRefValue.getText().toString().trim());
            it.putExtra("HEADER_VALUE", tv_caseRefValue.getText().toString().trim());
        }
        else if(isBWSelected)
        {
            it.putExtra("ITEM_NAME", tv_caseRefValue_BW.getText().toString().trim());
            it.putExtra("HEADER_VALUE", tv_caseRefValue_BW.getText().toString().trim());
        }
        it.putExtra("PS_CODE", psCode);
        it.putExtra("CASE_NO", caseNo);
        it.putExtra("CASE_YR", caseYr);
        it.putExtra("POSITION", wa_position);
        startActivity(it);
    }


    void buttonVisibilityForNbw() {

        if (pageNoNBW == totalNBWCount && totalNBWCount > 1) {
            bt_nextWA.setVisibility(View.GONE);
            bt_prevWA.setVisibility(View.VISIBLE);
        } else if (pageNoNBW == 1 && totalNBWCount > 1) {

            bt_prevWA.setVisibility(View.GONE);
            bt_nextWA.setVisibility(View.VISIBLE);
        } else if (pageNoNBW == 1 && totalNBWCount == 1) {
            bt_nextWA.setVisibility(View.GONE);
            bt_prevWA.setVisibility(View.GONE);
        } else {
            bt_nextWA.setVisibility(View.VISIBLE);
            bt_prevWA.setVisibility(View.VISIBLE);
        }

        if (totalNBWCount > 1) {
            tag = "1";
            rl_navigate.setVisibility(View.VISIBLE);

            bottomView.setBackgroundResource(R.color.colorWhite);
            txt_count.setVisibility(View.VISIBLE);
            txt_count.setText(pageNoNBW + "/" + totalNBWCount);
        } else {
            rl_navigate.setVisibility(View.GONE);
            if (totalNBWCount == 1) {

                bottomView.setBackgroundResource(R.drawable.buttom_bar);
                txt_count.setVisibility(View.VISIBLE);
                txt_count.setText(pageNoNBW + "/" + totalNBWCount);

            } else {
                bottomView.setBackgroundResource(R.color.colorWhite);
                txt_count.setVisibility(View.GONE);
            }
        }
    }


    void buttonVisibilityForBw() {

        if (pageNoBW == totalBWCount && totalBWCount > 1) {
            bt_nextWA.setVisibility(View.GONE);
            bt_prevWA.setVisibility(View.VISIBLE);
        } else if (pageNoBW == 1 && totalBWCount > 1) {

            bt_prevWA.setVisibility(View.GONE);
            bt_nextWA.setVisibility(View.VISIBLE);
        } else if (pageNoBW == 1 && totalBWCount == 1) {
            bt_nextWA.setVisibility(View.GONE);
            bt_prevWA.setVisibility(View.GONE);
        } else {
            bt_nextWA.setVisibility(View.VISIBLE);
            bt_prevWA.setVisibility(View.VISIBLE);
        }

        if (totalBWCount > 1) {
            tag = "2";
            rl_navigate.setVisibility(View.VISIBLE);

            bottomView.setBackgroundResource(R.color.colorWhite);
            txt_count.setVisibility(View.VISIBLE);
            txt_count.setText(pageNoBW + "/" + totalBWCount);
        } else {
            rl_navigate.setVisibility(View.GONE);
            if (totalBWCount == 1) {

                bottomView.setBackgroundResource(R.drawable.buttom_bar);
                txt_count.setVisibility(View.VISIBLE);
                txt_count.setText(pageNoBW + "/" + totalBWCount);
            } else {
                bottomView.setBackgroundResource(R.color.colorWhite);
                txt_count.setVisibility(View.GONE);
            }
        }
    }


    private void showDataNBW() {
        ll_BW.setVisibility(View.GONE);
        ll_NBW.setVisibility(View.VISIBLE);
        ll_dataShow.setVisibility(View.VISIBLE);
        ll_noDataShow.setVisibility(View.GONE);
    }

    private void parentHideWithAlldataSetNBW() {
        ll_NBW.setVisibility(View.GONE);
        ll_dataShow.setVisibility(View.VISIBLE);
        ll_noDataShow.setVisibility(View.GONE);
    }

    private void parentInvisibleWithNoDataNBW() {
        ll_NBW.setVisibility(View.GONE);
        ll_noDataShow.setVisibility(View.VISIBLE);
        ll_dataShow.setVisibility(View.GONE);
    }

    private void showNoDataFoundNBW() {

        ll_BW.setVisibility(View.GONE);
        ll_NBW.setVisibility(View.VISIBLE);
        ll_dataShow.setVisibility(View.GONE);
        ll_noDataShow.setVisibility(View.VISIBLE);
    }


    private void noDataTextSet_NBW() {

        ll_noDataShow.removeAllViews();
        TextView tv_NoData = new TextView(WarrantPendingDetailsActivity.this);
        tv_NoData.setText("No details available for NBW");
        tv_NoData.setTextColor(ContextCompat.getColor(WarrantPendingDetailsActivity.this, R.color.color_black));
        tv_NoData.setTextColor(ContextCompat.getColor(WarrantPendingDetailsActivity.this, R.color.color_black));
        tv_NoData.setTextSize(20);
        ll_noDataShow.addView(tv_NoData);
        ll_noDataShow.setGravity(Gravity.CENTER);
    }


    private void showDataBW() {

        ll_NBW.setVisibility(View.GONE);
        ll_BW.setVisibility(View.VISIBLE);
        ll_dataShow_BW.setVisibility(View.VISIBLE);
        ll_noDataShow_BW.setVisibility(View.GONE);
    }

    private void parentHideWithAlldataSetBW() {
        ll_BW.setVisibility(View.GONE);
        ll_dataShow_BW.setVisibility(View.VISIBLE);
        ll_noDataShow_BW.setVisibility(View.GONE);
        //ll_NBW.setVisibility(View.GONE);
    }

    private void showNoDataFoundBW() {

        ll_NBW.setVisibility(View.GONE);
        ll_BW.setVisibility(View.VISIBLE);
        ll_dataShow_BW.setVisibility(View.GONE);
        ll_noDataShow_BW.setVisibility(View.VISIBLE);
    }

    private void parentInvisibleWithNoDataBW() {
        Log.e("TAG:", " parentInvisibleWithNoDataBW");
        ll_BW.setVisibility(View.GONE);
        ll_noDataShow_BW.setVisibility(View.VISIBLE);
        ll_dataShow_BW.setVisibility(View.GONE);
    }

    private void noDataTextSet_BW() {

        ll_noDataShow_BW.removeAllViews();
        TextView tv_NoData = new TextView(WarrantPendingDetailsActivity.this);
        tv_NoData.setText("No details available for BW");
        tv_NoData.setTextColor(ContextCompat.getColor(WarrantPendingDetailsActivity.this, R.color.color_black));
        tv_NoData.setTextColor(ContextCompat.getColor(WarrantPendingDetailsActivity.this, R.color.color_black));
        tv_NoData.setTextSize(20);
        ll_noDataShow_BW.addView(tv_NoData);
        ll_noDataShow_BW.setGravity(Gravity.CENTER);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_nextWA:
                if (tag.equalsIgnoreCase("1")) {
                    nextButtonCallNBW();
                } else if (tag.equalsIgnoreCase("2")) {
                    nextButtonCallBW();
                } else {

                }

                break;

            case R.id.bt_prevWA:
                if (tag.equalsIgnoreCase("1")) {
                    previousButtonCallNBW();
                } else if (tag.equalsIgnoreCase("2")) {
                    previousButtonCallBW();
                } else {

                }

                break;

            case R.id.tv_nbw:

                isNBWSelected = true;
                isBWSelected = false;

                ll_NBW.setVisibility(View.VISIBLE);
                ll_BW.setVisibility(View.GONE);

                tv_nbw.setEnabled(false);
                tv_bw.setEnabled(true);
                nbwIndicator.setBackgroundColor(ContextCompat.getColor(this, R.color.color_light_blue));
                bwIndicator.setBackgroundColor(ContextCompat.getColor(this, R.color.color_indicator));

                buttonVisibilityForNbw();

                break;

            case R.id.tv_bw:

                isBWSelected = true;
                isNBWSelected = false;

                ll_BW.setVisibility(View.VISIBLE);
                ll_NBW.setVisibility(View.GONE);

                tv_bw.setEnabled(false);
                tv_nbw.setEnabled(true);
                bwIndicator.setBackgroundColor(ContextCompat.getColor(this, R.color.color_light_blue));
                nbwIndicator.setBackgroundColor(ContextCompat.getColor(this, R.color.color_indicator));

                buttonVisibilityForBw();

                break;
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    private void nextButtonCallNBW() {
        if (pageNoNBW < totalNBWCount) {

            warrantNbwAPICall(pageNoNBW + 1);
        }
    }

    private void nextButtonCallBW() {

        if (pageNoBW < totalBWCount) {
            warrantBWAPICall(pageNoBW + 1);
        }
    }

    private void previousButtonCallNBW() {
        if (pageNoNBW != 0) {
            pgNBW = pageNoNBW - 1;
        }

        if (pgNBW > 0) {
            warrantNbwAPICall(pgNBW);
        }
    }

    private void previousButtonCallBW() {
        if (pageNoBW != 0) {
            pgBW = pageNoBW - 1;
        }

        if (pgBW > 0) {
            warrantBWAPICall(pgBW);
        }
    }


    private void swipePagerCall() {

        includeView.setOnTouchListener(new OnSwipeTouchListener(WarrantPendingDetailsActivity.this) {

            @Override
            public void onSwipeLeft() {

                if (isNBWSelected && !isBWSelected && (pageNoNBW < totalNBWCount)) {
                    nextButtonCallNBW();
                } else if (isBWSelected && !isNBWSelected && (pageNoBW < totalBWCount)) {
                    nextButtonCallBW();
                } else {

                }
            }


            @Override
            public void onSwipeRight() {
                if (isNBWSelected && !isBWSelected) {
                    previousButtonCallNBW();
                } else if (isBWSelected && !isNBWSelected) {
                    previousButtonCallBW();
                } else {

                }
            }
        });
    }
}
