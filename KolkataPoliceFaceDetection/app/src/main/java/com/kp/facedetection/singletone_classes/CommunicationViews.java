package com.kp.facedetection.singletone_classes;


import com.kp.facedetection.interfaces.OnImageUploadListener;
import com.kp.facedetection.interfaces.OnMapIconChangeListener;

/**
 * Created by DAT-165 on 25-05-2017.
 */

public class CommunicationViews {

    private static CommunicationViews communicationViews = null;
    OnMapIconChangeListener onMapIconChangeListener;
    OnImageUploadListener onImageUploadListener;


    private CommunicationViews(){

    }

    public static CommunicationViews getInstance(){

        if(communicationViews == null){
            communicationViews = new CommunicationViews();
        }
        return communicationViews;
    }

    // for map icon change in ModifiedCaseSearchResultActivity
    public  void setOnMapIconChangeListener(OnMapIconChangeListener onMapIconChangeListener){
        this.onMapIconChangeListener = onMapIconChangeListener;
    }

    public void changeMapIcon(int pos, String poLat, String poLong){
        onMapIconChangeListener.mapIconChange(pos,poLat,poLong);
    }

    // for image upload in SearchResultActivity and CriminalSearchDetailsActivity
    public void setOnImageUploadListener(OnImageUploadListener onImageUploadListener){
        this.onImageUploadListener = onImageUploadListener;
    }

    public void uploadImage(int pos, String url){
        onImageUploadListener.imageUpload(pos,url);
    }

}
