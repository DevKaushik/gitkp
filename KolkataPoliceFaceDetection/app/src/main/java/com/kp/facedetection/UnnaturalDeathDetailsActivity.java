package com.kp.facedetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kp.facedetection.model.CrimeReviewDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.utils.OnSwipeTouchListener;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Observable;
import java.util.Observer;

public class UnnaturalDeathDetailsActivity extends BaseActivity implements View.OnClickListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String selectedDate = "";
    private String selected_div = "";
    private String selected_ps = "";
    View includeView;
    RelativeLayout bt_nextUD, bt_prevUD, rl_view_fir;
    TextView tv_ps, tv_repDate, tv_slNo, tv_personName, tv_sex, tv_age, tv_address, tv_causeOfDeath, tv_facts, tv_gdeNo, tv_gdeDate, tv_heading_unnatural_death;
    TextView txt_count;
    int totalPageNo;
    int pageNo = 1;
    Button bttn_view_fir;
    String headerText = "";//if view fir visible then CaseSearchFirDetails header
    String headerTextFir = "";
    String psCode = "";
    String caseNo = "";
    String caseYear = "";
    String fromDate = "";
    String toDate = "";
    TextView chargesheetNotificationCount;
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unnatural_death_details);
        ObservableObject.getInstance().addObserver(this);
        setToolBar();
        initView();

    }

    public void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }

    public void initView() {
        selectedDate = getIntent().getExtras().getString("SELECTED_DATE_UND");
        selected_ps = getIntent().getExtras().getString("SELECTED_PS_UND");
        fromDate = getIntent().getExtras().getString("SELECTED_FROM_DATE_UND");
        toDate = getIntent().getExtras().getString("SELECTED_TO_DATE_UND");
        bt_nextUD = (RelativeLayout) findViewById(R.id.bt_nextUD);
        bt_nextUD.setOnClickListener(this);
        bt_prevUD = (RelativeLayout) findViewById(R.id.bt_prevUD);
        bt_prevUD.setOnClickListener(this);
        bttn_view_fir = (Button) findViewById(R.id.bttn_view_fir);
        bttn_view_fir.setOnClickListener(this);
        rl_view_fir = (RelativeLayout) findViewById(R.id.rl_view_fir);
        txt_count = (TextView) findViewById(R.id.txt_count);
        tv_heading_unnatural_death = (TextView) findViewById(R.id.tv_heading_unnatural_death);
        includeView = findViewById(R.id.unnatural_death);
        tv_ps = (TextView) includeView.findViewById(R.id.tv_psValue);
        tv_repDate = (TextView) includeView.findViewById(R.id.tv_repDateValue);
        //tv_slNo=(TextView)includeView.findViewById(R.id.tv_SlNoValue);
        tv_personName = (TextView) includeView.findViewById(R.id.tv_personNameValue);
        tv_sex = (TextView) includeView.findViewById(R.id.tv_sexValue);
        tv_age = (TextView) includeView.findViewById(R.id.tv_ageVal);
        tv_address = (TextView) includeView.findViewById(R.id.tv_addressValue);
        tv_causeOfDeath = (TextView) includeView.findViewById(R.id.tv_cause_of_death_value);
        tv_facts = (TextView) includeView.findViewById(R.id.tv_factsValue);
        tv_gdeNo = (TextView) includeView.findViewById(R.id.tv_gdeNoValue);
        tv_gdeDate = (TextView) includeView.findViewById(R.id.tv_gdeDateValue);

        getUnnaturalDeathDetails(pageNo);
        swipePagerCall();
    }

    public void getUnnaturalDeathDetails(int pageNo) {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_UNNATURAL_DEATH_DETAILS);
        taskManager.setUnnaturalDeathDetails(true);
        String[] keys = {"currDate", "ps", "pageno", "fromDate", "toDate"};
        String[] values = {selectedDate.trim(), selected_ps.trim(), String.valueOf(pageNo), fromDate, toDate};
        taskManager.doStartTask(keys, values, true);
    }

    public void parseUnnaturalDeathDetailsResponse(String response) {
        if (response != null && !response.equals("")) {

            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.opt("status").toString().equalsIgnoreCase("1")) {

                    parsePSResponse(jobj);
                } else {
                    Utility.showAlertDialog(UnnaturalDeathDetailsActivity.this, Constants.ERROR_DETAILS_TITLE, jobj.optString("message"), false);
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(UnnaturalDeathDetailsActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }

    }

    private void parsePSResponse(JSONObject jobj) {

        JSONArray resultArray = jobj.optJSONArray("result");
        if (resultArray.length() > 0) {
            headerText = "";
            headerTextFir = "";
            txt_count.setText(jobj.optString("pageno") + "/" + jobj.optString("total"));
            if (jobj.optString("pageno") != null && !jobj.optString("pageno").equalsIgnoreCase("") && !jobj.optString("pageno").equalsIgnoreCase("null") && jobj.optString("total") != null && !jobj.optString("total").equalsIgnoreCase("") && !jobj.optString("total").equalsIgnoreCase("null")) {
                pageNo = Integer.parseInt(jobj.optString("pageno"));
                totalPageNo = Integer.parseInt(jobj.optString("total"));
                if (pageNo < totalPageNo) {
                    bt_nextUD.setVisibility(View.VISIBLE);
                } else {
                    bt_nextUD.setVisibility(View.GONE);
                }
                if (pageNo > 1) {
                    bt_prevUD.setVisibility(View.VISIBLE);
                } else {
                    bt_prevUD.setVisibility(View.GONE);
                }
            }
            for (int i = 0; i < resultArray.length(); i++) {
                try {
                    JSONObject jo = resultArray.optJSONObject(i);
                    if (jo.optString("PSNAME") != null && !jo.optString("PSNAME").equalsIgnoreCase("") && !jo.optString("PSNAME").equalsIgnoreCase("null")) {
                        tv_ps.setText(jo.optString("PSNAME"));
                        headerText = headerText + jo.optString("PSNAME");
                        headerTextFir = headerTextFir + jo.optString("PSNAME");

                    } else {
                        tv_ps.setText("NA");
                    }
                    if (jo.optString("REPDATE") != null && !jo.optString("REPDATE").equalsIgnoreCase("") && !jo.optString("REPDATE").equalsIgnoreCase("null")) {
                        tv_repDate.setText(jo.optString("REPDATE"));
                    } else {
                        tv_repDate.setText("NA");
                    }
               /*  if(jo.optString("SLNO")!=null && !jo.optString("SLNO").equalsIgnoreCase("")&& !jo.optString("SLNO").equalsIgnoreCase("null")){
                    tv_slNo.setText(jo.optString("SLNO"));
                 }
                 else
                 {
                     tv_slNo.setText("NA");
                 }*/
                    if (jo.optString("PERSON_NAME") != null && !jo.optString("PERSON_NAME").equalsIgnoreCase("") && !jo.optString("PERSON_NAME").equalsIgnoreCase("null")) {
                        tv_personName.setText(jo.optString("PERSON_NAME"));
                    } else {
                        tv_personName.setText("NA");
                    }
                    if (jo.optString("SEX") != null && !jo.optString("SEX").equalsIgnoreCase("") && !jo.optString("SEX").equalsIgnoreCase("null")) {
                        tv_sex.setText(jo.optString("SEX"));
                    } else {
                        tv_sex.setText("NA");
                    }
                    if (jo.optString("AGE_YRS") != null && !jo.optString("AGE_YRS").equalsIgnoreCase("") && !jo.optString("AGE_YRS").equalsIgnoreCase("null")) {
                        tv_age.setText(jo.optString("AGE_YRS"));
                    } else {
                        tv_age.setText("AGE_YRS");
                    }
                    if (jo.optString("ADDRESS") != null && !jo.optString("ADDRESS").equalsIgnoreCase("") && !jo.optString("ADDRESS").equalsIgnoreCase("null")) {
                        tv_address.setText(jo.optString("ADDRESS"));
                    } else {
                        tv_address.setText("NA");
                    }
                    if (jo.optString("CAUSEOFDEATH") != null && !jo.optString("CAUSEOFDEATH").equalsIgnoreCase("") && !jo.optString("CAUSEOFDEATH").equalsIgnoreCase("null")) {
                        tv_causeOfDeath.setText(jo.optString("CAUSEOFDEATH"));
                    } else {
                        tv_causeOfDeath.setText("NA");
                    }
                    if (jo.optString("FACTS") != null && !jo.optString("FACTS").equalsIgnoreCase("") && !jo.optString("FACTS").equalsIgnoreCase("null")) {
                        tv_facts.setText(jo.optString("FACTS"));
                    } else {
                        tv_facts.setText("NA");
                    }
                    if (jo.optString("GDENO") != null && !jo.optString("GDENO").equalsIgnoreCase("") && !jo.optString("GDENO").equalsIgnoreCase("null")) {
                        caseNo = jo.optString("GDENO");
                        //tv_gdeNo.setText(jo.optString("GDENO"));
                        headerTextFir = headerTextFir + " C/No. " + jo.optString("GDENO");

                    } else {
                        caseNo = "";
                        tv_gdeNo.setText("NA");
                    }
                    if (jo.optString("GDEDATE") != null && !jo.optString("GDEDATE").equalsIgnoreCase("") && !jo.optString("GDEDATE").equalsIgnoreCase("null")) {
                        caseYear = jo.optString("GDEDATE");
                        //tv_gdeDate.setText(jo.optString("GDEDATE"));
                        headerTextFir = headerTextFir + " dated " + jo.optString("GDEDATE");

                    } else {
                        caseYear = "";
                        tv_gdeDate.setText("NA");
                    }
                    if(jo.has("UNDER_SECTIONS"))
                    {
                        if (jo.optString("UNDER_SECTIONS") != null && !jo.optString("UNDER_SECTIONS").equalsIgnoreCase("") && !jo.optString("UNDER_SECTIONS").equalsIgnoreCase("null")) {

                            headerTextFir = headerTextFir + " u/s " + jo.optString("UNDER_SECTIONS");

                        }


                    }
                    if (jo.optString("UDGDENO") != null && !jo.optString("UDGDENO").equalsIgnoreCase("") && !jo.optString("UDGDENO").equalsIgnoreCase("null")) {

                        headerText = headerText + " GDE/No. " + jo.optString("UDGDENO");

                    }
                    if (jo.optString("UDGDEDATE") != null && !jo.optString("UDGDEDATE").equalsIgnoreCase("") && !jo.optString("UDGDEDATE").equalsIgnoreCase("null")) {

                        headerText = headerText + " dated " + jo.optString("UDGDEDATE");

                    }
                    if ((jo.optString("GDENO") != null && !jo.optString("GDENO").equalsIgnoreCase("") && !jo.optString("GDENO").equalsIgnoreCase("null")) && (jo.optString("GDEDATE") != null && !jo.optString("GDEDATE").equalsIgnoreCase("") && !jo.optString("GDEDATE").equalsIgnoreCase("null")) && (jo.optString("PS") != null && !jo.optString("PS").equalsIgnoreCase("") && !jo.optString("PS").equalsIgnoreCase("null"))) {
                        rl_view_fir.setVisibility(View.VISIBLE);
                    } else {
                        rl_view_fir.setVisibility(View.GONE);
                    }
                    if (jo.optString("PS") != null && !jo.optString("PS").equalsIgnoreCase("") && !jo.optString("PS").equalsIgnoreCase("null")) {
                        psCode = jo.optString("PS");
                    } else {
                        psCode = "";
                    }
                    tv_heading_unnatural_death.setText(headerText);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_nextUD:
                buttonNextCalled();
                break;
            case R.id.bt_prevUD:
                buttonPreviousCalled();
                break;
            case R.id.bttn_view_fir:
                L.e("Header Txt---------" + headerText);
                // String headerTextToSend= headerText;
                L.e("CASE_YEAR_UND---------" + caseYear);
                Intent it = new Intent(UnnaturalDeathDetailsActivity.this, CaseSearchFIRDetailsActivity.class);
                it.putExtra("FROM_PAGE", "UnnaturalDeathDetailsActivity");
                it.putExtra("ITEM_NAME", headerTextFir);
                it.putExtra("HEADER_VALUE", headerTextFir);
                it.putExtra("PS_CODE", psCode);
                it.putExtra("CASE_NO", caseNo);
                it.putExtra("CASE_YR", caseYear);
                it.putExtra("POSITION", pageNo);
                startActivity(it);
                break;
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    public void buttonNextCalled() {
        if (pageNo < totalPageNo) {
            pageNo = pageNo + 1;
            getUnnaturalDeathDetails(pageNo);
        }

    }

    public void buttonPreviousCalled() {
        if (pageNo > 1) {
            pageNo = pageNo - 1;
            getUnnaturalDeathDetails(pageNo);
        }

    }

    private void swipePagerCall() {

        includeView.setOnTouchListener(new OnSwipeTouchListener(UnnaturalDeathDetailsActivity.this) {

            @Override
            public void onSwipeLeft() {

                if (pageNo < totalPageNo) {
                    buttonNextCalled();
                }

            }


            @Override
            public void onSwipeRight() {
                if (pageNo > 1) {
                    buttonPreviousCalled();
                }
            }
        });
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
