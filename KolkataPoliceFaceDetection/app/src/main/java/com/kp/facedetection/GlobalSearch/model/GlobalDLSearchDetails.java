package com.kp.facedetection.GlobalSearch.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 03-10-2016.
 */
public class GlobalDLSearchDetails implements Serializable {

    private String dl_Name="";
    private String dl_No="";
    private String dl_SwdOf="";
    private String dl_Dob="";
    private String dl_Sex="";
    private String dl_PAddress="";
    private String dl_TAddress="";
    private String dl_IssueAuthority="";
    private String dl_Ntvldtodt="";
    private String dl_Tvldtodt="";
    private String dl_tele="";

    public String getDl_tele() {
        return dl_tele;
    }

    public void setDl_tele(String dl_tele) {
        this.dl_tele = dl_tele;
    }

    public String getDl_Name() {
        return dl_Name;
    }

    public void setDl_Name(String dl_Name) {
        this.dl_Name = dl_Name;
    }

    public String getDl_No() {
        return dl_No;
    }

    public void setDl_No(String dl_No) {
        this.dl_No = dl_No;
    }

    public String getDl_SwdOf() {
        return dl_SwdOf;
    }

    public void setDl_SwdOf(String dl_SwdOf) {
        this.dl_SwdOf = dl_SwdOf;
    }

    public String getDl_Dob() {
        return dl_Dob;
    }

    public void setDl_Dob(String dl_Dob) {
        this.dl_Dob = dl_Dob;
    }

    public String getDl_Sex() {
        return dl_Sex;
    }

    public void setDl_Sex(String dl_Sex) {
        this.dl_Sex = dl_Sex;
    }

    public String getDl_PAddress() {
        return dl_PAddress;
    }

    public void setDl_PAddress(String dl_PAddress) {
        this.dl_PAddress = dl_PAddress;
    }

    public String getDl_TAddress() {
        return dl_TAddress;
    }

    public void setDl_TAddress(String dl_TAddress) {
        this.dl_TAddress = dl_TAddress;
    }

    public String getDl_IssueAuthority() {
        return dl_IssueAuthority;
    }

    public void setDl_IssueAuthority(String dl_IssueAuthority) {
        this.dl_IssueAuthority = dl_IssueAuthority;
    }

    public String getDl_Ntvldtodt() {
        return dl_Ntvldtodt;
    }

    public void setDl_Ntvldtodt(String dl_Ntvldtodt) {
        this.dl_Ntvldtodt = dl_Ntvldtodt;
    }

    public String getDl_Tvldtodt() {
        return dl_Tvldtodt;
    }

    public void setDl_Tvldtodt(String dl_Tvldtodt) {
        this.dl_Tvldtodt = dl_Tvldtodt;
    }
}
