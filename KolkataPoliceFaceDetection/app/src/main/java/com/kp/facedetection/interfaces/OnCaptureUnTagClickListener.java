package com.kp.facedetection.interfaces;

import android.view.View;

/**
 * Created by DAT-165 on 08-06-2017.
 */

public interface OnCaptureUnTagClickListener {
    void onCaptureUnTag(View view, int position);
}
