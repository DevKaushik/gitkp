package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.ShowColorWithCategory;
import com.kp.facedetection.utility.Constants;

import java.util.ArrayList;

/**
 * Created by DAT-165 on 12-04-2017.
 */

public class MapColorWithCategoryAdapter extends BaseAdapter{

    Context context;
    ArrayList<ShowColorWithCategory> uniqueColorCategoryList;


    public MapColorWithCategoryAdapter(Context context, ArrayList<ShowColorWithCategory> uniqueColorCategoryList) {
        this.context = context;
        this.uniqueColorCategoryList = uniqueColorCategoryList;
    }

    @Override
    public int getCount() {
        return uniqueColorCategoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.custom_color_cat_item, null);

            holder.tv_category = (TextView) convertView.findViewById(R.id.tv_category);
            holder.iv_color = (ImageView) convertView.findViewById(R.id.iv_color);

            Constants.changefonts(holder.tv_category, context, "Calibri.ttf");
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_category.setText(uniqueColorCategoryList.get(position).getCategory());
        String color = uniqueColorCategoryList.get(position).getColor();
        //Log.e("color ",uniqueColorCategoryList.size()+" - "+color +" - " +position + " - "+ uniqueColorCategoryList.get(position).getCategory());
        holder.iv_color.setBackgroundColor(Color.parseColor(color));

        return convertView;
    }


    class ViewHolder {
        TextView tv_category;
        ImageView iv_color;
    }
}
