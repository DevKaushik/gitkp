package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-Asset-131 on 01-06-2016.
 */
public class AssociateDetails implements Serializable {

    private String associate_name = "";
    private String associate_address = "";
    private String associate_picture = "";

    private String associate_ps = "";
    private String associate_age = "";
    private String associate_father = "";
    private String associate_photoNo = "";
    private String associate_caseNo = "";
    private String associate_caseYear = "";
    private String associate_category = "";
    private String associate_underSection = "";
    private String associate_usClass = "";
    private String associate_class = "";
    private String associate_subClass = "";
    private String associate_property = "";
    private String associate_transport = "";
    private String associate_ground = "";
    private String associate_arms = "";

    private String associate_heightFeet = "";
    private String associate_heightInch = "";
    private String associate_beard = "";
    private String associate_birthmark = "";
    private String associate_built = "";
    private String associate_burnmark = "";
    private String associate_complexion = "";
    private String associate_cutmark = "";
    private String associate_deformity = "";
    private String associate_ear = "";
    private String associate_eye = "";
    private String associate_eyebrow = "";
    private String associate_face = "";

    private String associate_forehead = "";
    private String associate_hair = "";
    private String associate_mole = "";
    private String associate_moustache = "";
    private String associate_nose = "";
    private String associate_scarmark = "";
    private String associate_tattoomark = "";
    private String associate_wartmark = "";
    private  String associate_prov_criminal_number = "";

    public String getAssociate_prov_criminal_number() {
        return associate_prov_criminal_number;
    }

    public void setAssociate_prov_criminal_number(String associate_prov_criminal_number) {
        this.associate_prov_criminal_number = associate_prov_criminal_number;
    }
    public String getAssociate_name() {
        return associate_name;
    }

    public void setAssociate_name(String associate_name) {
        this.associate_name = associate_name;
    }

    public String getAssociate_address() {
        return associate_address;
    }

    public void setAssociate_address(String associate_address) {
        this.associate_address = associate_address;
    }

    public String getAssociate_picture() {
        return associate_picture;
    }

    public void setAssociate_picture(String associate_picture) {
        this.associate_picture = associate_picture;
    }

    public String getAssociate_ps() {
        return associate_ps;
    }

    public void setAssociate_ps(String associate_ps) {
        this.associate_ps = associate_ps;
    }

    public String getAssociate_age() {
        return associate_age;
    }

    public void setAssociate_age(String associate_age) {
        this.associate_age = associate_age;
    }

    public String getAssociate_father() {
        return associate_father;
    }

    public void setAssociate_father(String associate_father) {
        this.associate_father = associate_father;
    }

    public String getAssociate_photoNo() {
        return associate_photoNo;
    }

    public void setAssociate_photoNo(String associate_photoNo) {
        this.associate_photoNo = associate_photoNo;
    }

    public String getAssociate_caseNo() {
        return associate_caseNo;
    }

    public void setAssociate_caseNo(String associate_caseNo) {
        this.associate_caseNo = associate_caseNo;
    }

    public String getAssociate_caseYear() {
        return associate_caseYear;
    }

    public void setAssociate_caseYear(String associate_caseYear) {
        this.associate_caseYear = associate_caseYear;
    }

    public String getAssociate_category() {
        return associate_category;
    }

    public void setAssociate_category(String associate_category) {
        this.associate_category = associate_category;
    }

    public String getAssociate_underSection() {
        return associate_underSection;
    }

    public void setAssociate_underSection(String associate_underSection) {
        this.associate_underSection = associate_underSection;
    }

    public String getAssociate_usClass() {
        return associate_usClass;
    }

    public void setAssociate_usClass(String associate_usClass) {
        this.associate_usClass = associate_usClass;
    }

    public String getAssociate_class() {
        return associate_class;
    }

    public void setAssociate_class(String associate_class) {
        this.associate_class = associate_class;
    }

    public String getAssociate_subClass() {
        return associate_subClass;
    }

    public void setAssociate_subClass(String associate_subClass) {
        this.associate_subClass = associate_subClass;
    }

    public String getAssociate_property() {
        return associate_property;
    }

    public void setAssociate_property(String associate_property) {
        this.associate_property = associate_property;
    }

    public String getAssociate_transport() {
        return associate_transport;
    }

    public void setAssociate_transport(String associate_transport) {
        this.associate_transport = associate_transport;
    }

    public String getAssociate_ground() {
        return associate_ground;
    }

    public void setAssociate_ground(String associate_ground) {
        this.associate_ground = associate_ground;
    }

    public String getAssociate_arms() {
        return associate_arms;
    }

    public void setAssociate_arms(String associate_arms) {
        this.associate_arms = associate_arms;
    }

    public String getAssociate_heightFeet() {
        return associate_heightFeet;
    }

    public void setAssociate_heightFeet(String associate_heightFeet) {
        this.associate_heightFeet = associate_heightFeet;
    }

    public String getAssociate_heightInch() {
        return associate_heightInch;
    }

    public void setAssociate_heightInch(String associate_heightInch) {
        this.associate_heightInch = associate_heightInch;
    }

    public String getAssociate_beard() {
        return associate_beard;
    }

    public void setAssociate_beard(String associate_beard) {
        this.associate_beard = associate_beard;
    }

    public String getAssociate_birthmark() {
        return associate_birthmark;
    }

    public void setAssociate_birthmark(String associate_birthmark) {
        this.associate_birthmark = associate_birthmark;
    }

    public String getAssociate_built() {
        return associate_built;
    }

    public void setAssociate_built(String associate_built) {
        this.associate_built = associate_built;
    }

    public String getAssociate_burnmark() {
        return associate_burnmark;
    }

    public void setAssociate_burnmark(String associate_burnmark) {
        this.associate_burnmark = associate_burnmark;
    }

    public String getAssociate_complexion() {
        return associate_complexion;
    }

    public void setAssociate_complexion(String associate_complexion) {
        this.associate_complexion = associate_complexion;
    }

    public String getAssociate_cutmark() {
        return associate_cutmark;
    }

    public void setAssociate_cutmark(String associate_cutmark) {
        this.associate_cutmark = associate_cutmark;
    }

    public String getAssociate_deformity() {
        return associate_deformity;
    }

    public void setAssociate_deformity(String associate_deformity) {
        this.associate_deformity = associate_deformity;
    }

    public String getAssociate_ear() {
        return associate_ear;
    }

    public void setAssociate_ear(String associate_ear) {
        this.associate_ear = associate_ear;
    }

    public String getAssociate_eye() {
        return associate_eye;
    }

    public void setAssociate_eye(String associate_eye) {
        this.associate_eye = associate_eye;
    }

    public String getAssociate_eyebrow() {
        return associate_eyebrow;
    }

    public void setAssociate_eyebrow(String associate_eyebrow) {
        this.associate_eyebrow = associate_eyebrow;
    }

    public String getAssociate_face() {
        return associate_face;
    }

    public void setAssociate_face(String associate_face) {
        this.associate_face = associate_face;
    }

    public String getAssociate_forehead() {
        return associate_forehead;
    }

    public void setAssociate_forehead(String associate_forehead) {
        this.associate_forehead = associate_forehead;
    }

    public String getAssociate_hair() {
        return associate_hair;
    }

    public void setAssociate_hair(String associate_hair) {
        this.associate_hair = associate_hair;
    }

    public String getAssociate_mole() {
        return associate_mole;
    }

    public void setAssociate_mole(String associate_mole) {
        this.associate_mole = associate_mole;
    }

    public String getAssociate_moustache() {
        return associate_moustache;
    }

    public void setAssociate_moustache(String associate_moustache) {
        this.associate_moustache = associate_moustache;
    }

    public String getAssociate_nose() {
        return associate_nose;
    }

    public void setAssociate_nose(String associate_nose) {
        this.associate_nose = associate_nose;
    }

    public String getAssociate_scarmark() {
        return associate_scarmark;
    }

    public void setAssociate_scarmark(String associate_scarmark) {
        this.associate_scarmark = associate_scarmark;
    }

    public String getAssociate_tattoomark() {
        return associate_tattoomark;
    }

    public void setAssociate_tattoomark(String associate_tattoomark) {
        this.associate_tattoomark = associate_tattoomark;
    }

    public String getAssociate_wartmark() {
        return associate_wartmark;
    }

    public void setAssociate_wartmark(String associate_wartmark) {
        this.associate_wartmark = associate_wartmark;
    }
}
