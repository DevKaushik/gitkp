package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 17-02-2017.
 */
public class SubClassListCriminal implements Serializable {

    private String className = "";
    private String subClassName = "";

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSubClassName() {
        return subClassName;
    }

    public void setSubClassName(String subClassName) {
        this.subClassName = subClassName;
    }
}
