package com.kp.facedetection;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.asynctasks.AsynctaskForDownloadPDF;
import com.kp.facedetection.interfaces.OnImageUploadListener;
import com.kp.facedetection.interfaces.OnPDFDownload;
import com.kp.facedetection.model.CaseSearchAccusedDetails;
import com.kp.facedetection.model.CaseSearchFIRAllAccused;
import com.kp.facedetection.model.CaseSearchFIRAllArrested;
import com.kp.facedetection.model.CaseSearchFIRChargeSheet;
import com.kp.facedetection.model.CaseSearchFIRDetails;
import com.kp.facedetection.model.CaseSearchFIRWarrant;
import com.kp.facedetection.model.CaseSearchPropertyDetails;
import com.kp.facedetection.model.CaseSearchWitnessDetails;
import com.kp.facedetection.model.CaseTransferDetails;
import com.kp.facedetection.model.CriminalDetails;
import com.kp.facedetection.model.FIRWarranteesDetails;
import com.kp.facedetection.model.FirLogListDetails;
import com.kp.facedetection.singletone_classes.CommunicationViews;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;


/**
 * Created by DAT-Asset-131 on 25-07-2016.
 */
public class CaseSearchFIRDetailsActivity extends BaseActivity implements OnPDFDownload, LocationListener, OnImageUploadListener,View.OnClickListener, Observer {

    private String fromPage = "";
    private String ps_name = "";
    private String itemName = "";
    private String header_value = "";
    private String ps_code = "";
    private String case_no = "";
    private String case_yr = "";
    private String fir_yr  = "";
    private String pm_no = "";
    private String pm_yr = "";
    private String userName = "";
    private String loginNumber = "";
    private String user_Id = "";
    private String appVersion = "";
    private String log_response="";

    private List<CaseSearchFIRDetails> caseSearchFIRDetailsList = new ArrayList<CaseSearchFIRDetails>();
    private List<String> CommentList;

    private String position = "";

    //view initialization

    private RelativeLayout rl_FIR_Details, bac_dim_layout;
    private RelativeLayout rl_log;
    private RelativeLayout rl_case_log;
    private ImageView iv_pdf,iv_pm_pdf,iv_case_log,iv_log;

    private TextView tv_complainantAddress;
    private TextView tv_category;
    private TextView tv_IO,tv_OC,tv_AC,tv_DC;
    private TextView tv_IO_Phone,tv_oc_phone,tv_ac_phone,tv_dc_phone;
    private TextView tv_complaint;
    private TextView tv_gde;
    private TextView tv_BriefFact;
    private TextView tv_court;
    private TextView tv_CGR;
    private TextView tv_presentStatus;
    private TextView tv_warrant;
    private TextView tv_itemName;
    private TextView tv_downloadLog;

    private LinearLayout linear_comments;
    private LinearLayout linear_otherAccused;
    private LinearLayout linear_caseTranfer;
    private LinearLayout linear_downloadLog;
    private LinearLayout linear_io_phone_holder,ll_oc_phone_holder,ll_ac_phone_holder,ll_dc_phone_holder;


    /**
     * views of WARRANT popup window
     */
    private TextView tv_issuingCourtValue;
    private TextView tv_processNoValue;
    private TextView tv_typeValue;
    private TextView tv_returnableDateValue;
    private TextView tv_IOValue;
    private TextView tv_presentStatusValue;
    private TextView tv_remarksValue;
    private TextView tv_warrant_itemName;
    private TextView tv_warrantNo;
    private TextView tv_pdf, tv_pmReportPdf;

    private LinearLayout linear_warrantees;
    private LinearLayout linear_witness;
    private LinearLayout linear_accusedChargesheeted;
    private LinearLayout linear_accusedNotChargesheeted;
    private LinearLayout linear_propertySeizure;

    private ImageView iv_edit_comment;
    private Button btn_comment_post;

    private TextView tv_watermark;

    View inflatedFIRview;   //view used for popUpWindow
    private PopupWindow popupWindowForWarrant;
    private PopupWindow popWindowForChargeSheet;
    View inflatedFIRviewChargeSheet;

    View inflatedViewCaseTransfer;
    private PopupWindow popWindowForCaseTransLog;

    View inflatedViewDownloadLog;
    private PopupWindow popWindowForCaseDownloadLog;

    private ArrayList<CriminalDetails> allAccusedList;
    private ArrayList<CriminalDetails> allArrestedList;
    private boolean isOtherAccuesedAdded = false;
    private boolean isOtherArrestAdded = false;
    private boolean isCaseTransferAdded = false;

    // private String pdf_download_url = "http://182.71.240.212/kpfirs/uploads/2016/E1/626-2016-E1.pdf";
    private String pdf_download_url = "";
    private String existing_pdf_path = "";

    private String pmreport_pdf_download_url = "";
    private String pmreport_existing_pdf_path = "";

    private boolean isCase_PDF;


    private ImageView bt_crimeMap, bt_noCrimeMap;
    private Context context;


    // Flag for GPS status
    boolean isGPSEnabled = false;

    // Flag for network status
    boolean isNetworkEnabled = false;

    // Flag for GPS status
    boolean canGetLocation = false;

    Location location; // Location
    double latitude; // Latitude
    double longitude; // Longitude

    // Declaring a Location Manager
    protected LocationManager locationManager;

    protected String latitudeVal = "", longitudeVal = "";

    private String[] keys;
    private String[] values;

    ArrayList<FirLogListDetails> firDownloadLogList = new ArrayList<>();
    private int length;
    private LinearLayout linear_person_arrested;

    private RelativeLayout rl_fir;
    private RelativeLayout rl_pm;
    private boolean isFIR_PDF=false;
    private boolean isDownloadPDF=false;
    TextView chargesheetNotificationCount;
    String notificationCount="";


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_fir_details_popup_modified);
        ObservableObject.getInstance().addObserver(this);
        initialization();

        clickEvents();

    }

    private void initialization() {

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
            user_Id = Utility.getUserInfo(this).getUserId();

        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        // singletone class intialize
        CommunicationViews.getInstance().setOnImageUploadListener(this);

        fromPage = getIntent().getStringExtra("FROM_PAGE");

        if (fromPage.equalsIgnoreCase("ModifiedCaseSearchResultActivity")) {
            Log.e("FROM_PAGE", " :: fromPage : ModifiedCaseSearchResultActivity");

            itemName = getIntent().getStringExtra("ITEM_NAME");
            header_value = getIntent().getStringExtra("HEADER_VALUE");
            ps_code = getIntent().getStringExtra("PS_CODE");
            case_no = getIntent().getStringExtra("CASE_NO");
            case_yr = getIntent().getStringExtra("CASE_YR");
            //fromPage = getIntent().getStringExtra("FROM_PAGE");
            ps_name = getIntent().getStringExtra("PS_NAME");
            position = getIntent().getStringExtra("POSITION");
        } else if (fromPage.equalsIgnoreCase("CaseMapSearchDetailsActivity")) {
            Log.e("FROM_PAGE", " :: fromPage : CaseMapSearchDetailsActivity");

            itemName = getIntent().getStringExtra("ITEM_NAME");
            header_value = getIntent().getStringExtra("HEADER_VALUE");
            ps_code = getIntent().getStringExtra("PS_CODE");
            case_no = getIntent().getStringExtra("CASE_NO");
            case_yr = getIntent().getStringExtra("CASE_YR");
            position = getIntent().getStringExtra("POSITION");
        } else if (fromPage.equalsIgnoreCase("WarrantSearchDetailsActivity")) {
            Log.e("FROM_PAGE", " :: fromPage : WarrantSearchDetailsActivity");

            itemName = getIntent().getStringExtra("ITEM_NAME");
            header_value = getIntent().getStringExtra("HEADER_VALUE");
            ps_code = getIntent().getStringExtra("PS_CODE");
            case_no = getIntent().getStringExtra("CASE_NO");
            case_yr = getIntent().getStringExtra("CASE_YR");
            position = getIntent().getIntExtra("POSITION", 0) + "";
        } else if (fromPage.equalsIgnoreCase("DashboardArrestSpecificFragment")) {
            Log.e("FROM_PAGE", " :: fromPage : DashboardArrestSpecificFragment");

            itemName = getIntent().getStringExtra("ITEM_NAME");
            header_value = getIntent().getStringExtra("HEADER_VALUE");
            ps_code = getIntent().getStringExtra("PS_CODE");
            case_no = getIntent().getStringExtra("CASE_NO");
            case_yr = getIntent().getStringExtra("CASE_YR");
            position = getIntent().getStringExtra("POSITION");
        } else if (fromPage.equalsIgnoreCase("DashboardArrestOtherFragment")) {
            Log.e("FROM_PAGE", " :: fromPage : DashboardArrestOtherFragment");

            itemName = getIntent().getStringExtra("ITEM_NAME");
            header_value = getIntent().getStringExtra("HEADER_VALUE");
            ps_code = getIntent().getStringExtra("PS_CODE");
            case_no = getIntent().getStringExtra("CASE_NO");
            case_yr = getIntent().getStringExtra("CASE_YR");
            position = getIntent().getStringExtra("POSITION");
        } else if (fromPage.equalsIgnoreCase("DisposalCaseSearchResultActivity")) {
            Log.e("FROM_PAGE", " :: fromPage : DisposalCaseSearchResultActivity");

            itemName = getIntent().getStringExtra("ITEM_NAME");
            header_value = getIntent().getStringExtra("HEADER_VALUE");
            ps_code = getIntent().getStringExtra("PS_CODE");
            case_no = getIntent().getStringExtra("CASE_NO");
            case_yr = getIntent().getStringExtra("CASE_YR");
            position = getIntent().getStringExtra("POSITION");
        } else if (fromPage.equalsIgnoreCase("DisposalMapSearchDetailsActivity")) {
            Log.e("FROM_PAGE", " :: fromPage : DisposalMapSearchDetailsActivity");

            itemName = getIntent().getStringExtra("ITEM_NAME");
            header_value = getIntent().getStringExtra("HEADER_VALUE");
            ps_code = getIntent().getStringExtra("PS_CODE");
            case_no = getIntent().getStringExtra("CASE_NO");
            case_yr = getIntent().getStringExtra("CASE_YR");
            position = getIntent().getStringExtra("POSITION");
        } else if(fromPage.equalsIgnoreCase("WarrentActivityNew")) {
            itemName = getIntent().getStringExtra("ITEM_NAME");
            header_value = getIntent().getStringExtra("HEADER_VALUE");
            ps_code = getIntent().getStringExtra("PS_CODE");
            case_no = getIntent().getStringExtra("CASE_NO");
            case_yr = getIntent().getStringExtra("CASE_YR");
            position = getIntent().getIntExtra("POSITION", 0) + "";

        }
        else if(fromPage.equalsIgnoreCase("UnnaturalDeathDetailsActivity")) {
            itemName = getIntent().getStringExtra("ITEM_NAME");
            header_value = getIntent().getStringExtra("HEADER_VALUE");
            ps_code = getIntent().getStringExtra("PS_CODE");
            case_no = getIntent().getStringExtra("CASE_NO");
            case_yr = getIntent().getStringExtra("CASE_YR");
            position = getIntent().getIntExtra("POSITION", 0) + "";
            L.e("CASE_YEAR---------"+case_yr);

        }
        else if(fromPage.equalsIgnoreCase("ChargesheetDueCaseListActivity")) {
            itemName = getIntent().getStringExtra("ITEM_NAME");
            header_value = getIntent().getStringExtra("HEADER_VALUE");
            ps_code = getIntent().getStringExtra("PS_CODE");
            case_no = getIntent().getStringExtra("CASE_NO");
            case_yr = getIntent().getStringExtra("CASE_YR");
            position = getIntent().getIntExtra("POSITION", 0) + "";
            L.e("CASE_YEAR---------"+case_yr);

        }
        else if(fromPage.equalsIgnoreCase("SpecialServiceDetailsActivity")) {
            itemName = getIntent().getStringExtra("ITEM_NAME");
            header_value = getIntent().getStringExtra("HEADER_VALUE");
            ps_code = getIntent().getStringExtra("PS_CODE");
            case_no = getIntent().getStringExtra("CASE_NO");
            case_yr = getIntent().getStringExtra("CASE_YR");
            position = getIntent().getIntExtra("POSITION", 0) + "";


        }


        initView();

    }

    private void initView() {

        rl_FIR_Details = (RelativeLayout) findViewById(R.id.rl_FIR_Details);
        rl_FIR_Details.setVisibility(View.GONE);


        rl_fir = (RelativeLayout) findViewById(R.id.rl_fir);
        rl_pm = (RelativeLayout) findViewById(R.id.rl_pm);
        rl_log = (RelativeLayout) findViewById(R.id.rl_log);
        rl_case_log = (RelativeLayout) findViewById(R.id.rl_case_log);
        iv_pdf = (ImageView)findViewById(R.id.iv_pdf);
        iv_pm_pdf = (ImageView)findViewById(R.id.iv_pm_pdf);
        iv_case_log =(ImageView)findViewById(R.id.iv_case_log);
        iv_log =(ImageView)findViewById(R.id.iv_log);

        tv_complainantAddress = (TextView) findViewById(R.id.tv_underSection);
        tv_category = (TextView) findViewById(R.id.tv_category);
        tv_IO = (TextView)findViewById(R.id.tv_io_name);
        tv_OC = (TextView)findViewById(R.id.tv_oc_name);
        tv_AC = (TextView)findViewById(R.id.tv_ac_name);
        tv_DC = (TextView)findViewById(R.id.tv_dc_name);
        tv_IO_Phone =(TextView)findViewById(R.id.tv_io_phone);
        tv_oc_phone =(TextView)findViewById(R.id.tv_oc_phone);
        tv_ac_phone=(TextView)findViewById(R.id.tv_ac_phone);
        tv_dc_phone =(TextView)findViewById(R.id.tv_dc_phone);
        tv_IO_Phone =(TextView)findViewById(R.id.tv_io_phone);
        tv_complaint = (TextView) findViewById(R.id.tv_complaint);
        tv_gde = (TextView) findViewById(R.id.tv_gde);
        tv_BriefFact = (TextView) findViewById(R.id.tv_BriefFact);
        tv_court = (TextView) findViewById(R.id.tv_court);
        tv_CGR = (TextView) findViewById(R.id.tv_CGR);
        tv_presentStatus = (TextView) findViewById(R.id.tv_presentStatus);
        tv_warrant = (TextView) findViewById(R.id.tv_warrant);
        tv_itemName = (TextView) findViewById(R.id.tv_itemName);
        tv_watermark = (TextView) findViewById(R.id.tv_watermark);
        tv_pdf = (TextView) findViewById(R.id.tv_pdf);
        tv_pmReportPdf = (TextView) findViewById(R.id.tv_pmReportPdf);
        tv_downloadLog = (TextView) findViewById(R.id.tv_downloadLog);
       linear_io_phone_holder=(LinearLayout) findViewById(R.id.ll_io_phone_holder);
        ll_oc_phone_holder=(LinearLayout) findViewById(R.id.ll_oc_phone_holder);
        ll_ac_phone_holder=(LinearLayout) findViewById(R.id.ll_ac_phone_holder);
        ll_dc_phone_holder=(LinearLayout) findViewById(R.id.ll_dc_phone_holder);
        linear_comments = (LinearLayout) findViewById(R.id.linear_comments);
        linear_otherAccused = (LinearLayout) findViewById(R.id.linear_otherAccused);
        linear_person_arrested = (LinearLayout) findViewById(R.id.linear_person_arrested);

        linear_person_arrested.setVisibility(View.VISIBLE);

        iv_edit_comment = (ImageView) findViewById(R.id.iv_edit_comment);
        btn_comment_post = (Button) findViewById(R.id.btn_comment_post);

        btn_comment_post.setVisibility(View.GONE);
        Constants.buttonEffect(btn_comment_post);

        tv_presentStatus.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        tv_warrant.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        Utility.changefonts(tv_itemName, this, "Calibri Bold.ttf");
        L.e("Heading--------->"+itemName);

        tv_itemName.setText(itemName);

          /* Set user name as Watermark  */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);

        bt_crimeMap = (ImageView) findViewById(R.id.bt_crimeMap);
        bt_noCrimeMap = (ImageView) findViewById(R.id.bt_noCrimeMap);

        getCaseSearchFIRDetailsResult();

    }


    private void clickEvents() {

        rl_fir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFIR_PDF = true;
                isDownloadPDF =true;

                if (pdf_download_url.length() > 0) {
                        Intent intent = new Intent(CaseSearchFIRDetailsActivity.this, PDFLoadActivity.class);
                        intent.putExtra("PDF_DOWNLOAD_URL", pdf_download_url);
                        intent.putExtra("DOWNLOAD_PDF", isDownloadPDF);
                        intent.putExtra("REPORT_TYPE", isFIR_PDF);
                        intent.putExtra("USER_ID", user_Id);
                        intent.putExtra("PS_CODE", ps_code);
                        intent.putExtra("CASE_NO", case_no);
                        if(fromPage.equalsIgnoreCase("UnnaturalDeathDetailsActivity")){
                            intent.putExtra("CASE_YEAR",  fir_yr);
                        }
                        else {
                            intent.putExtra("CASE_YEAR", case_yr);
                        }
                        startActivity(intent);

                } else {
                    //Utility.showAlertDialog(CaseSearchFIRDetailsActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_PREVIEW_NOT_FOUND, false);
                }

            }
        });

        rl_pm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFIR_PDF = false;
                isDownloadPDF =true;
                //pmreport_pdf_download_url="http://182.71.240.211/crimebabuapp/uploads/pmreport/CMCH-2017-1093.pdf";

                if (pmreport_pdf_download_url.length() > 0) {


                        Intent intent = new Intent(CaseSearchFIRDetailsActivity.this, PDFLoadActivity.class);
                        intent.putExtra("REPORT_TYPE", isFIR_PDF);
                        intent.putExtra("PDF_DOWNLOAD_URL", pmreport_pdf_download_url);
                        intent.putExtra("USER_ID", user_Id);
                        intent.putExtra("PS_CODE", ps_code);
                        intent.putExtra("PM_NO", caseSearchFIRDetailsList.get(0).getPm_no());
                        intent.putExtra("PM_YEAR",  caseSearchFIRDetailsList.get(0).getPm_year());
                        intent.putExtra("DOWNLOAD_PDF", isDownloadPDF);
                        startActivity(intent);

                } else {
                   // Utility.showAlertDialog(CaseSearchFIRDetailsActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_PREVIEW_NOT_FOUND, false);
                }
            }
        });


        bt_crimeMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CaseSearchFIRDetailsActivity.this, CaseCrimeMapActivity.class);
                System.out.println("Result:" + caseSearchFIRDetailsList.get(0).getPoLat() + " Long: " + caseSearchFIRDetailsList.get(0).getPoLong());
                intent.putExtra("MAP_SEARCH", (Serializable) caseSearchFIRDetailsList);
                startActivity(intent);
            }
        });

        bt_noCrimeMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(getApplicationContext(),"Lat: "+ latitude +" - Long: "+ longitude,Toast.LENGTH_SHORT).show();
                getCurrentLocation();
                showAlertForProceed();

            }
        });

        rl_log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // PDF download Log part Load
                getDownloadLogForFIR();

            }
        });

        rl_case_log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popupWindowForCaseTransferLog();
            }
        });
    }


    private void popupWindowForCaseTransferLog() {

        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflatedViewCaseTransfer = layoutInflater.inflate(R.layout.log_layout, null, false);

        linear_caseTranfer = (LinearLayout) inflatedViewCaseTransfer.findViewById(R.id.linear_caseTranfer);

        if (caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().size() > 0) {

            int sl_no = 0;
            int length = caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().size();
            for (int i = 0; i < length; i++) {

                String fromPS = "";
                String toPS = "";
                String fromIoName = "";
                String toIoName = "";
                String toUnit = "";
                String caseNo = "";
                String caseTransYr = "";
                String remarks = "";
                String transferDt = "";

                LayoutInflater inflater = (LayoutInflater) this
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = inflater.inflate(R.layout.fir_case_transfer_list_item, null);

                final TextView tv_SlNo = (TextView) v.findViewById(R.id.tv_SlNo);
                final LinearLayout linear_case_transfer_details = (LinearLayout) v.findViewById(R.id.linear_case_transfer_details);

                List<String> caseTransferItemDetailsList = new ArrayList<>();

                if (!caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getFromPS().equals(null)) {
                    fromPS = "From PS :" + caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getFromPS();
                    caseTransferItemDetailsList.add(fromPS);
                }

                if (!caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getToPS().equals(null)) {
                    toPS = "To PS :" + caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getPsName();
                    caseTransferItemDetailsList.add(toPS);
                }

                if (!caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getFromIoName().equals(null)) {
                    fromIoName = "From IO Name :" + caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getFromIoName();
                    caseTransferItemDetailsList.add(fromIoName);
                }

                if (!caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getToIoName().equals(null)) {
                    toIoName = "To IO Name :" + caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getToIoName();
                    caseTransferItemDetailsList.add(toIoName);
                }

                if (!caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getToUnit().equals(null)) {
                    toUnit = "To Unit :" + caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getToUnit();
                    caseTransferItemDetailsList.add(toUnit);
                }

                if (!caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getCaseNo().equals(null)) {
                    caseNo = "Case No :" + caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getCaseNo();
                    caseTransferItemDetailsList.add(caseNo);
                }

                if (!caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getCaseYr().equals(null)) {
                    caseTransYr = "Case Year :" + caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getCaseYr();
                    caseTransferItemDetailsList.add(caseTransYr);
                }

                if (!caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getTranferDt().equals(null)) {
                    transferDt = "Transfer Date :" + caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getTranferDt();
                    caseTransferItemDetailsList.add(transferDt);
                }

                if (!caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getRemarks().equals(null)) {
                    remarks = "Remarks :" + caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(i).getRemarks();
                    caseTransferItemDetailsList.add(remarks);
                }
                sl_no++;
                tv_SlNo.setTextColor(ContextCompat.getColor(CaseSearchFIRDetailsActivity.this, R.color.color_light_blue));
                tv_SlNo.setText("" + sl_no);

                for (int j = 0; j < caseTransferItemDetailsList.size(); j++) {

                    View itemDetailsView = inflater.inflate(R.layout.fir_case_transfer_list_item_details, null);

                    TextView tv_key = (TextView) itemDetailsView.findViewById(R.id.tv_key);
                    TextView tv_value = (TextView) itemDetailsView.findViewById(R.id.tv_value);
                    RelativeLayout relative_case_transfer_item_details = (RelativeLayout) itemDetailsView.findViewById(R.id.relative_case_transfer_item_details);

                    String data = caseTransferItemDetailsList.get(j);
                    String key = data.substring(0, data.indexOf(":"));
                    String value = data.substring(data.indexOf(":") + 1);

                    tv_key.setTextColor(ContextCompat.getColor(CaseSearchFIRDetailsActivity.this, R.color.color_light_blue));
                    tv_value.setTextColor(ContextCompat.getColor(CaseSearchFIRDetailsActivity.this, R.color.color_light_blue));

                    tv_key.setText(key);
                    tv_value.setText(value);

                    linear_case_transfer_details.addView(relative_case_transfer_item_details, j);
                }

                linear_caseTranfer.addView(v);
                isCaseTransferAdded = true;

            }
            if (!isCaseTransferAdded) {

                Utility.showAlertDialog(CaseSearchFIRDetailsActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_CASE_LOG, false);
            } else {
                openPopupWindowForCaseTransLog();
            }
        } else {

            //Utility.showAlertDialog(CaseSearchFIRDetailsActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_CASE_LOG, false);
        }
    }


    private void showAlertForProceed() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(CaseSearchFIRDetailsActivity.this);
        // alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setTitle("Alert!");
        alertDialog.setMessage("Do you want to capture lat/long of current location and tag it to the P. O.?");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                fetchedLatLongSet();
                //update Map icon in ModifiedCaseSearchResultActivity using Singletone class
                CommunicationViews.getInstance().changeMapIcon(Integer.parseInt(position), latitudeVal, longitudeVal);
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        alertDialog.show();
    }


    private void fetchedLatLongSet() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CAPTURE_LATONG);
        taskManager.setCaptureLatLong(true);

        keys = new String[]{"ps", "case_no", "case_yr", "lat", "long", "userId"};
        if(fromPage.equalsIgnoreCase("UnnaturalDeathDetailsActivity")) {
            values = new String[]{ps_code.trim(), case_no.trim(), fir_yr.trim(), latitudeVal.trim(), longitudeVal.trim(), user_Id.trim()};
        }
        else{
            values = new String[]{ps_code.trim(), case_no.trim(), case_yr.trim(), latitudeVal.trim(), longitudeVal.trim(), user_Id.trim()};
        }
        taskManager.doStartTask(keys, values, true);
    }


    public void parseCapturedLatLong(String result, String[] keys, String[] values) {

        //System.out.println("parseCapturedLatLong: " + result);

        if (result != null && !result.equals("")) {
            bt_crimeMap.setVisibility(View.VISIBLE);
            bt_noCrimeMap.setVisibility(View.GONE);

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;

                    JSONObject result_obj = jObj.getJSONObject("result");
                    caseSearchFIRDetailsList.get(0).setPoLat(result_obj.optString("PO_LAT"));
                    caseSearchFIRDetailsList.get(0).setPoLong(result_obj.optString("PO_LONG"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(CaseSearchFIRDetailsActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
            }
        } else {
            showAlertDialog(CaseSearchFIRDetailsActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_LOCATION_NOT_FOUND, false);
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        ImageLoader.getInstance().destroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    private void getCaseSearchFIRDetailsResult() {



        if(fromPage.equalsIgnoreCase("UnnaturalDeathDetailsActivity"))
        {
            L.e("CASE_YEAR---------"+case_yr);
            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_MODIFIED_CASE_SEARCH_FIR_DETAILS_FROM_UNNATURAL_DEATH);
            taskManager.setModifedCaseSearchFIRDetails(true);
            String[] keys = {"pscode", "gdeno", "gdedate"};
            String[] values = {ps_code.trim(),case_no.trim(),case_yr.trim()};
            taskManager.doStartTask(keys, values, true);
        }
        else{
            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_MODIFIED_CASE_SEARCH_FIR_DETAILS);
            taskManager.setModifedCaseSearchFIRDetails(true);
            String[] keys = {"pscode", "caseno", "caseyr"};
            String[] values = {ps_code.trim(), case_no.trim(), case_yr.trim()};
            taskManager.doStartTask(keys, values, true);
        }

    }

    public void parseModifiedCaseSearchFIRDetailsResult(String result) {

        //System.out.println("CaseSearchFIRDetailsResult: " + result);

        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    rl_FIR_Details.setVisibility(View.VISIBLE);

                    JSONObject resultObj = jObj.getJSONObject("result");

                    parseCaseSearchFIRDetailsResponse(resultObj);

                } else {

                    rl_FIR_Details.setVisibility(View.GONE);
                    showAlertDialog(this, Constants.ERROR_DETAILS_TITLE, Constants.ERROR_MSG_DETAIL, false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }

    private void parseCaseSearchFIRDetailsResponse(JSONObject obj) {
        L.e("Total Response"+obj);
      /*  JSONObject obj=obj1;
        try {
            obj.put("IO_MOBILE","9432160679");
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        caseSearchFIRDetailsList.clear();//=new ArrayList<CaseSearchFIRDetails>();
        CommentList = new ArrayList<>();

        CaseSearchFIRDetails caseSearchFIRDetails = new CaseSearchFIRDetails();

        if (!obj.optString("PSNAME").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setPsName(obj.optString("PSNAME"));
        if (!obj.optString("CASENO").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setCaseNo(obj.optString("CASENO"));
        if(!obj.optString("pm_no").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setPm_no(obj.optString("pm_no"));
        if(!obj.optString("pm_year").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setPm_year(obj.optString("pm_year"));
        if (!obj.optString("IO").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setIo(obj.optString("IO"));
        if (!obj.optString("IO_MOBILE").equalsIgnoreCase("null"))  //IO phone No fetched from server
            caseSearchFIRDetails.setIoPhone(obj.optString("IO_MOBILE"));
        if (!obj.optString("OCNAME").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setOc(obj.optString("OCNAME"));
        if (!obj.optString("OC_NO").equalsIgnoreCase("null"))  //OC phone No fetched from server
            caseSearchFIRDetails.setOcPhone(obj.optString("OC_NO"));
        if (!obj.optString("ACNAME").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setAc(obj.optString("ACNAME"));
        if (!obj.optString("AC_NO").equalsIgnoreCase("null"))  //AC phone No fetched from server
            caseSearchFIRDetails.setAcPhone(obj.optString("AC_NO"));
        if (!obj.optString("DCNAME").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setDc(obj.optString("DCNAME"));
        if (!obj.optString("DC_NO").equalsIgnoreCase("null"))  //DC phone No fetched from server
            caseSearchFIRDetails.setDcPhone(obj.optString("DC_NO"));
        if (!obj.optString("COMPLAINANT").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setComplainant(obj.optString("COMPLAINANT"));
        if (!obj.optString("COMPLAINANT_ADDR").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setComplainant_address(obj.optString("COMPLAINANT_ADDR"));
        if (!obj.optString("GDENO").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setGdeNo(obj.optString("GDENO"));
        if (!obj.optString("FIR_YR").equalsIgnoreCase("null"))
            fir_yr=obj.optString("FIR_YR");
        caseSearchFIRDetails.setFirYr(obj.optString("FIR_YR"));
        if (!obj.optString("GDEDATE").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setGdeDate(obj.optString("GDEDATE"));
        if (!obj.optString("BRIEF_FACT").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setBriefFact(obj.optString("BRIEF_FACT"));
        if (!obj.optString("CASEDATE").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setCaseDate(obj.optString("CASEDATE"));
        if (!obj.optString("FIR_STATUS").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setFirStatus(obj.optString("FIR_STATUS"));
        if (!obj.optString("GR_C_NO").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setGr_c_no(obj.optString("GR_C_NO"));
        if (!obj.optString("STATUS_DATE").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setStatusDate(obj.optString("STATUS_DATE"));
        if (!obj.optString("COURTNAME").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setCourtName(obj.optString("COURTNAME"));
        if (!obj.optString("UNDER_SECTION").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setUnderSection(obj.optString("UNDER_SECTION"));
        if (!obj.optString("CASE_YR").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setCaseYr(obj.optString("CASE_YR"));
        if (!obj.optString("CATEGORY").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setCategory(obj.optString("CATEGORY"));
        if (!obj.optString("FLAG").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setFlag(obj.optString("FLAG"));
        if (!obj.optString("PDF_URL").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setPdf_url(obj.optString("PDF_URL"));
        if (!obj.optString("pmreport_pdf_url").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setPmreport_pdf_url(obj.optString("pmreport_pdf_url"));
        if (!obj.optString("PO_LAT").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setPoLat(obj.optString("PO_LAT"));
        if (!obj.optString("PO_LONG").equalsIgnoreCase("null"))
            caseSearchFIRDetails.setPoLong(obj.optString("PO_LONG"));

        caseSearchFIRDetails.setOcComment(obj.optString("OC_COMMENT"));
        caseSearchFIRDetails.setAcComment(obj.optString("AC_COMMENT"));
        caseSearchFIRDetails.setDcComment(obj.optString("DC_COMMENT"));

       /* if (!caseSearchFIRDetails.getAcComment().equalsIgnoreCase("null")){
            CommentList.add("AC Comment:- "+caseSearchFIRDetails.getAcComment());
        }
        if(!caseSearchFIRDetails.getDcComment().equalsIgnoreCase("null")){
            CommentList.add("DC Comment:- "+caseSearchFIRDetails.getDcComment());
        }
        if(!caseSearchFIRDetails.getOcComment().equalsIgnoreCase("null")){
            CommentList.add("OC Comment:- "+caseSearchFIRDetails.getOcComment());
        }*/
       if(obj.optJSONArray("ALL_ACCUSED")!=null) {

           if (obj.optJSONArray("ALL_ACCUSED").length() > 0) {

               JSONArray jsonArray = obj.optJSONArray("ALL_ACCUSED");
               for (int i = 0; i < jsonArray.length(); i++) {

                   JSONObject JSON_obj = null;

                   try {

                       JSON_obj = jsonArray.getJSONObject(i);
                       String alias = "";

                       CaseSearchFIRAllAccused caseSearchFIRAllAccused = new CaseSearchFIRAllAccused();

                       if (!JSON_obj.optString("NAME").equalsIgnoreCase("null"))
                           caseSearchFIRAllAccused.setName(JSON_obj.optString("NAME"));

                       if (!JSON_obj.optString("ALIAS1").equalsIgnoreCase("null"))
                           alias = " @" + JSON_obj.optString("ALIAS1");
                       if (!JSON_obj.optString("ALIAS2").equalsIgnoreCase("null"))
                           alias = alias + "/" + JSON_obj.optString("ALIAS2");
                       if (!JSON_obj.optString("ALIAS3").equalsIgnoreCase("null"))
                           alias = alias + "/" + JSON_obj.optString("ALIAS3");
                       if (!JSON_obj.optString("ALIAS4").equalsIgnoreCase("null"))
                           alias = alias + "/" + JSON_obj.optString("ALIAS4");

                       caseSearchFIRAllAccused.setAlias(alias);
                       caseSearchFIRAllAccused.setFather(JSON_obj.optString("FATHER"));
                       caseSearchFIRAllAccused.setAddress(JSON_obj.optString("ADDRESS"));
                       caseSearchFIRAllAccused.setPsCase(JSON_obj.optString("PS"));

                       caseSearchFIRDetails.setCaseSearchFIRAllAccusedList(caseSearchFIRAllAccused);


                   } catch (JSONException e) {
                       e.printStackTrace();
                   }
               }
           }
       }

        try {
            JSONArray all_accused_array = obj.getJSONArray("ALL_ACCUSED");

            allAccusedList = Utility
                    .parseCrimiinalAssociateRecords(CaseSearchFIRDetailsActivity.this, all_accused_array);

            caseSearchFIRDetails.setAllAccusedDataList(allAccusedList);

        } catch (JSONException e) {
            e.printStackTrace();
        }



        /* All Arrested Data */
        if(obj.optJSONArray("ALL_ARRESTED")!=null) {


            if (obj.optJSONArray("ALL_ARRESTED").length() > 0) {

                JSONArray jsonArray = obj.optJSONArray("ALL_ARRESTED");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject JSON_obj = null;

                    try {

                        JSON_obj = jsonArray.getJSONObject(i);
                        String alias = "";

                        CaseSearchFIRAllArrested caseSearchFIRAllArrested = new CaseSearchFIRAllArrested();

                        if (!JSON_obj.optString("ARRESTEE").equalsIgnoreCase("null"))
                            caseSearchFIRAllArrested.setName(JSON_obj.optString("ARRESTEE"));

                        if (!JSON_obj.optString("ALIASNAME").equalsIgnoreCase("null"))
                            alias = " @" + JSON_obj.optString("ALIASNAME");
                    /*if (!JSON_obj.optString("ALIAS2").equalsIgnoreCase("null"))
                        alias = alias + "/" + JSON_obj.optString("ALIAS2");
                    if (!JSON_obj.optString("ALIAS3").equalsIgnoreCase("null"))
                        alias = alias + "/" + JSON_obj.optString("ALIAS3");
                    if (!JSON_obj.optString("ALIAS4").equalsIgnoreCase("null"))
                        alias = alias + "/" + JSON_obj.optString("ALIAS4");*/

                        caseSearchFIRAllArrested.setAlias(alias);
                        caseSearchFIRAllArrested.setFather(JSON_obj.optString("FATHER_HUSBAND"));
                        caseSearchFIRAllArrested.setAddress(JSON_obj.optString("ADDRESS"));
                        caseSearchFIRAllArrested.setPsCase(JSON_obj.optString("PSCODE"));
                        caseSearchFIRAllArrested.setArrest_date(JSON_obj.optString("ARREST_DATE"));
                        caseSearchFIRAllArrested.setArrested_by(JSON_obj.optString("PS"));
                        caseSearchFIRAllArrested.setPrv_criminal_no(JSON_obj.optString("PROV_CRM_NO"));
                        caseSearchFIRDetails.setCaseSearchFIRAllArrestedList(caseSearchFIRAllArrested);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        try {
            JSONArray all_arrested_array = obj.getJSONArray("ALL_ARRESTED");
         //   Log.e("LOG","Log "+all_arrested_array );
            allArrestedList = Utility
                    .parseCrimiinalAssociateRecords(CaseSearchFIRDetailsActivity.this, all_arrested_array);
           // Log.e("LOG","Log "+allArrestedList.get(0) );
            caseSearchFIRDetails.setAllArrestedDataList(allArrestedList);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        /* Case Transfer data parse */
        try {
            JSONArray case_transfer_array = obj.getJSONArray("CASE_TRANSFER");
            for (int i = 0; i < case_transfer_array.length(); i++) {

                JSONObject JSON_obj = null;
                try {
                    JSON_obj = case_transfer_array.getJSONObject(i);
                    CaseTransferDetails caseTransferDetails = new CaseTransferDetails();

                    if (!JSON_obj.optString("FROM_PS").equalsIgnoreCase("null"))
                        caseTransferDetails.setFromPS(JSON_obj.optString("FROM_PS"));

                    if (!JSON_obj.optString("TO_PS").equalsIgnoreCase("null"))
                        caseTransferDetails.setToPS(JSON_obj.optString("TO_PS"));

                    if (!JSON_obj.optString("FROM_IONAME").equalsIgnoreCase("null"))
                        caseTransferDetails.setFromIoName(JSON_obj.optString("FROM_IONAME"));

                    if (!JSON_obj.optString("TO_IONAME").equalsIgnoreCase("null"))
                        caseTransferDetails.setToIoName(JSON_obj.optString("TO_IONAME"));

                    if (!JSON_obj.optString("TO_UNIT").equalsIgnoreCase("null"))
                        caseTransferDetails.setToUnit(JSON_obj.optString("TO_UNIT"));

                    if (!JSON_obj.optString("CASENO").equalsIgnoreCase("null"))
                        caseTransferDetails.setCaseNo(JSON_obj.optString("CASENO"));

                    if (!JSON_obj.optString("CASE_YR").equalsIgnoreCase("null"))
                        caseTransferDetails.setCaseYr(JSON_obj.optString("CASE_YR"));

                    if (!JSON_obj.optString("DT_TRANSFER").equalsIgnoreCase("null"))
                        caseTransferDetails.setTranferDt(JSON_obj.optString("DT_TRANSFER"));

                    if (!JSON_obj.optString("REMARKS").equalsIgnoreCase("null"))
                        caseTransferDetails.setRemarks(JSON_obj.optString("REMARKS"));

                    if (JSON_obj.optString("PSNAME") != null && !JSON_obj.optString("PSNAME").equalsIgnoreCase("null") && !JSON_obj.optString("PSNAME").equalsIgnoreCase(""))
                        caseTransferDetails.setPsName(JSON_obj.optString("PSNAME"));

                    caseSearchFIRDetails.setCaseSearchCaseTransferList(caseTransferDetails);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {
            if (obj.getJSONObject("CHARGE_SHEET") != null ) {

                JSONObject charge_sheet_obj = obj.getJSONObject("CHARGE_SHEET");
                CaseSearchFIRChargeSheet caseSearchFIRChargeSheet = new CaseSearchFIRChargeSheet();
                if (!charge_sheet_obj.optString("FR_CHGDATE").equalsIgnoreCase("null"))
                    caseSearchFIRChargeSheet.setFrChgDate(charge_sheet_obj.optString("FR_CHGDATE"));
                if (!charge_sheet_obj.optString("FR_CHGNO").equalsIgnoreCase("null"))
                    caseSearchFIRChargeSheet.setFrChgNo(charge_sheet_obj.optString("FR_CHGNO"));
                if (!charge_sheet_obj.optString("COURT_OF").equalsIgnoreCase("null"))
                    caseSearchFIRChargeSheet.setCourtOf(charge_sheet_obj.optString("COURT_OF"));
                if (!charge_sheet_obj.optString("FRTYPE").equalsIgnoreCase("null"))
                    caseSearchFIRChargeSheet.setFrType(charge_sheet_obj.optString("FRTYPE"));
                if (!charge_sheet_obj.optString("SUPPLEMENTARY_ORIGINAL").equalsIgnoreCase("null"))
                    caseSearchFIRChargeSheet.setSupplementaryOriginal(charge_sheet_obj.optString("SUPPLEMENTARY_ORIGINAL"));
                if (!charge_sheet_obj.optString("BRIEF_FACTS").equalsIgnoreCase("null"))
                    caseSearchFIRChargeSheet.setBriefFact(charge_sheet_obj.optString("BRIEF_FACTS"));

                String actsValue = "";

                if (!charge_sheet_obj.optString("ACT1").equalsIgnoreCase("null")) {
                    actsValue = charge_sheet_obj.optString("ACT1");
                }

                if (!charge_sheet_obj.optString("ACT2").equalsIgnoreCase("null")) {
                    actsValue = actsValue + "/" + charge_sheet_obj.optString("ACT2");
                }

                if (!charge_sheet_obj.optString("ACT3").equalsIgnoreCase("null")) {
                    actsValue = actsValue + "/" + charge_sheet_obj.optString("ACT3");
                }

                if (!charge_sheet_obj.optString("ACT_OTHER").equalsIgnoreCase("null")) {
                    actsValue = actsValue + "/" + charge_sheet_obj.optString("ACT_OTHER");
                }

                caseSearchFIRChargeSheet.setActs(actsValue);

                try {
                    if(charge_sheet_obj.has("witness")) {
                        JSONArray chargesheet_witness_array = charge_sheet_obj.optJSONArray("witness");

                        if (chargesheet_witness_array.length() > 0) {

                            for (int i = 0; i < chargesheet_witness_array.length(); i++) {

                                CaseSearchWitnessDetails caseSearchWitnessDetails = new CaseSearchWitnessDetails();
                                JSONObject witness_obj = chargesheet_witness_array.getJSONObject(i);

                                if (!witness_obj.optString("NAME_WITNESS").equalsIgnoreCase("null"))
                                    caseSearchWitnessDetails.setNameWitness(witness_obj.optString("NAME_WITNESS"));
                                if (!witness_obj.optString("ADDR_WITNESS").equalsIgnoreCase("null"))
                                    caseSearchWitnessDetails.setAddressWitness(witness_obj.optString("ADDR_WITNESS"));

                                caseSearchFIRChargeSheet.setCaseSearchWitnessDetailsList(caseSearchWitnessDetails);

                            }

                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                try {
                    if(charge_sheet_obj.has("property")) {
                        JSONArray property_array = charge_sheet_obj.optJSONArray("property");

                        if (property_array.length() > 0) {

                            for (int i = 0; i < property_array.length(); i++) {

                                CaseSearchPropertyDetails caseSearchPropertyDetails = new CaseSearchPropertyDetails();
                                JSONObject properties_obj = property_array.getJSONObject(i);

                                if (!properties_obj.optString("PROPERTYDESC").equalsIgnoreCase("null"))
                                    caseSearchPropertyDetails.setPropertyDesc(properties_obj.optString("PROPERTYDESC"));
                                if (!properties_obj.optString("PSPROPREGNO").equalsIgnoreCase("null"))
                                    caseSearchPropertyDetails.setPsPropRegNo(properties_obj.optString("PSPROPREGNO"));

                                caseSearchFIRChargeSheet.setCaseSearchPropertyDetailsList(caseSearchPropertyDetails);

                            }

                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                try {
                    if(charge_sheet_obj.has("acsd_chargesheet")) {
                        JSONArray acsd_chargesheet_array = charge_sheet_obj.optJSONArray("acsd_chargesheet");
                        if (acsd_chargesheet_array.length() > 0) {

                            for (int i = 0; i < acsd_chargesheet_array.length(); i++) {

                                CaseSearchAccusedDetails caseSearchAccusedDetails = new CaseSearchAccusedDetails();
                                JSONObject accused_obj = acsd_chargesheet_array.getJSONObject(i);

                                if (!accused_obj.optString("NAME_ACCUSED").equalsIgnoreCase("null"))
                                    caseSearchAccusedDetails.setNameAccused(accused_obj.optString("NAME_ACCUSED"));
                                if (!accused_obj.optString("ADDR_ACCUSED").equalsIgnoreCase("null"))
                                    caseSearchAccusedDetails.setAddressAccused(accused_obj.optString("ADDR_ACCUSED"));

                                caseSearchFIRChargeSheet.setCaseSearchAccusedDetailsList(caseSearchAccusedDetails);
                            }
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                try {
                    if(charge_sheet_obj.has("acsd_not_chargesheet")) {
                        JSONArray acsd_not_chargesheet_array = charge_sheet_obj.optJSONArray("acsd_not_chargesheet");
                        if (acsd_not_chargesheet_array.length() > 0) {

                            for (int i = 0; i < acsd_not_chargesheet_array.length(); i++) {

                                CaseSearchAccusedDetails caseSearchAccusedDetails = new CaseSearchAccusedDetails();
                                JSONObject accused_obj = acsd_not_chargesheet_array.getJSONObject(i);

                                if (!accused_obj.optString("NAME_ACCUSED").equalsIgnoreCase("null"))
                                    caseSearchAccusedDetails.setNameAccused(accused_obj.optString("NAME_ACCUSED"));
                                if (!accused_obj.optString("ADDR_ACCUSED").equalsIgnoreCase("null"))
                                    caseSearchAccusedDetails.setAddressAccused(accused_obj.optString("ADDR_ACCUSED"));

                                caseSearchFIRChargeSheet.setCaseSearchNotAccusedDetailsList(caseSearchAccusedDetails);
                            }
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                caseSearchFIRDetails.setCaseSearchFIRChargeSheetList(caseSearchFIRChargeSheet);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            if (obj.getJSONObject("WARRANT") != null) {

                CaseSearchFIRWarrant caseSearchFIRWarrant = new CaseSearchFIRWarrant();

                JSONObject warrant_obj = obj.getJSONObject("WARRANT");

                caseSearchFIRWarrant.setSlNo(warrant_obj.optString("SLNO"));
                caseSearchFIRWarrant.setSlDate(warrant_obj.optString("SLDATE"));
                caseSearchFIRWarrant.setWaType(warrant_obj.optString("WATYPE"));
                caseSearchFIRWarrant.setIssueCourt(warrant_obj.optString("ISSUE_COURT"));
                caseSearchFIRWarrant.setReturnableDateToCourt(warrant_obj.optString("RETURNABLE_DATE_TO_COURT"));
                caseSearchFIRWarrant.setOfficerToServe(warrant_obj.optString("OFFICER_TO_SERVE"));
                caseSearchFIRWarrant.setProcessNo(warrant_obj.optString("PROCESS_NO"));
                caseSearchFIRWarrant.setWaStatus(warrant_obj.optString("WA_STATUS"));
                caseSearchFIRWarrant.setRemarks(warrant_obj.optString("REMARKS"));
                caseSearchFIRWarrant.setPsName(warrant_obj.optString("PSNAME"));

                try {
                    if(warrant_obj.has("warrantees")) {

                        JSONArray warrantees_array = warrant_obj.optJSONArray("warrantees");

                        if (warrantees_array.length() > 0) {

                            for (int i = 0; i < warrantees_array.length(); i++) {

                                FIRWarranteesDetails firWarranteesDetails = new FIRWarranteesDetails();

                                JSONObject warranteesObj = warrantees_array.getJSONObject(i);

                                if (warranteesObj.optString("SLNO") != null && !warranteesObj.optString("SLNO").equalsIgnoreCase("") && !warranteesObj.optString("SLNO").equalsIgnoreCase("null")) {
                                    firWarranteesDetails.setSl_no(warranteesObj.optString("SLNO"));
                                }
                                if (warranteesObj.optString("NAME") != null && !warranteesObj.optString("NAME").equalsIgnoreCase("") && !warranteesObj.optString("NAME").equalsIgnoreCase("null")) {
                                    firWarranteesDetails.setWarrantee_name(warranteesObj.optString("NAME"));
                                }
                                if (warranteesObj.optString("ADDRESS") != null && !warranteesObj.optString("ADDRESS").equalsIgnoreCase("") && !warranteesObj.optString("ADDRESS").equalsIgnoreCase("null")) {
                                    firWarranteesDetails.setWarrantee_address(warranteesObj.optString("ADDRESS"));
                                }

                                caseSearchFIRWarrant.setFirWarranteesDetailsList(firWarranteesDetails);

                            }

                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                caseSearchFIRDetails.setCaseSearchFIRWarrantList(caseSearchFIRWarrant);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        caseSearchFIRDetailsList.add(caseSearchFIRDetails);
        setData(caseSearchFIRDetailsList);

    }

    @SuppressLint("NewApi")
    private void setData(final List<CaseSearchFIRDetails> caseSearchFIRDetailsList) {

        this.caseSearchFIRDetailsList = caseSearchFIRDetailsList;

        Log.e("FLAG", caseSearchFIRDetailsList.get(0).getFlag());

        String cahrgeSheetDetails = "";
        String warrantDetails = "";
        String us_address = "";
        String gde_details = "";
        String status_details = "";
        String warrant_status_details = "";

        if (caseSearchFIRDetailsList.get(0).getFirStatus() != null && !caseSearchFIRDetailsList.get(0).getFirStatus().equalsIgnoreCase("null") && !caseSearchFIRDetailsList.get(0).getFirStatus().equalsIgnoreCase("")) {

            // STATUS name change
            if (caseSearchFIRDetailsList.get(0).getFirStatus().equalsIgnoreCase(Constants.STATUS_INVESTIGATION_IN_PROGRESS))
                status_details = Constants.STATUS_PENDING;
            else if (caseSearchFIRDetailsList.get(0).getFirStatus().equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_NON_COGNIZABLE))
                status_details = Constants.STATUS_FR_NON_COG;
            else if (caseSearchFIRDetailsList.get(0).getFirStatus().equalsIgnoreCase(Constants.STATUS_CHARGESHEETED))
                status_details = Constants.STATUS_CS;
            else if (caseSearchFIRDetailsList.get(0).getFirStatus().equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_CIVIL))
                status_details = Constants.STATUS_FR_CIVIL;
            else if (caseSearchFIRDetailsList.get(0).getFirStatus().equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_TRUE))
                status_details = Constants.STATUS_FRT;
            else if (caseSearchFIRDetailsList.get(0).getFirStatus().equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_FALSE))
                status_details = Constants.STATUS_FR_FALSE;
            else if (caseSearchFIRDetailsList.get(0).getFirStatus().equalsIgnoreCase(Constants.STATUS_TRANSFERRED_TO_OTHER_UNIT)) {
                if (caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().size() > 0) {

                    String trfd_psDetails = "";
                    if (caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(0).getPsName() != null
                            && !caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(0).getPsName().equalsIgnoreCase("null")
                            && !caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(0).getPsName().equalsIgnoreCase("")) {

                        trfd_psDetails = caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(0).getPsName();

                        if (caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(0).getToUnit() != null
                                && !caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(0).getToUnit().equalsIgnoreCase("null")
                                && !caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(0).getToUnit().equalsIgnoreCase("")) {

                            trfd_psDetails = trfd_psDetails + ", " + caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(0).getToUnit();

                        }
                        status_details = trfd_psDetails;
                    } else if (caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(0).getToUnit() != null
                            && !caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(0).getToUnit().equalsIgnoreCase("null")
                            && !caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(0).getToUnit().equalsIgnoreCase("")) {

                        trfd_psDetails = caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().get(0).getToUnit();
                        status_details = trfd_psDetails;
                    } else {
                        status_details = Constants.STATUS_TRFD;
                    }
                } else {
                    status_details = Constants.STATUS_TRFD;
                }
            } else
                status_details = caseSearchFIRDetailsList.get(0).getFirStatus();
        }

        if (caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().size() > 0) {

            if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getFrChgNo().equalsIgnoreCase("")) {
                cahrgeSheetDetails = caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getFrChgNo();
            }

            if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getFrChgDate().equalsIgnoreCase("")) {
                cahrgeSheetDetails = cahrgeSheetDetails + " on " + caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getFrChgDate();
            }

            if (!cahrgeSheetDetails.startsWith("C")) {
                cahrgeSheetDetails = "C-" + cahrgeSheetDetails;
            }

            if (cahrgeSheetDetails.length() > 2) {
                cahrgeSheetDetails = status_details + " / " + cahrgeSheetDetails;
                SpannableString chargeSheetString = new SpannableString(cahrgeSheetDetails);
                chargeSheetString.setSpan(new UnderlineSpan(), 0, chargeSheetString.length(), 0);
                tv_presentStatus.setText(chargeSheetString);
            } else {
                tv_presentStatus.setText(status_details);
            }


            String str = status_details + " / ";
            if (tv_presentStatus.getText().toString().trim().contains(str)) {
                if (tv_presentStatus.getText().toString().trim().length() > 2) {

                    tv_presentStatus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popUpWindowForCaseSearchChargeSheetDetails(0, itemName);
                        }
                    });
                }
            }
        } else {
            tv_presentStatus.setText(status_details);
        }


        if (caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().size() > 0) {

            if (caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getWaStatus() != null && !caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getWaStatus().equalsIgnoreCase("null")
                    && !caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getWaStatus().equalsIgnoreCase("")) {
                warrant_status_details = caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getWaStatus();
            }

            if (fromPage.equalsIgnoreCase("ModifiedCaseSearchResultActivity")) {

                if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getSlNo().equalsIgnoreCase("")) {
                    warrantDetails = caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getSlNo();
                }

                if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getPsName().equalsIgnoreCase("")) {
                    warrantDetails = warrantDetails + " of " + caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getPsName();
                    warrantDetails = warrant_status_details + " / " + warrantDetails;
                }

                tv_warrant.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));


                SpannableString warrantString = new SpannableString(warrantDetails);
                warrantString.setSpan(new UnderlineSpan(), 0, warrantString.length(), 0);
                tv_warrant.setText(warrantString);

            } else {
                if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getSlNo().equalsIgnoreCase("")) {
                    warrantDetails = warrant_status_details + " / " + caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getSlNo() + " of " + ps_name;
                } else {
                    warrantDetails = warrant_status_details;
                }

                tv_warrant.setTextColor(ContextCompat.getColor(this, R.color.edit_text_hint_color));
                tv_warrant.setText(warrantDetails);
            }


            // warrant click event
            String str = warrant_status_details + " / ";

            if (tv_warrant.getText().toString().trim().contains(str)) {
                if (tv_warrant.getText().toString().trim().length() > 0) {

                    if (fromPage.equalsIgnoreCase("ModifiedCaseSearchResultActivity")) {

                        tv_warrant.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                popUpWindowForCaseSearchFIRWarrant(v);
                            }

                        });
                    }
                }
            } else {
                tv_warrant.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                tv_warrant.setText("No warrant found");
            }

        } else {
            tv_warrant.setTextColor(ContextCompat.getColor(this, R.color.color_black));
            tv_warrant.setText("No warrant found");
        }


        pdf_download_url = caseSearchFIRDetailsList.get(0).getPdf_url();
        if (pdf_download_url.length() == 0) {
            iv_pdf.setImageResource(R.drawable.pdf_fir_detail_deactivate);
            rl_fir.setOnClickListener(null);
        }
        pmreport_pdf_download_url = caseSearchFIRDetailsList.get(0).getPmreport_pdf_url();
        if (pmreport_pdf_download_url.length() == 0) {
            iv_pm_pdf.setImageResource(R.drawable.pdf_fir_detail_deactivate);
            rl_pm.setOnClickListener(null);
        }
        if (caseSearchFIRDetailsList.get(0).getCaseSearchCaseTransferList().size() == 0) {
            iv_case_log.setImageResource(R.drawable.case_log_deactivate);
            rl_case_log.setOnClickListener(null);
        }
        if (log_response.length() == 0) {
            iv_log.setImageResource(R.drawable.log_deactivate);
            rl_log.setOnClickListener(null);
        }


        Log.e("Lat :", caseSearchFIRDetailsList.get(0).getPoLat());
        Log.e("long :", caseSearchFIRDetailsList.get(0).getPoLong());

        // Crime Map button
        if (((caseSearchFIRDetailsList.get(0).getPoLat() != null) && (!caseSearchFIRDetailsList.get(0).getPoLat().equalsIgnoreCase("null")) && (!caseSearchFIRDetailsList.get(0).getPoLat().equalsIgnoreCase("")))
                && ((caseSearchFIRDetailsList.get(0).getPoLong() != null) && (!caseSearchFIRDetailsList.get(0).getPoLong().equalsIgnoreCase("null")) && (!caseSearchFIRDetailsList.get(0).getPoLong().equalsIgnoreCase("")))) {


            //bt_crimeMap.setBackground(ContextCompat.getDrawable(this,R.drawable.view_map));
            bt_crimeMap.setVisibility(View.VISIBLE);
            bt_noCrimeMap.setVisibility(View.GONE);
            bt_crimeMap.setClickable(true);
        } else {

            //bt_crimeMap.setBackground(ContextCompat.getDrawable(this,R.drawable.load_map));
            bt_noCrimeMap.setVisibility(View.VISIBLE);
            bt_crimeMap.setVisibility(View.GONE);
            bt_noCrimeMap.setClickable(true);
        }


        if (caseSearchFIRDetailsList.get(0).getComplainant_address() != null && !caseSearchFIRDetailsList.get(0).getComplainant_address().equalsIgnoreCase("null") && !caseSearchFIRDetailsList.get(0).getComplainant_address().equalsIgnoreCase("")) {
            us_address = caseSearchFIRDetailsList.get(0).getComplainant_address();
        }

        if (!us_address.equalsIgnoreCase("")) {
            tv_complainantAddress.setVisibility(View.VISIBLE);
            tv_complainantAddress.setText(us_address);
        } else {
            tv_complainantAddress.setVisibility(View.GONE);
        }

        tv_category.setText(caseSearchFIRDetailsList.get(0).getCategory());


        if (caseSearchFIRDetailsList.get(0).getIo() != null && !caseSearchFIRDetailsList.get(0).getIo().equalsIgnoreCase("null") && !caseSearchFIRDetailsList.get(0).getIo().equalsIgnoreCase("")) {
            tv_IO.setVisibility(View.VISIBLE);
            tv_IO.setText("I.O.: " + caseSearchFIRDetailsList.get(0).getIo());
        } else {
            tv_IO.setVisibility(View.GONE);
        }
        if (caseSearchFIRDetailsList.get(0).getOc() != null && !caseSearchFIRDetailsList.get(0).getOc().equalsIgnoreCase("null") && !caseSearchFIRDetailsList.get(0).getOc().equalsIgnoreCase("")) {
            tv_OC.setVisibility(View.VISIBLE);
            tv_OC.setText("O.C.: " + caseSearchFIRDetailsList.get(0).getOc()); //oc name show
            /*tv_OC.setText("O.C.: " );*/  //only rank show (oc)
        } else {
            tv_OC.setVisibility(View.GONE);
        }
        if (caseSearchFIRDetailsList.get(0).getAc() != null && !caseSearchFIRDetailsList.get(0).getAc().equalsIgnoreCase("null") && !caseSearchFIRDetailsList.get(0).getAc().equalsIgnoreCase("")) {
            tv_AC.setVisibility(View.VISIBLE);
            tv_AC.setText("A.C.: " + caseSearchFIRDetailsList.get(0).getAc()); //ac name show
            /*tv_AC.setText("A.C.: " );*/ //only ac rank show (ac)
        } else {
            tv_AC.setVisibility(View.GONE);
        }
        if (caseSearchFIRDetailsList.get(0).getDc() != null && !caseSearchFIRDetailsList.get(0).getDc().equalsIgnoreCase("null") && !caseSearchFIRDetailsList.get(0).getDc().equalsIgnoreCase("")) {
            tv_DC.setVisibility(View.VISIBLE);
            tv_DC.setText("D.C.: " + caseSearchFIRDetailsList.get(0).getDc()); //dc name show
            /*tv_DC.setText("D.C.: " );*/ //only rank show (dc)
        } else {
            tv_DC.setVisibility(View.GONE);
        }

        if (caseSearchFIRDetailsList.get(0).getIoPhone() != null && !caseSearchFIRDetailsList.get(0).getIoPhone().equalsIgnoreCase("null") && !caseSearchFIRDetailsList.get(0).getIoPhone().equalsIgnoreCase("")) {
            linear_io_phone_holder.setVisibility(View.VISIBLE);
            //  String IO_Phone="( "+caseSearchFIRDetailsList.get(0).getIoPhone()+" )";
            tv_IO_Phone.setText(caseSearchFIRDetailsList.get(0).getIoPhone());
            Linkify.addLinks(tv_IO_Phone, Linkify.PHONE_NUMBERS);

            if (caseSearchFIRDetailsList.get(0).getOcPhone() != null && !caseSearchFIRDetailsList.get(0).getOcPhone().equalsIgnoreCase("null") && !caseSearchFIRDetailsList.get(0).getOcPhone().equalsIgnoreCase("")) {
                ll_oc_phone_holder.setVisibility(View.VISIBLE);
                tv_oc_phone.setText(caseSearchFIRDetailsList.get(0).getOcPhone());
                Linkify.addLinks(tv_oc_phone, Linkify.PHONE_NUMBERS);

                if (caseSearchFIRDetailsList.get(0).getAcPhone() != null && !caseSearchFIRDetailsList.get(0).getAcPhone().equalsIgnoreCase("null") && !caseSearchFIRDetailsList.get(0).getAcPhone().equalsIgnoreCase("")) {
                    ll_ac_phone_holder.setVisibility(View.VISIBLE);
                    tv_ac_phone.setText(caseSearchFIRDetailsList.get(0).getAcPhone());
                    Linkify.addLinks(tv_ac_phone, Linkify.PHONE_NUMBERS);

                    if (caseSearchFIRDetailsList.get(0).getDcPhone() != null && !caseSearchFIRDetailsList.get(0).getDcPhone().equalsIgnoreCase("null") && !caseSearchFIRDetailsList.get(0).getDcPhone().equalsIgnoreCase("")) {
                        ll_dc_phone_holder.setVisibility(View.VISIBLE);
                        tv_dc_phone.setText(caseSearchFIRDetailsList.get(0).getDcPhone());
                        Linkify.addLinks(tv_dc_phone, Linkify.PHONE_NUMBERS);
          /* final SpannableStringBuilder sb = new SpannableStringBuilder(IO_Phone);
           // Span to set text color to some RGB value
           final ForegroundColorSpan fcs = new ForegroundColorSpan(getResources().getColor(android.R.color.black));
           sb.setSpan(fcs, 0, IO_Phone.indexOf("("), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
           tv_IO_Phone.setText(sb);*/
                        //tv_IO_Phone.setOnClickListener(this);
                    } else {
                        linear_io_phone_holder.setVisibility(View.GONE);
                    }

                    tv_complaint.setText("Complainant: " + caseSearchFIRDetailsList.get(0).getComplainant());
                    tv_court.setText(caseSearchFIRDetailsList.get(0).getCourtName());
                    tv_CGR.setText(caseSearchFIRDetailsList.get(0).getGr_c_no());


                    if (caseSearchFIRDetailsList.get(0).getGdeNo() != null && !caseSearchFIRDetailsList.get(0).getGdeNo().equalsIgnoreCase("null") && !caseSearchFIRDetailsList.get(0).getGdeNo().equalsIgnoreCase("")) {
                        gde_details = caseSearchFIRDetailsList.get(0).getGdeNo();
                    }
                    if (caseSearchFIRDetailsList.get(0).getGdeDate() != null && !caseSearchFIRDetailsList.get(0).getGdeDate().equalsIgnoreCase("null") && !caseSearchFIRDetailsList.get(0).getGdeDate().equalsIgnoreCase("")) {
                        gde_details = gde_details + " dt. " + caseSearchFIRDetailsList.get(0).getGdeDate();
                    }
                    tv_gde.setText(gde_details);


                    if (!caseSearchFIRDetailsList.get(0).getBriefFact().equalsIgnoreCase("")) {
                        tv_BriefFact.setText(caseSearchFIRDetailsList.get(0).getBriefFact());
                    } else {
                        tv_BriefFact.setText("No data found");
                    }

                    TextView tv_instruction_header = new TextView(this);
                    tv_instruction_header.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                    Utility.changefonts(tv_instruction_header, this, "Calibri.ttf");

                    if (caseSearchFIRDetailsList.get(0).getFlag().equalsIgnoreCase("OF") || caseSearchFIRDetailsList.get(0).getFlag().equalsIgnoreCase("CF")) {

                        iv_edit_comment.setVisibility(View.VISIBLE);

                        CommentList.clear();
                        CommentList.add("OC Comment:- " + caseSearchFIRDetailsList.get(0).getOcComment().replace("null", "N/A"));
                        CommentList.add("AC Comment:- " + caseSearchFIRDetailsList.get(0).getAcComment().replace("null", "N/A"));
                        CommentList.add("DC Comment:- " + caseSearchFIRDetailsList.get(0).getDcComment().replace("null", "N/A"));

                        if (CommentList.size() > 0) {


                            tv_instruction_header.setText("Officers as per hierarchy will be able to read/give their views/instructions.\n" +
                                    "To be viewed/edited by:-\n ");

                            linear_comments.addView(tv_instruction_header);
                            linear_comments.setId(0);

                        }

                        for (int i = 0; i < CommentList.size(); i++) {

                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                            EditText et_comment = new EditText(this);
                            et_comment.setText("  " + "\u2022" + "   " + CommentList.get(i).trim());
                            et_comment.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                            et_comment.setTextSize(14);

                            params.setMargins(0, 0, 0, 10);
                            et_comment.setLayoutParams(params);
                            et_comment.setEnabled(false);
                            Utility.changefonts(et_comment, this, "Calibri.ttf");
                            linear_comments.addView(et_comment);
                            linear_comments.setId(i + 1);

                        }


                    } else {

                        iv_edit_comment.setVisibility(View.GONE);

                        tv_instruction_header.setText("No comment found");

                        linear_comments.addView(tv_instruction_header);
                        linear_comments.setId(0);

                    }


                    iv_edit_comment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

                                btn_comment_post.setVisibility(View.VISIBLE);

                                for (int i = 1; i <= 3; i++) {
                                    ((EditText) linear_comments.getChildAt(i)).setEnabled(true);
                                    ((EditText) linear_comments.getChildAt(i)).setBackground(ContextCompat.getDrawable(CaseSearchFIRDetailsActivity.this, R.drawable.border));

                                }

                                final EditText et_OC_comment = ((EditText) linear_comments.getChildAt(1));
                                final EditText et_AC_comment = ((EditText) linear_comments.getChildAt(2));
                                final EditText et_DC_comment = ((EditText) linear_comments.getChildAt(3));

                                et_OC_comment.addTextChangedListener(new TextWatcher() {

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                        // TODO Auto-generated method stub

                                    }

                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count,
                                                                  int after) {
                                        // TODO Auto-generated method stub

                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        if (!s.toString().contains("OC Comment:- ")) {
                                            et_OC_comment.setText("  " + "\u2022" + "   " + "OC Comment:- ");
                                            Selection.setSelection(et_OC_comment.getText(), et_OC_comment.getText().length());

                                        }
                                    }
                                });

                                et_AC_comment.addTextChangedListener(new TextWatcher() {

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                        // TODO Auto-generated method stub

                                    }

                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count,
                                                                  int after) {
                                        // TODO Auto-generated method stub

                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {

                                        if (!s.toString().contains("AC Comment:- ")) {
                                            et_AC_comment.setText("  " + "\u2022" + "   " + "AC Comment:- ");
                                            Selection.setSelection(et_AC_comment.getText(), et_AC_comment.getText().length());

                                        }
                                    }
                                });

                                et_DC_comment.addTextChangedListener(new TextWatcher() {

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                        // TODO Auto-generated method stub

                                    }

                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count,
                                                                  int after) {
                                        // TODO Auto-generated method stub

                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {

                                        if (!s.toString().contains("DC Comment:- ")) {
                                            et_DC_comment.setText("  " + "\u2022" + "   " + "DC Comment:- ");
                                            Selection.setSelection(et_DC_comment.getText(), et_DC_comment.getText().length());

                                        }

                                    }
                                });

                            }

                        }
                    });


                    //-----------------------Post Button Click Events------------------------------------------//


                    btn_comment_post.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            btn_comment_post.setVisibility(View.GONE);

                            String oc_comment = ((EditText) linear_comments.getChildAt(1)).getText().toString().trim();
                            String ac_comment = ((EditText) linear_comments.getChildAt(2)).getText().toString().trim();
                            String dc_comment = ((EditText) linear_comments.getChildAt(3)).getText().toString().trim();

                            for (int i = 1; i <= 3; i++) {
                                ((EditText) linear_comments.getChildAt(i)).setEnabled(false);
                                ((EditText) linear_comments.getChildAt(i)).setBackgroundResource(0);

                            }
                            if (fromPage.equalsIgnoreCase("UnnaturalDeathDetailsActivity")) {
                                postCommentData(ps_code, case_no, fir_yr, oc_comment, ac_comment, dc_comment);
                            } else {
                                postCommentData(ps_code, case_no, case_yr, oc_comment, ac_comment, dc_comment);
                            }


                        }
                    });


                    if (caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllAccusedList().size() > 0) {

                        int sl_no = 0;

                        int length = caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllAccusedList().size();

                        for (int i = 0; i < length; i++) {


                            String accused_detail = "";

                            LayoutInflater inflater = (LayoutInflater) this
                                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                            View v = inflater.inflate(R.layout.fir_other_accused_list_item, null);

                            final RelativeLayout relative_otherAccused = (RelativeLayout) v.findViewById(R.id.relative_otherAccused);
                            relative_otherAccused.setId(i);
                            final TextView tv_SlNo = (TextView) v.findViewById(R.id.tv_SlNo);
                            final TextView tv_accused_details = (TextView) v.findViewById(R.id.tv_accused_details);
                            final View divider_firItem = v.findViewById(R.id.divider_firItem);

                            Utility.changefonts(tv_accused_details, this, "Calibri Bold.ttf");
                            tv_accused_details.setTextColor(ContextCompat.getColor(this, R.color.color_light_blue));

                            if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllAccusedList().get(i).getName().toUpperCase().contains("UNKNOWN")) {

                                sl_no++;

                                accused_detail = caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllAccusedList().get(i).getName()
                                        + caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllAccusedList().get(i).getAlias();

                                if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllAccusedList().get(i).getFather().equalsIgnoreCase("null")) {
                                    accused_detail = accused_detail + ", S/o " + caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllAccusedList().get(i).getFather();
                                }

                                if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllAccusedList().get(i).getAddress().equalsIgnoreCase("null")) {
                                    accused_detail = accused_detail + ", " + caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllAccusedList().get(i).getAddress() + ".";
                                }

                                //tv_SlNo.setText("" + sl_no);
                                tv_SlNo.setVisibility(View.GONE);
                                tv_accused_details.setBackgroundResource(0);
                                tv_accused_details.setText(sl_no + ". " + accused_detail);

                                if ((length == sl_no)) {
                                    divider_firItem.setVisibility(View.GONE);
                                } else {
                                    divider_firItem.setVisibility(View.VISIBLE);
                                }

                                linear_otherAccused.addView(v);
                                isOtherAccuesedAdded = true;

                                final int clickPosition = i;

                                relative_otherAccused.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        L.e("PROV CRIMINAL NO Accussed:" + caseSearchFIRDetailsList.get(0).getAllAccusedDataList().get(clickPosition).getProvCrmNo());

                                        if (caseSearchFIRDetailsList.get(0).getAllAccusedDataList().get(clickPosition).getProvCrmNo().equalsIgnoreCase(null) || caseSearchFIRDetailsList.get(0).getAllAccusedDataList().get(clickPosition).getProvCrmNo().equalsIgnoreCase("null") || caseSearchFIRDetailsList.get(0).getAllAccusedDataList().get(clickPosition).getProvCrmNo().equalsIgnoreCase("")) {

                                            Utility.showAlertDialog(CaseSearchFIRDetailsActivity.this, Constants.ERROR, Constants.ERROR_PRV_CRIMINAL_NO, false);

                                        } else {

                                            Intent intent = new Intent(CaseSearchFIRDetailsActivity.this, CriminalDetailActivity.class);
                                            intent.putExtra(Constants.OBJECT, caseSearchFIRDetailsList.get(0).getAllAccusedDataList().get(clickPosition));
                                            intent.putExtra("searchCase", "criminalSearch");
                                            startActivity(intent);
                                        }
                                    }
                                });

                            }
                        }


                        if (!isOtherAccuesedAdded) {
                            TextView tv_noOtherAccussed = new TextView(this);
                            tv_noOtherAccussed.setText("  No data available");
                            tv_noOtherAccussed.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                            Utility.changefonts(tv_noOtherAccussed, this, "Calibri.ttf");

                            linear_otherAccused.addView(tv_noOtherAccussed);
                            linear_otherAccused.setGravity(Gravity.CENTER);
                        }

                    } else {
                        TextView tv_noOtherAccussed = new TextView(this);
                        tv_noOtherAccussed.setText("  No data available");
                        tv_noOtherAccussed.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                        Utility.changefonts(tv_noOtherAccussed, this, "Calibri.ttf");

                        linear_otherAccused.addView(tv_noOtherAccussed);
                        linear_otherAccused.setGravity(Gravity.CENTER);
                    }

                    ///   test

                    if (caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().size() > 0) {

                        int sl_no = 0;

                        int length = caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().size();
                        // int lengtharrestted =  caseSearchFIRDetailsList.get(0).getAllArrestedDataList().size();
                        //  Log.d("Length:",""+lengtharrestted);


                        for (int i = 0; i < length; i++) {

                            Log.d("value of i", "" + i);
                            String arrested_detail = "";

                            LayoutInflater inflater = (LayoutInflater) this
                                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                            View v = inflater.inflate(R.layout.fir_other_accused_list_item, null);

                            final RelativeLayout relative_otherArrested = (RelativeLayout) v.findViewById(R.id.relative_otherAccused);
                            relative_otherArrested.setId(i);
                            final TextView tv_SlNo = (TextView) v.findViewById(R.id.tv_SlNo);
                            final TextView tv_arrested_details = (TextView) v.findViewById(R.id.tv_accused_details);
                            final View divider_firItem = v.findViewById(R.id.divider_firItem);

                            Utility.changefonts(tv_arrested_details, this, "Calibri Bold.ttf");
                            tv_arrested_details.setTextColor(ContextCompat.getColor(this, R.color.color_light_blue));

                            if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().get(i).getName().equalsIgnoreCase("") && !caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().get(i).getName().equalsIgnoreCase("null")
                                    && !caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().get(i).getName().toUpperCase().contains("UNKNOWN")) {

                                sl_no++;

                                arrested_detail = caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().get(i).getName()
                                        + caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().get(i).getAlias();

                                if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().get(i).getFather().equalsIgnoreCase("null")) {
                                    arrested_detail = arrested_detail + ", " + caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().get(i).getFather();
                                }

                                if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().get(i).getAddress().equalsIgnoreCase("null")) {
                                    arrested_detail = arrested_detail + ", " + caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().get(i).getAddress();
                                }

                                if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().get(i).getArrested_by().equalsIgnoreCase("null")) {
                                    arrested_detail = arrested_detail + ".\nArrested by " + caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().get(i).getArrested_by();
                                }

                                if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().get(i).getArrest_date().equalsIgnoreCase("null")) {
                                    arrested_detail = arrested_detail + " on " + caseSearchFIRDetailsList.get(0).getCaseSearchFIRAllArrestedList().get(i).getArrest_date() + ".";
                                }

                                //tv_SlNo.setText("" + sl_no);
                                tv_SlNo.setVisibility(View.GONE);
                                tv_arrested_details.setBackgroundResource(0);
                                tv_arrested_details.setText(sl_no + ". " + arrested_detail);

                                if (length == sl_no) {
                                    divider_firItem.setVisibility(View.GONE);
                                } else {
                                    divider_firItem.setVisibility(View.VISIBLE);
                                }

                                linear_person_arrested.addView(v);
                                isOtherArrestAdded = true;

                                final int clickArrestPosition = i;
                                L.e("PROV_CRIMINAL_NO Arrested --->" + caseSearchFIRDetailsList.get(0).getAllArrestedDataList().get(clickArrestPosition).getProvCrmNo());


                                relative_otherArrested.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (caseSearchFIRDetailsList.get(0).getAllArrestedDataList().get(clickArrestPosition).getProvCrmNo().equalsIgnoreCase(null) || caseSearchFIRDetailsList.get(0).getAllArrestedDataList().get(clickArrestPosition).getProvCrmNo().equalsIgnoreCase("null") || caseSearchFIRDetailsList.get(0).getAllArrestedDataList().get(clickArrestPosition).getProvCrmNo().equalsIgnoreCase("")) {

                                            Utility.showAlertDialog(CaseSearchFIRDetailsActivity.this, Constants.ERROR, Constants.ERROR_PRV_CRIMINAL_NO, false);

                                        } else {
                                            Intent intent2 = new Intent(CaseSearchFIRDetailsActivity.this, CriminalDetailActivity.class);
                                            L.e("ArrayList----------->" + caseSearchFIRDetailsList.get(0).getAllArrestedDataList().get(0));
                                            Log.e("LOG", "Log " + caseSearchFIRDetailsList.get(0).getAllArrestedDataList().get(0));
                                            intent2.putExtra(Constants.OBJECT, caseSearchFIRDetailsList.get(0).getAllArrestedDataList().get(clickArrestPosition));
                                            intent2.putExtra("searchCase", "criminalSearch");
                                            startActivity(intent2);
                                        }

                                    }
                                });


                            }

                        }

                        if (!isOtherArrestAdded) {
                            TextView tv_noOtherAccussed = new TextView(this);
                            tv_noOtherAccussed.setText("  No data available");
                            tv_noOtherAccussed.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                            Utility.changefonts(tv_noOtherAccussed, this, "Calibri.ttf");

                            linear_person_arrested.addView(tv_noOtherAccussed);
                            linear_person_arrested.setGravity(Gravity.CENTER);
                        }

                    } else {
                        TextView tv_noOtherAccussed = new TextView(this);
                        tv_noOtherAccussed.setText("  No data available");
                        tv_noOtherAccussed.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                        Utility.changefonts(tv_noOtherAccussed, this, "Calibri.ttf");

                        linear_person_arrested.addView(tv_noOtherAccussed);
                        linear_person_arrested.setGravity(Gravity.CENTER);
                    }

                }
            }
        }
    }

    private void popUpWindowForCaseSearchFIRWarrant(View view) {

        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popUpWindow layout
        inflatedFIRview = layoutInflater.inflate(R.layout.warrant_popup, null, false);


        tv_warrant_itemName = (TextView) inflatedFIRview.findViewById(R.id.tv_itemName);
        tv_warrantNo = (TextView) inflatedFIRview.findViewById(R.id.tv_warrantNo);
        tv_issuingCourtValue = (TextView) inflatedFIRview.findViewById(R.id.tv_issuingCourtValue);
        tv_processNoValue = (TextView) inflatedFIRview.findViewById(R.id.tv_processNoValue);
        tv_typeValue = (TextView) inflatedFIRview.findViewById(R.id.tv_typeValue);
        tv_returnableDateValue = (TextView) inflatedFIRview.findViewById(R.id.tv_returnableDateValue);
        tv_IOValue = (TextView) inflatedFIRview.findViewById(R.id.tv_IOValue);
        tv_presentStatusValue = (TextView) inflatedFIRview.findViewById(R.id.tv_presentStatusValue);
        tv_remarksValue = (TextView) inflatedFIRview.findViewById(R.id.tv_remarksValue);
        linear_warrantees = (LinearLayout) inflatedFIRview.findViewById(R.id.linear_warrantees);

        TextView tv_watermarkW = (TextView) inflatedFIRview.findViewById(R.id.tv_watermark);

         /* Set user name as Watermark at Warrant popup  */
        tv_watermarkW.setVisibility(View.VISIBLE);
        tv_watermarkW.setText(userName);
        tv_watermarkW.setRotation(-50);

        tv_warrantNo.setVisibility(View.VISIBLE);

        Utility.changefonts(tv_warrant_itemName, this, "Calibri Bold.ttf");
        Utility.changefonts(tv_warrantNo, this, "Calibri Bold.ttf");

        String warrant_details = "";
        String processNo = "";
        String issuingCourt = "";
        String warrant_type = "";
        String returnable_date_to_court = "";
        String io = "";
        String presentStatus = "";
        String remarks = "";

        if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getSlNo().equalsIgnoreCase("null")) {
            warrant_details = "Warrant No. " + caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getSlNo();
        }

        if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getSlDate().equalsIgnoreCase("null")) {
            warrant_details = warrant_details + " dated " + caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getSlDate();
        }

        if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getProcessNo().equalsIgnoreCase("null")) {
            processNo = caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getProcessNo();
        }

        if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getIssueCourt().equalsIgnoreCase("null")) {
            issuingCourt = caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getIssueCourt();
        }

        if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getWaType().equalsIgnoreCase("null")) {
            warrant_type = caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getWaType();
        }

        if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getReturnableDateToCourt().equalsIgnoreCase("null")) {
            returnable_date_to_court = caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getReturnableDateToCourt();
        }

        if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getOfficerToServe().equalsIgnoreCase("null")) {
            io = caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getOfficerToServe();
        }

        if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getWaStatus().equalsIgnoreCase("null")) {
            presentStatus = caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getWaStatus();
        }

        if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getRemarks().equalsIgnoreCase("null")) {
            remarks = caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getRemarks();
        }


        tv_warrant_itemName.setText(header_value);
        tv_warrantNo.setText(warrant_details);
        tv_processNoValue.setText(processNo);
        tv_issuingCourtValue.setText(issuingCourt);
        tv_typeValue.setText(warrant_type);
        tv_returnableDateValue.setText(returnable_date_to_court);
        tv_IOValue.setText(io);
        tv_presentStatusValue.setText(presentStatus);
        tv_remarksValue.setText(remarks);


        if (caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getFirWarranteesDetailsList().size() > 0) {

            int warranteesListSize = caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getFirWarranteesDetailsList().size();

            for (int i = 0; i < warranteesListSize; i++) {

                LayoutInflater inflater = (LayoutInflater) this
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View v = inflater.inflate(R.layout.fir_warrant_list_item, null);

                RelativeLayout relative_warrantDetails = (RelativeLayout) v.findViewById(R.id.relative_warrantDetails);
                relative_warrantDetails.setId(i);
                TextView tv_SlNo = (TextView) v.findViewById(R.id.tv_SlNo);
                TextView tv_warranteesName = (TextView) v.findViewById(R.id.tv_warranteesName);
                TextView tv_warranteesAddress = (TextView) v.findViewById(R.id.tv_warranteesAddress);

                tv_SlNo.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getFirWarranteesDetailsList().get(i).getSl_no());
                tv_warranteesName.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getFirWarranteesDetailsList().get(i).getWarrantee_name());
                tv_warranteesAddress.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRWarrantList().get(0).getFirWarranteesDetailsList().get(i).getWarrantee_address());


                linear_warrantees.addView(v);
            }

        } else {

            TextView tv_warrantees = new TextView(this);
            tv_warrantees.setText("  No data available");
            tv_warrantees.setTextColor(ContextCompat.getColor(this, R.color.color_black));
            Utility.changefonts(tv_warrantees, this, "Calibri.ttf");

            linear_warrantees.addView(tv_warrantees);
            linear_warrantees.setGravity(Gravity.CENTER);
        }


        openWarrantPopupWindow();

    }


    private void openWarrantPopupWindow() {

        // get device size
        Display display = this.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // set height depends on the device size
        popupWindowForWarrant = new PopupWindow(inflatedFIRview, width, height - 50, true);

        popupWindowForWarrant.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popupWindowForWarrant.setHeight(WindowManager.LayoutParams.MATCH_PARENT);

        popupWindowForWarrant.setAnimationStyle(R.style.PopupAnimation);

        //to generate popUpWindow at bottom of ActionBar
        //popWindow.showAsDropDown(getActivity().getActionBar().getCustomView(), 0, 20);
        popupWindowForWarrant.showAtLocation(this.getActionBar().getCustomView(), Gravity.CENTER, 0, 20);

        popupWindowForWarrant.setOutsideTouchable(true);
        popupWindowForWarrant.setFocusable(true);
        popupWindowForWarrant.getContentView().setFocusableInTouchMode(true);
        popupWindowForWarrant.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    if (popupWindowForWarrant != null && popupWindowForWarrant.isShowing()) {
                        popupWindowForWarrant.dismiss();
                    }

                    return true;
                }
                return false;
            }
        });

    }

    private void popUpWindowForCaseSearchChargeSheetDetails(int position, String itemName) {

        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popUpWindow layout
        inflatedFIRviewChargeSheet = layoutInflater.inflate(R.layout.chargesheet_popup, null, false);

        TextView tv_itemName = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_itemName);
        TextView tv_chargesheetNo = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_chargesheetNo);
        TextView tv_courtValue = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_courtValue);
        TextView tv_actsValue = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_actsValue);
        TextView tv_typeValue = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_typeValue);
        TextView tv_supplementaryValue = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_supplementaryValue);
        TextView tv_briefFactValue = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_briefFactValue);

        linear_witness = (LinearLayout) inflatedFIRviewChargeSheet.findViewById(R.id.linear_witness);
        linear_accusedChargesheeted = (LinearLayout) inflatedFIRviewChargeSheet.findViewById(R.id.linear_accusedChargesheeted);
        linear_accusedNotChargesheeted = (LinearLayout) inflatedFIRviewChargeSheet.findViewById(R.id.linear_accusedNotChargesheeted);
        linear_propertySeizure = (LinearLayout) inflatedFIRviewChargeSheet.findViewById(R.id.linear_propertySeizure);

        TextView tv_watermarkCS = (TextView) inflatedFIRviewChargeSheet.findViewById(R.id.tv_watermark);

          /* Set user name as Watermark at ChargeSheet popup */
        tv_watermarkCS.setVisibility(View.VISIBLE);
        tv_watermarkCS.setText(userName);
        tv_watermarkCS.setRotation(-50);

        Constants.changefonts(tv_itemName, this, "Calibri Bold.ttf");

        Utility.changefonts(tv_chargesheetNo, this, "Calibri Bold.ttf");

        tv_itemName.setText(header_value);

        String chargeSheetDetails = "";
        if (!caseSearchFIRDetailsList.get(position).getCaseSearchFIRChargeSheetList().get(0).getFrChgNo().equalsIgnoreCase("")) {
            chargeSheetDetails = "Chargesheet No. " + caseSearchFIRDetailsList.get(position).getCaseSearchFIRChargeSheetList().get(0).getFrChgNo();
        }

        if (!caseSearchFIRDetailsList.get(position).getCaseSearchFIRChargeSheetList().get(0).getFrChgDate().equalsIgnoreCase("")) {
            chargeSheetDetails = chargeSheetDetails + " dated " + caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getFrChgDate();
        }

        if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCourtOf().equalsIgnoreCase("")) {
            tv_courtValue.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCourtOf());
        }

        if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getFrType().equalsIgnoreCase("")) {
            tv_typeValue.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getFrType());
        }

        if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getSupplementaryOriginal().equalsIgnoreCase("")) {
            tv_supplementaryValue.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getSupplementaryOriginal());
        }

        if (!caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getBriefFact().equalsIgnoreCase("")) {
            tv_briefFactValue.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getBriefFact());
        } else {
            tv_briefFactValue.setText(" No data available");
        }

        tv_chargesheetNo.setText(chargeSheetDetails);
        tv_actsValue.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getActs());

        /*
        * this part is used for witness details  */

        if (caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchWitnessDetailsList().size() > 0) {

            int witnessListSize = caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchWitnessDetailsList().size();

            for (int i = 0; i < witnessListSize; i++) {

                LayoutInflater inflater = (LayoutInflater) this
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View v = inflater.inflate(R.layout.fir_warrant_list_item, null);

                RelativeLayout relative_warrantDetails = (RelativeLayout) v.findViewById(R.id.relative_warrantDetails);
                relative_warrantDetails.setId(i);
                TextView tv_SlNo = (TextView) v.findViewById(R.id.tv_SlNo);
                TextView tv_witnessName = (TextView) v.findViewById(R.id.tv_warranteesName);
                TextView tv_witnessAddress = (TextView) v.findViewById(R.id.tv_warranteesAddress);

                String sl_no = Integer.toString(i + 1);
                tv_SlNo.setText(sl_no);
                tv_witnessName.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchWitnessDetailsList().get(i).getNameWitness());
                tv_witnessAddress.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchWitnessDetailsList().get(i).getAddressWitness());

                linear_witness.addView(v);
            }

        } else {

            TextView tv_witness = new TextView(this);
            tv_witness.setText("  No data available");
            tv_witness.setTextColor(ContextCompat.getColor(this, R.color.color_black));
            Utility.changefonts(tv_witness, this, "Calibri.ttf");

            linear_witness.addView(tv_witness);
            linear_witness.setGravity(Gravity.CENTER);
        }

        /*
        * this part is used for chargesheet accussed details  */

        if (caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchAccusedDetailsList().size() > 0) {

            int accusedListSize = caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchAccusedDetailsList().size();

            for (int i = 0; i < accusedListSize; i++) {

                LayoutInflater inflater = (LayoutInflater) this
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View v = inflater.inflate(R.layout.fir_warrant_list_item, null);

                RelativeLayout relative_warrantDetails = (RelativeLayout) v.findViewById(R.id.relative_warrantDetails);
                relative_warrantDetails.setId(i);
                TextView tv_SlNo = (TextView) v.findViewById(R.id.tv_SlNo);
                TextView tv_witnessName = (TextView) v.findViewById(R.id.tv_warranteesName);
                TextView tv_witnessAddress = (TextView) v.findViewById(R.id.tv_warranteesAddress);

                String sl_no = Integer.toString(i + 1);
                tv_SlNo.setText(sl_no);
                tv_witnessName.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchAccusedDetailsList().get(i).getNameAccused());
                tv_witnessAddress.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchAccusedDetailsList().get(i).getAddressAccused());

                linear_accusedChargesheeted.addView(v);
            }

        } else {

            TextView tv_accusedChargeSheet = new TextView(this);
            tv_accusedChargeSheet.setText("  No data available");
            tv_accusedChargeSheet.setTextColor(ContextCompat.getColor(this, R.color.color_black));
            Utility.changefonts(tv_accusedChargeSheet, this, "Calibri.ttf");

            linear_accusedChargesheeted.addView(tv_accusedChargeSheet);
            linear_accusedChargesheeted.setGravity(Gravity.CENTER);
        }


        /*
        * this part is used for chargesheet not accussed details  */

        if (caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchNotAccusedDetailsList().size() > 0) {

            int notAccussedListSize = caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchNotAccusedDetailsList().size();

            for (int i = 0; i < notAccussedListSize; i++) {

                LayoutInflater inflater = (LayoutInflater) this
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View v = inflater.inflate(R.layout.fir_warrant_list_item, null);

                RelativeLayout relative_warrantDetails = (RelativeLayout) v.findViewById(R.id.relative_warrantDetails);
                relative_warrantDetails.setId(i);
                TextView tv_SlNo = (TextView) v.findViewById(R.id.tv_SlNo);
                TextView tv_witnessName = (TextView) v.findViewById(R.id.tv_warranteesName);
                TextView tv_witnessAddress = (TextView) v.findViewById(R.id.tv_warranteesAddress);

                String sl_no = Integer.toString(i + 1);
                tv_SlNo.setText(sl_no);
                tv_witnessName.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchNotAccusedDetailsList().get(i).getNameAccused());
                tv_witnessAddress.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchNotAccusedDetailsList().get(i).getAddressAccused());

                linear_accusedNotChargesheeted.addView(v);
            }

        } else {

            TextView tv_accusedNotChargeSheet = new TextView(this);
            tv_accusedNotChargeSheet.setText("  No data available");
            tv_accusedNotChargeSheet.setTextColor(ContextCompat.getColor(this, R.color.color_black));
            Utility.changefonts(tv_accusedNotChargeSheet, this, "Calibri.ttf");

            linear_accusedNotChargesheeted.addView(tv_accusedNotChargeSheet);
            linear_accusedNotChargesheeted.setGravity(Gravity.CENTER);
        }

         /*
        * this part is used for property details  */

        if (caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchPropertyDetailsList().size() > 0) {

            int proprtyListSize = caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchPropertyDetailsList().size();

            for (int i = 0; i < proprtyListSize; i++) {

                LayoutInflater inflater = (LayoutInflater) this
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View v = inflater.inflate(R.layout.fir_warrant_list_item, null);

                RelativeLayout relative_warrantDetails = (RelativeLayout) v.findViewById(R.id.relative_warrantDetails);
                relative_warrantDetails.setId(i);
                TextView tv_SlNo = (TextView) v.findViewById(R.id.tv_SlNo);
                TextView tv_witnessName = (TextView) v.findViewById(R.id.tv_warranteesName);
                TextView tv_witnessAddress = (TextView) v.findViewById(R.id.tv_warranteesAddress);

                String sl_no = Integer.toString(i + 1);
                tv_SlNo.setText(sl_no);
                tv_witnessName.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchPropertyDetailsList().get(i).getPropertyDesc());
                tv_witnessAddress.setText(caseSearchFIRDetailsList.get(0).getCaseSearchFIRChargeSheetList().get(0).getCaseSearchPropertyDetailsList().get(i).getPsPropRegNo());

                linear_propertySeizure.addView(v);
            }

        } else {

            TextView tv_proprtyDetails = new TextView(this);
            tv_proprtyDetails.setText("  No data available");
            tv_proprtyDetails.setTextColor(ContextCompat.getColor(this, R.color.color_black));
            Utility.changefonts(tv_proprtyDetails, this, "Calibri.ttf");

            linear_propertySeizure.addView(tv_proprtyDetails);
            linear_propertySeizure.setGravity(Gravity.CENTER);
        }


        openPopupWindowForChargeSheet();

    }


    private void openPopupWindowForChargeSheet() {

        // get device size
        Display display = this.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // set height depends on the device size
        popWindowForChargeSheet = new PopupWindow(inflatedFIRviewChargeSheet, width, height - 50, true);

        popWindowForChargeSheet.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindowForChargeSheet.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindowForChargeSheet.setAnimationStyle(R.style.PopupAnimation);

        //to generate popUpWindow at bottom of ActionBar
        // popWindowForChargeSheet.showAsDropDown(getActivity().getActionBar().getCustomView(), 0, 20);
        popWindowForChargeSheet.showAtLocation(this.getActionBar().getCustomView(), Gravity.CENTER, 0, 20);

        popWindowForChargeSheet.setOutsideTouchable(true);
        popWindowForChargeSheet.setFocusable(true);
        popWindowForChargeSheet.getContentView().setFocusableInTouchMode(true);
        popWindowForChargeSheet.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    if (popWindowForChargeSheet != null && popWindowForChargeSheet.isShowing()) {
                        popWindowForChargeSheet.dismiss();

                    }

                    return true;
                }
                return false;
            }
        });

    }


    private void openPopupWindowForCaseTransLog() {

        rl_FIR_Details.setAlpha(0.2f);

        // get device size
        Display display = this.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // set height depends on the device size
        popWindowForCaseTransLog = new PopupWindow(inflatedViewCaseTransfer, width, height - 50, true);

        popWindowForCaseTransLog.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindowForCaseTransLog.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindowForCaseTransLog.setAnimationStyle(R.style.PopupAnimation);

        //to generate popUpWindow at bottom of ActionBar
        popWindowForCaseTransLog.showAtLocation(this.getActionBar().getCustomView(), Gravity.CENTER, 0, 20);

        popWindowForCaseTransLog.setOutsideTouchable(true);
        popWindowForCaseTransLog.setFocusable(true);
        popWindowForCaseTransLog.getContentView().setFocusableInTouchMode(true);
        popWindowForCaseTransLog.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    if (popWindowForCaseTransLog != null && popWindowForCaseTransLog.isShowing()) {
                        popWindowForCaseTransLog.dismiss();

                    }
                    return true;
                }
                return false;
            }
        });

        popWindowForCaseTransLog.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //Do Something here
                rl_FIR_Details.setAlpha(1.0f);
            }
        });

    }


    private void openPopupWindowForCaseDownloadLog() {

        rl_FIR_Details.setAlpha(0.2f);

        // get device size
        Display display = this.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // set height depends on the device size
        popWindowForCaseDownloadLog = new PopupWindow(inflatedViewDownloadLog, width, height - 50, true);

        popWindowForCaseDownloadLog.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindowForCaseDownloadLog.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindowForCaseDownloadLog.setAnimationStyle(R.style.PopupAnimation);

        //to generate popUpWindow at bottom of ActionBar
        popWindowForCaseDownloadLog.showAtLocation(this.getActionBar().getCustomView(), Gravity.CENTER, 0, 20);

        popWindowForCaseDownloadLog.setOutsideTouchable(true);
        popWindowForCaseDownloadLog.setFocusable(true);
        popWindowForCaseDownloadLog.getContentView().setFocusableInTouchMode(true);
        popWindowForCaseDownloadLog.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    if (popWindowForCaseDownloadLog != null && popWindowForCaseDownloadLog.isShowing()) {
                        popWindowForCaseDownloadLog.dismiss();
                    }

                    return true;
                }
                return false;
            }
        });

        popWindowForCaseDownloadLog.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //Do Something here
                rl_FIR_Details.setAlpha(1.0f);
            }
        });

    }


    private void showAlertDialogForFinish() {
        final AlertDialog ad = new AlertDialog.Builder(context).create();
        ad.setTitle(Constants.ERROR_DETAILS_TITLE);
        ad.setMessage(Constants.ERROR_MSG_DETAIL);
        ad.setIcon(R.drawable.fail);
        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            //ad.dismiss();
                            finish();
                        }

                    }
                });
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                            finish();

                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    /*
    * This method is used
    * to post comment data
    * to server
    * */

    private void postCommentData(String ps_code, String case_no, String case_year, String oc_comment, String ac_comment, String dc_comment) {

        String modified_ocComment = oc_comment.substring(oc_comment.indexOf(":-") + 2).trim();
        String modified_acComment = ac_comment.substring(ac_comment.indexOf(":-") + 2).trim();
        String modified_dcComment = dc_comment.substring(dc_comment.indexOf(":-") + 2).trim();

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_POST_COMMENT);
        taskManager.setCaseSearchEditComment(true);
        String[] keys = {"ps", "caseno", "caseyr", "oc_comment", "ac_comment", "dc_comment"};
        //String[] values = {"HDV","288","2015","test","test","test"};
        String[] values = {ps_code.trim(), case_no.trim(), case_year.trim(), modified_ocComment.trim(), modified_acComment.trim(), modified_dcComment.trim()};
        taskManager.doStartTask(keys, values, true);

    }

    public void parseCaseSearchEditCommentDataResponse(String response) {

        //System.out.println("EditCommentDataResponse: " + response);

        if (response != null && !response.equals("")) {
            try {
                JSONObject jObj = new JSONObject(response);

                Utility.showToast(this, jObj.optString("message"), "long");

                if (jObj.optString("status").equalsIgnoreCase("1")) {

                    JSONObject result_obj = jObj.getJSONObject("result");
                    //parseEditCommentResponse(result_obj);
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    private void downloadPDFData(String pdf_url) {

        if (Constants.internetOnline(CaseSearchFIRDetailsActivity.this)) {
            AsynctaskForDownloadPDF asynctaskForDownloadPDF = new AsynctaskForDownloadPDF(CaseSearchFIRDetailsActivity.this, pdf_url, isCase_PDF);
            asynctaskForDownloadPDF.execute();
            asynctaskForDownloadPDF.setDelegate(CaseSearchFIRDetailsActivity.this);
        } else {

            showAlertDialog(CaseSearchFIRDetailsActivity.this, "Connectivity Problem", "Sorry, internet connection not found", false);

        }

    }

    @Override
    public void onPDFDownloadSuccess(String filePath, boolean fromWhich) {

        if (fromWhich) {
            existing_pdf_path = filePath;
            Log.e("OnPDFDownloadSuccess_cs", filePath);
            tv_pdf.setText("FIR: View");
            showPDF(filePath);
        } else {
            pmreport_existing_pdf_path = filePath;
            Log.e("OnPDFDownloadSuccess_PM", filePath);
            tv_pmReportPdf.setText("PM Report: View");
            showPDF(filePath);
        }
    }

    @Override
    public void onPDFDownloadError(String error) {

        Log.e("OnPDFDownloadError", error);

        CaseSearchFIRDetailsActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                Utility.showToast(CaseSearchFIRDetailsActivity.this, Constants.ERROR_EXCEPTION_MSG, "long");
            }
        });


    }

    private void showPDF(String filePath) {

        File file = new File(filePath);
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file), "application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something

            showAlertDialog(CaseSearchFIRDetailsActivity.this, "PDF Reader Problem", "Your device doesn't have PDF reader. Download a PDF reader app from play store", false);

        }

    }


    /* Fetch current location */
    public Location getCurrentLocation() {

        try {
            locationManager = (LocationManager) this
                    .getSystemService(LOCATION_SERVICE);


            // Getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // Getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // No network provider is enabled
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            0, 0, this);
                    Log.d("Network", "Network");

                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }

                // If GPS enabled, get latitude/longitude using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }

            latitudeVal = latitude + "";
            longitudeVal = longitude + "";
            Log.e("Lat  -  Long: ", latitudeVal + " - " + longitudeVal);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    private void getDownloadLogForFIR() {

        Log.e("Log Dwnload-- ", "Called Case");

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CASE_FIRLOGLIST);
        taskManager.setCaseFIRLogList(true);

        String keys[] = new String[]{"ps", "case_no", "case_yr"};
        String values[] = new String[]{ps_code.trim(), case_no.trim(), case_yr.trim()};

        taskManager.doStartTask(keys, values, true);
    }


    public void parseCaseSearchFIRLogListResponse(String response, String[] keys, String[] values) {

        //System.out.println("parseCaseSearchFIRLogListResponse -" + response);
        log_response = response;

        if (response != null && !response.equals("")) {

            try {
                JSONObject jObj = new JSONObject(response);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    JSONArray resultAry = jObj.getJSONArray("result");

                    parseCaseFIRDownloadLog(resultAry);

                } else {

                    Utility.showAlertDialog(CaseSearchFIRDetailsActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_DOWNLOAD_LOG, false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(CaseSearchFIRDetailsActivity.this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
            }

        }

    }


    private void parseCaseFIRDownloadLog(JSONArray resultAry) {

        firDownloadLogList.clear();
        for (int i = 0; i < resultAry.length(); i++) {

            //int slNo = 0;
            length = resultAry.length();

            FirLogListDetails firLogListDetails = new FirLogListDetails();
            try {
                JSONObject jObj = resultAry.getJSONObject(i);

                if (jObj.optString("CASENO") != null && !jObj.optString("CASENO").equalsIgnoreCase("") && !jObj.optString("CASENO").equalsIgnoreCase("null")) {
                    firLogListDetails.setCaseNo(jObj.optString("CASENO"));
                }

                if (jObj.optString("CASE_YR") != null && !jObj.optString("CASE_YR").equalsIgnoreCase("") && !jObj.optString("CASE_YR").equalsIgnoreCase("null")) {
                    firLogListDetails.setCaseYr(jObj.optString("CASE_YR"));
                }

                if (jObj.optString("EMAIL") != null && !jObj.optString("EMAIL").equalsIgnoreCase("") && !jObj.optString("EMAIL").equalsIgnoreCase("null")) {
                    firLogListDetails.setEmailId(jObj.optString("EMAIL"));
                }

                if (jObj.optString("LINK") != null && !jObj.optString("LINK").equalsIgnoreCase("") && !jObj.optString("LINK").equalsIgnoreCase("null")) {
                    firLogListDetails.setLink(jObj.optString("LINK"));
                }

                if (jObj.optString("MOBILE") != null && !jObj.optString("MOBILE").equalsIgnoreCase("") && !jObj.optString("MOBILE").equalsIgnoreCase("null")) {
                    firLogListDetails.setMobileNo(jObj.optString("MOBILE"));
                }

                if (jObj.optString("OTP") != null && !jObj.optString("OTP").equalsIgnoreCase("") && !jObj.optString("OTP").equalsIgnoreCase("null")) {
                    firLogListDetails.setOtp(jObj.optString("OTP"));
                }

                if (jObj.optString("PS") != null && !jObj.optString("PS").equalsIgnoreCase("") && !jObj.optString("PS").equalsIgnoreCase("null")) {
                    firLogListDetails.setPsCode(jObj.optString("PS"));
                }

                if (jObj.optString("REASON") != null && !jObj.optString("REASON").equalsIgnoreCase("") && !jObj.optString("REASON").equalsIgnoreCase("null")) {
                    firLogListDetails.setReason(jObj.optString("REASON"));
                }

                if (jObj.optString("RECORD_DATE") != null && !jObj.optString("RECORD_DATE").equalsIgnoreCase("") && !jObj.optString("RECORD_DATE").equalsIgnoreCase("null")) {
                    firLogListDetails.setRecordDate(jObj.optString("RECORD_DATE"));
                }

                if (jObj.optString("RECORD_TIME") != null && !jObj.optString("RECORD_TIME").equalsIgnoreCase("") && !jObj.optString("RECORD_TIME").equalsIgnoreCase("null")) {
                    firLogListDetails.setRecordTime(jObj.optString("RECORD_TIME"));
                }

                if (jObj.optString("RECORD_ID") != null && !jObj.optString("RECORD_ID").equalsIgnoreCase("") && !jObj.optString("RECORD_ID").equalsIgnoreCase("null")) {
                    firLogListDetails.setRecordId(jObj.optString("RECORD_ID"));
                }

                if (jObj.optString("RELATIONSHIP") != null && !jObj.optString("RELATIONSHIP").equalsIgnoreCase("") && !jObj.optString("RELATIONSHIP").equalsIgnoreCase("null")) {
                    firLogListDetails.setRelationShip(jObj.optString("RELATIONSHIP"));
                }

                if (jObj.optString("USERNAME") != null && !jObj.optString("USERNAME").equalsIgnoreCase("") && !jObj.optString("USERNAME").equalsIgnoreCase("null")) {
                    firLogListDetails.setUserName(jObj.optString("USERNAME"));
                }

                firDownloadLogList.add(firLogListDetails);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ShowContentFirLog(firDownloadLogList, length);
    }


    private void ShowContentFirLog(ArrayList<FirLogListDetails> firLogList, int length) {

        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflatedViewDownloadLog = layoutInflater.inflate(R.layout.log_layout, null, false);

        linear_downloadLog = (LinearLayout) inflatedViewDownloadLog.findViewById(R.id.linear_caseTranfer);
        final TextView tv_downloadLog = (TextView) inflatedViewDownloadLog.findViewById(R.id.tv_showCaseTransfer);
        tv_downloadLog.setText("FIR Download Log");


        int slNo = 0;
        if (firLogList.size() > 0) {

            for (int i = 0; i < length; i++) {

                String name = "";
                String email = "";
                String mobile = "";
                String downloadDt_tm = "";
                String relationship = "";
                String reason = "";


                LayoutInflater inflater = (LayoutInflater) this
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View v = inflater.inflate(R.layout.fir_case_transfer_list_item, null);

                final TextView tv_SlNo = (TextView) v.findViewById(R.id.tv_SlNo);
                final LinearLayout linear_fir_log_details = (LinearLayout) v.findViewById(R.id.linear_case_transfer_details);

                List<String> caseFirLogItemDetailsList = new ArrayList<>();

                if (firLogList.get(i).getUserName() != null && !firLogList.get(i).getUserName().equalsIgnoreCase("") && !firLogList.get(i).getUserName().equalsIgnoreCase("null")) {
                    name = "Name :" + firLogList.get(i).getUserName();
                    caseFirLogItemDetailsList.add(name);
                }

                if (firLogList.get(i).getEmailId() != null && !firLogList.get(i).getEmailId().equalsIgnoreCase("") && !firLogList.get(i).getEmailId().equalsIgnoreCase("null")) {
                    email = "Email :" + firLogList.get(i).getEmailId();
                    caseFirLogItemDetailsList.add(email);
                }

                if (firLogList.get(i).getMobileNo() != null && !firLogList.get(i).getMobileNo().equalsIgnoreCase("") && !firLogList.get(i).getMobileNo().equalsIgnoreCase("null")) {
                    mobile = "Mobile :" + firLogList.get(i).getMobileNo();
                    caseFirLogItemDetailsList.add(mobile);
                }

                if (firLogList.get(i).getRecordDate() != null && !firLogList.get(i).getRecordDate().equalsIgnoreCase("") && !firLogList.get(i).getRecordDate().equalsIgnoreCase("null")
                        && firLogList.get(i).getRecordTime() != null && !firLogList.get(i).getRecordTime().equalsIgnoreCase("") && !firLogList.get(i).getRecordTime().equalsIgnoreCase("")) {
                    downloadDt_tm = "Download Dt & Tm :" + firLogList.get(i).getRecordDate() + " " + firLogList.get(i).getRecordTime();
                    caseFirLogItemDetailsList.add(downloadDt_tm);
                }

                if (firLogList.get(i).getRelationShip() != null && !firLogList.get(i).getRelationShip().equalsIgnoreCase("") && !firLogList.get(i).getRelationShip().equalsIgnoreCase("null")) {
                    relationship = "Relationship :" + firLogList.get(i).getRelationShip();
                    caseFirLogItemDetailsList.add(relationship);
                }

                if (firLogList.get(i).getReason() != null && !firLogList.get(i).getReason().equalsIgnoreCase("") && !firLogList.get(i).getReason().equalsIgnoreCase("null")) {
                    reason = "Reason :" + firLogList.get(i).getReason();
                    caseFirLogItemDetailsList.add(reason);
                }

                slNo++;
                tv_SlNo.setTextColor(ContextCompat.getColor(CaseSearchFIRDetailsActivity.this, R.color.color_light_blue));
                tv_SlNo.setText("" + slNo);

                for (int j = 0; j < caseFirLogItemDetailsList.size(); j++) {

                    View itemDetailsView = inflater.inflate(R.layout.fir_case_transfer_list_item_details, null);

                    TextView tv_key = (TextView) itemDetailsView.findViewById(R.id.tv_key);
                    TextView tv_value = (TextView) itemDetailsView.findViewById(R.id.tv_value);
                    RelativeLayout relative_fir_log_item_details = (RelativeLayout) itemDetailsView.findViewById(R.id.relative_case_transfer_item_details);

                    String data = caseFirLogItemDetailsList.get(j);
                    String key = data.substring(0, data.indexOf(":"));
                    String value = data.substring(data.indexOf(":") + 1);

                    tv_key.setTextColor(ContextCompat.getColor(CaseSearchFIRDetailsActivity.this, R.color.color_light_blue));
                    tv_value.setTextColor(ContextCompat.getColor(CaseSearchFIRDetailsActivity.this, R.color.color_light_blue));

                    tv_key.setText(key);
                    tv_value.setText(value);

                    linear_fir_log_details.addView(relative_fir_log_item_details, j);
                }
                linear_downloadLog.addView(v);
            }
        }
        openPopupWindowForCaseDownloadLog();
    }


    @Override
    public void imageUpload(int position, String url) {

        allAccusedList.get(position).getPicture_list().add(url);
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.tv_io_phone:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+caseSearchFIRDetailsList.get(0).getIoPhone()));
                startActivity(intent);
                break;

            case R.id.tv_oc_phone:
                Intent ointent = new Intent(Intent.ACTION_DIAL);
                ointent.setData(Uri.parse("tel:" + caseSearchFIRDetailsList.get(0).getOcPhone()));
                startActivity(ointent);
                break;

            case R.id.tv_ac_phone:
                Intent aintent = new Intent(Intent.ACTION_DIAL);
                aintent.setData(Uri.parse("tel:" + caseSearchFIRDetailsList.get(0).getAcPhone()));
                startActivity(aintent);
                break;

            case R.id.tv_dc_phone:
                Intent dintent = new Intent(Intent.ACTION_DIAL);
                dintent.setData(Uri.parse("tel:" + caseSearchFIRDetailsList.get(0).getDcPhone()));
                startActivity(dintent);
                break;
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
