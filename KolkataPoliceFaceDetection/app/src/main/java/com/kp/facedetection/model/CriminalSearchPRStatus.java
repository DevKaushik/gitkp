package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 09-09-2016.
 */
public class CriminalSearchPRStatus implements Serializable {

    private String ps="";
    private String caseNo="";
    private String caseYr="";
    private String slNo="";
    private String event_SlNo="";
    private String evenDetails="";
    private String dateRecorded="";
    private String prsStatus="";
    private String dateTill="";

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getCaseYr() {
        return caseYr;
    }

    public void setCaseYr(String caseYr) {
        this.caseYr = caseYr;
    }

    public String getSlNo() {
        return slNo;
    }

    public void setSlNo(String slNo) {
        this.slNo = slNo;
    }

    public String getEvent_SlNo() {
        return event_SlNo;
    }

    public void setEvent_SlNo(String event_SlNo) {
        this.event_SlNo = event_SlNo;
    }

    public String getEvenDetails() {
        return evenDetails;
    }

    public void setEvenDetails(String evenDetails) {
        this.evenDetails = evenDetails;
    }

    public String getDateRecorded() {
        return dateRecorded;
    }

    public void setDateRecorded(String dateRecorded) {
        this.dateRecorded = dateRecorded;
    }

    public String getPrsStatus() {
        return prsStatus;
    }

    public void setPrsStatus(String prsStatus) {
        this.prsStatus = prsStatus;
    }

    public String getDateTill() {
        return dateTill;
    }

    public void setDateTill(String dateTill) {
        this.dateTill = dateTill;
    }
}
