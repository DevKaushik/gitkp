package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-Asset-131 on 04-07-2016.
 */
public class DescriptiveRole implements Serializable {

    private String beard;
    private String birth_mark;
    private String built;
    private String burn_mark;
    private String complexion;
    private String cut_mark;
    private String deformity;
    private String ear;
    private String eye;
    private String eye_brow;
    private String face;
    private String forehead;
    private String hair;
    private String mole;
    private String moustache;
    private String nose;
    private String scar_mark;
    private String tattoo_mark;
    private String wart_mark;

    public String getBeard() {
        return beard;
    }

    public void setBeard(String beard) {
        this.beard = beard;
    }

    public String getBirth_mark() {
        return birth_mark;
    }

    public void setBirth_mark(String birth_mark) {
        this.birth_mark = birth_mark;
    }

    public String getBuilt() {
        return built;
    }

    public void setBuilt(String built) {
        this.built = built;
    }

    public String getBurn_mark() {
        return burn_mark;
    }

    public void setBurn_mark(String burn_mark) {
        this.burn_mark = burn_mark;
    }

    public String getComplexion() {
        return complexion;
    }

    public void setComplexion(String complexion) {
        this.complexion = complexion;
    }

    public String getCut_mark() {
        return cut_mark;
    }

    public void setCut_mark(String cut_mark) {
        this.cut_mark = cut_mark;
    }

    public String getDeformity() {
        return deformity;
    }

    public void setDeformity(String deformity) {
        this.deformity = deformity;
    }

    public String getEar() {
        return ear;
    }

    public void setEar(String ear) {
        this.ear = ear;
    }

    public String getEye() {
        return eye;
    }

    public void setEye(String eye) {
        this.eye = eye;
    }

    public String getEye_brow() {
        return eye_brow;
    }

    public void setEye_brow(String eye_brow) {
        this.eye_brow = eye_brow;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public String getForehead() {
        return forehead;
    }

    public void setForehead(String forehead) {
        this.forehead = forehead;
    }

    public String getHair() {
        return hair;
    }

    public void setHair(String hair) {
        this.hair = hair;
    }

    public String getMole() {
        return mole;
    }

    public void setMole(String mole) {
        this.mole = mole;
    }

    public String getMoustache() {
        return moustache;
    }

    public void setMoustache(String moustache) {
        this.moustache = moustache;
    }

    public String getNose() {
        return nose;
    }

    public void setNose(String nose) {
        this.nose = nose;
    }

    public String getScar_mark() {
        return scar_mark;
    }

    public void setScar_mark(String scar_mark) {
        this.scar_mark = scar_mark;
    }

    public String getTattoo_mark() {
        return tattoo_mark;
    }

    public void setTattoo_mark(String tattoo_mark) {
        this.tattoo_mark = tattoo_mark;
    }

    public String getWart_mark() {
        return wart_mark;
    }

    public void setWart_mark(String wart_mark) {
        this.wart_mark = wart_mark;
    }
}
