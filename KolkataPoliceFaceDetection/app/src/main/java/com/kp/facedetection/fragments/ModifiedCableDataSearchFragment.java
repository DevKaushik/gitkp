package com.kp.facedetection.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.kp.facedetection.CableSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.model.CableSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 06-12-2016.
 */
public class ModifiedCableDataSearchFragment extends Fragment {

    private RelativeLayout relative_cable_main;
    private TextView tv_notAuthorized;

    private EditText et_pincode,et_holderName, et_phoneNo, et_address, et_stbNo, et_vcNo;
    private Spinner sp_zoneValue;
    private Button btn_cableSearch, btn_reset;

    ArrayAdapter<CharSequence> document_CableZoneAdapter;

    private String pageno="";
    private String totalResult="";
    private String[] keys;
    private String[] values;
    String userId="";
    String devicetoken="";
    String auth_key="";


    private List<CableSearchDetails> cableSearchDeatilsList;

    public static ModifiedCableDataSearchFragment newInstance() {

        ModifiedCableDataSearchFragment modifiedKMCDataSearchFragment= new ModifiedCableDataSearchFragment();
        return modifiedKMCDataSearchFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.modified_fragment_cable_search,container,false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        userId= Utility.getUserInfo(getContext()).getUserId();
        relative_cable_main = (RelativeLayout) view.findViewById(R.id.relative_cable_main);
        tv_notAuthorized = (TextView) view.findViewById(R.id.tv_notAuthorized);

        et_holderName = (EditText) view.findViewById(R.id.et_holderName);
        et_phoneNo = (EditText) view.findViewById(R.id.et_phoneNo);
        et_pincode = (EditText) view.findViewById(R.id.et_pincode);
        et_address = (EditText) view.findViewById(R.id.et_address);
        et_stbNo = (EditText) view.findViewById(R.id.et_stbNo);
        et_vcNo = (EditText) view.findViewById(R.id.et_vcNo);

        sp_zoneValue = (Spinner) view.findViewById(R.id.sp_zoneValue);

        btn_cableSearch = (Button) view.findViewById(R.id.btn_cableSearch);
        btn_reset = (Button) view.findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_cableSearch);
        Constants.buttonEffect(btn_reset);

        /** spinner set to ZONE part*/
        document_CableZoneAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.CableZone_Array, R.layout.simple_spinner_item);
        document_CableZoneAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_zoneValue.setAdapter(document_CableZoneAdapter);
        sp_zoneValue.setSelection(0);

        try {
            if (Utility.getUserInfo(getActivity()).getCable_allow().equalsIgnoreCase("1")) {
                relative_cable_main.setVisibility(View.VISIBLE);
                tv_notAuthorized.setVisibility(View.GONE);
            } else {
                relative_cable_main.setVisibility(View.GONE);
                tv_notAuthorized.setVisibility(View.VISIBLE);
                Constants.changefonts(tv_notAuthorized, getActivity(), "Calibri Bold.ttf");
            }
        }catch(NullPointerException e){
            e.printStackTrace();
            relative_cable_main.setVisibility(View.GONE);
            tv_notAuthorized.setVisibility(View.VISIBLE);
            Constants.changefonts(tv_notAuthorized, getActivity(), "Calibri Bold.ttf");
        }

        clickEvents();
    }

    private void clickEvents() {

        btn_cableSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String holderName = et_holderName.getText().toString().trim();
                String phoneNo = et_phoneNo.getText().toString().trim();
                String pincode = et_pincode.getText().toString().trim();
                String address = et_address.getText().toString().trim();
                String stbNo = et_stbNo.getText().toString().trim();
                String vcNo = et_vcNo.getText().toString().trim();
                String zone = sp_zoneValue.getSelectedItem().toString().trim().replace("Select the Zone","");

                if(!holderName.equalsIgnoreCase("") || !phoneNo.equalsIgnoreCase("")
                        || !pincode.equalsIgnoreCase("") || !address.equalsIgnoreCase("") || !stbNo.equalsIgnoreCase("")
                        || !vcNo.equalsIgnoreCase("") || !zone.equalsIgnoreCase("")){

                    showAlertForProceed();
                }
                else{
                    showAlertDialog(getActivity(), " Search Error!!! ", "Please provide atleast one value for search", false);
                }

            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_holderName.setText("");
                et_phoneNo.setText("");
                et_address.setText("");
                et_pincode.setText("");
                et_stbNo.setText("");
                et_vcNo.setText("");

                //*  for Zone part *//*
                sp_zoneValue.setSelection(0);
            }
        });
    }



    /*
   * Show alert before loading the data
   * */
    private void showAlertForProceed() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setTitle("Alert!");
        alertDialog.setMessage("It will take considerable time, proceed");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                fetchSearchResult();
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        alertDialog.show();
    }


    /*
  *    web service call
  * */
    private void fetchSearchResult() {

        String holderName = et_holderName.getText().toString().trim();
        String phoneNo = et_phoneNo.getText().toString().trim();
        String pincode = et_pincode.getText().toString().trim();
        String address = et_address.getText().toString().trim();
        String stbNo = et_stbNo.getText().toString().trim();
        String vcNo = et_vcNo.getText().toString().trim();
        String zone = sp_zoneValue.getSelectedItem().toString().trim().replace("Select the Zone","");

        if(!holderName.equalsIgnoreCase("") || !phoneNo.equalsIgnoreCase("")
                || !pincode.equalsIgnoreCase("") || !address.equalsIgnoreCase("") || !stbNo.equalsIgnoreCase("")
                || !vcNo.equalsIgnoreCase("") || !zone.equalsIgnoreCase("")){

            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.MODIFIED_METHOD_DOCUMENT_CABLE_SEARCH);
            taskManager.setDocumentCableSearch(true);
            try{
                devicetoken = GCMRegistrar.getRegistrationId(getActivity());
                auth_key=Utility.getDocumentSearchSesionId(getActivity());
            }
            catch (Exception e){

            }

            String []keys= {"name", "address", "pageno", "pincode", "phone_no", "stb_no", "vc_no", "zone","user_id","device_type", "device_token","imei","auth_key"};
            String []values= {holderName.trim(), address.trim(), "1", pincode.trim(), phoneNo.trim(), stbNo.trim(), vcNo.trim(), zone.trim(),userId,"android", devicetoken,Utility.getImiNO(getActivity()),auth_key};
            taskManager.doStartTask(keys,values,true,true);

        }
        else{
            showAlertDialog(getActivity(), " Search Error!!! ", "Please provide atleast one value for search", false);
        }
    }


    /*
    *   Parsing all data
    * */
    public void parseDocumentCableSearch(String result,String[] keys, String[] values) {

        //System.out.println("Document Cable Search Result: " + result);

        if(result != null && !result.equals("")){

            try{

                JSONObject jObj = new JSONObject(result);
                if(jObj.opt("STATUS").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("PAGE").toString();
                    totalResult = jObj.opt("COUNT").toString();

                    JSONArray resultArray = jObj.getJSONArray("RESULT");

                    parseKMCSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(getActivity()," Search Error ",jObj.optString("MESSAGE"),false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                showAlertDialog(getActivity(), " Search Error!!! ", "Some error has been encountered", false);
            }

        }
    }


    private void parseKMCSearchResponse(JSONArray resultArray) {

        cableSearchDeatilsList = new ArrayList<CableSearchDetails>();

        for(int i=0;i<resultArray.length();i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj = resultArray.getJSONObject(i);

                CableSearchDetails cableSearchDetails = new CableSearchDetails(Parcel.obtain());

                if(!jsonObj.optString("NAME").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderName(jsonObj.optString("NAME"));
                }

                if(!jsonObj.optString("ADDRESS").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderAddress(jsonObj.optString("ADDRESS"));
                }

                if(!jsonObj.optString("CITY").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderCity(jsonObj.optString("CITY"));
                }

                if(!jsonObj.optString("STATE").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderState(jsonObj.optString("STATE"));
                }

                if(!jsonObj.optString("ZONE").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderZone(jsonObj.optString("ZONE"));
                }

                if(!jsonObj.optString("PINCODE").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderPincode(jsonObj.optString("PINCODE"));
                }

                if(!jsonObj.optString("STB").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderSTBNo(jsonObj.optString("STB"));
                }

                if(!jsonObj.optString("VC").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_VCNo(jsonObj.optString("VC"));
                }

                if(!jsonObj.optString("MOBILENUMBER").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderMobileNo(jsonObj.optString("MOBILENUMBER"));
                }

                if(!jsonObj.optString("OFFICENUMBER").equalsIgnoreCase("null")){
                    cableSearchDetails.setCable_holderOfficeNo(jsonObj.optString("OFFICENUMBER"));
                }

                cableSearchDeatilsList.add(cableSearchDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Intent intent = new Intent(getActivity(), CableSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putParcelableArrayListExtra("CABLE_SEARCH_LIST", (ArrayList<? extends Parcelable>) cableSearchDeatilsList);
        startActivity(intent);
    }


    /*
   *  show alert if any error occured
   * */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }
}
