package com.kp.facedetection.interfaces;

/**
 * Created by JAYDEEP on 5/1/2018.
 */

public interface OnSpecialServiceSearchListListener {
    void onSpecialServiceSearchListItemListener(int position);

}
