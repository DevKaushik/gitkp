package com.kp.facedetection.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.kp.facedetection.MobileStolenSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.model.MobileStolenSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MobileStolenFragment extends Fragment implements View.OnClickListener{

    private TextView tv_fromDateValue;
    private TextView tv_toDateValue;
    private TextView tv_psValue;
    private EditText et_mobilenoValue;
    private EditText et_imeiNoValue;
    private EditText et_complainantNameValue;
    private EditText et_gdeValue;

    private Button btn_mobileStolenSearch;
    private Button btn_reset;

    private Spinner sp_status;
    private Spinner sp_FirGde;

    protected String[] array_policeStation;
    protected ArrayList<CharSequence> selectedPoliceStations = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedPoliceStationsId = new ArrayList<CharSequence>();
    private String policeStationString = "";

    private String pageno="";
    private String totalResult="";
    private String[] keys;
    private String[] values;

    private List<MobileStolenSearchDetails> mobileStolenSearchDetailsList;
    private List<CharSequence>  mobileStolenStatusList = new ArrayList<CharSequence>();

    private ArrayAdapter<CharSequence> mobileStolenStatusAdapter;
    ArrayAdapter<CharSequence> caseTypeAdapter;
    String userId="";
    ImageView toggleButtonSearchType;
    String ownjurisdictionFlag="1";
    boolean search_own_global_flag=false;


    public MobileStolenFragment() {
        // Required empty public constructor
    }

    public static MobileStolenFragment newInstance() {

        MobileStolenFragment mobileStolenFragment = new MobileStolenFragment();
        //warrantSearchFragment.setArguments(args);
        return mobileStolenFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mobile_stolen, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        userId= Utility.getUserInfo(getContext()).getUserId();
        toggleButtonSearchType=(ImageView) view.findViewById(R.id.Tb_search_type);
        toggleButtonSearchType.setOnClickListener(this);
        tv_fromDateValue = (TextView) view.findViewById(R.id.tv_fromDateValue);
        tv_toDateValue = (TextView) view.findViewById(R.id.tv_toDateValue);
        tv_psValue = (TextView) view.findViewById(R.id.tv_psValue);

        et_mobilenoValue = (EditText) view.findViewById(R.id.et_mobilenoValue);
        et_imeiNoValue = (EditText) view.findViewById(R.id.et_imeiNoValue);
        et_complainantNameValue = (EditText) view.findViewById(R.id.et_complainantNameValue);
        et_gdeValue = (EditText) view.findViewById(R.id.et_gdeValue);

        btn_mobileStolenSearch = (Button) view.findViewById(R.id.btn_mobileStolenSearch);
        btn_reset = (Button) view.findViewById(R.id.btn_reset);

        sp_status = (Spinner) view.findViewById(R.id.sp_status);
        sp_FirGde = (Spinner) view.findViewById(R.id.sp_FirGde);

        Constants.buttonEffect(btn_mobileStolenSearch);
        Constants.buttonEffect(btn_reset);

        array_policeStation = new String[Constants.policeStationNameArrayList.size()];
        array_policeStation = Constants.policeStationNameArrayList.toArray(array_policeStation);

        mobileStolenStatusList.add("Select a status");

        mobileStolenStatusAdapter = new ArrayAdapter<CharSequence>(getActivity(), R.layout.simple_spinner_item, mobileStolenStatusList);
        mobileStolenStatusAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_status.setAdapter(mobileStolenStatusAdapter);

        getMobileStolenStatusData();

        /** spiner set to case type*/
        caseTypeAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.CaseType_Array, R.layout.simple_spinner_item);
        caseTypeAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_FirGde.setAdapter(caseTypeAdapter);
        sp_FirGde.setSelection(0);

        clickEvents();

    }

    private void clickEvents() {

        /* Start Date field click event */
        tv_fromDateValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DateUtils.setDate(getActivity(), tv_fromDateValue, true);
            }
        });


        /* End Date field click event */
        tv_toDateValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DateUtils.setDate(getActivity(), tv_toDateValue, true);
            }
        });


         /* Police stations Click Event */
        tv_psValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Constants.policeStationNameArrayList.size() > 0) {
                    tv_psValue.setError(null);
                    showSelectPoliceStationsDialog();
                } else {
                    //Toast.makeText(getActivity(),"Sorry, some error encountered!",Toast.LENGTH_LONG).show();
                    showAlertDialog(getActivity(), " Search Error!!! ", "Some error has been encountered", false);
                }

            }
        });

         /* Resetting all fields to original state */

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resetDataFieldValues();

            }
        });

        btn_mobileStolenSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String startDate = tv_fromDateValue.getText().toString().trim();
                String endDate = tv_toDateValue.getText().toString().trim();

                if (!endDate.equalsIgnoreCase("") && startDate.equalsIgnoreCase("")) {
                    showAlertDialog(getActivity(), " Search Error!!! ", "Please provide both dates", false);
                } else if (endDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("")) {
                    showAlertDialog(getActivity(), " Search Error!!! ", "Please provide both dates", false);
                } else if (!endDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("")) {
                    boolean date_result_flag = DateUtils.dateComparison(startDate, endDate);
                    System.out.println("DateComparison: " + date_result_flag);
                    if (!date_result_flag) {
                        showAlertDialog(getActivity(), " Search Error!!! ", "From date can't be greater than To date", false);
                    } else {
                        fetchmobileStolenSearchResult();
                    }
                } else {
                    fetchmobileStolenSearchResult();
                }

            }
        });

    }

    private void resetDataFieldValues() {

        /* for police stations part*/
        tv_psValue.setText("Select Police Stations");
        policeStationString = "";
        selectedPoliceStations.clear();
        selectedPoliceStationsId.clear();
        tv_psValue.setError(null);

        //* for From Date part *//*
        tv_fromDateValue.setText("");

        //* for To Date part *//*
        tv_toDateValue.setText("");

        //* for Mobile no part *//*
        et_mobilenoValue.setText("");

        //* for IMEI No part *//*
        et_imeiNoValue.setText("");

        //* for Complainant Name part *//*
        et_complainantNameValue.setText("");

        //* for GDE part *//*
        et_gdeValue.setText("");

        sp_status.setSelection(0);
        sp_FirGde.setSelection(0);
    }

    /* This method shows Police Station list in a pop-up */
    protected void showSelectPoliceStationsDialog() {
        boolean[] checkedPoliceStations = new boolean[array_policeStation.length];
        int count = array_policeStation.length;

        for (int i = 0; i < count; i++) {
            checkedPoliceStations[i] = selectedPoliceStations.contains(array_policeStation[i]);
        }

        DialogInterface.OnMultiChoiceClickListener policeStationsDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedPoliceStations.add(array_policeStation[which]);
                    selectedPoliceStationsId.add((CharSequence) Constants.policeStationIDArrayList.get(which));
                } else {
                    selectedPoliceStations.remove(array_policeStation[which]);
                    selectedPoliceStationsId.remove((CharSequence) Constants.policeStationIDArrayList.get(which));
                }

//				onChangeSelectedPoliceStations();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Police Stations");
        builder.setMultiChoiceItems(array_policeStation, checkedPoliceStations, policeStationsDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                onChangeSelectedPoliceStations();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                tv_psValue.setText("Select Police Stations");
                policeStationString = "";
                selectedPoliceStations.clear();
                selectedPoliceStationsId.clear();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedPoliceStations() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

       /* if(ioAfterPS){

            scrollView1.scrollTo(0,scrollView1.getBottom());
            tv_ioValue.requestFocus();
            tv_ioValue.setError("");
        }*/

        for (CharSequence policeStation : selectedPoliceStations) {
            stringBuilder.append(policeStation + ",");
        }

        for (CharSequence policeStation : selectedPoliceStationsId) {
            stringBuilderId.append("\'" + policeStation + "\',");
        }

        if (selectedPoliceStations.size() == 0) {
            tv_psValue.setText("Select Police Stations");
            policeStationString = "";
        } else {
            tv_psValue.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
            policeStationString = stringBuilderId.toString().substring(0, stringBuilderId.toString().length() - 1);
        }

    }

    /**
     * Function to display simple Alert Dialog
     *
     * @param context - application context
     * @param title   - alert dialog title
     * @param message - alert message
     * @param status  - success/failure (used to set icon)
     */
    private void showAlertDialog(Context context, String title, final String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    private void fetchmobileStolenSearchResult() {

        String date_from = tv_fromDateValue.getText().toString().trim();
        String date_to = tv_toDateValue.getText().toString().trim();
        String mobileNo_value = et_mobilenoValue.getText().toString().trim().replace("Mobile No", "");
        String imeiNo_value = et_imeiNoValue.getText().toString().trim().replace("IMEI No", "");
        String complainantName = et_complainantNameValue.getText().toString().trim().replace("Complainant Name", "");
        String gdeNo = et_gdeValue.getText().toString().trim().replace("GDE / Case No", "");
        String status = sp_status.getSelectedItem().toString().trim().replace("Select a status", "");
        String firGde = sp_FirGde.getSelectedItem().toString().trim().replace("Select the Type", "");

        if (!policeStationString.equalsIgnoreCase("") || !date_from.equalsIgnoreCase("") || !date_to.equalsIgnoreCase("")
                || !mobileNo_value.equalsIgnoreCase("") || !imeiNo_value.equalsIgnoreCase("") || !complainantName.equalsIgnoreCase("")
                || !gdeNo.equalsIgnoreCase("") || !status.equalsIgnoreCase("") || !firGde.equalsIgnoreCase("")) {

            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.METHOD_MOBILE_STOLEN_SEARCH);
            taskManager.setMobileStolenSearch(true);

            String[] keys = {"from_date", "to_date", "ps", "mobile_no", "imeino", "comp_name", "gde_fir_no", "pageno", "status", "gde_fir", "user_id","own_jurisdiction"};

            String[] values = {date_from.trim(), date_to.trim(), policeStationString.trim(), mobileNo_value.trim(), imeiNo_value.trim(), complainantName.trim(), gdeNo.trim(), "1", status.trim(), firGde.trim(), userId,ownjurisdictionFlag};

            taskManager.doStartTask(keys, values, true);

        } else {

            showAlertDialog(getActivity(), " Search Error!!! ", "Please provide atleast one value for search", false);

        }

    }

    public void parseMobileStolenSearchResultResponse(String result, String[] keys, String[] values) {

        //System.out.println("parseMobileStolenSearchResultResponse: " + result);

        if(result != null && !result.equals("")){

            try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.opt("status").toString().equalsIgnoreCase("1")){

                    this.keys=keys;
                    this.values=values;
                    pageno=jObj.opt("page").toString();
                    totalResult=jObj.opt("count").toString();

                    JSONArray resultArray=jObj.getJSONArray("result");

                    parseMobileStolenSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(getActivity()," Search Error ",jObj.optString("message"),false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                showAlertDialog(getActivity(), " Search Error!!! ", "Some error has been encountered", false);
            }
        }

    }

    private void parseMobileStolenSearchResponse(JSONArray resultArray) {

        mobileStolenSearchDetailsList=new ArrayList<MobileStolenSearchDetails>();

        for(int i=0;i<resultArray.length();i++) {

            JSONObject jsonObj = null;

            try{
                jsonObj=resultArray.getJSONObject(i);

                MobileStolenSearchDetails mobileStolenSearchDetails = new MobileStolenSearchDetails();

                if(!jsonObj.optString("ROWNUMBER").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setRowNo(jsonObj.optString("ROWNUMBER"));
                }

                if(!jsonObj.optString("TRNID").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setTrnId(jsonObj.optString("TRNID"));
                }

                if(!jsonObj.optString("DTMISSING").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setDtMissing(jsonObj.optString("DTMISSING"));
                }

                if(!jsonObj.optString("PSCODE").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setPsCode(jsonObj.optString("PSCODE"));
                }

                if(!jsonObj.optString("MOBILENO").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setMobileNo(jsonObj.optString("MOBILENO"));
                }

                if(!jsonObj.optString("IMEI").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setImeiNo(jsonObj.optString("IMEI"));
                }

                if(!jsonObj.optString("COMPLNTNAME").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setComplainantName(jsonObj.optString("COMPLNTNAME"));
                }

                if(!jsonObj.optString("CASEREF").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setCaseRef(jsonObj.optString("CASEREF"));
                }

                if(!jsonObj.optString("OPNAME").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setPsName(jsonObj.optString("OPNAME"));
                }

                if(!jsonObj.optString("COMPLNTADDR").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setComplainantAddress(jsonObj.optString("COMPLNTADDR"));
                }

                if(!jsonObj.optString("PLMISSING").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setPlMissing(jsonObj.optString("PLMISSING"));
                }

                if(!jsonObj.optString("IONAME").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setIoName(jsonObj.optString("IONAME"));
                }

                if(!jsonObj.optString("GDEDATE").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setGdeDate(jsonObj.optString("GDEDATE"));
                }

                if(!jsonObj.optString("BRFACT").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setBrFact(jsonObj.optString("BRFACT"));
                }

                if(!jsonObj.optString("PRSTATUS").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setPrStatus(jsonObj.optString("PRSTATUS"));
                }

                if(!jsonObj.optString("REFERENCE").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setReference(jsonObj.optString("REFERENCE"));
                }

                if(!jsonObj.optString("FIR_GD").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setFirGD(jsonObj.optString("FIR_GD"));
                }

                if(!jsonObj.optString("CONTACTNO").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setContactNo(jsonObj.optString("CONTACTNO"));
                }

                if(!jsonObj.optString("USEC").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setUsec(jsonObj.optString("USEC"));
                }

                if(!jsonObj.optString("SPROVIDER").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setsProvider(jsonObj.optString("SPROVIDER"));
                }

                if(!jsonObj.optString("MAKE").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setMake(jsonObj.optString("MAKE"));
                }

                if(!jsonObj.optString("MODEL").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setModel(jsonObj.optString("MODEL"));
                }

                if(!jsonObj.optString("DTHANDEDOVER").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setDtHandedOver(jsonObj.optString("DTHANDEDOVER"));
                }

                if(!jsonObj.optString("SOURCENAME").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setSourceName(jsonObj.optString("SOURCENAME"));
                }

                if(!jsonObj.optString("TRACED_ON").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setTracedOn(jsonObj.optString("TRACED_ON"));
                }

                if(!jsonObj.optString("TRACED_MOBNO").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setTracedMobNo(jsonObj.optString("TRACED_MOBNO"));
                }

                if(!jsonObj.optString("NAME_USER").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setNameUser(jsonObj.optString("NAME_USER"));
                }

                if(!jsonObj.optString("ADDR").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setAddr(jsonObj.optString("ADDR"));
                }

                if(!jsonObj.optString("RECOVDT").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setRecoverDate(jsonObj.optString("RECOVDT"));
                }

                if(!jsonObj.optString("RECOVDBY").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setRecoverBy(jsonObj.optString("RECOVDBY"));
                }

                if(!jsonObj.optString("ESN").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setEsn(jsonObj.optString("ESN"));
                }

                if(!jsonObj.optString("DEVTYPE").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setDevType(jsonObj.optString("DEVTYPE"));
                }

                if(!jsonObj.optString("MOBILENO2").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setMobileNo2(jsonObj.optString("MOBILENO2"));
                }

                if(!jsonObj.optString("IMEI2").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setImei2(jsonObj.optString("IMEI2"));
                }

                if(!jsonObj.optString("ESN2").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setEsn2(jsonObj.optString("ESN2"));
                }

                if(!jsonObj.optString("SPROVIDER2").equalsIgnoreCase("null")){
                    mobileStolenSearchDetails.setSprovider2(jsonObj.optString("SPROVIDER2"));
                }



                mobileStolenSearchDetailsList.add(mobileStolenSearchDetails);

            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        Intent intent=new Intent(getActivity(), MobileStolenSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putExtra("MOBILE_STOLEN_SEARCH_LIST", (Serializable) mobileStolenSearchDetailsList);
        startActivity(intent);

    }

    private void getMobileStolenStatusData(){

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_MOBILE_STOLEN_STATUS);
        taskManager.setMobileStolenStatus(true);

        String[] keys = {  };
        String[] values = { };
        taskManager.doStartTask(keys, values, true);

    }

    public void parseGetMobileStolenStatusResponse(String response) {

        //System.out.println("Mobile Stolen Status Result: " + response);

        if (response != null && !response.equals("")) {

            JSONObject jObj = null;
            try {

                jObj = new JSONObject(response);
                if(jObj != null && jObj.optString("status").equalsIgnoreCase("1")){

                    mobileStolenStatusList.clear();
                    mobileStolenStatusList.add("Select a status");

                    JSONArray stausArray = jObj.getJSONObject("result").getJSONArray("status");

                    for(int i=0;i<stausArray.length();i++){

                        JSONObject status_obj = stausArray.getJSONObject(i);
                        mobileStolenStatusList.add(status_obj.optString("PRSTATUS"));
                    }

                    mobileStolenStatusAdapter.notifyDataSetChanged();

                }
                else {

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Tb_search_type:
                if(!search_own_global_flag){
                    toggleButtonSearchType.setImageResource(R.drawable.toggle_right);
                    ownjurisdictionFlag="0";
                    search_own_global_flag=true;
                }
                else{
                    toggleButtonSearchType.setImageResource(R.drawable.toggle_left);
                    ownjurisdictionFlag="1";
                    search_own_global_flag=false;
                }

                break;
        }

    }
}


