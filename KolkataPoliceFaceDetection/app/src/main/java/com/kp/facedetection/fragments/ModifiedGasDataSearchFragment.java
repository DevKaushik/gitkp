package com.kp.facedetection.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.kp.facedetection.GasSearchResultActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.model.ModifiedDocumentGasDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 02-12-2016.
 */
public class ModifiedGasDataSearchFragment extends Fragment {

    private RelativeLayout relative_gas_main;
    private TextView tv_notAuthorized;
    private Button btn_Search, btn_reset;
    private EditText et_distributorName, et_consumerId, et_consumerName, et_address, et_pincode, et_mobileNo;
    private Spinner sp_statusValue;

    ArrayAdapter<CharSequence> document_GasStatusAdapter;

    private String pageno="";
    private String totalResult="";
    private String[] keys;
    private String[] values;
    String userId="";
    String devicetoken="";
    String auth_key="";

    private List<ModifiedDocumentGasDetails> gasSearchDeatilsList;

    public static ModifiedGasDataSearchFragment newInstance() {

        ModifiedGasDataSearchFragment modifiedGasDataSearchFragment = new ModifiedGasDataSearchFragment();
        return modifiedGasDataSearchFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.modified_fragment_gas_data_search, container, false);
        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        userId= Utility.getUserInfo(getContext()).getUserId();
        relative_gas_main = (RelativeLayout) view.findViewById(R.id.relative_gas_main);
        tv_notAuthorized = (TextView) view.findViewById(R.id.tv_notAuthorized);

        btn_Search = (Button) view.findViewById(R.id.btn_gasSearch);
        btn_reset = (Button) view.findViewById(R.id.btn_reset);

        et_distributorName = (EditText) view.findViewById(R.id.et_distributorName);
        et_consumerId = (EditText) view.findViewById(R.id.et_consumerId);
        et_consumerName = (EditText) view.findViewById(R.id.et_consumerName);
        et_address = (EditText) view.findViewById(R.id.et_address);
        et_pincode = (EditText) view.findViewById(R.id.et_pincode);
        et_mobileNo = (EditText) view.findViewById(R.id.et_mobileNo);

        sp_statusValue = (Spinner) view.findViewById(R.id.sp_statusValue);

        Constants.buttonEffect(btn_Search);
        Constants.buttonEffect(btn_reset);

        /** spinner set to status part*/
        document_GasStatusAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.GasStatus_Array, R.layout.simple_spinner_item);
        document_GasStatusAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_statusValue.setAdapter(document_GasStatusAdapter);
        sp_statusValue.setSelection(0);

        try {
            if (Utility.getUserInfo(getActivity()).getGas_allow().equalsIgnoreCase("1")) {
                relative_gas_main.setVisibility(View.VISIBLE);
                tv_notAuthorized.setVisibility(View.GONE);
            } else {
                relative_gas_main.setVisibility(View.GONE);
                tv_notAuthorized.setVisibility(View.VISIBLE);
                Constants.changefonts(tv_notAuthorized, getActivity(), "Calibri Bold.ttf");
            }
        }catch(NullPointerException e){
            e.printStackTrace();
            relative_gas_main.setVisibility(View.GONE);
            tv_notAuthorized.setVisibility(View.VISIBLE);
            Constants.changefonts(tv_notAuthorized, getActivity(), "Calibri Bold.ttf");
        }

        clickEvents();
    }

    private void clickEvents() {

        btn_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String distributorName = et_distributorName.getText().toString().trim();
                String consumerId = et_consumerId.getText().toString().trim();
                String consumerName = et_consumerName.getText().toString().trim();
                String address = et_address.getText().toString().trim();
                String pincode = et_pincode.getText().toString().trim();
                String phnNo = et_mobileNo.getText().toString().trim();
                String consumerStatus = sp_statusValue.getSelectedItem().toString().trim().replace("Select the Status","");

                if(!distributorName.equalsIgnoreCase("") || !consumerId.equalsIgnoreCase("") || !consumerName.equalsIgnoreCase("")
                        || !address.equalsIgnoreCase("") || !pincode.equalsIgnoreCase("") || !phnNo.equalsIgnoreCase("")
                        ||!consumerStatus.equalsIgnoreCase("")){

                    showAlertForProceed();
                }
                else{
                    showAlertDialog(getActivity(), " Search Error!!! ", "Please provide atleast one value for search", false);
                }
            }
        });


        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //*  for Status part *//*
                sp_statusValue.setSelection(0);

                et_distributorName.setText("");
                et_consumerId.setText("");
                et_consumerName.setText("");
                et_address.setText("");
                et_pincode.setText("");
                et_mobileNo.setText("");
            }
        });
    }



    /*
    * Show alert before loading the data
    * */
    private void showAlertForProceed() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setTitle("Alert!");
        alertDialog.setMessage("It will take considerable time, proceed");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                fetchSearchResult();
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        alertDialog.show();
    }



    /*
    *  show alert if any error occured
    * */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }



    /*
    *  Fetch the data from service call
    * */
    private void fetchSearchResult() {


        String distributorName = et_distributorName.getText().toString().trim();
        String consumerId = et_consumerId.getText().toString().trim();
        String consumerName = et_consumerName.getText().toString().trim();
        String address = et_address.getText().toString().trim();
        String pincode = et_pincode.getText().toString().trim();
        String phnNo = et_mobileNo.getText().toString().trim();
        String consumerStatus = sp_statusValue.getSelectedItem().toString().trim().replace("Select the Status","");

        if(!distributorName.equalsIgnoreCase("") || !consumerId.equalsIgnoreCase("") || !consumerName.equalsIgnoreCase("")
                || !address.equalsIgnoreCase("") || !pincode.equalsIgnoreCase("") || !phnNo.equalsIgnoreCase("")
                ||!consumerStatus.equalsIgnoreCase("")){

            TaskManager taskManager = new TaskManager(this);
            taskManager.setMethod(Constants.MODIFIED_METHOD_DOCUMENT_GAS_SEARCH);
            taskManager.setDocumentGasSearch(true);
            try{
                devicetoken = GCMRegistrar.getRegistrationId(getActivity());
                auth_key=Utility.getDocumentSearchSesionId(getActivity());
            }
            catch (Exception e){

            }

            String[] keys = {"name", "address", "pincode", "phone_no","pageno", "distributor", "consumer_id", "status","user_id","device_type", "device_token","imei","auth_key"};

            String[] values = {consumerName.trim(), address.trim(), pincode.trim(), phnNo.trim(),"1", distributorName.trim(), consumerId.trim(), consumerStatus.trim(),userId,"android", devicetoken,Utility.getImiNO(getActivity()),auth_key};

            taskManager.doStartTask(keys, values, true, true);
        }
        else{

            showAlertDialog(getActivity(), " Search Error!!! ", "Please provide atleast one value for search", false);

        }
    }


    public void parseDocumentGasSearch(String result,String[] keys, String[] values) {

        //System.out.println("Document GAS Search Result: " + result);

        if(result != null && !result.equals("")){

            try{

                JSONObject jObj = new JSONObject(result);
                if(jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("page").toString();
                    totalResult = jObj.opt("count").toString();

                    JSONArray resultArray = jObj.getJSONArray("result");

                    parseGasSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(getActivity()," Search Error ",jObj.optString("message"),false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                showAlertDialog(getActivity(), " Search Error!!! ", "Some error has been encountered", false);
            }

        }

    }


    private void parseGasSearchResponse(JSONArray resultArray) {

        gasSearchDeatilsList = new ArrayList<ModifiedDocumentGasDetails>();

        for(int i=0;i<resultArray.length();i++){

            JSONObject jsonObj=null;

            try {
                jsonObj = resultArray.getJSONObject(i);

                ModifiedDocumentGasDetails gasSearchDetails = new ModifiedDocumentGasDetails();

                if(!jsonObj.optString("DISTRIBUTOR").equalsIgnoreCase("null")){
                    gasSearchDetails.setDistributor_id(jsonObj.optString("DISTRIBUTOR"));
                }

                if(!jsonObj.optString("DISTRIBUTOR_NAME").equalsIgnoreCase("null")){
                    gasSearchDetails.setDistributor_name(jsonObj.optString("DISTRIBUTOR_NAME"));
                }

                if(!jsonObj.optString("CONSUMER_ID").equalsIgnoreCase("null")){
                    gasSearchDetails.setConsumer_id(jsonObj.optString("CONSUMER_ID"));
                }

                if(!jsonObj.optString("CONSUMER_NAME").equalsIgnoreCase("null")){
                    gasSearchDetails.setConsumer_name(jsonObj.optString("CONSUMER_NAME"));
                }

                if(!jsonObj.optString("CONSUMER_STATUS").equalsIgnoreCase("null")){
                    gasSearchDetails.setConsumer_status(jsonObj.optString("CONSUMER_STATUS"));
                }

                if(!jsonObj.optString("ADDRESS").equalsIgnoreCase("null")){
                    gasSearchDetails.setAddress(jsonObj.optString("ADDRESS"));
                }

                if(!jsonObj.optString("PIN_CODE").equalsIgnoreCase("null")){
                    gasSearchDetails.setPincode(jsonObj.optString("PIN_CODE"));
                }

                if(!jsonObj.optString("MOBILE_NUMBER").equalsIgnoreCase("null")){
                    gasSearchDetails.setPhone_no(jsonObj.optString("MOBILE_NUMBER"));
                }

                if(!jsonObj.optString("SV_DATE").equalsIgnoreCase("null")){
                    gasSearchDetails.setSv_date(jsonObj.optString("SV_DATE"));
                }

                if(!jsonObj.optString("HAS_AADHAAR_NO").equalsIgnoreCase("null")){
                    gasSearchDetails.setHas_aadhar(jsonObj.optString("HAS_AADHAAR_NO"));
                }

                gasSearchDeatilsList.add(gasSearchDetails);

            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Intent intent = new Intent(getActivity(), GasSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putExtra("GAS_SEARCH_LIST", (Serializable) gasSearchDeatilsList);
        startActivity(intent);
    }
}
