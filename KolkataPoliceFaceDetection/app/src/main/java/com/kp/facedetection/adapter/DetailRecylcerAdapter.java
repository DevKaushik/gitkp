package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnItemClickListenerForIO;
import com.kp.facedetection.interfaces.OnItemClickListenerforLIPartialSaveIO;
import com.kp.facedetection.model.IO;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;

import java.util.List;


public class DetailRecylcerAdapter extends RecyclerView.Adapter<DetailRecylcerAdapter.ViewHolder> {
    OnItemClickListenerForIO onItemClickListenerForIO;
    OnItemClickListenerforLIPartialSaveIO onItemClickListenerforLIPartialSaveIO;
    List<IO> dataIO;

    Context mContext;

    public DetailRecylcerAdapter(Context context, List<IO> dataIO) {
        this.mContext = context;
        this.dataIO = dataIO;
    }
    public void setOnItemClickListenerForIO( OnItemClickListenerForIO onItemClickListenerForIO){
        this.onItemClickListenerForIO = onItemClickListenerForIO;
    }
    public void setOnItemClickListenerforLIPartialSave( OnItemClickListenerforLIPartialSaveIO onItemClickListenerforLIPartialSaveIO){
        this.onItemClickListenerforLIPartialSaveIO = onItemClickListenerforLIPartialSaveIO;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.special_services_inflater, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.name_TV.setText(dataIO.get(position).getOfficerName());
        holder.id_TV.setText(dataIO.get(position).getId());
        holder.caseRef_TV.setText(dataIO.get(position).getCaseRef());
        holder.reqType_TV.setText(dataIO.get(position).getRequestType());
        if(dataIO.get(position).getReconnection().equalsIgnoreCase("1")){
            holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[0]);
        }
        else if(dataIO.get(position).getDisconnection().equalsIgnoreCase("1")){
            holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[1]);
        }  else {
            if(dataIO.get(position).getUrgentGeneralFlag().equalsIgnoreCase("1")) {
                holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(URGENT)");
            }
            else {
                holder.div_request_subtype_TV.setText(Constants.SPECIAL_SERVICE_REQUEST_SUBTYPE[2] + "(GENERAL)");
            }
        }

        holder.div_TV.setText(dataIO.get(position).getDiv()+"/"+dataIO.get(position).getPs());
        holder.status.setBackgroundColor(Color.parseColor(dataIO.get(position).getStatusColour()));
        String date_time=dataIO.get(position).getRequestTime();
        String date="";
        if(date_time.contains(" ")) {
            String[] dateTimeArray= date_time.split(" ");
            date=dateTimeArray[0];
            String formatedDate= DateUtils.changeDateFormat(date);
            holder.date_time.setText(formatedDate);
        }

     //   holder.date_time.setText(dataIO.get(position).getRequestTime());
        holder.status.setText(dataIO.get(position).getStatusMsg());
        //holder.status.setText(status);
        Constants.changefonts(holder.name_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.id_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.caseRef_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.reqType_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.div_request_subtype_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.div_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.status_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.date_time_label, mContext,"Calibri Bold.ttf");

        Constants.changefonts(holder.name_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.id_TV_label, mContext,"Calibri Bold.ttf");
        Constants.changefonts(holder.caseRef_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.reqType_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.div_request_subtype_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.div_TV, mContext, "Calibri.ttf");
        Constants.changefonts(holder.status, mContext, "Calibri.ttf");
        Constants.changefonts(holder.date_time, mContext, "Calibri.ttf");



       /* if (position % 2 == 0) {
            holder.ll_special_service_request_item.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            holder.ll_special_service_request_item.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

*/

    }


    @Override
    public int getItemCount() {
        return (dataIO != null ? dataIO.size() : 0);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView id_TV,caseRef_TV,name_TV,reqType_TV,div_TV,status,div_request_subtype_TV,date_time;
        TextView id_TV_label,caseRef_TV_label,name_TV_label,reqType_TV_label,div_TV_label,status_label,div_request_subtype_label,date_time_label;
        LinearLayout ll_special_service_request_item;


        public ViewHolder(View itemView) {
            super(itemView);
            ll_special_service_request_item=(LinearLayout)itemView.findViewById(R.id.ll_special_service_request_item);
            id_TV =(TextView)itemView.findViewById(R.id.id_TV);
            name_TV = (TextView) itemView.findViewById(R.id.name_TV);
            caseRef_TV = (TextView) itemView.findViewById(R.id.caseRef_TV);
            reqType_TV = (TextView) itemView.findViewById(R.id.reqType_TV);
            div_request_subtype_TV=(TextView) itemView.findViewById(R.id.div_request_subtype_TV);
            div_request_subtype_label=(TextView)itemView.findViewById(R.id.div_request_subtype_label);
            div_TV =(TextView) itemView.findViewById(R.id.div_TV);
            status = (TextView) itemView.findViewById(R.id.status);
            date_time = (TextView) itemView.findViewById(R.id.date_time);
            id_TV_label =(TextView)itemView.findViewById(R.id.id_TV_label);
            caseRef_TV_label=(TextView) itemView.findViewById(R.id.caseRef_TV_label);
            name_TV_label=(TextView) itemView.findViewById(R.id.name_TV_label);
            reqType_TV_label=(TextView) itemView.findViewById(R.id.reqType_TV_label);
            div_TV_label=(TextView) itemView.findViewById(R.id.div_TV_label);
            status_label=(TextView) itemView.findViewById(R.id.status_label);
            date_time_label=(TextView) itemView.findViewById(R.id.date_time_label);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if(dataIO.get(getLayoutPosition()).getDraftSave().equalsIgnoreCase("1")&& dataIO.get(getLayoutPosition()).getCompleteSave().equalsIgnoreCase("0")){
              //  onItemClickListenerforLIPartialSaveIO.onItemClickforLIPartialSaveIO(getLayoutPosition());
                onItemClickListenerForIO.onItemClickForIO(v, getLayoutPosition());
            }
            else if(dataIO.get(getLayoutPosition()).getDraftSave().equalsIgnoreCase("1")&& dataIO.get(getLayoutPosition()).getCompleteSave().equalsIgnoreCase("1")){
                onItemClickListenerForIO.onItemClickForIO(v, getLayoutPosition());
            }
            else {
                onItemClickListenerForIO.onItemClickForIO(v, getLayoutPosition());
            }
        }
    }


}
