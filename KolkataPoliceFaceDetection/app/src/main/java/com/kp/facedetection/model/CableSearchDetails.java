package com.kp.facedetection.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DAT-165 on 07-12-2016.
 */
public class CableSearchDetails implements Parcelable {

    private String cable_holderName = "";
    private String cable_holderCity = "";
    private String cable_holderState = "";
    private String cable_holderZone = "";
    private String cable_holderPincode = "";
    private String cable_holderSTBNo = "";
    private String cable_VCNo = "";
    private String cable_holderMobileNo = "";
    private String cable_holderOfficeNo = "";
    private String cable_holderAddress = "";


    public CableSearchDetails(Parcel in) {
        setCable_holderName(in.readString());
        setCable_holderCity(in.readString());
        setCable_holderState(in.readString());
        setCable_holderZone(in.readString());
        setCable_holderPincode(in.readString());
        setCable_holderSTBNo(in.readString());
        setCable_VCNo(in.readString());
        setCable_holderMobileNo(in.readString());
        setCable_holderOfficeNo(in.readString());
        setCable_holderAddress(in.readString());

    }

    public static final Creator<CableSearchDetails> CREATOR = new Creator<CableSearchDetails>() {
        @Override
        public CableSearchDetails createFromParcel(Parcel in) {
            return new CableSearchDetails(in);
        }

        @Override
        public CableSearchDetails[] newArray(int size) {
            return new CableSearchDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getCable_holderName());
        dest.writeString(getCable_holderCity());
        dest.writeString(getCable_holderState());
        dest.writeString(getCable_holderZone());
        dest.writeString(getCable_holderPincode());
        dest.writeString(getCable_holderSTBNo());
        dest.writeString(getCable_VCNo());
        dest.writeString(getCable_holderMobileNo());
        dest.writeString(getCable_holderOfficeNo());
        dest.writeString(getCable_holderAddress());

    }


    public String getCable_holderName() {
        return cable_holderName;
    }

    public void setCable_holderName(String cable_holderName) {
        this.cable_holderName = cable_holderName;
    }

    public String getCable_holderCity() {
        return cable_holderCity;
    }

    public void setCable_holderCity(String cable_holderCity) {
        this.cable_holderCity = cable_holderCity;
    }

    public String getCable_holderState() {
        return cable_holderState;
    }

    public void setCable_holderState(String cable_holderState) {
        this.cable_holderState = cable_holderState;
    }

    public String getCable_holderZone() {
        return cable_holderZone;
    }

    public void setCable_holderZone(String cable_holderZone) {
        this.cable_holderZone = cable_holderZone;
    }

    public String getCable_holderPincode() {
        return cable_holderPincode;
    }

    public void setCable_holderPincode(String cable_holderPincode) {
        this.cable_holderPincode = cable_holderPincode;
    }

    public String getCable_holderSTBNo() {
        return cable_holderSTBNo;
    }

    public void setCable_holderSTBNo(String cable_holderSTBNo) {
        this.cable_holderSTBNo = cable_holderSTBNo;
    }

    public String getCable_VCNo() {
        return cable_VCNo;
    }

    public void setCable_VCNo(String cable_VCNo) {
        this.cable_VCNo = cable_VCNo;
    }

    public String getCable_holderMobileNo() {
        return cable_holderMobileNo;
    }

    public void setCable_holderMobileNo(String cable_holderMobileNo) {
        this.cable_holderMobileNo = cable_holderMobileNo;
    }

    public String getCable_holderOfficeNo() {
        return cable_holderOfficeNo;
    }

    public void setCable_holderOfficeNo(String cable_holderOfficeNo) {
        this.cable_holderOfficeNo = cable_holderOfficeNo;
    }

    public String getCable_holderAddress() {
        return cable_holderAddress;
    }

    public void setCable_holderAddress(String cable_holderAddress) {
        this.cable_holderAddress = cable_holderAddress;
    }
}
