package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.CoordinatorLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kp.facedetection.fragments.SDRDataSearchFragment;
import com.kp.facedetection.interfaces.OnShowAlertForProceedResult;
import com.kp.facedetection.model.SDRDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kaushik on 20-09-2016.
 */
public class SDRSearchActivity extends BaseActivity implements OnShowAlertForProceedResult,AdapterView.OnItemSelectedListener, View.OnClickListener{

    private EditText et_phoneNo;
    private EditText et_holderName;
    private EditText et_Id_number;
    private Button btn_Search, btn_reset;
    private TextView tv_notAuthorized;
    private RelativeLayout relative_main;
    private RadioGroup radioGroup;

    private String ph_no="";
    private String holder_name="";
    private String id_number="";

    private String pageno="";
    private String totalResult="";
    private String[] keys;
    private String[] values;
    private String matchvalue="1";
    String userId="";
    String devicetoken="";
    String auth_key="";
    Spinner allStateSpinner;

    private List<SDRDetails> sdrDetailsList;
    String [] stateNameList;
    String [] stateIdList;
    String stateId="1";
    TextView chargesheetNotificationCount;
    String notificationCount="";
    private String userName = "";
    private String loginNumber = "";
    private String user_Id = "";
    private String appVersion = "";
    private String log_response="";
    private  String session_id="";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_sdr_data_search);


        initView();

        populateState();
        getAllSearchSecurityCode();
        clickEvents();


    }

    public void populateState(){
        Toast.makeText(this, "populateState called", Toast.LENGTH_SHORT).show();
        try{
            auth_key=Utility.getDocumentSearchSesionId(this);
        }
        catch (Exception e){

        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.MODIFIED_METHOD_SDR_SEARCH_ALL_STATE);
        taskManager.setSDRSearchAllState(true);
        String[] keys = {"user_id","device_type", "device_token","imei","auth_key"};
        String[] values = {userId,"android", devicetoken,Utility.getImiNO(this),auth_key};
        taskManager.doStartTask(keys, values, true, true);

    }

    public void parseSDRSearchAllStateResponse(String response){
        Log.d("DAPL","Response:"+response);
        //Toast.makeText(getActivity(), "Result:"+response, Toast.LENGTH_SHORT).show();

        if(response!=null && !response.equals("")){
            try {
                JSONObject jsonObject = new JSONObject(response);
                if(jsonObject.optString("status").equals("1")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("result");
                    if (jsonArray.length() > 0) {
                        stateNameList=new String[jsonArray.length()];
                        stateIdList=new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObjectState=jsonArray.optJSONObject(i);
                            stateIdList[i]=jsonObjectState.optString("id");
                            stateNameList[i]=jsonObjectState.optString("state");
                        }
                        ArrayAdapter arrayAdapter=new ArrayAdapter(this,android.R.layout.simple_spinner_item,stateNameList);
                        arrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                        allStateSpinner.setOnItemSelectedListener(this);
                        allStateSpinner.setAdapter(arrayAdapter);
                        //allStateSpinner.setBackgroundColor(Color.parseColor("#1307f9"));
                    }
                }

            }
            catch (Exception e){

            }
        }


    }

    public void initView() {

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
            user_Id = Utility.getUserInfo(this).getUserId();

        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        userId= Utility.getUserInfo(this).getUserId();
        allStateSpinner=(Spinner)findViewById(R.id.sp_state);
        et_phoneNo = (EditText)findViewById(R.id.et_phoneNo);
        et_holderName = (EditText)findViewById(R.id.et_holderName);
        et_Id_number = (EditText)findViewById(R.id.et_Id_Number);
        radioGroup = (RadioGroup)findViewById(R.id.rd_Id_Number_option);

        tv_notAuthorized = (TextView)findViewById(R.id.tv_notAuthorized);

        relative_main = (RelativeLayout)findViewById(R.id.relative_main);

        btn_Search = (Button) findViewById(R.id.btn_Search);
        btn_reset = (Button) findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_Search);
        Constants.buttonEffect(btn_reset);

        try {
            if (Utility.getUserInfo(this).getSdr_allow().equalsIgnoreCase("1")) {
                relative_main.setVisibility(View.VISIBLE);
                tv_notAuthorized.setVisibility(View.GONE);
            } else {
                relative_main.setVisibility(View.GONE);
                tv_notAuthorized.setVisibility(View.VISIBLE);
                Constants.changefonts(tv_notAuthorized, this, "Calibri Bold.ttf");
            }
        }catch(NullPointerException e){
            e.printStackTrace();
            relative_main.setVisibility(View.GONE);
            tv_notAuthorized.setVisibility(View.VISIBLE);
            Constants.changefonts(tv_notAuthorized, this, "Calibri Bold.ttf");
        }

    }

    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }

    private void clickEvents(){
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if(checkedId == R.id.rd_exact_match)
                {
                    matchvalue="1";
                }
                if(checkedId == R.id.rd_partial_match)
                {
                    matchvalue="0";
                }
            }
        });
        btn_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ph_no = et_phoneNo.getText().toString().trim();
                holder_name = et_holderName.getText().toString().trim();
                id_number = et_Id_number.getText().toString().trim();

                if (!ph_no.equalsIgnoreCase("") || !holder_name.equalsIgnoreCase("") || !id_number.equalsIgnoreCase("")) {
                    Utility utility = new Utility();
                    utility.setDelegate(SDRSearchActivity.this);
                    Utility.showAlertForProceed(SDRSearchActivity.this);
                } else {

                    showAlertDialog(SDRSearchActivity.this, " Search Error!!! ", "Please provide at least one value for search", false);

                }



            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_phoneNo.setText("");
                et_holderName.setText("");
                et_Id_number.setText("");

            }
        });

    }

    @Override
    public void onShowAlertForProceedResultSuccess(String success) {

         if(!session_id.equalsIgnoreCase("")) {
             fetchSearchResult(ph_no, holder_name);
         }else{
             Utility.showAlertDialog(this,Constants.ERROR,Constants.ERROR_EXCEPTION_MSG,false);
         }
    }

    @Override
    public void onShowAlertForProceedResultError(String error) {

    }
    public void getAllSearchSecurityCode()
    {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_DOCUMENT_SESSION_ID);
        taskManager.setSessionForSDRSearch(true);
//		String devicetoken = PushUtility.fetchC2DMToken(this);
//      String devicetoken = GCMRegistrar.getRegistrationId(this);
        if(FirebaseInstanceId.getInstance().getToken()!=null) {
            devicetoken = FirebaseInstanceId.getInstance().getToken();
        }
        else{
            devicetoken=Utility.getFCMToken(this);
        }

        String[] keys = {"user_id","device_type", "device_token","imei","login_count"};
       /* String[] values = {et_userid.getText().toString().trim(),
                et_password.getText().toString(), "android", devicetoken,Constants.device_IMEI_No};*///Constants.device_IMEI_No
        String[] values = {userId,"android",devicetoken,Utility.getImiNO(this),loginNumber};//Utility.getImiNO(this)
        taskManager.doStartTask(keys, values, true, true);


    }
    public void parseSDRSearchAunthicatedResult(String response){
        if (response != null && !response.equals("")) {
            try {
                JSONObject jObj = new JSONObject(response);
                // L.e("response---------"+response);
                if (jObj.optString("status").equalsIgnoreCase("1")) {
                    if(jObj.optString("session_id")!=null && !jObj.optString("session_id").equalsIgnoreCase("")&& !jObj.optString("session_id").equalsIgnoreCase("null")) {
                        session_id = jObj.optString("session_id");
                        L.e("KP---------"+session_id);
                        //Utility.setDocumentSearchSesionId(SDRSearchActivity.this, session_id);


                    }
                }
                else{
                    Utility.showAlertDialog(this," Search Error ",Constants.ERROR_MSG_TO_RELOAD,false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    private void fetchSearchResult(String phone_no,String name) {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.MODIFIED_METHOD_SDR_SEARCH);
        taskManager.setSDRSearch(true);
        try{
            devicetoken = GCMRegistrar.getRegistrationId(this);
            auth_key=session_id;
        }
        catch (Exception e){

        }

        String[] keys = {"phone_no", "name","pageno","id_no","match_value","user_id","device_type", "device_token","imei","auth_key","state"};

        String[] values = {phone_no.trim(), name.trim(),"1",id_number.trim(),matchvalue.trim(),userId,"android", devicetoken,Utility.getImiNO(this),auth_key,stateId};

        taskManager.doStartTask(keys, values, true,true);


    }

    public void parseSDRSearchResultResponse(String result, String[] keys, String[] values) {

        //System.out.println("parseSDRSearchResultResponse: " + result);

        if(result != null && !result.equals("")){

            try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.opt("status").toString().equalsIgnoreCase("1")){

                    this.keys=keys;
                    this.values=values;
                    pageno=jObj.opt("page").toString();
                    totalResult=jObj.opt("count").toString();

                    JSONArray resultArray=jObj.getJSONArray("result");

                    parseSDRSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(this," Search Error ",jObj.optString("message"),false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                Utility.showToast(this, "Some error has been encountered", "long");
            }
        }

    }

    private void parseSDRSearchResponse(JSONArray resultArray) {

        sdrDetailsList = new ArrayList<SDRDetails>();

        for(int i=0;i<resultArray.length();i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj=resultArray.getJSONObject(i);

                SDRDetails sdrDetails = new SDRDetails();

                String present_address ="";
                String permanent_address = "";

                if(!jsonObj.optString("PROVIDER").equalsIgnoreCase("null") && !jsonObj.optString("PROVIDER").equalsIgnoreCase("")){
                    sdrDetails.setAcct(jsonObj.optString("PROVIDER"));
                }
                if(!jsonObj.optString("ACTIVATIONDATE").equalsIgnoreCase("null") && !jsonObj.optString("ACTIVATIONDATE").equalsIgnoreCase("")){
                    sdrDetails.setActDate(jsonObj.optString("ACTIVATIONDATE"));
                }
                if(!jsonObj.optString("ID_NUMBER").equalsIgnoreCase("null") && !jsonObj.optString("ID_NUMBER").equalsIgnoreCase("")){
                    sdrDetails.setId_Number(jsonObj.optString("ID_NUMBER"));
                }
                if(!jsonObj.optString("ID_TYPE").equalsIgnoreCase("null") && !jsonObj.optString("ID_TYPE").equalsIgnoreCase("")){
                    sdrDetails.setId_type(jsonObj.optString("ID_TYPE"));
                }
                if(!jsonObj.optString("MOBNO").equalsIgnoreCase("null") && !jsonObj.optString("MOBNO").equalsIgnoreCase("")){
                    sdrDetails.setMobNo(jsonObj.optString("MOBNO"));
                }

                if(!jsonObj.optString("ALTERNATEPHNUMBER").equalsIgnoreCase("null") && !jsonObj.optString("ALTERNATEPHNUMBER").equalsIgnoreCase("")){
                    sdrDetails.setPhone_no(jsonObj.optString("ALTERNATEPHNUMBER"));
                }

                if(!jsonObj.optString("NAME").equalsIgnoreCase("null") && !jsonObj.optString("NAME").equalsIgnoreCase("")){
                    sdrDetails.setName(jsonObj.optString("NAME"));
                }
                if(!jsonObj.optString("ADDRESS").equalsIgnoreCase("null") && !jsonObj.optString("ADDRESS").equalsIgnoreCase("")){
                    sdrDetails.setPresent_addres(jsonObj.optString("ADDRESS"));
                }

                if(!jsonObj.optString("PINCODE").equalsIgnoreCase("null") && !jsonObj.optString("PINCODE").equalsIgnoreCase("")){
                    sdrDetails.setPresent_pincode(jsonObj.optString("PINCODE"));
                }
                if(!jsonObj.optString("CITY").equalsIgnoreCase("null") && !jsonObj.optString("CITY").equalsIgnoreCase("")){
                    sdrDetails.setPresent_city(jsonObj.optString("CITY"));
                }
                if(!jsonObj.optString("STATE").equalsIgnoreCase("null") && !jsonObj.optString("STATE").equalsIgnoreCase("")){
                    sdrDetails.setPresent_state(jsonObj.optString("STATE"));
                }

               /* if(!jsonObj.optString("ADD1").equalsIgnoreCase("null") && !jsonObj.optString("ADD1").equalsIgnoreCase("")){
                    present_address = present_address + jsonObj.optString("ADD1");
                }
                if(!jsonObj.optString("ADD2").equalsIgnoreCase("null") && !jsonObj.optString("ADD2").equalsIgnoreCase("")){
                    present_address = present_address + ", " + jsonObj.optString("ADD2");
                }
                if(!jsonObj.optString("ADD3").equalsIgnoreCase("null") && !jsonObj.optString("ADD3").equalsIgnoreCase("")){
                    present_address = present_address + ", " + jsonObj.optString("ADD3");
                }

                sdrDetails.setPresent_addres(present_address);
*/

               /* if(!jsonObj.optString("OSADD1").equalsIgnoreCase("null") && !jsonObj.optString("OSADD1").equalsIgnoreCase("")){
                    permanent_address = permanent_address + jsonObj.optString("OSADD1");
                }
                if(!jsonObj.optString("OSADD2").equalsIgnoreCase("null") && !jsonObj.optString("OSADD2").equalsIgnoreCase("")){
                    permanent_address = permanent_address + ", " + jsonObj.optString("OSADD2");
                }
                if(!jsonObj.optString("OSADD3").equalsIgnoreCase("null") && !jsonObj.optString("OSADD3").equalsIgnoreCase("")){
                    permanent_address = permanent_address + ", " + jsonObj.optString("OSADD3");
                }

                sdrDetails.setPermanent_address(permanent_address);*/

               /* if(!jsonObj.optString("PIN1").equalsIgnoreCase("null") && !jsonObj.optString("PIN1").equalsIgnoreCase("")){
                    sdrDetails.setPresent_pincode(jsonObj.optString("PIN1"));
                }
                if(!jsonObj.optString("PIN2").equalsIgnoreCase("null") && !jsonObj.optString("PIN2").equalsIgnoreCase("")){
                    sdrDetails.setPermanent_pincode(jsonObj.optString("PIN2"));
                }
                if(!jsonObj.optString("ACTDATE").equalsIgnoreCase("null") && !jsonObj.optString("ACTDATE").equalsIgnoreCase("")){
                    sdrDetails.setActDate(jsonObj.optString("ACTDATE"));
                }
                if(!jsonObj.optString("CITY1").equalsIgnoreCase("null") && !jsonObj.optString("CITY1").equalsIgnoreCase("")){
                    sdrDetails.setPresent_city(jsonObj.optString("CITY1"));
                }
                if(!jsonObj.optString("CITY2").equalsIgnoreCase("null") && !jsonObj.optString("CITY2").equalsIgnoreCase("")){
                    sdrDetails.setPermanent_city(jsonObj.optString("CITY2"));
                }
                if(!jsonObj.optString("STATE1").equalsIgnoreCase("null") && !jsonObj.optString("STATE1").equalsIgnoreCase("")){
                    sdrDetails.setPresent_state(jsonObj.optString("STATE1"));
                }
                if(!jsonObj.optString("STATE2").equalsIgnoreCase("null") && !jsonObj.optString("STATE2").equalsIgnoreCase("")){
                    sdrDetails.setPermanent_state(jsonObj.optString("STATE2"));
                }
*/
                sdrDetailsList.add(sdrDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Intent intent=new Intent(this, SDRSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putExtra("SDR_SEARCH_LIST", (Serializable) sdrDetailsList);
        startActivity(intent);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        stateId=stateIdList[position];
        ((TextView) allStateSpinner.getSelectedView()).setTextColor(getResources().getColor(R.color.color_deep_blue));

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {

    }
}
