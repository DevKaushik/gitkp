package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kp.facedetection.fragments.DashboardArrestOthersFragment;
import com.kp.facedetection.fragments.DashboardArrestSpecificFragment;

import java.util.HashMap;

/**
 * Created by DAT-Asset-131 on 10-09-2016.
 */
public class DashboardArrestPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    Context context;
    private HashMap<Integer,Fragment> fragmentHashMap = new HashMap<>();

    public DashboardArrestPagerAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;
        switch (position) {

            case 0: // This will show FirstFragment
                frag = DashboardArrestSpecificFragment.newInstance();
                break;

            case 1: // This will show SecondFragment
                frag = DashboardArrestOthersFragment.newInstance();
                break;
        }
        fragmentHashMap.put(position,frag);
        return frag;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return null;
    }

    public HashMap<Integer, Fragment> getFragmentList(){
        return fragmentHashMap;
    }
}
