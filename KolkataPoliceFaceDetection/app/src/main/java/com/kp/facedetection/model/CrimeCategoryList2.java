package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-Asset-145 on 06-06-2016.
 */
public class CrimeCategoryList2 implements Serializable {

    private String category = "";
    private  boolean selected = false;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
