package com.kp.facedetection.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;


/**
 * Created by DAT-Asset-131 on 14-09-2016.
 */
public class CustomFIRStatusAdapter extends BaseAdapter {
    Context context;
    String[] FIRstatus;
    LayoutInflater inflter;

    public CustomFIRStatusAdapter(Context applicationContext, String[] FIRstatus) {
        this.context = applicationContext;
        this.FIRstatus=FIRstatus;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return FIRstatus.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_spinner_item, null);

        TextView statusName = (TextView) view.findViewById(R.id.tv_itemName);

        statusName.setText(FIRstatus[i]);
        return view;
    }
}
