package com.kp.facedetection.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by DAT-Asset-131 on 17-06-2016.
 */
public class CustomFooterAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private ViewHolder holder;
    private List<String> footerListData;

    public CustomFooterAdapter(Context c,List<String> footerListData) {

        context = c;
        this.footerListData = footerListData;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return footerListData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_footer_item,
                    parent, false);

            holder = new ViewHolder();

            holder.tv_footer_item_value = (TextView) convertView.findViewById(R.id.tv_footer_item_value);



            convertView.setTag(holder);

        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_footer_item_value.setText(footerListData.get(position));
        Constants.changefonts(holder.tv_footer_item_value, context, "Calibri Bold.ttf");

        return convertView;
    }

    class ViewHolder {

        TextView tv_footer_item_value;


    }
}
