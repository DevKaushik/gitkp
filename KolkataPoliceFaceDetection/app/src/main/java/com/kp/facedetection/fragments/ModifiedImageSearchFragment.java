package com.kp.facedetection.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.kp.facedetection.BaseActivity;
import com.kp.facedetection.R;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ModifiedImageSearchFragment extends Fragment {

    private Button btn_select_image;
    private ImageView iv_select_image;

    private String capturedImgFileName = "";
    private Bitmap userImageBitmap = null;
    private String path = "";


    public ModifiedImageSearchFragment() {
        // Required empty public constructor
    }

    public static ModifiedImageSearchFragment newInstance() {
        ModifiedImageSearchFragment modifiedImageSearchFragment = new ModifiedImageSearchFragment();

        return modifiedImageSearchFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_modified_image_search, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btn_select_image = (Button) view.findViewById(R.id.btn_select_image);
        iv_select_image = (ImageView) view.findViewById(R.id.iv_select_image);

        Constants.buttonEffect(btn_select_image);

        clickEvents();

    }

    private void clickEvents(){

        btn_select_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImagePermission();
            }
        });

    }


    /*
   * Checking for marshmallow Image permission
   * */
    private void uploadImagePermission() {
        int hasStorageAccess = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasCameraAccess = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);

        if (hasCameraAccess == PackageManager.PERMISSION_GRANTED && hasStorageAccess == PackageManager.PERMISSION_GRANTED) {
            uploadImageDialog();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Constants.PERMISSION_REQUEST);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Constants.PERMISSION_REQUEST:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    uploadImageDialog();
                } else {
                    uploadImagePermission();
                }
                break;
        }
    }

    public void uploadImageDialog() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    capturedImgFileName = FileUtils.getFileName();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    Uri fileUri = FileUtils.getOutputMediaFileUri(Constants.MEDIA_TYPE_IMAGE, capturedImgFileName);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                    startActivityForResult(intent, Constants.CAPTURE_INTENT_CALLED);
                } else if (options[item].equals("Choose from Gallery")) {
                    if (Build.VERSION.SDK_INT < 19) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constants.GALLERY_INTENT_CALLED);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("image/*");
                        startActivityForResult(intent, Constants.GALLERY_KITKAT_INTENT_CALLED);
                    }

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        int reqCode = 0;
        if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(getActivity(), "You have canceled", Toast.LENGTH_LONG).show();
        } else {
            Uri selectedImageUri = null;
            int rotation = 0;
            switch (requestCode) {
                case Constants.CAPTURE_INTENT_CALLED:
                    reqCode = Constants.CAPTURE_INTENT_CALLED;
                    File img = FileUtils.getOutputMediaFile(Constants.MEDIA_TYPE_IMAGE, capturedImgFileName);
                    Log.e("SIZE",""+img.length());
                    userImageBitmap = FileUtils.decodeSampledBitmap(img.getAbsolutePath(), 500, 500);
                    path = img.getAbsolutePath();
                    Log.e("CAPTURE_INTENT_CALLED", "" + path);
                    capturedImgFileName = "";
                    break;

                case Constants.GALLERY_INTENT_CALLED:
                    reqCode = Constants.GALLERY_INTENT_CALLED;
                    selectedImageUri = data.getData();
                    try {
                        InputStream input = getActivity().getContentResolver().openInputStream(selectedImageUri);
                        userImageBitmap = BitmapFactory.decodeStream(input);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    break;

                case Constants.GALLERY_KITKAT_INTENT_CALLED:
                    reqCode = Constants.GALLERY_KITKAT_INTENT_CALLED;
                    selectedImageUri = data.getData();
                    Log.e("GALLERY_KITKAT", "" + selectedImageUri);
                    try {
                        InputStream input = getActivity().getContentResolver().openInputStream(selectedImageUri);
                        userImageBitmap = BitmapFactory.decodeStream(input);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }

            if (reqCode != Constants.CAPTURE_INTENT_CALLED) {
                iv_select_image.setImageBitmap(userImageBitmap);
                return;
            }

            try {

                /////for rotation of image
                ExifInterface exif = new ExifInterface(path);
                rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

                Matrix matrix = new Matrix();
                matrix.postRotate(FileUtils.getOrientation(rotation));
                userImageBitmap = Bitmap.createBitmap(userImageBitmap, 0, 0, userImageBitmap.getWidth(), userImageBitmap.getHeight(), matrix, true);
                Log.e("tempBitmap", userImageBitmap.getWidth() + " " + userImageBitmap.getHeight());


                if (userImageBitmap.getHeight() > 1440 || userImageBitmap.getWidth() > 1280) {

                    System.out.println("imageResult dimen " + userImageBitmap.getWidth() + " " + userImageBitmap.getHeight());
                    userImageBitmap = FileUtils.decodeSampledBitmap(path, 720, 1280);
                    System.out.println("imageResult dimen AFTER " + userImageBitmap.getWidth() + " " + userImageBitmap.getHeight());

                    if (userImageBitmap.getWidth() > userImageBitmap.getHeight()) {
                        userImageBitmap = FileUtils.rotate(userImageBitmap, 90);
                        System.out.println("imageResult dimen AFTER Rotate " + userImageBitmap.getWidth() + " " + userImageBitmap.getHeight());
                    } else {
                        Log.e("tempBitmap ", "ModifiedImageSearchFragment- " + "width < height");
                    }

                } else {
                    Log.e("tempBitmap ", " ModifiedImageSearchFragment- " + "width < 1280 - height < 1440");
                }

                iv_select_image.setImageBitmap(userImageBitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
