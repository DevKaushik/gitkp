package com.kp.facedetection;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.GlobalSearch.activity.GlobalCableSearchDetailsActivity;
import com.kp.facedetection.GlobalSearch.activity.GlobalDLSearchDetailsActivity;
import com.kp.facedetection.GlobalSearch.activity.GlobalGasSearchDetailsActivity;
import com.kp.facedetection.GlobalSearch.activity.GlobalKMCSearchDetailsActivity;
import com.kp.facedetection.GlobalSearch.activity.GlobalSDRSearchDetailsActivity;
import com.kp.facedetection.GlobalSearch.activity.GlobalVehicleSearchDetailsActivity;
import com.kp.facedetection.GlobalSearch.model.GlobalDLSearchDetails;
import com.kp.facedetection.GlobalSearch.model.GlobalVehicleSearchDetails;
import com.kp.facedetection.adapter.AllDocumentSearchTypeAdapter;
import com.kp.facedetection.adapter.CableSearchAdapterModified;
import com.kp.facedetection.adapter.DLSearchAdapterModified;
import com.kp.facedetection.adapter.GasSearchAdapterModified;
import com.kp.facedetection.adapter.KMCSearchAdapterModified;
import com.kp.facedetection.adapter.SDRSearchAdapterModified;
import com.kp.facedetection.adapter.VehicleSearchAdapterModified;
import com.kp.facedetection.adapter.WarrentFailDueAdapter;
import com.kp.facedetection.interfaces.OnAllDocumentSearchTypeListener;
import com.kp.facedetection.interfaces.OnAllDoumentSearchValueListener;
import com.kp.facedetection.model.AllDocumentSearchType;
import com.kp.facedetection.model.CableSearchDetails;
import com.kp.facedetection.model.CrimeReviewDetails;
import com.kp.facedetection.model.DLSearchDetails;
import com.kp.facedetection.model.KMCSearchDetails;
import com.kp.facedetection.model.ModifiedDocumentGasDetails;
import com.kp.facedetection.model.SDRDetails;
import com.kp.facedetection.model.VehicleSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//
//
// import static android.support.v7.mediarouter.R.id.scrollView;

public class AllDocumentDataSearchResultActivity extends BaseActivity implements View.OnClickListener, OnAllDocumentSearchTypeListener,OnAllDoumentSearchValueListener {
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private RecyclerView recycler_allDocumentSearchList;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<AllDocumentSearchType> allDocumentSearchTypeList = new ArrayList<>();
    private AllDocumentSearchTypeAdapter allDocumentSearchTypeAdapter;

    private ListView alldataSearchListview;
    JSONObject jsonObjectResult;

    private DLSearchAdapterModified dlSearchAdapter;
    private VehicleSearchAdapterModified vehicleSearchAdapter;
    private SDRSearchAdapterModified sdrSearchAdapter;
    private CableSearchAdapterModified cableSearchAdapter;
    private KMCSearchAdapterModified kmcSearchAdapter;
    private GasSearchAdapterModified gasSearchAdapter;

    private List<DLSearchDetails> dlSearchDeatilsList = new ArrayList<>();
    private List<VehicleSearchDetails> vehicleSearchDetailsList = new ArrayList<>();
    private List<SDRDetails> sdrDetailsList = new ArrayList<>();
    private List<CableSearchDetails> cableSearchDeatilsList = new ArrayList<>();
    private List<KMCSearchDetails> kmcSearchDeatilsList = new ArrayList<>();
    private List<ModifiedDocumentGasDetails> gasSearchDeatilsList = new ArrayList<>();

    TextView chargesheetNotificationCount;
    String notificationCount = "";
    String selectedSearchType = "";

    String pageNoDL = "";
    String pageNoVechile = "";
    String pageNoSDR = "";
    String pageNoCable = "";
    String pageNoKMC = "";
    String pageNogas = "";

    String totalcountDL = "";
    String totalcountVechile = "";
    String totalcountSDR = "";
    String totalcountCable = "";
    String totalcountKMC = "";
    String totalcountgas = "";
    private String[] keys;
    private String[] values;
    private String pageno = "1";
    private String totalResult = "";
    private String searchType = "";

    private TextView tv_resultCount,tv_search_type;
    private Button btnLoadMore;
    TextView tv_load_more;
    EditText et_list_search;


    boolean userScrolled = false;
    int firstVisiblePosition;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_document_data_search_result);
        getIntendedData();
        setToolBar();
        parseAllDocumentSearchResult();
        initViews();
        clickEvents();
        implementScrollListener();
    }

    private void getIntendedData() {
        allDocumentSearchTypeList = (ArrayList<AllDocumentSearchType>) getIntent().getSerializableExtra("ALL_DOCUMENT_SEARCH_TYPE");
        selectedSearchType = allDocumentSearchTypeList.get(0).getName();
        keys = getIntent().getStringArrayExtra("keys");
        values = getIntent().getStringArrayExtra("value");


        try {
            jsonObjectResult = new JSONObject(getIntent().getStringExtra("ALL_DOCUMENT_SEARCH_LIST"));
            Log.e("JAY", String.valueOf(jsonObjectResult));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout) mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this, charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount = (TextView) mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount = Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }

    public void parseAllDocumentSearchResult() {
        if (jsonObjectResult.has("1")) {
            try {
                JSONObject jsonObjectDL = jsonObjectResult.optJSONObject("1");
                if (jsonObjectDL.optString("status").equalsIgnoreCase("1")) {
                    JSONArray jsonArrayDL = jsonObjectDL.optJSONArray("result");
                    if (jsonArrayDL.length() > 0) {
                        pageNoDL = jsonObjectDL.optString("page");
                        totalcountDL = jsonObjectDL.optString("count");
                        setDLSearchArrayList(jsonArrayDL);
                    }
                }
            } catch (Exception e) {

            }
        }
        if (jsonObjectResult.has("2")) {
            try {
                JSONObject jsonObjectVehicle = jsonObjectResult.getJSONObject("2");
                if (jsonObjectVehicle.optString("status").equalsIgnoreCase("1")) {
                    JSONArray jsonArrayVehicle = jsonObjectVehicle.optJSONArray("result");
                    if (jsonArrayVehicle.length() > 0) {
                        pageNoVechile = jsonObjectVehicle.optString("page");
                        totalcountVechile = jsonObjectVehicle.optString("count");
                        setVehicleSearchArrayList(jsonArrayVehicle);
                    }
                }
            } catch (Exception e) {

            }
        }
        if (jsonObjectResult.has("3")) {
            try {
                JSONObject jsonObjectSDR = jsonObjectResult.getJSONObject("3");
                if (jsonObjectSDR.optString("status").equalsIgnoreCase("1")) {
                    JSONArray jsonArraySDR = jsonObjectSDR.optJSONArray("result");
                    if (jsonArraySDR.length() > 0) {
                        pageNoSDR = jsonObjectSDR.optString("page");
                        totalcountSDR = jsonObjectSDR.optString("count");
                        setSDRSearchArrayList(jsonArraySDR);
                    }

                }
            } catch (Exception e) {

            }
        }
        if (jsonObjectResult.has("4")) {
            try {
                JSONObject jsonObjectCable = jsonObjectResult.getJSONObject("4");
                if (jsonObjectCable.optString("status").equalsIgnoreCase("1")) {
                    JSONArray jsonArrayCable = jsonObjectCable.optJSONArray("result");
                    if (jsonArrayCable.length() > 0) {
                        pageNoCable = jsonObjectCable.optString("page");
                        totalcountCable = jsonObjectCable.optString("count");
                        setCableSearchArrayList(jsonArrayCable);
                    }
                }
            } catch (Exception e) {

            }
        }
        if (jsonObjectResult.has("5")) {
            try {
                JSONObject jsonObjectKMC = jsonObjectResult.getJSONObject("5");
                if (jsonObjectKMC.optString("status").equalsIgnoreCase("1")) {
                    JSONArray jsonArrayKMC = jsonObjectKMC.optJSONArray("result");
                    if (jsonArrayKMC.length() > 0) {
                        pageNoKMC = jsonObjectKMC.optString("page");
                        totalcountKMC = jsonObjectKMC.optString("count");
                        setKMCSearchArrayList(jsonArrayKMC);
                    }

                }
            } catch (Exception e) {

            }
        }
        if (jsonObjectResult.has("6")) {
            try {
                JSONObject jsonObjectGas = jsonObjectResult.getJSONObject("6");
                if (jsonObjectGas.optString("status").equalsIgnoreCase("1")) {
                    JSONArray jsonArrayGas = jsonObjectGas.optJSONArray("result");
                    if (jsonArrayGas.length() > 0) {
                        pageNogas = jsonObjectGas.optString("page");
                        totalcountgas = jsonObjectGas.optString("count");
                        setGasSearchArrayList(jsonArrayGas);
                    }
                }
            } catch (Exception e) {

            }

        }

    }

    private void initViews() {

        recycler_allDocumentSearchList = (RecyclerView) findViewById(R.id.recv_allDocumentSearchType);
        tv_resultCount = (TextView) findViewById(R.id.tv_resultCount);
        tv_search_type = (TextView) findViewById(R.id.tv_search_type);
        tv_load_more = (TextView) findViewById(R.id.tv_load_more);
        et_list_search=(EditText) findViewById(R.id.et_list_search);
        Constants.changefonts(tv_load_more, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_search_type, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");


        // LoadMore button
        /*btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");
*/
        mLayoutManager = new LinearLayoutManager(this);
        recycler_allDocumentSearchList.setLayoutManager(mLayoutManager);
        //recycler_psList.setItemAnimator(new DefaultItemAnimator());
        recycler_allDocumentSearchList.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
        allDocumentSearchTypeAdapter = new AllDocumentSearchTypeAdapter(this, allDocumentSearchTypeList);
        allDocumentSearchTypeAdapter.setOnclickListener(this);
        recycler_allDocumentSearchList.setAdapter(allDocumentSearchTypeAdapter);

        alldataSearchListview = (ListView) findViewById(R.id.recv_allDocumentSearchList);

        if (allDocumentSearchTypeList.get(0).getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[0])) {
            dlSearchAdapter = new DLSearchAdapterModified(this, dlSearchDeatilsList);
            dlSearchAdapter.setOnAllDocumentSearchValue(this);
            alldataSearchListview.setAdapter(dlSearchAdapter);
            dlSearchAdapter.notifyDataSetChanged();

        } else if (allDocumentSearchTypeList.get(0).getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[1])) {
            vehicleSearchAdapter = new VehicleSearchAdapterModified(this, vehicleSearchDetailsList);
            vehicleSearchAdapter.setOnAllDocumentSearchValue(this);
            alldataSearchListview.setAdapter(vehicleSearchAdapter);
            vehicleSearchAdapter.notifyDataSetChanged();
        } else if (allDocumentSearchTypeList.get(0).getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[2])) {
            sdrSearchAdapter = new SDRSearchAdapterModified(this, sdrDetailsList);
            sdrSearchAdapter.setOnAllDocumentSearchValue(this);
            alldataSearchListview.setAdapter(sdrSearchAdapter);
            sdrSearchAdapter.notifyDataSetChanged();
        } else if (allDocumentSearchTypeList.get(0).getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[3])) {
            cableSearchAdapter = new CableSearchAdapterModified(this, cableSearchDeatilsList);
            cableSearchAdapter.setOnAllDocumentSearchValue(this);
            alldataSearchListview.setAdapter(cableSearchAdapter);
            cableSearchAdapter.notifyDataSetChanged();
        } else if (allDocumentSearchTypeList.get(0).getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[4])) {
            kmcSearchAdapter = new KMCSearchAdapterModified(this, kmcSearchDeatilsList);
            kmcSearchAdapter.setOnAllDocumentSearchValue(this);
            alldataSearchListview.setAdapter(kmcSearchAdapter);
            kmcSearchAdapter.notifyDataSetChanged();
        } else if (allDocumentSearchTypeList.get(0).getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[5])) {
            gasSearchAdapter = new GasSearchAdapterModified(this, gasSearchDeatilsList);
            gasSearchAdapter.setOnAllDocumentSearchValue(this);
            alldataSearchListview.setAdapter(gasSearchAdapter);
            gasSearchAdapter.notifyDataSetChanged();
        }

        setPageNoTotalCountLoarmore();
       /* if (Integer.parseInt(totalResult) > 20) {

            // Adding Load More button to listview at bottom
            //   alldataSearchListview.addFooterView(btnLoadMore);
            tv_load_more.setVisibility(View.VISIBLE);


        }*/
       //
        // alldataSearchListview.setOnItemClickListener(this);
        et_list_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[0])) {
                    dlSearchAdapter.filter(s.toString());
                } else if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[1])) {
                    vehicleSearchAdapter.filter(s.toString());
                } else if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[2])) {
                    sdrSearchAdapter.filter(s.toString());
                } else if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[3])) {
                     cableSearchAdapter.filter(s.toString());
                } else if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[4])) {
                       kmcSearchAdapter.filter(s.toString());
                } else if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[5])) {
                      gasSearchAdapter.filter(s.toString());
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        // allDocumentSearchTypeAdapter.setClickListener(this);
        // psListAdapter.setClickListenerWarrentFail(this);
        // psListAdapter.setClickListenerWarrentDue(this);
    }

    private void setPageNoTotalCountLoarmore() {
        if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[0])) {
            pageno = pageNoDL;
            totalResult = totalcountDL;
        } else if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[1])) {
            pageno = pageNoVechile;
            totalResult = totalcountVechile;
        } else if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[2])) {
            pageno = pageNoSDR;
            totalResult = totalcountSDR;
        } else if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[3])) {
            pageno=pageNoCable;
            totalResult = totalcountCable;
        } else if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[4])) {
             pageno=pageNoKMC;
            totalResult = totalcountKMC;
        } else if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[5])) {
            pageno=pageNogas;
            totalResult = totalcountgas;
        }
        tv_search_type.setText(selectedSearchType);
        //Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");
        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }

    }

    private void clickEvents() {

       /* btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Starting a new async task
                fetchAllSearchResultPagination();
            }
        });*/
      /*  tv_load_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchAllSearchResultPagination();
            }
        });
*/

    }
    private  void implementScrollListener(){
        alldataSearchListview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                  if(scrollState== AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                      userScrolled=true;
                  }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if(userScrolled && (firstVisibleItem + visibleItemCount == totalItemCount)){
                    if(totalItemCount!=Integer.parseInt(totalResult)) {
                        userScrolled = false;
                        firstVisiblePosition=firstVisibleItem;
                        fetchAllSearchResultPagination();
                        /*InputMethodManager imm = (InputMethodManager) AllDocumentDataSearchResultActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm.isAcceptingText()) {


                        } else {


                        }*/

                    }
                }
            }
        });
    }

    private void fetchAllSearchResultPagination() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_DOCUMENT_SEARCH);
        taskManager.setDocumentAllDataSearchPagination(true);
        values[3] = (Arrays.asList(Constants.ALL_DOCUMENT_SEARCH_TYPE).indexOf(selectedSearchType) + 1) + "";
        values[4] = (Integer.parseInt(pageno) + 1) + "";
        taskManager.doStartTask(keys, values, true, true);

    }

    public void parseAllDocumentDataSearchPaginationResponse(String result, String[] keys, String[] values) {
        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    //pageno=jObj.opt("pageno").toString();
                    // totalResult=jObj.opt("count").toString();
                    JSONObject resultObject = jObj.optJSONObject("result");
                    parseAllDocumentSearchPaginationResponse(resultObject);

                } else {
                   // Utility.showAlertDialog(this, " Search Error ", jObj.optString("message"), false);
                }


            } catch (JSONException e) {

                e.printStackTrace();
                Utility.showAlertDialog(this, " Search Error!!! ", "Some error has been encountered", false);
            }
        }
    }

    public void parseAllDocumentSearchPaginationResponse(JSONObject jsonObjectResult) {
        if (jsonObjectResult.has("1")) {
            try {
                JSONObject jsonObjectDL = jsonObjectResult.optJSONObject("1");
                if (jsonObjectDL.optString("status").equalsIgnoreCase("1")) {
                    JSONArray jsonArrayDL = jsonObjectDL.optJSONArray("result");
                    if (jsonArrayDL.length() > 0) {
                        pageNoDL = jsonObjectDL.optString("page");
                        //totalcountDL=jsonObjectDL.optString("count");
                        setDLSearchArrayList(jsonArrayDL);
                        dlSearchAdapter = new DLSearchAdapterModified(this, dlSearchDeatilsList);
                        dlSearchAdapter.setOnAllDocumentSearchValue(this);
                        alldataSearchListview.setAdapter(dlSearchAdapter);
                        dlSearchAdapter.notifyDataSetChanged();


                    }
                }
            } catch (Exception e) {

            }
        }
        if (jsonObjectResult.has("2")) {
            try {
                JSONObject jsonObjectVehicle = jsonObjectResult.getJSONObject("2");
                if (jsonObjectVehicle.optString("status").equalsIgnoreCase("1")) {
                    JSONArray jsonArrayVehicle = jsonObjectVehicle.optJSONArray("result");
                    if (jsonArrayVehicle.length() > 0) {
                        pageNoVechile = jsonObjectVehicle.optString("page");
                        // totalcountVechile=jsonObjectVehicle.optString("count");
                        setVehicleSearchArrayList(jsonArrayVehicle);
                        vehicleSearchAdapter = new VehicleSearchAdapterModified(this, vehicleSearchDetailsList);
                        vehicleSearchAdapter.setOnAllDocumentSearchValue(this);
                        alldataSearchListview.setAdapter(vehicleSearchAdapter);
                        vehicleSearchAdapter.notifyDataSetChanged();
                    }
                }
            } catch (Exception e) {

            }
        }
        if (jsonObjectResult.has("3")) {
            try {
                JSONObject jsonObjectSDR = jsonObjectResult.getJSONObject("3");
                if (jsonObjectSDR.optString("status").equalsIgnoreCase("1")) {
                    JSONArray jsonArraySDR = jsonObjectSDR.optJSONArray("result");
                    if (jsonArraySDR.length() > 0) {
                        pageNoSDR = jsonObjectSDR.optString("page");
                        setSDRSearchArrayList(jsonArraySDR);
                        sdrSearchAdapter = new SDRSearchAdapterModified(this, sdrDetailsList);
                        sdrSearchAdapter.setOnAllDocumentSearchValue(this);
                        alldataSearchListview.setAdapter(sdrSearchAdapter);
                        sdrSearchAdapter.notifyDataSetChanged();

                    }

                }
            } catch (Exception e) {

            }
        }
        if (jsonObjectResult.has("4")) {
            try {
                JSONObject jsonObjectCable = jsonObjectResult.getJSONObject("4");
                if (jsonObjectCable.optString("status").equalsIgnoreCase("1")) {
                    JSONArray jsonArrayCable = jsonObjectCable.optJSONArray("result");
                    if (jsonArrayCable.length() > 0) {
                        pageNoCable=jsonObjectCable.optString("page");
                        setCableSearchArrayList(jsonArrayCable);
                        cableSearchAdapter = new CableSearchAdapterModified(this, cableSearchDeatilsList);
                        cableSearchAdapter.setOnAllDocumentSearchValue(this);
                        alldataSearchListview.setAdapter(cableSearchAdapter);
                        cableSearchAdapter.notifyDataSetChanged();
                    }

                }
            } catch (Exception e) {

            }
        }
        if (jsonObjectResult.has("5")) {
            try {
                JSONObject jsonObjectKMC = jsonObjectResult.getJSONObject("5");
                if (jsonObjectKMC.optString("status").equalsIgnoreCase("1")) {
                    JSONArray jsonArrayKMC=jsonObjectKMC.optJSONArray("result");
                    if(jsonArrayKMC.length()>0){
                        pageNoKMC=jsonObjectKMC.optString("page");
                        setKMCSearchArrayList(jsonArrayKMC);
                        kmcSearchAdapter = new KMCSearchAdapterModified(this, kmcSearchDeatilsList);
                        kmcSearchAdapter.setOnAllDocumentSearchValue(this);
                        alldataSearchListview.setAdapter(kmcSearchAdapter);
                        kmcSearchAdapter.notifyDataSetChanged();

                    }

                }
            } catch (Exception e) {

            }
        }
        if (jsonObjectResult.has("6")) {
            try {
                JSONObject jsonObjectGas = jsonObjectResult.getJSONObject("6");
                if (jsonObjectGas.optString("status").equalsIgnoreCase("1")) {
                    JSONArray jsonArrayGas=jsonObjectGas.optJSONArray("result");
                    if(jsonArrayGas.length()>0){
                        pageNogas=jsonObjectGas.optString("page");
                        setGasSearchArrayList(jsonArrayGas);
                        gasSearchAdapter = new GasSearchAdapterModified(this, gasSearchDeatilsList);
                        gasSearchAdapter.setOnAllDocumentSearchValue(this);
                        alldataSearchListview.setAdapter(gasSearchAdapter);
                        gasSearchAdapter.notifyDataSetChanged();
                    }

                }
            } catch (Exception e) {

            }

        }
        setPageNoTotalCountLoarmore();
        int currentPosition = alldataSearchListview.getLastVisiblePosition();
        //Toast.makeText(this, "LastVisiblePosition:--"+ currentPosition, Toast.LENGTH_SHORT).show();
        //
        // Toast.makeText(this, "FirstVisiblePosition:--"+ firstVisiblePosition, Toast.LENGTH_SHORT).show();
        // Setting new scroll position
        alldataSearchListview.setSelectionFromTop(firstVisiblePosition + 5, 0);

      //  showHideLoadmore();



       /* if (Integer.parseInt(totalResult) > 20) {

            // Adding Load More button to listview at bottom
           // alldataSearchListview.addFooterView(btnLoadMore);
            tv_load_more.setVisibility(View.VISIBLE);

        }
        int currentPosition = alldataSearchListview.getFirstVisiblePosition();
        int lastVisiblePosition = alldataSearchListview.getLastVisiblePosition();

        // Setting new scroll position
        alldataSearchListview.setSelectionFromTop(currentPosition + 1, 0);

        if((Integer.parseInt(totalResult)-lastVisiblePosition)<20){
           // alldataSearchListview.removeFooterView(btnLoadMore);
            tv_load_more.setVisibility(View.GONE);
        }
*/

    }

    public void showHideLoadmore() {
        if (Integer.parseInt(totalResult) > (Integer.parseInt(pageno) * 20)) {
            tv_load_more.setVisibility(View.VISIBLE);
        } else {
            tv_load_more.setVisibility(View.GONE);
        }
    }

    public void setDLSearchArrayList(JSONArray resultArray) {
        for (int i = 0; i < resultArray.length(); i++) {


            JSONObject jsonObj = null;

            try {
                jsonObj = resultArray.getJSONObject(i);

                DLSearchDetails dlSearchDetails = new DLSearchDetails();

                String dlpa_address = "";
                String dlta_address = "";

                if (!jsonObj.optString("DLNO").equalsIgnoreCase("null") && !jsonObj.optString("DLNO").equalsIgnoreCase("")) {
                    dlSearchDetails.setDl_No(jsonObj.optString("DLNO"));
                }
                else {
                    dlSearchDetails.setDl_No("");
                }

                if (!jsonObj.optString("DLNAME").equalsIgnoreCase("null") && !jsonObj.optString("DLNAME").equalsIgnoreCase("")) {
                    dlSearchDetails.setDl_Name(jsonObj.optString("DLNAME"));
                }

                if (!jsonObj.optString("DLSWDOF").equalsIgnoreCase("null") && !jsonObj.optString("DLSWDOF").equalsIgnoreCase("")) {
                    dlSearchDetails.setDl_SwdOf(jsonObj.optString("DLSWDOF"));
                }

                if (!jsonObj.optString("DLDOB").equalsIgnoreCase("null") && !jsonObj.optString("DLDOB").equalsIgnoreCase("")) {
                    dlSearchDetails.setDl_Dob(jsonObj.optString("DLDOB"));
                }

                if (!jsonObj.optString("DLSEX").equalsIgnoreCase("null")  && !jsonObj.optString("DLSEX").equalsIgnoreCase("")) {
                    dlSearchDetails.setDl_Sex(jsonObj.optString("DLSEX"));
                }

                if (!jsonObj.optString("DLPADD1").equalsIgnoreCase("null") && !jsonObj.optString("DLPADD1").equalsIgnoreCase("")) {
                    dlpa_address = dlpa_address + jsonObj.optString("DLPADD1");
                }
                if (!jsonObj.optString("DLPADD2").equalsIgnoreCase("null") && !jsonObj.optString("DLPADD2").equalsIgnoreCase("")) {
                    dlpa_address = dlpa_address + ", " + jsonObj.optString("DLPADD2");
                }
                if (!jsonObj.optString("DLPADD3").equalsIgnoreCase("null") && !jsonObj.optString("DLPADD3").equalsIgnoreCase("")) {
                    dlpa_address = dlpa_address + ", " + jsonObj.optString("DLPADD3");
                }
                if (!jsonObj.optString("DLPPINCD").equalsIgnoreCase("null") && !jsonObj.optString("DLPPINCD").equalsIgnoreCase("")) {
                    dlpa_address = dlpa_address + ", " + jsonObj.optString("DLPPINCD");
                }

                dlSearchDetails.setDl_PAddress(dlpa_address);

                if (!jsonObj.optString("DLTADD1").equalsIgnoreCase("null") && !jsonObj.optString("DLTADD1").equalsIgnoreCase("")) {
                    dlta_address = dlta_address + jsonObj.optString("DLTADD1");
                }
                if (!jsonObj.optString("DLTADD2").equalsIgnoreCase("null") && !jsonObj.optString("DLTADD2").equalsIgnoreCase("")) {
                    dlta_address = dlta_address + ", " + jsonObj.optString("DLTADD2");
                }
                if (!jsonObj.optString("DLTADD3").equalsIgnoreCase("null") && !jsonObj.optString("DLTADD3").equalsIgnoreCase("")) {
                    dlta_address = dlta_address + ", " + jsonObj.optString("DLTADD3");
                }
                if (!jsonObj.optString("DLTPINCD").equalsIgnoreCase("null") && !jsonObj.optString("DLTPINCD").equalsIgnoreCase("")) {
                    dlta_address = dlta_address + ", " + jsonObj.optString("DLTPINCD");
                }

                dlSearchDetails.setDl_TAddress(dlta_address);

                if (!jsonObj.optString("DLISSUEAUTHORITY").equalsIgnoreCase("null") && !jsonObj.optString("DLISSUEAUTHORITY").equalsIgnoreCase("")) {
                    dlSearchDetails.setDl_IssueAuthority(jsonObj.optString("DLISSUEAUTHORITY"));
                }

                if (!jsonObj.optString("DLNTVLDTODT").equalsIgnoreCase("null") && !jsonObj.optString("DLNTVLDTODT").equalsIgnoreCase("")) {
                    dlSearchDetails.setDl_Ntvldtodt(jsonObj.optString("DLNTVLDTODT"));
                }

                if (!jsonObj.optString("DLTVLDTODT").equalsIgnoreCase("null") && !jsonObj.optString("DLTVLDTODT").equalsIgnoreCase("")) {
                    dlSearchDetails.setDl_Tvldtodt(jsonObj.optString("DLTVLDTODT"));
                }

                if (!jsonObj.optString("DLTELE").equalsIgnoreCase("null") && !jsonObj.optString("DLTELE").equalsIgnoreCase("")) {
                    dlSearchDetails.setDl_tele(jsonObj.optString("DLTELE"));
                }

                dlSearchDeatilsList.add(dlSearchDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    public void setVehicleSearchArrayList(JSONArray jsonArrayVehicle) {
        for (int i = 0; i < jsonArrayVehicle.length(); i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj = jsonArrayVehicle.getJSONObject(i);
                VehicleSearchDetails vehicleSearchDetails = new VehicleSearchDetails();

                String address = "";
                String p_address = "";

                if (!jsonObj.optString("ROWNUMBER").equalsIgnoreCase("null") && !jsonObj.optString("ROWNUMBER").equalsIgnoreCase("")) {
                    vehicleSearchDetails.setRol(jsonObj.optString("ROWNUMBER"));
                }

                if (!jsonObj.optString("MOBILE_NO").equalsIgnoreCase("null") && !jsonObj.optString("MOBILE_NO").equalsIgnoreCase("")) {
                    vehicleSearchDetails.setHolder_mobileNo(jsonObj.optString("MOBILE_NO"));
                }

                if (!jsonObj.optString("REGN_NO").equalsIgnoreCase("null") && !jsonObj.optString("REGN_NO").equalsIgnoreCase("")) {
                    vehicleSearchDetails.setRegn_no(jsonObj.optString("REGN_NO"));
                }
                else{
                    vehicleSearchDetails.setRegn_no("");
                }

                if (!jsonObj.optString("REGN_DT").equalsIgnoreCase("null") && !jsonObj.optString("REGN_DT").equalsIgnoreCase("")) {
                    vehicleSearchDetails.setRegn_dt(jsonObj.optString("REGN_DT"));
                }

                if (!jsonObj.optString("RTO_CD").equalsIgnoreCase("null") && !jsonObj.optString("RTO_CD").equalsIgnoreCase("")) {
                    vehicleSearchDetails.setRto_cd(jsonObj.optString("RTO_CD"));
                }

                if (!jsonObj.optString("ENG_NO").equalsIgnoreCase("null") && !jsonObj.optString("ENG_NO").equalsIgnoreCase("")) {
                    vehicleSearchDetails.setEng_no(jsonObj.optString("ENG_NO"));
                }

                if (!jsonObj.optString("CHASI_NO").equalsIgnoreCase("null") && !jsonObj.optString("CHASI_NO").equalsIgnoreCase("")) {
                    vehicleSearchDetails.setChasis_no(jsonObj.optString("CHASI_NO"));
                }

                if (!jsonObj.optString("O_NAME").equalsIgnoreCase("null") && !jsonObj.optString("O_NAME").equalsIgnoreCase("")) {
                    vehicleSearchDetails.setO_name(jsonObj.optString("O_NAME"));
                }

                if (!jsonObj.optString("F_NAME").equalsIgnoreCase("null") && !jsonObj.optString("F_NAME").equalsIgnoreCase("")) {
                    vehicleSearchDetails.setF_name(jsonObj.optString("F_NAME"));
                }

                if (!jsonObj.optString("GARAGE_ADD").equalsIgnoreCase("null") && !jsonObj.optString("GARAGE_ADD").equalsIgnoreCase("")) {
                    vehicleSearchDetails.setGarage_address(jsonObj.optString("GARAGE_ADD"));
                }

                if (!jsonObj.optString("COLOR").equalsIgnoreCase("null") && !jsonObj.optString("COLOR").equalsIgnoreCase("")) {
                    vehicleSearchDetails.setColor(jsonObj.optString("COLOR"));
                }

                if (!jsonObj.optString("MAKER_MODEL").equalsIgnoreCase("null") && !jsonObj.optString("MAKER_MODEL").equalsIgnoreCase("")) {
                    vehicleSearchDetails.setMaker_model(jsonObj.optString("MAKER_MODEL"));
                }

                if (!jsonObj.optString("CL_DESC").equalsIgnoreCase("null") && !jsonObj.optString("CL_DESC").equalsIgnoreCase("")) {
                    vehicleSearchDetails.setCl_desc(jsonObj.optString("CL_DESC"));
                }

                if (!jsonObj.optString("ADD1").equalsIgnoreCase("null") && !jsonObj.optString("ADD1").equalsIgnoreCase("")) {
                    address = address + jsonObj.optString("ADD1");
                }
                if (!jsonObj.optString("ADD2").equalsIgnoreCase("null") && !jsonObj.optString("ADD2").equalsIgnoreCase("")) {
                    address = address + ", " + jsonObj.optString("ADD2");
                }
                if (!jsonObj.optString("CITY").equalsIgnoreCase("null") && !jsonObj.optString("CITY").equalsIgnoreCase("")) {
                    address = address + ", " + jsonObj.optString("CITY");
                }
                if (!jsonObj.optString("PINCODE").equalsIgnoreCase("null") && !jsonObj.optString("PINCODE").equalsIgnoreCase("")) {
                    address = address + ", " + jsonObj.optString("PINCODE");
                }

                vehicleSearchDetails.setAddress(address);

                if (!jsonObj.optString("PADD1").equalsIgnoreCase("null") && !jsonObj.optString("PADD1").equalsIgnoreCase("")) {
                    p_address = p_address + jsonObj.optString("PADD1");
                }
                if (!jsonObj.optString("PADD2").equalsIgnoreCase("null") && !jsonObj.optString("PADD2").equalsIgnoreCase("")) {
                    p_address = p_address + ", " + jsonObj.optString("PADD2");
                }
                if (!jsonObj.optString("PPINCODE").equalsIgnoreCase("null") && !jsonObj.optString("PPINCODE").equalsIgnoreCase("")) {
                    p_address = p_address + ", " + jsonObj.optString("PPINCODE");
                }

                vehicleSearchDetails.setP_address(p_address);

                vehicleSearchDetailsList.add(vehicleSearchDetails);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }

    public void setSDRSearchArrayList(JSONArray jsonArraySDR) {
        for (int i = 0; i < jsonArraySDR.length(); i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj = jsonArraySDR.getJSONObject(i);

                SDRDetails sdrDetails = new SDRDetails();

                String present_address = "";
                String permanent_address = "";

                if (!jsonObj.optString("PROVIDER").equalsIgnoreCase("null") && !jsonObj.optString("PROVIDER").equalsIgnoreCase("")) {
                    sdrDetails.setAcct(jsonObj.optString("PROVIDER"));
                }
                if (!jsonObj.optString("ACTIVATIONDATE").equalsIgnoreCase("null") && !jsonObj.optString("ACTIVATIONDATE").equalsIgnoreCase("")) {
                    sdrDetails.setActDate(jsonObj.optString("ACTIVATIONDATE"));
                }
                if (!jsonObj.optString("ID_NUMBER").equalsIgnoreCase("null") && !jsonObj.optString("ID_NUMBER").equalsIgnoreCase("")) {
                    sdrDetails.setId_Number(jsonObj.optString("ID_NUMBER"));
                }
                else{
                    sdrDetails.setId_Number("");
                }
                if (!jsonObj.optString("ID_TYPE").equalsIgnoreCase("null") && !jsonObj.optString("ID_TYPE").equalsIgnoreCase("")) {
                    sdrDetails.setId_type(jsonObj.optString("ID_TYPE"));
                }
                if (!jsonObj.optString("MOBNO").equalsIgnoreCase("null") && !jsonObj.optString("MOBNO").equalsIgnoreCase("")) {
                    sdrDetails.setMobNo(jsonObj.optString("MOBNO"));
                }
                else{
                    sdrDetails.setMobNo("");
                }

                if (!jsonObj.optString("ALTERNATEPHNUMBER").equalsIgnoreCase("null") && !jsonObj.optString("ALTERNATEPHNUMBER").equalsIgnoreCase("")) {
                    sdrDetails.setPhone_no(jsonObj.optString("ALTERNATEPHNUMBER"));
                }

                if (!jsonObj.optString("NAME").equalsIgnoreCase("null") && !jsonObj.optString("NAME").equalsIgnoreCase("")) {
                    sdrDetails.setName(jsonObj.optString("NAME"));
                }
                if (!jsonObj.optString("ADDRESS").equalsIgnoreCase("null") && !jsonObj.optString("ADDRESS").equalsIgnoreCase("")) {
                    sdrDetails.setPresent_addres(jsonObj.optString("ADDRESS"));
                }
                else{
                    sdrDetails.setPresent_addres(jsonObj.optString("ADDRESS"));
                }

                if (!jsonObj.optString("PINCODE").equalsIgnoreCase("null") && !jsonObj.optString("PINCODE").equalsIgnoreCase("")) {
                    sdrDetails.setPresent_pincode(jsonObj.optString("PINCODE"));
                }
                if (!jsonObj.optString("CITY").equalsIgnoreCase("null") && !jsonObj.optString("CITY").equalsIgnoreCase("")) {
                    sdrDetails.setPresent_city(jsonObj.optString("CITY"));
                }
                if (!jsonObj.optString("STATE").equalsIgnoreCase("null") && !jsonObj.optString("STATE").equalsIgnoreCase("")) {
                    sdrDetails.setPresent_state(jsonObj.optString("STATE"));
                }


                sdrDetailsList.add(sdrDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }
    public void setCableSearchArrayList(JSONArray jsonArrayCable){
        for(int i=0;i<jsonArrayCable.length();i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj = jsonArrayCable.getJSONObject(i);


                CableSearchDetails cableSearchDetails = new CableSearchDetails(Parcel.obtain());

                if(!jsonObj.optString("Name").equalsIgnoreCase("null") && !jsonObj.optString("Name").equalsIgnoreCase("")){
                    cableSearchDetails.setCable_holderName(jsonObj.optString("Name"));
                }

                if(!jsonObj.optString("Address").equalsIgnoreCase("null") && !jsonObj.optString("Address").equalsIgnoreCase("")){
                    cableSearchDetails.setCable_holderAddress(jsonObj.optString("Address"));
                }
                else{
                    cableSearchDetails.setCable_holderAddress("");
                }

                if(!jsonObj.optString("City").equalsIgnoreCase("null") && !jsonObj.optString("City").equalsIgnoreCase("")){
                    cableSearchDetails.setCable_holderCity(jsonObj.optString("City"));
                }

                if(!jsonObj.optString("State").equalsIgnoreCase("null") && !jsonObj.optString("State").equalsIgnoreCase("")){
                    cableSearchDetails.setCable_holderState(jsonObj.optString("State"));
                }

                if(!jsonObj.optString("Zone").equalsIgnoreCase("null") && !jsonObj.optString("Zone").equalsIgnoreCase("")){
                    cableSearchDetails.setCable_holderZone(jsonObj.optString("Zone"));
                }

                if(!jsonObj.optString("Pincode").equalsIgnoreCase("null") && !jsonObj.optString("Pincode").equalsIgnoreCase("")){
                    cableSearchDetails.setCable_holderPincode(jsonObj.optString("Pincode"));
                }

                if(!jsonObj.optString("STB").equalsIgnoreCase("null") && !jsonObj.optString("STB").equalsIgnoreCase("")){
                    cableSearchDetails.setCable_holderSTBNo(jsonObj.optString("STB"));
                }
                else{
                    cableSearchDetails.setCable_holderSTBNo("");
                }

                if(!jsonObj.optString("VC").equalsIgnoreCase("null") && !jsonObj.optString("VC").equalsIgnoreCase("")){
                    cableSearchDetails.setCable_VCNo(jsonObj.optString("VC"));
                }

                if(!jsonObj.optString("MobileNumber").equalsIgnoreCase("null") && !jsonObj.optString("MobileNumber").equalsIgnoreCase("")){
                    cableSearchDetails.setCable_holderMobileNo(jsonObj.optString("MobileNumber"));
                }
                else{
                    cableSearchDetails.setCable_holderMobileNo("");
                }

                if(!jsonObj.optString("OfficeNumber").equalsIgnoreCase("null") && !jsonObj.optString("OfficeNumber").equalsIgnoreCase("")){
                    cableSearchDetails.setCable_holderOfficeNo(jsonObj.optString("OfficeNumber"));
                }

                cableSearchDeatilsList.add(cableSearchDetails);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    }
    public void setKMCSearchArrayList(JSONArray jsonArrayKMC){
        for(int i=0;i<jsonArrayKMC.length();i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj = jsonArrayKMC.getJSONObject(i);

                KMCSearchDetails kmcSearchDetails = new KMCSearchDetails(Parcel.obtain());

                if(!jsonObj.optString("ROWNUMBER").equalsIgnoreCase("null") && !jsonObj.optString("ROWNUMBER").equalsIgnoreCase("")){
                    kmcSearchDetails.setKmc_slNo(jsonObj.optString("ROWNUMBER"));
                }

                if(!jsonObj.optString("WARD").equalsIgnoreCase("null") && !jsonObj.optString("WARD").equalsIgnoreCase("")){
                    kmcSearchDetails.setKmc_wardNo(jsonObj.optString("WARD"));
                }else{
                    kmcSearchDetails.setKmc_wardNo("");
                }

                if(!jsonObj.optString("PREMISES").equalsIgnoreCase("null") && !jsonObj.optString("PREMISES").equalsIgnoreCase("")){
                    kmcSearchDetails.setKmc_premises(jsonObj.optString("PREMISES"));
                }

                if(!jsonObj.optString("OWNER").equalsIgnoreCase("null") && !jsonObj.optString("OWNER").equalsIgnoreCase("")){
                    kmcSearchDetails.setKmc_owner(jsonObj.optString("OWNER"));
                }
                else{
                    kmcSearchDetails.setKmc_owner("");
                }

                if(!jsonObj.optString("PERSON_LIABLE").equalsIgnoreCase("null") && !jsonObj.optString("PERSON_LIABLE").equalsIgnoreCase("")){
                    kmcSearchDetails.setKmc_personLiable(jsonObj.optString("PERSON_LIABLE"));
                }

                if(!jsonObj.optString("MAILING_ADDRESS").equalsIgnoreCase("null") && !jsonObj.optString("MAILING_ADDRESS").equalsIgnoreCase("")){
                    kmcSearchDetails.setKmc_mailingAddress(jsonObj.optString("MAILING_ADDRESS"));
                }else {
                    kmcSearchDetails.setKmc_mailingAddress("");
                }

                kmcSearchDeatilsList.add(kmcSearchDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    public void setGasSearchArrayList(JSONArray jsonArrayGas){
        for(int i=0;i<jsonArrayGas.length();i++){

            JSONObject jsonObj=null;

            try {
                jsonObj = jsonArrayGas.getJSONObject(i);

                ModifiedDocumentGasDetails gasSearchDetails = new ModifiedDocumentGasDetails();

                if(!jsonObj.optString("DISTRIBUTOR").equalsIgnoreCase("null") && !jsonObj.optString("DISTRIBUTOR").equalsIgnoreCase("")){
                    gasSearchDetails.setDistributor_id(jsonObj.optString("DISTRIBUTOR"));
                }

                if(!jsonObj.optString("DISTRIBUTOR_NAME").equalsIgnoreCase("null") && !jsonObj.optString("DISTRIBUTOR_NAME").equalsIgnoreCase("")){
                    gasSearchDetails.setDistributor_name(jsonObj.optString("DISTRIBUTOR_NAME"));
                }

                if(!jsonObj.optString("CONSUMER_ID").equalsIgnoreCase("null") && !jsonObj.optString("CONSUMER_ID").equalsIgnoreCase("")){
                    gasSearchDetails.setConsumer_id(jsonObj.optString("CONSUMER_ID"));
                }
                else{
                    gasSearchDetails.setConsumer_id("");
                }

                if(!jsonObj.optString("CONSUMER_NAME").equalsIgnoreCase("null") && !jsonObj.optString("CONSUMER_NAME").equalsIgnoreCase("")){
                    gasSearchDetails.setConsumer_name(jsonObj.optString("CONSUMER_NAME"));
                }

                if(!jsonObj.optString("CONSUMER_STATUS").equalsIgnoreCase("null") && !jsonObj.optString("CONSUMER_STATUS").equalsIgnoreCase("")){
                    gasSearchDetails.setConsumer_status(jsonObj.optString("CONSUMER_STATUS"));
                }

                if(!jsonObj.optString("ADDRESS").equalsIgnoreCase("null") && !jsonObj.optString("ADDRESS").equalsIgnoreCase("")){
                    gasSearchDetails.setAddress(jsonObj.optString("ADDRESS"));
                }
                else{
                    gasSearchDetails.setAddress(jsonObj.optString(""));
                }

                if(!jsonObj.optString("PIN_CODE").equalsIgnoreCase("null") && !jsonObj.optString("PIN_CODE").equalsIgnoreCase("")){
                    gasSearchDetails.setPincode(jsonObj.optString("PIN_CODE"));
                }

                if(!jsonObj.optString("MOBILE_NUMBER").equalsIgnoreCase("null") && !jsonObj.optString("MOBILE_NUMBER").equalsIgnoreCase("")){
                    gasSearchDetails.setPhone_no(jsonObj.optString("MOBILE_NUMBER"));
                }

                if(!jsonObj.optString("SV_DATE").equalsIgnoreCase("null") && !jsonObj.optString("SV_DATE").equalsIgnoreCase("")){
                    gasSearchDetails.setSv_date(jsonObj.optString("SV_DATE"));
                }

                if(!jsonObj.optString("HAS_AADHAAR_NO").equalsIgnoreCase("null") && !jsonObj.optString("HAS_AADHAAR_NO").equalsIgnoreCase("")){
                    gasSearchDetails.setHas_aadhar(jsonObj.optString("HAS_AADHAAR_NO"));
                }

                gasSearchDeatilsList.add(gasSearchDetails);

            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onClick(View v) {

    }


    public void updateNotificationCount(String notificationCount) {

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        } else {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }

    @Override
    public void OnAllDocumentSearchTypeItemClick(View v, int pos) {
        String searchtext=et_list_search.getText().toString();
       // et_list_search.setText("");
        if (allDocumentSearchTypeList.get(pos).getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[0])) {
            selectedSearchType = Constants.ALL_DOCUMENT_SEARCH_TYPE[0];
            dlSearchAdapter = new DLSearchAdapterModified(this, dlSearchDeatilsList);
            dlSearchAdapter.setOnAllDocumentSearchValue(this);
            alldataSearchListview.setAdapter(dlSearchAdapter);
            dlSearchAdapter.notifyDataSetChanged();
            if(!searchtext.isEmpty()){
                dlSearchAdapter.filter(searchtext);
            }


        } else if (allDocumentSearchTypeList.get(pos).getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[1])) {
            selectedSearchType = Constants.ALL_DOCUMENT_SEARCH_TYPE[1];
            vehicleSearchAdapter = new VehicleSearchAdapterModified(this, vehicleSearchDetailsList);
            vehicleSearchAdapter.setOnAllDocumentSearchValue(this);
            alldataSearchListview.setAdapter(vehicleSearchAdapter);
            vehicleSearchAdapter.notifyDataSetChanged();
            if(!searchtext.isEmpty()){
                vehicleSearchAdapter.filter(searchtext);
            }
        } else if (allDocumentSearchTypeList.get(pos).getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[2])) {
            selectedSearchType = Constants.ALL_DOCUMENT_SEARCH_TYPE[2];
            sdrSearchAdapter = new SDRSearchAdapterModified(this, sdrDetailsList);
            sdrSearchAdapter.setOnAllDocumentSearchValue(this);
            alldataSearchListview.setAdapter(sdrSearchAdapter);
            sdrSearchAdapter.notifyDataSetChanged();
            if(!searchtext.isEmpty()){
                sdrSearchAdapter.filter(searchtext);
            }
        } else if (allDocumentSearchTypeList.get(pos).getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[3])) {
            selectedSearchType = Constants.ALL_DOCUMENT_SEARCH_TYPE[3];
            cableSearchAdapter = new CableSearchAdapterModified(this, cableSearchDeatilsList);
            cableSearchAdapter.setOnAllDocumentSearchValue(this);
            alldataSearchListview.setAdapter(cableSearchAdapter);
            cableSearchAdapter.notifyDataSetChanged();
            if(!searchtext.isEmpty()){
                cableSearchAdapter.filter(searchtext);
            }

        } else if (allDocumentSearchTypeList.get(pos).getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[4])) {
            selectedSearchType = Constants.ALL_DOCUMENT_SEARCH_TYPE[4];
            kmcSearchAdapter = new KMCSearchAdapterModified(this, kmcSearchDeatilsList);
            kmcSearchAdapter.setOnAllDocumentSearchValue(this);
            alldataSearchListview.setAdapter(kmcSearchAdapter);
            kmcSearchAdapter.notifyDataSetChanged();
            if(!searchtext.isEmpty()){
                kmcSearchAdapter.filter(searchtext);
            }
        } else if (allDocumentSearchTypeList.get(pos).getName().equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[5])) {
            selectedSearchType = Constants.ALL_DOCUMENT_SEARCH_TYPE[5];
            gasSearchAdapter = new GasSearchAdapterModified(this, gasSearchDeatilsList);
            gasSearchAdapter.setOnAllDocumentSearchValue(this);
            alldataSearchListview.setAdapter(gasSearchAdapter);
            gasSearchAdapter.notifyDataSetChanged();
            if(!searchtext.isEmpty()){
                gasSearchAdapter.filter(searchtext);
            }
        }

        setPageNoTotalCountLoarmore();
        //showHideLoadmore();
    }

   /* @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[0])) {
            callDLSearchDetails(position);
        }
        if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[1])) {
            callVehicleSearchDetails(position);
        }
        if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[2])) {
            callSDRSearchDetails(position);
        }
        if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[3])) {
            callCableSearchDetails(position);
        }
        if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[4])) {
            callKMCSearchDetails(position);
        }
        if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[5])) {
            callGasSearchDetails(position);
        }

    }*/

    public void callDLSearchDetails(String uniqueKey) {
        int position=-1;
        for(int i=0;i<dlSearchDeatilsList.size();i++){
            if(dlSearchDeatilsList.get(i).getDl_No().equalsIgnoreCase(uniqueKey))
            {
                position=i;
            }

        }
        if(position!=-1) {
            Intent in = new Intent(AllDocumentDataSearchResultActivity.this, GlobalDLSearchDetailsActivity.class);
            in.putExtra("Holder_DLno", dlSearchDeatilsList.get(position).getDl_No());
            in.putExtra("Holder_DlName", dlSearchDeatilsList.get(position).getDl_Name());
            in.putExtra("Holder_DlSwdof", dlSearchDeatilsList.get(position).getDl_SwdOf());
            in.putExtra("Holder_DlDob", dlSearchDeatilsList.get(position).getDl_Dob());
            in.putExtra("Holder_DlSex", dlSearchDeatilsList.get(position).getDl_Sex());
            in.putExtra("Holder_DlPAddress", dlSearchDeatilsList.get(position).getDl_PAddress());
            in.putExtra("Holder_DlTAddress", dlSearchDeatilsList.get(position).getDl_TAddress());
            in.putExtra("Holder_DlAuthority", dlSearchDeatilsList.get(position).getDl_IssueAuthority());
            in.putExtra("Holder_DLNTVLDTODT", dlSearchDeatilsList.get(position).getDl_Ntvldtodt());
            in.putExtra("Holder_DLTVLDTODT", dlSearchDeatilsList.get(position).getDl_Tvldtodt());
            in.putExtra("Holder_DLTELE", dlSearchDeatilsList.get(position).getDl_tele());
            startActivity(in);
        }
        else{
            Utility.showAlertDialog(this,"Error","No details found",false);
        }

    }

    public void callVehicleSearchDetails(String uniqueKey) {
        int position=-1;
        for(int i=0;i<vehicleSearchDetailsList.size();i++){
            if(vehicleSearchDetailsList.get(i).getRegn_no().equalsIgnoreCase(uniqueKey))
            {
                position=i;
            }

        }
        if(position!=-1) {
            Intent in = new Intent(AllDocumentDataSearchResultActivity.this, GlobalVehicleSearchDetailsActivity.class);
            in.putExtra("Holder_Veh_HolderMobileNo", vehicleSearchDetailsList.get(position).getHolder_mobileNo());
            in.putExtra("Holder_Veh_RegNo", vehicleSearchDetailsList.get(position).getRegn_no());
            in.putExtra("Holder_Veh_RegDate", vehicleSearchDetailsList.get(position).getRegn_dt());
            in.putExtra("Holder_Veh_HolderName", vehicleSearchDetailsList.get(position).getO_name());
            in.putExtra("Holder_Veh_FatherName", vehicleSearchDetailsList.get(position).getF_name());
            in.putExtra("Holder_Veh_PresentAddress", vehicleSearchDetailsList.get(position).getAddress());
            in.putExtra("Holder_Veh_PermanentAddress", vehicleSearchDetailsList.get(position).getP_address());
            in.putExtra("Holder_Veh_EngineNo", vehicleSearchDetailsList.get(position).getEng_no());
            in.putExtra("Holder_Veh_ChasisNo", vehicleSearchDetailsList.get(position).getChasis_no());
            in.putExtra("Holder_Veh_GarageAddress", vehicleSearchDetailsList.get(position).getGarage_address());
            in.putExtra("Holder_Veh_Color", vehicleSearchDetailsList.get(position).getColor());
            in.putExtra("Holder_Veh_Maker", vehicleSearchDetailsList.get(position).getMaker_model());
            in.putExtra("Holder_Veh_Cldesc", vehicleSearchDetailsList.get(position).getCl_desc());

            startActivity(in);
        }  else{
            Utility.showAlertDialog(this,"Error","No details found",false);
        }


    }

    public void callSDRSearchDetails(String uniqueKey) {
        int position=-1;
        for(int i=0;i<sdrDetailsList.size();i++){
            if(sdrDetailsList.get(i).getMobNo().equalsIgnoreCase(uniqueKey))
            {
                position=i;
            }

        }
        if(position!=-1) {
            Intent in = new Intent(AllDocumentDataSearchResultActivity.this, GlobalSDRSearchDetailsActivity.class);
            in.putExtra("HOLDER_NAME", sdrDetailsList.get(position).getName());
            in.putExtra("ID_NUMBER", sdrDetailsList.get(position).getId_Number());
            in.putExtra("ID_TYPE", sdrDetailsList.get(position).getId_type());
            in.putExtra("HOLDER_PRESENT_ADDRESS", sdrDetailsList.get(position).getPresent_address());
            in.putExtra("HOLDER_PERMANENT_ADDRESS", sdrDetailsList.get(position).getPresent_address());
            in.putExtra("HOLDER_CITY", sdrDetailsList.get(position).getPresent_city());
            in.putExtra("HOLDER_STATE", sdrDetailsList.get(position).getPresent_state());
            in.putExtra("HOLDER_PINCODE", sdrDetailsList.get(position).getPresent_pincode());
            in.putExtra("HOLDER_ACT_PROVIDER", sdrDetailsList.get(position).getAcct());
            in.putExtra("HOLDER_ACT_DATE", sdrDetailsList.get(position).getActDate());
            in.putExtra("HOLDER_MOBILE", sdrDetailsList.get(position).getMobNo());
            in.putExtra("HOLDER_PHONE", sdrDetailsList.get(position).getPhone_no());
            startActivity(in);
        } else{
            Utility.showAlertDialog(this,"Error","No details found",false);
        }



    }
    public void callCableSearchDetails(String uniqueKey){
        int position=-1;
        for(int i=0;i<cableSearchDeatilsList.size();i++){
            if(cableSearchDeatilsList.get(i).getCable_holderSTBNo()!=null) {
                if (cableSearchDeatilsList.get(i).getCable_holderSTBNo().equalsIgnoreCase(uniqueKey)) {
                    position = i;
                }
            }

        }
      //  Toast.makeText(this, "position:"+position, Toast.LENGTH_SHORT).show();

       if(position!=-1) {

            Intent in = new Intent(AllDocumentDataSearchResultActivity.this, GlobalCableSearchDetailsActivity.class);
            in.putExtra("POSITION", position);
            in.putParcelableArrayListExtra("CABLE_SEARCH_RESULT", (ArrayList<? extends Parcelable>) cableSearchDeatilsList);
            //in.putParcelableArrayListExtra()
            startActivity(in);
        }
        else{
            Utility.showAlertDialog(this,"Error","No details found",false);
        }
    }
    public void callKMCSearchDetails(String uniqueKey){
        int position=-1;
        for(int i=0;i<kmcSearchDeatilsList.size();i++){
            if(kmcSearchDeatilsList.get(i).getKmc_mailingAddress().equalsIgnoreCase(uniqueKey))
            {
                position=i;
            }

        }
        if(position!=-1) {
            Intent in = new Intent(AllDocumentDataSearchResultActivity.this, GlobalKMCSearchDetailsActivity.class);
            in.putExtra("POSITION", position);
            in.putParcelableArrayListExtra("KMC_SEARCH_RESULT", (ArrayList<? extends Parcelable>) kmcSearchDeatilsList);
            //in.putParcelableArrayListExtra()
            startActivity(in);
        }
        else{
            Utility.showAlertDialog(this,"Error","No details found",false);
        }

    }
    public void callGasSearchDetails(String uniqueKey){
        int position=-1;      //CONSUMER_ID
        for(int i=0;i<gasSearchDeatilsList.size();i++){
            if(gasSearchDeatilsList.get(i).getConsumer_id().equalsIgnoreCase(uniqueKey))
            {
                position=i;
            }

        }
        if(position!=-1) {
            Intent in = new Intent(AllDocumentDataSearchResultActivity.this, GlobalGasSearchDetailsActivity.class);
            in.putExtra("DISTRIBUTOR_ID", gasSearchDeatilsList.get(position).getDistributor_id());
            in.putExtra("DISTRIBUTOR_NAME", gasSearchDeatilsList.get(position).getDistributor_name());
            in.putExtra("CONSUMER_ID", gasSearchDeatilsList.get(position).getConsumer_id());
            in.putExtra("CONSUMER_NAME", gasSearchDeatilsList.get(position).getConsumer_name());
            in.putExtra("CONSUMER_ADDRESS", gasSearchDeatilsList.get(position).getAddress());
            in.putExtra("CONSUMER_PINCODE", gasSearchDeatilsList.get(position).getPincode());
            in.putExtra("CONSUMER_MOBILE", gasSearchDeatilsList.get(position).getPhone_no());
            in.putExtra("CONSUMER_STATUS", gasSearchDeatilsList.get(position).getConsumer_status());
            in.putExtra("CONSUMER_SV_DATE", gasSearchDeatilsList.get(position).getSv_date());
            in.putExtra("CONSUMER_AADHAR", gasSearchDeatilsList.get(position).getHas_aadhar());
            startActivity(in);
        }
        else{
            Utility.showAlertDialog(this,"Error","No details found",false);
        }

    }

    @Override
    public void OnAllDoumentSearchValueItemClick(String uniquekey) {
        if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[0])) {
            callDLSearchDetails(uniquekey);
        }
        if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[1])) {
            callVehicleSearchDetails(uniquekey);
        }
        if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[2])) {
            callSDRSearchDetails(uniquekey);
        }
        if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[3])) {
            callCableSearchDetails(uniquekey);
        }
        if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[4])) {
            callKMCSearchDetails(uniquekey);
        }
        if (selectedSearchType.equalsIgnoreCase(Constants.ALL_DOCUMENT_SEARCH_TYPE[5])) {
            callGasSearchDetails(uniquekey);
        }


    }
}
