package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnAllDoumentSearchValueListener;
import com.kp.facedetection.model.DLSearchDetails;
import com.kp.facedetection.model.KMCSearchDetails;
import com.kp.facedetection.model.ModifiedDocumentGasDetails;
import com.kp.facedetection.utility.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DAT-165 on 05-12-2016.
 */
public class GasSearchAdapterModified extends BaseAdapter {

    private Context context;
    private List<ModifiedDocumentGasDetails> gasSearchDetailsList;
    List<ModifiedDocumentGasDetails> searchItemList = new ArrayList<ModifiedDocumentGasDetails>();
    OnAllDoumentSearchValueListener onAllDoumentSearchValueListener;

    public GasSearchAdapterModified(Context context, List<ModifiedDocumentGasDetails> gasSearchDetailsList) {
        this.context = context;
        this.gasSearchDetailsList = gasSearchDetailsList;
        searchItemList.addAll(gasSearchDetailsList);
    }
    public void setOnAllDocumentSearchValue(OnAllDoumentSearchValueListener onAllDoumentSearchValueListener){
        this.onAllDoumentSearchValueListener=onAllDoumentSearchValueListener;
    }



    @Override
    public int getCount() {
        int size = (searchItemList.size() > 0) ? searchItemList.size() : 0;
        return size;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.modified_sdr_search_list_item, null);
            holder.relative_main=(RelativeLayout)convertView.findViewById(R.id.relative_main);
            holder.tv_slNo = (TextView) convertView.findViewById(R.id.tv_slNo);
            holder.tv_consumerId = (TextView) convertView.findViewById(R.id.tv_name);  // set consumer id to name field
            holder.tv_consumerName = (TextView) convertView.findViewById(R.id.tv_address); //set name to address field
            holder.tv_consumerAddress = (TextView) convertView.findViewById(R.id.tv_mobileNo);  // set address to mobile field

            Constants.changefonts(holder.tv_slNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_consumerId, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_consumerName, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_consumerAddress, context, "Calibri.ttf");

            convertView.setTag(holder);
        }
        else{

            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);

        holder.tv_slNo.setText(sl_no);

        holder.tv_consumerId.setText("CONSUMER ID: "+searchItemList.get(position).getConsumer_id().trim());
        holder.tv_consumerName.setText("CONSUMER NAME: "+searchItemList.get(position).getConsumer_name().trim());
        holder.tv_consumerAddress.setText("ADDRESS: "+searchItemList.get(position).getAddress().trim());
        holder.relative_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onAllDoumentSearchValueListener!=null){
                    String uk=searchItemList.get(position).getConsumer_id();
                    onAllDoumentSearchValueListener.OnAllDoumentSearchValueItemClick(uk);
                }

            }
        });

        return convertView;
    }


    class ViewHolder {

        TextView tv_slNo,tv_consumerName,tv_consumerAddress,tv_consumerId;
        RelativeLayout relative_main;

    }
    public void filter(String charText) {
        Log.d("JAY",charText);
        charText = charText.toLowerCase();
        searchItemList.clear();
        if (charText.length() == 0) {
            searchItemList.addAll(gasSearchDetailsList);
        } else {
            for (ModifiedDocumentGasDetails item : gasSearchDetailsList) {
                if (item.getConsumer_name().toLowerCase().contains(charText)|| item.getConsumer_id().contains(charText)||item.getAddress().toLowerCase().contains(charText)) {
                    searchItemList.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }
}
