package com.kp.facedetection.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextPaint;
import android.util.AttributeSet;

import com.kp.facedetection.R;


/**
 * TODO: Created by Tanay Mondal on 25-05-2017
 */

public class CircleTextView_new extends AppCompatTextView {

    private Paint backgroundPaint = new Paint();
    private TextPaint textPaint1 = new TextPaint();
    private TextPaint textPaint2 = new TextPaint();
    private String backgroundColor;
    private String initials;

    public CircleTextView_new(Context context) {
        super(context);
    }

    public CircleTextView_new(Context context, AttributeSet attrs) {
        super(context, attrs);

        L.e("CircleView 1");

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircleInitialView, 0, 0);
        try {
            backgroundColor = typedArray.getString(R.styleable.CircleInitialView_circleBackgroundColor);
            initials = typedArray.getString(R.styleable.CircleInitialView_initials);
            L.e(String.valueOf(backgroundColor));
        } finally {
            typedArray.recycle();
        }
    }

    public CircleTextView_new(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        L.e("CircleView 2");

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircleInitialView, defStyleAttr, 0);
        try {
            backgroundColor = typedArray.getString(R.styleable.CircleInitialView_circleBackgroundColor);
            initials = typedArray.getString(R.styleable.CircleInitialView_initials);
            L.e(String.valueOf(backgroundColor));
        } finally {
            typedArray.recycle();
        }
    }

    public void setInitials(String initial) {
        this.initials = initial;
        invalidate();
        requestLayout();
    }

    @Override
    protected void onDraw(Canvas canvas) {

/*        textPaint1.setTextSize((float) (getMeasuredHeight() / 1.7));
        textPaint1.setColor(ContextCompat.getColor(getContext(), R.color.blue));
        textPaint1.setAntiAlias(true);
        textPaint1.setDither(true);
        textPaint1.setTextAlign(Paint.Align.CENTER);
        textPaint1.setTypeface(AppData.getTypefaceRegular(getContext()));

        textPaint2.setTextSize((float) (getMeasuredHeight() / 1.7));
        textPaint2.setColor(ContextCompat.getColor(getContext(), R.color.blue));
        textPaint2.setAntiAlias(true);
        textPaint2.setDither(true);
        textPaint2.setTextAlign(Paint.Align.CENTER);
        textPaint2.setTypeface(AppData.getTypefaceRegular(getContext()));*/

        backgroundPaint.setColor(ContextCompat.getColor(getContext(), android.R.color.white));
        backgroundPaint.setAntiAlias(true);
        backgroundPaint.setDither(true);

        canvas.drawCircle(getMeasuredWidth() / 2, getMeasuredWidth() / 2, (getMeasuredWidth() / 2), backgroundPaint);

        /*if (initials.length() > 1 && initials.length() <= 2) {
            int xPos1 = (int) (canvas.getWidth() / 2 + ((textPaint1.descent() + textPaint1.ascent()) / 2));
            int yPos1 = (int) ((canvas.getHeight() / 2) - ((textPaint1.descent() + textPaint1.ascent()) / 2));

            int xPos2 = (int) (canvas.getWidth() / 2 - ((textPaint2.descent() + textPaint2.ascent()) / 2));
            int yPos2 = (int) ((canvas.getHeight() / 2) - ((textPaint2.descent() + textPaint2.ascent()) / 2));

            canvas.drawText(String.valueOf(initials.charAt(0)), xPos1, yPos1, textPaint1);
            canvas.drawText(String.valueOf(initials.charAt(1)), xPos2, yPos2, textPaint2);
            L.e("Double initial");
        } else {
            L.e("Single initial");
            int xPos1 = canvas.getWidth() / 2;
            int yPos1 = (int) ((canvas.getHeight() / 2) - ((textPaint1.descent() + textPaint1.ascent()) / 2));

            canvas.drawText(String.valueOf(initials.charAt(0)), xPos1, yPos1, textPaint1);
        }*/

        super.onDraw(canvas);
    }

    /**
     * @param color provide color string
     */
    public void setInitialsBackgroundColor(String color) {
        L.e("color " + color);
        backgroundColor = "#" + String.valueOf(color);
        invalidate();
        requestLayout();
    }
}
