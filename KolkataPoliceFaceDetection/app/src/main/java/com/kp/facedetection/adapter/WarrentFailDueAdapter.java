package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnItemClickListenerForAllFIRView;
import com.kp.facedetection.interfaces.OnItemClickListenerForWarrentDue;
import com.kp.facedetection.interfaces.OnItemClickListenerForWarrentFail;
import com.kp.facedetection.model.CrimeReviewDetails;

import java.util.ArrayList;

/**
 * Created by DAT-Asset-128 on 31-10-2017.
 */

public class WarrentFailDueAdapter  extends RecyclerView.Adapter<WarrentFailDueAdapter.MyViewHolder>  {
    Context con;
    ArrayList<CrimeReviewDetails> psList = new ArrayList<>();
    private OnItemClickListenerForAllFIRView allFirViewClickListener;
    private OnItemClickListenerForWarrentFail onItemClickListenerForWarrentFail;
    private OnItemClickListenerForWarrentDue onItemClickListenerForWarrentDue;

    public WarrentFailDueAdapter(Context con, ArrayList<CrimeReviewDetails> psList) {
        this.con = con;
        this.psList = psList;
    }

    @Override
    public WarrentFailDueAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.warrant_fail_due_item_layout, parent, false);

        return new WarrentFailDueAdapter.MyViewHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(WarrentFailDueAdapter.MyViewHolder holder, int position) {
        final CrimeReviewDetails crimeReviewDetails = psList.get(position);
        if (crimeReviewDetails.getCrimeCategory() != null && !crimeReviewDetails.getCrimeCategory().equalsIgnoreCase("")
                && !crimeReviewDetails.getCrimeCategory().equalsIgnoreCase("null")) {
            holder.tv_categoryName.setText(crimeReviewDetails.getCrimeCategory());
        }
        if (crimeReviewDetails.getCountOfCrime() != null && !crimeReviewDetails.getCountOfCrime().equalsIgnoreCase("")
                && !crimeReviewDetails.getCountOfCrime().equalsIgnoreCase("null")) {
            if(crimeReviewDetails.getCountOfCrime().contains("/")) {
                try {
                    String[] dateAll = crimeReviewDetails.getCountOfCrime().split("/");
                    String failedDate = dateAll[0];
                    if (!failedDate.equalsIgnoreCase("0")) {
                        final SpannableStringBuilder sb = new SpannableStringBuilder(crimeReviewDetails.getCountOfCrime());
                        // Span to set text color to some RGB value
                        final ForegroundColorSpan fcs = new ForegroundColorSpan(con.getResources().getColor(android.R.color.holo_red_dark));
                        sb.setSpan(fcs, 0, crimeReviewDetails.getCountOfCrime().indexOf("/"), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                        holder.tv_count.setText(sb);
                    } else {
                        holder.tv_count.setText(crimeReviewDetails.getCountOfCrime());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            else {

                holder.tv_count.setText(crimeReviewDetails.getCountOfCrime());
            }
        }


    }

    @Override
    public int getItemCount() {
        return psList == null ? 0 : psList.size();
    }

    public void setClickListener(OnItemClickListenerForAllFIRView allFirViewClickListener) {
        this.allFirViewClickListener = allFirViewClickListener;
    }
    public void setClickListenerWarrentFail(OnItemClickListenerForWarrentFail onItemClickListenerForWarrentFail) {
        this.onItemClickListenerForWarrentFail = onItemClickListenerForWarrentFail;
    }
    public void setClickListenerWarrentDue(OnItemClickListenerForWarrentDue onItemClickListenerForWarrentDue) {
        this.onItemClickListenerForWarrentDue = onItemClickListenerForWarrentDue;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_categoryName, tv_count ;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_categoryName = (TextView) itemView.findViewById(R.id.tv_categoryName);
            tv_count = (TextView) itemView.findViewById(R.id.tv_count);
          //  tv_count.setOnClickListener(this);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (allFirViewClickListener != null) {
                allFirViewClickListener.onClick(view, getAdapterPosition());
            }
         /*   switch (view.getId())
            {
                case R.id.tv_count:
                    onItemClickListenerForWarrentFail.onItemClickListenerWarrentFail(getAdapterPosition());
                    break;
             *//*   case R.id.tv_count_due:
                    onItemClickListenerForWarrentDue.onItemClickListenerWarrentDue(getAdapterPosition());
                    break;*//*
            }*/
        }
    }
}
