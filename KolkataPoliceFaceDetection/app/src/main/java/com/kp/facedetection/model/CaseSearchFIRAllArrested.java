package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 18-07-2017.
 */

public class CaseSearchFIRAllArrested implements Serializable {
    private String name="";
    private String father="";
    private String address="";
    private String alias="";
    private String psCase = "";
    private String arrest_date = "";
    private String arrested_by = "";
    private String prv_criminal_no ="";

    public String getPrv_criminal_no() {
        return prv_criminal_no;
    }

    public void setPrv_criminal_no(String prv_criminal_no) {
        this.prv_criminal_no = prv_criminal_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPsCase() {
        return psCase;
    }

    public void setPsCase(String psCase) {
        this.psCase = psCase;
    }

    public String getArrest_date() {
        return arrest_date;
    }

    public void setArrest_date(String arrest_date) {
        this.arrest_date = arrest_date;
    }

    public String getArrested_by() {
        return arrested_by;
    }

    public void setArrested_by(String arrested_by) {
        this.arrested_by = arrested_by;
    }
}
