package com.kp.facedetection.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DAT-165 on 22-03-2017.
 */

public class MapSearchDetails implements Parcelable {

    private String caseNo = "";
    private String caseDate = "";
    private String crimeCat = "";
    private String psCode = "";
    private String psName = "";
    private String divisionId = "";
    private String longitudeValue = "";
    private String latitudeValue = "";
    private String lngMin = "";
    private String latMin = "";
    private String lngSec = "";
    private String latSec = "";
    private String firYr = "";
    private String poLat = "";
    private String poLong = "";
    private String colorCode = "";



    public MapSearchDetails(Parcel in) {
        caseNo = in.readString();
        caseDate = in.readString();
        crimeCat = in.readString();
        psCode = in.readString();
        psName = in.readString();
        divisionId = in.readString();
        longitudeValue = in.readString();
        latitudeValue = in.readString();
        lngMin = in.readString();
        latMin = in.readString();
        lngSec = in.readString();
        latSec = in.readString();
        firYr = in.readString();
        poLat = in.readString();
        poLong = in.readString();
        colorCode = in.readString();
    }

    public static final Creator<MapSearchDetails> CREATOR = new Creator<MapSearchDetails>() {
        @Override
        public MapSearchDetails createFromParcel(Parcel in) {
            return new MapSearchDetails(in);
        }

        @Override
        public MapSearchDetails[] newArray(int size) {
            return new MapSearchDetails[size];
        }
    };




    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(caseNo);
        dest.writeString(caseDate);
        dest.writeString(crimeCat);
        dest.writeString(psCode);
        dest.writeString(psName);
        dest.writeString(divisionId);
        dest.writeString(longitudeValue);
        dest.writeString(latitudeValue);
        dest.writeString(lngMin);
        dest.writeString(latMin);
        dest.writeString(lngSec);
        dest.writeString(latSec);
        dest.writeString(firYr);
        dest.writeString(poLat);
        dest.writeString(poLong);
        dest.writeString(colorCode);
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getCaseDate() {
        return caseDate;
    }

    public void setCaseDate(String caseDate) {
        this.caseDate = caseDate;
    }

    public String getCrimeCat() {
        return crimeCat;
    }

    public void setCrimeCat(String crimeCat) {
        this.crimeCat = crimeCat;
    }

    public String getPsCode() {
        return psCode;
    }

    public void setPsCode(String psCode) {
        this.psCode = psCode;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public String getLongitudeValue() {
        return longitudeValue;
    }

    public void setLongitudeValue(String longitudeValue) {
        this.longitudeValue = longitudeValue;
    }

    public String getLatitudeValue() {
        return latitudeValue;
    }

    public void setLatitudeValue(String latitudeValue) {
        this.latitudeValue = latitudeValue;
    }

    public String getLngMin() {
        return lngMin;
    }

    public void setLngMin(String lngMin) {
        this.lngMin = lngMin;
    }

    public String getLatMin() {
        return latMin;
    }

    public void setLatMin(String latMin) {
        this.latMin = latMin;
    }

    public String getLngSec() {
        return lngSec;
    }

    public void setLngSec(String lngSec) {
        this.lngSec = lngSec;
    }

    public String getLatSec() {
        return latSec;
    }

    public void setLatSec(String latSec) {
        this.latSec = latSec;
    }

    public String getPsName() {
        return psName;
    }

    public void setPsName(String psName) {
        this.psName = psName;
    }

    public String getFirYr() {
        return firYr;
    }

    public void setFirYr(String firYr) {
        this.firYr = firYr;
    }

    public String getPoLat() {
        return poLat;
    }

    public void setPoLat(String poLat) {
        this.poLat = poLat;
    }

    public String getPoLong() {
        return poLong;
    }

    public void setPoLong(String poLong) {
        this.poLong = poLong;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }
}
