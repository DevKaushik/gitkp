package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 20-09-2016.
 */
public class SDRDetails implements Serializable {

    private String acct="";
    private String mobNo="";
    private String name="";
    private String present_address="";
    private String present_pincode ="";
    private String present_city ="";
    private String present_state="";
    private String osAddress="";
    private String actDate="";
    private String phone_no="";
    private String permanent_pincode ="";
    private String permanent_address ="";
    private String permanent_city ="";
    private String permanent_state ="";
    private String id_Number ="";
    private String id_type ="";

    public String getId_Number() {
        return id_Number;
    }

    public void setId_Number(String id_Number) {
        this.id_Number = id_Number;
    }

    public String getId_type() {
        return id_type;
    }

    public void setId_type(String id_type) {
        this.id_type = id_type;
    }

    public String getPermanent_state() {
        return permanent_state;
    }

    public void setPermanent_state(String permanent_state) {
        this.permanent_state = permanent_state;
    }

    public String getPermanent_city() {
        return permanent_city;
    }

    public void setPermanent_city(String permanent_city) {
        this.permanent_city = permanent_city;
    }

    public String getPermanent_address() {
        return permanent_address;
    }

    public void setPermanent_address(String permanent_address) {
        this.permanent_address = permanent_address;
    }

    public String getPermanent_pincode() {
        return permanent_pincode;
    }

    public void setPermanent_pincode(String permanent_pincode) {
        this.permanent_pincode = permanent_pincode;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPresent_address() {
        return present_address;
    }

    public void setPresent_addres(String present_address) {
        this.present_address = present_address;
    }

    public String getPresent_pincode() {
        return present_pincode;
    }

    public void setPresent_pincode(String present_pincode) {
        this.present_pincode = present_pincode;
    }

    public String getPresent_city() {
        return present_city;
    }

    public void setPresent_city(String present_city) {
        this.present_city = present_city;
    }

    public String getPresent_state() {
        return present_state;
    }

    public void setPresent_state(String present_state) {
        this.present_state = present_state;
    }

    public String getOsAddress() {
        return osAddress;
    }

    public void setOsAddress(String osAddress) {
        this.osAddress = osAddress;
    }

    public String getActDate() {
        return actDate;
    }

    public void setActDate(String actDate) {
        this.actDate = actDate;
    }
}
