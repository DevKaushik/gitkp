package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 17-02-2017.
 */
public class ClassListCriminal implements Serializable {

    private String className = "";

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
