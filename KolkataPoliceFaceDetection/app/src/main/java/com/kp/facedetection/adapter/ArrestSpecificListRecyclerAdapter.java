package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnArrestItemListener;
import com.kp.facedetection.model.SpecificArrestDetails;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by DAT-165 on 07-06-2017.
 */

public class ArrestSpecificListRecyclerAdapter extends RecyclerView.Adapter<ArrestSpecificListRecyclerAdapter.MyViewHolder> {

    Context con;
    ArrayList<SpecificArrestDetails> specificArrestList = new ArrayList<>();
    private OnArrestItemListener onArrestItemListener;

    public ArrestSpecificListRecyclerAdapter(Context con, ArrayList<SpecificArrestDetails> specificArrestList) {
        this.con = con;
        this.specificArrestList = specificArrestList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.specific_arrest_item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final SpecificArrestDetails specificArrestDetails = specificArrestList.get(position);
        String name = "";
        String psDetail = "";
        String caseDetail = "";
        String genAndAge = "";
        String address = "";

        if (specificArrestDetails.getArrestee() != null && !specificArrestDetails.getArrestee().equalsIgnoreCase("") && !specificArrestDetails.getArrestee().equalsIgnoreCase("null")) {
            name = specificArrestDetails.getArrestee();
        } else {
            name = "NA";
        }
        if (specificArrestDetails.getAliasName() != null && !specificArrestDetails.getAliasName().equalsIgnoreCase("")
                && !specificArrestDetails.getAliasName().equalsIgnoreCase("NA") && !specificArrestDetails.getAliasName().equalsIgnoreCase("NIL")) {
            name = name + " @" + specificArrestDetails.getAliasName();
        }


        if (specificArrestDetails.getGender() != null && !specificArrestDetails.getGender().equalsIgnoreCase("NA") && !specificArrestDetails.getGender().equalsIgnoreCase("") && !specificArrestDetails.getGender().equalsIgnoreCase("null")) {
            genAndAge = " (" + specificArrestDetails.getGender();
        } else {
            genAndAge = " (";
        }

        if (specificArrestDetails.getGender() != null && !specificArrestDetails.getAge().equalsIgnoreCase("") && !specificArrestDetails.getAge().equalsIgnoreCase("null") && !specificArrestDetails.getAge().equalsIgnoreCase("NA")) {

            genAndAge = genAndAge + " - " + specificArrestDetails.getAge() + ")";
        } else {
            genAndAge = genAndAge + specificArrestDetails.getAge() + ")";
        }

        if (genAndAge.toString().equals(" ()")) {
            genAndAge = "";
        } else {

        }

        name = name + genAndAge;
        holder.tv_nameDetail.setText(name);


        if (specificArrestDetails.getFatherName() != null && !specificArrestDetails.getFatherName().equalsIgnoreCase("") && !specificArrestDetails.getFatherName().equalsIgnoreCase("null")) {
            holder.tv_fatherName.setText("S/o " + specificArrestDetails.getFatherName());
        }

        if (specificArrestDetails.getAddress() != null && !specificArrestDetails.getAddress().equalsIgnoreCase("") && !specificArrestDetails.getAddress().equalsIgnoreCase("null")) {
            address = specificArrestDetails.getAddress();
        } else {
            address = "NA";
        }
        holder.tv_address.setText(address);


        if (specificArrestDetails.getPsName() != null && !specificArrestDetails.getPsName().equalsIgnoreCase("") && !specificArrestDetails.getPsName().equalsIgnoreCase("null")) {
            psDetail = "vide " + specificArrestDetails.getPsName();
        }

        if (specificArrestDetails.getCaseRef() != null && !specificArrestDetails.getCaseRef().equalsIgnoreCase("") && !specificArrestDetails.getCaseRef().equalsIgnoreCase("null")) {
            psDetail = psDetail + " C/No." + specificArrestDetails.getCaseRef() + " dated";
        }
        holder.tv_psDetail.setText(psDetail);

        if (specificArrestDetails.getCaseDate() != null && !specificArrestDetails.getCaseDate().equalsIgnoreCase("") && !specificArrestDetails.getCaseDate().equalsIgnoreCase("null")) {
            caseDetail = specificArrestDetails.getCaseDate();
        }
        if (specificArrestDetails.getUnderSection() != null && !specificArrestDetails.getUnderSection().equalsIgnoreCase("") && !specificArrestDetails.getUnderSection().equalsIgnoreCase("null")) {
            caseDetail = caseDetail + " u/s " + specificArrestDetails.getUnderSection();
        }
        holder.tv_usDetail.setText(caseDetail);

        String psDtl = "";
        if (specificArrestDetails.getPS() != null && !specificArrestDetails.getPS().equalsIgnoreCase("") && !specificArrestDetails.getPS().equalsIgnoreCase("null")) {
            psDtl = specificArrestDetails.getPS();
        }
        if (!psDtl.equalsIgnoreCase("")) {
            holder.tv_ps.setText("Arrested by: " + psDtl);
        } else {
            holder.tv_ps.setVisibility(View.GONE);
        }

        String arrestCount = "";
        if (specificArrestDetails.getUNIT() != null && !specificArrestDetails.getUNIT().equalsIgnoreCase("") && !specificArrestDetails.getUNIT().equalsIgnoreCase("null")) {
            arrestCount = specificArrestDetails.getUNIT();
        }
        if (!arrestCount.equalsIgnoreCase("")) {
            holder.txt_unit.setVisibility(View.VISIBLE);
            holder.txt_unit.setText(arrestCount);
        } else {
            holder.txt_unit.setVisibility(View.GONE);
        }

        // Set Image using specificArrestDetails.getPICTURE_URL()
        if (specificArrestDetails.getPicture_list().size() > 0) {

            Picasso.with(con).load(specificArrestDetails.getPicture_list().get(0)).placeholder(R.drawable.human3).resize(180, 180).centerInside().into(holder.img_person);
        } else {
            holder.img_person.setImageResource(R.drawable.human3);
        }

    }

    @Override
    public int getItemCount() {
        return specificArrestList == null ? 0 : specificArrestList.size();
    }

    public void setArrestItemClickListener(OnArrestItemListener onArrestItemListener) {
        this.onArrestItemListener = onArrestItemListener;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_nameDetail, tv_fatherName, tv_address, tv_psDetail, tv_usDetail, tv_ps, txt_unit;
        ImageView img_person;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_nameDetail = (TextView) itemView.findViewById(R.id.tv_nameDetail);
            tv_fatherName = (TextView) itemView.findViewById(R.id.tv_fatherName);
            tv_address = (TextView) itemView.findViewById(R.id.tv_address);
            tv_psDetail = (TextView) itemView.findViewById(R.id.tv_psDetail);
            tv_usDetail = (TextView) itemView.findViewById(R.id.tv_usDetail);
            tv_ps = (TextView) itemView.findViewById(R.id.tv_ps);
            txt_unit = (TextView) itemView.findViewById(R.id.txt_unit);
            img_person = (ImageView) itemView.findViewById(R.id.img_person);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
           if (onArrestItemListener != null) {
                onArrestItemListener.onArrestItemClick(v, getAdapterPosition());
            }
        }
    }
}
