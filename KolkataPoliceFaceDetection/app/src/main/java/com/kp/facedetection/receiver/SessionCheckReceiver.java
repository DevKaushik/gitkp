package com.kp.facedetection.receiver;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.kp.facedetection.BuildConfig;
import com.kp.facedetection.utility.Utility;

import java.util.List;

/**
 * Created by DAT-Asset-131 on 31-03-2016.
 */
public class SessionCheckReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context c, Intent i) {
        // TODO Auto-generated method stub
        // DO YOUR LOGOUT LOGIC HERE
        System.out.println("On Receive Call");
        Log.e("TAG", "On Receive Call");

        /*Intent logoutIntent = new Intent(c, LogoutUsingService.class);
        c.startService(logoutIntent);*/
        Utility.logout2(c);
        closeAppFromBackground(c);
       // Toast.makeText(c, "You are now logged out.", Toast.LENGTH_LONG).show();
    }

    private void closeAppFromBackground(Context mContext){

        List<ApplicationInfo> packages;
        PackageManager pm;
        pm = mContext.getPackageManager();
        String appPackageName = BuildConfig.APPLICATION_ID;
        Log.e("Package Name",appPackageName);
        //get a list of installed apps.
        packages = pm.getInstalledApplications(0);

        ActivityManager mActivityManager = (ActivityManager)mContext.getSystemService(Context.ACTIVITY_SERVICE);

        for (ApplicationInfo packageInfo : packages) {
            if((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM)==1)continue;
            if(packageInfo.packageName.equals(appPackageName)) continue;
            mActivityManager.killBackgroundProcesses(packageInfo.packageName);
        }

    }
}
