package com.kp.facedetection.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.imageloader.ImageLoader;
import com.kp.facedetection.model.CriminalDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utils.CircularTextView;

import java.util.ArrayList;
import java.util.List;

public class CriminalAdapter extends ArrayAdapter<CriminalDetails> {
    Context mContext;
    ArrayList<CriminalDetails> criminalList;
    ImageLoader imageLoader;
    private List<String> imageExtensionList = new ArrayList<String>();

    public CriminalAdapter(Context context, int resource,
                           int textViewResourceId, ArrayList<CriminalDetails> criminalList) {
        super(context, resource, textViewResourceId, criminalList);
        this.mContext = context;
        this.criminalList = criminalList;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getCount() {

        //System.out.println("Size :"+criminalList.size());

        if (criminalList.size() > 0)
            return criminalList.size();
        else
            return 0;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final ViewHolder viewHolder;
        if (row == null) {
            row = View.inflate(mContext, R.layout.modified_search_listitem_layout, null);
            viewHolder = new ViewHolder(row);
            row.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) row.getTag();
        }


        if (position % 2 == 0) {
            row.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            row.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        viewHolder.tv_alias_name.setVisibility(View.GONE);

        if (criminalList.get(position).getNameAccused().contains("NAME_ACCUSED: ")) {

            if (!criminalList.get(position).getNameAccused().replace("NAME_ACCUSED: ", "").equalsIgnoreCase("")) {

                if (!criminalList.get(position).getCriminal_alias().equalsIgnoreCase("")) {
                    viewHolder.tv_name.setText(criminalList.get(position).getNameAccused().replace("NAME_ACCUSED: ", "") + criminalList.get(position).getCriminal_alias());
                } else {
                    viewHolder.tv_name.setText(criminalList.get(position).getNameAccused().replace("NAME_ACCUSED: ", ""));
                }
            } else {
                viewHolder.tv_name.setVisibility(View.GONE);
            }
        }
        /*if(criminalList.get(position).getAliasName().contains("ALIAS: "))
        if (!criminalList.get(position).getAliasName().replace("ALIAS: ", "").equalsIgnoreCase(""))
            viewHolder.tv_alias_name.setText(criminalList.get(position).getAliasName().replace("ALIAS: ", ""));
        else {
            viewHolder.tv_alias_name.setVisibility(View.GONE);
        }*/

        //viewHolder.tv_contact.setText("");//criminalList.get(position).getMobileNo());
        if (criminalList.get(position).getAddressAccused().contains("ADDR_ACCUSED: ")) {
            viewHolder.tv_dob.setVisibility(View.VISIBLE);
            if (!criminalList.get(position).getAddressAccused().replace("ADDR_ACCUSED: ", "").equalsIgnoreCase("")) {
                viewHolder.tv_dob.setVisibility(View.VISIBLE);
                viewHolder.tv_dob.setText(criminalList.get(position).getAddressAccused().replace("ADDR_ACCUSED: ", ""));
            } else {
                viewHolder.tv_dob.setVisibility(View.GONE);
            }
        } else {
            viewHolder.tv_dob.setVisibility(View.GONE);
        }

        /*if(criminalList.get(position).getFlag().contains("FLAG: ")){
            viewHolder.tv_flag.setText(criminalList.get(position).getFlag().replace("FLAG: ",""));
        }*/
        // "http://182.71.240.209/crimebabu/uploads/criminal_pic/2015/0007-2015.jpg"

        //viewHolder.iv_searchResultImage.setImageDrawable(null);

        StringBuilder stringBuilder,stringBuilder2;
        int briefList_size= criminalList.get(position).getBriefMatch_list().size() ;

        if (briefList_size > 0) {

            viewHolder.tv_briefMatch.setVisibility(View.VISIBLE);

            stringBuilder= new StringBuilder();
            stringBuilder2= new StringBuilder();
            stringBuilder.append("Brief Match: ");

            for(int j=0;j<briefList_size;j++){

                if(j==briefList_size -1){
                    stringBuilder2.append(criminalList.get(position).getBriefMatch_list().get(j));
                }else{
                    stringBuilder2.append(criminalList.get(position).getBriefMatch_list().get(j)+" / ");
                }
            }
            viewHolder.tv_briefMatch.setText(""+stringBuilder+stringBuilder2);
        }else{
            viewHolder.tv_briefMatch.setVisibility(View.GONE);
        }


        if (criminalList.get(position).getPicture_list().size() > 0) {

            imageExtensionList.clear();

            imageExtensionList.add("bmp");
            imageExtensionList.add("gif");
            imageExtensionList.add("jpg");
            imageExtensionList.add("jpeg");
            imageExtensionList.add("png");


            final String original_url = criminalList.get(position).getPicture_list().get(0);
            final String extension = (original_url.substring(original_url.lastIndexOf(".") + 1)).toLowerCase();
            System.out.println("Extension: " + extension);
            imageExtensionList.remove(imageExtensionList.indexOf(extension));
            new Thread(new Runnable() {
                @Override
                public void run() {

                        if (imageLoader.getBitmap(original_url) != null) {
                            ((Activity) mContext).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    imageLoader.DisplayImage(
                                            original_url,
                                            viewHolder.iv_searchResultImage);

                                }
                            });
                        } else {

                            for (int i = 0; i < imageExtensionList.size(); i++) {

                                if (imageLoader.getBitmap(original_url.replace(extension, imageExtensionList.get(i))) != null) {

                                    final int finalI = i;
                                    ((Activity) mContext).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            imageLoader.DisplayImage(
                                                    original_url.replace(extension, imageExtensionList.get(finalI)),
                                                    viewHolder.iv_searchResultImage);
                                        }
                                    });

                                    break;
                                }

                            }
                        }

                }
            }).start();

            System.out.println(" SHOW image Url::"+ imageLoader.getBitmap(original_url) );

        }
        else{
            viewHolder.iv_searchResultImage.setImageDrawable(ContextCompat.getDrawable(mContext,R.drawable.human3));
        }


        return row;
    }

    class ViewHolder {
        TextView tv_name, tv_alias_name, tv_dob, tv_cr_flagValue, tv_briefMatch;
        ImageView iv_searchResultImage;
        CircularTextView tv_flag;

        ViewHolder(View view) {
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_alias_name = (TextView) view.findViewById(R.id.tv_alias_name);
            //tv_contact=(TextView)view.findViewById(R.id.tv_contact);
            tv_dob = (TextView) view.findViewById(R.id.tv_dob);
            //           tv_flag = (CircularTextView) view.findViewById(R.id.tv_flag);
            iv_searchResultImage = (ImageView) view.findViewById(R.id.iv_searchResultImage);

            tv_cr_flagValue = (TextView) view.findViewById(R.id.tv_cr_flagValue);

            tv_briefMatch = (TextView) view.findViewById(R.id.tv_briefMatch);

            tv_dob.setEllipsize(TextUtils.TruncateAt.END);

            //           tv_flag.setStrokeWidth(1);

            Constants.changefonts(tv_name, mContext, "Calibri Bold.ttf");
            Constants.changefonts(tv_alias_name, mContext, "Calibri.ttf");
            Constants.changefonts(tv_dob, mContext, "Calibri.ttf");
            Constants.changefonts(tv_cr_flagValue, mContext, "Calibri Bold.ttf");
            Constants.changefonts(tv_briefMatch, mContext, "Calibri.ttf");
        }
    }
}
