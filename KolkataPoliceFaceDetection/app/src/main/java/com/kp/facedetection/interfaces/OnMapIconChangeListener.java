package com.kp.facedetection.interfaces;

/**
 * Created by DAT-165 on 25-05-2017.
 */

public interface OnMapIconChangeListener {
    void mapIconChange(int pos, String po_lat, String po_long);
}
