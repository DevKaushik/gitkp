package com.kp.facedetection.GlobalSearch.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.CableSearchDetails;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by DAT-165 on 05-12-2016.
 */
public class GlobalCableSearchAdapter extends BaseAdapter {

    private Context context;
    private List<CableSearchDetails> cableSearchDetailsList;

    public GlobalCableSearchAdapter(Context context, List<CableSearchDetails> cableSearchDetailsList) {
        this.context = context;
        this.cableSearchDetailsList = cableSearchDetailsList;
    }


    @Override
    public int getCount() {
        int size = (cableSearchDetailsList.size() > 0) ? cableSearchDetailsList.size() : 0;
        return size;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.modified_sdr_search_list_item, null);

            holder.tv_slNo = (TextView) convertView.findViewById(R.id.tv_slNo);
            holder.tv_holderName = (TextView) convertView.findViewById(R.id.tv_name);  // set cable holderName to name field
            holder.tv_address = (TextView) convertView.findViewById(R.id.tv_address); //set holder address to address field
            holder.tv_phoneNo = (TextView) convertView.findViewById(R.id.tv_mobileNo);  // set phoneNo to mobile field

            Constants.changefonts(holder.tv_slNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_holderName, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_address, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_phoneNo, context, "Calibri.ttf");

            convertView.setTag(holder);
        }
        else{

            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);
        String address = "";

        holder.tv_slNo.setText(sl_no);

        if(cableSearchDetailsList.get(position).getCable_holderName() != null)
            holder.tv_holderName.setText("HOLDER NAME: "+cableSearchDetailsList.get(position).getCable_holderName().trim());

        if(cableSearchDetailsList.get(position).getCable_holderMobileNo() != null)
            holder.tv_phoneNo.setText("MOBILE NO: "+cableSearchDetailsList.get(position).getCable_holderMobileNo().trim());

        /*
        *   Setting address part by concat all address related fields
        * */
        if(cableSearchDetailsList.get(position).getCable_holderAddress() != null)
            address = cableSearchDetailsList.get(position).getCable_holderAddress().trim();

        if (cableSearchDetailsList.get(position).getCable_holderCity() != null)
            address = address + ", " + cableSearchDetailsList.get(position).getCable_holderCity().trim();

        if(cableSearchDetailsList.get(position).getCable_holderState() != null)
            address = address + ", "+ cableSearchDetailsList.get(position).getCable_holderState().trim();

        if (cableSearchDetailsList.get(position).getCable_holderPincode() != null)
            address = address + ", "+ cableSearchDetailsList.get(position).getCable_holderPincode().trim();

        holder.tv_address.setText("ADDRESS: "+address);

        return convertView;
    }


    class ViewHolder {

        TextView tv_slNo,tv_holderName,tv_address,tv_phoneNo;
    }
}
