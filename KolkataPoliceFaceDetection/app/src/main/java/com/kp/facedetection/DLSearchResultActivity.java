package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kp.facedetection.adapter.DLSearchAdapter;
import com.kp.facedetection.model.DLSearchDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class DLSearchResultActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, Observer {

    private String[] keys;
    private String[] values;
    private String pageno;
    private String totalResult="";
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private TextView tv_resultCount;
    private ListView lv_DLSearchList;
    private Button btnLoadMore;
    private TextView tv_caseName;

    private TextView tv_watermark;

    private List<DLSearchDetails> dlSearchDeatilsList;
    DLSearchAdapter dlSearchAdapter;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sdrsearch_result);
        ObservableObject.getInstance().addObserver(this);
        initialization();
        clickEvents();

    }

    private void initialization(){


        tv_resultCount=(TextView)findViewById(R.id.tv_resultCount);
        lv_DLSearchList = (ListView)findViewById(R.id.lv_sdrSearchList);

        // LoadMore button
        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        keys = getIntent().getStringArrayExtra("keys");
        values = getIntent().getStringArrayExtra("value");
        pageno = getIntent().getStringExtra("pageno");
        totalResult = getIntent().getStringExtra("totalResult");

        dlSearchDeatilsList = (List<DLSearchDetails>) getIntent().getSerializableExtra("DL_SEARCH_LIST");

        Constants.changefonts(tv_resultCount, this, "Calibri Bold.ttf");

        if (totalResult.equalsIgnoreCase("1")) {
            tv_resultCount.setText("Total Result: " + totalResult);
        } else {
            tv_resultCount.setText("Total Results: " + totalResult);
        }

        dlSearchAdapter = new DLSearchAdapter(this,dlSearchDeatilsList);
        lv_DLSearchList.setAdapter(dlSearchAdapter);
        dlSearchAdapter.notifyDataSetChanged();

        if (Integer.parseInt(totalResult) > 20) {

            // Adding Load More button to listview at bottom
            lv_DLSearchList.addFooterView(btnLoadMore);
        }

        lv_DLSearchList.setOnItemClickListener(this);

        /*  Set user name as Watermark  */
        tv_watermark = (TextView)findViewById(R.id.tv_watermark);
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-55);

    }

    private void clickEvents(){

        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // Starting a new async task
                fetchSDRSearchResultPagination();
            }
        });

    }



    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this,Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }


    /**
     * Function to display simple Alert Dialog
     *
     * @param context - application context
     * @param title   - alert dialog title
     * @param message - alert message
     * @param status  - success/failure (used to set icon)
     */
    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }


    private void fetchSDRSearchResultPagination() {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.MODIFIED_METHOD_DOCUMENT_DL_SEARCH);
        taskManager.setDocumentDLSearchPaginarion(true);

        values[4] = (Integer.parseInt(pageno) + 1) + "";

        taskManager.doStartTask(keys, values, true, true);


    }

    public void parseDocumentDLSearchPagination(String result,String[] keys, String[] values) {

        //System.out.println("Document DL Search Result Pagination: " + result);


        if(result != null && !result.equals("")){

            try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.opt("status").toString().equalsIgnoreCase("1")){

                    this.keys=keys;
                    this.values=values;
                    pageno=jObj.opt("page").toString();
                    //totalResult=jObj.opt("count").toString();

                    JSONArray resultArray=jObj.getJSONArray("result");

                    parseDLSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(this,Constants.ERROR_DETAILS_TITLE,Constants.ERROR_MSG_DETAIL,false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }

    }

    private void parseDLSearchResponse(JSONArray resultArray) {

        for(int i=0;i<resultArray.length();i++){

            JSONObject jsonObj=null;

            try {
                jsonObj=resultArray.getJSONObject(i);

                DLSearchDetails dlSearchDetails = new DLSearchDetails();

                String dlpa_address="";
                String dlta_address="";

                if(!jsonObj.optString("DLNO").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_No(jsonObj.optString("DLNO"));
                }

                if(!jsonObj.optString("DLNAME").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_Name(jsonObj.optString("DLNAME"));
                }

                if(!jsonObj.optString("DLSWDOF").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_SwdOf(jsonObj.optString("DLSWDOF"));
                }

                if(!jsonObj.optString("DLDOB").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_Dob(jsonObj.optString("DLDOB"));
                }

                if(!jsonObj.optString("DLSEX").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_Sex(jsonObj.optString("DLSEX"));
                }

                if(!jsonObj.optString("DLPADD1").equalsIgnoreCase("null") && !jsonObj.optString("DLPADD1").equalsIgnoreCase("")){
                    dlpa_address = dlpa_address + jsonObj.optString("DLPADD1");
                }
                if(!jsonObj.optString("DLPADD2").equalsIgnoreCase("null") && !jsonObj.optString("DLPADD2").equalsIgnoreCase("")){
                    dlpa_address = dlpa_address +", "+ jsonObj.optString("DLPADD2");
                }
                if(!jsonObj.optString("DLPADD3").equalsIgnoreCase("null") && !jsonObj.optString("DLPADD3").equalsIgnoreCase("")){
                    dlpa_address = dlpa_address +", "+ jsonObj.optString("DLPADD3");
                }
                if(!jsonObj.optString("DLPPINCD").equalsIgnoreCase("null") && !jsonObj.optString("DLPPINCD").equalsIgnoreCase("")){
                    dlpa_address = dlpa_address +", "+ jsonObj.optString("DLPPINCD");
                }

                dlSearchDetails.setDl_PAddress(dlpa_address);

                if(!jsonObj.optString("DLTADD1").equalsIgnoreCase("null") && !jsonObj.optString("DLTADD1").equalsIgnoreCase("") ){
                    dlta_address = dlta_address + jsonObj.optString("DLTADD1");
                }
                if(!jsonObj.optString("DLTADD2").equalsIgnoreCase("null") && !jsonObj.optString("DLTADD2").equalsIgnoreCase("")){
                    dlta_address = dlta_address +", "+ jsonObj.optString("DLTADD2");
                }
                if(!jsonObj.optString("DLTADD3").equalsIgnoreCase("null") && !jsonObj.optString("DLTADD3").equalsIgnoreCase("")){
                    dlta_address = dlta_address +", "+ jsonObj.optString("DLTADD3");
                }
                if(!jsonObj.optString("DLTPINCD").equalsIgnoreCase("null") && !jsonObj.optString("DLTPINCD").equalsIgnoreCase("")){
                    dlta_address = dlta_address +", "+ jsonObj.optString("DLTPINCD");
                }

                dlSearchDetails.setDl_TAddress(dlta_address);

                if(!jsonObj.optString("DLISSUEAUTHORITY").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_IssueAuthority(jsonObj.optString("DLISSUEAUTHORITY"));
                }

                if(!jsonObj.optString("DLNTVLDTODT").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_Ntvldtodt(jsonObj.optString("DLNTVLDTODT"));
                }

                if(!jsonObj.optString("DLTVLDTODT").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_Tvldtodt(jsonObj.optString("DLTVLDTODT"));
                }

                if(!jsonObj.optString("DLTELE").equalsIgnoreCase("null")){
                    dlSearchDetails.setDl_tele(jsonObj.optString("DLTELE"));
                }

                dlSearchDeatilsList.add(dlSearchDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        int currentPosition = lv_DLSearchList.getFirstVisiblePosition();
        int lastVisiblePosition = lv_DLSearchList.getLastVisiblePosition();

        // Setting new scroll position
        lv_DLSearchList.setSelectionFromTop(currentPosition + 1, 0);

        if((Integer.parseInt(totalResult)-lastVisiblePosition)<20){
            lv_DLSearchList.removeFooterView(btnLoadMore);
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent in = new Intent(DLSearchResultActivity.this, DLSearchDetailsActivity.class);
        in.putExtra("Holder_DLno",dlSearchDeatilsList.get(position).getDl_No());
        in.putExtra("Holder_DlName",dlSearchDeatilsList.get(position).getDl_Name());
        in.putExtra("Holder_DlSwdof",dlSearchDeatilsList.get(position).getDl_SwdOf());
        in.putExtra("Holder_DlDob",dlSearchDeatilsList.get(position).getDl_Dob());
        in.putExtra("Holder_DlSex",dlSearchDeatilsList.get(position).getDl_Sex());
        in.putExtra("Holder_DlPAddress",dlSearchDeatilsList.get(position).getDl_PAddress());
        in.putExtra("Holder_DlTAddress",dlSearchDeatilsList.get(position).getDl_TAddress());
        in.putExtra("Holder_DlAuthority", dlSearchDeatilsList.get(position).getDl_IssueAuthority());
        in.putExtra("Holder_DLNTVLDTODT",dlSearchDeatilsList.get(position).getDl_Ntvldtodt());
        in.putExtra("Holder_DLTVLDTODT",dlSearchDeatilsList.get(position).getDl_Tvldtodt());
        in.putExtra("Holder_DLTELE",dlSearchDeatilsList.get(position).getDl_tele());

        startActivity(in);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }
    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
