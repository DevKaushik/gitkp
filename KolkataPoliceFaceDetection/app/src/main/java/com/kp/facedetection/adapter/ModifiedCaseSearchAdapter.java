package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnMapIconClickListener;
import com.kp.facedetection.model.CaseSearchDetails;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by DAT-165 on 14-07-2016.
 */
public class ModifiedCaseSearchAdapter extends BaseAdapter{

    private Context context;
    List<CaseSearchDetails> caseSearchDetailsList;
    OnMapIconClickListener onMapIconClickListener;

    public ModifiedCaseSearchAdapter(Context context, List<CaseSearchDetails> caseSearchDetailsList) {

        this.context = context;
        this.caseSearchDetailsList = caseSearchDetailsList;
    }
    public void setModifiedCaseSearchAdapter( OnMapIconClickListener onMapIconClickListener){
        this.onMapIconClickListener=onMapIconClickListener;
    }

    @Override
    public int getCount() {

        if(caseSearchDetailsList.size()>0) {
            return caseSearchDetailsList.size();
        }
        else {
            return 0;
        }

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.changed_case_search_list_item, null);


            holder.tv_caseName=(TextView)convertView.findViewById(R.id.tv_caseName);
            holder.tv_caseStatus=(TextView)convertView.findViewById(R.id.tv_caseStatus);
            holder.tv_caseSlno=(TextView)convertView.findViewById(R.id.tv_caseSlno);
            holder.tv_briefMatch=(TextView)convertView.findViewById(R.id.tv_briefMatch);
            holder.iv_map = (ImageView)convertView.findViewById(R.id.iv_map);

            Constants.changefonts(holder.tv_caseName, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_caseStatus, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_caseSlno, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_briefMatch, context, "Calibri.ttf");

            holder.tv_caseName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));

            holder.tv_briefMatch.setVisibility(View.VISIBLE);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String caseDetails = caseSearchDetailsList.get(position).getPs()+" C/No. "+caseSearchDetailsList.get(position).getCaseNo()+" dated "+caseSearchDetailsList.get(position).getCaseDate()+" u/s "+caseSearchDetailsList.get(position).getUnderSection();

        holder.tv_caseSlno.setText(caseSearchDetailsList.get(position).getRowNumber());

        if(caseSearchDetailsList.get(position).getFirStatus() != null && !caseSearchDetailsList.get(position).getFirStatus().equalsIgnoreCase("null") && !caseSearchDetailsList.get(position).getFirStatus().equalsIgnoreCase("")){
            if(caseSearchDetailsList.get(position).getFirStatus().equalsIgnoreCase(Constants.STATUS_INVESTIGATION_IN_PROGRESS))
                holder.tv_caseStatus.setText(Constants.STATUS_PENDING);
            else if(caseSearchDetailsList.get(position).getFirStatus().equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_NON_COGNIZABLE))
                holder.tv_caseStatus.setText(Constants.STATUS_FR_NON_COG);
            else if (caseSearchDetailsList.get(position).getFirStatus().equalsIgnoreCase(Constants.STATUS_CHARGESHEETED))
                holder.tv_caseStatus.setText(Constants.STATUS_CS);
            else if (caseSearchDetailsList.get(position).getFirStatus().equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_CIVIL))
                holder.tv_caseStatus.setText(Constants.STATUS_FR_CIVIL);
            else if (caseSearchDetailsList.get(position).getFirStatus().equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_TRUE))
                holder.tv_caseStatus.setText(Constants.STATUS_FRT);
            else if (caseSearchDetailsList.get(position).getFirStatus().equalsIgnoreCase(Constants.STATUS_FINAL_REPORT_FALSE))
                holder.tv_caseStatus.setText(Constants.STATUS_FR_FALSE);
            else if (caseSearchDetailsList.get(position).getFirStatus().equalsIgnoreCase(Constants.STATUS_TRANSFERRED_TO_OTHER_UNIT)){

                if (caseSearchDetailsList.get(position).getCaseSearchCaseTransferList().size() > 0 ){

                    String trfd_psDetails = "";
                    if(caseSearchDetailsList.get(position).getCaseSearchCaseTransferList().get(0).getPsName() != null
                            && !caseSearchDetailsList.get(position).getCaseSearchCaseTransferList().get(0).getPsName().equalsIgnoreCase("null")
                            && !caseSearchDetailsList.get(position).getCaseSearchCaseTransferList().get(0).getPsName().equalsIgnoreCase("")){

                        trfd_psDetails = caseSearchDetailsList.get(position).getCaseSearchCaseTransferList().get(0).getPsName();

                        if(caseSearchDetailsList.get(position).getCaseSearchCaseTransferList().get(0).getToUnit() != null
                                && !caseSearchDetailsList.get(position).getCaseSearchCaseTransferList().get(0).getToUnit().equalsIgnoreCase("null")
                                && !caseSearchDetailsList.get(position).getCaseSearchCaseTransferList().get(0).getToUnit().equalsIgnoreCase("")){

                            trfd_psDetails = trfd_psDetails + ", " + caseSearchDetailsList.get(position).getCaseSearchCaseTransferList().get(0).getToUnit();
                        }
                        holder.tv_caseStatus.setText(trfd_psDetails);
                    }
                    else if(caseSearchDetailsList.get(position).getCaseSearchCaseTransferList().get(0).getToUnit() != null
                            && !caseSearchDetailsList.get(position).getCaseSearchCaseTransferList().get(0).getToUnit().equalsIgnoreCase("null")
                            && !caseSearchDetailsList.get(position).getCaseSearchCaseTransferList().get(0).getToUnit().equalsIgnoreCase("")){

                        trfd_psDetails = caseSearchDetailsList.get(position).getCaseSearchCaseTransferList().get(0).getToUnit();
                        holder.tv_caseStatus.setText(trfd_psDetails);
                    }
                    else{
                        holder.tv_caseStatus.setText(Constants.STATUS_TRFD);
                    }
                }
                else{
                    holder.tv_caseStatus.setText(Constants.STATUS_TRFD);
                }
            }
            else
                holder.tv_caseStatus.setText(caseSearchDetailsList.get(position).getFirStatus());
        }


        holder.tv_caseName.setText(caseDetails);

        if (caseSearchDetailsList.get(position).getPoLat() != null && !caseSearchDetailsList.get(position).getPoLat().equalsIgnoreCase("") && !caseSearchDetailsList.get(position).getPoLat().equalsIgnoreCase("null")
                && caseSearchDetailsList.get(position).getPoLong() != null && !caseSearchDetailsList.get(position).getPoLong().equalsIgnoreCase("") && !caseSearchDetailsList.get(position).getPoLong().equalsIgnoreCase("null")){
            holder.iv_map.setImageResource(R.drawable.view_map);
        }
        else{
            holder.iv_map.setImageResource(R.drawable.load_map_mod);
        }

        StringBuilder stringBuilder,stringBuilder2;
        int briefList_size= caseSearchDetailsList.get(position).getBriefMatch_list().size() ;

        if (briefList_size > 0) {

            holder.tv_briefMatch.setVisibility(View.VISIBLE);

            stringBuilder= new StringBuilder();
            stringBuilder2= new StringBuilder();
            stringBuilder.append("Brief Match: ");

            for(int j=0;j<briefList_size;j++){

                if(j==briefList_size -1){
                    stringBuilder2.append(caseSearchDetailsList.get(position).getBriefMatch_list().get(j));
                }else{
                    stringBuilder2.append(caseSearchDetailsList.get(position).getBriefMatch_list().get(j)+" / ");
                }
            }
            holder.tv_briefMatch.setText(""+stringBuilder+stringBuilder2);
        }else{
            holder.tv_briefMatch.setVisibility(View.GONE);
        }
        holder.iv_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMapIconClickListener.onMapClick(position);
            }
        });

        return convertView;
    }

    class ViewHolder {

        TextView tv_caseName,tv_caseStatus,tv_caseSlno, tv_briefMatch;
        ImageView iv_map;
    }
}
