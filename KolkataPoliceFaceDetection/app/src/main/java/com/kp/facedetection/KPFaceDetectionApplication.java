package com.kp.facedetection;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDexApplication;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.kp.facedetection.model.CaseSearchDetails;
import com.kp.facedetection.model.MapSearchDetails;
import com.kp.facedetection.services.ChargesheetDueNotificationService;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class KPFaceDetectionApplication extends MultiDexApplication {
	private static Context mContext;
	private Activity mCurrentActivity;
	ArrayList<Activity> openActivityList;
	static KPFaceDetectionApplication mInstance;
	private static boolean activityVisible;
	private static boolean session;

	private Tracker mTracker;

	private ArrayList<MapSearchDetails> mapAllList;
	private ArrayList<CaseSearchDetails> caseMapAllList;

	/**
	 * Gets the default {@link Tracker} for this {@link Application}.
	 * @return tracker
	 */
	synchronized public Tracker getDefaultTracker() {
		if (mTracker == null) {
			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			// Setting mTracker to Analytics Tracker declared in our xml Folder
			mTracker = analytics.newTracker(R.xml.analytics_tracker);
		}
		return mTracker;
	}
	public void onCreate() {
		super.onCreate();
		this.mContext = this;
		mInstance = this;
		openActivityList=new ArrayList<Activity>();

		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.human3)
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.build();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
				.defaultDisplayImageOptions(defaultOptions)
				.build();
		ImageLoader.getInstance().init(config);

	}


	@Override
	public void onTerminate() {
		super.onTerminate();
		 Intent intent = new Intent(this, ChargesheetDueNotificationService.class);
         stopService(intent);
	}

	public static Context getContext() {
		return mContext;
	}

	public Activity getCurrentActivity() {
		return mCurrentActivity;
	}
	public void setCurrentActivity(Activity activity) {
		mCurrentActivity=activity;
	}
	public static KPFaceDetectionApplication getApplication() {
		return mInstance;
	}

	public ArrayList<Activity> getOpenActivityList() {
		return openActivityList;
	}

	public void addActivityToList(Activity activity) {
		
		if(this.openActivityList==null)
			openActivityList=new ArrayList<Activity>();
		openActivityList.add(activity);
	}
	public void removeActivityToList(Activity activity) {
		
		if(this.openActivityList!=null)
			openActivityList.remove(activity);;
	}

	public static boolean isActivityVisible() {
		return activityVisible;
	}

	public static void activityResumed() {
		activityVisible = true;
		System.out.println("App is in foreground");

		if(!Utility.getTimestamp(getApplication()).equalsIgnoreCase("")) {
			System.out.println("Previous time stamp available");
			System.out.println("Current Time Stamp: "+System.currentTimeMillis());
			Constants.time_diff = (System.currentTimeMillis()- Utility.getTimestampValue(getApplication()))/1000;
			System.out.println("Time Diff: "+Constants.time_diff +" s");
			if(Constants.time_diff  > Constants.logout_interval){
				System.out.println("1 min over");
				Constants.prev_timeStampString="";
				Utility.storeTimestampValue(getApplication(), 0L);
				Utility.storeTimestamp(getApplication(), Constants.prev_timeStampString);
				//Utility.logout2(Constants.baseActivity);

				session = false;

			}
			else{
				System.out.println("with in 1 min");
				session = true;
			}

			Constants.prev_timeStampString="";
			Utility.storeTimestampValue(getApplication(), 0L);
			Utility.storeTimestamp(getApplication(),Constants.prev_timeStampString);
		}
		else{
			System.out.println("No previous time stamp available");
			session = true;
		}

	}

	public static void activityPaused() {
		activityVisible = false;
		System.out.println("App is in background");
		Long tsLong = System.currentTimeMillis();
		if(Utility.getTimestamp(getApplication()).equalsIgnoreCase("")) {
			Constants.prev_timeStamp = tsLong;
			Constants.prev_timeStampString = tsLong.toString();
			Utility.storeTimestampValue(getApplication(), Constants.prev_timeStamp);
			Utility.storeTimestamp(getApplication(),Constants.prev_timeStampString);
			System.out.println("Background Time Stamp: " + Utility.getTimestamp(getApplication()));
		}

	}

	public static boolean sessionCheck(){

		return session;

	}



	public void setMapAllListData(ArrayList<MapSearchDetails> mapAllList){
		this.mapAllList = mapAllList;
	}


	public ArrayList<MapSearchDetails> getMapAllListData(){
		return mapAllList;
	}


	public ArrayList<CaseSearchDetails> getCaseMapAllList() {
		return caseMapAllList;
	}

	public void setCaseMapAllList(ArrayList<CaseSearchDetails> caseMapAllList) {
		this.caseMapAllList = caseMapAllList;
	}

}