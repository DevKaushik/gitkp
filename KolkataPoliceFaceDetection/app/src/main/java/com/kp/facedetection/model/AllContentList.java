package com.kp.facedetection.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by DAT-Asset-131 on 18-03-2016.
 */
public class AllContentList implements Serializable {


    private String status="";
    private String message= "";
    private ArrayList<PoliceStationList> obj_policeStationList = new ArrayList<PoliceStationList>();

    private ArrayList<CrimeCategoryList> obj_categoryCrimeList = new ArrayList<CrimeCategoryList>();

    private ArrayList<DivisionList> obj_divisionList = new ArrayList<DivisionList>();

    private ArrayList<ModusOperandiList> obj_modusOperandiList = new ArrayList<ModusOperandiList>();

    private ArrayList<IOList> obj_ioList = new ArrayList<IOList>();

    private ArrayList<ClassListCriminal> obj_classList = new ArrayList<ClassListCriminal>();

    public ArrayList<IOList> getObj_ioList() {
        return obj_ioList;
    }

    public void setObj_ioList(IOList obj) {
        obj_ioList.add(obj);
    }

    public ArrayList<ModusOperandiList> getObj_modusOperandiList() {
        return obj_modusOperandiList;
    }

    public void setObj_modusOperandiList(ModusOperandiList obj) {
        obj_modusOperandiList.add(obj);
    }

    public ArrayList<CrimeCategoryList> getObj_categoryCrimeList() {
        return obj_categoryCrimeList;
    }

    public void setObj_categoryCrimeList(CrimeCategoryList obj) {
        obj_categoryCrimeList.add(obj);
    }


    public ArrayList<PoliceStationList> getObj_policeStationList() {
        return obj_policeStationList;
    }

    public void setObj_policeStationList(PoliceStationList obj) {
        obj_policeStationList.add(obj);
    }

    public ArrayList<DivisionList> getObj_DivisionList() {
        return obj_divisionList;
    }

    public void setObj_DivisionList(DivisionList obj) {
        obj_divisionList.add(obj);
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public ArrayList<ClassListCriminal> getObj_classList() {
        return obj_classList;
    }

    public void setObj_classList(ClassListCriminal obj) {
        obj_classList.add(obj);
    }

}
