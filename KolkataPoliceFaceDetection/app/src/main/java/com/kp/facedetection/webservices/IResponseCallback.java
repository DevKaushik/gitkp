package com.kp.facedetection.webservices;

public interface IResponseCallback {
	public void onSuccess(String response,String[] keys,String[] values);
	public void onFailure(String response);
}
