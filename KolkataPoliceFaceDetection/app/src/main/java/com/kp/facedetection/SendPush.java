package com.kp.facedetection;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONObject;

import java.util.Observable;
import java.util.Observer;

public class SendPush extends BaseActivity implements View.OnClickListener, Observer {

//    View rootLayout;
    EditText et_from, et_subject, et_details;
    Button btn_send;

    private String userName="";
    private String loginNumber="";
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_push);
        ObservableObject.getInstance().addObserver(this);

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = this.getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        tv_username.setText("Welcome "+userName);
        tv_loginCount.setText("Login Count: "+loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        //KPFaceDetectionApplication.getApplication().addActivityToList(getActivity());

        initView();

    }

    private void initView() {
        et_details = (EditText) findViewById(R.id.et_details);
        et_from = (EditText) findViewById(R.id.et_from);
        et_subject = (EditText) findViewById(R.id.et_subject);
        btn_send = (Button) findViewById(R.id.btn_send);
        Log.e("object", et_details + "**" + et_from + "**" + et_subject + "**"
                + btn_send);

        Constants.buttonEffect(btn_send);
        btn_send.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send:
               // sendPushTask();
                Utility.showAlertDialog(SendPush.this,"Server Problem!!!","Sorry, unable to send push at this moment.Please try again later.",false);
                break;

            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
                break;


            default:
                break;
        }
    }

    private void sendPushTask() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_SEND_PUSH);
        taskManager.setSendPush(true);
        // [subject=>
        // 1,description=>'dsadasas',sendername=>'bisu',userid=>1,'devicetype'=>'android']
        String[] keys = { "user_id", "device_type", "sendername", "subject",
                "description" };
        String[] values = { Utility.getUserInfo(this).getUserId().trim(), "android",
                et_from.getText().toString().trim(),
                et_subject.getText().toString().trim(),
                et_details.getText().toString().trim() };
        taskManager.doStartTask(keys, values, true);
    }

    public void parseNotificationResponse(String response) {
        Log.e("Response", "Response " + response);
        // {"success":true,"message":"Success","result":{"notificationid":34}}
        try {
            JSONObject jObj = new JSONObject(response);
            if (jObj != null && jObj.opt("status").toString().equalsIgnoreCase("1")) {
                et_details.setText("");
                et_from.setText("");
                et_subject.setText("");
                Toast.makeText(this, R.string.txt_successful_push,
                        Toast.LENGTH_LONG).show();
//				notificationDetaileTask(jObj.optJSONObject("result").optString(
//						"notificationid"));
            } else {
                Toast.makeText(this, "Message failed to deliever!",
                        Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Message failed to deliever!",
                    Toast.LENGTH_LONG).show();
        }

    }


    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
