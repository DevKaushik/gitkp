package com.kp.facedetection.interfaces;

import android.graphics.Bitmap;

/**
 * Created by Kaushik on 20-12-2016.
 */
public interface OnCameraListener {

    void onBitmapReceivedFromCamera(Bitmap mBitmap, String mPath);
    void onBitmapReceivedFromGallery(Bitmap mBitmap, String mPath);

}
