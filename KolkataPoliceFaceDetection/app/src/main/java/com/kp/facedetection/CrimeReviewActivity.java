package com.kp.facedetection;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kp.facedetection.adapter.RecyclerAdapterForCategory;
import com.kp.facedetection.interfaces.OnItemClickListenerForCrimeReview;
import com.kp.facedetection.model.CrimeReviewDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

public class CrimeReviewActivity extends BaseActivity implements View.OnClickListener, OnItemClickListenerForCrimeReview, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private int year, month, day;
    private String fromDate, toDate;

    private TextView tv_fromDate, tv_toDate;
    private RelativeLayout rl_fromDate, rl_toDate;
    private RecyclerView recycle_crimeList;
    private Button bt_apply;
    private LayoutManager mLayoutManager;
    private RecyclerAdapterForCategory crimeReviewAdapter;
    private ArrayList<CrimeReviewDetails> crimeReviewDetailsArrayList = new ArrayList<>();

    private String select_div ="";
    private String select_ps ="";
    private String select_crime ="";
    TextView chargesheetNotificationCount;
    String notificationCount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crime_review_layout);
        ObservableObject.getInstance().addObserver(this);
        setToolBar();
        initViews();
    }


    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }


    private void initViews() {

        select_div = getIntent().getExtras().getString("SELECTED_DIV_CRIME_REV");
        select_ps = getIntent().getExtras().getString("SELECTED_PS_CRIME_REV");
        select_crime = getIntent().getExtras().getString("SELECTED_CRIME_CRIME_REV");

        tv_fromDate = (TextView) findViewById(R.id.tv_fromDate);
        tv_toDate = (TextView) findViewById(R.id.tv_toDate);
        getCurrentDate();

        rl_fromDate = (RelativeLayout) findViewById(R.id.rl_fromDate);
        rl_fromDate.setOnClickListener(this);

        rl_toDate = (RelativeLayout) findViewById(R.id.rl_toDate);
        rl_toDate.setOnClickListener(this);

        bt_apply = (Button) findViewById(R.id.bt_apply);
        Constants.buttonEffect(bt_apply);
        bt_apply.setOnClickListener(this);

        recycle_crimeList = (RecyclerView) findViewById(R.id.recycle_crimeList);
        mLayoutManager = new LinearLayoutManager(this);
        recycle_crimeList.setLayoutManager(mLayoutManager);
        //recycle_crimeList.setItemAnimator(new DefaultItemAnimator());
        recycle_crimeList.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));

        crimeReviewAdapter = new RecyclerAdapterForCategory(this, crimeReviewDetailsArrayList);
        recycle_crimeList.setAdapter(crimeReviewAdapter);
        crimeReviewAdapter.setClickListener(this);
    }


    private void getCurrentDate() {

        // set To date
        Calendar calender = Calendar.getInstance();
        year = calender.get(Calendar.YEAR);
        month = calender.get(Calendar.MONTH) + 1;
        day = calender.get(Calendar.DAY_OF_MONTH);

        String dtStr1 = dateFormat(day, month, year);
        tv_toDate.setText(dtStr1);

        // set From date
        Calendar calPre = Calendar.getInstance();
        calPre.add(Calendar.DATE, -30);
        year = calPre.get(Calendar.YEAR);
        month = calPre.get(Calendar.MONTH) + 1;
        day = calPre.get(Calendar.DAY_OF_MONTH);
        String dtStr2 = dateFormat(day, month, year);
        tv_fromDate.setText(dtStr2);
    }


    private void setToDate(final TextView tv_view, boolean isMaxCurrentDate) {
        Calendar calender = Calendar.getInstance();
        DatePickerDialog mDialog = new DatePickerDialog(CrimeReviewActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        month++;

                        String dt = dateFormat(day, month, year);
                        tv_view.setText(dt);
                    }
                }, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender
                .get(Calendar.DAY_OF_MONTH));
        if (isMaxCurrentDate) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                mDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                mDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            }
        }
        mDialog.show();
    }

    private void setFromDate(final TextView tv_view) {
        Calendar calender = Calendar.getInstance();
        DatePickerDialog mDialog = new DatePickerDialog(CrimeReviewActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        month++;

                        String dt = dateFormat(day, month, year);
                        tv_view.setText(dt);
                    }
                }, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender
                .get(Calendar.DAY_OF_MONTH));
        mDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDialog.show();
    }


    private String dateFormat(int day, int month, int year) {
        String dateStr;
        if (day <= 9) {
            if (month <= 9)
                dateStr = "0" + day + "-0" + month + "-" + year;
            else
                dateStr = "0" + day + "-" + month + "-" + year;
        } else {
            if (month <= 9)
                dateStr = day + "-0" + month + "-" + year;
            else
                dateStr = day + "-" + month + "-" + year;
        }
        return dateStr;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.rl_fromDate:
                setFromDate(tv_fromDate);
                break;

            case R.id.rl_toDate:
                setToDate(tv_toDate, true);
                break;

            case R.id.bt_apply:
                fetchCrimeReviewData();
                break;
            case R.id.charseet_due_notification:
                getChargesheetdetailsAsperRole(this);
        }
    }


    private void fetchCrimeReviewData() {

        fromDate = tv_fromDate.getText().toString().trim();
        toDate = tv_toDate.getText().toString().trim();

        if (!toDate.equalsIgnoreCase("") && !fromDate.equalsIgnoreCase("")) {
            boolean date_result_flag = DateUtils.dateComparison(fromDate, toDate);
            System.out.println("DateComparison: " + date_result_flag);
            if (!date_result_flag) {
                Utility.showAlertDialog(this, "Alert !!!", "From date can't be greater than To date", false);
            } else {
                fetchCrimeReviewResult();
            }
        }
    }


    private void fetchCrimeReviewResult() {

        if (!Constants.internetOnline(this)) {
            Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIME_REVIEW);
        taskManager.setCrimeReview(true);

        String[] keys = {"from_date","to_date","div","ps","category"};
        String[] values = {fromDate.trim(),toDate.trim(), select_div.trim(),select_ps.trim(), select_crime.trim()};
        taskManager.doStartTask(keys, values, true);
    }

    public void parseCrimeReviewResponse(String response) {
        //System.out.println("parseCrimeReviewResponse" + response);

        if (response != null && !response.equals("")) {

            try {
                JSONObject jObj = new JSONObject(response);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    JSONArray resultAry = jObj.getJSONArray("result");
                    parseListCrimeCategory(resultAry);
                }
                else{
                    Utility.showAlertDialog(CrimeReviewActivity.this,Constants.ERROR_DETAILS_TITLE,Constants.ERROR_MSG_DETAIL,false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_EXCEPTION_MSG, false);
            }
        }
    }

    private void parseListCrimeCategory(JSONArray resultAry) {

        crimeReviewDetailsArrayList.clear();
        Log.e("CATEGORY",String.valueOf(resultAry));
        for(int i = 0; i<resultAry.length(); i++){

            CrimeReviewDetails crimeReviewDetails = new CrimeReviewDetails(Parcel.obtain());
            try {
                JSONObject jObj = resultAry.getJSONObject(i);
                if(jObj.optString("CATEGORY") != null && !jObj.optString("CATEGORY").equalsIgnoreCase("") && !jObj.optString("CATEGORY").equalsIgnoreCase("null")
                        && !jObj.optString("CATEGORY").equalsIgnoreCase("0")){
                    crimeReviewDetails.setCrimeCategory(jObj.optString("CATEGORY"));
                    if(jObj.optString("CASES_CURRENT_YEAR") != null && !jObj.optString("CASES_CURRENT_YEAR").equalsIgnoreCase("") && !jObj.optString("CASES_CURRENT_YEAR").equalsIgnoreCase("null") && jObj.optString("CASES_PREVIOUS_YEAR") != null && !jObj.optString("CASES_PREVIOUS_YEAR").equalsIgnoreCase("") && !jObj.optString("CASES_PREVIOUS_YEAR").equalsIgnoreCase("null")){
                        crimeReviewDetails.setCountOfCrime(jObj.optString("CASES_CURRENT_YEAR"));
                        crimeReviewDetails.setPrevCountOfCrime(jObj.optString("CASES_PREVIOUS_YEAR"));
                    }
                    crimeReviewDetailsArrayList.add(crimeReviewDetails);
                }
            }
            catch(JSONException ex){
                ex.printStackTrace();
            }
        }
        crimeReviewAdapter.notifyDataSetChanged();
        Log.e("CATEGORIES",String.valueOf(crimeReviewDetailsArrayList));

    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
           /* case R.id.menu_send_push:
                //tabHost.setCurrentTab(2);
                //startActivity(new Intent(this, SendPushActivity.class));
                //	pushFragments("Push", new SendPushActivity(), true, false);
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this, Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View view, int position) {
        final CrimeReviewDetails crimeReviewDetails = crimeReviewDetailsArrayList.get(position);
        String category = crimeReviewDetails.getCrimeCategory();

        Intent intent = new Intent(CrimeReviewActivity.this, CrimeReviewDetailsActivity.class);
        intent.putExtra("FROM_DATE",fromDate);
        intent.putExtra("TO_DATE",toDate);
        intent.putExtra("CATEGORY",category);
        intent.putExtra("SELECTED_DIV",select_div);
        intent.putExtra("SELECTED_PS",select_ps);

        startActivity(intent);
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }
    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
