package com.kp.facedetection.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DAT-165 on 24-03-2017.
 */

public class CourtTypeListDetails implements Parcelable {

    private String caseType = "";

    public CourtTypeListDetails(Parcel in) {
        caseType = in.readString();
    }

    public static final Creator<CourtTypeListDetails> CREATOR = new Creator<CourtTypeListDetails>() {
        @Override
        public CourtTypeListDetails createFromParcel(Parcel in) {
            return new CourtTypeListDetails(in);
        }

        @Override
        public CourtTypeListDetails[] newArray(int size) {
            return new CourtTypeListDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(caseType);
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }
}
