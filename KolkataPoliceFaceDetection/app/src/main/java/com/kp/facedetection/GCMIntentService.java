package com.kp.facedetection;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.kp.facedetection.utility.Utility;

public class GCMIntentService extends GCMBaseIntentService {
	final String TAG = "GCM";

	public GCMIntentService() {
		super();
	}

	public static final String SENDER_ID = "287249696857";//"739765573785";//"633257529368";

	@Override
	protected void onRegistered(Context mcontext, String registrationId) {
		if (registrationId != null) {
			Log.e(TAG, "Device Token " + registrationId);
		}
	}
	 @Override
	  protected String[] getSenderIds(Context context) {
	     String[] ids = new String[1];
	     ids[0] = SENDER_ID;
	     return ids;
	  }
	
	@SuppressWarnings("unused")
	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.e(TAG, "Message Received");
		// Log.e("Data",
		// "Data "+intent.getExtras().getString("msg")+" "+intent.getExtras().getString("alertid")+" "+intent.getExtras().getString("type"));
		/********
		 * ask about home from vikas
		 */
//		Utility.showNotification(context, SplashActivity.class, intent);
	}

	@Override
	protected void onError(Context context, String errorId) {
		Log.e(TAG, "ERROR: " + errorId);
		System.exit(0);
	}

	@Override
	protected void onUnregistered(Context arg0, String arg1) {

	}

}
