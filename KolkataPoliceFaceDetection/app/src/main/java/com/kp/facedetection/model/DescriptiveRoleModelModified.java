package com.kp.facedetection.model;

import java.util.ArrayList;

/**
 * Created by DAT-Asset-128 on 07-10-2017.
 */

public class DescriptiveRoleModelModified {
    String category;
    String indValue;
    ArrayList<String> value =new ArrayList<>();
    ArrayList<String> isSelected=new ArrayList<>();

    public String getIndValue() {
        return indValue;
    }

    public void setIndValue(String indValue) {
        this.indValue = indValue;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ArrayList<String> getValue() {
        return value;
    }

    public void setValue(ArrayList<String> value) {
        this.value = value;
    }

    public ArrayList<String> getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(ArrayList<String> isSelected) {
        this.isSelected = isSelected;
    }
}
