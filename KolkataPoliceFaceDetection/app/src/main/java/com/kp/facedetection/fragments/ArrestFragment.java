package com.kp.facedetection.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.adapter.ExpandableListAdapterForArrest;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by DAT-Asset-131 on 01-04-2016.
 */
public class ArrestFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String TRNID = "TRNID";
    public static final String PS_CODE = "PS_CODE";
    private String trnid="";
    private String ps_code="";
    private String details="";

  /*  private TextView tv_details;

    private RelativeLayout relative_details;*/
    private ExpandableListView lvExp;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    ExpandableListAdapterForArrest listAdapter;
    private TextView tv_noData;

    private int mPage;

    public static ArrestFragment newInstance(int page,String trnid, String ps_code ) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        args.putString(TRNID, trnid);
        args.putString(PS_CODE, ps_code);
        ArrestFragment fragment = new ArrestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        trnid =  getArguments().getString(TRNID).replace("TRNID: ", "");
        ps_code = getArguments().getString(PS_CODE).replace("PSCODE: ","");
    }

    // Inflate the fragment layout we defined above for this fragment
    // Set the associated text for the title
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       // View view = inflater.inflate(R.layout.fragment_details, container, false);
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

      /*  tv_details = (TextView) view.findViewById(R.id.tv_detail);
        tv_noData = (TextView) view.findViewById(R.id.tv_noData);
        relative_details = (RelativeLayout)view.findViewById(R.id.relative_details);

        Utility.changefonts(tv_noData, getActivity(), "Calibri.ttf");*/

       // getArrestDetails(trnid, ps_code);

        tv_noData = (TextView) view.findViewById(R.id.tv_noData);
        lvExp = (ExpandableListView) view.findViewById(R.id.lvExp);

       // Utility.changefonts(tv_noData, getActivity(), "Calibri.ttf");

        tv_noData.setVisibility(View.GONE);
        lvExp.setVisibility(View.GONE);


       // getModifiedArrestDetails(trnid, ps_code);

    }


    private void getArrestDetails(String trnid, String ps_code){

        this.trnid = trnid;
        this.ps_code = ps_code;

        System.out.println("TRNID: " + trnid);
        System.out.println("PSCODE: " + ps_code);

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_DETAILS);
        taskManager.setCriminalDetailsArrest(true);

        String[] keys = { "flag","pscode","trnid" };
        String[] values = { "A", ps_code.trim(), trnid.trim() };

        taskManager.doStartTask(keys, values, true);
    }

    public void parseArrestResultResponse(String response) {

        //System.out.println("parseArrestResultResponse: " + response);

       /* if (response != null && !response.equals("")) {

            try {
                JSONObject jObj = new JSONObject(response);
                if(jObj.optString("status").equalsIgnoreCase("1")){

                    relative_details.setVisibility(View.VISIBLE);
                    tv_noData.setVisibility(View.GONE);

                    JSONArray result = jObj.getJSONArray("result");
                    JSONObject criminal_details = result.getJSONObject(0);
                    createTextResults(criminal_details);

                }
                else{
                    //Toast.makeText(getActivity(), "Sorry, no arrest details found", Toast.LENGTH_LONG).show();

                    relative_details.setVisibility(View.GONE);
                    tv_noData.setVisibility(View.VISIBLE);
                    tv_noData.setText("Sorry, no arrest details found");
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        else {
            Toast.makeText(getActivity(), "Some error has been encountered", Toast.LENGTH_LONG).show();
        }
*/
    }

    private void createTextResults(JSONObject criminal_details){

        if (criminal_details.optString("ARREST_DATE")!=null && !criminal_details.optString("ARREST_DATE").equalsIgnoreCase("null"))
            details ="ARREST_DATE : "+ criminal_details.optString("ARREST_DATE");
        if (criminal_details.optString("HALF")!=null && !criminal_details.optString("HALF").equalsIgnoreCase("null"))
            details = details + "\n\n" + "HALF : "+ criminal_details.optString("HALF");
        if (criminal_details.optString("UNIT")!=null && !criminal_details.optString("UNIT").equalsIgnoreCase("null"))
            details = details + "\n\n" + "UNIT : "+ criminal_details.optString("UNIT");
        if (criminal_details.optString("PS")!=null && !criminal_details.optString("PS").equalsIgnoreCase("null"))
            details = details + "\n\n" + "PS : "+ criminal_details.optString("PS");
        if (criminal_details.optString("ARRESTEE")!=null && !criminal_details.optString("ARRESTEE").equalsIgnoreCase("null"))
            details = details + "\n\n" + "ARRESTEE : "+ criminal_details.optString("ARRESTEE");
        if (criminal_details.optString("ALIASNAME")!=null && !criminal_details.optString("ALIASNAME").equalsIgnoreCase("null"))
            details = details + "\n\n" + "ALIASNAME : "+ criminal_details.optString("ALIASNAME");
        if (criminal_details.optString("SEX")!=null && !criminal_details.optString("SEX").equalsIgnoreCase("null"))
            details = details + "\n\n" + "SEX : "+ criminal_details.optString("SEX");
        if (criminal_details.optString("AGE")!=null && !criminal_details.optString("AGE").equalsIgnoreCase("null"))
            details = details + "\n\n" + "AGE : "+ criminal_details.optString("AGE");
        if (criminal_details.optString("FATHER_HUSBAND")!=null && !criminal_details.optString("FATHER_HUSBAND").equalsIgnoreCase("null"))
            details = details + "\n\n" + "FATHER_HUSBAND : "+ criminal_details.optString("FATHER_HUSBAND");
        if (criminal_details.optString("ADDRESS")!=null && !criminal_details.optString("ADDRESS").equalsIgnoreCase("null"))
            details = details + "\n\n" + "ADDRESS : "+ criminal_details.optString("ADDRESS");
        if (criminal_details.optString("CASEREF")!=null && !criminal_details.optString("CASEREF").equalsIgnoreCase("null"))
            details = details + "\n\n" + "CASEREF : "+ criminal_details.optString("CASEREF");
        if (criminal_details.optString("PLACE_ARREST")!=null && !criminal_details.optString("PLACE_ARREST").equalsIgnoreCase("null"))
            details = details + "\n\n" + "PLACE_ARREST : "+ criminal_details.optString("PLACE_ARREST");
        if (criminal_details.optString("CRIME_HEAD")!=null && !criminal_details.optString("CRIME_HEAD").equalsIgnoreCase("null"))
            details = details + "\n\n" + "CRIME_HEAD : "+ criminal_details.optString("CRIME_HEAD");
        if (criminal_details.optString("TRNID")!=null && !criminal_details.optString("TRNID").equalsIgnoreCase("null"))
            details = details + "\n\n" + "TRNID : "+ criminal_details.optString("TRNID");
        if (criminal_details.optString("PSCODE")!=null && !criminal_details.optString("PSCODE").equalsIgnoreCase("null"))
            details = details + "\n\n" + "PSCODE : "+ criminal_details.optString("PSCODE");
        if (criminal_details.optString("REPDATE")!=null && !criminal_details.optString("REPDATE").equalsIgnoreCase("null"))
            details = details + "\n\n" + "REPDATE : "+ criminal_details.optString("REPDATE");
        if (criminal_details.optString("CASEDATE")!=null && !criminal_details.optString("CASEDATE").equalsIgnoreCase("null"))
            details = details + "\n\n" + "CASEDATE : "+ criminal_details.optString("CASEDATE");
        if (criminal_details.optString("RELIGION")!=null && !criminal_details.optString("RELIGION").equalsIgnoreCase("null"))
            details = details + "\n\n" + "RELIGION : "+ criminal_details.optString("RELIGION");
        if (criminal_details.optString("PROFESSION")!=null && !criminal_details.optString("PROFESSION").equalsIgnoreCase("null"))
            details = details + "\n\n" + "PROFESSION : "+ criminal_details.optString("PROFESSION");
        if (criminal_details.optString("LOCAL_OUTSIDE")!=null && !criminal_details.optString("LOCAL_OUTSIDE").equalsIgnoreCase("null"))
            details = details + "\n\n" + "LOCAL_OUTSIDE : "+ criminal_details.optString("LOCAL_OUTSIDE");
        if (criminal_details.optString("UNDER_SECTION")!=null && !criminal_details.optString("UNDER_SECTION").equalsIgnoreCase("null"))
            details = details + "\n\n" + "UNDER_SECTION : "+ criminal_details.optString("UNDER_SECTION");
        if (criminal_details.optString("ARRAGAINST")!=null && !criminal_details.optString("ARRAGAINST").equalsIgnoreCase("null"))
            details = details + "\n\n" + "ARRAGAINST : "+ criminal_details.optString("ARRAGAINST");
        if (criminal_details.optString("CASE_YR")!=null && !criminal_details.optString("CASE_YR").equalsIgnoreCase("null"))
            details = details + "\n\n" + "CASE_YR : "+ criminal_details.optString("CASE_YR");

        //tv_details.setText(details);

    }

    private void getModifiedArrestDetails(String trnid, String ps_code){

        this.trnid = trnid;
        this.ps_code = ps_code;

        System.out.println("TRNID: " + trnid);
        System.out.println("PSCODE: " + ps_code);

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_CRIMINAL_ARREST_DETAILS);
        taskManager.setModifiedCriminalDetailsArrest(true);

        String[] keys = { "pscode","trnid" };
        String[] values = {ps_code.trim(), trnid.trim() };
        //String[] values = {"G", "608" };

        taskManager.doStartTask(keys, values, true);
    }

    public void parseModifiedArrestResultResponse(String response) {

        //System.out.println("parseModifiedArrestResultResponse: " + response);

        if (response != null && !response.equals("")) {

            try {
                JSONObject jObj = new JSONObject(response);
                if(jObj.optString("status").equalsIgnoreCase("1")){
                    tv_noData.setVisibility(View.GONE);
                    lvExp.setVisibility(View.VISIBLE);

                    JSONObject result = jObj.getJSONObject("result");
                    parseJSONObject(result);


                }
                else{
                    tv_noData.setVisibility(View.VISIBLE);
                    lvExp.setVisibility(View.GONE);
                    tv_noData.setText("Sorry, no arrest details found");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        else {
            Utility.showToast(getActivity(), Constants.ERROR_MSG_DETAIL, "long");
        }

    }

    private void parseJSONObject(JSONObject obj){

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        if(obj.length()>0){

            listDataHeader.add("Arrest: "+obj.optString("ARREST_DATE"));

            Iterator iterator_obj = obj.keys();
            List<String> list_obj = new ArrayList<>();

            while(iterator_obj.hasNext()){
                String key = (String)iterator_obj.next();

                if(!obj.optString(key).equalsIgnoreCase("null")) {

                    String value = obj.optString(key);
                    list_obj.add(key.replace("_"," ") + "^" + value);
                }

            }

            listDataChild.put(listDataHeader.get(listDataHeader.indexOf("Arrest: "+obj.optString("ARREST_DATE"))), list_obj);

        }

        listAdapter = new ExpandableListAdapterForArrest(getActivity(), listDataHeader, listDataChild);

        // setting list adapter
        lvExp.setAdapter(listAdapter);

    }

}
