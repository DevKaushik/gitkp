package com.kp.facedetection;


import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kp.facedetection.adapter.CustomDialogAdapterForHotel;
import com.kp.facedetection.interfaces.OnShowAlertForProceedResult;
import com.kp.facedetection.model.HotelDetails;
import com.kp.facedetection.model.HotelsList;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HotelSearchActivity extends BaseActivity implements OnShowAlertForProceedResult,SearchView.OnQueryTextListener,View.OnClickListener{

    private EditText et_name,et_age,et_fatherName,et_address,et_idNumber,et_contact,et_hotelName;
    private TextView tv_arrivalDate,tv_depatureDate;
    private ImageView iv_cancel_age,iv_cancel_arrival_date,iv_cancel_departure_date,iv_cancel_hotel_name;
    private RadioGroup radioGroup;
    String name ="",age="",fathername="",address="",idNumber="",contact="",hotelName="",arrivalDate="",departureDate="";
    private String pageno="";
    private String totalResult="";
    private String[] keys;
    private String[] values;
    String userId="";
    private Button btn_Search, btn_reset;
    String devicetoken ="";
    String auth_key="";
    String visitoreType="1";
    ListView lv_dialog;
    boolean hotel_status;
    private String hotelString = "";

    private String[] ageArray= {"5-10", "11-20", "21-30", "31-40", "41-50","51-60","61-70","71-80","81-90","91-100"};
    private ArrayList<String> ageArrayList;
    private ArrayList<String> hotelArrayList;
    private String [] hotelArrayString;
    public static ArrayList<HotelsList> hotel_namesList=new ArrayList<HotelsList>();
    TextView chargesheetNotificationCount;
    String notificationCount="";
    private String userName = "";
    private String loginNumber = "";
    private String user_Id = "";
    private String appVersion = "";
    private String log_response="";

    ArrayList<HotelDetails> hotelDetailsArrayList ;
    CustomDialogAdapterForHotel customDialogAdapterForHotel;
    public HotelSearchActivity() {
        // Required empty public constructor
    }
    public static HotelSearchActivity newInstance(){
        HotelSearchActivity HotelSearchActivity =new HotelSearchActivity();
        return HotelSearchActivity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_hotel_search);

        initView();
        //return rootLayout;
    }

    public void initView() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();
            user_Id = Utility.getUserInfo(this).getUserId();

        } catch (Exception e) {
            e.printStackTrace();
        }

        appVersion = Utility.getAppVersion(this);

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);
        tv_appVersion.setText("Version: " + appVersion);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        userId= Utility.getUserInfo(this).getUserId();
        et_name = (EditText) findViewById(R.id.et_holderName);
        et_age = (EditText) findViewById(R.id.et_holderAge);
        et_fatherName= (EditText) findViewById(R.id.et_holderFatherName);
        et_address = (EditText) findViewById(R.id.et_address);
        et_idNumber= (EditText) findViewById(R.id.et_Id_Number);
        et_contact= (EditText) findViewById(R.id.et_Contact);
        et_hotelName= (EditText) findViewById(R.id.et_hotel_name);
        tv_arrivalDate=(TextView) findViewById(R.id.tv_date_arrival_val);
        tv_depatureDate= (TextView) findViewById(R.id.tv_date_departure_val);
        iv_cancel_age=(ImageView) findViewById(R.id.iv_cancel_age);
        iv_cancel_age.setClickable(true);
        iv_cancel_arrival_date=(ImageView) findViewById(R.id.iv_cancel_arrival_date);
        iv_cancel_departure_date=(ImageView) findViewById(R.id.iv_cancel_departure_date);
        iv_cancel_hotel_name=(ImageView) findViewById(R.id.iv_cancel_hotel_name);


        radioGroup=(RadioGroup) findViewById(R.id.rd_Id_Number_option);

        btn_Search = (Button) findViewById(R.id.btn_Search);
        btn_reset = (Button) findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_Search);
        Constants.buttonEffect(btn_reset);
        backgroundSetForBelowApi();
        ageArrayList = new ArrayList<String>(Arrays.asList(ageArray));
        clickEvents();
    }

    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }

    private void backgroundSetForBelowApi(){
        if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.LOLLIPOP){
            // Do something for lollipop and above versions
            et_name.setBackgroundResource(R.drawable.text_underline_selector);
            et_age .setBackgroundResource(R.drawable.text_underline_selector);
            et_fatherName.setBackgroundResource(R.drawable.text_underline_selector);
            et_address.setBackgroundResource(R.drawable.text_underline_selector);
            et_idNumber.setBackgroundResource(R.drawable.text_underline_selector);
            et_contact.setBackgroundResource(R.drawable.text_underline_selector);
            et_hotelName.setBackgroundResource(R.drawable.text_underline_selector);
            tv_arrivalDate.setBackgroundResource(R.drawable.text_underline_selector);
            tv_depatureDate.setBackgroundResource(R.drawable.text_underline_selector);

        }else{
            Log.e("DAPL:","API above LOLLIPOP");
        }
    }

    private void clickEvents(){


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if(checkedId == R.id.rd_national)
                {
                    visitoreType="1";
                }
                if(checkedId == R.id.rd_foreign)
                {
                    visitoreType="2";
                }
            }
        });
        btn_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name = et_name.getText().toString().trim();
                age= et_age.getText().toString().trim();
                fathername= et_fatherName.getText().toString().trim();
                address= et_address.getText().toString().trim();
                idNumber= et_idNumber.getText().toString().trim();
                contact= et_contact.getText().toString().trim();
                hotelName= et_hotelName.getText().toString().trim();
                arrivalDate= tv_arrivalDate.getText().toString().trim();
                departureDate= tv_depatureDate.getText().toString().trim();
                if (!departureDate.equalsIgnoreCase("") && !arrivalDate.equalsIgnoreCase("")) {
                    boolean date_result_flag = DateUtils.dateComparison(arrivalDate, departureDate);
                    System.out.println("DateComparison: " + date_result_flag);

                    if (!date_result_flag) {
                        showAlertDialog(HotelSearchActivity.this, "Alert !!!", "Arrival date can't be greater than departure date", false);
                    }
                    else{
                        Utility utility = new Utility();
                        utility.setDelegate(HotelSearchActivity.this);
                        Utility.showAlertForProceed(HotelSearchActivity.this);

                    }
                }else if (!name.isEmpty() || !age.isEmpty()||!fathername.isEmpty() || !address.isEmpty()|| !idNumber.isEmpty() || !contact.isEmpty()|| !hotelName.isEmpty() || !arrivalDate.isEmpty() || !departureDate.isEmpty()) {
                    Utility utility = new Utility();
                    utility.setDelegate(HotelSearchActivity.this);
                    Utility.showAlertForProceed(HotelSearchActivity.this);
                }

                else {

                    showAlertDialog(HotelSearchActivity.this, " Search Error!!! ", "Please provide at least one value for search", false);

                }



            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_name.setText("");
                et_age.setText("");
                et_fatherName.setText("");
                et_address.setText("");
                et_idNumber.setText("");
                et_contact.setText("");
                et_hotelName.setText("");
                tv_arrivalDate.setText("");
                tv_depatureDate.setText("");


            }
        });
        tv_arrivalDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    DateUtils.setDate(HotelSearchActivity.this, tv_arrivalDate, true);
                }
                return false;
            }
        });

        tv_depatureDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    DateUtils.setDate(HotelSearchActivity.this, tv_depatureDate, true);
                }
                return false;
            }
        });
        et_age.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    showAgedialog();

                }
                return false;
            }
        });
        iv_cancel_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_age.setText("");
            }
        });
        iv_cancel_arrival_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_arrivalDate.setText("");

            }
        });
        iv_cancel_departure_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_depatureDate.setText("");
            }
        });
        iv_cancel_hotel_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_hotelName.setText("");
            }
        });

        et_hotelName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    getHotelList();

                }
                return false;
            }
        });


    }
    public void getHotelList(){
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_GET_ALL_HOTEL_LIST);
        taskManager.setExistingHotelList(true);
        //user_id,device_token,device_type,imei,auth_key,name,address,pageno
        String[] keys = {};
        String[] values = {};
        taskManager.doStartTask(keys, values, true,true);
    }
    public void parseHotelListResponse(String result){
        if(result != null && !result.equals("")){

            try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.opt("status").toString().equalsIgnoreCase("1")){

                    JSONArray resultArray=jObj.getJSONArray("result");
                    if(resultArray.length()>0){
                        for(int i=0;i<resultArray.length();i++){
                            JSONObject jObjHotelName=resultArray.getJSONObject(i);
                            HotelsList hotelsList=new HotelsList();
                            hotelsList.setHotel_name(jObjHotelName.getString("Hotel_Name"));
                            hotel_namesList.add(hotelsList);
                        }

                        showHoteldialog();
                    }
                    else {
                        showAlertDialog(this," Search Error ","No Hotel Found",false);
                    }

                }
                else{
                    showAlertDialog(this," Search Error ",jObj.optString("message"),false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                //Utility.showToast(getActivity(), "Some error has been encountered", "long");
                showAlertDialog(this," Search Error ","Some error has been encountered",false);
            }
        }
    }
    private void showHoteldialog() {
        if (hotel_namesList.size() > 0) {
            hotel_status = true;
            customDialogAdapterForHotel = new CustomDialogAdapterForHotel(this, hotel_namesList);
            customDialogAdapterForHotel();

        }
    }

    private void customDialogAdapterForHotel() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);

        TextView tv_title = (TextView) dialogView.findViewById(R.id.tv_title);
        Button btn_cancel = (Button) dialogView.findViewById(R.id.btn_cancel);
        Button btn_done = (Button) dialogView.findViewById(R.id.btn_done);
        lv_dialog = (ListView) dialogView.findViewById(R.id.lv_dialog);
        SearchView searchView = (SearchView) dialogView.findViewById(R.id.searchView);


        Constants.changefonts(tv_title, this, "Calibri Bold.ttf");
        tv_title.setText("Select Hotels");

        customDialogAdapterForHotel.notifyDataSetChanged();
        lv_dialog.setAdapter(customDialogAdapterForHotel);
        //lv_dialog.setScrollingCacheEnabled(false);
        lv_dialog.setTextFilterEnabled(true);

        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(HotelSearchActivity.this);
        searchView.setSubmitButtonEnabled(false);
        searchView.setQueryHint("Search Here");


        final AlertDialog alertDialog = builder.show();

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                hotel_status = false;

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hotel_status = false;

                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();

                }

            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("No of Selected Items: " + CustomDialogAdapterForHotel.selectedItemList.size());
                System.out.println("Selected Items: " + CustomDialogAdapterForHotel.selectedItemList);

                hotel_status = false;

                onSelectedHotel(CustomDialogAdapterForHotel.selectedItemList);

                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();

                }

            }
        });

    }
    private void onSelectedHotel(List<String> hotelList) {


        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderModusOperandi = new StringBuilder();

        for (CharSequence modusOperandi : hotelList) {
            stringBuilder.append(modusOperandi + ",");

            String modusOperandiModifiedString = stringBuilderModusOperandi.append("\'" + modusOperandi.toString() + "\',").toString();

            hotelString = modusOperandiModifiedString.substring(0, modusOperandiModifiedString.length() - 1);

        }

        if (hotelList.size() == 0) {
            //tv_modus_operandi.setText("Select an Operandi");
            et_hotelName.setText("");
            hotelString = "";
        } else {
            System.out.println("Modus Operandi String: " + hotelString);
            et_hotelName.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
        }
    }


    private void showAgedialog() {

        if (ageArrayList.size() > 0) {
            final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this, R.style.myDialog);
            builder.setTitle("Select Age Range")
                    .setItems(R.array.Age_array_new, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // The 'which' argument contains the index position
                            // of the selected item
                            et_age.setText(ageArrayList.get(which));
                        }
                    });

            android.support.v7.app.AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    @Override
    public void onShowAlertForProceedResultSuccess(String success) {
        fetchSearchResult(name,age,fathername,address,idNumber,contact,hotelName,arrivalDate,departureDate);
    }

    @Override
    public void onShowAlertForProceedResultError(String error) {

    }
    private void fetchSearchResult(String name,String age,String fathername,String address,String idNumber,String contact,String hotelName,String arrivalDate,String departureDate) {

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_HOTEL_SEARCH);
        taskManager.setHotelSearch(true);
        try{
           // devicetoken = GCMRegistrar.getRegistrationId(this);
            if(FirebaseInstanceId.getInstance().getToken()!=null) {
                devicetoken = FirebaseInstanceId.getInstance().getToken();
            }
            else{
                devicetoken=Utility.getFCMToken(this);
            }
            auth_key= Utility.getHotelSearchSesionId(this);
        }
        catch (Exception e){

        }
        //user_id,device_token,device_type,imei,auth_key,name,address,pageno
        String[] keys = {"name","age","father_name","address","idnumber","contact","datearraival","datedepurture","hotel_name","visitor_type","pageno","user_id","device_type","imei","auth_key"};

        String[] values = {name,age,fathername,address,idNumber,contact,arrivalDate,departureDate,hotelName,visitoreType,"1",userId,"android", Utility.getImiNO(this),auth_key};

        taskManager.doStartTask(keys, values, true,true);


    }
    public void parseHotelSearchResultResponse(String result, String[] keys, String[] values){
        if(result != null && !result.equals("")){

            try{
                JSONObject jObj = new JSONObject(result);
                if(jObj.opt("status").toString().equalsIgnoreCase("1")){

                    this.keys=keys;
                    this.values=values;
                    pageno=jObj.opt("pageno").toString();
                    totalResult=jObj.opt("count").toString();

                    JSONArray resultArray=jObj.getJSONArray("result");

                    parseHotelSearchResponse(resultArray);

                }
                else{
                    showAlertDialog(this," Search Error ",jObj.optString("message"),false);
                }


            }catch (JSONException e){

                e.printStackTrace();
                //Utility.showToast(getActivity(), "Some error has been encountered", "long");
                showAlertDialog(this," Search Error ","Some error has been encountered",false);
            }
        }
    }
    private void parseHotelSearchResponse(JSONArray resultArray) {
        hotelDetailsArrayList = new ArrayList<HotelDetails>();
        for(int i=0;i<resultArray.length();i++) {

            JSONObject jsonObj = null;

            try {
                jsonObj=resultArray.getJSONObject(i);
                String address="";

                HotelDetails hotelDetails = new HotelDetails();
                if(visitoreType.equalsIgnoreCase("1")) {

                    if (!jsonObj.optString("Full_Name").equalsIgnoreCase("null") && !jsonObj.optString("Full_Name").equalsIgnoreCase("")) {
                        hotelDetails.setName(jsonObj.optString("Full_Name"));
                    }
                    if (!jsonObj.optString("Village_Street_Name_with_No").equalsIgnoreCase("null") && !jsonObj.optString("Village_Street_Name_with_No").equalsIgnoreCase("")) {
                        address=address+jsonObj.optString("Village_Street_Name_with_No");
                    }
                    if (!jsonObj.optString("Post_Office").equalsIgnoreCase("null") && !jsonObj.optString("Post_Office").equalsIgnoreCase("")) {
                        address=address+","+jsonObj.optString("Post_Office");
                    }
                    if (!jsonObj.optString("District").equalsIgnoreCase("null") && !jsonObj.optString("District").equalsIgnoreCase("")) {
                        address=address+","+jsonObj.optString("District");
                    }
                    if (!jsonObj.optString("State").equalsIgnoreCase("null") && !jsonObj.optString("State").equalsIgnoreCase("")) {
                        address=address+","+jsonObj.optString("State");
                    }
                    if (!jsonObj.optString("PIN").equalsIgnoreCase("null") && !jsonObj.optString("PIN").equalsIgnoreCase("")) {
                        address=address+","+jsonObj.optString("PIN");
                    }
                    hotelDetails.setAddress(address);
                    String imageURL="";
                    if (!jsonObj.optString("Photograph").equalsIgnoreCase("null") && !jsonObj.optString("Photograph").equalsIgnoreCase("")) {
                        imageURL=imageURL+ Constants.IMAGE_URL_NATIONAL;
                        imageURL=imageURL+jsonObj.optString("Photograph");
                    }
                    hotelDetails.setImage(imageURL);

                    if (!jsonObj.optString("ID").equalsIgnoreCase("null") && !jsonObj.optString("ID").equalsIgnoreCase("")) {
                        hotelDetails.setId(jsonObj.optString("ID"));
                    }
                    if (!jsonObj.optString("Age").equalsIgnoreCase("null") && !jsonObj.optString("Age").equalsIgnoreCase("")) {
                        hotelDetails.setAge(jsonObj.optString("Age"));
                    } if (!jsonObj.optString("Sex").equalsIgnoreCase("null") && !jsonObj.optString("Sex").equalsIgnoreCase("")) {
                        hotelDetails.setSex(jsonObj.optString("Sex"));
                    }if (!jsonObj.optString("Contact_No").equalsIgnoreCase("null") && !jsonObj.optString("Contact_No").equalsIgnoreCase("")) {
                        hotelDetails.setContactNumber(jsonObj.optString("Contact_No"));
                    }if (!jsonObj.optString("ID_Proof_No").equalsIgnoreCase("null") && !jsonObj.optString("ID_Proof_No").equalsIgnoreCase("")) {
                        hotelDetails.setIdNumber(jsonObj.optString("ID_Proof_No"));
                    }if (!jsonObj.optString("Type_of_ID_Proof").equalsIgnoreCase("null") && !jsonObj.optString("Type_of_ID_Proof").equalsIgnoreCase("")) {
                        hotelDetails.setIdtype(jsonObj.optString("Type_of_ID_Proof"));
                    }if (!jsonObj.optString("Coming_From").equalsIgnoreCase("null") && !jsonObj.optString("Coming_From").equalsIgnoreCase("")) {
                        hotelDetails.setCommingFrom(jsonObj.optString("Coming_From"));
                    }if (!jsonObj.optString("Proceeding_To").equalsIgnoreCase("null") && !jsonObj.optString("Proceeding_To").equalsIgnoreCase("")) {
                        hotelDetails.setProceedingTo(jsonObj.optString("Proceeding_To"));
                    }if (!jsonObj.optString("Accompaning_No_Major").equalsIgnoreCase("null") && !jsonObj.optString("Accompaning_No_Major").equalsIgnoreCase("")) {
                        hotelDetails.setAdult(jsonObj.optString("Accompaning_No_Major"));
                    }if (!jsonObj.optString("Accompaning_No_Minor").equalsIgnoreCase("null") && !jsonObj.optString("Accompaning_No_Minor").equalsIgnoreCase("")) {
                        hotelDetails.setChild(jsonObj.optString("Accompaning_No_Minor"));
                    }if (!jsonObj.optString("Hotel_Name").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_Name").equalsIgnoreCase("")) {
                        hotelDetails.setHotelName(jsonObj.optString("Hotel_Name"));
                        if (!jsonObj.optString("Hotel_Code").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_Code").equalsIgnoreCase("")) {
                            hotelDetails.setHotelCode(jsonObj.optString("Hotel_Code"));
                        }

                        if (!jsonObj.optString("Hotel_PS").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_PS").equalsIgnoreCase("")) {
                            hotelDetails.setHotelPs(jsonObj.optString("Hotel_PS"));
                        }
                    }if (!jsonObj.optString("Hotel_Address").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_Address").equalsIgnoreCase("")) {
                        hotelDetails.setHoteladdress(jsonObj.optString("Hotel_Address"));
                    }if (!jsonObj.optString("Room_No").equalsIgnoreCase("null") && !jsonObj.optString("Room_No").equalsIgnoreCase("")) {
                        hotelDetails.setHotelRoomNo(jsonObj.optString("Room_No"));
                    }
                    if (!jsonObj.optString("Status").equalsIgnoreCase("null") && !jsonObj.optString("Status").equalsIgnoreCase("")) {
                        hotelDetails.setHotelStatus(jsonObj.optString("Status"));
                    }
                    String arrival_date_time="";
                    if (!jsonObj.optString("Date_of_Arrival").equalsIgnoreCase("null") && !jsonObj.optString("Date_of_Arrival").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Date_of_Arrival").split(" ");
                        arrival_date_time= DateUtils.changeDateFormat(arrOfStr[0]);
                       /* try {
                           String[] arrOfStr = jsonObj.optString("Date_of_Arrival").split(" ");
                           SimpleDateFormat simpledatafo = new SimpleDateFormat("dd-MM-yyyy");
                           Date date = simpledatafo.parse(arrOfStr[0]);

                           String formatedDate = simpledatafo.format(date);
                           arrival_date_time = arrOfStr[0];
                       }catch (Exception e){

                       }*/
                    }
                    if (!jsonObj.optString("Time_of_Arrival").equalsIgnoreCase("null") && !jsonObj.optString("Time_of_Arrival").equalsIgnoreCase("")) {


                        arrival_date_time=arrival_date_time+ " at "+jsonObj.optString("Time_of_Arrival");
                    }
                    hotelDetails.setArrivalDate(arrival_date_time);
                    String dep_date_time="";
                    if (!jsonObj.optString("Date_of_Departure").equalsIgnoreCase("null") && !jsonObj.optString("Date_of_Departure").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Date_of_Departure").split(" ");
                        dep_date_time= DateUtils.changeDateFormat(arrOfStr[0]);

                     /*   try {

                            String[] arrOfStr = jsonObj.optString("Date_of_Departure").split(" ");
                            SimpleDateFormat simpledatafo = new SimpleDateFormat("dd-MM-yyyy");
                            Date date = simpledatafo.parse(arrOfStr[0]);

                            String formatedDate2 = simpledatafo.format(date);
                            dep_date_time = arrOfStr[0];
                        }catch(Exception e){

                        }*/
                    }
                    if (!jsonObj.optString("Time_of_Departure").equalsIgnoreCase("null") && !jsonObj.optString("Time_of_Departure").equalsIgnoreCase("")) {

                        dep_date_time=dep_date_time+ " at "+jsonObj.optString("Time_of_Departure");
                    }
                    hotelDetails.setDepatureDate(dep_date_time);
                    int  purposeOfVisit;
                    if (!jsonObj.optString("Purpose_of_visit").equalsIgnoreCase("null") && !jsonObj.optString("Purpose_of_visit").equalsIgnoreCase("")) {

                        purposeOfVisit=jsonObj.optInt("Purpose_of_visit");
                        hotelDetails.setVisitPurpose(Constants.PURPOSE_OF_VISIT[purposeOfVisit-1]);
                    }
                    if (!jsonObj.optString("Police_Area").equalsIgnoreCase("null") && !jsonObj.optString("Police_Area").equalsIgnoreCase("")) {

                        hotelDetails.setPoliceArea(jsonObj.optString("Police_Area"));
                    }
                    if (!jsonObj.optString("divn").equalsIgnoreCase("null") && !jsonObj.optString("divn").equalsIgnoreCase("")) {

                        hotelDetails.setDivision(jsonObj.optString("divn"));
                    }



                }
                else if(visitoreType.equalsIgnoreCase("2")){
                    if (!jsonObj.optString("Full_Name").equalsIgnoreCase("null") && !jsonObj.optString("Full_Name").equalsIgnoreCase("")) {
                        hotelDetails.setName(jsonObj.optString("Full_Name"));
                    }
                    if (!jsonObj.optString("Address_in_Country_where_Residing_Permanently").equalsIgnoreCase("null") && !jsonObj.optString("Address_in_Country_where_Residing_Permanently").equalsIgnoreCase("")) {
                        hotelDetails.setAddress_foreign(jsonObj.optString("Address_in_Country_where_Residing_Permanently"));
                    }
                    if (!jsonObj.optString("Address_Reference_in_India").equalsIgnoreCase("null") && !jsonObj.optString("Address_Reference_in_India").equalsIgnoreCase("")) {
                        hotelDetails.setAddress(jsonObj.optString("Address_Reference_in_India"));
                    }
                    String imageURL="";
                    if (!jsonObj.optString("Photograph").equalsIgnoreCase("null") && !jsonObj.optString("Photograph").equalsIgnoreCase("")) {
                        imageURL=imageURL+ Constants.IMAGE_URL_FOREIGN;
                        imageURL=imageURL+jsonObj.optString("Photograph");
                    }
                    hotelDetails.setImage(imageURL);

                    if (!jsonObj.optString("ID").equalsIgnoreCase("null") && !jsonObj.optString("ID").equalsIgnoreCase("")) {
                        hotelDetails.setId(jsonObj.optString("ID"));
                    }
                    if (!jsonObj.optString("Date_of_Birth").equalsIgnoreCase("null") && !jsonObj.optString("Date_of_Birth").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Date_of_Birth").split(" ");
                        String date_of_birth= DateUtils.changeDateFormat(arrOfStr[0]);
                        hotelDetails.setDateOfBirth(date_of_birth);
                    }
                    if (!jsonObj.optString("Nationality").equalsIgnoreCase("null") && !jsonObj.optString("Nationality").equalsIgnoreCase("")) {
                        hotelDetails.setNationality(jsonObj.optString("Nationality"));
                    }
                    if (!jsonObj.optString("Passport_No").equalsIgnoreCase("null") && !jsonObj.optString("Passport_No").equalsIgnoreCase("")) {
                        hotelDetails.setPassportNo(jsonObj.optString("Passport_No"));
                    }
                    if (!jsonObj.optString("Place_of_Issue_of_Passport").equalsIgnoreCase("null") && !jsonObj.optString("Place_of_Issue_of_Passport").equalsIgnoreCase("")) {
                        hotelDetails.setPlaceOfIssuePassport(jsonObj.optString("Place_of_Issue_of_Passport"));
                    }
                    if (!jsonObj.optString("Date_of_Issue_of_Passport").equalsIgnoreCase("null") && !jsonObj.optString("Date_of_Issue_of_Passport").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Date_of_Issue_of_Passport").split(" ");
                        String date_of_issue_of_passport= DateUtils.changeDateFormat(arrOfStr[0]);
                        hotelDetails.setDateOfIssuePasport(date_of_issue_of_passport);
                    }
                    if (!jsonObj.optString("Passport_Valid_Till").equalsIgnoreCase("null") && !jsonObj.optString("Passport_Valid_Till").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Passport_Valid_Till").split(" ");
                        String passport_valid_till= DateUtils.changeDateFormat(arrOfStr[0]);
                        hotelDetails.setPassportVlidTill(passport_valid_till);
                    }
                    if (!jsonObj.optString("Visa_No").equalsIgnoreCase("null") && !jsonObj.optString("Visa_No").equalsIgnoreCase("")) {
                        hotelDetails.setVissaNo(jsonObj.optString("Visa_No"));
                    }
                    if (!jsonObj.optString("Visa_Issue_Date").equalsIgnoreCase("null") && !jsonObj.optString("Visa_Issue_Date").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Visa_Issue_Date").split(" ");
                        String visa_issue_date= DateUtils.changeDateFormat(arrOfStr[0]);
                        hotelDetails.setVissaIsueDate(visa_issue_date);
                    }
                    if (!jsonObj.optString("Visa_Valid_Till").equalsIgnoreCase("null") && !jsonObj.optString("Visa_Valid_Till").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Visa_Valid_Till").split(" ");
                        String visa_valid_till= DateUtils.changeDateFormat(arrOfStr[0]);
                        hotelDetails.setVissaValidTill(visa_valid_till);
                    }
                    if (!jsonObj.optString("Type_of_Visa").equalsIgnoreCase("null") && !jsonObj.optString("Type_of_Visa").equalsIgnoreCase("")) {
                        hotelDetails.setTypeOfVissa(jsonObj.optString("Type_of_Visa"));
                    }
                    if (!jsonObj.optString("Visa_Issue_Place").equalsIgnoreCase("null") && !jsonObj.optString("Visa_Issue_Place").equalsIgnoreCase("")) {
                        hotelDetails.setVissaIssuePlace(jsonObj.optString("Visa_Issue_Place"));
                    }

                    if (!jsonObj.optString("Phone_No_in_India").equalsIgnoreCase("null") && !jsonObj.optString("Phone_No_in_India").equalsIgnoreCase("")) {
                        hotelDetails.setContactNumber(jsonObj.optString("Phone_No_in_India"));
                    }
                    if (!jsonObj.optString("Phone_No_in_Country_Where_Permanently_Residing").equalsIgnoreCase("null") && !jsonObj.optString("Phone_No_in_Country_Where_Permanently_Residing").equalsIgnoreCase("")) {
                        hotelDetails.setContactNumberForeign(jsonObj.optString("Phone_No_in_Country_Where_Permanently_Residing"));
                    }
                    if (!jsonObj.optString("Arrived_From").equalsIgnoreCase("null") && !jsonObj.optString("Arrived_From").equalsIgnoreCase("")) {
                        hotelDetails.setCommingFrom(jsonObj.optString("Arrived_From"));
                    }
                    if (!jsonObj.optString("Proceeding_To").equalsIgnoreCase("null") && !jsonObj.optString("Proceeding_To").equalsIgnoreCase("")) {
                        hotelDetails.setProceedingTo(jsonObj.optString("Proceeding_To"));
                    }
                    if (!jsonObj.optString("Hotel_Name").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_Name").equalsIgnoreCase("")) {
                        hotelDetails.setHotelName(jsonObj.optString("Hotel_Name"));
                    }
                    if (!jsonObj.optString("Hotel_Code").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_Code").equalsIgnoreCase("")) {
                        hotelDetails.setHotelCode(jsonObj.optString("Hotel_Code"));
                    }
                    if (!jsonObj.optString("Hotel_Address").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_Address").equalsIgnoreCase("")) {
                        hotelDetails.setHoteladdress(jsonObj.optString("Hotel_Address"));
                    }
                    if (!jsonObj.optString("Hotel_PS").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_PS").equalsIgnoreCase("")) {
                        hotelDetails.setHotelPs(jsonObj.optString("Hotel_PS"));
                    }
                    if (!jsonObj.optString("Hotel_Phone_No").equalsIgnoreCase("null") && !jsonObj.optString("Hotel_Phone_No").equalsIgnoreCase("")) {
                        hotelDetails.setHotelphone(jsonObj.optString("Hotel_Phone_No"));
                    }
                    if (!jsonObj.optString("Status").equalsIgnoreCase("null") && !jsonObj.optString("Status").equalsIgnoreCase("")) {
                        hotelDetails.setHotelStatus(jsonObj.optString("Status"));
                    }

                    if (!jsonObj.optString("Room_No").equalsIgnoreCase("null") && !jsonObj.optString("Room_No").equalsIgnoreCase("")) {
                        hotelDetails.setHotelRoomNo(jsonObj.optString("Room_No"));
                    }

                    String arrival_date_time="";
                    if (!jsonObj.optString("Date_of_Arrival_in_Hotel").equalsIgnoreCase("null") && !jsonObj.optString("Date_of_Arrival_in_Hotel").equalsIgnoreCase("")) {

                        String[] arrOfStr = jsonObj.optString("Date_of_Arrival_in_Hotel").split(" ");
                        arrival_date_time= DateUtils.changeDateFormat(arrOfStr[0]);
                          /*  SimpleDateFormat simpledatafo = new SimpleDateFormat("yyyy-MM-dd");
                            Date date = simpledatafo.parse(arrOfStr[0]);
                            SimpleDateFormat simpledatafonew= new SimpleDateFormat("dd-MMM-yyyy");
                            String formatedDate = simpledatafonew.format(date);
                            arrival_date_time = formatedDate;*/

                    }
                    if (!jsonObj.optString("Time_of_Arrival_in_Hotel").equalsIgnoreCase("null") && !jsonObj.optString("Time_of_Arrival_in_Hotel").equalsIgnoreCase("")) {


                        arrival_date_time=arrival_date_time+ " at "+jsonObj.optString("Time_of_Arrival_in_Hotel");
                    }
                    hotelDetails.setArrivalDate(arrival_date_time);
                    String dep_date_time="";
                    if (!jsonObj.optString("Date_of_Departure").equalsIgnoreCase("null") && !jsonObj.optString("Date_of_Departure").equalsIgnoreCase("")) {
                        String[] arrOfStr = jsonObj.optString("Date_of_Departure").split(" ");
                        dep_date_time= DateUtils.changeDateFormat(arrOfStr[0]);
                     /*   try {

                            String[] arrOfStr = jsonObj.optString("Date_of_Departure").split(" ");
                            SimpleDateFormat simpledatafo = new SimpleDateFormat("yyyy-MM-dd");
                            Date date = simpledatafo.parse(arrOfStr[0]);
                            SimpleDateFormat simpledatafonew= new SimpleDateFormat("dd-MMM-yyyy");
                            String formatedDate2 = simpledatafonew.format(date);
                            dep_date_time =formatedDate2;
                        }catch(Exception e){

                        }*/
                    }
                    if (!jsonObj.optString("Time_of_Departure").equalsIgnoreCase("null") && !jsonObj.optString("Time_of_Departure").equalsIgnoreCase("")) {

                        dep_date_time=dep_date_time+ " at "+jsonObj.optString("Time_of_Departure");
                    }
                    hotelDetails.setDepatureDate(dep_date_time);
                    if (!jsonObj.optString("Purpose_of_Visit").equalsIgnoreCase("null") && !jsonObj.optString("Purpose_of_Visit").equalsIgnoreCase("")) {

                        hotelDetails.setVisitPurpose(jsonObj.optString("Purpose_of_Visit"));
                    }
                    if (!jsonObj.optString("Police_Area").equalsIgnoreCase("null") && !jsonObj.optString("Police_Area").equalsIgnoreCase("")) {

                        hotelDetails.setPoliceArea(jsonObj.optString("Police_Area"));
                    }
                    if (!jsonObj.optString("divn").equalsIgnoreCase("null") && !jsonObj.optString("divn").equalsIgnoreCase("")) {

                        hotelDetails.setDivision(jsonObj.optString("divn"));
                    }




                }

                hotelDetailsArrayList.add(hotelDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Intent intent=new Intent(this, HotelSearchResultActivity.class);
        intent.putExtra("keys",keys);
        intent.putExtra("value",values);
        intent.putExtra("pageno",pageno);
        intent.putExtra("totalResult", totalResult);
        intent.putExtra("visitoreType",visitoreType);
        intent.putExtra("HOTEL_SEARCH_LIST", (Serializable) hotelDetailsArrayList);
        startActivity(intent);

    }

    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog ad = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        ad.setTitle(title);

        // Setting Dialog Message
        ad.setMessage(message);

        // Setting alert dialog icon
        ad.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
       /* ad.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(ad!=null && ad.isShowing()){
                    ad.dismiss();
                }
            }
        });*/

        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            ad.dismiss();
                        }

                    }
                });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (hotel_status) {
            customDialogAdapterForHotel.filter(newText.toString());
        }
        return false;
    }

    @Override
    public void onClick(View v) {

    }
}
