package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 04-10-2016.
 */
public class VehicleSearchDetails implements Serializable {

    private String rol="";
    private String regn_no="";
    private String regn_dt="";
    private String rto_cd="";
    private String eng_no="";
    private String chasis_no="";
    private String o_name="";
    private String f_name="";
    private String address="";
    private String p_address="";
    private String garage_address="";
    private String color="";
    private String maker_model="";
    private String cl_desc="";
    private String holder_mobileNo="";

    public String getHolder_mobileNo() {
        return holder_mobileNo;
    }

    public void setHolder_mobileNo(String holder_mobileNo) {
        this.holder_mobileNo = holder_mobileNo;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getRegn_no() {
        return regn_no;
    }

    public void setRegn_no(String regn_no) {
        this.regn_no = regn_no;
    }

    public String getRegn_dt() {
        return regn_dt;
    }

    public void setRegn_dt(String regn_dt) {
        this.regn_dt = regn_dt;
    }

    public String getRto_cd() {
        return rto_cd;
    }

    public void setRto_cd(String rto_cd) {
        this.rto_cd = rto_cd;
    }

    public String getEng_no() {
        return eng_no;
    }

    public void setEng_no(String eng_no) {
        this.eng_no = eng_no;
    }

    public String getChasis_no() {
        return chasis_no;
    }

    public void setChasis_no(String chasis_no) {
        this.chasis_no = chasis_no;
    }

    public String getO_name() {
        return o_name;
    }

    public void setO_name(String o_name) {
        this.o_name = o_name;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getP_address() {
        return p_address;
    }

    public void setP_address(String p_address) {
        this.p_address = p_address;
    }

    public String getGarage_address() {
        return garage_address;
    }

    public void setGarage_address(String garage_address) {
        this.garage_address = garage_address;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMaker_model() {
        return maker_model;
    }

    public void setMaker_model(String maker_model) {
        this.maker_model = maker_model;
    }

    public String getCl_desc() {
        return cl_desc;
    }

    public void setCl_desc(String cl_desc) {
        this.cl_desc = cl_desc;
    }
}
