package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-Asset-131 on 18-03-2016.
 */
public class PoliceStationList implements Serializable{

    private String ps_code="";
    private String ps_name="";
    private String div_code="";
    private String oc_name = "";

    public String getOc_name() {
        return oc_name;
    }

    public void setOc_name(String oc_name) {
        this.oc_name = oc_name;
    }

    public String getPs_code() {
        return ps_code;
    }

    public void setPs_code(String ps_code) {
        this.ps_code = ps_code;
    }

    public String getPs_name() {
        return ps_name;
    }

    public void setPs_name(String ps_name) {
        this.ps_name = ps_name;
    }

    public String getDiv_code() {
        return div_code;
    }

    public void setDiv_code(String div_code) {
        this.div_code = div_code;
    }



}
