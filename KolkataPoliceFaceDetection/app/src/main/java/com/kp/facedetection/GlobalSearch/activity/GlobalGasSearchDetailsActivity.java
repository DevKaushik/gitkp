package com.kp.facedetection.GlobalSearch.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.kp.facedetection.AllDocumentDataSearchResultActivity;
import com.kp.facedetection.BaseActivity;
import com.kp.facedetection.FeedbackActivity;
import com.kp.facedetection.KPFaceDetectionApplication;
import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnShowAlertForProceedResult;
import com.kp.facedetection.model.AllDocumentSearchType;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class GlobalGasSearchDetailsActivity extends BaseActivity implements View.OnClickListener, Observer,OnShowAlertForProceedResult {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private String distributorId = "";
    private String distributorName = "";
    private String consumerId = "";
    private String consumerName = "";
    private String consumerAddress = "";
    private String pincode = "";
    private String phoneNo = "";
    private String consumerStatus = "";
    private String svDate = "";
    private String hasAadhar = "";

    private TextView tv_DistributorIdValue;
    private TextView tv_DistributorNameValue;
    private TextView tv_ConsumerIdValue;
    private TextView tv_ConsumerNameValue;
    private TextView tv_addressValue;
    private TextView tv_pincodeValue;
    private TextView tv_statusValue;
    private TextView tv_mobileNoValue;
    private TextView tv_SvDateValue;
    private TextView tv_aadharValue;

    private TextView tv_heading;

    private TextView tv_watermark;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    String name = "", address = "", contact = "";
    CheckBox chk_dl,chk_vehicle,chk_sdr,chk_cable,chk_kmc,chk_gas,chk_select_all;
    private String pageno = "";
    private String totalResult = "";
    private String[] keys;
    private String[] values;
    String userId = "";
    private Button btn_Search, btn_reset;
    String datafields ="";
    ArrayList<AllDocumentSearchType> allDocumentSearchTypesArrayList = new ArrayList<AllDocumentSearchType>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gas_search_details);
        ObservableObject.getInstance().addObserver(this);

        initialization();
    }


    /*
    *  Mapping all views
    * */
    private void initialization() {

        tv_DistributorIdValue = (TextView)findViewById(R.id.tv_DistributorIdValue);
        tv_DistributorNameValue = (TextView)findViewById(R.id.tv_DistributorNameValue);
        tv_ConsumerIdValue = (TextView)findViewById(R.id.tv_ConsumerIdValue);
        tv_ConsumerNameValue = (TextView)findViewById(R.id.tv_ConsumerNameValue);
        tv_addressValue = (TextView)findViewById(R.id.tv_addressValue);
        tv_pincodeValue = (TextView)findViewById(R.id.tv_pincodeValue);
        tv_statusValue = (TextView)findViewById(R.id.tv_statusValue);
        tv_mobileNoValue = (TextView)findViewById(R.id.tv_mobileNoValue);
        tv_SvDateValue = (TextView)findViewById(R.id.tv_SvDateValue);
        tv_aadharValue = (TextView)findViewById(R.id.tv_aadharValue);

        tv_heading = (TextView)findViewById(R.id.tv_heading);

        tv_watermark = (TextView)findViewById(R.id.tv_watermark);

        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);

        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);

        distributorId = getIntent().getStringExtra("DISTRIBUTOR_ID").trim();
        distributorName = getIntent().getStringExtra("DISTRIBUTOR_NAME").trim();
        consumerId = getIntent().getStringExtra("CONSUMER_ID").trim();
        consumerName = getIntent().getStringExtra("CONSUMER_NAME").trim();
        consumerAddress = getIntent().getStringExtra("CONSUMER_ADDRESS").trim();
        pincode = getIntent().getStringExtra("CONSUMER_PINCODE").trim();
        phoneNo = getIntent().getStringExtra("CONSUMER_MOBILE").trim();
        consumerStatus = getIntent().getStringExtra("CONSUMER_STATUS").trim();
        svDate = getIntent().getStringExtra("CONSUMER_SV_DATE").trim();
        hasAadhar = getIntent().getStringExtra("CONSUMER_AADHAR").trim();

        tv_DistributorIdValue.setText(distributorId);
        tv_DistributorNameValue.setText(distributorName);
        tv_ConsumerIdValue.setText(consumerId);
        tv_ConsumerNameValue.setText(consumerName);
        tv_addressValue.setText(consumerAddress);
        tv_pincodeValue.setText(pincode);
        tv_statusValue.setText(consumerStatus);
        tv_mobileNoValue.setText(phoneNo);
        tv_SvDateValue.setText(svDate);
        tv_aadharValue.setText(hasAadhar);


        Constants.changefonts(tv_DistributorIdValue, this, "Calibri.ttf");
        Constants.changefonts(tv_DistributorNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_ConsumerIdValue, this, "Calibri.ttf");
        Constants.changefonts(tv_ConsumerNameValue, this, "Calibri.ttf");
        Constants.changefonts(tv_addressValue, this, "Calibri.ttf");
        Constants.changefonts(tv_pincodeValue, this, "Calibri.ttf");
        Constants.changefonts(tv_statusValue, this, "Calibri.ttf");
        Constants.changefonts(tv_mobileNoValue, this, "Calibri.ttf");
        Constants.changefonts(tv_SvDateValue, this, "Calibri.ttf");
        Constants.changefonts(tv_aadharValue, this, "Calibri.ttf");


        Constants.changefonts(tv_heading, this, "Calibri Bold.ttf");

        /* Set username as Watermark */
        tv_watermark.setVisibility(View.VISIBLE);
        tv_watermark.setText(userName);
        tv_watermark.setRotation(-50);

          /* section for check box and search button  initilization */
        chk_dl=(CheckBox)findViewById(R.id.chk_dl);
        chk_dl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_dl.setChecked(true);
                else
                    chk_dl.setChecked(false);
            }
        });

        chk_vehicle=(CheckBox)findViewById(R.id.chk_vehicle);
        chk_vehicle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_vehicle.setChecked(true);
                else
                    chk_vehicle.setChecked(false);
            }
        });

        chk_sdr=(CheckBox)findViewById(R.id.chk_sdr);
        chk_sdr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_sdr.setChecked(true);
                else
                    chk_sdr.setChecked(false);
            }
        });


        chk_cable=(CheckBox)findViewById(R.id.chk_cable);
        chk_cable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_cable.setChecked(true);
                else
                    chk_cable.setChecked(false);
            }
        });


        chk_kmc=(CheckBox)findViewById(R.id.chk_kmc);
        chk_kmc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_kmc.setChecked(true);
                else
                    chk_kmc.setChecked(false);
            }
        });


       /* chk_gas=(CheckBox)findViewById(R.id.chk_gas);

        chk_gas.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    chk_gas.setChecked(true);
                else
                    chk_gas.setChecked(false);
            }
        });*/

        chk_select_all=(CheckBox)findViewById(R.id.chk_select_all);
        chk_select_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    chk_dl.setChecked(true);
                    chk_vehicle.setChecked(true);
                    chk_sdr.setChecked(true);
                    chk_cable.setChecked(true);
                    chk_kmc.setChecked(true);

                }
                else{
                    chk_dl.setChecked(false);
                    chk_vehicle.setChecked(false);
                    chk_sdr.setChecked(false);
                    chk_cable.setChecked(false);
                    chk_kmc.setChecked(false);
                }
            }
        });


        btn_Search = (Button) findViewById(R.id.btn_Search);
        btn_reset = (Button) findViewById(R.id.btn_reset);

        Constants.buttonEffect(btn_Search);
        Constants.buttonEffect(btn_reset);
        clickEvents();

    }
    public void clickEvents() {
        btn_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name = consumerName;

                address = consumerAddress.toLowerCase();

                contact = phoneNo;

                if(chk_dl.isChecked()){
                    datafields="1";
                }else{
                    datafields=datafields.replace("1","");
                }

                if(chk_vehicle.isChecked()){
                    datafields=datafields+","+"2";
                }
                else{
                    datafields=datafields.replace(",2","");
                }

                if(chk_sdr.isChecked()){
                    datafields=datafields+","+"3";
                }
                else{
                    datafields=datafields.replace(",3","");
                }

                if(chk_cable.isChecked()){
                    datafields=datafields+","+"4";
                }
                else{
                    datafields=datafields.replace(",4","");

                }

                if(chk_kmc.isChecked()){
                    datafields=datafields+","+"5";
                }
                else{
                    datafields=datafields.replace(",5","");

                }


                if(datafields.equalsIgnoreCase("")){
                    Utility.showAlertDialog(GlobalGasSearchDetailsActivity.this, " Search Error!!! ", "Please select at least one  search type", false);
                }
                else{
                    if (!name.isEmpty() || !address.isEmpty() || !contact.isEmpty()) {
                        Utility utility = new Utility();
                        utility.setDelegate(GlobalGasSearchDetailsActivity.this);
                        Utility.showAlertForProceed(GlobalGasSearchDetailsActivity.this);
                    } else {

                        Utility.showAlertDialog(GlobalGasSearchDetailsActivity.this, " Search Error!!! ", "Not getting any value for search", false);

                    }


                }


            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                chk_dl.setChecked(false);
                chk_vehicle.setChecked(false);
                chk_sdr.setChecked(false);
                chk_cable.setChecked(false);
                chk_kmc.setChecked(false);
                chk_gas.setChecked(false);
                chk_select_all.setChecked(false);



            }
        });
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }*/
    @Override
    public void onShowAlertForProceedResultSuccess(String success) {
        fetchSearchResult();
    }
    private void fetchSearchResult() {
        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_ALL_DOCUMENT_RELATIONAL_SEARCH);
        taskManager.setGasAllDataRelationalSearch(true);
        //user_id,device_token,device_type,imei,auth_key,name,address,pageno
        String[] keys = {"name", "address", "phoneNumber", "dataFields", "page_no"};
        String[] values = {name, address, contact,datafields, "1"};
        taskManager.doStartTask(keys, values, true, true);
    }
    public void parseGasAllDataRelationalSearchResultResponse(String result, String[] keys, String[] values) {
        if (result != null && !result.equals("")) {

            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.opt("status").toString().equalsIgnoreCase("1")) {

                    this.keys = keys;
                    this.values = values;
                    pageno = jObj.opt("page").toString();
                    totalResult = jObj.opt("count").toString();

                    JSONObject resultObject = jObj.getJSONObject("result");

                    createAllDocumentSearchTypeList(resultObject);

                } else {
                    Utility.showAlertDialog(this, " Search Error ", jObj.optString("message"), false);
                }


            } catch (JSONException e) {

                e.printStackTrace();
                //Utility.showToast(getActivity(), "Some error has been encountered", "long");
                Utility.showAlertDialog(this, " Search Error ", "Some error has been encountered", false);
            }
        }

    }

    public void createAllDocumentSearchTypeList(JSONObject resultArray) {
        allDocumentSearchTypesArrayList.clear();
        if (resultArray.has("1")) {
            try {
                JSONObject jsonObjectDL = resultArray.getJSONObject("1");
                if (jsonObjectDL.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[0]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }

        if (resultArray.has("2")) {
            try {
                JSONObject jsonObjectVehicle = resultArray.getJSONObject("2");
                if (jsonObjectVehicle.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[1]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("3")) {
            try {
                JSONObject jsonObjectSDR = resultArray.getJSONObject("3");
                if (jsonObjectSDR.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[2]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("4")) {
            try {
                JSONObject jsonObjectCable = resultArray.getJSONObject("4");
                if (jsonObjectCable.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[3]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("5")) {
            try {
                JSONObject jsonObjectKMC = resultArray.getJSONObject("5");
                if (jsonObjectKMC.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[4]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }
        }
        if (resultArray.has("6")) {
            try {
                JSONObject jsonObjectGas = resultArray.getJSONObject("6");
                if (jsonObjectGas.optString("status").equalsIgnoreCase("1")) {
                    AllDocumentSearchType allDocumentSearchType = new AllDocumentSearchType();
                    allDocumentSearchType.setName(Constants.ALL_DOCUMENT_SEARCH_TYPE[5]);
                    allDocumentSearchTypesArrayList.add(allDocumentSearchType);
                }
            } catch (Exception e) {

            }

        }
        Intent intent = new Intent(this, AllDocumentDataSearchResultActivity.class);
        intent.putExtra("keys", keys);
        intent.putExtra("value", values);
        intent.putExtra("ALL_DOCUMENT_SEARCH_TYPE", (Serializable) allDocumentSearchTypesArrayList);
        intent.putExtra("ALL_DOCUMENT_SEARCH_LIST", String.valueOf(resultArray));
        startActivity(intent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            /*case R.id.menu_send_push:
                startActivity(new Intent(this, SendPush.class));
                break;*/

            case R.id.menu_app_info:
                Utility.customDialogForAppInfo(this,Utility.getAppVersion(this));
                break;

            case R.id.menu_change_password:
                Utility.customDialogForChangePwd(this);
                break;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.menu_logout:
                logoutTask();
                break;
        }
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPFaceDetectionApplication.getApplication().removeActivityToList(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.charseet_due_notification :
                getChargesheetdetailsAsperRole(this);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }

    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }




    @Override
    public void onShowAlertForProceedResultError(String error) {

    }
}
