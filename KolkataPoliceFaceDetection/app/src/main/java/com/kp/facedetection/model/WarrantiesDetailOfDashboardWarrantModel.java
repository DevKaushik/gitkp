package com.kp.facedetection.model;

/**
 * Created by DAT-165 on 28-06-2017.
 */

public class WarrantiesDetailOfDashboardWarrantModel {
    String waName;
    String waAddress;
    String waFatherName;

    public String getWaFatherName() {
        return waFatherName;
    }

    public void setWaFatherName(String waFatherName) {
        this.waFatherName = waFatherName;
    }

    public String getWaName() {
        return waName;
    }

    public void setWaName(String waName) {
        this.waName = waName;
    }

    public String getWaAddress() {
        return waAddress;
    }

    public void setWaAddress(String waAddress) {
        this.waAddress = waAddress;
    }
}
