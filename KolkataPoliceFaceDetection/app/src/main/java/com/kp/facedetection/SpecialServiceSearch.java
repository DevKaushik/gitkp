package com.kp.facedetection;

import android.app.ActionBar;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kp.facedetection.adapter.SpecialServiceSearchAdapter;
import com.kp.facedetection.databinding.ActivitySpecialServiceSearchBinding;
import com.kp.facedetection.interfaces.OnSpecialServiceSearchListListener;
import com.kp.facedetection.model.Result;
import com.kp.facedetection.model.SpecialServiceSearchModel;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.DateUtils;
import com.kp.facedetection.utility.SimpleDividerItemDecoration;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SpecialServiceSearch extends BaseActivity implements View.OnClickListener,OnSpecialServiceSearchListListener {
    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";
    TextView chargesheetNotificationCount;
    String notificationCount = "";
    ActivitySpecialServiceSearchBinding activitySpecialServiceSearchBinding;
    private String[] requestArray;//= {"CDR", "SDR", "IMEI TRACK", "TOWER LOCATION", "IPDR", "ILD", "CALL DUMP", "CAF", "IP RESOLUTION"};//1,2,3,4,5,6,7,8,9
    private List<String> requestList = new ArrayList<String>();
    private String[] referenceArry = {"Case", "Enquiry"};
    private String[] liArray = {"Phone", "Imei"};
    private String selectedrequestTypeValue = "";
    private ArrayList<String> requestArrayList = new ArrayList<String>();
    private RecyclerView.LayoutManager layoutManager;
    SpecialServiceSearchModel modelService;
    String req_type="",from_date="",to_date="";

    @Override
    public void onBackPressed(){
        Intent intent=new Intent();
        intent.putExtra("REQUEST_TYPE",req_type);
        intent.putExtra("FROM_DATE",from_date);
        intent.putExtra("TO_DATE",to_date);
        setResult(SpecialServiceList.REQUEST_CODE,intent);
        finish();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySpecialServiceSearchBinding= DataBindingUtil.setContentView(this, R.layout.activity_special_service_search);
        setToolBar();
        initViews();
    }
    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout) mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this, charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount = (TextView) mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount = Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");
        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }
    public void initViews() {
        creatRequestTypeList();
        requestArray = requestArrayList.toArray(new String[requestArrayList.size()]);
        activitySpecialServiceSearchBinding.bttnSearch.setOnClickListener(this);
        layoutManager= new LinearLayoutManager(this);
        activitySpecialServiceSearchBinding.RvSpServiceFilteredList.setLayoutManager(layoutManager);
        clickEvents();

    }
    private void creatRequestTypeList() {
        //CALL DUMP ,IP RESL
        if (Utility.getUserInfo(this).getCdrRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("CDR");
        }
        if (Utility.getUserInfo(this).getSdrRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("SDR");
        }
        if (Utility.getUserInfo(this).getImeiRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("IMEI TRACK");
        }
        if (Utility.getUserInfo(this).getTowerRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("TOWER LOCATION");
        }
        if (Utility.getUserInfo(this).getIpdrRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("IPDR");
        }
        if (Utility.getUserInfo(this).getIldRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("ILD");
        }
        if (Utility.getUserInfo(this).getCafRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("CAF");
        }
        if (Utility.getUserInfo(this).getLoggerRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("LI");
        }
        if (Utility.getUserInfo(this).getMnpRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("MNP");
        }
        if (Utility.getUserInfo(this).getRdRequestAccess().equalsIgnoreCase("1")) {
            requestArrayList.add("Recharge Details");
        }
        if (Utility.getUserInfo(this).getCellIdChart().equalsIgnoreCase("1")) {
            requestArrayList.add("CELL-ID CHART");
        }


    }
    private void clickEvents() {

        activitySpecialServiceSearchBinding.etRequestType.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showRequestTypedialog();

                }
                return false;
            }
        });
        activitySpecialServiceSearchBinding.etFromDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    DateUtils.setDate(SpecialServiceSearch.this, activitySpecialServiceSearchBinding.etFromDate, true);
                }
                return false;
            }
        });
        activitySpecialServiceSearchBinding.etToDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    DateUtils.setDate(SpecialServiceSearch.this, activitySpecialServiceSearchBinding.etToDate, true);
                }
                return false;
            }
        });


    }
    private void showRequestTypedialog() {

        if (requestArrayList.size() > 0) {
            final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this, R.style.myDialog);
            builder.setTitle("Select Request Type")
                    .setItems(requestArray, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // The 'which' argument contains the index position
                            // of the selected item

                            activitySpecialServiceSearchBinding.etRequestType.setText(requestArrayList.get(which));
                            //selectedrequestType = requestArray[which];


                        }
                    });

            android.support.v7.app.AlertDialog dialog = builder.create();
            dialog.show();
        }
    }
    public void updateNotificationCount(String notificationCount) {

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        } else {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }


    @Override
    public void onClick(View v) {
        int id=v.getId();
        switch (id){
            case R.id.bttn_search:
                if (activitySpecialServiceSearchBinding.etRequestType.getText().toString().isEmpty()) {

                    Utility.showAlertDialog(this, " Error ", "Please enter Request type ", false);
                    return;

                }
                else if (activitySpecialServiceSearchBinding.etFromDate.getText().toString().isEmpty()) {
                    // showMesage("Please enter a valid Password", et_password);
                    Utility.showAlertDialog(this, " Error ", "Please enter from date", false);
                    return;
                } else if (activitySpecialServiceSearchBinding.etToDate.getText().toString().isEmpty()) {
                    // showMesage("Please enter a valid Password", et_password);
                    Utility.showAlertDialog(this, " Error ", "Please enter to date", false);
                    return;
                } else if (!activitySpecialServiceSearchBinding.etFromDate.getText().toString().equalsIgnoreCase("") && !activitySpecialServiceSearchBinding.etToDate.getText().toString().equalsIgnoreCase("")) {

                    boolean date_result_flag = DateUtils.dateComparison(activitySpecialServiceSearchBinding.etFromDate.getText().toString(), activitySpecialServiceSearchBinding.etToDate.getText().toString());

                    if (!date_result_flag) {
                        Utility.showAlertDialog(this, "Alert !!!", "From date can't be greater than To date", false);
                        return;
                    } else {
                         setselectedrequestTypeValue();
                         req_type=selectedrequestTypeValue;
                         from_date=activitySpecialServiceSearchBinding.etFromDate.getText().toString();
                         to_date=activitySpecialServiceSearchBinding.etToDate.getText().toString();
                          Intent intent=new Intent();
                          intent.putExtra("REQUEST_TYPE",req_type);
                          intent.putExtra("FROM_DATE",from_date);
                          intent.putExtra("TO_DATE",to_date);
                          setResult(SpecialServiceList.REQUEST_CODE,intent);
                          finish();
                          //searchSpecialService(activitySpecialServiceSearchBinding.etRequestType.getText().toString(),activitySpecialServiceSearchBinding.etFromDate.getText().toString(),activitySpecialServiceSearchBinding.etToDate.getText().toString());

                        //allfildvalidate = true;
                    }

                    break;
                }
        }

    }
    public void setselectedrequestTypeValue() {
        if (activitySpecialServiceSearchBinding.etRequestType.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[0])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[0];
        } else if (activitySpecialServiceSearchBinding.etRequestType.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[1])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[1];
        } else if (activitySpecialServiceSearchBinding.etRequestType.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[2])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[2];
        } else if (activitySpecialServiceSearchBinding.etRequestType.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[3])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[3];
        } else if (activitySpecialServiceSearchBinding.etRequestType.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[4])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[4];
        } else if (activitySpecialServiceSearchBinding.etRequestType.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[5])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[5];
        } else if (activitySpecialServiceSearchBinding.etRequestType.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[6])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[6];
        } else if (activitySpecialServiceSearchBinding.etRequestType.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[7])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[7];
        } else if (activitySpecialServiceSearchBinding.etRequestType.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[8])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[8];
        } else if (activitySpecialServiceSearchBinding.etRequestType.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[9])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[9];
        }else if (activitySpecialServiceSearchBinding.etRequestType.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[10])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[10];
        }else if (activitySpecialServiceSearchBinding.etRequestType.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[11])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[11];
        }
        else if (activitySpecialServiceSearchBinding.etRequestType.getText().toString().equalsIgnoreCase(Constants.SPECIAL_SERVICE_ARRAY[12])) {
            selectedrequestTypeValue = Constants.SPECIAL_SERVICE_VALUE[12];
        }


    }


    @Override
    public void onSpecialServiceSearchListItemListener(int position) {
      /*  Result result=modelService.getResult().get(position);
        Intent intent=new Intent(this,SpecialServiceDetailsActivity.class);
        intent.putExtra("SEARCH_DETAILS",result);
        startActivity(intent);*/
    }
}
