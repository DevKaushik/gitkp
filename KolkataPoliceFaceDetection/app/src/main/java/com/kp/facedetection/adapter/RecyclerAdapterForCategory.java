package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.interfaces.OnItemClickListenerForCrimeReview;
import com.kp.facedetection.model.CrimeReviewDetails;

import java.util.ArrayList;

/**
 * Created by DAT-165 on 07-06-2017.
 */

public class RecyclerAdapterForCategory extends RecyclerView.Adapter<RecyclerAdapterForCategory.MyViewHolder> {

    Context con;
    ArrayList<CrimeReviewDetails> crimeReviewDetailsArrayList = new ArrayList<>();
    private OnItemClickListenerForCrimeReview crimeReviewClickListener;

    public RecyclerAdapterForCategory(Context con, ArrayList<CrimeReviewDetails> crimeReviewDetailsArrayList) {
        this.con = con;
        this.crimeReviewDetailsArrayList = crimeReviewDetailsArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.crime_review_list_item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final CrimeReviewDetails crimeReviewDetails = crimeReviewDetailsArrayList.get(position);
        if(crimeReviewDetails.getCrimeCategory() != null && !crimeReviewDetails.getCrimeCategory().equalsIgnoreCase("")
                && !crimeReviewDetails.getCrimeCategory().equalsIgnoreCase("null") && !crimeReviewDetails.getCrimeCategory().equalsIgnoreCase("0")) {
            holder.tv_categoryName.setText(crimeReviewDetails.getCrimeCategory());
            if(crimeReviewDetails.getCountOfCrime() != null && !crimeReviewDetails.getCountOfCrime().equalsIgnoreCase("")
                    && !crimeReviewDetails.getCountOfCrime().equalsIgnoreCase("null")){

                holder.tv_count.setText(crimeReviewDetails.getCountOfCrime());
                holder.tv_prev_count.setText(crimeReviewDetails.getPrevCountOfCrime());
            }
        }

    }

    @Override
    public int getItemCount() {
        return crimeReviewDetailsArrayList == null ? 0 : crimeReviewDetailsArrayList.size();
    }

    public void setClickListener(OnItemClickListenerForCrimeReview crimeReviewClickListener) {
        this.crimeReviewClickListener = crimeReviewClickListener;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_categoryName,tv_count,tv_prev_count;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_categoryName = (TextView) itemView.findViewById(R.id.tv_categoryName);
            tv_count = (TextView) itemView.findViewById(R.id.tv_count);
            tv_prev_count = (TextView) itemView.findViewById(R.id.tv_prev_count);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (crimeReviewClickListener != null) {
                crimeReviewClickListener.onClick(view, getAdapterPosition());
            }
        }
    }
}
