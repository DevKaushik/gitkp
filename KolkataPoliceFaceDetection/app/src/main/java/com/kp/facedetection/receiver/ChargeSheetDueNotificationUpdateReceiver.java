package com.kp.facedetection.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.TextView;

import com.kp.facedetection.DashboardActivity;
import com.kp.facedetection.interfaces.ChargesheetNotificationUpdateListener;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utils.L;

/**
 * Created by DAT-Asset-128 on 06-12-2017.
 */

public class ChargeSheetDueNotificationUpdateReceiver extends BroadcastReceiver implements ChargesheetNotificationUpdateListener{

    ChargesheetNotificationUpdateListener chargesheetNotificationUpdateListener;
    String value="0";

    @Override
    public void onReceive(Context context, Intent intent) {
        L.e("BROADCASTRECIVER  VALUE-----------"+intent.getStringExtra("data"));
        int count= Integer.parseInt(intent.getStringExtra("data"));
        ObservableObject.getInstance().updateValue(count);
       // context.sendBroadcast(new Intent("CHARGESHEET_UPDATE_VALUE"));

        /*DashboardActivity dashboardActivity=new DashboardActivity();
          dashboardActivity.setUpdatedChargesheetNotification(intent.getStringExtra("data"));*/
        /*value=intent.getStringExtra("data");
          onChargesheetNotificationUpdate(value);*/
    }
    public void setChargesheetNotificationUpdateListener(ChargesheetNotificationUpdateListener chargesheetNotificationUpdateListener){
        this.chargesheetNotificationUpdateListener=chargesheetNotificationUpdateListener;

    }



    public void onChargesheetNotificationUpdate(String value) {
        if(chargesheetNotificationUpdateListener!=null){
            L.e("BROADCASTRECIVER  VALUE set to Listener-----------"+value);
            chargesheetNotificationUpdateListener.onChargesheetNotificationUpdate(value);
        }


    }
}
