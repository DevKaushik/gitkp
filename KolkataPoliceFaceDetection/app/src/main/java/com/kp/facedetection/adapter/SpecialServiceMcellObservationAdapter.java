package com.kp.facedetection.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.SuggestionModel;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by user on 04-06-2018.
 */

public class SpecialServiceMcellObservationAdapter  extends RecyclerView.Adapter<SpecialServiceMcellObservationAdapter.ViewHolder>{
    private List<SuggestionModel> suggestionList;
    Context context;

    public SpecialServiceMcellObservationAdapter(Context context,List<SuggestionModel> suggestionList) {
        this.suggestionList = suggestionList;
        this.context = context;
    }

    @Override
    public SpecialServiceMcellObservationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nodal_suggestion_item_layout, parent, false);
        SpecialServiceMcellObservationAdapter.ViewHolder viewHolder = new SpecialServiceMcellObservationAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SpecialServiceMcellObservationAdapter.ViewHolder holder, int position) {
        Constants.changefonts(holder.comment_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.comment_date_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.comment_by_TV_label, context, "Calibri Bold.ttf");
        Constants.changefonts(holder.comment_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.comment_date_TV, context, "Calibri.ttf");
        Constants.changefonts(holder.comment_by_TV, context, "Calibri.ttf");
        holder.comment_TV.setText(suggestionList.get(position).getSuggestionDetails());
        holder.comment_date_TV.setText(suggestionList.get(position).getSuggestedTime());
        holder.comment_by_TV.setText(suggestionList.get(position).getSuggestionBy());



    }

    @Override
    public int getItemCount() {
        return (suggestionList != null ? suggestionList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView comment_TV_label,comment_date_TV_label,comment_by_TV_label;
        TextView comment_TV,comment_date_TV,comment_by_TV;


        public ViewHolder(View itemView) {
            super(itemView);
            comment_TV_label=(TextView)itemView.findViewById(R.id.comment_TV_label);
            comment_date_TV_label=(TextView)itemView.findViewById(R.id.comment_date_TV_label);
            comment_by_TV_label=(TextView)itemView.findViewById(R.id.comment_by_TV_label);
            comment_TV=(TextView)itemView.findViewById(R.id.comment_TV);
            comment_date_TV=(TextView)itemView.findViewById(R.id.comment_date_TV);
            comment_by_TV=(TextView)itemView.findViewById(R.id.comment_by_TV);
        }
    }
}
