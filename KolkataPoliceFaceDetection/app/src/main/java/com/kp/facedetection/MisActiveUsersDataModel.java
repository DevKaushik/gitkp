package com.kp.facedetection;

public class MisActiveUsersDataModel {
    String name;
    String psname;
    String rank;
    String appversion;
    String lastlogindate;
    String logintime;
    String cur_ver;


    public MisActiveUsersDataModel(String name, String psname, String rank, String appversion, String lastlogindate, String logintime,String cur_ver ) {
        this.name=name;
        this.psname=psname;
        this.rank=rank;
        this.appversion=appversion;
        this.lastlogindate=lastlogindate;
        this.logintime=logintime;
        this.cur_ver=cur_ver;

    }


    public String getName() {
        return name;
    }


    public String getPosting() {
        return psname;
    }


    public String getRank() {
        return rank;
    }

    public String getAppversion() {
        return appversion;
    }

    public String getLastlogindate() {
        return lastlogindate;
    }

    public String getLogInTime() {
        return logintime;
    }

    public String getCurVer() {
        return cur_ver;
    }
}

