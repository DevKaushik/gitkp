package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by DAT-165 on 10-09-2016.
 */
public class ModifiedDocumentGasDetails implements Serializable{

    private String distributor_name="";
    private String distributor_id= "";
    private String consumer_name="";
    private String consumer_id="";
    private String address = "";
    private String pincode= "";
    private String phone_no="";
    private String consumer_status = "";
    private String sv_date="";
    private String has_aadhar = "";


    public String getDistributor_name() {
        return distributor_name;
    }

    public void setDistributor_name(String distributor_name) {
        this.distributor_name = distributor_name;
    }

    public String getDistributor_id() {
        return distributor_id;
    }

    public void setDistributor_id(String distributor_id) {
        this.distributor_id = distributor_id;
    }

    public String getConsumer_name() {
        return consumer_name;
    }

    public void setConsumer_name(String consumer_name) {
        this.consumer_name = consumer_name;
    }

    public String getConsumer_id() {
        return consumer_id;
    }

    public void setConsumer_id(String consumer_id) {
        this.consumer_id = consumer_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getConsumer_status() {
        return consumer_status;
    }

    public void setConsumer_status(String consumer_status) {
        this.consumer_status = consumer_status;
    }

    public String getSv_date() {
        return sv_date;
    }

    public void setSv_date(String sv_date) {
        this.sv_date = sv_date;
    }

    public String getHas_aadhar() {
        return has_aadhar;
    }

    public void setHas_aadhar(String has_aadhar) {
        this.has_aadhar = has_aadhar;
    }
}
