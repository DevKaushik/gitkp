package com.kp.facedetection;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.model.NotificationDetails;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONObject;

import java.util.Observable;
import java.util.Observer;

public class NotificationDetailActivity extends BaseActivity  {
	NotificationDetails notificationData;
	TextView tv_sender, tv_subject, tv_description, tv_time;


	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notification_detail_layout);


		ActionBar actionBar = getActionBar();
		actionBar.setLogo(R.drawable.kplogo4);
		actionBar.setDisplayShowHomeEnabled(true);

		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#00004d")));
		getActionBar().setIcon(R.drawable.kplogo4);
		getActionBar().setTitle("");

		KPFaceDetectionApplication.getApplication().addActivityToList(this);
		
		initView();
	}

	private void initView() {
		tv_sender = (TextView) findViewById(R.id.tv_sender);
		tv_subject = (TextView) findViewById(R.id.tv_subject);
		tv_description = (TextView) findViewById(R.id.tv_description);
		tv_time = (TextView) findViewById(R.id.tv_time);
		notificationData = (NotificationDetails) getIntent().getBundleExtra(
				Constants.OBJECT).getSerializable(Constants.OBJECT);
		notificationDetaileTask(notificationData.getNotificationId());

	}

	private void notificationDetaileTask(String notificationId) {

		if (!Constants.internetOnline(this)) {
			Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
			return;
		}

		TaskManager taskManager = new TaskManager(this);
		taskManager.setMethod(Constants.METHOD_NOTIFICATION_DETAILS);
		taskManager.setNotificationDetails(true);
		String[] keys = { "notificationid" };
		String[] values = { notificationId.trim() };
		taskManager.doStartTask(keys, values, true);
	}

	public void parseNotificationDetails(String response) {
		Log.e("Response", "Response " + response);
		try {
			JSONObject jObj = new JSONObject(response);
			if (jObj.opt("status").toString().equalsIgnoreCase("1")) {
				JSONObject jsonResult = jObj.optJSONObject("result");
				tv_description.setText(jsonResult.optString("NOTIFICATION_DESCRIPTION"));
				tv_subject.setText(jsonResult.optString("NOTIFICATION_SUBJECT"));
				tv_sender.setText(jsonResult.optString("NOTIFICATION_SENDER"));
				tv_time.setText(jsonResult.optString("NOTIFICATION_DATE"));
				Utility.cancelNotification(this,
						notificationData.getNotificationId());
			}else{
				Toast.makeText(this, "Some error encountered!", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			Toast.makeText(this, "Some error encountered!", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}

	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_main, menu);
		return super.onCreateOptionsMenu(menu);
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		super.onOptionsItemSelected(item);

		switch (item.getItemId()) {
			/*case R.id.menu_send_push:
				startActivity(new Intent(this, SendPush.class));
				break;*/

			case R.id.menu_app_info:
				Utility.customDialogForAppInfo(this,Utility.getAppVersion(this));
				break;

			case R.id.menu_change_password:
				Utility.customDialogForChangePwd(this);
				break;

			case R.id.menu_feedback:
				startActivity(new Intent(this, FeedbackActivity.class));
				break;

			case R.id.menu_logout:
				logoutTask();
				// Utility.logout(this);
				break;
		}
		return true;

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		KPFaceDetectionApplication.getApplication().removeActivityToList(this);
	}


}
