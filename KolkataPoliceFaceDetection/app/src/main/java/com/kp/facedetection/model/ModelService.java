package com.kp.facedetection.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ModelService implements Serializable {
    private int status;
    private String message = "";
    private ArrayList<ModelRank> IO = new ArrayList<>();
    private ArrayList<ModelRank> OC = new ArrayList<>();
    private ArrayList<ModelRank> AC = new ArrayList<>();
    private ArrayList<ModelRank> DC = new ArrayList<>();
    private ArrayList<ModelRank> ADDLCP = new ArrayList<>();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ModelRank> getIO() {
        return IO;
    }

    public void setIO(ArrayList<ModelRank> IO) {
        this.IO = IO;
    }

    public ArrayList<ModelRank> getOC() {
        return OC;
    }

    public void setOC(ArrayList<ModelRank> OC) {
        this.OC = OC;
    }

    public ArrayList<ModelRank> getAC() {
        return AC;
    }

    public void setAC(ArrayList<ModelRank> AC) {
        this.AC = AC;
    }

    public ArrayList<ModelRank> getDC() {
        return DC;
    }

    public void setDC(ArrayList<ModelRank> DC) {
        this.DC = DC;
    }

    public ArrayList<ModelRank> getADDLCP() {
        return ADDLCP;
    }

    public void setADDLCP(ArrayList<ModelRank> ADDLCP) {
        this.ADDLCP = ADDLCP;
    }


}
