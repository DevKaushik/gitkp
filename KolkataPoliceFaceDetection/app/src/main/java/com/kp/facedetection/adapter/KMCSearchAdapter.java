package com.kp.facedetection.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kp.facedetection.R;
import com.kp.facedetection.model.KMCSearchDetails;
import com.kp.facedetection.utility.Constants;

import java.util.List;

/**
 * Created by DAT-165 on 05-12-2016.
 */
public class KMCSearchAdapter extends BaseAdapter {

    private Context context;
    private List<KMCSearchDetails> kmcSearchDetailsList;

    public KMCSearchAdapter(Context context, List<KMCSearchDetails> kmcSearchDetailsList) {
        this.context = context;
        this.kmcSearchDetailsList = kmcSearchDetailsList;
    }


    @Override
    public int getCount() {
        int size = (kmcSearchDetailsList.size() > 0) ? kmcSearchDetailsList.size() : 0;
        return size;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.modified_sdr_search_list_item, null);

            holder.tv_slNo = (TextView) convertView.findViewById(R.id.tv_slNo);
            holder.tv_wardNo = (TextView) convertView.findViewById(R.id.tv_name);  // set consumer id to name field
            holder.tv_ownerName = (TextView) convertView.findViewById(R.id.tv_address); //set name to address field
            holder.tv_address = (TextView) convertView.findViewById(R.id.tv_mobileNo);  // set address to mobile field
            holder.tv_id_number=(TextView) convertView.findViewById(R.id.tv_id_number);
            holder.tv_id_type=(TextView)convertView.findViewById(R.id.tv_id_type);
           // holder.tv_id_number.setVisibility(View.GONE);
           // holder.tv_id_type.setVisibility(View.GONE);
            Constants.changefonts(holder.tv_slNo, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_wardNo, context, "Calibri Bold.ttf");
            Constants.changefonts(holder.tv_ownerName, context, "Calibri.ttf");
            Constants.changefonts(holder.tv_address, context, "Calibri.ttf");

            convertView.setTag(holder);
        }
        else{

            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
        }

        String sl_no = Integer.toString(position + 1);

        holder.tv_slNo.setText(sl_no);

        if(kmcSearchDetailsList.get(position).getKmc_wardNo() != null)
            holder.tv_wardNo.setText("WARD NO: "+kmcSearchDetailsList.get(position).getKmc_wardNo().trim());

        if(kmcSearchDetailsList.get(position).getKmc_owner() != null)
            holder.tv_ownerName.setText("OWNER NAME: "+kmcSearchDetailsList.get(position).getKmc_owner().trim());

        if(kmcSearchDetailsList.get(position).getKmc_mailingAddress() != null)
            holder.tv_address.setText("ADDRESS: "+kmcSearchDetailsList.get(position).getKmc_mailingAddress().trim());

        return convertView;
    }


    class ViewHolder {

        TextView tv_slNo,tv_ownerName,tv_address,tv_wardNo,tv_id_number,tv_id_type;
    }
}
