package com.kp.facedetection.adapter;

/**
 * Created by DAT-Asset-131 on 31-05-2016.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.text.InputType;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kp.facedetection.R;
import com.kp.facedetection.imageloader.ImageLoader;
import com.kp.facedetection.interfaces.OnDescriptiveRoleUpdateListener;
import com.kp.facedetection.model.AssociateDetails;
import com.kp.facedetection.model.DescriptiveRoleModelModified;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.utils.L;
import com.kp.facedetection.utils.MyClickableSpan;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ExpandableListAdapterForProfile extends BaseExpandableListAdapter {

    private String selectedSubItem = "";
    private int pos_selected;
    private boolean isChecked = false;
    int posSelect;
    OnDescriptiveRoleUpdateListener onDescriptiveRoleUpdateListener;

    private Context _context;
    private List<String> _listDataHeader=new ArrayList<>(); // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild =new HashMap<>();
    private List<AssociateDetails> associateDetailsList;
    private List<DescriptiveRoleModelModified> descriptiveRoleList;
    private List<String> CRSList = new ArrayList<String>();
    private List<String> CRSImageList = new ArrayList<String>();


    private boolean isEditClicked;
    private boolean isAlertCicked;

    ImageLoader imageLoader;
    String title = "", message = "";


    public ExpandableListAdapterForProfile(Context context, List<String> listDataHeader,
                                           HashMap<String, List<String>> listChildData, List<DescriptiveRoleModelModified> descriptiveRoleList, List<String> CRSList, List<String> CRSImageList, List<AssociateDetails> associateDetailsList) {
        this._context = context;
        this._listDataHeader.addAll( listDataHeader);
        this._listDataChild.putAll(listChildData);
        this.descriptiveRoleList = descriptiveRoleList;
        this.CRSList = CRSList;
        this.CRSImageList = CRSImageList;
        this.associateDetailsList = associateDetailsList;
        imageLoader = new ImageLoader(context);

    }

    public void setOnDescriptiveRoleUpdateListener(OnDescriptiveRoleUpdateListener onDescriptiveRoleUpdateListener) {
        this.onDescriptiveRoleUpdateListener = onDescriptiveRoleUpdateListener;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(_context, R.layout.layout_criminal_details_item_row, null);
        }
        final String childText = (String) getChild(groupPosition, childPosition);
        final String groupText = (String) getGroup(groupPosition);
        final TextView tv_value;
        final TextView tv_key;
        // final EditText ed_value;
        TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
        TextView tv_address = (TextView) convertView.findViewById(R.id.tv_address);
        ImageView iv_searchResultImage = (ImageView) convertView.findViewById(R.id.iv_searchResultImage);
        RelativeLayout relative_profile = (RelativeLayout) convertView.findViewById(R.id.relative_profile);
        RelativeLayout relative_descriptive = (RelativeLayout) convertView.findViewById(R.id.relative_desc);
        RelativeLayout relative_associate = (RelativeLayout) convertView.findViewById(R.id.relative_associate);
        relative_descriptive.setVisibility(View.GONE);
        relative_associate.setVisibility(View.GONE);
        relative_profile.setVisibility(View.GONE);


        if (groupText.equalsIgnoreCase("Descriptive Role")) {
            tv_key = (TextView) convertView.findViewById(R.id.tv_key_desc);
            tv_value = (TextView) convertView.findViewById(R.id.tv_value_desc);
        } else {
            tv_key = (TextView) convertView.findViewById(R.id.tv_key);
            tv_value = (TextView) convertView.findViewById(R.id.tv_value);
        }


        if (groupText.equalsIgnoreCase("Associate")) {
            relative_associate.setVisibility(View.VISIBLE);
            relative_profile.setVisibility(View.GONE);
            relative_descriptive.setVisibility(View.GONE);
            Constants.changefonts(tv_name, _context, "Calibri.ttf");
            Constants.changefonts(tv_address, _context, "Calibri.ttf");

            if (Integer.parseInt(childText) % 2 == 0) {
                convertView.setBackgroundColor(Color.parseColor("#f2f2f2"));
            } else {
                convertView.setBackgroundColor(Color.parseColor("#dbe5f1"));
            }

            tv_address.setEllipsize(TextUtils.TruncateAt.END);


            tv_name.setText(associateDetailsList.get(Integer.parseInt(childText)).getAssociate_name());
            tv_address.setText(associateDetailsList.get(Integer.parseInt(childText)).getAssociate_address());

            imageLoader.DisplayImage(
                    associateDetailsList.get(Integer.parseInt(childText)).getAssociate_picture(),
                    iv_searchResultImage);

        } else if (groupText.equalsIgnoreCase("CRS")) {
            relative_associate.setVisibility(View.GONE);
            relative_profile.setVisibility(View.VISIBLE);
            relative_descriptive.setVisibility(View.GONE);

            try {
                String original_key = childText.substring(0, childText.indexOf("^"));
                String modified_key = original_key.substring(0, 1).toUpperCase() + original_key.substring(1);
                String value = childText.substring(childText.indexOf("^") + 1);

                tv_key.setText(modified_key);

                if (modified_key.contains("Photo(s)")) {

                    Constants.changefonts(tv_key, _context, "Calibri Bold.ttf");
                    Constants.changefonts(tv_value, _context, "Calibri Bold.ttf");

                    SpannableString ss = null;
                    SpannableStringBuilder totalSpannedString = new SpannableStringBuilder();

                    String data[];

                    data = value.split("  ");

                    for (int i = 0; i < data.length; i++) {
                        ss = new SpannableString(data[i]);
                        ss.setSpan(new MyClickableSpan(i, _context, CRSImageList), 0, data[i].length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        totalSpannedString.append(ss);
                        totalSpannedString.append(" ");
                    }

                    System.out.println("Length " + data.length);
                    tv_value.setText(totalSpannedString);
                    tv_value.setMovementMethod(LinkMovementMethod.getInstance());
                    tv_value.setLineSpacing(0.2f, 1.1f);

                    // tv_value.setTextColor(ContextCompat.getColor(_context, R.color.colorPrimaryDark));


                } else {
                    tv_value.setTextColor(ContextCompat.getColor(_context, R.color.color_black));
                    tv_value.setText(value);


                    Constants.changefonts(tv_key, _context, "Calibri Bold.ttf");
                    Constants.changefonts(tv_value, _context, "Calibri.ttf");
                }


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if (groupText.equalsIgnoreCase("Descriptive Role")) {

            relative_associate.setVisibility(View.GONE);
            relative_profile.setVisibility(View.GONE);
            relative_descriptive.setVisibility(View.VISIBLE);
            tv_key.setText(descriptiveRoleList.get(childPosition).getCategory());

            if (descriptiveRoleList.get(childPosition).getCategory().equalsIgnoreCase("HEIGHT_FEET") || descriptiveRoleList.get(childPosition).getCategory().equalsIgnoreCase("HEIGHT_INCH")) {
                if (descriptiveRoleList.get(childPosition).getIndValue().isEmpty() || descriptiveRoleList.get(childPosition).getIndValue().equalsIgnoreCase("null")) {
                    tv_value.setText("");
                } else {
                    tv_value.setText(descriptiveRoleList.get(childPosition).getIndValue());
                    // ed_value.setText(descriptiveRoleList.get(childPosition).getIndValue());
                }
            } else {
                final ArrayList<String> isSelected2 = descriptiveRoleList.get(childPosition).getIsSelected();

                final int pos = isSelected2.indexOf("1");
                final ArrayList<String> value2 = descriptiveRoleList.get(childPosition).getValue();
                if (pos != -1) {
                    tv_value.setText(value2.get(pos));
                } else {
                    tv_value.setText("");
                }
            }
            Constants.changefonts(tv_key, _context, "Calibri Bold.ttf");
            Constants.changefonts(tv_value, _context, "Calibri.ttf");

            if (isEditClicked) {
                tv_value.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (descriptiveRoleList.get(childPosition).getCategory().equalsIgnoreCase("HEIGHT_FEET") || descriptiveRoleList.get(childPosition).getCategory().equalsIgnoreCase("HEIGHT_INCH")) {

                            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(_context, R.style.MyAlertDialogTheme));
                            View editView = LayoutInflater.from(_context).inflate(R.layout.descriptive_height_dialog_layout, null);
                            final EditText edText = (EditText) editView.findViewById(R.id.ed_height);
                            edText.setText(descriptiveRoleList.get(childPosition).getIndValue());
                            alertDialog.setTitle("Enter height");
                            alertDialog.setView(editView);
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (edText.getText().toString().trim().isEmpty()) {
                                        Toast.makeText(_context, "Please enter a value", Toast.LENGTH_SHORT).show();

                                    }
                                    else  if(descriptiveRoleList.get(childPosition).getCategory().equalsIgnoreCase("HEIGHT_FEET") && (Integer.parseInt(edText.getText().toString().trim())>7 ||Integer.parseInt(edText.getText().toString().trim())<1))
                                    {
                                        Toast.makeText(_context, "Please enter the value between 1 to 7", Toast.LENGTH_SHORT).show();
                                    }
                                    else if(descriptiveRoleList.get(childPosition).getCategory().equalsIgnoreCase("HEIGHT_INCH")&& Integer.parseInt(edText.getText().toString().trim())>11)
                                    {
                                        Toast.makeText(_context, "Please enter the value between 0 to 11", Toast.LENGTH_SHORT).show();
                                    }
                                    else {
                                        descriptiveRoleList.get(childPosition).setIndValue(edText.getText().toString().trim());
                                        notifyDataSetChanged();
                                        dialog.dismiss();


                                    }

                                }
                            });
                            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            alertDialog.show();

                            //Toast.makeText(_context, tv_value.getText().toString(), Toast.LENGTH_SHORT).show();


                        } else {

                            final ArrayList<String> isSelected = descriptiveRoleList.get(childPosition).getIsSelected();
                            L.e(isSelected);

                            final ArrayList<String> value = descriptiveRoleList.get(childPosition).getValue();
                            L.e(value);


                            Log.e("CLICK", "position:" + value.size() + "cate:" + tv_key.getText().toString());

                            if (!tv_value.getText().toString().trim().equalsIgnoreCase("")) {
                                posSelect = value.indexOf(tv_value.getText().toString());
                                pos_selected = posSelect;
                                //pos_selected = isSelected.indexOf("1");
                                L.e("position any" + pos_selected);
                            } else {
                                pos_selected = -1;
                                L.e("position -1 :---->" + pos_selected);
                            }


                            final String[] subItemArray;

                            if (value.size() > 0) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(_context, R.style.myDialog);
                                builder.setTitle("Select");
                                subItemArray = new String[value.size()];
                                final Object[] subItem = value.toArray();
                                for (int i = 0; i < subItem.length; i++) {
                                    subItemArray[i] = (String) subItem[i];
                                }


                                builder.setSingleChoiceItems(subItemArray, pos_selected, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int pos) {
                                        L.e("Selected Attrib Position:---->" + pos);
                                        pos_selected = pos;
                                        selectedSubItem = Arrays.asList(subItemArray).get(pos);
                                        L.e("Selected Name:---->" + selectedSubItem);
                                        isChecked = true;

                                    }


                                });

                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        if (isChecked) {
                                            L.e("Selected Name before setting :---->" + selectedSubItem);
                                            Constants.changefonts(tv_value, _context, "Calibri.ttf");
                                            tv_value.setText(selectedSubItem);
                                            int index_of_val = value.indexOf(selectedSubItem);//for updating the descriptive role arraylist

                                            isSelected.set(posSelect, "0");
                                            isSelected.set(index_of_val, "1");

                                            selectedSubItem = "";
                                            isChecked = false;
                                                       /* isSelected.set(posSelect,"0");
                                                        isSelected.set(pos_selected,"1");*/
                                            notifyDataSetChanged();
                                        }


                                    }
                                });


                                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        selectedSubItem = "";
                                    }
                                });

                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }


                    }
                });
            } else {
                tv_value.setOnClickListener(null);
            }


        } else {
            relative_associate.setVisibility(View.GONE);
            relative_profile.setVisibility(View.VISIBLE);
            relative_descriptive.setVisibility(View.GONE);

            try {

                String original_key = childText.substring(0, childText.indexOf("^"));
                String modified_key = original_key.substring(0, 1).toUpperCase() + original_key.substring(1);
                String value = childText.substring(childText.indexOf("^") + 1);

                Constants.changefonts(tv_key, _context, "Calibri Bold.ttf");
                Constants.changefonts(tv_value, _context, "Calibri.ttf");

                tv_key.setText(modified_key);
                tv_value.setText(value);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        relative_descriptive.setClickable(isEditClicked);
        return convertView;
    }

    public void editDescriptiveRole() {
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int size = 0;
        try {
Log.e("TAG","TAG Child:"+_listDataChild.size());
            Log.e("TAG","TAG Header:"+_listDataHeader.size());
            Log.e("TAG","TAG Position:"+groupPosition);
            Log.e("TAG","TAG Position:"+_listDataHeader.get(groupPosition));
            Log.e("TAG","TAG Position:"+_listDataChild.get(_listDataHeader.get(groupPosition)).size());

            size = this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return size;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded,
                             View convertView, final ViewGroup parent) {

        final String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        final ImageView ivEdit = (ImageView) convertView.findViewById(R.id.lblListEdit);


        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        ivEdit.setImageResource(isEditClicked ? R.drawable.save_dec_role : R.drawable.edit_desc_role);

        if (headerTitle.equals("Descriptive Role")) {
            if (getChildrenCount(groupPosition) > 0) {
                ivEdit.setVisibility(View.VISIBLE);
            } else {
                ivEdit.setVisibility(View.GONE);
            }
        } else {
            ivEdit.setVisibility(View.GONE);
        }


        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEditClicked) {
                    JSONObject jo = new JSONObject();
                    for (int i = 0; i < descriptiveRoleList.size(); i++) {
                        DescriptiveRoleModelModified descriptiveRoleModelModified = descriptiveRoleList.get(i);
                        if (descriptiveRoleList.get(i).getCategory().equalsIgnoreCase("HEIGHT_FEET") || descriptiveRoleList.get(i).getCategory().equalsIgnoreCase("HEIGHT_INCH")) {
                            try {
                                jo.put(descriptiveRoleModelModified.getCategory(), descriptiveRoleModelModified.getIndValue());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            ArrayList<String> isSelected = descriptiveRoleList.get(i).getIsSelected();
                            int indexOf = isSelected.indexOf("1");
                            ArrayList<String> value = descriptiveRoleList.get(i).getValue();
                            String setVal = "";
                            if (indexOf == -1) {

                            } else {
                                setVal = value.get(indexOf);
                            }

                            try {
                                jo.put(descriptiveRoleModelModified.getCategory(), setVal);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    L.e("TEST " + jo);
                    onDescriptiveRoleUpdateListener.updateDescriptiveRole(jo);


                } else {
                    Utility.showAlertDialog(_context,Constants.ERROR_CAPTURE_INFO_TITLE,Constants.DESCRIPTIVE_ROLE_EDIT_MESSAGE,true);
                  /*  final android.app.AlertDialog ad = new android.app.AlertDialog.Builder(_context).create();

                    // Setting Dialog Title
                    ad.setTitle(Constants.ERROR_CAPTURE_ALERT_TITLE);

                    // Setting Dialog Message
                    ad.setMessage(Constants.DESCRIPTIVE_ROLE_EDIT_MESSAGE);

                    // Setting alert dialog icon
                    ad.setIcon(R.drawable.success);


                    ad.setButton(DialogInterface.BUTTON_POSITIVE,
                            "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    isAlertCicked = true;


                                }
                            });

                    // Showing Alert Message
                    ad.show();
                    ad.setCanceledOnTouchOutside(false);*/
                }
                isEditClicked = !isEditClicked;
                notifyDataSetChanged();

            }
        });

        /*tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jo = new JSONObject();
                for (int i = 0; i < descriptiveRoleList.size(); i++) {
                    DescriptiveRoleModelModified descriptiveRoleModelModified = descriptiveRoleList.get(i);
                    ArrayList<String> isSelected = descriptiveRoleList.get(i).getIsSelected();
                    int indexOf = isSelected.indexOf("1");
                    ArrayList<String> value = descriptiveRoleList.get(i).getValue();
                    String setVal = "";
                    if (indexOf == -1) {

                    } else {
                        setVal = value.get(indexOf);
                    }

                    try {
                        jo.put(descriptiveRoleModelModified.getCategory(), setVal);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                L.e(jo);
                onDescriptiveRoleUpdateListener.updateDescriptiveRole(jo);
            }
        });*/


      /*  ImageView img_selection=(ImageView) view.findViewById(R.id.img_selection);
        int imageResourceId = isExpanded ? R.drawable.group_closed
                : R.drawable.group_open;
        img_selection.setImageResource(imageResourceId);*/


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}
