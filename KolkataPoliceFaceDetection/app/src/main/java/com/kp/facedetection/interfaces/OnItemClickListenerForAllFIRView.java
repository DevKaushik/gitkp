package com.kp.facedetection.interfaces;

import android.view.View;

/**
 * Created by DAT-165 on 08-06-2017.
 */

public interface OnItemClickListenerForAllFIRView {
    void onClick(View view, int position);
}
