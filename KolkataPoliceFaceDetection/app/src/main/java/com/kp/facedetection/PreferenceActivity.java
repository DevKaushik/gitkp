package com.kp.facedetection;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kp.facedetection.adapter.CustomDialogAdapterForCategoryCrimeList2;
import com.kp.facedetection.model.AllContentList;
import com.kp.facedetection.model.CrimeCategoryList;
import com.kp.facedetection.model.CrimeCategoryList2;
import com.kp.facedetection.model.DivisionList;
import com.kp.facedetection.model.PoliceStationList;
import com.kp.facedetection.utility.Constants;
import com.kp.facedetection.utility.ObservableObject;
import com.kp.facedetection.utility.Utility;
import com.kp.facedetection.webservices.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class PreferenceActivity extends BaseActivity implements View.OnClickListener, SearchView.OnQueryTextListener, Observer {

    private String userName = "";
    private String loginNumber = "";
    private String appVersion = "";

    private CheckBox chk_kp, chk_division, chk_ps;
    private Spinner spn_division, spn_ps;
    private TextView txt_category, txt_ps, txt_division;
    private Button btn_done, btn_reset;

    protected String[] array_policeStation;
    protected String[] array_division;
    protected ArrayList<CharSequence> selectedPoliceStations = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedPoliceStationsId = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedDivision = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedDivisionId = new ArrayList<CharSequence>();
    private String policeStationString = "", divisionString = "";

    public AllContentList allContentList;
    public JSONArray policeStationListArray;
    public JSONArray crimeCategoryListArray;
    public JSONArray divisionListArray;


    protected String[] array_crimeCategory;

    List<String> policeStationArrayList = new ArrayList<String>();
    List<String> policeStationIDArrayList = new ArrayList<String>();

    List<String> divisionArrayList = new ArrayList<String>();
    List<String> divisionArrayList_code = new ArrayList<String>();


    private ArrayList<CrimeCategoryList2> obj_categoryCrimeList2;
    List<CrimeCategoryList> new_crimeCatList;
    ArrayList<PoliceStationList> obj_policeStationList;

    private String crimeCategoryString = "";
    private boolean crimeCategory_status = false;
    CustomDialogAdapterForCategoryCrimeList2 customDialogAdapterForCategoryCrimeList2;
    private ListView lv_dialog;
    private List<String> modified_crimeCategoryList = new ArrayList<String>();

    private String selectedDate = "";

    // Shared Preference set for save all arraylist
    public static final String MyPREFERENCES = "MyPrefs_for_PreferenceActivity";
    SharedPreferences sharedPrefs;
    SharedPreferences.Editor editor;
    Gson gson;

    String json_psNameList;
    String json_psIDList;
    String json_divNameList;
    String json_divCodeList;
    String json_crimeCatList;

    public static final String TAG_PS_NAME_LIST = "tag_ps_name";
    public static final String TAG_PS_ID_LIST = "tag_ps_id";
    public static final String TAG_DIV_NAME_LIST = "tag_div_name";
    public static final String TAG_DIV_CODE_LIST = "tag_div_code";
    public static final String TAG_CRIME_CATEGORY_LIST = "tag_crime_category";

    String div = "", ps = "", crime = "";

    private boolean state = false;
    TextView chargesheetNotificationCount;
    String notificationCount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);
        ObservableObject.getInstance().addObserver(this);
        setToolBar();
        initFields();
    }


    private void setToolBar() {
        try {
            userName = Utility.getUserInfo(this).getName();
            loginNumber = Utility.getUserInfo(this).getLoginNumber();

            System.out.println("UserName: " + userName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#00004d")));
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_layout, null);
        TextView tv_username = (TextView) mCustomView.findViewById(R.id.tv_username);
        TextView tv_loginCount = (TextView) mCustomView.findViewById(R.id.tv_loginCount);
        CoordinatorLayout charseet_due_notification = (CoordinatorLayout)mCustomView.findViewById(R.id.charseet_due_notification);
        makeNotifictionVisibleAsPerRole(this,charseet_due_notification);
        charseet_due_notification.setOnClickListener(this);
        chargesheetNotificationCount=(TextView)mCustomView.findViewById(R.id.tv_chargeSheet_notification_count);
        notificationCount=Utility.getUserNotificationCount(this);
        updateNotificationCount(notificationCount);
        tv_username.setText("Welcome " + userName);
        tv_loginCount.setText("Login Count: " + loginNumber);

        Constants.changefonts(tv_username, this, "Calibri Bold.ttf");
        Constants.changefonts(tv_loginCount, this, "Calibri.ttf");

        //App Version set
        appVersion = Utility.getAppVersion(this);
        TextView tv_appVersion = (TextView) mCustomView.findViewById(R.id.tv_appVersion);
        tv_appVersion.setText("Version: " + appVersion);
        Constants.changefonts(tv_appVersion, this, "Calibri.ttf");

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        KPFaceDetectionApplication.getApplication().addActivityToList(this);
    }

    private void initFields() {

        selectedDate = getIntent().getExtras().getString("SELECTED_DATE_PREFERENCE");

        chk_kp = (CheckBox) findViewById(R.id.chk_kp);
        chk_division = (CheckBox) findViewById(R.id.chk_division);
        chk_ps = (CheckBox) findViewById(R.id.chk_ps);

        spn_division = (Spinner) findViewById(R.id.spn_division);
        spn_ps = (Spinner) findViewById(R.id.spn_ps);

        txt_ps = (TextView) findViewById(R.id.txt_ps);
        txt_ps.setOnClickListener(this);

        txt_division = (TextView) findViewById(R.id.txt_division);
        txt_division.setOnClickListener(this);

        txt_category = (TextView) findViewById(R.id.txt_category);
        txt_category.setOnClickListener(this);

        btn_done = (Button) findViewById(R.id.btn_done);
        btn_done.setOnClickListener(this);
        Constants.buttonEffect(btn_done);

        btn_reset = (Button) findViewById(R.id.btn_reset);
        btn_reset.setOnClickListener(this);
        Constants.buttonEffect(btn_reset);

        /*
        initialize Shared preference and create Gson object to store
        PS_NAME_LIST, PS_ID_LIST, DIV_NAME_LIST, DIV_CIDE_LIST, CRIME_LIST
        */
        sharedPrefs = getSharedPreferences(PreferenceActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPrefs.edit();
        gson = new Gson();

        if (!sharedPrefs.contains(TAG_PS_NAME_LIST) && !sharedPrefs.contains(TAG_PS_ID_LIST) && !sharedPrefs.contains(TAG_DIV_NAME_LIST) && !sharedPrefs.contains(TAG_DIV_CODE_LIST) && !sharedPrefs.contains(TAG_CRIME_CATEGORY_LIST)) {
            fetchAllContent();
            Log.e("MOITRI:", "ps list 111111");
        } else {

            // Retreive data from shared preference
            String json_ps = sharedPrefs.getString(TAG_PS_NAME_LIST, null);
            ArrayList<String> arrayList_ps = gson.fromJson(json_ps, new TypeToken<ArrayList<String>>() {
            }.getType());
            Log.e("MOITRI:", "ps list - " + arrayList_ps.size());
        }



        DashboardActivity.sharedPrefs_forinnerData = getSharedPreferences(DashboardActivity.MyPREFERENCES_INNER_DATA, Context.MODE_PRIVATE);
        DashboardActivity.editor_forinnerData = DashboardActivity.sharedPrefs_forinnerData.edit();

        if (DashboardActivity.sharedPrefs_forinnerData.contains(DashboardActivity.TAG_CHECKED_KP) || DashboardActivity.sharedPrefs_forinnerData.contains(DashboardActivity.TAG_CHECKED_DIV) || DashboardActivity.sharedPrefs_forinnerData.contains(DashboardActivity.TAG_CHECKED_PS) || DashboardActivity.sharedPrefs_forinnerData.contains(DashboardActivity.TAG_CRIME_CATEGORY_LIST_INNER)) {
            showAllPreferenceData();
        }


        chk_kp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    state = true;

                    chk_division.setChecked(true);
                    chk_division.setEnabled(false);
                    resetDivisionValue();
                    txt_division.setVisibility(View.GONE);

                    chk_ps.setChecked(true);
                    chk_ps.setEnabled(false);
                    resetPSValue();
                    txt_ps.setVisibility(View.GONE);

                } else {
                    chk_division.setChecked(false);
                    chk_division.setEnabled(true);
                    chk_ps.setChecked(false);
                    chk_ps.setEnabled(true);
                }
            }
        });

        chk_division.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    state = true;

                    txt_division.setVisibility(View.VISIBLE);

                    chk_ps.setChecked(true);
                    chk_ps.setEnabled(false);

                    resetPSValue();
                    txt_ps.setVisibility(View.GONE);

                } else {

                    resetDivisionValue();
                    txt_division.setVisibility(View.GONE);
                    chk_ps.setChecked(false);
                    chk_ps.setEnabled(true);
                }
            }
        });

        chk_ps.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    state = true;

                    resetPSValue();
                    txt_ps.setVisibility(View.VISIBLE);

                } else {

                    resetPSValue();
                    txt_ps.setVisibility(View.GONE);
                }
            }
        });
    }


    private void showAllPreferenceData() {

        Log.e("MOI::", "showAllPreferenceData");

        if (DashboardActivity.sharedPrefs_forinnerData.contains(DashboardActivity.TAG_CHECKED_KP)) {
            chk_kp.setChecked(DashboardActivity.sharedPrefs_forinnerData.getBoolean(DashboardActivity.TAG_CHECKED_KP, false));
            //chk_division.setChecked(sharedPrefs_forinnerData.getBoolean(TAG_CHECKED_DIV, false));
            //chk_ps.setChecked(sharedPrefs_forinnerData.getBoolean(TAG_CHECKED_PS, false));
            chk_division.setChecked(true);
            chk_division.setEnabled(false);
            chk_ps.setChecked(true);
            chk_ps.setEnabled(false);

            Log.e("MOI::", "KP show");
        } else if (DashboardActivity.sharedPrefs_forinnerData.contains(DashboardActivity.TAG_CHECKED_DIV)) {
            //chk_kp.setChecked(sharedPrefs_forinnerData.getBoolean(TAG_CHECKED_KP, false));
            chk_division.setChecked(DashboardActivity.sharedPrefs_forinnerData.getBoolean(DashboardActivity.TAG_CHECKED_DIV, false));
            //chk_ps.setChecked(sharedPrefs_forinnerData.getBoolean(TAG_CHECKED_PS, false));

            chk_kp.setChecked(false);
            chk_ps.setChecked(true);
            chk_ps.setEnabled(false);

            //if(sharedPrefs_forinnerData.contains(TAG_DIV_NAME_LIST_INNER))
            txt_division.setVisibility(View.VISIBLE);
            txt_division.setText(DashboardActivity.sharedPrefs_forinnerData.getString(DashboardActivity.TAG_DIV_NAME_LIST_INNER, null));
            div = DashboardActivity.sharedPrefs_forinnerData.getString(DashboardActivity.TAG_DIV_CODE_LIST_INNER, "");


        } else if (DashboardActivity.sharedPrefs_forinnerData.contains(DashboardActivity.TAG_CHECKED_PS)) {
            //chk_kp.setChecked(sharedPrefs_forinnerData.getBoolean(TAG_CHECKED_KP, false));
            //chk_division.setChecked(sharedPrefs_forinnerData.getBoolean(TAG_CHECKED_DIV, false));
            chk_ps.setChecked(DashboardActivity.sharedPrefs_forinnerData.getBoolean(DashboardActivity.TAG_CHECKED_PS, false));

            chk_kp.setChecked(false);
            chk_division.setChecked(false);

            txt_ps.setVisibility(View.VISIBLE);
            txt_ps.setText(DashboardActivity.sharedPrefs_forinnerData.getString(DashboardActivity.TAG_PS_NAME_LIST_INNER, null));
            ps = DashboardActivity.sharedPrefs_forinnerData.getString(DashboardActivity.TAG_PS_ID_LIST_INNER, "");


        } else if (DashboardActivity.sharedPrefs_forinnerData.contains(DashboardActivity.TAG_CRIME_CATEGORY_LIST_INNER) && !DashboardActivity.sharedPrefs_forinnerData.contains(DashboardActivity.TAG_CHECKED_KP) && !DashboardActivity.sharedPrefs_forinnerData.contains(DashboardActivity.TAG_CHECKED_DIV) && !DashboardActivity.sharedPrefs_forinnerData.contains(DashboardActivity.TAG_CHECKED_PS)) {

            if(DashboardActivity.sharedPrefs_forinnerData.getString(DashboardActivity.TAG_CRIME_CATEGORY_SHOW_LIST_INNER, "") != null){
                Log.e("FREE","1. TXT_crime free");
                txt_category.setText("Select Category Of Crime");
                crime = DashboardActivity.sharedPrefs_forinnerData.getString(DashboardActivity.TAG_CRIME_CATEGORY_LIST_INNER, "");
            }
            else {
                Log.e("FREE","2. TXT_crime free");
                txt_category.setText(DashboardActivity.sharedPrefs_forinnerData.getString(DashboardActivity.TAG_CRIME_CATEGORY_SHOW_LIST_INNER, null));
                crime = DashboardActivity.sharedPrefs_forinnerData.getString(DashboardActivity.TAG_CRIME_CATEGORY_LIST_INNER, "");
            }
        } else {
            Log.e("FREE","3. TXT_crime free");
        }

        if (DashboardActivity.sharedPrefs_forinnerData.contains(DashboardActivity.TAG_CRIME_CATEGORY_LIST_INNER) && DashboardActivity.sharedPrefs_forinnerData.contains(DashboardActivity.TAG_CRIME_CATEGORY_SHOW_LIST_INNER)) {
            txt_category.setText(DashboardActivity.sharedPrefs_forinnerData.getString(DashboardActivity.TAG_CRIME_CATEGORY_SHOW_LIST_INNER, null));
            crime = DashboardActivity.sharedPrefs_forinnerData.getString(DashboardActivity.TAG_CRIME_CATEGORY_LIST_INNER, "");
        } else {
            Log.e("CRIME::", "CRIME not show");
        }

        /*div = sharedPrefs_forinnerData.getString(TAG_DIV_CODE_LIST_INNER, "");
        ps = sharedPrefs_forinnerData.getString(TAG_PS_ID_LIST_INNER, "");
        crime = sharedPrefs_forinnerData.getString(TAG_CRIME_CATEGORY_LIST_INNER, "");*/

        Log.e("MOI:", "1. DIV -" + div);
        Log.e("MOI:", "1. PS -" + ps);
        Log.e("MOI:", "1. CRIME -" + crime);

    }

    private void resetDivisionValue() {

        txt_division.setText("Select Divisions");
        selectedDivision.clear();
        selectedDivisionId.clear();
        divisionString = "";
    }

    private void resetPSValue() {

        txt_ps.setText("Select Police Stations");
        selectedPoliceStations.clear();
        selectedPoliceStationsId.clear();
        policeStationString = "";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_ps:

                state = true;

                if (sharedPrefs.contains(TAG_PS_NAME_LIST) && sharedPrefs.contains(TAG_PS_ID_LIST)) {
                    String json_ps = sharedPrefs.getString(TAG_PS_NAME_LIST, null);
                    policeStationArrayList = gson.fromJson(json_ps, new TypeToken<ArrayList<String>>() {
                    }.getType());

                    String json_ps_id = sharedPrefs.getString(TAG_PS_ID_LIST, null);
                    policeStationIDArrayList = gson.fromJson(json_ps_id, new TypeToken<ArrayList<String>>() {
                    }.getType());
                }

                array_policeStation = new String[policeStationArrayList.size()];
                array_policeStation = policeStationArrayList.toArray(array_policeStation);


                //if (Constants.policeStationNameArrayList.size() > 0) {
                if (policeStationArrayList.size() > 0) {
                    //txt_ps.setError(null);
                    showSelectPoliceStationsDialog();
                } else {
                    Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }
                break;

            case R.id.txt_division:

                state = true;

                if (sharedPrefs.contains(TAG_DIV_NAME_LIST) && sharedPrefs.contains(TAG_DIV_CODE_LIST)) {
                    String json_div = sharedPrefs.getString(TAG_DIV_NAME_LIST, null);
                    divisionArrayList = gson.fromJson(json_div, new TypeToken<ArrayList<String>>() {
                    }.getType());

                    String json_div_code = sharedPrefs.getString(TAG_DIV_CODE_LIST, null);
                    divisionArrayList_code = gson.fromJson(json_div_code, new TypeToken<ArrayList<String>>() {
                    }.getType());
                }

                array_division = new String[divisionArrayList.size()];
                array_division = divisionArrayList.toArray(array_division);


                if (divisionArrayList.size() > 0) {
                    //txt_division.setError(null);
                    showSelectDivisionDialog();
                } else {
                    Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }
                break;

            case R.id.txt_category:

                state = true;

                if (sharedPrefs.contains(TAG_CRIME_CATEGORY_LIST)) {
                    String json_crime = sharedPrefs.getString(TAG_CRIME_CATEGORY_LIST, null);
                    new_crimeCatList = gson.fromJson(json_crime, new TypeToken<ArrayList<CrimeCategoryList>>() {
                    }.getType());
                }

                createModifiedCrimeCategoryList(new_crimeCatList);

                array_crimeCategory = new String[modified_crimeCategoryList.size()];
                array_crimeCategory = modified_crimeCategoryList.toArray(array_crimeCategory);

                if (modified_crimeCategoryList.size() > 0) {

                    txt_category.setText("Select Category Of Crime");
                    crimeCategoryString = "";
                    crimeCategory_status = true;


                    customDialogAdapterForCategoryCrimeList2 = new CustomDialogAdapterForCategoryCrimeList2(this, obj_categoryCrimeList2);
                    customDialogForCategoryCrimeList();
                } else {
                    Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                }
                break;

            case R.id.btn_done:

                if (!Constants.internetOnline(this)) {

                    Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
                    return;

                }
                preferenceAPICall();
                break;

            case R.id.btn_reset:

                if (!chk_kp.isChecked() && !chk_division.isChecked() && !chk_ps.isChecked() && txt_category.getText().toString().equalsIgnoreCase("Select Category Of Crime")) {
                    Utility.showToast(this,"Already reset!!","short");
                }
                else {

                    alertShowToReset();
                }
                break;
            case R.id.charseet_due_notification :
                    getChargesheetdetailsAsperRole(this);
                    break;



        }
    }


    private void preferenceAPICall() {

        if(!state){
            finish();
        }else{

        }

        if (txt_division.getVisibility() == View.VISIBLE && txt_division.getText().toString().equalsIgnoreCase("Select Divisions")) {
            Log.e("MOITRI:", "22 a");
            txt_division.setError("");
            Utility.showAlertDialog(this, "Alert!!", "Please select division", false);
            return;
        } else if (txt_ps.getVisibility() == View.VISIBLE && txt_ps.getText().toString().equalsIgnoreCase("Select Police Stations")) {
            Log.e("MOITRI:", "33 a");
            txt_ps.setError("");
            Utility.showAlertDialog(this, "Alert!!", "Please select PS", false);
            return;
        } else {

        }


        DashboardActivity.editor_forinnerData.clear().commit();

        if (chk_kp.isChecked() /*&& chk_division.isChecked() && chk_ps.isChecked()*/) {

            Log.e("MOITRI:", "11");
            DashboardActivity.editor_forinnerData.putBoolean(DashboardActivity.TAG_CHECKED_KP, true);
            div = "";
            ps = "";
            DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_DIV_CODE_LIST_INNER, div);
            DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_PS_ID_LIST_INNER, ps);

        }
        else{

        }

        if (!chk_kp.isChecked() && chk_division.isChecked() /*&& chk_ps.isChecked()*/) {

            Log.e("MOITRI:", "22 b");
            DashboardActivity.editor_forinnerData.putBoolean(DashboardActivity.TAG_CHECKED_DIV, true);
            DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_DIV_NAME_LIST_INNER, txt_division.getText().toString());

            ps = "";
            DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_PS_ID_LIST_INNER, ps);

            if (div.equalsIgnoreCase("")) {
                div = divisionString;
                DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_DIV_CODE_LIST_INNER, div);
            } else if (!div.equalsIgnoreCase("") && !divisionString.equalsIgnoreCase("")) {
                div = divisionString;
                DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_DIV_CODE_LIST_INNER, div);
            } else {
                DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_DIV_CODE_LIST_INNER, div);
            }

        }
        else{
            div ="";
            DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_DIV_CODE_LIST_INNER, div);
        }

        if (!chk_kp.isChecked() && !chk_division.isChecked() && chk_ps.isChecked()) {

            Log.e("MOITRI:", "33 b");
            DashboardActivity.editor_forinnerData.putBoolean(DashboardActivity.TAG_CHECKED_PS, true);

            DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_PS_NAME_LIST_INNER, txt_ps.getText().toString());
            div = "";
            DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_DIV_CODE_LIST_INNER, div);

            if (ps.equalsIgnoreCase("")) {
                ps = policeStationString;
                DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_PS_ID_LIST_INNER, ps);
            } else if (!ps.equalsIgnoreCase("") && !policeStationString.equalsIgnoreCase("")) {
                ps = policeStationString;
                DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_PS_ID_LIST_INNER, ps);
            } else {
                DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_PS_ID_LIST_INNER, ps);
            }
        }
        else {
            ps = "";
            DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_PS_ID_LIST_INNER, ps);
        }


        if (!txt_category.getText().toString().equalsIgnoreCase("Select Category Of Crime")) {
            if (crime.equalsIgnoreCase("")) {
                crime = crimeCategoryString;
                DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_CRIME_CATEGORY_LIST_INNER, crime);
            } else if (!crime.equalsIgnoreCase("") && !crimeCategoryString.equalsIgnoreCase("")) {
                crime = crimeCategoryString;
                DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_CRIME_CATEGORY_LIST_INNER, crime);
            } else {
                DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_CRIME_CATEGORY_LIST_INNER, crime);
            }
            DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_CRIME_CATEGORY_SHOW_LIST_INNER, txt_category.getText().toString());
        } else {
            crime = "";
            DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_CRIME_CATEGORY_LIST_INNER, crime);
        }

        DashboardActivity.editor_forinnerData.commit();

        if (!chk_kp.isChecked() && !chk_division.isChecked() && !chk_ps.isChecked() && txt_category.getText().toString().equalsIgnoreCase("Select Category Of Crime")) {
            Log.e("MOI:","all unchecked");
            txt_category.setText("Select Category Of Crime");
            //div = "";
            //ps = "";
            //crime = "";
            DashboardActivity.editor_forinnerData.clear().commit();
            //finish();
        } else {

        }


        if (!ps.equalsIgnoreCase("") && policeStationString.equalsIgnoreCase("")) {
            ps = ps;
        } else if (!policeStationString.equalsIgnoreCase("")) {
            ps = policeStationString;
        } else {
            ps = "";
        }


        if (!div.equalsIgnoreCase("") && divisionString.equalsIgnoreCase("")) {
            div = div;
        } else if (!divisionString.equalsIgnoreCase("")) {
            div = divisionString;
        } else {
            div = "";
        }

        if (!crime.equalsIgnoreCase("") && crimeCategoryString.equalsIgnoreCase("")) {
            crime = crime;
        } else if (!crimeCategoryString.equalsIgnoreCase("")) {
            crime = crimeCategoryString;
        } else {
            crime = "";
        }


        DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_DIV_CODE_LIST_INNER,div);
        DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_PS_ID_LIST_INNER,ps);
        DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_CRIME_CATEGORY_LIST_INNER,crime);
        DashboardActivity.editor_forinnerData.commit();


        Intent intent = new Intent();
        intent.putExtra("DATE_PREFERENCE", selectedDate);
        intent.putExtra("DIV_PREFERENCE", div);
        intent.putExtra("PS_PREFERENCE", ps);
        intent.putExtra("CRIME_CAT_PREFERENCE", crime);
        setResult(RESULT_OK, intent);
       // onBackPressed();
        finish();
    }


    private void alertShowToReset() {

        final AlertDialog ad = new AlertDialog.Builder(this).create();

        // Setting Dialog Title
        ad.setTitle("Reset Data!!!");

        // Setting Dialog Message
        ad.setMessage("Do you want to reset previous preferences?");


        ad.setButton(DialogInterface.BUTTON_POSITIVE,
                "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (ad != null && ad.isShowing()) {
                            state = true;
                            resetAllFields();
                        }

                    }
                });

        ad.setButton(DialogInterface.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (ad != null && ad.isShowing()) {
                    state = false;
                    ad.dismiss();
                }
            }
        });

        // Showing Alert Message
        ad.show();
        ad.setCanceledOnTouchOutside(false);
    }

    private void resetAllFields() {

        Log.e("RESET:","ResetCall");
        DashboardActivity.editor_forinnerData.clear().commit();


        //DashboardActivity.editor_dashboardData.clear().commit();

        chk_kp.setChecked(false);
        chk_division.setChecked(false);
        chk_ps.setChecked(false);


        txt_division.setVisibility(View.GONE);
        resetDivisionValue();

        txt_ps.setVisibility(View.GONE);
        resetPSValue();

        txt_category.setText("Select Category Of Crime");
        crimeCategoryString = "";

        div = "";
        ps = "";
        crime = "";

      /*  DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_DIV_CODE_LIST_INNER,div);
        DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_PS_ID_LIST_INNER,ps);
        DashboardActivity.editor_forinnerData.putString(DashboardActivity.TAG_CRIME_CATEGORY_LIST_INNER,crime);
        DashboardActivity.editor_forinnerData.commit();*/
    }


    protected void showSelectPoliceStationsDialog() {
        boolean[] checkedPoliceStations = new boolean[array_policeStation.length];
        int count = array_policeStation.length;

        for (int i = 0; i < count; i++) {
            checkedPoliceStations[i] = selectedPoliceStations.contains(array_policeStation[i]);
        }

        DialogInterface.OnMultiChoiceClickListener policeStationsDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedPoliceStations.add(array_policeStation[which]);
                    selectedPoliceStationsId.add((CharSequence) policeStationIDArrayList.get(which));
                } else {
                    selectedPoliceStations.remove(array_policeStation[which]);
                    selectedPoliceStationsId.remove((CharSequence) policeStationIDArrayList.get(which));
                }

            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Police Stations");
        builder.setMultiChoiceItems(array_policeStation, checkedPoliceStations, policeStationsDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button

                onChangeSelectedPoliceStations();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                txt_ps.setText("Select Police Stations");
                //policeStationString="";
                selectedPoliceStations.clear();
                selectedPoliceStationsId.clear();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedPoliceStations() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for (CharSequence policeStation : selectedPoliceStations) {
            stringBuilder.append(policeStation + ",");
        }

        for (CharSequence policeStation : selectedPoliceStationsId) {
            stringBuilderId.append("\'" + policeStation + "\',");
        }

        if (selectedPoliceStations.size() == 0) {
            txt_ps.setText("Select Police Stations");
            policeStationString = "";
        } else {
            txt_ps.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
            policeStationString = stringBuilderId.toString().substring(0, stringBuilderId.toString().length() - 1);
        }

    }


    protected void showSelectDivisionDialog() {
        boolean[] checkedDivisions = new boolean[array_division.length];
        int count = array_division.length;

        for (int i = 0; i < count; i++) {
            checkedDivisions[i] = selectedDivision.contains(array_division[i]);
        }

        DialogInterface.OnMultiChoiceClickListener divisionDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedDivision.add(array_division[which]);
                    selectedDivisionId.add((CharSequence) divisionArrayList_code.get(which));
                    //selectedDivisionId.add(array_division_code[which]);
                } else {
                    selectedDivision.remove(array_division[which]);
                    selectedDivisionId.remove((CharSequence) divisionArrayList_code.get(which));
                    //selectedDivisionId.remove(array_division_code[which]);
                }

            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Divisions");
        builder.setMultiChoiceItems(array_division, checkedDivisions, divisionDialogListener);

        // Set the positive/yes button click listener
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button

                onChangeSelectedDivision();
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
                txt_division.setText("Select Divisions");
//                divisionString="";
                selectedDivision.clear();
                selectedDivisionId.clear();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void onChangeSelectedDivision() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();

        for (CharSequence division : selectedDivision) {
            stringBuilder.append(division + ",");
        }

        for (CharSequence division_id : selectedDivisionId) {
            stringBuilderId.append("\'" + division_id + "\',");
        }

        if (selectedDivision.size() == 0) {
            txt_division.setText("Select Divisions");
            divisionString = "";
        } else {
            txt_division.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
            divisionString = stringBuilderId.toString().substring(0, stringBuilderId.toString().length() - 1);
        }

    }


    private void customDialogForCategoryCrimeList() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);

        TextView tv_title = (TextView) dialogView.findViewById(R.id.tv_title);
        Button btn_cancel = (Button) dialogView.findViewById(R.id.btn_cancel);
        Button btn_done = (Button) dialogView.findViewById(R.id.btn_done);
        lv_dialog = (ListView) dialogView.findViewById(R.id.lv_dialog);
        SearchView searchView = (SearchView) dialogView.findViewById(R.id.searchView);


        Constants.changefonts(tv_title, this, "Calibri Bold.ttf");
        tv_title.setText("Select Crime Category");

        customDialogAdapterForCategoryCrimeList2.notifyDataSetChanged();
        lv_dialog.setAdapter(customDialogAdapterForCategoryCrimeList2);
        //lv_dialog.setScrollingCacheEnabled(false);
        lv_dialog.setTextFilterEnabled(true);

        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(PreferenceActivity.this);
        searchView.setSubmitButtonEnabled(false);
        searchView.setQueryHint("Search Here");


        final AlertDialog alertDialog = builder.show();

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                crimeCategory_status = false;
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();

                }
                crimeCategory_status = false;

            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("No of Selected Items: " + CustomDialogAdapterForCategoryCrimeList2.selectedItemList.size());
                System.out.println("Selected Items: " + CustomDialogAdapterForCategoryCrimeList2.selectedItemList);
                crimeCategory_status = false;

                onSelectedCategoryOfCrime(CustomDialogAdapterForCategoryCrimeList2.selectedItemList);

                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            }
        });
    }


    private void onSelectedCategoryOfCrime(List<String> crimeCategoryList) {

        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderCrimeCategory = new StringBuilder();

        for (CharSequence crimeCategory : crimeCategoryList) {
            stringBuilder.append(crimeCategory + ",");

            String crimeCategoryModifiedString = stringBuilderCrimeCategory.append("\'" + crimeCategory.toString() + "\',").toString();

            crimeCategoryString = crimeCategoryModifiedString.substring(0, crimeCategoryModifiedString.length() - 1);
        }

        if (crimeCategoryList.size() == 0) {
            txt_category.setText("Select Category Of Crime");
        } else {
            System.out.println("Crime Category String: " + crimeCategoryString);
            txt_category.setText(stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1));
        }

    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (crimeCategory_status) {
            customDialogAdapterForCategoryCrimeList2.filter(newText.toString());
        }
        return true;
    }


    private void fetchAllContent() {

        if (!Constants.internetOnline(this)) {
            Utility.showAlertDialog(this, Constants.ERROR_TITLE, Constants.ERROR_MSG, false);
            return;
        }

        TaskManager taskManager = new TaskManager(this);
        taskManager.setMethod(Constants.METHOD_GETCONTENT);
        taskManager.setAllContentPreference(true);

        String[] keys = {};
        String[] values = {};
        taskManager.doStartTask(keys, values, true);
    }

    public void parseGetAllContentResponse(String response) {
        try {

            //System.out.println("Get All Content Result:------- " + response);

            if (response != null && !response.equals("")) {

                try {
                    Constants.divisionArrayList.clear();
                    Constants.divCodeArrayList.clear();
                    Constants.crimeCategoryArrayList.clear();
                    Constants.policeStationIDArrayList.clear();
                    Constants.policeStationNameArrayList.clear();

                    JSONObject jObj = new JSONObject(response);
                    allContentList = new AllContentList();
                    if (jObj != null && jObj.optString("status").equalsIgnoreCase("1")) {

                        allContentList.setStatus(jObj.optString("status"));
                        allContentList.setMessage(jObj.optString("message"));

                        policeStationListArray = jObj.getJSONObject("result").getJSONArray("policestationlist");
                        crimeCategoryListArray = jObj.getJSONObject("result").getJSONArray("crimecategorylist");
                        divisionListArray = jObj.getJSONObject("result").getJSONArray("divisionlist");

                        for (int i = 0; i < policeStationListArray.length(); i++) {

                            PoliceStationList policeStationList = new PoliceStationList();

                            JSONObject row = policeStationListArray.getJSONObject(i);
                            policeStationList.setPs_code(row.optString("PSCODE"));
                            policeStationList.setPs_name(row.optString("PSNAME"));
                            policeStationList.setDiv_code(row.optString("DIVCODE"));

                            policeStationArrayList.add(row.optString("PSNAME"));

                            Constants.policeStationNameArrayList.add(row.optString("PSNAME"));
                            Constants.policeStationIDArrayList.add(row.optString("PSCODE"));

                            allContentList.setObj_policeStationList(policeStationList);

                        }

                        for (int j = 0; j < crimeCategoryListArray.length(); j++) {

                            CrimeCategoryList crimeCategoryList = new CrimeCategoryList();

                            JSONObject row = crimeCategoryListArray.getJSONObject(j);
                            crimeCategoryList.setCategory(row.optString("CATEGORY"));
                            crimeCategoryList.setCategory_desc(row.optString("CATEGORYDESC"));
                            crimeCategoryList.setBroad_category(row.optString("BROAD_CATEGORY"));
                            crimeCategoryList.setIPC_SSl(row.optString("IPC_SLL"));
                            crimeCategoryList.setSections(row.optString("SECTIONS"));
                            crimeCategoryList.setVerified(row.optString("VERIFIED"));
                            crimeCategoryList.setFavourite(row.optString("FAVOURITE"));

                            Constants.crimeCategoryArrayList.add(row.optString("CATEGORY"));


                            allContentList.setObj_categoryCrimeList(crimeCategoryList);

                        }

                        //createModifiedCrimeCategoryList(allContentList.getObj_categoryCrimeList());

                        for (int k = 0; k < divisionListArray.length(); k++) {

                            DivisionList divisionList = new DivisionList();
                            JSONObject row = divisionListArray.getJSONObject(k);
                            divisionList.setDiv_name(row.optString("DIVNAME"));
                            divisionList.setDiv_code(row.optString("DIVCODE"));

                            divisionArrayList.add(row.optString("DIVNAME"));

                            Constants.divisionArrayList.add(row.optString("DIVNAME"));
                            Constants.divCodeArrayList.add(row.optString("DIVCODE"));

                            allContentList.setObj_DivisionList(divisionList);
                        }

                        obj_policeStationList = allContentList.getObj_policeStationList();
                    } else {

                        Utility.showAlertDialog(this, Constants.SEARCH_ERROR_TITLE, Constants.ERROR_MSG_TO_RELOAD, false);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            array_policeStation = new String[Constants.policeStationNameArrayList.size()];
            array_policeStation = Constants.policeStationNameArrayList.toArray(array_policeStation);

            array_division = new String[Constants.divisionArrayList.size()];
            array_division = Constants.divisionArrayList.toArray(array_division);

            // Save data to shared preference
            json_psNameList = gson.toJson(Constants.policeStationNameArrayList);
            json_psIDList = gson.toJson(Constants.policeStationIDArrayList);
            json_divNameList = gson.toJson(Constants.divisionArrayList);
            json_divCodeList = gson.toJson(Constants.divCodeArrayList);
            json_crimeCatList = gson.toJson(allContentList.getObj_categoryCrimeList());

            editor.putString(TAG_PS_NAME_LIST, json_psNameList);
            editor.putString(TAG_PS_ID_LIST, json_psIDList);
            editor.putString(TAG_DIV_NAME_LIST, json_divNameList);
            editor.putString(TAG_DIV_CODE_LIST, json_divCodeList);
            editor.putString(TAG_CRIME_CATEGORY_LIST, json_crimeCatList);

            editor.commit();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void createModifiedCrimeCategoryList(List<CrimeCategoryList> ccList) {

        modified_crimeCategoryList.clear();
        System.out.println("CCList Size: " + ccList.size());

        obj_categoryCrimeList2 = new ArrayList<CrimeCategoryList2>();

        List<String> ccFavouriteList = new ArrayList<String>();
        List<String> ccNonFavouriteList = new ArrayList<String>();

        for (int i = 0; i < ccList.size(); i++) {

            if (ccList.get(i).getFavourite().equalsIgnoreCase("Y")) {
                ccFavouriteList.add(ccList.get(i).getCategory());
            } else {
                ccNonFavouriteList.add(ccList.get(i).getCategory());
            }

        }
        modified_crimeCategoryList.add("Frequently Used");
        modified_crimeCategoryList.addAll(ccFavouriteList);
        modified_crimeCategoryList.add("Others");
        modified_crimeCategoryList.addAll(ccNonFavouriteList);


        System.out.println("CC FAV Size: " + ccFavouriteList.size());
        System.out.println("CC FAV List: " + ccFavouriteList);
        System.out.println("CC Non FAV Size: " + ccNonFavouriteList.size());
        System.out.println("CC Non FAV List: " + ccNonFavouriteList);
        System.out.println("CC Non FAV List: " + modified_crimeCategoryList.size());

        for (int i = 0; i < modified_crimeCategoryList.size(); i++) {

            CrimeCategoryList2 crimeCategoryList2 = new CrimeCategoryList2();
            crimeCategoryList2.setCategory(modified_crimeCategoryList.get(i));

            obj_categoryCrimeList2.add(crimeCategoryList2);
        }

    }


    @Override
    public void update(Observable observable, Object data) {
        try {
            int count = (int) data;
            if (count > 0) {
                chargesheetNotificationCount.setVisibility(View.VISIBLE);
                if(!chargesheetNotificationCount.getText().toString().trim().equals(String.valueOf(data)))
                    chargesheetNotificationCount.setText(String.valueOf(count));
            }
            else
            {
                chargesheetNotificationCount.setVisibility(View.GONE);
            }
        }catch(Exception e){

        }
    }
    public void updateNotificationCount(String notificationCount){

        int count = Integer.parseInt(notificationCount);
        if (count > 0) {
            chargesheetNotificationCount.setVisibility(View.VISIBLE);
            chargesheetNotificationCount.setText(String.valueOf(count));
        }
        else
        {
            chargesheetNotificationCount.setVisibility(View.GONE);
        }

    }
}
