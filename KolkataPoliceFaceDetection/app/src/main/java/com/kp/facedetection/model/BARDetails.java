package com.kp.facedetection.model;

import java.io.Serializable;

/**
 * Created by user on 15-02-2018.
 */

public class BARDetails implements Serializable {
    private String name="";
    private String address="";
    private String slNo="";

    public String getSlNo() {
        return slNo;
    }

    public void setSlNo(String slNo) {
        this.slNo = slNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
